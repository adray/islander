#pragma once
#include "CollisionTypes.h"

namespace Islander
{
    class Camera3D;

    namespace Collision
    {
        struct CollisionStaticMesh;
        struct CollisionDynamicMesh;
    }

    namespace Physics
    {
        struct NonRealisticPhysicsSystem;

        void RayCastHeightmap(NonRealisticPhysicsSystem* system, Camera3D* camera, float px, float py, float pz, float dx, float dy, float dz, IslanderCollisionRayInfo* info);
        void RayCastFine(NonRealisticPhysicsSystem* system, Camera3D* camera, float px, float py, float pz, float dx, float dy, float dz, IslanderCollisionRayInfo* info);
        void RayCast(NonRealisticPhysicsSystem* system, Camera3D* camera, float px, float py, float pz, float dx, float dy, float dz, int group, IslanderCollisionRayInfo* info);
        void TransformRay(Camera3D* camera, float* px, float* py, float* pz, float* dx, float* dy, float* dz);
        //float CollisionSweep(CollisionSystem* system, CollisionDynamicMesh* mesh, float* vector);
        void CollisionOverlap(NonRealisticPhysicsSystem* system, Islander::Collision::CollisionDynamicMesh* mesh, IslanderCollisionOverlapInfo* info);
        float CollisionSweep(NonRealisticPhysicsSystem* system, Islander::Collision::CollisionStaticMesh* mesh, float* vector);
        bool TestRayBox(float px, float py, float pz, float dx, float dy, float dz, const IslanderCollisionOBB& box, float& dist);
        bool TestRayBox(float px, float py, float pz, float dx, float dy, float dz, const IslanderCollisionBox& box, float& dist);
    }
}
