#pragma once
#include <deque>
#include <vector>
#include <array>
#include "CollisionTypes.h"
#include "StaticCollisionGrid.h"
#include "DynamicCollisionGrid.h"
#include "CollisionPrimitives.h"

constexpr int PHYSICS_SYSTEM_MAX_STATIC_ITEMS = 4096;
constexpr int PHYSICS_SYSTEM_MAX_DYNAMIC_ITEMS = 128;
constexpr int PHYSICS_SYSTEM_DATA_ITEMS = ISLANDER_COLLISION_DATA_COUNT;

namespace Islander
{
    class Camera3D;

    namespace Model
    {
        struct PolygonModel;
    }

    namespace Physics
    {
        template<typename T, int size>
        struct CollisionArray
        {
            T array[size];
            int count;
            std::deque<int> freeslots;

            CollisionArray() : count(0) { }
        };

        struct NonRealisticPhysicsSystem
        {
            CollisionArray<Islander::Collision::CollisionStaticMesh, PHYSICS_SYSTEM_MAX_STATIC_ITEMS> static_mesh;
            CollisionArray<Islander::Collision::CollisionDynamicMesh, PHYSICS_SYSTEM_MAX_DYNAMIC_ITEMS> dynamic_mesh;
            CollisionArray<Islander::Collision::CollisionStaticMesh, PHYSICS_SYSTEM_MAX_STATIC_ITEMS> static_mesh_ray;
            CollisionArray<Islander::Collision::CollisionDynamicMesh, PHYSICS_SYSTEM_MAX_DYNAMIC_ITEMS> dynamic_mesh_ray;
            Islander::Collision::CollisionGrid grid;
            Islander::Collision::DynamicCollisionGrid dynGrid;
        };

        NonRealisticPhysicsSystem* CreateNonRealisticPhysicsSystem();
        void RunNonRealisticPhysics(NonRealisticPhysicsSystem* system, float delta);
        bool AddHeightmap(Islander::Physics::NonRealisticPhysicsSystem* system, bool rayonly, Islander::Model::Heightmap* heightmap, const Islander::Collision::CollisionTransform& transform, Islander::Collision::CollisionStaticMesh** mesh);
        bool AddStaticMesh(NonRealisticPhysicsSystem* system, bool rayonly, bool triangleCollider, int material, Islander::Model::PolygonModel* meshData, const Islander::Collision::CollisionTransform& transform, Islander::Collision::CollisionStaticMesh** mesh);
        bool AddDynamicMesh(NonRealisticPhysicsSystem* system, bool rayonly, Islander::Collision::CollisionDynamicMesh** mesh);
        void RemoveStaticMesh(NonRealisticPhysicsSystem* system, Islander::Collision::CollisionStaticMesh* mesh);
        void RemoveDynamicMesh(NonRealisticPhysicsSystem* system, Islander::Collision::CollisionDynamicMesh* mesh);
        void UpdateStaticMesh(NonRealisticPhysicsSystem* system, Islander::Collision::CollisionStaticMesh* mesh, IslanderCollisionStaticMeshInfo* meshInfo);
        void OverrideStaticMeshOBB(Islander::Collision::CollisionStaticMesh* mesh, float* verts);
        void CreateDebugGeometry(NonRealisticPhysicsSystem* system, IslanderCollisionGeometryData* gData);
        void CreateDebugGeometry(NonRealisticPhysicsSystem* system, Islander::Collision::CollisionDynamicMesh* mesh, IslanderCollisionGeometryData* gData);
        void QueryPhysicsSystem(NonRealisticPhysicsSystem* system, IslanderCollisionQueryInfo* info);
    }
}
