#pragma once
#include "PhysicsTypes.h"

namespace Islander
{
    namespace Physics
    {
        void* CreateJoltPhysics();
        void DestroyJoltPhysics(void* jolt);
        void AddJoltPhysicsBody(void* jolt, IslanderPhysicsBody& body);
        void RemoveJoltPhysicsBody(void* jolt, uint32_t id);
        void RunJoltPhysics(void* jolt, float dt);
        void RetrieveJoltPhysicsBody(void* jolt, IslanderPhysicsBody& body);
        void ApplyJoltPhysicsVelocity(void* jolt, IslanderPhysicsBody& body);
    }
}
