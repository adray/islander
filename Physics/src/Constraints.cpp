#include "Constraints.h"
//#include "CollisionPrimitives.h"
#include "Vector.h"
#include <assert.h>

//using namespace Islander::Collision;
//
//void AddConstraintToMesh(int constraintId, IslanderCollisionConstraintInfo* constraint)
//{
//    auto mesh1 = (Islander::Collision::CollisionDynamicMesh*)constraint->mesh1;
//    auto mesh2 = (Islander::Collision::CollisionDynamicMesh*)constraint->mesh2;
//    
//    mesh1->constraints[mesh1->constraintCount++] = constraintId;
//    mesh2->constraints[mesh2->constraintCount++] = constraintId;
//}
//
//void RemoveConstraintFromMesh(int constraintId, CollisionDynamicMesh* mesh)
//{
//    for (int i = 0; i < mesh->constraintCount; i++)
//    {
//        if (mesh->constraints[i] == constraintId)
//        {
//            mesh->constraintCount--;
//            mesh->constraints[i] = mesh->constraints[mesh->constraintCount];
//            break;
//        }
//    }
//}
//
//bool ValidateConstraint(IslanderCollisionConstraintInfo* constraint)
//{
//    switch (constraint->type)
//    {
//    case ISLANDER_COLLISION_CONSTRAINT_TWO_BODY:
//        if (constraint->mesh1 != nullptr &&
//            constraint->mesh2 != nullptr &&
//            constraint->minDist >= 0.0f &&
//            constraint->maxDist >= constraint->minDist)
//        {
//            return ((Islander::Collision::CollisionDynamicMesh*)constraint->mesh1)->constraintCount < ISLANDER_COLLISION_CONSTRAINT_COUNT &&
//                ((Islander::Collision::CollisionDynamicMesh*)constraint->mesh2)->constraintCount < ISLANDER_COLLISION_CONSTRAINT_COUNT;
//        }
//        break;
//    default:
//        break;
//    }
//
//    return false;
//}
//
//int Islander::Collision::AddConstraint(Constraints* constraints, IslanderCollisionConstraintInfo* constraint)
//{
//    if (!ValidateConstraint(constraint))
//    {
//        return -1;
//    }
//
//    if (constraints->freeSlots.size() > 0)
//    {
//        int slotId = constraints->freeSlots[0];
//
//        // Move last constraint to start and erase last slot.
//        constraints->freeSlots[0] = constraints->freeSlots[constraints->freeSlots.size() - 1];
//        constraints->freeSlots.erase(constraints->freeSlots.end());
//
//        constraints->constraints[slotId] = *constraint;
//
//        AddConstraintToMesh(slotId, constraint);
//        return slotId;
//    }
//
//    if (constraints->count == ISLANDER_CONSTRAINT_MAX_COUNT)
//    {
//        return -1;
//    }
//
//    int id = constraints->count;
//    constraints->constraints[id++] = *constraint;
//    AddConstraintToMesh(id, constraint);
//    return id;
//}
//
//void Islander::Collision::RemoveConstraint(Constraints* constraints, int constraintId)
//{
//    constraints->freeSlots.push_back(constraintId);
//
//    auto& constraint = constraints->constraints[constraintId];
//
//    switch (constraint.type)
//    {
//    case ISLANDER_COLLISION_CONSTRAINT_TWO_BODY:
//        RemoveConstraintFromMesh(constraintId, (CollisionDynamicMesh*)constraint.mesh1);
//        RemoveConstraintFromMesh(constraintId, (CollisionDynamicMesh*)constraint.mesh2);
//        break;
//    default:
//        break;
//    }
//}
//
//void TwoBodyConstraint(IslanderCollisionConstraintInfo& constraint, CollisionDynamicMesh* mesh, float* pos, float* velocity)
//{
//    CollisionDynamicMesh* other = (CollisionDynamicMesh*)constraint.mesh1 == mesh ? (CollisionDynamicMesh*)constraint.mesh2 : (CollisionDynamicMesh*)constraint.mesh1;
//    assert(other->active);
//
//    float dist[3];
//    dist[0] = pos[0] - other->info.px + velocity[0];
//    dist[1] = pos[1] - other->info.py + velocity[1];
//    dist[2] = pos[2] - other->info.pz + velocity[2];
//
//    float distSqrt = Islander::Numerics::LengthSqr(dist);
//    if (distSqrt >= constraint.minDist && distSqrt <= constraint.maxDist)
//    {
//        return;
//    }
//
//    // TODO: solve constraints
//    // Need object masses.
//
//    if (distSqrt < constraint.minDist)
//    {
//        velocity[0] = 0.0f;
//        velocity[1] = 0.0f;
//        velocity[2] = 0.0f;
//    }
//
//    if (distSqrt > constraint.maxDist)
//    {
//        velocity[0] = 0.0f;
//        velocity[1] = 0.0f;
//        velocity[2] = 0.0f;
//    }
//}
//
//void Islander::Collision::ResolveConstraints(Constraints* constraints, CollisionDynamicMesh* mesh, float* pos, float* velocity)
//{
//    for (int i = 0; i < mesh->constraintCount; i++)
//    {
//        auto& constraint = constraints->constraints[mesh->constraints[i]];
//        switch (constraint.type)
//        {
//        case ISLANDER_COLLISION_CONSTRAINT_TWO_BODY:
//            TwoBodyConstraint(constraint, mesh, pos, velocity);
//            break;
//        }
//    }
//}
