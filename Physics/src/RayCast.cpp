#include <unordered_set>
#include <algorithm>
#include "RayCast.h"
#include "StaticCollisionGrid.h"
#include "EPA.h"
#include "Component.h"
#include "NonRealisticPhysics.h"
#include "CollisionPrimitives.h"
#include "Heightmap.h"

using namespace Islander;
using namespace Islander::Collision;
using namespace Islander::Numerics;

void CreatePlane(Plane2* p, const IslanderCollisionOBB& obb, int index, bool flip)
{
    p->n[0] = obb.extents[index * 3 + 0] - obb.centre[0];
    p->n[1] = obb.extents[index * 3 + 1] - obb.centre[1];
    p->n[2] = obb.extents[index * 3 + 2] - obb.centre[2];

    if (flip)
    {
        p->n[0] = -p->n[0];
        p->n[1] = -p->n[1];
        p->n[2] = -p->n[2];
    }

    Islander::Numerics::NormalizeVec3(p->n);

    p->p[0] = obb.extents[index * 3 + 0];
    p->p[1] = obb.extents[index * 3 + 1];
    p->p[2] = obb.extents[index * 3 + 2];

    p->d = -Dot(p->p[0], p->p[1], p->p[2], p->n[0], p->n[1], p->n[2]);
}

//bool Islander::Physics::TestRayBox(float px, float py, float pz, float dx, float dy, float dz, const IslanderCollisionOBB& box, float& dist)
//{
//    //float c[3] =
//    //{
//    //    (box.minx + box.maxx) / 2,
//    //    (box.miny + box.maxy) / 2,
//    //    (box.minz + box.maxz) / 2
//    //};
//    //float tmin[3] = {
//    //    c[0] - box.minx,
//    //    c[1] - box.miny,
//    //    c[2] - box.minz
//    //};
//    //float tmax[3] = {
//    //    box.maxx - c[0],
//    //    box.maxy - c[1],
//    //    box.maxz - c[2]
//    //};
//
//    // for each plane/slab check if they intersect with ray
//    Plane2 plane[6];
//    CreatePlane(&plane[0], box, 0, false);
//    CreatePlane(&plane[1], box, 1, false);
//    CreatePlane(&plane[2], box, 2, false);
//
//    CreatePlane(&plane[3], box, 3, true);
//    CreatePlane(&plane[4], box, 4, true);
//    CreatePlane(&plane[5], box, 5, true);
//
//    float di[6] = {
//        std::numeric_limits<float>::max(),
//        std::numeric_limits<float>::max(),
//        std::numeric_limits<float>::max(),
//        std::numeric_limits<float>::lowest(),
//        std::numeric_limits<float>::lowest(),
//        std::numeric_limits<float>::lowest()
//    };
//
//    float o[3] = { px, py, pz };
//    float d[3] = { dx, dy, dz };
//
//    for (int i = 0; i < 3; i++)
//    {
//        float t1, t2;
//
//        bool b1 = RayPlaneTest(o, d, &plane[i], t1) && t1 >= 0.0f;
//        bool b2 = RayPlaneTest(o, d, &plane[i + 3], t2) && t2 >= 0.0f;
//
//        if (!b1 || !b2)
//        {
//            float* n1 = plane[i].n;
//            float* n2 = plane[i + 3].n;
//            float dot1 = Dot(n1[0], n1[1], n1[2], o[0], o[1], o[2]) + plane[i].d;
//            float dot2 = Dot(n2[0], n2[1], n2[2], o[0], o[1], o[2]) + plane[i + 3].d;
//
//            if ((dot1 > 0 && dot2 > 0) || (dot1 < 0 && dot2 < 0))
//            {
//                return false;
//            }
//        }
//        else
//        {
//            if (t1 > 0 && t2 > 0)
//            {
//                if (t1 < t2)
//                {
//                    di[i] = t1;
//                    di[i + 3] = t2;
//                }
//                else
//                {
//                    di[i] = t2;
//                    di[i + 3] = t1;
//                }
//            }
//            else if (t1 > 0 && t2 < 0)
//            {
//                di[i] = t1;
//                di[i + 3] = t1;
//            }
//            else if (t2 > 0 && t1 < 0)
//            {
//                di[i] = t2;
//                di[i + 3] = t2;
//            }
//        }
//    }
//
//    float mind = std::min(di[0], std::min(di[1], di[2]));
//    float maxd = std::max(di[3], std::max(di[4], di[5]));
//
//    if (mind <= maxd)
//    {
//        if (mind > 0)
//        {
//            dist = mind;
//        }
//        else
//        {
//            dist = maxd;
//        }
//
//        return true;
//    }
//
//    return false;
//}

bool Islander::Physics::TestRayBox(float px, float py, float pz, float dx, float dy, float dz, const IslanderCollisionOBB& box, float& dist)
{
    // TODO: this appears it may be broken?

    // for each plane/slab check if they intersect with ray
    Plane2 plane[6];
    CreatePlane(&plane[0], box, 0, false);
    CreatePlane(&plane[1], box, 1, false);
    CreatePlane(&plane[2], box, 2, false);

    CreatePlane(&plane[3], box, 3, true);
    CreatePlane(&plane[4], box, 4, true);
    CreatePlane(&plane[5], box, 5, true);

    float di[6] = {
        std::numeric_limits<float>::lowest(),
        std::numeric_limits<float>::lowest(),
        std::numeric_limits<float>::lowest(),
        std::numeric_limits<float>::max(),
        std::numeric_limits<float>::max(),
        std::numeric_limits<float>::max()
    };

    // For each set of planes
    // Calculate the minT and maxT
    // If they are negative ignore

    float o[3] = { px, py, pz };
    float d[3] = { dx, dy, dz };

    for (int i = 0; i < 3; i++)
    {
        float t1, t2;

        bool b1 = RayPlaneTest(o, d, &plane[i], t1) && t1 >= 0.0f;
        bool b2 = RayPlaneTest(o, d, &plane[i + 3], t2) && t2 >= 0.0f;

        if (b1)
        {
            di[i] = std::min(t1, t2);
        }

        if (b2)
        {
            di[i * 2] = std::max(t1, t2);
        }

        // If perpendicular to the normal
        // we need to test if it is within the slab, if not no intersection occurs
        if (!b1 && !b2) // TODO: this may not mean it is perpendicular...
        {
            float d1 = DotVec3(plane[i].n, o) + plane[i].d;
            float d2 = DotVec3(plane[i + 3].n, o) + plane[i + 3].d;

            if ((d1 > 0 && d2 > 0) || (d1 < 0 && d2 < 0))
            {
                return false;
            }
        }
    }

    float mind = std::max(di[0], std::max(di[1], di[2]));
    float maxd = std::min(di[3], std::min(di[4], di[5]));

    if (mind <= maxd && mind >= 0.0f)
    {
        dist = mind;
        return true;
    }

    return false;
}

bool Islander::Physics::TestRayBox(float px, float py, float pz, float dx, float dy, float dz, const IslanderCollisionBox& box, float& dist)
{
    float c[3] =
    {
        (box.minx + box.maxx) / 2,
        (box.miny + box.maxy) / 2,
        (box.minz + box.maxz) / 2
    };
    float tmin[3] = {
        c[0] - box.minx,
        c[1] - box.miny,
        c[2] - box.minz
    };
    float tmax[3] = {
        box.maxx - c[0],
        box.maxy - c[1],
        box.maxz - c[2]
    };

    // for each plane/slab check if they intersect with ray
    Plane2 plane[6];
    CreatePlane(&plane[0], 0.0f, 1.0f, 0.0f, c[0], c[1] - tmin[1], c[2]);
    CreatePlane(&plane[1], 1.0f, 0.0f, 0.0f, c[0] - tmin[0], c[1], c[2]);
    CreatePlane(&plane[2], 0.0f, 0.0f, 1.0f, c[0], c[1], c[2] - tmin[2]);

    CreatePlane(&plane[3], 0.0f, 1.0f, 0.0f, c[0], c[1] + tmax[1], c[2]);
    CreatePlane(&plane[4], 1.0f, 0.0f, 0.0f, c[0] + tmax[0], c[1], c[2]);
    CreatePlane(&plane[5], 0.0f, 0.0f, 1.0f, c[0], c[1], c[2] + tmax[2]);

    float o[3] = { px, py, pz };
    float d[3] = { dx, dy, dz };

    float minT = std::numeric_limits<float>::max();
    bool hit = false;

    for (int i = 0; i < 6; i++)
    {
        float t;
        bool b = RayPlaneTest(o, d, &plane[i], t) && t >= 0;

        if (b)
        {
            float hitPos[3] = {
                o[0] + d[0] * t,
                o[1] + d[1] * t,
                o[2] + d[2] * t,
            };
            if (t < minT && PointBoxCollision(box, hitPos[0], hitPos[1], hitPos[2]))
            {
                minT = t;
                hit = true;
            }
        }
    }

    dist = minT;
    return hit;
}

//void GetDynamicMeshBoundingBox(CollisionDynamicMesh* dynamic_mesh, IslanderCollisionBox& box)
//{
//    int shape = dynamic_mesh->info.collisionShape;
//    switch (shape)
//    {
//    case ISLANDER_COLLISION_SHAPE_BOX:
//        box = dynamic_mesh->info.box;
//        Islander::Collision::CreateTransformedAABB(dynamic_mesh->info.px, dynamic_mesh->info.py, dynamic_mesh->info.pz,
//            dynamic_mesh->info.rx, dynamic_mesh->info.ry, dynamic_mesh->info.rz,
//            dynamic_mesh->info.sx, dynamic_mesh->info.sy, dynamic_mesh->info.sz, box);
//        break;
//    case ISLANDER_COLLISION_SHAPE_CAPSULE:
//        IslanderCollisionCapsule capsule = dynamic_mesh->info.capsule;
//        GetBoundingBox(capsule, box);
//        break;
//    case ISLANDER_COLLISION_SHAPE_SPHERE:
//        IslanderCollisionSphere sphere = dynamic_mesh->info.sphere;
//        GetBoundingBox(sphere, box);
//        break;
//    }
//}

template<typename T>
void RayCastDynamcMeshGridQuery(DynamicCollisionGrid& grid, T** closest, T* ignore, float* minDist, float px, float py, float pz, float dx, float dy, float dz)
{
    //dx = 1 / dx;
    //dy = 1 / dy;
    //dz = 1 / dz;

    for (int i = 0; i < grid.cells.size(); i++)
    {
        auto& cell = grid.cells[i];
        const IslanderCollisionBox& box = grid.cells[i].box;
        float dist;
        if (cell.items.size() > 0 && Islander::Physics::TestRayBox(px, py, pz, dx, dy, dz, box, dist))
        {
            for (int j = 0; j < cell.items.size(); j++)
            {
                if (ignore == cell.items[j]) {
                    continue;
                }
                IslanderCollisionBox mesh_box;
                GetBoundingBox(cell.items[j], mesh_box);
                if (Islander::Physics::TestRayBox(px, py, pz, dx, dy, dz, mesh_box, dist) && dist < *minDist)
                {
                    *minDist = dist;
                    *closest = cell.items[j];
                }
            }
        }
    }
}

template<typename T>
void RayCastDynamcMesh(T* elements, int count, T** closest, T* ignore, float* minDist, float px, float py, float pz, float dx, float dy, float dz)
{
    //dx = 1 / dx;
    //dy = 1 / dy;
    //dz = 1 / dz;

    for (int i = 0; i < count; i++)
    {
        T* dynamic_mesh = &elements[i];
        if (dynamic_mesh == ignore || !dynamic_mesh->active)
        {
            continue;
        }

        IslanderCollisionBox box;
        GetBoundingBox(dynamic_mesh, box);

        float dist;
        if (Islander::Physics::TestRayBox(px, py, pz, dx, dy, dz, box, dist) && dist < *minDist)
        {
            *minDist = dist;
            *closest = dynamic_mesh;
        }
    }
}

static bool RayTriTest(IslanderCollisionTriangle& tri, float* o, float* dir, float* pt, float* t)
{
    // Real-time rendering Ray/Triangle intersection 22.8

    float e1[3];
    float e2[3];

    SubVec3(tri.vertices[1], tri.vertices[0], e1);
    SubVec3(tri.vertices[2], tri.vertices[0], e2);

    float q[3];
    CrossVec3(dir, e2, q);

    float a = Dot(e1[0], e1[1], e1[2], q[0], q[1], q[2]);

    const float epsilon = 0.00001f;
    if (a > -epsilon && a < epsilon)
    {
        return false;
    }

    float f = 1 / a;

    float s[3];
    SubVec3(o, tri.vertices[0], s);
    float u = f * Dot(s[0], s[1], s[2], q[0], q[1], q[2]);
    if (u < 0)
    {
        return false;
    }

    float r[3];
    CrossVec3(s, e1, r);
    float v = f * Dot(dir[0], dir[1], dir[2], r[0], r[1], r[2]);
    if (v < 0 || u + v > 1)
    {
        return false;
    }

    *t = f * Dot(e2[0], e2[1], e2[2], r[0], r[1], r[2]);
    if (*t < 0)
    {
        return false;
    }

    pt[0] = o[0] + dir[0] * *t;
    pt[1] = o[1] + dir[1] * *t;
    pt[2] = o[2] + dir[2] * *t;

    return true;
}

template<typename T>
void RayCastStaticMesh(T* elements, int count, T** closest, T* ignore, float* minDist, float px, float py, float pz, float dx, float dy, float dz)
{
    for (int i = 0; i < count; i++)
    {
        T* static_mesh = &elements[i];
        if (static_mesh == ignore || !static_mesh->active)
        {
            continue;
        }

        //if (static_mesh->hit)
        //{
        //    continue;
        //}

        float dist;
        if (Islander::Physics::TestRayBox(px, py, pz, dx, dy, dz, static_mesh->parentBox, dist))
        {
            bool hit = false;

            float o[3] = { px,py,pz };
            float d[3] = { dx,dy,dz };

            if (static_mesh->heightmapCollider)
            {
                if (TestRayHeightmap(static_mesh->heightmap, static_mesh->transform, o, d, dist) && dist < *minDist)
                {
                    hit = true;
                }
            }
            else
            {
                float triDist = std::numeric_limits<float>::max();
                for (auto tri : static_mesh->triangles)
                {
                    float t;
                    float hitpos[3];
                    if (RayTriTest(tri, o, d, hitpos, &t) && t < triDist && t < *minDist)
                    {
                        hit = true;
                        dist = t;
                        triDist = t;
                    }
                }
            }

            if (hit)
            {
                *minDist = dist;
                *closest = static_mesh;
            }
        }
    }
}

#if 0
void RayCastStaticMeshSearch(Islander::Collision::CollisionGrid& grid, int* min, int* max, Islander::Collision::CollisionStaticMesh** closest, float* minDist, float* p, float* d, float cellSizeX, float cellSizeZ)
{
    IslanderCollisionBox box;
    box.minx = grid.box.minx + cellSizeX * min[0];
    box.maxx = grid.box.minx + cellSizeX * (max[0] + 1);
    box.miny = grid.box.miny;
    box.maxy = grid.box.maxy;
    box.minz = grid.box.minz + cellSizeZ * min[1];
    box.maxz = grid.box.minz + cellSizeZ * (max[1] + 1);

    float dist;
    if (TestRayBox(p[0], p[1], p[2], d[0], d[1], d[2], box, dist))
    {
        if (min[0] == max[0] && min[1] == max[1])
        {
            int x = min[0];
            int z = min[1];
            assert(x >= 0 && x < COLLISION_SYSTEM_GRID_COUNT);
            assert(z >= 0 && z < COLLISION_SYSTEM_GRID_COUNT);
            int index = z * COLLISION_SYSTEM_GRID_COUNT + x;
            auto cell = &grid.cells[index];

            for (auto& static_mesh : cell->entries)
            {
                if (!static_mesh->active)
                {
                    continue;
                }

                IslanderCollisionBox box = static_mesh->box;
                Islander::Collision::CreateTransformedAABB(static_mesh->transform.px, static_mesh->transform.py, static_mesh->transform.pz, static_mesh->transform.rx, static_mesh->transform.ry, static_mesh->transform.rz, static_mesh->transform.sx, static_mesh->transform.sy, static_mesh->transform.sz, box);

                float dist;
                if (TestRayBox(p[0], p[1], p[2], d[0], d[1], d[2], box, dist))
                {
                    bool hit = false;

                    for (auto tri : static_mesh->triangles)
                    {
                        float hitpos[3];
                        if (RayTriTest(tri, p, d, hitpos, &dist) && dist < *minDist)
                        {
                            hit = true;
                        }
                    }

                    if (hit)
                    {
                        *minDist = dist;
                        *closest = static_mesh;
                    }
                }
            }
        }
        else
        {
            int min1[2] = { min[0], min[1] };
            int max1[2] = { (min[0] + max[0]) / 2, (min[1] + max[1]) / 2 };

            int min2[2] = { (min[0] + max[0]) / 2 + 1, min[1] };
            int max2[2] = { max[0], (min[1] + max[1]) / 2 };

            int min3[2] = { min[0], (min[1] + max[1]) / 2 + 1 };
            int max3[2] = { (min[0] + max[0]) / 2, max[1] };

            int min4[2] = { (min[0] + max[0]) / 2 + 1, (min[1] + max[1]) / 2 + 1 };
            int max4[2] = { max[0], max[1] };

            RayCastStaticMeshSearch(grid, min1, max1, closest, minDist, p, d, cellSizeX, cellSizeZ);
            RayCastStaticMeshSearch(grid, min2, max2, closest, minDist, p, d, cellSizeX, cellSizeZ);
            RayCastStaticMeshSearch(grid, min3, max3, closest, minDist, p, d, cellSizeX, cellSizeZ);
            RayCastStaticMeshSearch(grid, min4, max4, closest, minDist, p, d, cellSizeX, cellSizeZ);
        }
    }
}

void RayCastStaticMeshSearch(Islander::Collision::CollisionGrid& grid, Islander::Collision::CollisionStaticMesh** closest, float* minDist, float px, float py, float pz, float dx, float dy, float dz)
{
    float cellSizeX = (grid.box.maxx - grid.box.minx) / COLLISION_SYSTEM_GRID_COUNT;
    float cellSizeZ = (grid.box.maxz - grid.box.minz) / COLLISION_SYSTEM_GRID_COUNT;

    float d[3] = { dx,dy,dz };
    float p[3] = { px,py,pz };
    int min[2] = { 0, 0 };
    int max[2] = { COLLISION_SYSTEM_GRID_COUNT - 1, COLLISION_SYSTEM_GRID_COUNT - 1 };

    RayCastStaticMeshSearch(grid, min, max, closest, minDist, p, d, cellSizeX, cellSizeZ);
}

void RayCastStaticMesh(Islander::Collision::CollisionGrid& grid, Islander::Collision::CollisionStaticMesh** closest, float* minDist, float px, float py, float pz, float dx, float dy, float dz)
{
    float cellSizeX = (grid.box.maxx - grid.box.minx) / COLLISION_SYSTEM_GRID_COUNT;
    float cellSizeZ = (grid.box.maxz - grid.box.minz) / COLLISION_SYSTEM_GRID_COUNT;

    // 1. Find the first cell to test against. This will be the cell the ray originates in. If the ray origin is outside the grid then find the first cell it intersects.
    std::unordered_set<Islander::Collision::CollisionCell*> visited;
    std::unordered_set<Islander::Collision::CollisionStaticMesh*> visitedMesh;
    std::queue<Islander::Collision::CollisionCell*> openlist;
    if (Islander::Collision::PointBoxCollision(grid.box, px, py, pz))
    {
        int x = std::min(COLLISION_SYSTEM_GRID_COUNT - 1, (int)((px - grid.box.minx) / cellSizeX));
        int z = std::min(COLLISION_SYSTEM_GRID_COUNT - 1, (int)((pz - grid.box.minz) / cellSizeZ));
        assert(x >= 0 && x < COLLISION_SYSTEM_GRID_COUNT);
        assert(z >= 0 && z < COLLISION_SYSTEM_GRID_COUNT);
        int index = z * COLLISION_SYSTEM_GRID_COUNT + x;
        auto cell = &grid.cells[index];
        openlist.push(cell);
        visited.insert(cell);
    }
    else
    {
        float dist = 0;
        if (TestRayBox(px, py, pz, dx, dy, dz, grid.box, dist))
        {
            float rx = px + dist / dx;
            float rz = pz + dist / dz;
            int x = std::min(COLLISION_SYSTEM_GRID_COUNT - 1, (int)((rx - grid.box.minx) / cellSizeX));
            int z = std::min(COLLISION_SYSTEM_GRID_COUNT - 1, (int)((rz - grid.box.minz) / cellSizeZ));
            assert(x >= 0 && x < COLLISION_SYSTEM_GRID_COUNT);
            assert(z >= 0 && z < COLLISION_SYSTEM_GRID_COUNT);
            int index = z * COLLISION_SYSTEM_GRID_COUNT + x;
            //assert(PointBoxCollision(cell->box, rx, py + dist / dy, rz));
            auto cell = &grid.cells[index];
            openlist.push(cell);
            visited.insert(cell);
        }
    }

    // 2. Start at the first cell and test on these objects.
    // If there isn't an object the ray hits then move to the next cell the rays passes.

    while (openlist.size() > 0)
    {
        Islander::Collision::CollisionCell* cell = openlist.front();
        openlist.pop();

        bool cellHit = false;
        for (auto& static_mesh : cell->entries)
        {
            if (!static_mesh->active || visitedMesh.find(static_mesh) == visitedMesh.end())
            {
                continue;
            }

            visitedMesh.insert(static_mesh);

            IslanderCollisionBox box = static_mesh->box;
            Islander::Collision::CreateTransformedAABB(static_mesh->transform.px, static_mesh->transform.py, static_mesh->transform.pz, static_mesh->transform.rx, static_mesh->transform.ry, static_mesh->transform.rz, static_mesh->transform.sx, static_mesh->transform.sy, static_mesh->transform.sz, box);

            float dist;
            if (TestRayBox(px, py, pz, dx, dy, dz, box, dist))
            {
                bool hit = false;

                float o[3] = { px,py,pz };
                float d[3] = { dx,dy,dz };

                for (auto tri : static_mesh->triangles)
                {
                    float hitpos[3];
                    if (RayTriTest(tri, o, d, hitpos, &dist) && dist < *minDist)
                    {
                        hit = true;
                    }
                }

                if (hit)
                {
                    *minDist = dist;
                    *closest = static_mesh;
                    cellHit = true;
                }
            }
        }

        //if (!cellHit)
        {
            for (int i = -1; i < 2; i++)
            {
                for (int j = -1; j < 2; j++)
                {
                    int zPrime = cell->z + i;
                    int xPrime = cell->x + j;
                    if (xPrime >= 0 && xPrime < COLLISION_SYSTEM_GRID_COUNT &&
                        zPrime >= 0 && zPrime < COLLISION_SYSTEM_GRID_COUNT &&
                        visited.find(cell) == visited.end())
                    {
                        visited.insert(cell);
                        int index = zPrime * COLLISION_SYSTEM_GRID_COUNT + xPrime;
                        auto cellNext = &grid.cells[index];
                        float dist = 0;
                        if (TestRayBox(px, py, pz, dx, dy, dz, cellNext->box, dist))
                        {
                            openlist.push(cellNext);
                        }
                    }
                }
            }
        }
    }
}
#endif

struct QuadTreeRayHit
{
    int index;
    float dist;
};

void RayQuadTree(Islander::Model::HeightmapChunk* heightmapChunk, float* pos, float* dir, std::vector<QuadTreeRayHit>& hits)
{
    if (heightmapChunk && pos)
    {
        std::queue<int> quadId;
        for (int i = 0; i < 4; i++)
        {
            auto& quad = heightmapChunk->quadTree[i];

            IslanderCollisionBox box;
            box.maxx = quad.max[0];
            box.maxy = quad.max[1];
            box.maxz = quad.max[2];
            box.minx = quad.min[0];
            box.miny = quad.min[1];
            box.minz = quad.min[2];

            float dist;
            if (Islander::Physics::TestRayBox(pos[0], pos[1], pos[2], dir[0], dir[1], dir[2], box, dist))
            {
                quadId.push(i);
            }
        }

        while (quadId.size() > 0)
        {
            int id = quadId.front(); 
            quadId.pop();

            auto& parent = heightmapChunk->quadTree[id];
            for (int i = 0; i < parent.numNodes; i++)
            {
                auto& quad = heightmapChunk->quadTree[parent.childNodes[i]];
                IslanderCollisionBox box;
                box.maxx = quad.max[0];
                box.maxy = quad.max[1];
                box.maxz = quad.max[2];
                box.minx = quad.min[0];
                box.miny = quad.min[1];
                box.minz = quad.min[2];

                float dist;
                if (Islander::Physics::TestRayBox(pos[0], pos[1], pos[2], dir[0], dir[1], dir[2], box, dist))
                {
                    if (quad.numNodes == 0)
                    {
                        QuadTreeRayHit hit;
                        hit.index = parent.childNodes[i];
                        hit.dist = dist;
                        hits.push_back(hit);
                    }
                    else
                    {
                        quadId.push(parent.childNodes[i]);
                    }
                }
            }
        }
    }
}

bool TestRayHeightmapChunk(Islander::Model::HeightmapChunk* chunk, Islander::Model::HeightmapDebugRayInfo& debug, const float vertexStride, float* o, float* dir, float hitPt[3], float& dist)
{
    dist = std::numeric_limits<float>::max();

    std::vector<QuadTreeRayHit> hits;
    RayQuadTree(chunk, o, dir, hits);

    // TODO: sort hits
    int bestIndex = -1;
    float bestDist = std::numeric_limits<float>::max();
    for (QuadTreeRayHit& hit : hits)
    {
        if (hit.dist < bestDist)
        {
            bestDist = hit.dist;
            bestIndex = hit.index;
        }
    }

    debug.quads += hits.size();

    bool hit = false;
    //while (bestIndex > -1)
    for (int k = 0; k < hits.size(); k++)
    {
        bestIndex = hits[k].index;

        auto& quad = chunk->quadTree[bestIndex];
        for (int i = 0; i < quad.numIndices.size(); i++) // triangle strips
        {
            for (int j = 0; j < quad.numIndices[i]; j+=3) // each triangle in strip
            {
                int i1 = chunk->indexData[(quad.startIndex[i] + j)]  * vertexStride * sizeof(float);
                int i2 = chunk->indexData[(quad.startIndex[i] + j + 1)] * vertexStride * sizeof(float);
                int i3 = chunk->indexData[(quad.startIndex[i] + j + 2)] * vertexStride * sizeof(float);

                IslanderCollisionTriangle tri;

                char* v1 = &chunk->vertexData[i1];
                char* v2 = &chunk->vertexData[i2];
                char* v3 = &chunk->vertexData[i3];

                tri.vertices[0] = (float*)v1;
                tri.vertices[1] = (float*)v2;
                tri.vertices[2] = (float*)v3;

                float pt[3]; float t;
                if (RayTriTest(tri, o, dir, pt, &t) && t < dist)
                {
                    dist = t;
                    CopyVec3(pt, hitPt);
                    hit = true;
                }
            }
        }

        bestIndex = -1;

        //if (hit)
        //{
        //    break;
        //}
        //else
        //{
        //    // Otherwise we need to try other neighbouring quad nodes.
        //    // Start at the top of the tree and move down?
        //    // .. And repeat ray-tri tests.

        //    while (quad.parentNode != -1)
        //    {
        //        bool quadTreeHit = false;

        //        float _dist = std::numeric_limits<float>::max();
        //        auto& parent = chunk->quadTree[quad.parentNode];
        //        for (int i = 0; i < parent.numNodes; i++)
        //        {
        //            auto& sibling = chunk->quadTree[parent.childNodes[i]];
        //            if (&sibling != &quad)
        //            {
        //                IslanderCollisionBox box;
        //                box.minx = sibling.min[0];
        //                box.miny = sibling.min[1];
        //                box.minz = sibling.min[2];
        //                box.maxx = sibling.max[0];
        //                box.maxy = sibling.max[1];
        //                box.maxz = sibling.max[2];

        //                if (TestRayBox(o[0], o[1], o[2], dir[0], dir[1], dir[2], box, dist) && dist < _dist)
        //                {
        //                    _dist = dist;
        //                    quadId = parent.childNodes[i];
        //                    quadTreeHit = true;
        //                }
        //            }
        //        }

        //        if (quadTreeHit) { break; }

        //        // TODO: loop up to the parent? and down again?
        //    }
        //}
    }

    return hit;
}

void GetQuadrantInfo(int x, int z, float dx, float dz, int* n_x, int* n_z)
{
    if (dx > 0)
    {
        if (dz > 0)
        {
            n_x[0] = x + 1; n_z[0] = z;
            n_x[1] = x;     n_z[1] = z + 1;
            n_x[2] = x + 1; n_z[2] = z + 1;
        }
        else
        {
            n_x[0] = x + 1; n_z[0] = z;
            n_x[1] = x;     n_z[1] = z - 1;
            n_x[2] = x + 1; n_z[2] = z - 1;
        }
    }
    else
    {
        if (dz > 0)
        {
            n_x[0] = x - 1; n_z[0] = z;
            n_x[1] = x;     n_z[1] = z + 1;
            n_x[2] = x - 1; n_z[2] = z + 1;
        }
        else
        {
            n_x[0] = x - 1; n_z[0] = z;
            n_x[1] = x;     n_z[1] = z - 1;
            n_x[2] = x - 1; n_z[2] = z - 1;
        }
    }
}

#if 0

bool TestRayHeightmap(Islander::Model::Heightmap* heightmap, const Islander::Collision::CollisionTransform& transform, float* o, float* dir, float& dist)
{
    float pos[3] = {
        o[0] - transform.px,
        o[1] - transform.py,
        o[2] - transform.pz
    };

    for (int i = 0; i < heightmap->numChunks; i++)
    {
        float hitPos[3];
        if (TestRayHeightmapChunk(&heightmap->chunks[i], heightmap->vertexStride, pos, dir, hitPos, dist))
        {
            return true;
        }
    }

    return false;
}
#else
bool TestRayHeightmap(Islander::Model::Heightmap* heightmap, const Islander::Collision::CollisionTransform& transform, float* o, float* dir, float& dist)
{
    Islander::Model::HeightmapDebugRayInfo rayInfo;

    rayInfo.iterations = 0;
    rayInfo.quads = 0;
    rayInfo.tris = 0;
    CopyVec3(o, rayInfo.pos);
    CopyVec3(o, rayInfo.exitPos);
    CopyVec3(dir, rayInfo.dir);

    float pos[3] = {
        o[0] - transform.px,
        o[1] - transform.py,
        o[2] - transform.pz
    };

    if (abs(dir[1] + 1.0f) < 0.0001f)
    {
        //heightmap->debugInfo.ray.push_back(rayInfo);

        float height;
        if (Islander::Model::SampleHeightmap(heightmap, pos[0], pos[2], &height))
        {
            dist = pos[1] - height;
            return true;
        }

        return false;
    }

    float range[3] = {
        heightmap->max[0] - heightmap->min[0],
        heightmap->max[1] - heightmap->min[1],
        heightmap->max[2] - heightmap->min[2]
    };

    int numWidth = heightmap->width / heightmap->chunkWidth;
    int numHeight = heightmap->height / heightmap->chunkHeight;

    float cellSizeX = range[0] / numWidth;
    float cellSizeZ = range[2] / numHeight;

    IslanderCollisionBox box;
    box.minx = heightmap->min[0];
    box.miny = heightmap->min[1];
    box.minz = heightmap->min[2];
    box.maxx = heightmap->max[0];
    box.maxy = heightmap->max[1];
    box.maxz = heightmap->max[2];

    int chunkId = -1;
    int cellX = -1;
    int cellZ = -1;

    if (PointBoxCollision(box, pos[0], pos[1], pos[2]))
    {
        // Case where we start inside the terrain bounds

        cellX = (pos[0] - heightmap->min[0]) / cellSizeX;
        cellZ = (pos[2] - heightmap->min[2]) / cellSizeZ;

        // clamp
        cellX = std::min(cellX, numWidth-1);
        cellZ = std::min(cellZ, numHeight-1);

        chunkId = cellZ + cellX * numHeight;
    }
    else
    {
        // Case where we start outside the bounds.
        if (Islander::Physics::TestRayBox(pos[0], pos[1], pos[2], dir[0], dir[1], dir[2], box, dist) && dist >= 0)
        {
            float hitPos[3] = {
                pos[0] + dir[0] * dist,
                pos[1] + dir[1] * dist,
                pos[2] + dir[2] * dist
            };

            cellX = (hitPos[0] - heightmap->min[0]) / cellSizeX;
            cellZ = (hitPos[2] - heightmap->min[2]) / cellSizeZ;

            // clamp
            cellX = std::min(cellX, numWidth-1);
            cellZ = std::min(cellZ, numHeight-1);

            chunkId = cellZ + cellX * numHeight;

            CopyVec3(hitPos, pos);
        }
    }

    while (chunkId > -1)
    {
        rayInfo.iterations++;

        float hitPos[3];
        if (heightmap->chunks[chunkId].loaded && TestRayHeightmapChunk(&heightmap->chunks[chunkId], rayInfo, heightmap->vertexStride, pos, dir, hitPos, dist))
        {
            float startPos[3] = {
                o[0] - transform.px,
                o[1] - transform.py,
                o[2] - transform.pz
            };
            float dt[3];
            Islander::Numerics::SubVec3(hitPos, startPos, dt);
            dist = Islander::Numerics::LengthSqr(dt);

            rayInfo.exitPos[0] = pos[0] + transform.px;
            rayInfo.exitPos[1] = pos[1] + transform.py;
            rayInfo.exitPos[2] = pos[2] + transform.pz;
            //heightmap->debugInfo.ray.push_back(rayInfo);

            return true;
        }
        
        float dist;

        chunkId = -1; // Clear chunk id 

        int _cellX = 0; int _cellZ = 0;

        int n_x[3] = {}; int n_z[3] = {};
        GetQuadrantInfo(cellX, cellZ, dir[0], dir[2], n_x, n_z);

        float _dist = std::numeric_limits<float>::max();
        for (int i = 0; i < 3; i++)
        {
            if (n_x[i] >= 0 && n_x[i] < numWidth && n_z[i] >= 0 && n_z[i] < numHeight)
            {
                const int _chunkId = n_z[i] + n_x[i] * numHeight;

                box.minx = heightmap->chunks[_chunkId].min[0];
                box.miny = heightmap->chunks[_chunkId].min[1];
                box.minz = heightmap->chunks[_chunkId].min[2];
                box.maxx = heightmap->chunks[_chunkId].max[0];
                box.maxy = heightmap->chunks[_chunkId].max[1];
                box.maxz = heightmap->chunks[_chunkId].max[2];

                if (/*!PointBoxCollision(box, pos[0], pos[1], pos[2]) &&*/ Islander::Physics::TestRayBox(pos[0], pos[1], pos[2], dir[0], dir[1], dir[2], box, dist) && dist >= 0 && dist < _dist)
                {
                    _dist = dist;
                    chunkId = _chunkId;
                    _cellX = n_x[i];
                    _cellZ = n_z[i];
                }
            }
        }

        // Set new origin to collision point.
        if (chunkId > -1)
        {
            pos[0] = pos[0] + dir[0] * _dist;
            pos[1] = pos[1] + dir[1] * _dist;
            pos[2] = pos[2] + dir[2] * _dist;

            cellX = _cellX;
            cellZ = _cellZ;
        }
    }

    rayInfo.exitPos[0] = pos[0] + transform.px;
    rayInfo.exitPos[1] = pos[1] + transform.py;
    rayInfo.exitPos[2] = pos[2] + transform.pz;
    //heightmap->debugInfo.ray.push_back(rayInfo);
    return false;
}
#endif

template<bool precise>
void RayCastStaticMeshGridQuery2(Islander::Collision::CollisionGrid& grid, Islander::Collision::CollisionStaticMesh* ignore, Islander::Collision::CollisionStaticMesh** closest,
    float* minDist, float* normal,
    float px, float py, float pz, float dx, float dy, float dz)
{
    float cellSizeX = (grid.box.maxx - grid.box.minx) / COLLISION_SYSTEM_GRID_COUNT;
    float cellSizeZ = (grid.box.maxz - grid.box.minz) / COLLISION_SYSTEM_GRID_COUNT;

    assert((*closest) == nullptr);

    // 1. Find the first cell to test against. This will be the cell the ray originates in. If the ray origin is outside the grid then find the first cell it intersects.
    int x = 0; int z = 0; int index = -1;

    if (Islander::Collision::PointBoxCollision(grid.box, px, py, pz))
    {
        x = std::min(COLLISION_SYSTEM_GRID_COUNT - 1, (int)((px - grid.box.minx) / cellSizeX));
        z = std::min(COLLISION_SYSTEM_GRID_COUNT - 1, (int)((pz - grid.box.minz) / cellSizeZ));
        assert(x >= 0 && x < COLLISION_SYSTEM_GRID_COUNT);
        assert(z >= 0 && z < COLLISION_SYSTEM_GRID_COUNT);
        index = z * COLLISION_SYSTEM_GRID_COUNT + x;
    }
    else
    {
        float dist = 0;
        if (Islander::Physics::TestRayBox(px, py, pz, dx, dy, dz, grid.box, dist))
        {
            float rx = px + dist * dx;
            float rz = pz + dist * dz;
            x = std::min(COLLISION_SYSTEM_GRID_COUNT - 1, (int)((rx - grid.box.minx) / cellSizeX));
            z = std::min(COLLISION_SYSTEM_GRID_COUNT - 1, (int)((rz - grid.box.minz) / cellSizeZ));
            assert(x >= 0 && x < COLLISION_SYSTEM_GRID_COUNT);
            assert(z >= 0 && z < COLLISION_SYSTEM_GRID_COUNT);
            index = z * COLLISION_SYSTEM_GRID_COUNT + x;
        }
    }

    Islander::Model::Heightmap* heightmapCache = nullptr; // last heightmap which has been checked.

    while (index >= 0)
    {
        // 2. Check if it hits any objects in this cell
        auto& cell = grid.cells[index];
        index = -1;

        for (int i = 0; i < cell.entries.size(); i++)
        {
            CollisionStaticMesh* mesh = cell.entries[i];

            float dist;
            if (mesh->active && ignore != mesh)
            {
                bool hit = false;
                if (mesh->heightmapCollider)
                {
                    if (heightmapCache != mesh->heightmap)
                    {
                        float origin[3] = { px, py, pz };
                        float dir[3] = { dx, dy, dz };
                        hit = TestRayHeightmap(mesh->heightmap, mesh->transform, origin, dir, dist);

                        if (!hit)
                        {
                            heightmapCache = mesh->heightmap;
                        }
                    }
                }
                else
                {
                    if (precise && Islander::Physics::TestRayBox(px, py, pz, dx, dy, dz, mesh->parentBox, dist))
                    {
                        float origin[3] = { px, py, pz };
                        float dir[3] = { dx, dy, dz };
                        float triDist = std::numeric_limits<float>::max();
                        for (auto& tri : mesh->triangles)
                        {
                            float t;
                            float hitpos[3];
                            if (RayTriTest(tri, origin, dir, hitpos, &t) && t < triDist && t < *minDist)
                            {
                                hit = true;
                                dist = t;
                                triDist = t;
                                CopyVec3(tri.normal, normal);
                            }
                        }
                    }
                    else if (!precise && Islander::Physics::TestRayBox(px, py, pz, dx, dy, dz, mesh->obb, dist))
                    {
                        hit = true;
                    }
                }

                if (hit && dist < *minDist)
                {
                    *minDist = dist;
                    *closest = mesh;
                }
            }
        }

        // 3. Find the next cell. Move origin to the intersect position.
        {
            // Determine quadrant to test against (possibly can jump to right cell?)
            int n_x[3] = {}; int n_z[3] = { };

            GetQuadrantInfo(x, z, dx, dz, n_x, n_z);

            float _minDist = std::numeric_limits<float>::max();
            for (int i = 0; i < 3; i++)
            {
                float dist;
                int _x = n_x[i];
                int _z = n_z[i];
                if (_x >= 0 && _x < COLLISION_SYSTEM_GRID_COUNT && _z >= 0 && _z < COLLISION_SYSTEM_GRID_COUNT)
                {
                    int _index = _z * COLLISION_SYSTEM_GRID_COUNT + _x;
                    auto& _cell = grid.cells[_index];
                    // TODO: we can cut these tests down, only need to test 1 plane per box
                    if (Islander::Physics::TestRayBox(px, py, pz, dx, dy, dz, _cell.box, dist) && dist < _minDist)
                    {
                        index = _index;
                        _minDist = dist;
                        x = _x;
                        z = _z;
                    }
                }
            }

            // Terminate early if the object hit position is closer than the cell hit position.
            if (*closest && *minDist < _minDist)
            {
                index = -1;
            }
        }
    }
}

void RayCastStaticMeshGridQuery(Islander::Collision::CollisionGrid& grid, Islander::Collision::CollisionStaticMesh* ignore, Islander::Collision::CollisionStaticMesh** closest, float* minDist, float px, float py, float pz, float dx, float dy, float dz)
{
    for (int i = 0; i < grid.cells.size(); i++)
    {
        auto& cell = grid.cells[i];
        float boxDist;
        if (cell.entries.size() > 0 && Islander::Physics::TestRayBox(px, py, pz, dx, dy, dz, cell.box, boxDist))
        {
            for (auto entry : cell.entries)
            {
                if (entry == ignore || !entry->active)
                {
                    continue;
                }

                float dist;
                bool hit = false;

                if (entry->heightmapCollider)
                {
                    float origin[3] = { px, py, pz };
                    float dir[3] = { dx, dy, dz };
                    hit = TestRayHeightmap(entry->heightmap, entry->transform, origin, dir, dist);
                }
                else if (Islander::Physics::TestRayBox(px, py, pz, dx, dy, dz, entry->obb/*->parentBox*/, dist))
                {
                    hit = true;
                }

                if (hit && dist < *minDist)
                {
                    *minDist = dist;
                    *closest = entry;
                }
            }
        }
    }
}

void Islander::Physics::RayCastHeightmap(NonRealisticPhysicsSystem* system, Camera3D* camera, float px, float py, float pz, float dx, float dy, float dz, IslanderCollisionRayInfo* info)
{
    //BuildCollisionGrid(system->grid, system->static_mesh.count, system->static_mesh.array);

    Normalize(dx, dy, dz, dx, dy, dz);

    if (camera != nullptr)
    {
        TransformRay(camera, &px, &py, &pz, &dx, &dy, &dz);
    }

    float minDistStatic = std::numeric_limits<float>().max();
    CollisionStaticMesh* staticClosest = nullptr;

    float o[3] = { px,py,pz };
    float dir[3] = { dx,dy,dz };

    for (int i = 0; i < system->static_mesh.count; i++)
    {
        auto& mesh = system->static_mesh.array[i];
        if (mesh.active && mesh.heightmapCollider)
        {
            float dist;
            if (TestRayHeightmap(mesh.heightmap, mesh.transform, o, dir, dist) && dist < minDistStatic)
            {
                minDistStatic = dist;
                staticClosest = &mesh;
            }
        }
    }

    info->static_mesh = staticClosest;

    info->static_hit_pos[0] = px + dx * minDistStatic;
    info->static_hit_pos[1] = py + dy * minDistStatic;
    info->static_hit_pos[2] = pz + dz * minDistStatic;
}

void Islander::Physics::RayCastFine(NonRealisticPhysicsSystem* system, Camera3D* camera, float px, float py, float pz, float dx, float dy, float dz, IslanderCollisionRayInfo* info)
{
    BuildCollisionGrid(system->grid, system->static_mesh.count, system->static_mesh.array);

    Normalize(dx, dy, dz, dx, dy, dz);

    if (camera != nullptr)
    {
        TransformRay(camera, &px, &py, &pz, &dx, &dy, &dz);
    }

    float minDistStatic = std::numeric_limits<float>().max();
    float minDistDynamic = std::numeric_limits<float>().max();
    CollisionDynamicMesh* dynamicClosest = nullptr;
    CollisionStaticMesh* staticClosest = nullptr;
    float staticNormal[3];

    RayCastDynamcMesh<CollisionDynamicMesh>(system->dynamic_mesh.array, system->dynamic_mesh.count, &dynamicClosest, (CollisionDynamicMesh*)info->ignore_mesh, &minDistDynamic, px, py, pz, dx, dy, dz);
    //RayCastStaticMesh<CollisionStaticMesh>(system->static_mesh.array, system->static_mesh.count, &staticClosest, (CollisionStaticMesh*)info->ignore_static_mesh, &minDistStatic, px, py, pz, dx, dy, dz);
    RayCastStaticMeshGridQuery2<true>(system->grid, (CollisionStaticMesh*)info->ignore_static_mesh, &staticClosest, &minDistStatic, staticNormal, px, py, pz, dx, dy, dz);

    RayCastDynamcMesh<CollisionDynamicMesh>(system->dynamic_mesh_ray.array, system->dynamic_mesh_ray.count, &dynamicClosest, (CollisionDynamicMesh*)info->ignore_mesh, &minDistDynamic, px, py, pz, dx, dy, dz);
    RayCastStaticMesh<CollisionStaticMesh>(system->static_mesh_ray.array, system->static_mesh_ray.count, &staticClosest, (CollisionStaticMesh*)info->ignore_static_mesh, &minDistStatic, px, py, pz, dx, dy, dz);

    info->dynamic_mesh = dynamicClosest;
    info->static_mesh = staticClosest;

    info->static_hit_pos[0] = px + dx * minDistStatic;
    info->static_hit_pos[1] = py + dy * minDistStatic;
    info->static_hit_pos[2] = pz + dz * minDistStatic;

    info->dynamic_hit_pos[0] = px + dx * minDistDynamic;
    info->dynamic_hit_pos[1] = py + dy * minDistDynamic;
    info->dynamic_hit_pos[2] = pz + dz * minDistDynamic;

    CopyVec3(staticNormal, info->static_normal);
}

void Islander::Physics::RayCast(NonRealisticPhysicsSystem* system, Camera3D* camera, float px, float py, float pz, float dx, float dy, float dz, int group, IslanderCollisionRayInfo* info)
{
    BuildCollisionGrid(system->grid, system->static_mesh.count, system->static_mesh.array);
    BuildDynamicGrid(system->dynGrid, system->grid.box, system->dynamic_mesh.array, system->dynamic_mesh.count, group);

    Normalize(dx, dy, dz, dx, dy, dz);

    if (camera != nullptr)
    {
        TransformRay(camera, &px, &py, &pz, &dx, &dy, &dz);
    }

    float minDistStatic = std::numeric_limits<float>().max();
    float minDistDynamic = std::numeric_limits<float>().max();
    CollisionDynamicMesh* dynamicClosest = nullptr;
    CollisionStaticMesh* staticClosest = nullptr;
    float staticNormal[3];

    //RayCastDynamcMesh<CollisionDynamicMesh>(system->dynamic_mesh.array, system->dynamic_mesh.count, &dynamicClosest, (CollisionDynamicMesh*)info->ignore_mesh, &minDistDynamic, px, py, pz, dx, dy, dz);
    RayCastDynamcMeshGridQuery<CollisionDynamicMesh>(system->dynGrid, &dynamicClosest, (CollisionDynamicMesh*)info->ignore_mesh, &minDistDynamic, px, py, pz, dx, dy, dz);
    RayCastStaticMeshGridQuery2<false>(system->grid, (CollisionStaticMesh*)info->ignore_static_mesh, &staticClosest, &minDistStatic, staticNormal, px, py, pz, dx, dy, dz);

    RayCastDynamcMesh<CollisionDynamicMesh>(system->dynamic_mesh_ray.array, system->dynamic_mesh_ray.count, &dynamicClosest, (CollisionDynamicMesh*)info->ignore_mesh, &minDistDynamic, px, py, pz, dx, dy, dz);
    RayCastStaticMesh<CollisionStaticMesh>(system->static_mesh_ray.array, system->static_mesh_ray.count, &staticClosest, (CollisionStaticMesh*)info->ignore_static_mesh, &minDistStatic, px, py, pz, dx, dy, dz);

    info->dynamic_mesh = dynamicClosest;
    info->static_mesh = staticClosest;

    info->static_hit_pos[0] = px + dx * minDistStatic;
    info->static_hit_pos[1] = py + dy * minDistStatic;
    info->static_hit_pos[2] = pz + dz * minDistStatic;

    info->dynamic_hit_pos[0] = px + dx * minDistDynamic;
    info->dynamic_hit_pos[1] = py + dy * minDistDynamic;
    info->dynamic_hit_pos[2] = pz + dz * minDistDynamic;
    
    CopyVec3(staticNormal, info->static_normal);
}

void Islander::Physics::TransformRay(Camera3D* camera, float* px, float* py, float* pz, float* dx, float* dy, float* dz)
{
    Matrix4x4 inverseView;
    Matrix4x4::CreateInverseViewMatrix(camera->pos.GetX(), camera->pos.GetY(), camera->pos.GetZ(), camera->look.GetX(), camera->look.GetY(), camera->look.GetZ(), &inverseView);

    Matrix4x4::TransformVector(inverseView, px, py, pz);
    Matrix4x4::TransformVector(inverseView, dx, dy, dz, 0.0f);

    float dirX, dirY, dirZ;
    Normalize(*dx, *dy, *dz, dirX, dirY, dirZ);

    *dx = dirX;
    *dy = dirY;
    *dz = dirZ;
}

void Islander::Physics::CollisionOverlap(NonRealisticPhysicsSystem* system, CollisionDynamicMesh* mesh, IslanderCollisionOverlapInfo* info)
{
    info->hit_count = 0;

    ClearColliders(mesh);

    CollisionEventList evlist;
    BuildCollisionGrid(system->grid, system->static_mesh.count, system->static_mesh.array);

    IslanderCollisionCapsule capsule = mesh->info.capsule;
    IslanderCollisionSphere sphere = mesh->info.sphere;
    IslanderCollisionBox box = mesh->info.box;
    switch (mesh->info.collisionShape)
    {
    case ISLANDER_COLLISION_SHAPE_CAPSULE:
        CalculateCapsule(capsule, mesh);
        QueryGridCells(system->grid, capsule, mesh, evlist);
        break;
    case ISLANDER_COLLISION_SHAPE_SPHERE:
        CalculateSphere(sphere, mesh);
        QueryGridCells(system->grid, sphere, mesh, evlist);
        break;
    case ISLANDER_COLLISION_SHAPE_BOX:
        CalculateBox(box, mesh);
        QueryGridCells<IslanderCollisionBox, 0>(system->grid, box, mesh, evlist);
        break;
    }

    for (int i = 0; i < mesh->colData.hit_count; i++)
    {
        if ((mesh->colData.hits[i].flags & ISLANDER_COLLISION_HIT_FLAG_STATIC) == ISLANDER_COLLISION_HIT_FLAG_STATIC)
        {
            info->hits[info->hit_count] = mesh->colData.hits[i];
            info->hit_count++;
        }
    }

    // TODO: build,query dynamic grid

    for (int i = 0; i < system->dynamic_mesh.count; i++)
    {
        auto& obj = system->dynamic_mesh.array[i];
        if (&obj != mesh && obj.active)
        {
            bool ishit = false;
            float normal[3];
            switch (mesh->info.collisionShape)
            {
            case ISLANDER_COLLISION_SHAPE_CAPSULE:
                ishit = DispatchCollisionTest(capsule, obj.info, normal);
                break;
            }

            if (ishit)
            {
                auto& hit = info->hits[info->hit_count++];
                hit.flags = ISLANDER_COLLISION_HIT_FLAG_DYNAMIC;
                hit.dynamic_mesh = &obj;

                if (info->hit_count == ISLANDER_COLLISION_DATA_COUNT)
                {
                    return;
                }
            }
        }
    }
}

float Islander::Physics::CollisionSweep(NonRealisticPhysicsSystem* system, CollisionStaticMesh* mesh, float* vector)
{
    float time = 1;
    if (mesh->triangleCollider)
    {

    }
    else
    {
        for (int i = 0; i < system->static_mesh.count; i++)
        {
            if (mesh == &system->static_mesh.array[i])
            {
                continue;
            }

            float normal[3];
            if (system->static_mesh.array[i].triangleCollider)
            {

            }
            else if (system->static_mesh.array[i].heightmapCollider)
            {

            }
            else
            {
                time = std::min(time, Islander::Collision::GJK_Swept(mesh->obb, system->static_mesh.array[i].obb, vector, normal));
            }
        }
    }
    return time;
}
