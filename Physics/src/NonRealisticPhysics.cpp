#include "NonRealisticPhysics.h"
#include "AnimationController.h"
#include "Matrix.h"
#include "Component.h"
#include "Polygon.h"
#include "CollisionEvent.h"
#include "CollisionPrimitives.h"
#include "RayCast.h"
#include "Heightmap.h"
#include "OBB.h"
#include <algorithm>
#include <cfloat>
#include <cstring>
#include <unordered_set>
#include <limits>

#define _USE_MATH_DEFINES
#include <math.h>

using namespace Islander::Numerics;

Islander::Physics::NonRealisticPhysicsSystem* Islander::Physics::CreateNonRealisticPhysicsSystem()
{
    auto system = new Islander::Physics::NonRealisticPhysicsSystem();
    system->grid.dirty = false;
    system->dynGrid.dirty = false;
    return system;
}

template<int flags>
void AddDynamicCollider(Islander::Collision::CollisionDynamicMesh* mesh, Islander::Collision::CollisionDynamicMesh* add)
{
    for (int i = 0; i < mesh->colData.dynamic_mesh_count; i++)
    {
        if (mesh->colData.dynamic_mesh[i] == add)
        {
            return;
        }
    }

    if (mesh->colData.dynamic_mesh_count < PHYSICS_SYSTEM_DATA_ITEMS)
    {
        mesh->colData.dynamic_mesh[mesh->colData.dynamic_mesh_count++] = add;
    }

    if (mesh->colData.hit_count < PHYSICS_SYSTEM_DATA_ITEMS)
    {
        auto& hit = mesh->colData.hits[mesh->colData.hit_count++];
        hit.dynamic_mesh = add;
        hit.flags = ISLANDER_COLLISION_HIT_FLAG_DYNAMIC | flags;
    }
}

//void AddStaticCollider(Islander::Collision::CollisionDynamicMesh* mesh, Islander::Collision::CollisionStaticMesh* add)
//{
//    for (int i = 0; i < mesh->collider_static_count; i++)
//    {
//        if (mesh->collider_static[i] == add)
//        {
//            return;
//        }
//    }
//
//    if (mesh->collider_static_count < COLLISION_SYSTEM_DATA_ITEMS)
//    {
//        mesh->collider_static[mesh->collider_static_count++] = add;
//    }
//}

#if 0
// Modified from https://www.gamedev.net/articles/programming/general-and-gameplay-programming/swept-aabb-collision-detection-and-response-r3084/

float SweptCollisionBoxTest(const IslanderCollisionBox& startBox, const IslanderCollisionBox& endBox, const IslanderCollisionBox& staticBox, float& normalx, float& normaly, float& normalz)
{
    // Perform broadphase test.
    IslanderCollisionBox broadphaseBox;
    broadphaseBox.maxx = std::max(startBox.maxx, endBox.maxx);
    broadphaseBox.maxy = std::max(startBox.maxy, endBox.maxy);
    broadphaseBox.maxz = std::max(startBox.maxz, endBox.maxz);
    broadphaseBox.minx = std::min(startBox.minx, endBox.minx);
    broadphaseBox.miny = std::min(startBox.miny, endBox.miny);
    broadphaseBox.minz = std::min(startBox.minz, endBox.minz);

    if (!Islander::Collision::CollisionBoxTest(broadphaseBox, staticBox))
    {
        normalx = 0;
        normaly = 0;
        normalz = 0;
        return 1;
    }

    float xInvEntry, yInvEntry, zInvEntry;
    float xInvExit, yInvExit, zInvExit;

    float vx = endBox.maxx - startBox.maxx;
    float vy = endBox.maxy - startBox.maxy;
    float vz = endBox.maxz - startBox.maxz;

    // find the distance between the objects on the near and far sides for both x and y 
    if (vx > 0)
    {
        xInvEntry = staticBox.minx - startBox.maxx;
        xInvExit = staticBox.maxx - startBox.minx;
    }
    else
    {
        xInvEntry = staticBox.maxx - startBox.minx;
        xInvExit = staticBox.minx - startBox.maxx;
    }

    if (vy > 0)
    {
        yInvEntry = staticBox.miny - startBox.maxy;
        yInvExit = startBox.maxy - startBox.miny;
    }
    else
    {
        yInvEntry = staticBox.maxy - startBox.miny;
        yInvExit = staticBox.miny - startBox.maxy;
    }

    if (vz > 0)
    {
        zInvEntry = staticBox.minz - startBox.maxz;
        zInvExit = staticBox.maxz - startBox.minz;
    }
    else
    {
        zInvEntry = staticBox.maxz - startBox.minz;
        zInvExit = staticBox.minz - startBox.maxz;
    }

    // find time of collision and time of leaving for each axis (if statement is to prevent divide by zero) 
    float xEntry, yEntry, zEntry;
    float xExit, yExit, zExit;

    if (vx == 0.0f)
    {
        xEntry = -std::numeric_limits<float>::infinity();
        xExit = std::numeric_limits<float>::infinity();
    }
    else
    {
        xEntry = xInvEntry / vx;
        xExit = xInvExit / vx;
    }

    if (vy == 0.0f)
    {
        yEntry = -std::numeric_limits<float>::infinity();
        yExit = std::numeric_limits<float>::infinity();
    }
    else
    {
        yEntry = yInvEntry / vy;
        yExit = yInvExit / vy;
    }

    if (vz == 0.0f)
    {
        zEntry = -std::numeric_limits<float>::infinity();
        zExit = std::numeric_limits<float>::infinity();
    }
    else
    {
        zEntry = zInvEntry / vz;
        zExit = zInvExit / vz;
    }

    if (xEntry > 1.0f) xEntry = -std::numeric_limits<float>::infinity();
    if (yEntry > 1.0f) yEntry = -std::numeric_limits<float>::infinity();
    if (zEntry > 1.0f) zEntry = -std::numeric_limits<float>::infinity();

    // find the earliest/latest times of collision
    float entryTime = std::max(std::max(xEntry, yEntry), zEntry);
    float exitTime = std::min(std::min(xExit, yExit), zExit);

    // If we already colliding then the normal is the direction to separate the objects.
    if (Islander::Collision::CollisionBoxTest(startBox, staticBox))
    {
        float vx = endBox.maxx - startBox.maxx;
        float vy = endBox.maxy - startBox.maxy;
        float vz = endBox.maxz - startBox.maxz;

        //float distX =

        //normalx = (endBox.maxx - endBox.minx) / 2 + endBox.minx - ((startBox.maxx - startBox.minx) / 2 + startBox.minx);
        //normaly = (endBox.maxy - endBox.miny) / 2 + endBox.miny - ((startBox.maxy - startBox.miny) / 2 + startBox.miny);
        //normalz = (endBox.maxz - endBox.minz) / 2 + endBox.minz - ((startBox.maxz - startBox.minz) / 2 + startBox.minz);
        //Islander::Normalize(-normalx, -normaly, -normalz, normalx, normaly, normalz);

        Normalize(-vx, -vy, -vz, normalx, normaly, normalz);

        //return entryTime;
        return 0;
    }

    normalx = 0.0f;
    normaly = 0.0f;
    normalz = 0.0f;
    // if there was no collision
    if (entryTime > exitTime)
    {
        return 1.0f;
    }
    else if (xEntry < 0.0f && yEntry < 0.0f && zEntry < 0.0f)
    {
        return 1.0f;
    }
    else if (xEntry < 0.0f && yEntry < 0.0f)
    {
        if ((startBox.maxx < staticBox.minx || startBox.minx > staticBox.maxx) && (startBox.maxy < staticBox.miny || startBox.miny > staticBox.maxy))
        {
            return 1.0f;
        }
    }
    else if (xEntry < 0.0f && zEntry < 0.0f)
    {
        if ((startBox.maxx < staticBox.minx || startBox.minx > staticBox.maxx) && (startBox.maxz < staticBox.minz || startBox.minz > staticBox.maxz))
        {
            return 1.0f;
        }
    }
    else if (yEntry < 0.0f && zEntry < 0.0f)
    {
        if ((startBox.maxy < staticBox.miny || startBox.miny > staticBox.maxy) && (startBox.maxz < staticBox.minz || startBox.minz > staticBox.maxz))
        {
            return 1.0f;
        }
    }
    else if (xEntry < 0.0f)
    {
        if (startBox.maxx < staticBox.minx || startBox.minx > staticBox.maxx)
        {
            return 1.0f;
        }
    }
    else if (yEntry < 0.0f)
    {
        if (startBox.maxy < staticBox.miny || startBox.miny > staticBox.maxy)
        {
            return 1.0f;
        }
    }
    else if (zEntry < 0.0f)
    {
        if (startBox.maxz < staticBox.minz || startBox.minz > staticBox.maxz)
        {
            return 1.0f;
        }
    }
     // if there was a collision 
    {
        // calculate normal of collided surface
        if (xEntry > yEntry && xEntry > zEntry)
        {
            if (xInvEntry < 0.0f)
            {
                normalx = 1.0f;
                normaly = 0.0f;
                normalz = 0.0f;
            }
            else
            {
                normalx = -1.0f;
                normaly = 0.0f;
                normalz = 0.0f;
            }
        }
        else if (zEntry > yEntry && zEntry > xEntry)
        {
            if (zInvEntry < 0.0f)
            {
                normalx = 0.0f;
                normaly = 0.0f;
                normalz = 1.0f;
            }
            else
            {
                normalx = 0.0f;
                normaly = 0.0f;
                normalz = -1.0f;
            }
        }
        else
        {
            if (yInvEntry < 0.0f)
            {
                normalx = 0.0f;
                normaly = 1.0f;
                normalz = 0.0f;
            }
            else
            {
                normalx = 0.0f;
                normaly = -1.0f;
                normalz = 0.0f;
            }
        }

        return entryTime; // return the time of collision
    }
}
#endif

void CreateStaticMeshCollisionData(Islander::Collision::CollisionStaticMesh* mesh)
{
    // 1. Transform vertices

    int totalNumVerts = 0;
    int totalIndices = 0;
    for (int i = 0; i < mesh->meshData->numMeshes; i++)
    {
        totalNumVerts += mesh->meshData->meshes[i].numVerts;
        totalIndices += mesh->meshData->meshes[i].numIndices;
    }

    mesh->vertices = new float[totalNumVerts * 3];

    Matrix4x4 matrix(Matrix4x4Type::MATRIX_4X4_IDENTITY);
    Matrix4x4::CreateTransformationMatrix(
        mesh->transform.px,
        mesh->transform.py,
        mesh->transform.pz,
        mesh->transform.rx,
        mesh->transform.ry,
        mesh->transform.rz,
        mesh->transform.sx,
        mesh->transform.sy,
        mesh->transform.sz,
        &matrix);

    int vertIndex = 0;
    for (int j = 0; j < mesh->meshData->numMeshes; j++)
    {
        int initialVertIndex = vertIndex;
        int numVerts = mesh->meshData->meshes[j].numVerts * 3;
        for (int i = 0; i < numVerts; i += 3)
        {
            float v[3] =
            {
                mesh->meshData->meshes[j].posData[i],
                mesh->meshData->meshes[j].posData[i + 1],
                mesh->meshData->meshes[j].posData[i + 2]
            };
            Matrix4x4::TransformVector(matrix, &v[0], &v[1], &v[2]);
            mesh->vertices[vertIndex++] = v[0];
            mesh->vertices[vertIndex++] = v[1];
            mesh->vertices[vertIndex++] = v[2];
        }

        auto meshItem = &mesh->meshData->meshes[j];
        for (int i = 0; i < meshItem->numIndices; i += 3)
        {
            IslanderCollisionTriangle tri;
            tri.vertices[0] = &mesh->vertices[initialVertIndex + meshItem->indexData[i]*3];
            tri.vertices[1] = &mesh->vertices[initialVertIndex + meshItem->indexData[i+1]*3];
            tri.vertices[2] = &mesh->vertices[initialVertIndex + meshItem->indexData[i+2]*3];
            Islander::Collision::CalculateTriangleNormal(tri.vertices[0], tri.vertices[1], tri.vertices[2], tri.normal);
            mesh->triangles.push_back(tri);
        }
    }

    // 2. Calculate OBB

    Islander::Collision::ComputeOBB(mesh->obb, matrix, mesh->meshData->min, mesh->meshData->max);

    // 3. Calculate the parent bounding box
    IslanderCollisionBox& parentBox = mesh->parentBox;
    parentBox.minx = parentBox.miny = parentBox.minz = std::numeric_limits<float>().max();
    parentBox.maxx = parentBox.maxy = parentBox.maxz = -std::numeric_limits<float>().max();
    for (int i = 0; i < vertIndex; i += 3)
    {
        auto vert = &mesh->vertices[i];

        parentBox.minx = std::min(vert[0], parentBox.minx);
        parentBox.miny = std::min(vert[1], parentBox.miny);
        parentBox.minz = std::min(vert[2], parentBox.minz);

        parentBox.maxx = std::max(vert[0], parentBox.maxx);
        parentBox.maxy = std::max(vert[1], parentBox.maxy);
        parentBox.maxz = std::max(vert[2], parentBox.maxz);
    }
}

void Islander::Physics::UpdateStaticMesh(Islander::Physics::NonRealisticPhysicsSystem* system, Islander::Collision::CollisionStaticMesh* mesh, IslanderCollisionStaticMeshInfo* meshInfo)
{
    delete[] mesh->vertices;
    mesh->vertices = nullptr;

    mesh->meshData = (Islander::Model::PolygonModel*) meshInfo->meshData;
    mesh->rayonly = meshInfo->rayonly == 1;
    mesh->active = true;
    mesh->hit = false;
    mesh->triangleCollider = meshInfo->triangleCollider == 1;
    mesh->heightmapCollider = false;
    mesh->heightmap = nullptr;
    mesh->transform.px = meshInfo->px;
    mesh->transform.py = meshInfo->py;
    mesh->transform.pz = meshInfo->pz;
    mesh->transform.rx = meshInfo->rx;
    mesh->transform.ry = meshInfo->ry;
    mesh->transform.rz = meshInfo->rz;
    mesh->transform.sx = meshInfo->sx;
    mesh->transform.sy = meshInfo->sy;
    mesh->transform.sz = meshInfo->sz;
    mesh->material = meshInfo->material;

    if (!mesh->rayonly)
    {
        if (mesh->meshData != nullptr)
        {
            CreateStaticMeshCollisionData(mesh);
        }
        system->grid.dirty = true;
        system->dynGrid.dirty = true; // rebuild this as well
    }
}

void Islander::Physics::OverrideStaticMeshOBB(Islander::Collision::CollisionStaticMesh* mesh, float* verts)
{
    std::memcpy(mesh->obb.verticies, verts, sizeof(mesh->obb.verticies));
    Islander::Collision::ComputeOBBFromVertices(mesh->obb);
}

bool AllocateStaticMesh(Islander::Collision::CollisionStaticMesh* array, int* count, std::deque<int>& freeslots, Islander::Collision::CollisionStaticMesh** mesh)
{
    if (freeslots.size() > 0)
    {
        *mesh = &array[freeslots.back()];
        freeslots.pop_back();
    }
    else if (*count < PHYSICS_SYSTEM_MAX_STATIC_ITEMS)
    {
        *mesh = &array[*count];
        (*count)++;
    }
    else
    {
        return false;
    }

    return true;
}

void GetHeightmapBox(Islander::Model::Heightmap* heightmap, const Islander::Collision::CollisionTransform& transform, float* min, float* max)
{
    float min_[3] = {
        std::numeric_limits<float>::max(),
        std::numeric_limits<float>::max(),
        std::numeric_limits<float>::max()
    };
    float max_[3] = {
        std::numeric_limits<float>::lowest(),
        std::numeric_limits<float>::lowest(),
        std::numeric_limits<float>::lowest()
    };

    Islander::Numerics::CopyVec3(min_, min);
    Islander::Numerics::CopyVec3(max_, max);

    for (int j = 0; j < heightmap->numChunks; j++)
    {
        if (heightmap->chunks[j].loaded)
        {
            for (int i = 0; i < heightmap->chunks[j].numVertices; i++)
            {
                float pos[3];
                Islander::Numerics::CopyVec3((float*)(heightmap->chunks[j].vertexData + i * heightmap->vertexStride * sizeof(float)), pos);

                min[0] = std::min(min[0], pos[0]);
                min[1] = std::min(min[1], pos[1]);
                min[2] = std::min(min[2], pos[2]);

                max[0] = std::max(max[0], pos[0]);
                max[1] = std::max(max[1], pos[1]);
                max[2] = std::max(max[2], pos[2]);
            }
        }
    }
}

void GetHeightmapParentBox(Islander::Model::Heightmap* heightmap, const Islander::Collision::CollisionTransform& transform, float* min, float* max)
{
    float min_[3] = {
        std::numeric_limits<float>::max(),
        std::numeric_limits<float>::max(),
        std::numeric_limits<float>::max()
    };
    float max_[3] = {
        std::numeric_limits<float>::lowest(),
        std::numeric_limits<float>::lowest(),
        std::numeric_limits<float>::lowest()
    };

    Islander::Numerics::CopyVec3(min_, min);
    Islander::Numerics::CopyVec3(max_, max);

    Islander::Numerics::Matrix4x4 t;
    Islander::Numerics::Matrix4x4::CreateTransformationMatrix(
        transform.px, transform.py, transform.pz,
        transform.rx, transform.ry, transform.rz,
        transform.sx, transform.sy, transform.sz, &t);

    for (int j = 0; j < heightmap->numChunks; j++)
    {
        if (heightmap->chunks[j].loaded)
        {
            for (int i = 0; i < heightmap->chunks[j].numVertices; i++)
            {
                float pos[3];
                Islander::Numerics::CopyVec3((float*)(heightmap->chunks[j].vertexData + i * heightmap->vertexStride * sizeof(float)), pos);
                Islander::Numerics::Matrix4x4::TransformVector(t, &pos[0], &pos[1], &pos[2]);

                min[0] = std::min(min[0], pos[0]);
                min[1] = std::min(min[1], pos[1]);
                min[2] = std::min(min[2], pos[2]);

                max[0] = std::max(max[0], pos[0]);
                max[1] = std::max(max[1], pos[1]);
                max[2] = std::max(max[2], pos[2]);
            }
        }
    }
}

bool Islander::Physics::AddHeightmap(Islander::Physics::NonRealisticPhysicsSystem* system, bool rayonly, Islander::Model::Heightmap* heightmap, const Islander::Collision::CollisionTransform& transform, Islander::Collision::CollisionStaticMesh** mesh)
{
    if (rayonly)
    {
        if (AllocateStaticMesh(system->static_mesh_ray.array, &system->static_mesh_ray.count, system->static_mesh_ray.freeslots, mesh))
        {
            //(*mesh)->childboxcount = 0;
            //(*mesh)->childbox = nullptr;
            (*mesh)->vertices = nullptr;
            (*mesh)->rayonly = rayonly;
            (*mesh)->meshData = nullptr;
            (*mesh)->transform = transform;
            (*mesh)->active = true;
            (*mesh)->hit = false;
            (*mesh)->triangleCollider = false;
            (*mesh)->heightmapCollider = true;
            (*mesh)->heightmap = heightmap;
            (*mesh)->material = 0;
            return true;
        }
    }
    else
    {
        if (AllocateStaticMesh(system->static_mesh.array, &system->static_mesh.count, system->static_mesh.freeslots, mesh))
        {
            (*mesh)->meshData = nullptr;
            (*mesh)->rayonly = rayonly;
            (*mesh)->active = true;
            (*mesh)->hit = false;
            (*mesh)->transform = transform;
            (*mesh)->triangleCollider = false;
            (*mesh)->heightmapCollider = true;
            (*mesh)->heightmap = heightmap;
            (*mesh)->material = 0;

            float min[3]; float max[3];
            GetHeightmapParentBox(heightmap, transform, min, max);

            (*mesh)->parentBox.minx = min[0];
            (*mesh)->parentBox.miny = min[1];
            (*mesh)->parentBox.minz = min[2];
            
            (*mesh)->parentBox.maxx = max[0];
            (*mesh)->parentBox.maxy = max[1];
            (*mesh)->parentBox.maxz = max[2];

            GetHeightmapBox(heightmap, transform, min, max);

            (*mesh)->box.minx = min[0];
            (*mesh)->box.miny = min[1];
            (*mesh)->box.minz = min[2];

            (*mesh)->box.maxx = max[0];
            (*mesh)->box.maxy = max[1];
            (*mesh)->box.maxz = max[2];

            (*mesh)->vertices = nullptr;
            system->grid.dirty = true;
            system->dynGrid.dirty = true; // rebuild this as well

            return true;
        }
    }

    return false;
}

bool Islander::Physics::AddStaticMesh(Islander::Physics::NonRealisticPhysicsSystem* system, bool rayonly, bool triangleCollider, int material, Islander::Model::PolygonModel* meshData, const Islander::Collision::CollisionTransform& transform, Islander::Collision::CollisionStaticMesh** mesh)
{
    if (rayonly)
    {
        if (AllocateStaticMesh(system->static_mesh_ray.array, &system->static_mesh_ray.count, system->static_mesh_ray.freeslots, mesh))
        {
            //(*mesh)->childboxcount = 0;
            //(*mesh)->childbox = nullptr;
            (*mesh)->rayonly = rayonly;
            (*mesh)->meshData = meshData;
            (*mesh)->transform = transform;
            (*mesh)->active = true;
            (*mesh)->hit = false;
            (*mesh)->triangleCollider = triangleCollider;
            (*mesh)->heightmapCollider = false;
            (*mesh)->heightmap = nullptr;
            (*mesh)->material = material;

            if (meshData != nullptr)
            {
                CreateStaticMeshCollisionData(*mesh);
            }
            else
            {
                (*mesh)->vertices = nullptr;
            }

            return true;
        }
    }
    else
    {
        if (AllocateStaticMesh(system->static_mesh.array, &system->static_mesh.count, system->static_mesh.freeslots, mesh))
        {
            (*mesh)->meshData = meshData;
            (*mesh)->rayonly = rayonly;
            (*mesh)->active = true;
            (*mesh)->hit = false;
            (*mesh)->transform = transform;
            (*mesh)->triangleCollider = triangleCollider;
            (*mesh)->heightmapCollider = false;
            (*mesh)->heightmap = nullptr;
            (*mesh)->material = material;

            if (meshData != nullptr)
            {
                CreateStaticMeshCollisionData(*mesh);
                system->grid.dirty = true;
                system->dynGrid.dirty = true; // rebuild this as well
            }
            else
            {
                //(*mesh)->childboxcount = 0;
                //(*mesh)->childbox = nullptr;
                (*mesh)->vertices = nullptr;
            }
            return true;
        }
    }

    return false;
}

bool AllocateDynamicMesh(Islander::Collision::CollisionDynamicMesh* array, int* count, std::deque<int>& freeslots, Islander::Collision::CollisionDynamicMesh** mesh)
{
    if (freeslots.size() > 0)
    {
        *mesh = &array[freeslots.back()];
        freeslots.pop_back();
    }
    else if (*count < PHYSICS_SYSTEM_MAX_DYNAMIC_ITEMS)
    {
        *mesh = &array[*count];
        (*count)++;
    }
    else
    {
        return false;
    }

    return true;
}

bool Islander::Physics::AddDynamicMesh(Islander::Physics::NonRealisticPhysicsSystem* system, bool rayonly, Islander::Collision::CollisionDynamicMesh** mesh)
{
    if (rayonly)
    {
        if (AllocateDynamicMesh(system->dynamic_mesh_ray.array, &system->dynamic_mesh_ray.count, system->dynamic_mesh_ray.freeslots, mesh))
        {
            (*mesh)->info.rayonly = rayonly;
            (*mesh)->active = true;
            (*mesh)->hit = false;
            system->dynGrid.dirty = true;
            return true;
        }
    }
    else
    {
        if (AllocateDynamicMesh(system->dynamic_mesh.array, &system->dynamic_mesh.count, system->dynamic_mesh.freeslots, mesh))
        {
            (*mesh)->info.rayonly = rayonly;
            (*mesh)->active = true;
            (*mesh)->hit = false;
            system->dynGrid.dirty = true;
            return true;
        }
    }

    return false;
}

void Islander::Physics::RemoveStaticMesh(Islander::Physics::NonRealisticPhysicsSystem* system, Islander::Collision::CollisionStaticMesh* mesh)
{
    if (mesh->rayonly)
    {
        int idx = (mesh - &system->static_mesh_ray.array[0]);
        if (idx >= 0 && idx < system->static_mesh_ray.count && system->static_mesh_ray.array[idx].active)
        {
            //delete[] system->static_mesh_ray.array[idx].childbox;
            //system->static_mesh_ray.array[idx].childbox = nullptr;
            delete[] system->static_mesh_ray.array[idx].vertices;
            system->static_mesh_ray.array[idx].vertices = nullptr;
            system->static_mesh_ray.array[idx].active = false;
            system->static_mesh_ray.array[idx].triangles.clear();
            system->static_mesh_ray.freeslots.push_back(idx);
        }
    }
    else
    {
        int idx = (mesh - &system->static_mesh.array[0]);
        if (idx >= 0 && idx < system->static_mesh.count && system->static_mesh.array[idx].active)
        {
            //delete[] system->static_mesh.array[idx].childbox;
            //system->static_mesh.array[idx].childbox = nullptr;
            delete[] system->static_mesh.array[idx].vertices;
            system->static_mesh.array[idx].vertices = nullptr;
            system->static_mesh.array[idx].active = false;
            system->static_mesh.array[idx].triangles.clear();
            system->static_mesh.freeslots.push_back(idx);
            system->grid.dirty = true;
        }
    }
}

void Islander::Physics::RemoveDynamicMesh(Islander::Physics::NonRealisticPhysicsSystem* system, Islander::Collision::CollisionDynamicMesh* mesh)
{
    if (mesh->info.rayonly)
    {
        int idx = (mesh - &system->dynamic_mesh_ray.array[0]);
        if (idx >= 0 && idx < system->dynamic_mesh_ray.count && system->dynamic_mesh_ray.array[idx].active)
        {
            system->dynamic_mesh_ray.array[idx].active = false;
            system->dynamic_mesh_ray.freeslots.push_back(idx);
            system->dynGrid.dirty = true;
        }
    }
    else
    {
        int idx = (mesh - &system->dynamic_mesh.array[0]);
        if (idx >= 0 && idx < system->dynamic_mesh.count && system->dynamic_mesh.array[idx].active)
        {
            system->dynamic_mesh.array[idx].active = false;
            system->dynamic_mesh.freeslots.push_back(idx);
            system->dynGrid.dirty = true;
        }
    }
}

void BuildDynamicGrid(Islander::Physics::NonRealisticPhysicsSystem* system)
{
    Islander::Collision::BuildDynamicGrid(system->dynGrid,
        system->grid.box,               // Dynamic grid size based on the static grid
        system->dynamic_mesh.array,
        system->dynamic_mesh.count,
        0xFFFFFF                        // Include all groups
    );
}

//bool CalculatePos(const Islander::Collision::CollisionStaticMesh* mesh, float* pos)
//{
//    float dist;
//    if (Islander::Physics::TestRayBox(pos[0], pos[1], pos[2], 0.0f, -1.0f, 0.0f, mesh->obb, dist))
//    {
//        pos[1] = pos[1] - dist;
//
//        return true;
//    }
//    return false;
//}

bool RampCapsuleCast(const Islander::Collision::CollisionStaticMesh* mesh, IslanderCollisionCapsule& capsule, float* velocity, float& t)
{
    IslanderCollisionCapsule capsuleCast = capsule;
    capsuleCast.centreX += velocity[0];
    capsuleCast.centreY += 1.0f;
    capsuleCast.centreZ += velocity[2];
    float capsuleCastVel[3] = { 0.0f, -2.0f, 0.0f };

    float normal[3];
    
    const float time = Islander::Collision::GJK_Swept(capsuleCast, mesh->obb, capsuleCastVel, normal);
    t = time * capsuleCastVel[1];
    return time < 1.0f;
}

bool MoveAlongHeightMap(const IslanderCollisionCapsule& capsule, IslanderCollisionData& data, const Islander::Collision::CollisionTransform& transform, float& moveUp)
{
    float* v0 = data.hits[0].v1;
    float* v1 = data.hits[0].v2;
    float* v2 = data.hits[0].v3;

    const float* minVec = v0;
    float maxY = v0[1];
    if (v1[1] > maxY) {
        maxY = v1[1];
        minVec = v1;
    } 
    if (v2[1] > maxY) {
        maxY = v2[1];
        minVec = v2;
    }

    float height = maxY + transform.py;

    moveUp = height + capsule.height / 2.0f + capsule.radius + 0.01f; // where the centre of the capsule needs to be

    return true;
} 

bool QueryStepDown(Islander::Physics::NonRealisticPhysicsSystem* system, Islander::Collision::CollisionDynamicMesh* mesh, IslanderCollisionCapsule& capsule, float maxStepDown, float& stepDownY)
{
    float offsetX[4] = { 0.0f,capsule.radius,0.0f,-capsule.radius };
    float offsetZ[4] = {capsule.radius,0.0f,-capsule.radius,0.0f};

    bool hit=false;
    float maxHitPosY = std::numeric_limits<float>::lowest();

    const float rayPosY = capsule.centreY;

    for (int i = 0; i < 4; i++)
    {
        IslanderCollisionRayInfo info;
        info.ignore_mesh = mesh;
        Islander::Physics::RayCast(system, nullptr, capsule.centreX + offsetX[i], rayPosY, capsule.centreZ + offsetZ[i], 0.0f, -1.0f, 0.0f, ISLANDER_COLLISION_GROUP_DEFAULT, &info);
        
        if (info.dynamic_mesh && info.dynamic_hit_pos[1] > maxHitPosY)
        {
            auto dyn = reinterpret_cast<Islander::Collision::CollisionDynamicMesh*>(info.dynamic_mesh);
            if (!dyn->info.trigger)
            {
                maxHitPosY = info.dynamic_hit_pos[1];
                hit = false;
            }
        }
        
        if (info.static_mesh && info.static_hit_pos[1] > maxHitPosY)
        {
            maxHitPosY = info.static_hit_pos[1];
            hit = true;
        }
    }

    if (hit)
    {
        float dist = maxHitPosY - rayPosY + capsule.height / 2.0f + capsule.radius;
        if (dist < 0 && dist > -maxStepDown)
        {
            stepDownY = maxHitPosY + capsule.height / 2.0f + capsule.radius + 0.01f;
            return true;
        }
    }

    return false;
}

template<bool gravityPass>
void RunCollisionIterations(Islander::Physics::NonRealisticPhysicsSystem* system, Islander::Collision::CollisionDynamicMesh* mesh, float delta)
{
    float initial_velocity[3] =
    {
        mesh->info.vx,
        mesh->info.vy,
        mesh->info.vz
    };

    float velocityModifier[3];
    if (gravityPass)
    {
        velocityModifier[0] = .0f;
        velocityModifier[1] = 1.0f;
        velocityModifier[2] = .0f;
    }
    else
    {
        velocityModifier[0] = 1.0f;
        velocityModifier[1] = .0f;
        velocityModifier[2] = 1.0f;
    }

    constexpr int MAX_ITERATIONS = ISLANDER_COLLISION_ITERATION_DATA_COUNT;
    constexpr int flags = gravityPass ? ISLANDER_COLLISION_HIT_FLAG_GRAVITY : 0;

    int iteration = 0;

    bool completed = false;
    while (!completed && iteration < MAX_ITERATIONS)
    {
        float x = mesh->info.px;
        float y = mesh->info.py;
        float z = mesh->info.pz;

        float rx = mesh->info.rx + (gravityPass ? 0.0f : mesh->info.rdx);
        float ry = mesh->info.ry + (gravityPass ? 0.0f : mesh->info.rdy);
        float rz = mesh->info.rz + (gravityPass ? 0.0f : mesh->info.rdz);

        float velocity[3] = { mesh->info.vx * velocityModifier[0], mesh->info.vy * velocityModifier[1], mesh->info.vz * velocityModifier[2] };

        auto anim = (Islander::Model::AnimationSystem*)mesh->info.animationSystem;
        if (anim)
        {
            int controller = mesh->info.parentControllerId;
            if (controller > -1 && anim->controllers[controller].animation.isRootMotion && mesh->meshData)
            {
                float rot[3] = { rx,ry,rz };
                float scale[3] = { mesh->info.sx,mesh->info.sy,mesh->info.sz };

                float displacement[3] = {};
                Islander::Model::CalculateRootMotion(mesh->meshData, anim, controller, true, rot, scale, displacement);

                velocity[0] = displacement[0] * velocityModifier[0];
                velocity[1] = displacement[1] * velocityModifier[1];
                velocity[2] = displacement[2] * velocityModifier[2];
            }
        }

        x += velocity[0];
        y += velocity[1];
        z += velocity[2];

        bool clearPlatform = gravityPass;

        float collisionTime=1.0f;
        float normal[3];
        bool collide = false;

        IslanderCollisionBox boxStart = mesh->info.box;
        IslanderCollisionSphere sphereStart = mesh->info.sphere;
        IslanderCollisionCapsule capsuleStart = mesh->info.capsule;
        IslanderCollisionOBB obbStart = mesh->info.obb;

        if (mesh->info.collisionShape == ISLANDER_COLLISION_SHAPE_BOX)
        {
            Islander::Collision::CalculateBox(boxStart, mesh);

            if ((mesh->info.trigger & ISLANDER_COLLISION_IGNORE_STATIC) != ISLANDER_COLLISION_IGNORE_STATIC)
            {
                Islander::Collision::CollisionEventList evlist;
                Islander::Collision::QueryGridCells<IslanderCollisionBox, flags>(system->grid, boxStart, velocity, mesh, evlist);

                if ((mesh->info.trigger & ISLANDER_COLLISION_IS_GHOST) != ISLANDER_COLLISION_IS_GHOST)
                {
                    if (mesh->colData.static_mesh_count > 0 && !gravityPass)
                    {
                        // Repeat with step up

                        QueryGridCellsStepUp(system->grid, boxStart, velocity, mesh, mesh->info.stepUpY, evlist);
                    }

                    float collisionY;
                    bool canstepup;
                    Islander::Collision::ResolveCollisions(evlist, collisionY, collisionTime, canstepup, collide, normal);
                    if (canstepup)
                    {
                        y += collisionY;
                        mesh->colData.static_mesh_count = 0;
                        mesh->colData.hit_count = 0;
                    }
                }
            }
        }
        else if (mesh->info.collisionShape == ISLANDER_COLLISION_SHAPE_SPHERE)
        {
            CalculateSphere(sphereStart, mesh);

            if ((mesh->info.trigger & ISLANDER_COLLISION_IGNORE_STATIC) != ISLANDER_COLLISION_IGNORE_STATIC)
            {
                Islander::Collision::CollisionEventList evlist;
                Islander::Collision::QueryGridCells<IslanderCollisionSphere, flags>(system->grid, sphereStart, velocity, mesh, evlist);
                
                if ((mesh->info.trigger & ISLANDER_COLLISION_IS_GHOST) != ISLANDER_COLLISION_IS_GHOST)
                {
                    if (mesh->colData.static_mesh_count > 0 && !gravityPass)
                    {
                        // Repeat with step up

                        QueryGridCellsStepUp(system->grid, sphereStart, velocity, mesh, mesh->info.stepUpY, evlist);
                    }

                    float collisionY;
                    bool canstepup;
                    Islander::Collision::ResolveCollisions(evlist, collisionY, collisionTime, canstepup, collide, normal);
                    if (canstepup)
                    {
                        y += collisionY;
                        mesh->colData.static_mesh_count = 0;
                        mesh->colData.hit_count = 0;
                    }
                }
            }
        }
        else if (mesh->info.collisionShape == ISLANDER_COLLISION_SHAPE_CAPSULE)
        {
            CalculateCapsule(capsuleStart, mesh);

            if ((mesh->info.trigger & ISLANDER_COLLISION_IGNORE_STATIC) != ISLANDER_COLLISION_IGNORE_STATIC)
            {
                Islander::Collision::CollisionEventList evlist;
                Islander::Collision::QueryGridCells<IslanderCollisionCapsule, flags>(system->grid, capsuleStart, velocity, mesh, evlist);

                if ((mesh->info.trigger & ISLANDER_COLLISION_IS_GHOST) != ISLANDER_COLLISION_IS_GHOST)
                {
                    bool rampHandled = false;
                    if (mesh->colData.static_mesh_count > 0 && !gravityPass)
                    {
                        // Repeat with ramp

                        // 1. Determine the object to try to use as a ramp

                        // TODO: can do a capsule cast downwards
                        //Islander::Collision::CollisionEventList evlist;
                        //Islander::Collision::QueryGridCells<IslanderCollisionCapsule, flags>(system->grid, capsuleStart, velocity, mesh, evlist);

                        auto ramp = reinterpret_cast<Islander::Collision::CollisionStaticMesh*>(mesh->colData.static_mesh[0]); // TODO: using this to get started!

                        if (ramp->heightmapCollider)
                        {
                            //Islander::Model::HeightmapDebugMoveAlongHeightmap moveDebug = {};
                            //moveDebug.collision = mesh;

                            float moveUpY;
                            //if (MoveAlongHeightMap(capsuleStart, velocity, ramp->heightmap, ramp->transform, moveUpY))
                            if (MoveAlongHeightMap(capsuleStart, mesh->colData, ramp->transform, moveUpY))
                            {
                                float dy = moveUpY - capsuleStart.centreY;

                                if (abs(dy) < mesh->info.stepUpY*2.0f)
                                {
                                    float vel[3] = { mesh->info.vx, dy, mesh->info.vz };
                                    //CopyVec3(vel, moveDebug.vel);

                                    Islander::Collision::CollisionEventList evlist2;
                                    QueryGridCells(system->grid, capsuleStart, vel, mesh, evlist2);
                                    if (evlist2.collisions.size() > 0 && evlist2.collisions[0].mesh->heightmapCollider)
                                    {
                                        evlist2.collisions.erase(evlist2.collisions.begin());
                                    }
                                    if (evlist2.collisions.size() == 0) // check if we collide moving along heightmap
                                    {
                                        y = moveUpY + (y - capsuleStart.centreY);

                                        mesh->colData.static_mesh_count = 0;
                                        mesh->colData.hit_count = 0;
                                        collide = false;
                                        collisionTime = 1.0f;

                                        rampHandled = true;
                                    }
                                }
                            }

                            //moveDebug.rampHandled = rampHandled;
                            //ramp->heightmap->debugInfo.moves.push_back(moveDebug);
                        }
                        else
                        {
                            // 2. Determine the ramp vector (and angle) and calculate new velocity if angle is ok.

                            const float posDelta = (y - capsuleStart.centreY);

                            float vel[3] = { mesh->info.vx, 0.0f, mesh->info.vz };
                            //float targetPos[3] = { capsuleStart.centreX + vel[0], capsuleStart.centreY + 1.0f, capsuleStart.centreZ + vel[2] };

                            float dist;
                            if (RampCapsuleCast(ramp, capsuleStart, vel, dist))
                            {
                                //float targetY = dist + (capsuleStart.centreY + 1.0f) + 0.001f;// +capsuleStart.height / 2.0f + capsuleStart.radius;
                                //vel[1] = targetY - capsuleStart.centreY;
                                vel[1] = 1.0f - std::abs(dist) + 0.001f;
                                const float magnitude = LengthSqr(vel);
                                const float incline = std::asin(vel[1] / magnitude);

                                if (incline < M_PI_4)
                                {
                                    y += vel[1];
                                    capsuleStart.centreY += vel[1];

                                    // 3. Do QueryGridCells with the new velocity.

                                    Islander::Collision::CollisionEventList evlist2;
                                    QueryGridCells(system->grid, capsuleStart, vel, mesh, evlist2);
                                    if (evlist2.collisions.size() == 0) // check if we collide upon step up
                                    {
                                        //y += vel[1] + posDelta;
                                        //y = targetPos[1] + capsuleStart.radius + capsuleStart.height / 2.0f;
                                        mesh->colData.static_mesh_count = 0;
                                        mesh->colData.hit_count = 0;
                                        collide = false;
                                        collisionTime = 1.0f;

                                        rampHandled = true;
                                    }
                                    else
                                    {
                                        // Re-resolve collisions without stepping up.
                                        evlist2.stepUps.clear();

                                        //float collisionY;
                                        //bool canstepup;
                                        //ResolveCollisions(evlist2, collisionY, collisionTime, canstepup, collide, normal);

                                        y -= vel[1];
                                        capsuleStart.centreY -= vel[1];
                                    }
                                }
                                vel[1] = 0.0f;
                            }
                        }
                    }

                    if (!rampHandled)
                    {
                        if (mesh->colData.static_mesh_count > 0 && !gravityPass)
                        {
                            // Repeat with step up

                            QueryGridCellsStepUp(system->grid, capsuleStart, velocity, mesh, mesh->info.stepUpY, evlist);
                        }
                        else if (mesh->colData.static_mesh_count == 0 && !gravityPass && mesh->info.vy < 0.0f)
                        {
                            // Test for step down

                            float stepDownY;
                            if (QueryStepDown(system, mesh, capsuleStart, mesh->info.stepUpY, stepDownY))
                            {
                                y = stepDownY + (y - capsuleStart.centreY);
                            }
                        }

                        float collisionY;
                        bool canstepup;
                        Islander::Collision::ResolveCollisions(evlist, collisionY, collisionTime, canstepup, collide, normal);
                        if (canstepup)
                        {
                            Islander::Collision::CollisionEventList evlist2;
                            IslanderCollisionCapsule capsule = capsuleStart;
                            capsule.centreY += collisionY;
                            float vel[3] = { mesh->info.vx, 0, mesh->info.vz };
                            QueryGridCells(system->grid, capsule, vel, mesh, evlist2);
                            if (evlist2.collisions.size() == 0) // check if we collide upon step up
                            {
                                y += collisionY;
                                mesh->colData.static_mesh_count = 0;
                                mesh->colData.hit_count = 0;
                            }
                            else
                            {
                                // Re-resolve collisions without stepping up.
                                evlist.stepUps.clear();
                                ResolveCollisions(evlist, collisionY, collisionTime, canstepup, collide, normal);
                            }
                        }
                    }
                }
            }
        }
        else if (mesh->info.collisionShape == ISLANDER_COLLISION_SHAPE_ORIENTATED_BOX)
        {
            CalculateOBB(obbStart, mesh);

            if ((mesh->info.trigger & ISLANDER_COLLISION_IGNORE_STATIC) != ISLANDER_COLLISION_IGNORE_STATIC)
            {
                Islander::Collision::CollisionEventList evlist;
                Islander::Collision::QueryGridCells<IslanderCollisionOBB, flags>(system->grid, obbStart, velocity, mesh, evlist);

                if ((mesh->info.trigger & ISLANDER_COLLISION_IS_GHOST) != ISLANDER_COLLISION_IS_GHOST)
                {
                    if (mesh->colData.static_mesh_count > 0 && !gravityPass)
                    {
                        // Repeat with step up

                        QueryGridCellsStepUp(system->grid, obbStart, velocity, mesh, mesh->info.stepUpY, evlist);
                    }

                    float collisionY;
                    bool canstepup;
                    Islander::Collision::ResolveCollisions(evlist, collisionY, collisionTime, canstepup, collide, normal);
                    if (canstepup)
                    {
                        Islander::Collision::CollisionEventList evlist2;
                        IslanderCollisionOBB obb = obbStart;

                        for (int i = 1; i < 24; i += 3)
                        {
                            obb.verticies[i] += collisionY;
                        }

                        float vel[3] = { mesh->info.vx, 0, mesh->info.vz };
                        QueryGridCells(system->grid, obb, vel, mesh, evlist2);
                        if (evlist2.collisions.size() == 0) // check if we collide upon step up
                        {
                            y += collisionY;
                            mesh->colData.static_mesh_count = 0;
                            mesh->colData.hit_count = 0;
                        }
                        else
                        {
                            // Re-resolve collisions without stepping up.
                            evlist.stepUps.clear();
                            ResolveCollisions(evlist, collisionY, collisionTime, canstepup, collide, normal);
                        }
                    }
                }
            }
        }

        if ((mesh->info.trigger & ISLANDER_COLLISION_IGNORE_DYNAMIC) != ISLANDER_COLLISION_IGNORE_DYNAMIC)
        {
            Islander::Collision::CollisionEventList evList;
            bool isNotTrigger = (mesh->info.trigger & ISLANDER_COLLISION_IS_TRIGGER) != ISLANDER_COLLISION_IS_TRIGGER;
            //constexpr int collisionTimeEpsilon = 0.00001f;

            constexpr int max_count = 32;
            Islander::Collision::CollisionDynamicMesh* meshes[max_count];
            int count = Islander::Collision::QueryDynamicGrid(system->dynGrid, mesh, max_count, meshes);

            //constexpr int max_count = 128;
            //Islander::Collision::CollisionDynamicMesh* meshes[max_count];
            //int count = 0;
            //for (int i = 0; i < system->dynGrid.cells.size(); i++)
            //{
            //    auto& cell = system->dynGrid.cells[i];
            //    for (auto& item : cell.items)
            //    {
            //        if (count < max_count)
            //        {
            //            meshes[count++] = item;
            //        }
            //    }
            //}

            // TODO: sometimes platform does not appear in the dynamic grid query.
            // Possibly, this is because the query does not use the swept bounding box?
            // Therefore include the platform.
            if (count < max_count && mesh->platformMesh != nullptr)
            {
                meshes[count++] = reinterpret_cast<Islander::Collision::CollisionDynamicMesh *>(mesh->platformMesh);
            }

            if (count < max_count && mesh->pushMesh != nullptr)
            {
                meshes[count++] = reinterpret_cast<Islander::Collision::CollisionDynamicMesh*>(mesh->pushMesh);
            }

            mesh->colData.it[mesh->colData.iterations].numCandidates = count;

            for (int j = 0; j < count; j++)
            {
                auto dynamic_mesh = meshes[j];
                if (dynamic_mesh != mesh)
                {
                    auto& candidate = mesh->colData.it[mesh->colData.iterations].candidates[j];
                    candidate.mesh = dynamic_mesh;
                    candidate.active = dynamic_mesh->active;
                    candidate.shape = dynamic_mesh->info.collisionShape;

                    if (!dynamic_mesh->active || dynamic_mesh->info.collisionShape == ISLANDER_COLLISION_SHAPE_NULL)
                    {
                        continue;
                    }

                    if (mesh->platformMesh == dynamic_mesh && !gravityPass)
                    {
                        continue;
                    }

                    if (dynamic_mesh->pushMesh == mesh)
                    {
                        continue;
                    }

                    int pen_iterations = 0;
                    float n[3];
                    if (mesh->info.collisionShape == ISLANDER_COLLISION_SHAPE_BOX)
                    {
                        if (abs(velocity[0]) + abs(velocity[2]) + abs(velocity[1]) > 0)
                        {
                            float t = Islander::Collision::DispatchCollisionSweptTest(boxStart, dynamic_mesh->info, velocity, n, &pen_iterations);
                            candidate.t = t;
                            candidate.pen_iterations = pen_iterations;
                            candidate.tested = true;
                            if (1 - t > 0)
                            {
                                AddDynamicCollider<flags>(mesh, dynamic_mesh);

                                if ((dynamic_mesh->info.trigger & ISLANDER_COLLISION_IS_TRIGGER) != ISLANDER_COLLISION_IS_TRIGGER && isNotTrigger)
                                {
                                    Islander::Collision::CollisionEvent ev;
                                    ev.collisionTime = t;
                                    ev.dynamic = true;
                                    ev.dynMesh = dynamic_mesh;
                                    CopyVec3(n, ev.normal);
                                    CollisionListInsertAtLowerBound(evList, t, ev);
                                }
                            }
                        }
                        else if (Islander::Collision::DispatchCollisionTest(boxStart, dynamic_mesh->info, n))
                        {
                            AddDynamicCollider<flags>(mesh, dynamic_mesh);
                        }
                    }
                    else if (mesh->info.collisionShape == ISLANDER_COLLISION_SHAPE_SPHERE)
                    {
                        if (abs(velocity[0]) + abs(velocity[2]) + abs(velocity[1]) > 0)
                        {
                            float t = Islander::Collision::DispatchCollisionSweptTest(sphereStart, dynamic_mesh->info, velocity, n, &pen_iterations);
                            candidate.t = t;
                            candidate.pen_iterations = pen_iterations;
                            candidate.tested = true;
                            if (1 - t > 0)
                            {
                                AddDynamicCollider<flags>(mesh, dynamic_mesh);

                                if ((dynamic_mesh->info.trigger & ISLANDER_COLLISION_IS_TRIGGER) != ISLANDER_COLLISION_IS_TRIGGER && isNotTrigger)
                                {
                                    Islander::Collision::CollisionEvent ev;
                                    ev.collisionTime = t;
                                    ev.dynamic = true;
                                    ev.dynMesh = dynamic_mesh;
                                    CopyVec3(n, ev.normal);
                                    CollisionListInsertAtLowerBound(evList, t, ev);
                                }
                            }
                        }
                        else if (Islander::Collision::DispatchCollisionTest(sphereStart, dynamic_mesh->info, n))
                        {
                            AddDynamicCollider<flags>(mesh, dynamic_mesh);
                        }
                    }
                    else if (mesh->info.collisionShape == ISLANDER_COLLISION_SHAPE_CAPSULE)
                    {
                        if (abs(velocity[0]) + abs(velocity[2]) + abs(velocity[1]) > 0)
                        {
                            float t = Islander::Collision::DispatchCollisionSweptTest(capsuleStart, dynamic_mesh->info, velocity, n, &pen_iterations);
                            candidate.t = t;
                            candidate.pen_iterations = pen_iterations;
                            candidate.tested = true;
                            if (1 - t > 0)
                            {
                                AddDynamicCollider<flags>(mesh, dynamic_mesh);

                                if ((dynamic_mesh->info.trigger & ISLANDER_COLLISION_IS_TRIGGER) != ISLANDER_COLLISION_IS_TRIGGER && isNotTrigger)
                                {
                                    Islander::Collision::CollisionEvent ev;
                                    ev.collisionTime = t;
                                    ev.dynamic = true;
                                    ev.dynMesh = dynamic_mesh;
                                    CopyVec3(n, ev.normal);
                                    CollisionListInsertAtLowerBound(evList, t, ev);
                                }
                            }
                        }
                        else if (Islander::Collision::DispatchCollisionTest(capsuleStart, dynamic_mesh->info, n))
                        {
                            AddDynamicCollider<flags>(mesh, dynamic_mesh);
                        }
                    }
                    else if (mesh->info.collisionShape == ISLANDER_COLLISION_SHAPE_ORIENTATED_BOX)
                    {
                        if (abs(velocity[0]) + abs(velocity[2]) + abs(velocity[1]) > 0)
                        {
                            float t = Islander::Collision::DispatchCollisionSweptTest(obbStart, dynamic_mesh->info, velocity, n, &pen_iterations);
                            candidate.t = t;
                            candidate.pen_iterations = pen_iterations;
                            candidate.tested = true;
                            if (1 - t > 0)
                            {
                                AddDynamicCollider<flags>(mesh, dynamic_mesh);

                                if ((dynamic_mesh->info.trigger & ISLANDER_COLLISION_IS_TRIGGER) != ISLANDER_COLLISION_IS_TRIGGER && isNotTrigger)
                                {
                                    Islander::Collision::CollisionEvent ev;
                                    ev.collisionTime = t;
                                    ev.dynamic = true;
                                    ev.dynMesh = dynamic_mesh;
                                    CopyVec3(n, ev.normal);
                                    CollisionListInsertAtLowerBound(evList, t, ev);
                                }
                            }
                        }
                        else if (Islander::Collision::DispatchCollisionTest(obbStart, dynamic_mesh->info, n))
                        {
                            AddDynamicCollider<flags>(mesh, dynamic_mesh);
                        }
                    }
                }
            }

            // TODO: when velocity is 0 and a collision occurs it will cause an issue
            if (evList.collisions.size() > 0)
            {
                float dynCollisionTime; float collisionY; bool stepUp; bool dynCollide; float dynNormal[3] = {};
                const int index = ResolveCollisions(evList, collisionY, dynCollisionTime, stepUp, dynCollide, dynNormal);
                if (index > -1 && dynCollisionTime < collisionTime)
                {
                    collisionTime = dynCollisionTime;
                    collide = dynCollide;
                    CopyVec3(dynNormal, normal);

                    if (evList.collisions[index].dynMesh->info.platformType == ISLANDER_COLLISION_IS_PLATFORM)
                    {
                        if (gravityPass)
                        {
                            if (mesh->platformMesh == nullptr)
                            {
                                // HACK: copy the mesh properties and move it back to where it would have been previously if platform didn't update..
                                auto copy = evList.collisions[index].dynMesh->info;
                                if (evList.collisions[index].dynMesh->active)
                                {
                                    copy.py -= copy.vy;
                                }

                                if (mesh->info.collisionShape == ISLANDER_COLLISION_SHAPE_CAPSULE &&
                                    Islander::Collision::IsAbove(capsuleStart, /*evList.collisions[index].dynMesh->info*/ copy))
                                {
                                    mesh->platformMesh = evList.collisions[index].dynMesh;
                                    clearPlatform = false;

                                    // Stepup
                                    const float stepY = Islander::Collision::StepUpToPlatform(capsuleStart, evList.collisions[index].dynMesh->info);
                                    const float dist = stepY - capsuleStart.centreY;
                                    if (dist < mesh->info.stepUpY && dist > 0)
                                    {
                                        //y += dist;
                                        mesh->info.py += dist;
                                        collisionTime = 0.0f;
                                        collide = true;
                                    }
                                }
                                else if (mesh->info.collisionShape == ISLANDER_COLLISION_SHAPE_BOX &&
                                    Islander::Collision::IsAbove(boxStart, /*evList.collisions[index].dynMesh->info*/ copy))
                                {
                                    mesh->platformMesh = evList.collisions[index].dynMesh;
                                    clearPlatform = false;
                                }
                            }
                            else
                            {
                                if (mesh->info.collisionShape == ISLANDER_COLLISION_SHAPE_CAPSULE &&
                                    Islander::Collision::IsAbove(capsuleStart, evList.collisions[index].dynMesh->info))
                                {
                                    mesh->platformMesh = evList.collisions[index].dynMesh;
                                    clearPlatform = false;
                                }
                                else if (mesh->info.collisionShape == ISLANDER_COLLISION_SHAPE_BOX &&
                                    Islander::Collision::IsAbove(boxStart, evList.collisions[index].dynMesh->info))
                                {
                                    mesh->platformMesh = evList.collisions[index].dynMesh;
                                    clearPlatform = false;
                                }
                            }
                        }
                        else
                        {
                            if (mesh->info.collisionShape == ISLANDER_COLLISION_SHAPE_CAPSULE)
                            {
                                // Stepup
                                const float stepY = Islander::Collision::StepUpToPlatform(capsuleStart, evList.collisions[index].dynMesh->info);
                                const float dist = stepY - capsuleStart.centreY;
                                if (dist < mesh->info.stepUpY && dist > 0)
                                {
                                    y += dist;
                                    mesh->platformMesh = evList.collisions[index].dynMesh;
                                    clearPlatform = false;
                                    collide = false;
                                }
                            }
                        }
                    }
                    else if (index > -1 && evList.collisions[index].dynMesh->info.pushType == ISLANDER_COLLISION_IS_PUSHABLE && mesh->info.pushType == ISLANDER_COLLISION_IS_PUSHER)
                    {
                        if (!gravityPass)
                        {
                            mesh->pushMesh = evList.collisions[index].dynMesh;
                        }
                    }
                }
            }
        }

        if (clearPlatform)
        {
            if (mesh->platformMesh != nullptr)
            {
                mesh->platformMesh = nullptr;
            }
        }

        if (!collide)
        {
            if (!gravityPass)
            {
                mesh->colData.pushing = mesh->pushMesh != nullptr;
                mesh->colData.grounded = true;
            }
            else
            {
                mesh->colData.falling = false;
                if (mesh->info.vy * velocityModifier[1] < 0.0f)
                {
                    mesh->coyoteTimeElapsed += delta;

                    if (mesh->coyoteTimeElapsed < mesh->info.coyoteTime)
                    {
                        y = mesh->info.py;
                    }
                    else
                    {
                        mesh->colData.falling = true;
                        mesh->colData.grounded = false;
                    }
                }
            }

            mesh->info.px = x;
            mesh->info.py = y;
            mesh->info.pz = z;
            mesh->info.rx = rx;
            mesh->info.ry = ry;
            mesh->info.rz = rz;
            completed = true;

            mesh->colData.it[mesh->colData.iterations].collide = false;
            mesh->colData.it[mesh->colData.iterations].gravity = gravityPass;
            mesh->colData.it[mesh->colData.iterations].velocity[0] = mesh->info.vx;
            mesh->colData.it[mesh->colData.iterations].velocity[1] = mesh->info.vy;
            mesh->colData.it[mesh->colData.iterations].velocity[2] = mesh->info.vz;
            mesh->colData.it[mesh->colData.iterations].pos[0] = mesh->info.px;
            mesh->colData.it[mesh->colData.iterations].pos[1] = mesh->info.py;
            mesh->colData.it[mesh->colData.iterations].pos[2] = mesh->info.pz;
        }
        else
        {
            if (gravityPass)
            {
                if (mesh->info.vy * velocityModifier[1] < 0.0f)
                {
                    mesh->coyoteTimeElapsed = 0.0f;
                }
                mesh->colData.falling = false;
            }
            else
            {
                mesh->colData.pushing = mesh->pushMesh != nullptr;
                mesh->colData.grounded = true;
            }

            //mesh->colData.it[mesh->colData.iterations].collide = true;
            //mesh->colData.it[mesh->colData.iterations].gravity = gravityPass;
            //mesh->colData.it[mesh->colData.iterations].time = collisionTime;
            //mesh->colData.it[mesh->colData.iterations].normal[0] = normal[0];
            //mesh->colData.it[mesh->colData.iterations].normal[1] = normal[1];
            //mesh->colData.it[mesh->colData.iterations].normal[2] = normal[2];
            //mesh->colData.it[mesh->colData.iterations].velocity[0] = mesh->info.vx;
            //mesh->colData.it[mesh->colData.iterations].velocity[1] = mesh->info.vy;
            //mesh->colData.it[mesh->colData.iterations].velocity[2] = mesh->info.vz;
            //mesh->colData.it[mesh->colData.iterations].pos[0] = mesh->info.px;
            //mesh->colData.it[mesh->colData.iterations].pos[1] = mesh->info.py;
            //mesh->colData.it[mesh->colData.iterations].pos[2] = mesh->info.pz;

            // Epsilon is needed since floating point isn't deterministic and where one frame a collision occurs the next frame
            // a collision may not occur if they are too close to the object they collided with in the first frame.
            const float epsilon = std::min(collisionTime, 0.01f/*0.5f*/); //0.0001f;

            constexpr float skinWidth = 0.01f;/*0.5f;*/
            float skinCollisionTimeX = mesh->info.vx == 0 ? std::numeric_limits<float>::infinity() : (abs(mesh->info.vx) * collisionTime - skinWidth) / abs(mesh->info.vx);
            float skinCollisionTimeY = mesh->info.vy == 0 ? std::numeric_limits<float>::infinity() : (abs(mesh->info.vy) * collisionTime - skinWidth) / abs(mesh->info.vy);
            float skinCollisionTimeZ = mesh->info.vz == 0 ? std::numeric_limits<float>::infinity() : (abs(mesh->info.vz) * collisionTime - skinWidth) / abs(mesh->info.vz);
            float skinCollisionTime = std::max(0.0f, std::min(skinCollisionTimeX, std::min(skinCollisionTimeY, skinCollisionTimeZ)));

            assert(collisionTime >= 0.0f && collisionTime <= 1.0f);

            // SkinCollisionTime can be infinity if the velocity is 0. This could occur if the object is a trigger thus won't be moved out of the mesh its colliding with.
            // We shouldn't perform any collision response on these.
            if (isinf(skinCollisionTime))
            {
                completed = true;
            }
            else if (!gravityPass)
            {
                //mesh->info.px += collisionTime * mesh->info.vx +normal[0] * epsilon;
                //mesh->info.py += collisionTime * mesh->info.vy +normal[1] * epsilon;
                //mesh->info.pz += collisionTime * mesh->info.vz +normal[2] * epsilon;

                //mesh->info.px += collisionTime * mesh->info.vx - mesh->info.vx * epsilon;
                //mesh->info.py += collisionTime * mesh->info.vy - mesh->info.vy * epsilon;
                //mesh->info.pz += collisionTime * mesh->info.vz - mesh->info.vz * epsilon;

                collisionTime = skinCollisionTime;
                mesh->info.px += collisionTime * mesh->info.vx * velocityModifier[0];
                mesh->info.py += collisionTime * mesh->info.vy * velocityModifier[1];
                mesh->info.pz += collisionTime * mesh->info.vz * velocityModifier[2];

                // TODO: we can allow rebounding as well (generally handled by game code)

                if (mesh->pushMesh == nullptr)
                {
                    // slide 
                    float dotprod = (mesh->info.vx * velocityModifier[0] * normal[2] + normal[0] * velocityModifier[2] * mesh->info.vz) * (1 - collisionTime);
                    float vx = dotprod * normal[2]; float vz = dotprod * normal[0];

                    mesh->info.vx = vx;
                    mesh->info.vy = 0;
                    mesh->info.vz = vz;
                    completed = abs(vx) + abs(vz) < 0.00001f;
                }
                else
                {
                    //mesh->info.vx = 0.0f;
                    //mesh->info.vy = 0.0f;
                    //mesh->info.vz = 0.0f;
                    completed = true;
                }

                //float v = Islander::LengthSqr(mesh->info.vx, mesh->info.vy, mesh->info.vz);
                //float invNormal[3] =
                //{
                //    -normal[0] * v,
                //    -normal[1] * v,
                //    -normal[2] * v
                //};
                //float dir[3] =
                //{
                //    mesh->info.vx - invNormal[0],
                //    mesh->info.vy - invNormal[1],
                //    mesh->info.vz - invNormal[2]
                //};

                //mesh->info.vx = dir[0];
                //mesh->info.vy = dir[1];
                //mesh->info.vz = dir[2];

                //completed = abs(mesh->info.vx) + abs(mesh->info.vy) + abs(mesh->info.vz) < 0.00001f;
            }
            else
            {
                //mesh->info.px += collisionTime * mesh->info.vx + normal[0] * epsilon;
                //mesh->info.py += collisionTime * mesh->info.vy + normal[1] * epsilon;
                //mesh->info.pz += collisionTime * mesh->info.vz + normal[2] * epsilon;

                //mesh->info.px += collisionTime * mesh->info.vx - mesh->info.vx * epsilon;
                //mesh->info.py += collisionTime * mesh->info.vy - mesh->info.vy * epsilon;
                //mesh->info.pz += collisionTime * mesh->info.vz - mesh->info.vz * epsilon;

                collisionTime = skinCollisionTime;

                //float vel[3] = {
                //    /*collisionTime **/ mesh->info.vx * velocityModifier[0],
                //    /*collisionTime **/ mesh->info.vy * velocityModifier[1],
                //    /*collisionTime **/ mesh->info.vz * velocityModifier[2]
                //};
                //Reflect(vel, normal, vel);

                //if (vel[1] < 0.0f)
                //{
                //    mesh->info.vx = vel[0];
                //    mesh->info.vy = 0.0f;
                //    mesh->info.vz = vel[2];

                //    mesh->info.py += vel[1] * collisionTime;
                //}
                //else
                //{
                //    mesh->info.px += collisionTime * mesh->info.vx * velocityModifier[0];
                //    mesh->info.py += collisionTime * mesh->info.vy * velocityModifier[1];
                //    mesh->info.pz += collisionTime * mesh->info.vz * velocityModifier[2];
                //}

                mesh->info.px += collisionTime * mesh->info.vx * velocityModifier[0];
                mesh->info.py += collisionTime * mesh->info.vy * velocityModifier[1];
                mesh->info.pz += collisionTime * mesh->info.vz * velocityModifier[2];

                completed = true;
            }

            mesh->info.rx = rx;
            mesh->info.ry = ry;
            mesh->info.rz = rz;

            mesh->info.rdx = 0;
            mesh->info.rdy = 0;
            mesh->info.rdz = 0;

            mesh->colData.it[mesh->colData.iterations].collide = true;
            mesh->colData.it[mesh->colData.iterations].gravity = gravityPass;
            mesh->colData.it[mesh->colData.iterations].time = collisionTime;
            mesh->colData.it[mesh->colData.iterations].normal[0] = normal[0];
            mesh->colData.it[mesh->colData.iterations].normal[1] = normal[1];
            mesh->colData.it[mesh->colData.iterations].normal[2] = normal[2];
            mesh->colData.it[mesh->colData.iterations].velocity[0] = mesh->info.vx;
            mesh->colData.it[mesh->colData.iterations].velocity[1] = mesh->info.vy;
            mesh->colData.it[mesh->colData.iterations].velocity[2] = mesh->info.vz;
            mesh->colData.it[mesh->colData.iterations].pos[0] = mesh->info.px;
            mesh->colData.it[mesh->colData.iterations].pos[1] = mesh->info.py;
            mesh->colData.it[mesh->colData.iterations].pos[2] = mesh->info.pz;
        }

        mesh->colData.iterations++;
        iteration++;
    }

    // restore velocity
    mesh->info.vx = initial_velocity[0];
    mesh->info.vy = initial_velocity[1];
    mesh->info.vz = initial_velocity[2];

    if (mesh->pushMesh != nullptr)
    {
        float velocity[3] =
        {
            mesh->info.vx,
            0.0f,
            mesh->info.vz
        };

        auto dynamic_mesh = reinterpret_cast<Islander::Collision::CollisionDynamicMesh*>(mesh->pushMesh);

        if (mesh->info.collisionShape == ISLANDER_COLLISION_SHAPE_CAPSULE)
        {
            IslanderCollisionCapsule capsuleStart = mesh->info.capsule;
            CalculateCapsule(capsuleStart, mesh);
            float n[3] = {};
            int pen_iterations = 0;
            float t = Islander::Collision::DispatchCollisionSweptTest(capsuleStart, dynamic_mesh->info, velocity, n, &pen_iterations);
            if (1 - t <= 0)
            {
                mesh->pushMesh = nullptr;
            }
        }
    }
}

void MovementPass(Islander::Physics::NonRealisticPhysicsSystem* system, float delta)
{
    // platforms algo
    // 1. Move platforms, don't perform any collision detection
    // 2. Record the distance they travelled (its equal to velocity)
    // 3. Adjust any objects which are on the platform by the same value

    // Pushing algo
    // 1. Move pushable by desired amount (of pusher) with collision detection
    // 2. Record the distance travelled
    // 3. Adjust pusher to have the same velocity

    // Move platforms (no collision detection)
    for (int i = 0; i < system->dynamic_mesh.count; i++)
    {
        auto mesh = &system->dynamic_mesh.array[i];
        if (mesh->active && mesh->info.platformType == 1)
        {
            mesh->info.px += mesh->info.vx;
            mesh->info.pz += mesh->info.vz;
        }
    }

    BuildCollisionGrid(system->grid, system->static_mesh.count, system->static_mesh.array);
    BuildDynamicGrid(system);

    // Calculate pushable velocity
    for (int i = 0; i < system->dynamic_mesh.count; i++)
    {
        auto mesh = &system->dynamic_mesh.array[i];
        if (mesh->active && mesh->info.pushType == ISLANDER_COLLISION_IS_PUSHER &&
            mesh->pushMesh != nullptr)
        {
            auto push = reinterpret_cast<Islander::Collision::CollisionDynamicMesh*>(mesh->pushMesh);
            push->info.vx += mesh->info.vx;
            push->info.vz += mesh->info.vz;
        }
    }

    // Move pushables
    for (int i = 0; i < system->dynamic_mesh.count; i++)
    {
        auto mesh = &system->dynamic_mesh.array[i];
        if (mesh->active && mesh->info.pushType == ISLANDER_COLLISION_IS_PUSHABLE)
        {
            if (mesh->platformMesh != nullptr)
            {
                auto platform = reinterpret_cast<Islander::Collision::CollisionDynamicMesh*>(mesh->platformMesh);

                mesh->info.px += platform->info.vx;
                mesh->info.pz += platform->info.vz;
            }

            RunCollisionIterations<false>(system, mesh, delta);
        }
    }

    // Move regular objects
    for (int i = 0; i < system->dynamic_mesh.count; i++)
    {
        auto mesh = &system->dynamic_mesh.array[i];
        if (mesh->active && mesh->info.platformType == 0 && mesh->info.pushType != ISLANDER_COLLISION_IS_PUSHABLE)
        {
            if (mesh->platformMesh != nullptr)
            {
                auto platform = reinterpret_cast<Islander::Collision::CollisionDynamicMesh*>(mesh->platformMesh);

                mesh->info.px += platform->info.vx;
                mesh->info.pz += platform->info.vz;
            }

            RunCollisionIterations<false>(system, mesh, delta);
        }
    }
}

void GravityPass(Islander::Physics::NonRealisticPhysicsSystem* system, float delta)
{
    // platforms algo
    // 1. Move platforms, don't perform any collision detection
    // 2. Record the distance they travelled (its equal to velocity)
    // 3. Adjust any objects which are on the platform by the same value

    // Pushing algo
    // 1. Move pushable by desired amount (of pusher) with collision detection
    // 2. Record the distance travelled
    // 3. Adjust pusher to have the same velocity

    // Move platforms (no collision detection)
    for (int i = 0; i < system->dynamic_mesh.count; i++)
    {
        auto mesh = &system->dynamic_mesh.array[i];
        if (mesh->active && mesh->info.platformType == 1)
        {
            mesh->info.py += mesh->info.vy;
        }
    }

    BuildCollisionGrid(system->grid, system->static_mesh.count, system->static_mesh.array);
    BuildDynamicGrid(system);

    // Calculate pushable velocity
    for (int i = 0; i < system->dynamic_mesh.count; i++)
    {
        auto mesh = &system->dynamic_mesh.array[i];
        if (mesh->active && mesh->info.pushType == ISLANDER_COLLISION_IS_PUSHER &&
            mesh->pushMesh != nullptr)
        {
            auto push = reinterpret_cast<Islander::Collision::CollisionDynamicMesh*>(mesh->pushMesh);
            push->info.vy += mesh->info.vy;
        }
    }

    // Move pushables
    for (int i = 0; i < system->dynamic_mesh.count; i++)
    {
        auto mesh = &system->dynamic_mesh.array[i];
        if (mesh->active && mesh->info.pushType == ISLANDER_COLLISION_IS_PUSHABLE)
        {
            if (mesh->platformMesh != nullptr)
            {
                auto platform = reinterpret_cast<Islander::Collision::CollisionDynamicMesh*>(mesh->platformMesh);

                mesh->info.py += platform->info.vy;
            }

            RunCollisionIterations<true>(system, mesh, delta);
        }
    }

    // Move regular objects
    for (int i = 0; i < system->dynamic_mesh.count; i++)
    {
        auto mesh = &system->dynamic_mesh.array[i];
        if (mesh->active && mesh->info.platformType == 0 && mesh->info.pushType != ISLANDER_COLLISION_IS_PUSHABLE)
        {
            if (mesh->platformMesh != nullptr)
            {
                auto platform = reinterpret_cast<Islander::Collision::CollisionDynamicMesh*>(mesh->platformMesh);

                mesh->info.py += platform->info.vy;
            }

            RunCollisionIterations<true>(system, mesh, delta);
        }
    }
}

void Islander::Physics::RunNonRealisticPhysics(Islander::Physics::NonRealisticPhysicsSystem* system, float delta)
{
    for (int i = 0; i < system->dynamic_mesh.count; i++)
    {
        auto mesh = &system->dynamic_mesh.array[i];
        if (mesh->active)
        {
            ClearColliders(mesh);
        }
    }

    MovementPass(system, delta);
    GravityPass(system, delta);

    // Set to dirty since dynamic meshes moved.
    system->dynGrid.dirty = true;
}

void Islander::Physics::CreateDebugGeometry(NonRealisticPhysicsSystem* system, Islander::Collision::CollisionDynamicMesh* mesh, IslanderCollisionGeometryData* gData)
{
    if (mesh->info.collisionShape == ISLANDER_COLLISION_SHAPE_CAPSULE)
    {
        IslanderCollisionCapsule capsule = mesh->info.capsule;
        CalculateCapsule(capsule, mesh);

        float top[3] = { 0, capsule.height / 2, 0 };
        float bottom[3] = { 0, -capsule.height / 2, 0 };

        const int slices = 12;
        const int stacks = 12;

        //int middle_vertex_count = 2 * segments;
        //int middle_index_count = 3 * segments;
        //int hemisphere_vertex_count = 2 * (segments - 1);
        //int hemisphere_index_count = 3 * (segments - 1);

        //gData->strideSize = 9 * sizeof(float);
        //gData->vertexCount = middle_vertex_count + hemisphere_vertex_count;
        //gData->vertexData = new float[gData->vertexCount * 9];
        //gData->indexCount = middle_index_count + hemisphere_index_count;
        //gData->indexData = new int[gData->indexCount];

        int sphere_vertex_count = (stacks - 1) * slices + 2;
        int sphere_index_count = slices * 6 + (stacks - 2) * slices * 6;

        gData->strideSize = 9 * sizeof(float);
        gData->vertexCount = sphere_vertex_count;
        gData->vertexData = new float[gData->vertexCount * 9];
        gData->indexCount = sphere_index_count;
        gData->indexData = new int[gData->indexCount];

        memset(gData->vertexData, 0, (unsigned long long)gData->vertexCount * 9 * sizeof(float));

        int vertexOffset = 0;

        gData->vertexData[0] = capsule.centreX;
        gData->vertexData[1] = capsule.centreY + capsule.radius + capsule.height / 2; // top vertex
        gData->vertexData[2] = capsule.centreZ;
        vertexOffset += 9;

        for (int j = 0; j < stacks - 1; j++)
        {
            float theta = M_PI * (j + 1) / stacks;

            for (int i = 0; i < slices; i++)
            {
                float phi = M_PI * 2 * i / slices;
                float dirX = sinf(theta) * cosf(phi);
                float dirY = cosf(theta);
                float dirZ = sinf(theta) * sinf(phi);

                float y = capsule.centreY;
                if (dirY > 0)
                {
                    y += capsule.height / 2;
                }
                else
                {
                    y -= capsule.height / 2;
                }

                gData->vertexData[vertexOffset] = capsule.centreX + dirX * capsule.radius;
                gData->vertexData[vertexOffset + 1] = y + dirY * capsule.radius;
                gData->vertexData[vertexOffset + 2] = capsule.centreZ + dirZ * capsule.radius;
                vertexOffset += 9;
            }
        }

        gData->vertexData[vertexOffset] = capsule.centreX;
        gData->vertexData[vertexOffset + 1] = capsule.centreY - capsule.radius - capsule.height / 2; // bottom
        gData->vertexData[vertexOffset + 2] = capsule.centreZ;

        int indexOffset = 0;

        // top/bottom tris
        for (int i = 0; i < slices; i++)
        {
            int i0 = i + 1;
            int i1 = (i + 1) % slices + 1;
            gData->indexData[indexOffset] = 0;
            gData->indexData[indexOffset + 1] = i1;
            gData->indexData[indexOffset + 2] = i0;

            int i2 = i + slices * (stacks - 2) + 1;
            int i3 = (i + 1) % slices + slices * (stacks - 2) + 1;
            gData->indexData[indexOffset + 3] = sphere_vertex_count - 1;
            gData->indexData[indexOffset + 4] = i2;
            gData->indexData[indexOffset + 5] = i3;
            indexOffset += 6;
        }

        // Add tris per stacks/slices
        for (int j = 0; j < stacks - 2; j++)
        {
            int j0 = j * slices + 1;
            int j1 = (j + 1) * slices + 1;

            for (int i = 0; i < slices; i++)
            {
                int i0 = j0 + i;
                int i1 = j0 + (i + 1) % slices;
                int i2 = j1 + (i + 1) % slices;
                int i3 = j1 + i;

                gData->indexData[indexOffset] = i0;
                gData->indexData[indexOffset + 1] = i1;
                gData->indexData[indexOffset + 2] = i2;
                gData->indexData[indexOffset + 3] = i1;
                gData->indexData[indexOffset + 4] = i2;
                gData->indexData[indexOffset + 5] = i3;

                indexOffset += 6;
            }
        }

        //for (int i = 0; i < segments; i++)
        //{
        //    float angle = M_PI * 2 * i / segments;
        //    float dirX = sinf(angle);
        //    float dirZ = cosf(angle);

        //    float data[18] =
        //    {
        //        bottom[0] + dirX * capsule.radius, bottom[1], bottom[2] + dirZ * capsule.radius,
        //        0, 0, 0,
        //        0, 0, 0,
        //        top[0] + dirX * capsule.radius, top[1], top[2] + dirZ * capsule.radius,
        //        0, 0, 0,
        //        0, 0, 0
        //    };

        //    memcpy(&gData->vertexData[i * 18], data, sizeof(data));

        //    gData->indexData[i * 3] = i * 2;
        //    gData->indexData[i * 3 + 1] = i * 2;
        //    gData->indexData[i * 3 + 2] = i * 2 + 1;
        //}

        //float points[segments * 3];
        //for (int i = 0; i < segments; i++)
        //{
        //    float angle = M_PI * 2 * i / segments;
        //    float dirX = sinf(angle);
        //    float dirZ = cosf(angle);

        //    points[i * 3] = bottom[0] + dirX * capsule.radius;
        //    points[i * 3 + 1] = bottom[1];// +dirX * capsule.radius;
        //    points[i * 3 + 2] = bottom[2] + dirZ * capsule.radius;
        //}

        //for (int i = 0; i < segments - 1; i++)
        //{
        //    float data[18] =
        //    {
        //        points[i * 3], points[i * 3 + 1], points[i * 3 + 2],
        //        0, 0, 0,
        //        0, 0, 0,
        //        points[i * 3 + 3], points[i * 3 + 4], points[i * 3 + 5],
        //        0, 0, 0,
        //        0, 0, 0
        //    };

        //    memcpy(&gData->vertexData[i * 18 + middle_vertex_count * 9], data, sizeof(data));

        //    gData->indexData[i * 3 + middle_index_count] = i * 2 + middle_vertex_count;
        //    gData->indexData[i * 3 + 1 + middle_index_count] = i * 2 + middle_vertex_count;
        //    gData->indexData[i * 3 + 2 + middle_index_count] = i * 2 + 1 + middle_vertex_count;
        //}
    }
    else if (mesh->info.collisionShape == ISLANDER_COLLISION_SHAPE_SPHERE)
    {
        auto sphere = mesh->info.sphere;
        CalculateSphere(sphere, mesh);

        const int slices = 12;
        const int stacks = 12;

        int sphere_vertex_count = (stacks-1) * slices + 2;
        int sphere_index_count = slices * 6 + (stacks-2)*slices *6;

        gData->strideSize = 9 * sizeof(float);
        gData->vertexCount = sphere_vertex_count;
        gData->vertexData = new float[gData->vertexCount * 9];
        gData->indexCount = sphere_index_count;
        gData->indexData = new int[gData->indexCount];

        memset(gData->vertexData, 0, (unsigned long long)gData->vertexCount * 9 * sizeof(float));

        // Sphere equations
        // x = p * sin(theta) * cos(phi)
        // y = p * sin(theta) * sin(phi)
        // z = p * cos(phi)
        // where p is radius

        int vertexOffset = 0;

        gData->vertexData[0] = sphere.centreX;
        gData->vertexData[1] = sphere.centreY + sphere.radius; // top vertex
        gData->vertexData[2] = sphere.centreZ;
        vertexOffset += 9;

        for (int j = 0; j < stacks - 1; j++)
        {
            float theta = M_PI * (j + 1) / stacks;

            for (int i = 0; i < slices; i++)
            {
                float phi = M_PI * 2 * i / slices;
                float dirX = sinf(theta) * cosf(phi);
                float dirY = cosf(theta);
                float dirZ = sinf(theta) * sinf(phi);

                gData->vertexData[vertexOffset] = sphere.centreX + dirX * sphere.radius;
                gData->vertexData[vertexOffset + 1] = sphere.centreY + dirY * sphere.radius;
                gData->vertexData[vertexOffset + 2] = sphere.centreZ + dirZ * sphere.radius;
                vertexOffset += 9;
            }
        }

        gData->vertexData[vertexOffset] = sphere.centreX;
        gData->vertexData[vertexOffset + 1] = sphere.centreY -sphere.radius; // bottom
        gData->vertexData[vertexOffset + 2] = sphere.centreZ;
        int indexOffset = 0;

        // top/bottom tris
        for (int i = 0; i < slices; i++)
        {
            int i0 = i + 1;
            int i1 = (i + 1) % slices + 1;
            gData->indexData[indexOffset] = 0;
            gData->indexData[indexOffset + 1] = i1;
            gData->indexData[indexOffset + 2] = i0;

            int i2 = i + slices * (stacks - 2) + 1;
            int i3 = (i + 1) % slices + slices * (stacks - 2) + 1;
            gData->indexData[indexOffset + 3] = sphere_vertex_count - 1;
            gData->indexData[indexOffset + 4] = i2;
            gData->indexData[indexOffset + 5] = i3;
            indexOffset += 6;
        }

        // Add tris per stacks/slices
        for (int j = 0; j < stacks - 2; j++)
        {
            int j0 = j * slices + 1;
            int j1 = (j + 1) * slices + 1;

            for (int i = 0; i < slices; i++)
            {
                int i0 = j0 + i;
                int i1 = j0 + (i + 1) % slices;
                int i2 = j1 + (i + 1) % slices;
                int i3 = j1 + i;

                gData->indexData[indexOffset] = i0;
                gData->indexData[indexOffset + 1] = i1;
                gData->indexData[indexOffset + 2] = i2;
                gData->indexData[indexOffset + 3] = i1;
                gData->indexData[indexOffset + 4] = i2;
                gData->indexData[indexOffset + 5] = i3;

                indexOffset += 6;
            }
        }
    }
    else if (mesh->info.collisionShape == ISLANDER_COLLISION_SHAPE_ORIENTATED_BOX)
    {
        auto obb = mesh->info.obb;
        CalculateOBB(obb, mesh);

        gData->indexCount = 3 * 12;
        gData->vertexCount = 8;
        gData->strideSize = 9 * sizeof(float);
        gData->vertexData = new float[8 * 9];
        gData->indexData = new int32_t[gData->indexCount];

        float data[] =
        {
            obb.verticies[0], obb.verticies[1], obb.verticies[2], 0, 0, 0, 0, 0, 0,
            obb.verticies[3], obb.verticies[4], obb.verticies[5], 0, 0, 0, 0, 0, 0,
            obb.verticies[6], obb.verticies[7], obb.verticies[8], 0, 0, 0, 0, 0, 0,
            obb.verticies[9], obb.verticies[10], obb.verticies[11], 0, 0, 0, 0, 0, 0,
            obb.verticies[12], obb.verticies[13], obb.verticies[14], 0, 0, 0, 0, 0, 0,
            obb.verticies[15], obb.verticies[16], obb.verticies[17], 0, 0, 0, 0, 0, 0,
            obb.verticies[18], obb.verticies[19], obb.verticies[20], 0, 0, 0, 0, 0, 0,
            obb.verticies[21], obb.verticies[22], obb.verticies[23], 0, 0, 0, 0, 0, 0
        };

        memcpy(gData->vertexData, data, sizeof(data));

        int32_t indexData[] =
        {
            7, 3, 2,
            7, 2, 4,
            5, 6, 0,
            5, 0, 1,
            3, 6, 0,
            3, 0, 2,
            7, 5, 1,
            7, 1, 4,
            4, 2, 0,
            4, 2, 1,
            7, 3, 5,
            6, 3, 5
        };

        memcpy(gData->indexData, indexData, sizeof(indexData));
    }
    else if (mesh->info.collisionShape == ISLANDER_COLLISION_SHAPE_BOX)
    {
        auto box = mesh->info.box;
        CalculateBox(box, mesh);

        gData->indexCount = 3 * 12;
        gData->vertexCount = 8;
        gData->strideSize = 9 * sizeof(float);
        gData->vertexData = new float[8 * 9];
        gData->indexData = new int32_t[gData->indexCount];

        float data[] =
        {
            box.maxx, box.maxy, box.maxz, 0, 0, 0, 0, 0, 0,
            box.maxx, box.miny, box.maxz, 0, 0, 0, 0, 0, 0,
            box.maxx, box.maxy, box.minz, 0, 0, 0, 0, 0, 0,
            box.minx, box.maxy, box.minz, 0, 0, 0, 0, 0, 0,
            box.maxx, box.miny, box.minz, 0, 0, 0, 0, 0, 0,
            box.minx, box.miny, box.maxz, 0, 0, 0, 0, 0, 0,
            box.minx, box.maxy, box.maxz, 0, 0, 0, 0, 0, 0,
            box.minx, box.miny, box.minz, 0, 0, 0, 0, 0, 0,
        };

        memcpy(gData->vertexData, data, sizeof(data));

        int32_t indexData[] =
        {
            7, 3, 2,
            7, 2, 4,
            5, 6, 0,
            5, 0, 1,
            3, 6, 0,
            3, 0, 2,
            7, 5, 1,
            7, 1, 4,
            4, 2, 0,
            4, 2, 1,
            7, 3, 5,
            6, 3, 5
        };

        memcpy(gData->indexData, indexData, sizeof(indexData));
    }
}

void Islander::Physics::CreateDebugGeometry(NonRealisticPhysicsSystem* system, IslanderCollisionGeometryData* gData)
{
    //std::vector<IslanderCollisionBox> boxes;

    //for (int i = 0; i < system->static_mesh.count; i++)
    //{
    //    auto& element = system->static_mesh.array[i];
    //    if (element.active)
    //    {
    //        for (int j = 0; j < element.childboxcount; j++)
    //        {
    //            auto& childbox = element.childbox[j];
    //            boxes.push_back(childbox);
    //        }
    //    }
    //}

    //gData->indexCount = boxes.size() * 3 * 12;
    //gData->vertexCount = boxes.size() * 8;
    //gData->strideSize = 9 * sizeof(float);
    //gData->vertexData = new float[boxes.size() * 8 * 9];
    //gData->indexData = new int32_t[gData->indexCount];

    //for (int i = 0; i < boxes.size(); i++)
    //{
    //    auto& box = boxes[i];

    //    float data[] =
    //    {
    //        box.maxx, box.maxy, box.maxz, 0, 0, 0, 0, 0, 0,
    //        box.maxx, box.miny, box.maxz, 0, 0, 0, 0, 0, 0,
    //        box.maxx, box.maxy, box.minz, 0, 0, 0, 0, 0, 0,
    //        box.minx, box.maxy, box.minz, 0, 0, 0, 0, 0, 0,
    //        box.maxx, box.miny, box.minz, 0, 0, 0, 0, 0, 0,
    //        box.minx, box.miny, box.maxz, 0, 0, 0, 0, 0, 0,
    //        box.minx, box.maxy, box.maxz, 0, 0, 0, 0, 0, 0,
    //        box.minx, box.miny, box.minz, 0, 0, 0, 0, 0, 0,
    //    };

    //    memcpy(gData->vertexData + i * (sizeof(data) / sizeof(float)), data, sizeof(data));

    //    const int offset = i * 8;
    //    int32_t indexData[] =
    //    {
    //        7 + offset, 3 + offset, 2 + offset,
    //        7 + offset, 2 + offset, 4 + offset,
    //        5 + offset, 6 + offset, 0 + offset,
    //        5 + offset, 0 + offset, 1 + offset,
    //        3 + offset, 6 + offset, 0 + offset,
    //        3 + offset, 0 + offset, 2 + offset,
    //        7 + offset, 5 + offset, 1 + offset,
    //        7 + offset, 1 + offset, 4 + offset,
    //        4 + offset, 2 + offset, 0 + offset,
    //        4 + offset, 2 + offset, 1 + offset,
    //        7 + offset, 3 + offset, 5 + offset,
    //        6 + offset, 3 + offset, 5 + offset
    //    };

    //    memcpy(gData->indexData + i * (sizeof(indexData) / sizeof(int32_t)), indexData, sizeof(indexData));
    //}

    std::vector<IslanderCollisionTriangle> triangles;
    std::vector<IslanderCollisionOBB> boxes;

    for (int i = 0; i < system->static_mesh.count; i++)
    {
        auto& element = system->static_mesh.array[i];
        if (element.active)
        {
            if (element.triangleCollider)
            {
                for (int j = 0; j < element.triangles.size(); j++)
                {
                    auto& childbox = element.triangles[j];
                    triangles.push_back(childbox);
                }
            }
            else
            {
                boxes.push_back(element.obb);
            }
        }
    }

    gData->indexCount = triangles.size() * 3 + boxes.size() * 3 * 12;
    gData->vertexCount = triangles.size() * 3 + boxes.size() * 8;
    gData->strideSize = 9 * sizeof(float);
    gData->vertexData = new float[triangles.size() * 3 * 9 + boxes.size() * 8 * 9];
    gData->indexData = new int32_t[gData->indexCount];

    for (int i = 0; i < triangles.size(); i++)
    {
        auto& tri = triangles[i];

        float data[] =
        {
            tri.vertices[0][0], tri.vertices[0][1], tri.vertices[0][2], 0, 0, 0, 0, 0, 0,
            tri.vertices[1][0], tri.vertices[1][1], tri.vertices[1][2], 0, 0, 0, 0, 0, 0,
            tri.vertices[2][0], tri.vertices[2][1], tri.vertices[2][2], 0, 0, 0, 0, 0, 0,
        };

        memcpy(gData->vertexData + i * (sizeof(data) / sizeof(float)), data, sizeof(data));

        const int offset = i * 3;
        int32_t indexData[] =
        {
            0 + offset, 1 + offset, 2 + offset,
        };

        memcpy(gData->indexData + i * (sizeof(indexData) / sizeof(int32_t)), indexData, sizeof(indexData));
    }

    for (int i = 0; i < boxes.size(); i++)
    {
        auto& obb = boxes[i];

        //float data[] =
        //{
        //    obb.centre[0] + obb.half_size[0], obb.centre[1] + obb.half_size[1], obb.centre[2] + obb.half_size[2], 0, 0, 0, 0, 0, 0,
        //    obb.centre[0] - obb.half_size[0], obb.centre[1] + obb.half_size[1], obb.centre[2] + obb.half_size[2], 0, 0, 0, 0, 0, 0,
        //    obb.centre[0] + obb.half_size[0], obb.centre[1] - obb.half_size[1], obb.centre[2] + obb.half_size[2], 0, 0, 0, 0, 0, 0,
        //    obb.centre[0] + obb.half_size[0], obb.centre[1] + obb.half_size[1], obb.centre[2] - obb.half_size[2], 0, 0, 0, 0, 0, 0,
        //    obb.centre[0] + obb.half_size[0], obb.centre[1] - obb.half_size[1], obb.centre[2] - obb.half_size[2], 0, 0, 0, 0, 0, 0,
        //    obb.centre[0] - obb.half_size[0], obb.centre[1] + obb.half_size[1], obb.centre[2] - obb.half_size[2], 0, 0, 0, 0, 0, 0,
        //    obb.centre[0] - obb.half_size[0], obb.centre[1] - obb.half_size[1], obb.centre[2] + obb.half_size[2], 0, 0, 0, 0, 0, 0,
        //    obb.centre[0] - obb.half_size[0], obb.centre[1] - obb.half_size[1], obb.centre[2] - obb.half_size[2], 0, 0, 0, 0, 0, 0
        //}
        float data[] =
        {
            obb.verticies[0], obb.verticies[1], obb.verticies[2], 0, 0, 0, 0, 0, 0,
            obb.verticies[3], obb.verticies[4], obb.verticies[5], 0, 0, 0, 0, 0, 0,
            obb.verticies[6], obb.verticies[7], obb.verticies[8], 0, 0, 0, 0, 0, 0,
            obb.verticies[9], obb.verticies[10], obb.verticies[11], 0, 0, 0, 0, 0, 0,
            obb.verticies[12], obb.verticies[13], obb.verticies[14], 0, 0, 0, 0, 0, 0,
            obb.verticies[15], obb.verticies[16], obb.verticies[17], 0, 0, 0, 0, 0, 0,
            obb.verticies[18], obb.verticies[19], obb.verticies[20], 0, 0, 0, 0, 0, 0,
            obb.verticies[21], obb.verticies[22], obb.verticies[23], 0, 0, 0, 0, 0, 0
        };

        memcpy(gData->vertexData + i * (sizeof(data) / sizeof(float)) + triangles.size() * 3 * gData->strideSize, data, sizeof(data));

        const int offset = i * 8;
        int32_t indexData[] =
        {
            7 + offset, 3 + offset, 2 + offset,
            7 + offset, 2 + offset, 4 + offset,
            5 + offset, 6 + offset, 0 + offset,
            5 + offset, 0 + offset, 1 + offset,
            3 + offset, 6 + offset, 0 + offset,
            3 + offset, 0 + offset, 2 + offset,
            7 + offset, 5 + offset, 1 + offset,
            7 + offset, 1 + offset, 4 + offset,
            4 + offset, 2 + offset, 0 + offset,
            4 + offset, 2 + offset, 1 + offset,
            7 + offset, 3 + offset, 5 + offset,
            6 + offset, 3 + offset, 5 + offset
        };

        memcpy(gData->indexData + i * (sizeof(indexData) / sizeof(int32_t)) + triangles.size() * 3, indexData, sizeof(indexData));
    }
}

void Islander::Physics::QueryPhysicsSystem(NonRealisticPhysicsSystem* system, IslanderCollisionQueryInfo* info)
{
    info->numDynamicColliders = system->dynamic_mesh.count - system->dynamic_mesh.freeslots.size();
    info->numStaticColliders = system->static_mesh.count - system->static_mesh.freeslots.size();
    info->maxDynamicColliders = PHYSICS_SYSTEM_MAX_DYNAMIC_ITEMS;
    info->maxStaticColliders = PHYSICS_SYSTEM_MAX_STATIC_ITEMS;

    if ((info->queryFlags & ISLANDER_COLLISION_QUERY_FLAGS_ITEMS) == ISLANDER_COLLISION_QUERY_FLAGS_ITEMS)
    {
        int idx = 0;
        for (int i = 0; i < info->maxStaticColliders; i++)
        {
            if (system->static_mesh.array[i].active)
            {
                auto& item = info->colliders[idx++];
                item.is_static = true;
                item.ptr = &system->static_mesh.array[i];
            }
        }

        for (int i = 0; i < info->maxDynamicColliders; i++)
        {
            if (system->dynamic_mesh.array[i].active)
            {
                auto& item = info->colliders[idx++];
                item.is_static = false;
                item.ptr = &system->dynamic_mesh.array[i];
            }
        }
    }
}
