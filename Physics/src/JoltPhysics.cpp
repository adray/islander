#include "JoltPhysics.h"

#include <string>

#include "Jolt.h"
#include "Physics\PhysicsSystem.h"
#include "Physics\Body\BodyCreationSettings.h"
#include "Physics\Collision\Shape\BoxShape.h"
#include "Physics\Collision\Shape\CapsuleShape.h"
#include "Physics\Collision\Shape\SphereShape.h"
#include "Physics\Collision\Shape\ConvexHullShape.h"
#include "Physics\Character\Character.h"
#include "Core\JobSystemThreadPool.h"
#include "Core\TempAllocator.h"
#include <RegisterTypes.h>
#include <ObjectStream/TypeDeclarations.h>
#include <Physics\Constraints\PointConstraint.h>

#define MAX_BODIES 256
#define MAX_BODY_PAIRS 65536
#define MAX_BODY_CONSTRAINTS 1024

using namespace Islander::Physics;

namespace JPH
{
    class JoltPhysicsMaterial : public JPH::PhysicsMaterial
    {
    public:
        JPH_DECLARE_SERIALIZABLE_VIRTUAL(JoltPhysicsMaterial)

        /// Constructor
        JoltPhysicsMaterial() = default;
        JoltPhysicsMaterial(const std::string& inName, JPH::ColorArg inColor) : mDebugName(inName), mDebugColor(inColor) { }
        JoltPhysicsMaterial(const std::string& inName, JPH::ColorArg inColor, int32_t materialId, int32_t materialType) : mDebugName(inName), mDebugColor(inColor), mMaterialId(materialId), mMaterialType(materialType) { }

        // Properties
        virtual const char* GetDebugName() const override { return mDebugName.c_str(); }
        virtual JPH::ColorArg                   GetDebugColor() const override { return mDebugColor; }

        // See: PhysicsMaterial::SaveBinaryState
        virtual void                            SaveBinaryState(JPH::StreamOut& inStream) const override;

        int32_t GetMaterialID() const { return mMaterialId; }
        int32_t GetMaterialType() const { return mMaterialType; }

    protected:
        // See: PhysicsMaterial::RestoreBinaryState
        virtual void                            RestoreBinaryState(JPH::StreamIn& inStream) override;

    private:
        std::string                                 mDebugName;                         ///< Name of the material, used for debugging purposes
        JPH::Color                                  mDebugColor = JPH::Color::sGrey;    ///< Color of the material, used to render the shapes
        int32_t                                     mMaterialId = 0;
        int32_t                                     mMaterialType = 0;
    };

    JPH_IMPLEMENT_SERIALIZABLE_VIRTUAL(JoltPhysicsMaterial)
    {
        JPH_ADD_BASE_CLASS(JoltPhysicsMaterial, PhysicsMaterial)

        JPH_ADD_ATTRIBUTE(JoltPhysicsMaterial, mDebugName)
        JPH_ADD_ATTRIBUTE(JoltPhysicsMaterial, mDebugColor)
        JPH_ADD_ATTRIBUTE(JoltPhysicsMaterial, mMaterialId)
        JPH_ADD_ATTRIBUTE(JoltPhysicsMaterial, mMaterialType)
    }
}

void JPH::JoltPhysicsMaterial::SaveBinaryState(JPH::StreamOut& inStream) const
{

}

void JPH::JoltPhysicsMaterial::RestoreBinaryState(JPH::StreamIn& inStream)
{

}

struct JoltPhysics
{
    JPH::PhysicsSystem ps;
    JPH::ObjectToBroadPhaseLayer obpl;
    JPH::PhysicsSettings settings;
    JPH::JobSystem* job;
    JPH::TempAllocator* allocator;

    JPH::JoltPhysicsMaterial* defaultMaterial;
    JPH::JoltPhysicsMaterial* platformMaterial;
};

namespace BroadPhaseLayers
{
    static constexpr JPH::BroadPhaseLayer UNUSED(0);
    static constexpr JPH::BroadPhaseLayer NON_MOVING(1);
    static constexpr JPH::BroadPhaseLayer MOVING(2);
}

namespace Layers
{
    static constexpr JPH::ObjectLayer UNUSED0       = 0;
    static constexpr JPH::ObjectLayer UNUSED1       = 1;
    static constexpr JPH::ObjectLayer UNUSED2       = 2;
    static constexpr JPH::ObjectLayer NON_MOVING    = 3;
    static constexpr JPH::ObjectLayer MOVING        = 4;
}

bool Filter(JPH::ObjectLayer objectLayer, JPH::BroadPhaseLayer bpl)
{
    return true;
}

bool PairFilter(JPH::ObjectLayer objectLayer1, JPH::ObjectLayer objectLayer2)
{
    return true;
}

void* Islander::Physics::CreateJoltPhysics()
{
    // Register all Jolt physics types
    JPH::RegisterTypes();

    JoltPhysics* jps = new JoltPhysics();
    jps->obpl.push_back(JPH::BroadPhaseLayer(BroadPhaseLayers::UNUSED));
    jps->obpl.push_back(JPH::BroadPhaseLayer(BroadPhaseLayers::UNUSED));
    jps->obpl.push_back(JPH::BroadPhaseLayer(BroadPhaseLayers::UNUSED));
    jps->obpl.push_back(JPH::BroadPhaseLayer(BroadPhaseLayers::NON_MOVING));
    jps->obpl.push_back(JPH::BroadPhaseLayer(BroadPhaseLayers::MOVING));

    jps->job = new JPH::JobSystemThreadPool(2048, 8);
    jps->allocator = new JPH::TempAllocatorImpl(16 * 1024 * 1024);

    jps->defaultMaterial = new JPH::JoltPhysicsMaterial("Default", JPH::ColorArg(0, 0, 0), 0, 0);
    jps->platformMaterial = new JPH::JoltPhysicsMaterial("Platform", JPH::ColorArg(0, 0, 0), 0, 1);
    JPH::PhysicsMaterial::sDefault = jps->defaultMaterial;

    jps->ps.Init(MAX_BODIES, 0, MAX_BODY_PAIRS, MAX_BODY_CONSTRAINTS, jps->obpl, Filter, PairFilter);
    jps->ps.SetPhysicsSettings(jps->settings);
    return jps;
}

void Islander::Physics::DestroyJoltPhysics(void* jolt)
{
    auto jps = reinterpret_cast<JoltPhysics*>(jolt);
    delete jps;
}

void Islander::Physics::AddJoltPhysicsBody(void* jolt, IslanderPhysicsBody& body)
{
    auto jps = reinterpret_cast<JoltPhysics*>(jolt);

    auto& i = jps->ps.GetBodyInterface();

    JPH::Quat rot = JPH::Quat::sEulerAngles(JPH::Vec3(body.rot[0], body.rot[1], body.rot[2]));

    JPH::ShapeSettings::ShapeResult shapeResult;
    if (body.shape == ISLANDER_COLLISION_SHAPE_CAPSULE)
    {
        JPH::CapsuleShapeSettings capsuleSettings(body.capsule.height / 2.0f, body.capsule.radius);
        shapeResult = capsuleSettings.Create();
    }
    else if (body.shape == ISLANDER_COLLISION_SHAPE_SPHERE)
    {
        JPH::SphereShapeSettings sphereSettings(body.sphere.radius);
        shapeResult = sphereSettings.Create();
    }
    else if (body.shape == ISLANDER_COLLISION_SHAPE_ORIENTATED_BOX)
    {
        JPH::Vec3 verts[8];
        for (int i = 0; i < 24; i += 3)
        {
            verts[i / 3] = JPH::Vec3(body.obb.verticies[i], body.obb.verticies[i + 1], body.obb.verticies[i + 2]);
        }

        JPH::ConvexHullShapeSettings convexSettings(verts, 8);
        shapeResult = convexSettings.Create();
    }
    else if (body.shape == ISLANDER_COLLISION_SHAPE_BOX)
    {
        float halfx = (body.box.maxx - body.box.minx) / 2.0f;
        float halfy = (body.box.maxy - body.box.miny) / 2.0f;
        float halfz = (body.box.maxz - body.box.minz) / 2.0f;

        if (body.body_type == ISLANDER_PHYSICS_BODY_TYPE_KINEMATIC)
        {
            JPH::BoxShapeSettings boxSettings(JPH::Vec3Arg(halfx * body.scale[0], halfy * body.scale[1], halfz * body.scale[2]), 0.05f, jps->platformMaterial);
            shapeResult = boxSettings.Create();
        }
        else
        {
            JPH::BoxShapeSettings boxSettings(JPH::Vec3Arg(halfx * body.scale[0], halfy * body.scale[1], halfz * body.scale[2]));
            shapeResult = boxSettings.Create();
        }
    }

    if (body.is_character)
    {
        JPH::Ref<JPH::CharacterSettings> settings = new JPH::CharacterSettings();
        settings->mFriction = 0.5f;
        settings->mMass = body.mass;
        settings->mShape = shapeResult.Get();
        //settings->mMaxSlopeAngle 
        settings->mLayer = Layers::MOVING;

        JPH::Character* character = new JPH::Character(settings, JPH::Vec3(body.pos[0], body.pos[1], body.pos[2]), rot, nullptr, &jps->ps);
        character->AddToPhysicsSystem(JPH::EActivation::Activate);
        body.id = character->GetBodyID().GetIndexAndSequenceNumber();
        body.character = character;
    }
    else
    {
        JPH::EMotionType type = JPH::EMotionType::Static;
        switch (body.body_type)
        {
        case ISLANDER_PHYSICS_BODY_TYPE_DYNAMIC:
            type = JPH::EMotionType::Dynamic;
            break;
        case ISLANDER_PHYSICS_BODY_TYPE_STATIC:
            type = JPH::EMotionType::Static;
            break;
        case ISLANDER_PHYSICS_BODY_TYPE_KINEMATIC:
            type = JPH::EMotionType::Kinematic;
            break;
        }

        JPH::Body* b = i.CreateBody(JPH::BodyCreationSettings(shapeResult.Get(), JPH::Vec3(body.pos[0], body.pos[1], body.pos[2]), rot, type, Layers::MOVING));
        i.AddBody(b->GetID(), JPH::EActivation::Activate);
        body.id = b->GetID().GetIndexAndSequenceNumber();
    }
}

void Islander::Physics::RemoveJoltPhysicsBody(void* jolt, uint32_t id)
{
    auto jps = reinterpret_cast<JoltPhysics*>(jolt);

    auto& i = jps->ps.GetBodyInterface();

    i.RemoveBody(JPH::BodyID(id));
}

void Islander::Physics::RunJoltPhysics(void* jolt, float dt)
{
    auto jps = reinterpret_cast<JoltPhysics*>(jolt);

    jps->ps.Update(dt, 1, 1, jps->allocator, jps->job);
}

void Islander::Physics::RetrieveJoltPhysicsBody(void* jolt, IslanderPhysicsBody& body)
{
    auto jps = reinterpret_cast<JoltPhysics*>(jolt);

    auto& i = jps->ps.GetBodyInterface();

    JPH::Vec3 pos;
    JPH::Quat rot;
    i.GetPositionAndRotation(JPH::BodyID(body.id), pos, rot);

    body.pos[0] = pos[0];
    body.pos[1] = pos[1];
    body.pos[2] = pos[2];

    JPH::Vec3 euler = rot.GetEulerAngles();
    body.rot[0] = euler[0];
    body.rot[1] = euler[1];
    body.rot[2] = euler[2];

    auto& velocity = i.GetLinearVelocity(JPH::BodyID(body.id));
    body.velocity[0] = velocity[0];
    body.velocity[1] = velocity[1];
    body.velocity[2] = velocity[2];

    if (body.is_character)
    {
        JPH::Character* character = reinterpret_cast<JPH::Character*>(body.character);
        if (character != nullptr)
        {
            character->PostSimulation(0.05f);

            auto material = reinterpret_cast<const JPH::JoltPhysicsMaterial*>(character->GetGroundMaterial());
            int32_t matType = material->GetMaterialType();
            if (matType == 1)
            {
                auto& groundId = character->GetGroundBodyID();
                auto& groundVelocity = i.GetLinearVelocity(groundId);

                // platform
                if (groundVelocity[1] > 0.0f)
                {
                    body.velocity[1] = -groundVelocity[1];
                }
                else
                {
                    body.velocity[1] = groundVelocity[1];
                }
            }
        }
    }
}

void Islander::Physics::ApplyJoltPhysicsVelocity(void* jolt, IslanderPhysicsBody& body)
{
    auto jps = reinterpret_cast<JoltPhysics*>(jolt);

    JPH::Vec3 velocity(body.velocity[0], body.velocity[1], body.velocity[2]);

    JPH::Character* character = reinterpret_cast<JPH::Character*>(body.character);
    if (character != nullptr)
    {

        character->SetLinearVelocity(velocity);
    }
    else
    {
        auto& i = jps->ps.GetBodyInterface();

        i.SetLinearVelocity(JPH::BodyID(body.id), velocity);
    }
}
