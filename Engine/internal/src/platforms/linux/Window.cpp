#include "Window.h"
#include <Logger.h>
#include <sstream>
#include <string>
#include "platforms/linux/Linux.h"
#include "X11/Xatom.h"
#include "X11/XKBlib.h"

using namespace Islander;


namespace Islander
{
	class InternalWindow : public RenderState
	{
	public:
		uint32_t width;
		uint32_t height;

		Input _input;

		Atom _deleteMsg;
	};

	Islander::Input::Input() :
			control(false),
			shift(false),
			alt(false),
			rbuttondown(MOUSE_INPUT_NONE),
			lbuttondown(MOUSE_INPUT_NONE),
			mouseWheelDelta(0),
			mouseWheel(0)
			{
			}

}

static GLint   att[] =
{
	GLX_X_RENDERABLE, True,
	GLX_DRAWABLE_TYPE, GLX_WINDOW_BIT,
	GLX_RENDER_TYPE, GLX_RGBA_BIT,
	GLX_X_VISUAL_TYPE, GLX_TRUE_COLOR,
	GLX_RED_SIZE, 8,
	GLX_GREEN_SIZE, 8,
	GLX_BLUE_SIZE, 8,
	GLX_ALPHA_SIZE, 8,
	GLX_DEPTH_SIZE, 24,
	GLX_STENCIL_SIZE, 8,
	GLX_DOUBLEBUFFER, True,
	None
};

WINDOW::WINDOW(void* parent)
{
	auto window = _impl = new InternalWindow();

        window->display = XOpenDisplay(nullptr);

	if (window->display == nullptr)
	{
		Logger::GetLogger()->Log("XOpenDisplay failed",
			LOGGING_INFO);
		return;
	}

	XSetCloseDownMode(window->display, DestroyAll);
	
	window->root = DefaultRootWindow(window->display);

	int temp[5] = { GLX_RGBA, GLX_DEPTH_SIZE, 24, GLX_DOUBLEBUFFER, None };
	window->vi = glXChooseVisual(window->display,
				     0,
				     temp);

	window->context = glXCreateContext(window->display, window->vi,
					   nullptr, GL_TRUE);

	if (window->context == nullptr)
	{
		Logger::GetLogger()->Log("Failed to create dummy context",
					 LOGGING_INFO);
		return;
	}

	glXMakeCurrent(window->display, 0, window->context);

	glewExperimental = true;
	
	if (glewInit() != GLEW_OK || glxewInit() != GLEW_OK)
	{
	        Logger::GetLogger()->Log("Failed to init GLEW", LOGGING_INFO);
		return;
	}
	
	int configCount = 0;
	auto configs = glXChooseFBConfig(window->display,
					 DefaultScreen(window->display),
					 att,
					 &configCount);
	
	if (configCount == 0)
	{
		Logger::GetLogger()->Log("glXChooseFBConfig failed",
			LOGGING_INFO);
		return;
	}

	window->vi = glXGetVisualFromFBConfig(window->display, configs[0]);
	
	std::stringstream ss;
	ss << "Visual selected " << (void*)window->vi->visualid;
	Logger::GetLogger()->Log(ss.str(), LOGGING_INFO);
	ss.clear();

	window->cmap = XCreateColormap(window->display, window->root,
				       window->vi->visual,
				       AllocNone);
	window->swa.colormap = window->cmap;
	window->swa.event_mask = ExposureMask | KeyPressMask |
		KeyReleaseMask | ButtonPressMask |
		ButtonReleaseMask | PointerMotionMask;

	window->width = 1366;
	window->height = 768;
	
	window->win = XCreateWindow(window->display,
				    window->root,
				    0, 0, window->width, window->height,
				    0,
				    window->vi->depth,
				    InputOutput,
				    window->vi->visual,
				    CWColormap | CWEventMask,
				    &window->swa);
	XMapWindow(window->display, window->win);
	XStoreName(window->display, window->win, "Game");
	
	glXDestroyContext(window->display, window->context);
	
	int context_attribs[] =
	{
		GLX_CONTEXT_MAJOR_VERSION_ARB, 4,
		GLX_CONTEXT_MINOR_VERSION_ARB, 0,
		None
	};
	
	window->context = glXCreateContextAttribsARB(window->display,
						     configs[0],
						     0,
						     True,
		                                     context_attribs);
	
	glXMakeCurrent(window->display, window->win, window->context);

	window->_deleteMsg = XInternAtom(window->display,
					 "WM_DELETE_WINDOW", False);
	if (XSetWMProtocols(window->display, window->win,
			    &window->_deleteMsg, 1) == 0)
	{
		Logger::GetLogger()->Log("Unable to register XSetWMProtocols",
					 LOGGING_INFO);
	}

	// Send the non-exclusive fullscreen event
	Atom fullscreen = XInternAtom(window->display, "_NET_WM_STATE_FULLSCREEN", False);
	Atom state = XInternAtom(window->display, "_NET_WM_STATE", False);

	XEvent evf;
	evf.type = ClientMessage;
	evf.xclient.window = window->win;
	evf.xclient.message_type = state;
	evf.xclient.format = 32;
	evf.xclient.data.l[0] = 2; // 0 = remove, 1 = add, 2 = toggle
	evf.xclient.data.l[1] = fullscreen;
	evf.xclient.data.l[2] = 0;
	evf.xclient.data.l[3] = 1; // source (normal application)
	evf.xclient.data.l[4] = 0;

	XSendEvent(window->display, window->root, 0,
		   SubstructureRedirectMask |
		   SubstructureNotifyMask, &evf);
	
	XFree(configs);

	_impl->_input.keymap[XK_a] = KEY_A;
	_impl->_input.keymap[XK_b] = KEY_B;
	_impl->_input.keymap[XK_c] = KEY_C;
	_impl->_input.keymap[XK_d] = KEY_D;
	_impl->_input.keymap[XK_e] = KEY_E;
	_impl->_input.keymap[XK_f] = KEY_F;
	_impl->_input.keymap[XK_g] = KEY_G;
	_impl->_input.keymap[XK_h] = KEY_H;
	_impl->_input.keymap[XK_i] = KEY_I;
	_impl->_input.keymap[XK_j] = KEY_J;
	_impl->_input.keymap[XK_k] = KEY_K;
	_impl->_input.keymap[XK_l] = KEY_L;
	_impl->_input.keymap[XK_m] = KEY_M;
	_impl->_input.keymap[XK_n] = KEY_N;
	_impl->_input.keymap[XK_o] = KEY_O;
	_impl->_input.keymap[XK_p] = KEY_P;
	_impl->_input.keymap[XK_q] = KEY_Q;
	_impl->_input.keymap[XK_r] = KEY_R;
	_impl->_input.keymap[XK_s] = KEY_S;
	_impl->_input.keymap[XK_t] = KEY_T;
	_impl->_input.keymap[XK_u] = KEY_U;
	_impl->_input.keymap[XK_v] = KEY_V;
	_impl->_input.keymap[XK_w] = KEY_W;
	_impl->_input.keymap[XK_x] = KEY_X;
	_impl->_input.keymap[XK_y] = KEY_Y;
	_impl->_input.keymap[XK_z] = KEY_Z;

	_impl->_input.keymap[XK_0] = KEY_0;
	_impl->_input.keymap[XK_1] = KEY_1;
	_impl->_input.keymap[XK_2] = KEY_2;
	_impl->_input.keymap[XK_3] = KEY_3;
	_impl->_input.keymap[XK_4] = KEY_4;
	_impl->_input.keymap[XK_5] = KEY_5;
	_impl->_input.keymap[XK_6] = KEY_6;
	_impl->_input.keymap[XK_7] = KEY_7;
	_impl->_input.keymap[XK_8] = KEY_8;
	_impl->_input.keymap[XK_9] = KEY_9;
	
	_impl->_input.keymap[XK_Escape] = KEY_ESCAPE;
	_impl->_input.keymap[XK_Shift_L] = KEY_SHIFT;
	_impl->_input.keymap[XK_Shift_R] = KEY_SHIFT;
	_impl->_input.keymap[XK_Alt_L] = KEY_ALT;
	_impl->_input.keymap[XK_Alt_R] = KEY_ALT;
	_impl->_input.keymap[XK_Return] = KEY_ENTER;
	_impl->_input.keymap[XK_BackSpace] = KEY_BACKSPACE;
	_impl->_input.keymap[XK_Control_L] = KEY_CTRL;
	_impl->_input.keymap[XK_Control_R] = KEY_CTRL;
	_impl->_input.keymap[XK_space] = KEY_SPACE;
    _impl->_input.keymap[XK_Delete] = KEY_DELETE;
    _impl->_input.keymap[XK_Tab] = KEY_TAB;
    _impl->_input.keymap[XK_Home] = KEY_HOME;
    _impl->_input.keymap[XK_Prior] = KEY_PAGE_UP;
    _impl->_input.keymap[XK_Next] = KEY_PAGE_DOWN;

	_impl->_input.keymap[XK_Left] = KEY_LEFT;
	_impl->_input.keymap[XK_Right] = KEY_RIGHT;
	_impl->_input.keymap[XK_Up] = KEY_UP;
	_impl->_input.keymap[XK_Down] = KEY_DOWN;

	_impl->_input.keymap[XK_F1] = KEY_F1;
	_impl->_input.keymap[XK_F2] = KEY_F2;
	_impl->_input.keymap[XK_F3] = KEY_F3;
	_impl->_input.keymap[XK_F4] = KEY_F4;
	_impl->_input.keymap[XK_F5] = KEY_F5;
	_impl->_input.keymap[XK_F6] = KEY_F6;
	_impl->_input.keymap[XK_F7] = KEY_F7;
	_impl->_input.keymap[XK_F8] = KEY_F8;
	_impl->_input.keymap[XK_F9] = KEY_F9;
	_impl->_input.keymap[XK_F10] = KEY_F10;
	_impl->_input.keymap[XK_F11] = KEY_F11;
	_impl->_input.keymap[XK_F12] = KEY_F12;
}

void WINDOW::GetDisplays(Displays* displays)
{
    displays->_count = 0;
}

void WINDOW::SelectDisplay(int id)
{
}

int WINDOW::Process()
{
	uint32_t val=0;
	return Process(val);
}

int WINDOW::Process(uint32_t& msg)
{
	while (XPending(_impl->display))
	{
		XEvent ev;
		XNextEvent(_impl->display, &ev);

		switch (ev.type)
		{
		case ClientMessage:
			if (ev.xclient.data.l[0] == _impl->_deleteMsg)
			{
				return 0;
			}
			break;
	        case MotionNotify:
			_impl->_input.mousepos.SetX(ev.xmotion.x);
			_impl->_input.mousepos.SetY(ev.xmotion.y);
			break;
		case ButtonPress:
			if (ev.xbutton.button == Button1)
			{
				_impl->_input.lbuttondown = MOUSE_INPUT_DOWN;
			}
			else if (ev.xbutton.button == Button3)
			{
				_impl->_input.rbuttondown = MOUSE_INPUT_DOWN;
			}			
			break;
		case ButtonRelease:
			if (ev.xbutton.button == Button1)
			{
				_impl->_input.lbuttondown = MOUSE_INPUT_UP;
			}
			else if (ev.xbutton.button == Button3)
			{
				_impl->_input.rbuttondown = MOUSE_INPUT_UP;
			}
			break;
		case KeyPress:
		{
			auto key = XkbKeycodeToKeysym(_impl->display,
						      ev.xkey.keycode,
						      0, 0);
			auto val = _impl->_input.keymap.find(key);
			if (val != _impl->_input.keymap.end())
			{
				_impl->_input.keysdown[val->second] = true;
			}
		}
			break;
		case KeyRelease:
		{
			auto key = XkbKeycodeToKeysym(_impl->display,
						      ev.xkey.keycode,
						      0, 0);
			auto val = _impl->_input.keymap.find(key);
			if (val != _impl->_input.keymap.end())
			{
				_impl->_input.keysdown[val->second] = false;
			}
		}
			break;
		}
	}
	
	return 1;
}

Input* WINDOW::GetInput()
{
	return &_impl->_input;
}

void WINDOW::Destroy()
{
}

void WINDOW::SetSize(int32_t width, int32_t height)
{
	_impl->width = width;
	_impl->height = height;
	XResizeWindow(_impl->display, _impl->win, width, height);
}

int32_t WINDOW::Width() const
{
	return _impl->width;
}

int32_t WINDOW::Height() const
{
	return _impl->height;
}

void WINDOW::SetTitle(const std::string& title)
{
}

void* WINDOW::Handle() const
{
	return _impl;
}

void WINDOW::SetFullscreen(bool fullscreen)
{
}

void Islander::WINDOW::SetWindowIcon(char* data, int32_t width, int32_t height)
{
}

