#include "OpenGL.h"
#include "Window.h"
#include "platforms\windows\stdafx.h"
#include <gl\glew.h>
#include <gl\GL.h>

#pragma comment(lib, "opengl32.lib")

using namespace Islander;
using namespace Islander::Renderer;

namespace Islander
{
	namespace Renderer
	{
		class InternalOpenGLContext
		{
		public:
			
			HGLRC _context;

		};
	}
}

OpenGLContext::OpenGLContext()
{
	_impl = new InternalOpenGLContext();
}

int OpenGLContext::InitializeGL(WINDOW* window)
{
	// create dummy window to initialize the context properly

	HWND hwnd = static_cast<HWND>(window->Handle());

	HDC dc = GetDC(hwnd);

	PIXELFORMATDESCRIPTOR pfd =
	{
		sizeof(PIXELFORMATDESCRIPTOR),
		1,
		PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER,    //Flags
		PFD_TYPE_RGBA,            //The kind of framebuffer. RGBA or palette.
		32,                        //Colordepth of the framebuffer.
		0, 0, 0, 0, 0, 0,
		0,
		0,
		0,
		0, 0, 0, 0,
		24,                        //Number of bits for the depthbuffer
		8,                        //Number of bits for the stencilbuffer
		0,                        //Number of Aux buffers in the framebuffer.
		PFD_MAIN_PLANE,
		0,
		0, 0, 0
	};

	int format = ChoosePixelFormat(dc, &pfd);
	if (!SetPixelFormat(dc, format, &pfd))
		return -1;

	_impl->_context = wglCreateContext(dc);
	
	GLenum res;

	if (_impl->_context == nullptr)
	{
		res = glGetError();
	}

	wglMakeCurrent(dc, _impl->_context);

	glewExperimental = GL_TRUE;
	res = glewInit();

	if (res != GLEW_OK)
		return -1;	
}

void OpenGLContext::DeactivateGL(WINDOW* window)
{
	HWND hwnd = static_cast<HWND>(window->Handle());

	HDC dc = GetDC(hwnd);

	wglMakeCurrent(dc, _impl->_context);

	wglDeleteContext(_impl->_context);
}

void OpenGLContext::Present(WINDOW* window)
{
	HWND hwnd = static_cast<HWND>(window->Handle());

	auto dc = GetDC(hwnd);
	wglSwapLayerBuffers(dc, WGL_SWAP_MAIN_PLANE);

	GLenum res = glGetError();
}
