#include "platforms\windows\stdafx.h"
#include "Window.h"
#include "Logger.h"
#include "backends/imgui_impl_win32.h"
#include <functional>

using namespace Islander;

Input::Input() : rbuttondown(MOUSE_INPUT_NONE), lbuttondown(MOUSE_INPUT_NONE), mouseWheelDelta(0), mouseWheel(0)
{
    ZeroMemory(&keysdown, sizeof(keysdown));
}

class Islander::InternalWindow
{
public:
    HWND _hwnd;
    int32_t _width;
    int32_t _height;
    std::wstring _title;

    Input _input;
    bool _fullscreen;
    wchar_t _name[20];
    DWORD _flags;

    Displays _displays;
        
    ATOM MyRegisterClass(HINSTANCE hInstance);

    BOOL InitInstance(HINSTANCE hInstance, int nCmdShow, HWND * hWnd, HWND * parent);

    static LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);

    InternalWindow(HWND* parent) : _width(800), _height(600), _fullscreen(false), _flags(0), _hwnd(nullptr)
    {
        SetTitle(L"Game");

        HINSTANCE hInstance = GetModuleHandle(0);

        MyRegisterClass(hInstance);

        int cmd = SW_SHOWDEFAULT;

        if (!InitInstance(hInstance, cmd, &_hwnd, parent))
        {
            throw std::exception("Failed to create window");
        }
    }

    void SetTitle(const std::string& title)
    {
        SetWindowTextA(_hwnd, title.c_str()); // ???
    }
    
    void SetTitle(const std::wstring& title)
    {
        _title = title;
        auto str = _title.c_str();
        int characters = std::min(20, (int)title.length());
        memcpy_s(_name, 20 * sizeof(wchar_t), str, sizeof(wchar_t)*characters);
        _name[characters] = '\0';

        if (_hwnd)
        {
            SetWindowText(_hwnd, _name);
        }
    }

    void FindDisplays()
    {
        _displays._count = 0;

        EnumDisplayMonitors(nullptr, nullptr, (MONITORENUMPROC) &InternalWindow::EnumDisplayMonitorsCallback, (LPARAM)this);
    }

    static BOOL EnumDisplayMonitorsCallback(
        HMONITOR monitor,
        HDC hdc,
        LPRECT rect,
        LPARAM ptr)
    {
        MONITORINFOEX info;
        info.cbSize = sizeof(MONITORINFOEX);
        if (GetMonitorInfo(monitor, &info))
        {
            auto window = (InternalWindow*)ptr;

            auto display = &window->_displays._displays[window->_displays._count];

            display->_id = window->_displays._count;

            DISPLAY_DEVICE device;
            device.cb = sizeof(DISPLAY_DEVICE);
            if (EnumDisplayDevices(info.szDevice, 0, &device, 0))
            {
                wcstombs_s(nullptr, display->_name, device.DeviceString, sizeof(device.DeviceString) / sizeof(WCHAR));
            }
            else
            {
                wcstombs_s(nullptr, display->_name, info.szDevice, sizeof(info.szDevice) / sizeof(WCHAR));
            }

            display->_pos.SetX(info.rcMonitor.left);
            display->_pos.SetY(info.rcMonitor.top);

            display->_size.SetX(info.rcMonitor.right - info.rcMonitor.left);
            display->_size.SetY(info.rcMonitor.bottom - info.rcMonitor.top);

            window->_displays._count++;
        }

        return true;
    }
};

WINDOW::WINDOW(void * parent=nullptr)
{
    HWND* hwnd = static_cast<HWND*>(parent);
    _impl = new InternalWindow(hwnd);
    _impl->FindDisplays();

    _impl->_input.keymap['A'] = KEY_A;
    _impl->_input.keymap['B'] = KEY_B;
    _impl->_input.keymap['C'] = KEY_C;
    _impl->_input.keymap['D'] = KEY_D;
    _impl->_input.keymap['E'] = KEY_E;
    _impl->_input.keymap['F'] = KEY_F;
    _impl->_input.keymap['G'] = KEY_G;
    _impl->_input.keymap['H'] = KEY_H;
    _impl->_input.keymap['I'] = KEY_I;
    _impl->_input.keymap['J'] = KEY_J;
    _impl->_input.keymap['K'] = KEY_K;
    _impl->_input.keymap['L'] = KEY_L;
    _impl->_input.keymap['M'] = KEY_M;
    _impl->_input.keymap['N'] = KEY_N;
    _impl->_input.keymap['O'] = KEY_O;
    _impl->_input.keymap['P'] = KEY_P;
    _impl->_input.keymap['Q'] = KEY_Q;
    _impl->_input.keymap['R'] = KEY_R;
    _impl->_input.keymap['S'] = KEY_S;
    _impl->_input.keymap['T'] = KEY_T;
    _impl->_input.keymap['U'] = KEY_U;
    _impl->_input.keymap['V'] = KEY_V;
    _impl->_input.keymap['W'] = KEY_W;
    _impl->_input.keymap['X'] = KEY_X;
    _impl->_input.keymap['Y'] = KEY_Y;
    _impl->_input.keymap['Z'] = KEY_Z;

    _impl->_input.keymap['0'] = KEY_0;
    _impl->_input.keymap['1'] = KEY_1;
    _impl->_input.keymap['2'] = KEY_2;
    _impl->_input.keymap['3'] = KEY_3;
    _impl->_input.keymap['4'] = KEY_4;
    _impl->_input.keymap['5'] = KEY_5;
    _impl->_input.keymap['6'] = KEY_6;
    _impl->_input.keymap['7'] = KEY_7;
    _impl->_input.keymap['8'] = KEY_8;
    _impl->_input.keymap['9'] = KEY_9;
    
    _impl->_input.keymap[VK_NUMPAD0] = KEY_0;
    _impl->_input.keymap[VK_NUMPAD1] = KEY_1;
    _impl->_input.keymap[VK_NUMPAD2] = KEY_2;
    _impl->_input.keymap[VK_NUMPAD3] = KEY_3;
    _impl->_input.keymap[VK_NUMPAD4] = KEY_4;
    _impl->_input.keymap[VK_NUMPAD5] = KEY_5;
    _impl->_input.keymap[VK_NUMPAD6] = KEY_6;
    _impl->_input.keymap[VK_NUMPAD7] = KEY_7;
    _impl->_input.keymap[VK_NUMPAD8] = KEY_8;
    _impl->_input.keymap[VK_NUMPAD9] = KEY_9;
    
    _impl->_input.keymap[VK_ESCAPE] = KEY_ESCAPE;
    _impl->_input.keymap[VK_SHIFT] = KEY_SHIFT;
    _impl->_input.keymap[VK_MENU] = KEY_ALT;
    _impl->_input.keymap[VK_RETURN] = KEY_ENTER;
    _impl->_input.keymap[VK_BACK] = KEY_BACKSPACE;
    _impl->_input.keymap[VK_CONTROL] = KEY_CTRL;
    _impl->_input.keymap[VK_SPACE] = KEY_SPACE;
    _impl->_input.keymap[VK_DELETE] = KEY_DELETE;
    _impl->_input.keymap[VK_TAB] = KEY_TAB;
    _impl->_input.keymap[VK_HOME] = KEY_HOME;
    _impl->_input.keymap[VK_NAVIGATION_UP] = KEY_PAGE_UP;
    _impl->_input.keymap[VK_NAVIGATION_DOWN] = KEY_PAGE_DOWN;

    _impl->_input.keymap[VK_LEFT] = KEY_LEFT;
    _impl->_input.keymap[VK_RIGHT] = KEY_RIGHT;
    _impl->_input.keymap[VK_UP] = KEY_UP;
    _impl->_input.keymap[VK_DOWN] = KEY_DOWN;

    _impl->_input.keymap[VK_F1] = KEY_F1;
    _impl->_input.keymap[VK_F2] = KEY_F2;
    _impl->_input.keymap[VK_F3] = KEY_F3;
    _impl->_input.keymap[VK_F4] = KEY_F4;
    _impl->_input.keymap[VK_F5] = KEY_F5;
    _impl->_input.keymap[VK_F6] = KEY_F6;
    _impl->_input.keymap[VK_F7] = KEY_F7;
    _impl->_input.keymap[VK_F8] = KEY_F8;
    _impl->_input.keymap[VK_F9] = KEY_F9;
    _impl->_input.keymap[VK_F10] = KEY_F10;
    _impl->_input.keymap[VK_F11] = KEY_F11;
    _impl->_input.keymap[VK_F12] = KEY_F12;
}

void WINDOW::GetDisplays(Displays* displays)
{
    *displays = _impl->_displays;
}

void WINDOW::SelectDisplay(int id)
{
    if (id < _impl->_displays._count)
    {
        auto display = &_impl->_displays._displays[id];

        int posX = display->_pos.GetX() + (display->_size.GetX() - _impl->_width) / 2;
        int posY = display->_pos.GetY() + (display->_size.GetY() - _impl->_height) / 2;

        SetWindowPos(_impl->_hwnd, HWND_TOP, posX, posY, _impl->_width, _impl->_height, 0);
    }
}

int32_t WINDOW::Width() const
{
    return _impl->_width;
}

int32_t WINDOW::Height() const
{
    return _impl->_height;
}

void WINDOW::SetMousePos(int x, int y)
{
    POINT p;
    p.x = x;
    p.y = y;
    ClientToScreen(_impl->_hwnd, &p);
    SetCursorPos(p.x, p.y);
}

void WINDOW::Destroy()
{
    DestroyWindow(_impl->_hwnd);
    delete _impl;
    _impl = nullptr;
}

void WINDOW::SetTitle(const std::string& title)
{
    _impl->SetTitle(title);
}

void* WINDOW::Handle() const
{
    return _impl->_hwnd;
}

void WINDOW::SetSize(int32_t width, int32_t height)
{
    _impl->_width = width;
    _impl->_height = height;

    int posX = (GetSystemMetrics(SM_CXSCREEN) - _impl->_width) / 2;
    int posY = (GetSystemMetrics(SM_CYSCREEN) - _impl->_height) / 2;
    
    int errorCode = 0;
        
    if (!SetWindowPos(_impl->_hwnd, HWND_TOP, posX, posY, _impl->_width, _impl->_height, 0))
    {
        errorCode = GetLastError();
    }
}

void WINDOW::SetFullscreen(bool fullscreen)
{
    _impl->_fullscreen = fullscreen;
}

Input* WINDOW::GetInput()
{
    return &_impl->_input;
}

int WINDOW::Process()
{
    uint32_t message;
    return Process(message);
}

int WINDOW::Process(uint32_t& message)
{
    _impl->_input.mouseWheel = 0;

    MSG msg;
    bool result=1;
    if (PeekMessage(&msg, nullptr, 0, 0, PM_REMOVE))
    {
        TranslateMessage(&msg);
        DispatchMessage(&msg);

        if (msg.message == WM_QUIT)
        {
            result=0;
        }
    }
    message = msg.message;
    return result;
}

void Islander::WINDOW::SetWindowIcon(const char* data, int32_t width, int32_t height)
{
    HDC dc = GetDC(0);
    HBITMAP img = CreateCompatibleBitmap(dc, width, height);

    BITMAPINFO descriptor;
    memset(&descriptor, 0, sizeof(descriptor));
    descriptor.bmiHeader.biSize = sizeof(descriptor);
    descriptor.bmiHeader.biWidth = width;
    descriptor.bmiHeader.biHeight = height;
    descriptor.bmiHeader.biPlanes = 1;
    descriptor.bmiHeader.biBitCount = 32;
    descriptor.bmiHeader.biCompression = BI_RGB;

    SetDIBits(dc, img, 0, height, data, &descriptor, DIB_RGB_COLORS);

    HBITMAP hMask = CreateCompatibleBitmap(GetDC(NULL),
        width, height);

    ICONINFO ii = { 0 };
    ii.fIcon = 0;
    ii.hbmColor = img;
    ii.hbmMask = hMask;
    ii.xHotspot = 0;
    ii.yHotspot = 0;

    auto icon = CreateIconIndirect(&ii);

    SendMessage(_impl->_hwnd, WM_SETICON, ICON_SMALL, (LPARAM)icon);
    SendMessage(_impl->_hwnd, WM_SETICON, ICON_BIG, (LPARAM)icon);
}

ATOM Islander::InternalWindow::MyRegisterClass(HINSTANCE hInstance)
{
    WNDCLASSEX wcex;

    wcex.cbSize = sizeof(WNDCLASSEX);

    auto wndproc = &Islander::InternalWindow::WndProc;
    
    wcex.style			= CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
    wcex.lpfnWndProc	= wndproc;
    wcex.cbClsExtra		= 0;
    wcex.cbWndExtra		= 0;
    wcex.hInstance		= hInstance;
    wcex.hIcon			= NULL; //LoadIcon(hInstance, MAKEINTRESOURCE(IDI_CLIENT));
    wcex.hCursor		= LoadCursor(NULL, IDC_ARROW);
    wcex.hbrBackground	= (HBRUSH)(COLOR_WINDOW+1);
    wcex.lpszMenuName	= NULL;// MAKEINTRESOURCE(IDC_CLIENT);
    wcex.lpszClassName	= _name;
    wcex.hIconSm		= NULL; //LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

    return RegisterClassEx(&wcex);
}


BOOL Islander::InternalWindow::InitInstance(HINSTANCE hInstance, int nCmdShow, HWND * hWnd, HWND * parent)
{
    auto hInst = hInstance;

    int posX = (GetSystemMetrics(SM_CXSCREEN)-_width)/2;
    int posY = (GetSystemMetrics(SM_CYSCREEN)-_height)/2;

    _flags = WS_CLIPSIBLINGS | WS_CLIPCHILDREN;

    _flags |= parent ? WS_CHILD : WS_POPUP;

    *hWnd = CreateWindowEx(WS_EX_APPWINDOW, _name, _name, _flags, //WS_OVERLAPPEDWINDOW,
        posX, posY, _width, _height, parent ? *parent : NULL, NULL, hInstance, NULL);

    if (!*hWnd)
    {
        int code = GetLastError();
        return FALSE;
    }

    SetWindowLongPtr(*hWnd, GWLP_USERDATA, reinterpret_cast<LONG_PTR>(this));

    ShowWindow(*hWnd, nCmdShow);
    UpdateWindow(*hWnd);

    return TRUE;
}

// Forward declare message handler from imgui_impl_win32.cpp
extern IMGUI_IMPL_API LRESULT ImGui_ImplWin32_WndProcHandler(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);

LRESULT CALLBACK Islander::InternalWindow::WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    if (ImGui_ImplWin32_WndProcHandler(hWnd, message, wParam, lParam))
        return true;

    int wmId, wmEvent;
    PAINTSTRUCT ps;
    HDC hdc;

    float resX = GetSystemMetrics(SM_CXSCREEN);
    float resY = GetSystemMetrics(SM_CYSCREEN);
    RECT rect;
    GetWindowRect(hWnd, &rect);

    auto window = reinterpret_cast<Islander::InternalWindow*>(GetWindowLongPtr(hWnd, GWLP_USERDATA));

    switch (message)
    {
    case WM_SYSKEYDOWN:
        if(wParam == VK_RETURN && GetAsyncKeyState(VK_MENU))
        {
            window->_fullscreen = !window->_fullscreen;
        }
        break;
    case WM_KEYDOWN:
        {
            auto value = window->_input.keymap.find(wParam);
            if (value != window->_input.keymap.end())
                window->_input.keysdown[value->second] = true;
            /*if (wParam == KEY_ALT)
                window->_input.alt = true;
            if (wParam == KEY_CONTROL)
                window->_input.control = true;
            if (wParam == KEY_SHIFT)
            window->_input.shift = true;*/
        }
        break;
    case WM_KEYUP:
        {
            auto value = window->_input.keymap.find(wParam);
            if (value != window->_input.keymap.end())
                window->_input.keysdown[value->second] = false;
            /*if (wParam == KEY_ALT)
                window->_input.alt = false;
            if (wParam == KEY_CONTROL)
                window->_input.control = false;
            if (wParam == KEY_SHIFT)
            window->_input.shift = false;*/
        }
        break;
    case WM_MOUSEMOVE:
        {

            POINT p;
            if (GetCursorPos(&p))
            {
                if (ScreenToClient(hWnd, &p))
                {
                    Islander::Vec2i pos(p.x, p.y);
                    if (window->_fullscreen)
                    {
                        pos.SetX(p.x * window->_width / resX);
                        pos.SetY(p.y * window->_height / resY);
                    }
                    window->_input.mousepos = pos;
                }
            }

        }
        break;
    case WM_LBUTTONDOWN:
        {

            POINT p;
            if (GetCursorPos(&p))
            {
                if (ScreenToClient(hWnd, &p))
                {
                    Islander::Vec2i pos(p.x, p.y);
                    if (window->_fullscreen)
                    {
                        pos.SetX(p.x * window->_width / resX);
                        pos.SetY(p.y * window->_height / resY);
                    }
                    window->_input.mouse_input.push(pos);
                    window->_input.lbuttondown = MOUSE_INPUT_DOWN;
                }
            }

        }
        break;
    case WM_LBUTTONUP:
        {

            window->_input.lbuttondown = MOUSE_INPUT_UP;

        }
        break;
    case WM_RBUTTONDOWN:
        {
            
            POINT p;
            if (GetCursorPos(&p))
            {
                if (ScreenToClient(hWnd, &p))
                {
                    if (window->_fullscreen)
                    {
                        p.x = p.x * window->_width / resX;
                        p.y = p.y * window->_height / resY;
                    }
                    window->_input.initial_pos.SetX(p.x);
                    window->_input.initial_pos.SetY(p.y);
                    window->_input.rbuttondown = MOUSE_INPUT_DOWN;
                }
            }

        }
        break;
    case WM_RBUTTONUP:
        {

            window->_input.rbuttondown = MOUSE_INPUT_UP;

        }
        break;
    case WM_MOUSEWHEEL:
        {
            // Handle the mouse wheel, we don't support reporting under delta moves.
            auto zDelta = GET_WHEEL_DELTA_WPARAM(wParam);

            window->_input.mouseWheelDelta += zDelta;
            window->_input.mouseWheel = window->_input.mouseWheelDelta / WHEEL_DELTA;
            window->_input.mouseWheelDelta %= WHEEL_DELTA;

            return true;
        }
    case WM_SETCURSOR:
        return true;
    case WM_CLOSE:
    case WM_DESTROY:
        PostQuitMessage(wParam);
        break;
    default:
        return DefWindowProc(hWnd, message, wParam, lParam);
    }
    return 0;
}

void screenToViewport(float x, float y, WINDOW* window, float & outx, float & outy)
{

    outx = x*2 / window->Width() -1;
    outy = -(y*2 / window->Height() -1);

}
