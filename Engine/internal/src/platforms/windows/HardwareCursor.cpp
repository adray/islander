#include "HardwareCursor.h"
#include "TextureLoader.h"
#include "Logger.h"

namespace Windows
{
#include <Windows.h>
}

using namespace Islander;
using namespace Islander::Renderer;

#undef LoadCursor

HardwareCursor::HardwareCursor(WINDOW* window) : _window(window)
{
}

int HardwareCursor::LoadCursor(const std::string& path)
{
	unsigned char* data;
	TextureLoadInfo info;
	if ((data = LoadTexture(path.c_str(), TEXTURE_LOADER_FLIP_Y, info)) == 0)
	{
		Logger::GetLogger()->Log("Failed to load cursor", LOGGING_INFO);
		return 0;
	}

	Windows::HDC dc = Windows::GetDC(0);
	Windows::HBITMAP img = CreateCompatibleBitmap(dc, info._width, info._height);

	Windows::BITMAPINFO descriptor;
	memset(&descriptor, 0, sizeof(descriptor));
	descriptor.bmiHeader.biSize = sizeof(descriptor);
	descriptor.bmiHeader.biWidth = info._width;
	descriptor.bmiHeader.biHeight = info._height;
	descriptor.bmiHeader.biPlanes = 1;
	descriptor.bmiHeader.biBitCount = 32;
	descriptor.bmiHeader.biCompression = BI_RGB;

	SetDIBits(dc, img, 0, info._height, data, &descriptor, DIB_RGB_COLORS);

	Windows::HBITMAP hMask = Windows::CreateCompatibleBitmap(Windows::GetDC(NULL),
		info._width, info._height);

	Windows::ICONINFO ii = { 0 };
	ii.fIcon = 0;
	ii.hbmColor = img;
	ii.hbmMask = hMask;
	ii.xHotspot = 0;
	ii.yHotspot = 0;

	_images.push_back(Windows::CreateIconIndirect(&ii));
	return _images.size() - 1;
}

void HardwareCursor::SetCursor(int type)
{
	Windows::SetCursor((Windows::HCURSOR)_images[type]);
}

void HardwareCursor::ShowCursor()
{
    Windows::CURSORINFO info;
    info.cbSize = sizeof(Windows::CURSORINFO);

    Windows::GetCursorInfo(&info);
    if (info.flags == 0)
    {
        Windows::ShowCursor(true);
    }
}

void HardwareCursor::HideCursor()
{
    Windows::CURSORINFO info;
    info.cbSize = sizeof(Windows::CURSORINFO);

    Windows::GetCursorInfo(&info);
    if (info.flags == CURSOR_SHOWING)
    {
        Windows::ShowCursor(false);
    }
}

HardwareCursor::~HardwareCursor()
{
}
