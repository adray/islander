#include "RenderSystems.h"
#include "d3d11\D3D11Renderer.h"
#include "opengl\GLRenderer.h"

using namespace Islander::Renderer;

void RenderSystems::GetRenderers(std::vector<IRenderer*>& renderers)
{
	if (_systems.size() == 0)
	{
		_systems.push_back(new D3d11Renderer());
		_systems.push_back(new GLRenderer());
	}

	for (auto system : _systems)
	{
		renderers.push_back(system);
	}
}
