#include "Device.h"
#include "Quad.h"
#include "GraphicsBuffer.h"
#include "GraphicsShader.h"
#include "FontEngine.h"
#include "Matrix.h"
#include "RenderSystems.h"
#include "ResourceLoader.h"
#include <FileSystem.h>
#include <Logger.h>
#include "LocklessQueue.h"
#include "Polygon.h"
#include "AnimationController.h"
#include "Frustum.h"
#include "RenderGrid.h"
#include "ParticleSystem\ParticleSystem.h"
#include "glm\gtx\quaternion.hpp"
#include <Component.h>
#include <sstream>
#include <thread>

#if ISLANDER_FREETYPE_SUPPORTED
#include "freetype\FreeTypeEngine.h"
#endif

using namespace Islander;
using namespace Islander::Renderer;
using namespace Islander::Numerics;
using namespace Islander::Model;

extern "C"
{
#include <entity.h>

    struct render_sprite_entity
    {
        struct row_entity entity;
        struct component_transform transform;
        struct component_sprite sprite;
    };

    struct render_mesh_entity
    {
        struct row_entity entity;
        struct component_transform transform;
        struct component_mesh mesh;
    };

    struct render_text_entity
    {
        struct row_entity entity;
        struct component_transform transform;
        struct component_text text;
    };
}

namespace Islander
{
    namespace Renderer
    {
        struct RenderGrid;
        class ParticleRenderer;

        struct Query
        {
            ISLANDER_RESULT_SET result;
            int volatile waithandle;
            ISLANDER_WORLD scene;
            ISLANDER_QUERY query;
        };

        struct RenderProperty
        {
            ISLANDER_QUERY spriteQuery;
            ISLANDER_QUERY textQuery;
            ISLANDER_QUERY meshQuery;
            bool handlersAdded;

            RenderProperty()
                : handlersAdded(false), spriteQuery(nullptr), textQuery(nullptr), meshQuery(nullptr)
            { }
        };

        struct QueryEngine
        {
            LocklessQueue<Query*> queue;
            std::mutex _mutex;
            std::condition_variable _condition;
            bool _process_data;
            bool shutting_down;

            QueryEngine()
                : shutting_down(false),
                _process_data(false)
            {}
        };

        static void QueryAsync(QueryEngine* engine, Query* query)
        {
            query->waithandle = 0;
            engine->queue.Queue(query);

            {
                // acquire the lock, once held allow the queue to be processed
                std::unique_lock<decltype(engine->_mutex)>lock(engine->_mutex);
                engine->_process_data = true;
            }

            engine->_condition.notify_one();
        }

        static void Wait(Query* query)
        {
            // Wait using a spin
            while (!query->waithandle){};
        }

        static void ConsumeQueue(QueryEngine* engine)
        {
            Query* query;
            while (!engine->shutting_down)
            {
                if (engine->queue.TryDequeue(&query))
                {
                    IslanderExecuteQuery(query->scene,
                        query->query,
                        &query->result);
                    query->waithandle = 1;
                }
                else
                {
                    // Acquire the lock and then release the lock when wait is called
                    // Then wait until can process data, when the lock is then reacquired
                    // Set the process flag and release the lock
                    std::unique_lock<decltype(engine->_mutex)>lock(engine->_mutex);
                    engine->_condition.wait(lock, [engine] { return engine->_process_data; });
                    engine->_process_data = false;
                }
            }
        }
    }
}

Device::Device(void)
    :
    drawcalls(0),
    numtriangles(0),
    current_rendertarget(-1),
    preferred(ISLANDER_RENDERER_TYPE_GL),
    renderer(nullptr),
    resourceLoader(nullptr),
    resourceLoader2(nullptr),
    local_allocator(nullptr)
{
    passlist.AddPass(current_rendertarget, ISLANDER_RENDER_FLAGS_CLEAR, "Default Pass");
}

Islander::IResourceLoader2* Device::GetResourceLoader2() { return resourceLoader2; };

int Device::Init(WINDOW *hWindow, FontDescription& font)
{
    renderSystems = new RenderSystems();

    std::vector<IRenderer*> renderers;
    renderSystems->GetRenderers(renderers);

    for (auto it : renderers)
    {
        if (it->RendererType() == preferred)
        {
            renderer = it;
            break;
        }
    }

    if (renderer == nullptr)
    {
        renderer = renderers.at(0);
    }

    LoggerStream& stream = Logger::GetStream(LOGGING_RENDERING) << "Using Renderer: ";

    if (renderer->RendererType() == ISLANDER_RENDERER_TYPE_D3D11)
    {
        stream << "DirectX11";
    }
    else if (renderer->RendererType() == ISLANDER_RENDERER_TYPE_GL)
    {
        stream << "OpenGL";
    }

    stream.endl();

    renderer->Init(hWindow, font);

    resourceLoader = new ResourceLoader(*this);
    resourceLoader2 = new ResourceLoader2(*this);

#if ISLANDER_FREETYPE_SUPPORTED
	_freetype = new FreeTypeEngine();
#endif

    auto particleRenderer = renderer->GetParticleRenderer();
    particleSystem = CreateParticleSystem(particleRenderer, this);

    LoadFont(font);

    quad = new Quad();
    line = new Line();
    quad->UpdateBuffer(renderer);
    line->UpdateBuffer(renderer);

    window = hWindow;
    
    // Start executing
    queryEngine = new QueryEngine();
    querythread = new std::thread(ConsumeQueue, queryEngine);

    debugbonemesh = nullptr;

    return 0;
}

int32_t Device::GetCurrentRenderer()
{
    if (renderer == nullptr)
    {
        return preferred;
    }

    return renderer->RendererType();
}

void Device::SetFullscreen(bool mode)
{
    renderer->SetFullscreen(mode);
}

Device::~Device(void)
{
    DestroyParticleSystem(particleSystem);
    delete renderer;
    delete renderSystems;

    for (auto engine : engine)
        delete engine.second;

    queryEngine->shutting_down = true;
    querythread->join();
    delete querythread;
}

int Device::Release(void)
{
    renderer->Release();

    return 1;
}

void Device::SetPreferredRenderer(int32_t type)
{
    preferred = type;
}

void Device::MeasureText(const std::string& name, char* text, int wrap, int size, float& width, float& height)
{
    component_text ctext;
    component_transform ctransform;

    TextData data;
    data._text = text;
    data.truetype_size = size;
    data._font = new char[name.size()+1];
    strcpy(data._font, name.c_str());
    data._font[name.size()] = '\0';

    ctext.data = &data;
    ctext.wrap = wrap;

    ctransform.sx = width;

    auto it = engine.find(name);
    if (it != engine.end())
    {
        it->second->engine.MeasureText(&ctext, &ctransform);

        width = data._rect.width;
        height = data._rect.height;
    }
#if ISLANDER_FREETYPE_SUPPORTED
    else
    {
        // Must be freetype based
        _freetype->MeasureText(&ctext, &ctransform, getWidth(), getHeight(), 0);

        width = data._rect.width;
        height = data._rect.height;
    }
#endif
}

void* Islander::Renderer::CreateRenderProperty(void* scene)
{
    RenderProperty* property = new RenderProperty();

    ISLANDER_QUERY spritequery;
    ISLANDER_QUERY meshquery;
    ISLANDER_QUERY textquery;
        
    // TODO: freeing
    spritequery = IslanderCompileQuery(scene, "SELECT sprite WHERE sprite.active=1 ORDER BY index");
    meshquery = IslanderCompileQuery(scene, "SELECT mesh WHERE mesh.active=1");
    textquery = IslanderCompileQuery(scene, "SELECT text WHERE text.active=1");

    property->meshQuery = meshquery;
    property->spriteQuery = spritequery;
    property->textQuery = textquery;

    return property;
}

void Device::TextHandler(void* userData, void* row, int e)
{
    if (e == ISLANDER_ROW_EVENT_REMOVED)
    {
        Device* device = reinterpret_cast<Device*>(userData);
        render_text_entity* text = reinterpret_cast<render_text_entity*>(row);

        if (text->text._vertid >= 0)
        {
            device->renderer->FreeVertexBuffer(text->text._vertid);
            #if ISLANDER_FREETYPE_SUPPORTED
            device->_freetype->FreeBuffer(device->renderer, text->text._vertid);
            #endif
        }
    }
}

void Device::MeshHandler(void* userData, void* row, int e)
{
    if (e == ISLANDER_ROW_EVENT_REMOVED)
    {
        Device* device = reinterpret_cast<Device*>(userData);
        render_mesh_entity* mesh = reinterpret_cast<render_mesh_entity*>(row);

        if (mesh->mesh.meshId >= 0)
        {
            device->renderer->FreeVertexBuffer(mesh->mesh.meshId);
        }

        if (mesh->mesh.meshIndexId >= 0)
        {
            device->renderer->FreeIndexBuffer(mesh->mesh.meshIndexId);
        }
    }
}

void GetMaterial(int materialId, component_material& material, IslanderRenderable& renderable, Islander::Model::PolygonModel* poly, ResourceLoader2* resourceLoader)
{
    for (int tex = 0; tex < 4; tex++)
    {
        int id = -1;

        switch (renderable.mesh.material.slot_flags[tex])
        {
        case ISLANDER_RENDERABLE_MATERIAL_SLOT_CUSTOM:
            id = renderable.mesh.material.slot_data[tex];
            break;
        case ISLANDER_RENDERABLE_MATERIAL_SLOT_DIFFUSE:
            if (materialId >= 0 && materialId < poly->numMaterials)
            {
                auto& m = poly->materials[materialId];
                int texId = renderable.mesh.material.slot_data[tex];
                if (texId < m.numDiffuseTextures)
                {
                    id = m.diffuseIndex[texId];
                }
            }
            break;
        case ISLANDER_RENDERABLE_MATERIAL_SLOT_NORMAL:
            if (materialId >= 0 && materialId < poly->numMaterials)
            {
                auto& m = poly->materials[materialId];
                int texId = renderable.mesh.material.slot_data[tex];
                if (texId < m.numNormalTextures)
                {
                    id = m.normalIndex[texId];
                }
            }
            break;
        }

        material.textures[tex].index = id;
    }

    material.pixelShader = renderable.mesh.pixelShader;
    material.vertexShader = renderable.mesh.vertexShader;
    material.geometryShader = renderable.mesh.geometryShader;
}

int Device::RenderScene3D(IslanderRenderablePass* renderablePasses, int renderPassCount, IslanderRenderable* renderables)
{
    // Finish loading any resources
    resourceLoader2->ProcessResources();

    for (int pass = 0; pass < passlist.Count(); ++pass)
    {
        PASS_CONFIG config;
        passlist.FetchPass(pass, &config);

        renderer->BeginPass(config, pass, render_flags);

        IslanderRenderablePass& renderPass = renderablePasses[pass];

        Camera3D* camera = reinterpret_cast<Camera3D*>(renderPass.camera);

        Frustum frustum;
        Matrix4x4 proj(Matrix4x4Type::MATRIX_4X4_ZERO);
        if (camera)
        {
            if (camera->orthographic)
            {
                Matrix4x4::CreateOrthographicMatrix(camera->width, camera->height, camera->nearZ, camera->farZ, &proj);
                CreateFrustumOrthographic(&frustum, camera->width, camera->height, camera->nearZ, camera->farZ);
            }
            else
            {
                Matrix4x4::CreateProjectionMatrix(camera->fovY, camera->aspect, camera->nearZ, camera->farZ, &proj);
                CreateFrustum(&frustum, camera->fovY, camera->aspect, camera->nearZ, camera->farZ);
            }
        }

        Matrix4x4 view(Matrix4x4Type::MATRIX_4X4_ZERO);

        if (camera)
        {
            Matrix4x4::CreateViewMatrix(camera->pos.GetX(), camera->pos.GetY(), camera->pos.GetZ(), camera->look.GetX(), camera->look.GetY(), camera->look.GetZ(), &view);
        }

        float* stream;
        int streamSize = (48 + config.constantDataSize) * sizeof(float);
        if (local_allocator)
        {
            stream = (float*)local_allocator(streamSize);
            //meshqueryasyncptr = new (local_allocator(sizeof(Query))) Query;
        }
        else
        {
            stream = new float[(48 + config.constantDataSize)];
        }

        //mesh rendering
        for (int i = renderPass.renderableBegin; i < renderPass.renderableCount + renderPass.renderableBegin; ++i)
        {
            auto& renderable = renderables[i];

            int indexBuffer = -1;// mesh->mesh.meshIndexId;
            int vertexBuffer = -1;// mesh->mesh.meshId;
            int indexCount = 0;// mesh->mesh.config.index_count;

            auto poly = (Islander::Model::PolygonModel*)renderable.mesh.polydata;

            if (poly != nullptr)
            {
                for (int meshIndex = 0; meshIndex < poly->numMeshes; meshIndex++)
                {
                    auto polyMesh = &poly->meshes[meshIndex];

                    if (poly->reloaded)
                    {
                        if (polyMesh->vertexBuffer > -1)
                        {
                            renderer->FreeVertexBuffer(polyMesh->vertexBuffer);
                            polyMesh->vertexBuffer = -1;
                        }

                        if (polyMesh->indexBuffer > -1)
                        {
                            renderer->FreeIndexBuffer(polyMesh->indexBuffer);
                            polyMesh->indexBuffer = -1;
                        }

                        poly->reloaded = false;
                    }

                    if (polyMesh->vertexBuffer == -1)
                    {
                        polyMesh->vertexBuffer = renderer->CreateVertexBuffer(USAGE_STATIC,
                            polyMesh->numVerts * polyMesh->stride,
                            polyMesh->vertexData,
                            polyMesh->stride,
                            BUFFERING_SINGLE)->ID();
                    }

                    if (polyMesh->indexBuffer == -1)
                    {
                        polyMesh->indexBuffer = renderer->CreateIndexBuffer(USAGE_STATIC,
                            polyMesh->numIndices * sizeof(int),
                            polyMesh->indexData)->ID();
                    }

                    vertexBuffer = polyMesh->vertexBuffer;
                    indexBuffer = polyMesh->indexBuffer;
                    indexCount = polyMesh->numIndices;
                }
            }
            //else if (mesh->mesh.dirty == 1)
            //{
            //    if (mesh->mesh.meshId != -1)
            //    {
            //        renderer->FreeVertexBuffer(mesh->mesh.meshId);
            //        mesh->mesh.meshId = -1;
            //    }

            //    if (mesh->mesh.meshIndexId != -1)
            //    {
            //        renderer->FreeIndexBuffer(mesh->mesh.meshIndexId);
            //        mesh->mesh.meshIndexId = -1;
            //    }

            //    if (mesh->mesh.config.count > 0)
            //    {
            //        mesh->mesh.meshId = renderer->CreateVertexBuffer(USAGE_STATIC,
            //            sizeof(float) * mesh->mesh.config.count,
            //            mesh->mesh.config.data,
            //            mesh->mesh.config.stride,
            //            BUFFERING_SINGLE)->ID();
            //    }

            //    if (mesh->mesh.config.index_count > 0)
            //    {
            //        mesh->mesh.meshIndexId = renderer->CreateIndexBuffer(USAGE_STATIC,
            //            sizeof(int) * mesh->mesh.config.index_count,
            //            mesh->mesh.config.index)->ID();
            //    }

            //    mesh->mesh.dirty = 0;

            //    indexBuffer = mesh->mesh.meshIndexId;
            //    vertexBuffer = mesh->mesh.meshId;

            //    // Commit change to data store (possible to do it async in future)
            //    IslanderUpdateComponent(scene, COMPONENT_MESH, mesh->entity.id, mesh);
            //}

            if (vertexBuffer == -1)
            {
                continue;
            }

            bool skinning = poly != nullptr && poly->meshes[0].skeleton != nullptr && poly->animations != nullptr;

            if (skinning)
            {
                RenderSkinnedMesh(renderable, proj, view, config);
            }
            else
            {
                Matrix4x4 world(Matrix4x4Type::MATRIX_4X4_IDENTITY);
                if (renderable.transform.is_static)
                {
                    world = Matrix4x4(renderable.transform.mat);
                }
                else
                {
                    Matrix4x4::CreateTransformationMatrix(renderable.transform.pos[0],
                        renderable.transform.pos[1],
                        renderable.transform.pos[2],
                        renderable.transform.rot[0],
                        renderable.transform.rot[1],
                        renderable.transform.rot[2],
                        renderable.transform.scale[0],
                        renderable.transform.scale[1],
                        renderable.transform.scale[2],
                        &world);
                }

                if (renderable.mesh.parentEntity != -1)
                {
                    Matrix4x4 parent(Matrix4x4Type::MATRIX_4X4_IDENTITY);

                    //render_mesh_entity parent_transform;
                    //IslanderGetComponent(scene, COMPONENT_MESH, mesh->mesh.parentEntity, &parent_transform);

                    auto& parent_renderable = renderables[renderable.mesh.parentEntity];
                    float pos[3] = {
                        parent_renderable.transform.pos[0],
                        parent_renderable.transform.pos[1],
                        parent_renderable.transform.pos[2]
                    };
                    
                    float rot[3] = {
                        parent_renderable.transform.rot[0],
                        parent_renderable.transform.rot[1],
                        parent_renderable.transform.rot[2]
                    };
                    float scale[3] = {
                        parent_renderable.transform.scale[0],
                        parent_renderable.transform.scale[1],
                        parent_renderable.transform.scale[2]
                    };

                    auto animSystem = (Model::AnimationSystem*)parent_renderable.mesh.animationSystem;

                    auto& controller = animSystem->controllers[parent_renderable.mesh.controllerId];
                    if (parent_renderable.mesh.polydata != nullptr && (controller.animation.isRootMotion || controller.blend.isRootMotion))
                    {
                        auto parent_poly = (Islander::Model::PolygonModel*)parent_renderable.mesh.polydata;

                        float displacement[3] = {};
                        CalculateRootMotion(parent_poly, animSystem, parent_renderable.mesh.controllerId, false, rot, scale, displacement);

                        pos[0] -= displacement[0];
                        pos[1] -= displacement[1];
                        pos[2] -= displacement[2];
                    }

                    Matrix4x4::CreateTransformationMatrix(
                        pos[0],
                        pos[1],
                        pos[2],
                        parent_renderable.transform.rot[0],
                        parent_renderable.transform.rot[1],
                        parent_renderable.transform.rot[2],
                        parent_renderable.transform.scale[0],
                        parent_renderable.transform.scale[1],
                        parent_renderable.transform.scale[2],
                        &parent);

                    // Find parent bone transform if required.
                    //if (mesh->mesh.parentBone != -1 && parent_transform.mesh.polydata != nullptr)
                    if (renderable.mesh.parentBone != -1 && parent_renderable.mesh.polydata != nullptr)
                    {
                        auto parent_poly = (Islander::Model::PolygonModel*)parent_renderable.mesh.polydata;

                        CalculateTransformForPose(parent_poly, animSystem, parent_renderable.mesh.controllerId, renderable.mesh.parentBone, parent);
                    }

                    Matrix4x4::Multiply(world, parent, &world);
                }

                // Perform frustum culling
                if (poly != nullptr && camera)
                {
                    AABB aabb;
                    aabb.min[0] = poly->min[0]; aabb.min[1] = poly->min[1]; aabb.min[2] = poly->min[2];
                    aabb.max[0] = poly->max[0]; aabb.max[1] = poly->max[1]; aabb.max[2] = poly->max[2];
                    CreateTransformedAABB(world, aabb);

                    float centre[3];
                    float radius;
                    AABBToSphere(aabb, centre, &radius);

                    Matrix4x4::TransformVector(view, &centre[0], &centre[1], &centre[2]);
                    if (!FrustumTest(&frustum, centre, radius))
                        continue;
                }

                memcpy(stream, proj.GetData(), sizeof(float) * 16);
                memcpy(stream + 16, view.GetData(), sizeof(float) * 16);
                memcpy(stream + 32, world.GetData(), sizeof(float) * 16);
                if (config.constantDataSize > 0)
                {
                    memcpy(stream + 48, config.constantData, config.constantDataSize);
                }

                IBuffer* constant = renderer->CreateConstantBuffer(USAGE_DYNAMIC, streamSize, stream);

                bool frontface_culling = (config._flags & ISLANDER_RENDER_FLAGS_FRONTFACE_CULL) == ISLANDER_RENDER_FLAGS_FRONTFACE_CULL;
                bool backface_culling = !frontface_culling && !renderable.mesh.wireframe;
                bool no_culling = (config._flags & ISLANDER_RENDER_FLAGS_NO_CULL) == ISLANDER_RENDER_FLAGS_NO_CULL;
                if (no_culling)
                {
                    frontface_culling = false;
                    backface_culling = false;
                }

                if (constant != nullptr)
                {
                    IBuffer* perMeshConstantBuffer = nullptr;
                    if (renderable.mesh.constantBufferDataSize > 0)
                    {
                        perMeshConstantBuffer = renderer->CreateConstantBuffer(USAGE_DYNAMIC, renderable.mesh.constantBufferDataSize, renderable.mesh.constantBufferData);
                    }

                    if (/*poly == nullptr ||*/ poly->numMeshes == 1)
                    {
                        RasterState rasterState;
                        rasterState._backface_culling = backface_culling;
                        rasterState._frontface_culling = frontface_culling;
                        rasterState._multi_sampling = true;
                        rasterState._wireframe = renderable.mesh.wireframe;

                        component_material material;
                        GetMaterial(poly->meshes[0].materialId, material, renderable, poly, resourceLoader2);

                        if (perMeshConstantBuffer != nullptr)
                        {
                            renderer->SubmitDraw(RenderOperation(vertexBuffer, constant->ID(), perMeshConstantBuffer->ID(), indexBuffer, &material, TOPOLOGY_TRIANGLE, SAMPLER_CLAMP, rasterState, 0, indexCount));
                        }
                        else
                        {
                            renderer->SubmitDraw(RenderOperation(vertexBuffer, constant->ID(), indexBuffer, &material, TOPOLOGY_TRIANGLE, SAMPLER_CLAMP, rasterState, 0, indexCount));
                        }
                        renderer->RecycleBuffer(constant);

#if _DEBUG
                        drawcalls++;
                        //numtriangles += buffers.at(line->buffer)->primitivecount;
#endif
                    }
                    else
                    {
                        RasterState rasterState;
                        rasterState._backface_culling = true;
                        rasterState._frontface_culling = frontface_culling;
                        rasterState._multi_sampling = true;
                        rasterState._wireframe = renderable.mesh.wireframe;

                        for (int meshIndex = 0; meshIndex < poly->numMeshes; meshIndex++)
                        {
                            component_material material;
                            GetMaterial(poly->meshes[meshIndex].materialId, material, renderable, poly, resourceLoader2);

                            if (perMeshConstantBuffer != nullptr)
                            {
                                renderer->SubmitDraw(RenderOperation(poly->meshes[meshIndex].vertexBuffer, constant->ID(), perMeshConstantBuffer->ID(), poly->meshes[meshIndex].indexBuffer, &material, TOPOLOGY_TRIANGLE, SAMPLER_CLAMP, rasterState, 0, poly->meshes[meshIndex].numIndices));
                            }
                            else
                            {
                                renderer->SubmitDraw(RenderOperation(poly->meshes[meshIndex].vertexBuffer, constant->ID(), poly->meshes[meshIndex].indexBuffer, &material, TOPOLOGY_TRIANGLE, SAMPLER_CLAMP, rasterState, 0, poly->meshes[meshIndex].numIndices));
                            }
#if _DEBUG
                            drawcalls++;
                            //numtriangles += buffers.at(line->buffer)->primitivecount;
#endif
                        }

                        renderer->RecycleBuffer(constant);
                    }

                    if (perMeshConstantBuffer != nullptr)
                    {
                        renderer->RecycleBuffer(perMeshConstantBuffer);
                    }
                }
            }
        }

        //IslanderFreeResultSet(&meshqueryasyncptr->result);

        if (!local_allocator)
        {
            delete[] stream;
            //delete meshqueryasyncptr;
        }
    }

    render_flags = ISLANDER_RENDER_FLAGS_CLEAR;

    return 1;
}

float Device::Interpolate(float a, float b, float t)
{
    return a + (b - a) * t;
}

void Device::CalculateBoneTransform(PolygonAnimationFrameBone* bone, Matrix4x4& tr, float time)
{
    int start = 0;
    int end = 0;

    for (int i = 1; i < bone->frames.size(); i++)
    {
        if (bone->frames.at(i).time > time)
        {
            start = i - 1;
            end = i;
            break;
        }
    }

    if (start == 0 && end == 0)
    {
        auto f = bone->frames.rbegin();
        Matrix4x4::CreateTransformationMatrix(f->x,
            f->y,
            f->z,
            f->rx,
            f->ry,
            f->rz,
            f->sx,
            f->sy,
            f->sz,
            &tr);
    }
    else
    {
        auto& f1 = bone->frames.at(start);
        auto& f2 = bone->frames.at(end);

        float t = (time - f1.time) / (f2.time - f1.time);

        glm::qua<float> q1;
        q1.w = f1.q[0];
        q1.x = f1.q[1];
        q1.y = f1.q[2];
        q1.z = f1.q[3];

        glm::qua<float> q2;
        q2.w = f2.q[0];
        q2.x = f2.q[1];
        q2.y = f2.q[2];
        q2.z = f2.q[3];

        auto slerp = glm::slerp(q1, q2, t);

        auto rot = glm::eulerAngles(slerp);

        Matrix4x4::CreateTransformationMatrix(Interpolate(f1.x, f2.x, t),
            Interpolate(f1.y, f2.y, t),
            Interpolate(f1.z, f2.z, t),
            rot.x,
            rot.y,
            rot.z,
            Interpolate(f1.sx, f2.sx, t),
            Interpolate(f1.sy, f2.sy, t),
            Interpolate(f1.sz, f2.sz, t),
            &tr);
    }
}

void Device::CalculateBoneMatrixForParenting(Matrix4x4* boneMatrix, PolygonAnimation* animation, PolygonJoint* parent, Matrix4x4& transform, Matrix4x4& inverseMeshTransform, float elapsedTime)
{
    Matrix4x4 t(Matrix4x4Type::MATRIX_4X4_IDENTITY);

    // Find the animation frame associated with bone
    for (int i = 0; i < animation->boneCount; i++)
    {
        if (animation->bones[i].bone != nullptr && parent->id == animation->bones[i].bone->id)
        {
            CalculateBoneTransform(&animation->bones[i], t, elapsedTime); // "global transform"
            break;
        }
    }

    // For the bind pose this should come to the identity matrix (maybe not in all cases?)

    Matrix4x4::Multiply(t, transform, &t); // (parent->parent->transform * parent->transform) * t
    boneMatrix[parent->id] = t;
    Matrix4x4::Multiply(boneMatrix[parent->id], inverseMeshTransform, &boneMatrix[parent->id]);

    for (int i = 0; i < parent->childCount; i++)
    {
        CalculateBoneMatrixForParenting(boneMatrix, animation, parent->children[i], t, inverseMeshTransform, elapsedTime);
    }
}

void Device::CalculateBoneMatrix(Matrix4x4* boneMatrix, PolygonAnimation* animation, PolygonJoint* parent, Matrix4x4* animationFrames, Matrix4x4& transform, Matrix4x4& inverseMeshTransform)
{
    Matrix4x4 t(Matrix4x4Type::MATRIX_4X4_IDENTITY);

    // Find the animation frame associated with bone
    for (int i = 0; i < animation->boneCount; i++)
    {
        if (animation->bones[i].bone != nullptr && parent->id == animation->bones[i].bone->id)
        {
            t = animationFrames[i]; // "global transform"
            break;
        }
    }

    // For the bind pose this should come to the identity matrix (maybe not in all cases?)

    Matrix4x4::Multiply(t, transform, &t); // (parent->parent->transform * parent->transform) * t
    Matrix4x4::Multiply(parent->transform, t, &boneMatrix[parent->id]);
    Matrix4x4::Multiply(boneMatrix[parent->id], inverseMeshTransform,  &boneMatrix[parent->id]);

    for (int i = 0; i < parent->childCount; i++)
    {
        CalculateBoneMatrix(boneMatrix, animation, parent->children[i], animationFrames, t, inverseMeshTransform);
    }
}

void Device::RenderSkinnedMesh(IslanderRenderable& mesh, const Matrix4x4& proj, const Matrix4x4& view, const PASS_CONFIG& config)
{
    auto poly = (Islander::Model::PolygonModel*)mesh.mesh.polydata;

    int animId = 0;
    float elapsed = 0;
    float len = 0;
    float elapsed2 = 0;
    float len2 = 0;
    static Matrix4x4 animationTransforms[MAX_BONES];
    bool isRootMotion = false;

    auto animSystem = (Model::AnimationSystem*)mesh.mesh.animationSystem;
    if (animSystem != nullptr)
    {
        auto& controller = animSystem->controllers[mesh.mesh.controllerId];
        animId = controller.animation.animationId;
        elapsed = controller.animation.elapsedTime;
        len = GetAnimationLength(poly, animId);

        if (controller.animation.loop)
        {
            int iterations = (int)(elapsed / len);
            elapsed = elapsed - (iterations * len);
        }
        else
        {
            elapsed = std::min(elapsed, len);
        }

        if (controller.state == AnimationState::Blended)
        {
            elapsed2 = controller.animation.elapsedTime;
            len2 = GetAnimationLength(poly, controller.animation.animationId2);

            if (controller.animation.loop2)
            {
                int iterations = (int)(elapsed2 / len2);
                elapsed2 = elapsed2 - (iterations * len2);
            }
            else
            {
                elapsed2 = std::min(elapsed2, len2);
            }
        }

        if (animId < poly->animations->animationCount)
        {
            CalculateBoneTransforms(poly->animations, animationTransforms, elapsed, elapsed2, animSystem, mesh.mesh.controllerId);
        }

        isRootMotion = controller.animation.isRootMotion;
        if (controller.state == AnimationState::BlendToAnimation)
        {
            isRootMotion |= controller.blend.isRootMotion;
        }
    }

    Matrix4x4 world(Matrix4x4Type::MATRIX_4X4_IDENTITY);
    if (mesh.transform.is_static)
    {
        world = Matrix4x4(mesh.transform.mat);
    }
    else
    {
        float pos[3] = {
            mesh.transform.pos[0],
            mesh.transform.pos[1],
            mesh.transform.pos[2]
        };
        float rot[3] = {
            mesh.transform.rot[0],
            mesh.transform.rot[1],
            mesh.transform.rot[2]
        };
        float scale[3] = {
            mesh.transform.scale[0],
            mesh.transform.scale[1],
            mesh.transform.scale[2]
        };

        if (isRootMotion)
        {
            float displacement[3] = {};
            Islander::Model::CalculateRootMotion(poly, animSystem, mesh.mesh.controllerId, false, rot, scale, displacement);
            pos[0] -= displacement[0];
            pos[1] -= displacement[1];
            pos[2] -= displacement[2];
        }

        Matrix4x4::CreateTransformationMatrix(
            pos[0], pos[1], pos[2],
            rot[0], rot[1], rot[2],
            scale[0], scale[1], scale[2],
            &world);
    }

    IBuffer* perMeshConstantBuffer = nullptr;
    if (mesh.mesh.constantBufferDataSize > 0)
    {
        perMeshConstantBuffer = renderer->CreateConstantBuffer(USAGE_DYNAMIC, mesh.mesh.constantBufferDataSize, mesh.mesh.constantBufferData);
    }

    for (int meshIndex = 0; meshIndex < poly->numMeshes; meshIndex++)
    {
        auto polyMesh = &poly->meshes[meshIndex];

        if (polyMesh->skeleton == nullptr)
        {
            continue;
        }

        IBuffer* constant = nullptr;

        Matrix4x4& inverseMeshTransform = polyMesh->transform;
        Matrix4x4 transform;
        Matrix4x4::Multiply(polyMesh->transform2, world, &transform);

        RasterState rasterState;
        rasterState._backface_culling = true;
        rasterState._frontface_culling = false;
        rasterState._multi_sampling = true;
        rasterState._wireframe = mesh.mesh.wireframe || (mesh.mesh.debugbones && debugbonemesh);

        // Create a constant buffer foreach sub mesh with a matrix for each bone

        static Matrix4x4 boneMatrix[MAX_BONES];

        if (animId < poly->animations->animationCount)
        {
            auto anim = &poly->animations->animations[animId];
            CalculateBoneMatrix(&boneMatrix[0], anim, polyMesh->skeleton->root, animationTransforms, animationTransforms[0], inverseMeshTransform);
        }
        //else
        //{
        //    Matrix4x4 rootTransform(MATRIX_4X4_IDENTITY);
        //    CalculateBoneMatrix2(&boneMatrix[0], nullptr, polyMesh->skeleton->root, rootTransform, inverseMeshTransform, elapsed);
        //}

        // Build the material - using the submaterial if defined.
        component_material material;
        GetMaterial(polyMesh->materialId, material, mesh, poly, resourceLoader2);

        float* stream = nullptr;
        int size = (16 * 3 + 16 * SUBMESH_MAX_COUNT) * sizeof(float) + config.constantDataSize;
        if (local_allocator) { stream = (float*)local_allocator(size); }
        else { stream = new float[size / sizeof(float)]; }
        for (int i = 0; i < polyMesh->subMeshCount; i++)
        {
            memcpy(stream, proj.GetData(), sizeof(float) * 16);
            memcpy(stream + 16, view.GetData(), sizeof(float) * 16);
            memcpy(stream + 32, /*world.GetData()*/transform.GetData(), sizeof(float) * 16);

            auto submesh = &polyMesh->subMeshes[i];
            for (int j = 0; j < submesh->jointCount; j++)
            {
                auto joint = submesh->joints[j];
                memcpy(stream + 48 + 16 * j, boneMatrix[joint->id].GetData(), sizeof(float) * 16);
            }

            if (config.constantDataSize > 0)
            {
                memcpy(stream + 48 + 16 * SUBMESH_MAX_COUNT, config.constantData, config.constantDataSize);
            }

            constant = renderer->CreateConstantBuffer(USAGE_DYNAMIC, size, stream);

            if (perMeshConstantBuffer != nullptr)
            {
                renderer->SubmitDraw(RenderOperation(polyMesh->vertexBuffer, constant->ID(), perMeshConstantBuffer->ID(), polyMesh->indexBuffer, &material, TOPOLOGY_TRIANGLE, SAMPLER_CLAMP, rasterState, submesh->startIndex, submesh->indexCount));
            }
            else
            {
                renderer->SubmitDraw(RenderOperation(polyMesh->vertexBuffer, constant->ID(), polyMesh->indexBuffer, &material, TOPOLOGY_TRIANGLE, SAMPLER_CLAMP, rasterState, submesh->startIndex, submesh->indexCount));
            }
            renderer->RecycleBuffer(constant);
        }

        if (!local_allocator)
        {
            delete[] stream;
        }
    }

    if (perMeshConstantBuffer != nullptr)
    {
        renderer->RecycleBuffer(perMeshConstantBuffer);
    }

    // Render debug bones

    if (mesh.mesh.debugbones && debugbonemesh && poly->numMeshes > 0)
    {
        auto polyMesh = &poly->meshes[0];

        if (animId < poly->animations->animationCount)
        {
            auto anim = &poly->animations->animations[animId];
            Matrix4x4& inverseMeshTransform = polyMesh->transform;

            Matrix4x4 boneMatrix[MAX_BONES];
            Matrix4x4 rootTransform(Matrix4x4Type::MATRIX_4X4_IDENTITY);
            CalculateBoneTransform(&anim->bones[0], rootTransform, elapsed);
            CalculateBoneMatrixForParenting(&boneMatrix[0], anim, polyMesh->skeleton->root, rootTransform, inverseMeshTransform, elapsed);

            for (int j = 0; j < polyMesh->skeleton->jointCount; j++)
            {
                auto joint = &polyMesh->skeleton->joints[j];

                if (joint->bound)
                {
                    float px, py, pz, qx, qy, qz, qw;

                    Matrix4x4::Decompose(boneMatrix[j], &px, &py, &pz, &qx, &qy, &qz, &qw);

                    Matrix4x4::TransformVector(world, &px, &py, &pz);
                    auto parent = Matrix4x4(Matrix4x4Type::MATRIX_4X4_IDENTITY);
                    Matrix4x4::CreateTranslationMatrix(px, py, pz, &parent);

                    RenderDebugBone(joint, mesh, proj, view, parent);
                }
            }
        }
    }
}

void Device::RenderDebugBone(Islander::Model::PolygonJoint* joint, IslanderRenderable& mesh, const Matrix4x4& proj, const Matrix4x4& view, const Matrix4x4& parent)
{
    for (int i = 0; i < debugbonemesh->numMeshes; i++)
    {
        auto bonemesh = &debugbonemesh->meshes[i];

        if (bonemesh->vertexBuffer == -1)
        {
            bonemesh->vertexBuffer = renderer->CreateVertexBuffer(USAGE_STATIC,
                bonemesh->numVerts * bonemesh->stride,
                bonemesh->vertexData,
                bonemesh->stride,
                BUFFERING_SINGLE)->ID();
        }

        if (bonemesh->indexBuffer == -1)
        {
            bonemesh->indexBuffer = renderer->CreateIndexBuffer(USAGE_STATIC,
                bonemesh->numIndices * sizeof(int),
                bonemesh->indexData)->ID();
        }

        int vertexBuffer = bonemesh->vertexBuffer;
        int indexBuffer = bonemesh->indexBuffer;

        Matrix4x4 world(Matrix4x4Type::MATRIX_4X4_IDENTITY);
        Matrix4x4::CreateTransformationMatrix(0,
            0,
            0,
            0,
            0,
            0,
            .1f,
            .1f,
            .1f,
            &world);

        Matrix4x4::Multiply(world, parent, &world);

        //float y = mesh->transform.py / window->Height();
        //float x = mesh->transform.px / window->Width();

        //float sx = mesh->transform.sx;
        //float sy = mesh->transform.sy;

        float stream[48];
        memcpy(stream, proj.GetData(), sizeof(float) * 16);
        memcpy(stream + 16, view.GetData(), sizeof(float) * 16);
        memcpy(stream + 32, world.GetData(), sizeof(float) * 16);
        //memcpy(stream + 32, joint->transform.GetData(), sizeof(float) * 16);

        auto constant = renderer->CreateConstantBuffer(USAGE_DYNAMIC, sizeof(stream), stream);

        Islander::component_material material;
        PolygonModel* poly = (PolygonModel*)mesh.mesh.polydata;
        GetMaterial(poly->meshes[0].materialId, material, mesh, poly, resourceLoader2);
        material.pixelShader = bonePixelShader;
        material.vertexShader = boneVertexShader;

        if (constant != nullptr)
        {
            RasterState rasterState;
            rasterState._backface_culling = true;
            rasterState._frontface_culling = false;
            rasterState._multi_sampling = true;
            rasterState._wireframe = mesh.mesh.wireframe;

            renderer->SubmitDraw(RenderOperation(vertexBuffer, constant->ID(), indexBuffer, &material, TOPOLOGY_TRIANGLE, SAMPLER_CLAMP, rasterState, 0, bonemesh->numIndices));
            renderer->RecycleBuffer(constant);

#if _DEBUG
            drawcalls++;
            //numtriangles += buffers.at(line->buffer)->primitivecount;
#endif
        }
    }
}

int Device::DispatchCompute(IslanderComputeWorkload* computeWorkload)
{
    IBuffer* constantBuffer = nullptr;
    if (computeWorkload->constantBufferData)
    {
        constantBuffer = renderer->CreateConstantBuffer(USAGE_DYNAMIC, computeWorkload->constantBufferDataSize, computeWorkload->constantBufferData);
    }

    IBuffer* computeBuffer = nullptr;
    if (computeWorkload->computeData)
    {
        computeBuffer = renderer->CreateStructuredBuffer(USAGE_DEFAULT, computeWorkload->computeDataNumBytes, computeWorkload->computeDataStride, computeWorkload->computeData);
    }

    int textures[4] = {};
    for (int i = 0; i < 4; i++)
    {
        if (computeWorkload->computeTextureFlags[i])
        {
            textures[i] = computeWorkload->computeTextures[i];
        }
        else
        {
            textures[i] = -1;
        }
    }

    renderer->DispatchCompute(ComputeOperation(computeWorkload->computeShader,
        computeWorkload->threadGroups,
        constantBuffer != nullptr ? constantBuffer->ID() : -1,
        textures,
        computeBuffer != nullptr ? computeBuffer->ID() : -1
        ));

    if (constantBuffer)
    {
        renderer->RecycleBuffer(constantBuffer);
    }

    if (computeBuffer)
    {
        renderer->FreeStructuredBuffer(computeBuffer->ID());
    }

    return 0;
}

int Device::RenderScene(void* scene, Cameraf* camera, void* renderProperty)
{

#if _DEBUG
    drawcalls = 0;
    numtriangles = 0;
#endif

    // Finish loading any resources
    resourceLoader2->ProcessResources();
    
    ISLANDER_QUERY spritequery;
    ISLANDER_QUERY meshquery;
    ISLANDER_QUERY textquery;

    RenderProperty* property = reinterpret_cast<RenderProperty*>(renderProperty);
    spritequery = property->spriteQuery;
    meshquery = property->meshQuery;
    textquery = property->textQuery;

    if (!property->handlersAdded)
    {
        // Install handlers (if needed)
        IslanderAddComponentHandler(scene, IslanderGetComponentTable(scene, "text"), &Device::TextHandler, this);
        IslanderAddComponentHandler(scene, IslanderGetComponentTable(scene, "mesh"), &Device::MeshHandler, this);

        property->handlersAdded = true;
    }

    Query spritequeryasync;
    spritequeryasync.query = spritequery;
    spritequeryasync.scene = scene;
    IslanderExecuteQuery(spritequeryasync.scene, spritequeryasync.query, &spritequeryasync.result);
    
    render_sprite_entity* entity;

    // hackily set z-index
    for (int i = 0; i < spritequeryasync.result._count; ++i)
    {
        entity = (render_sprite_entity*)spritequeryasync.result._data + i;

        int t0 = entity->sprite.material.textures[0].index + 1;
        int t1 = entity->sprite.material.textures[1].index + 1;
        
        entity->sprite.zindex = (int) (entity->transform.pz * 10000);

        IslanderUpdateComponent(scene, COMPONENT_SPRITE, entity->entity.id, entity);
    }

    IslanderFreeResultSet(&spritequeryasync.result);

    const int maxBatchSize = 100;
    float bindings[(24+4)*maxBatchSize*6];
    
    float quadData[24];
    quadData[0] = -1;
    quadData[1] = 1;
    quadData[2] = 0;
    quadData[3] = 0;

    quadData[4] = 1;
    quadData[5] = 1;
    quadData[6] = 1;
    quadData[7] = 0;

    quadData[8] = -1;
    quadData[9] = -1;
    quadData[10] = 0;
    quadData[11] = 1;

    quadData[12] = -1;
    quadData[13] = -1;
    quadData[14] = 0;
    quadData[15] = 1;

    quadData[16] = 1;
    quadData[17] = 1;
    quadData[18] = 1;
    quadData[19] = 0;

    quadData[20] = 1;
    quadData[21] = -1;
    quadData[22] = 1;
    quadData[23] = 1;

    IslanderExecuteQuery(spritequeryasync.scene, spritequeryasync.query, &spritequeryasync.result);

    for (int pass = 0; pass < passlist.Count(); ++pass)
    {
        PASS_CONFIG config;
        passlist.FetchPass(pass, &config);

        renderer->BeginPass(config, pass, render_flags);
        
        render_sprite_entity* entityNext;
        int texture1 = -1;
        int texture2 = -1;
        int texture3 = -1;
        int texture4 = -1;
        int batchSize = 0;
        
        Query* meshqueryasyncptr = new Query();
        meshqueryasyncptr->query = meshquery;
        meshqueryasyncptr->scene = scene;

        Query* textqueryasyncptr =  new Query();
        textqueryasyncptr->query = textquery;
        textqueryasyncptr->scene = scene;

        QueryAsync(queryEngine, meshqueryasyncptr);
        QueryAsync(queryEngine, textqueryasyncptr);
        
        Matrix3x3 scale(Matrix3x3Type::MATRIX_3X3_IDENTITY);
        Matrix3x3 shear(Matrix3x3Type::MATRIX_3X3_IDENTITY);
        Matrix3x3 rotation(Matrix3x3Type::MATRIX_3X3_IDENTITY);
        Matrix3x3 translation(Matrix3x3Type::MATRIX_3X3_IDENTITY);
        Matrix3x3 proj(Matrix3x3Type::MATRIX_3X3_IDENTITY);
        Matrix3x3 transform(Matrix3x3Type::MATRIX_3X3_IDENTITY);
        
        for (int i = 0; i < spritequeryasync.result._count; ++i)
        {
            entity = (render_sprite_entity*)spritequeryasync.result._data + i;

            // TODO: encode in query?
            if (entity->sprite.material.pass != pass)
                continue;

            float x = entity->transform.px;
            float y = entity->transform.py;

            bool tex1_flip = renderer->RequiresFlipping(entity->sprite.material.textures[0].index);
            bool tex2_flip = renderer->RequiresFlipping(entity->sprite.material.textures[1].index);
            
            float sx = entity->transform.sx;
            float sy = entity->transform.sy;

            // cull off screen objects
            //if (!(x + -camera->GetX() >= -1-obj-> && obj->pos[0]+scene->offsetx <= 1&&
            //    obj->pos[1] + scene->offsety >= -1-obj->scale[1] && obj->pos[1]+scene->offsety <= 1))
            //    continue;
            
            Matrix3x3::CreateScalingMatrix(sx, sy, &scale);

            Matrix3x3::CreateRotationMatrix(entity->transform.dx, entity->transform.dy, &rotation);

            Matrix3x3::CreateTranslationMatrix(x - camera->GetX()*window->Width(), y - camera->GetY()*window->Height(), &translation);

            Matrix3x3::CreateScalingMatrix(camera->GetZ() / (float)window->Width(), camera->GetZ() / (float)window->Height(), &proj);

            Matrix3x3::CreateShearMatrix(entity->transform.shearX, entity->transform.shearY, &shear);

            Matrix3x3::Multiply(proj, translation, &transform);
            Matrix3x3::Multiply(transform, rotation, &transform);
            Matrix3x3::Multiply(transform, shear, &transform);
            Matrix3x3::Multiply(transform, scale, &transform);

            
            float realstream[28];
            float* stream = realstream+4;
            stream[0] = transform(0, 0);
            stream[1] = transform(1, 0);
            stream[2] = transform(2, 0);
            stream[3] = transform(0, 1);
            if (entity->sprite.material.textures[0].source == TEXTURE_SRC_DEFAULT)
            {
                stream[4] = entity->sprite.material.textures[0].px;
                stream[5] = tex1_flip ? 1 - entity->sprite.material.textures[0].py : entity->sprite.material.textures[0].py;
            }
            else if (entity->sprite.material.textures[0].source == TEXTURE_SRC_ATLAS)
            {
                stream[4] = entity->sprite.material.textures[0].atlasIndex;
                stream[5] = 0;
            }
            stream[6] = entity->sprite.material.textures[0].sx;
            stream[7] = tex1_flip ? - entity->sprite.material.textures[0].sy : entity->sprite.material.textures[0].sy;
            stream[8] = 1;
            stream[9] = 1;
            stream[10] = 1;
            stream[11] = 1 / (1 + entity->transform.pz) - 0.00001f;
            stream[12] = transform(1, 1);
            stream[13] = transform(2, 1);
            stream[14] = transform(0, 2);
            stream[15] = transform(1, 2);
            stream[16] = 0;
            stream[17] = entity->sprite.material.clipping;
            stream[18] = getWidth() / 2;
            stream[19] = 1.0f;
            if (entity->sprite.material.textures[1].source == TEXTURE_SRC_DEFAULT)
            {
                stream[20] = entity->sprite.material.textures[1].px;
                stream[21] = tex2_flip ? 1 - entity->sprite.material.textures[1].py : entity->sprite.material.textures[1].py;
            }
            else if (entity->sprite.material.textures[1].source == TEXTURE_SRC_ATLAS)
            {
                stream[20] = entity->sprite.material.textures[1].atlasIndex;
                stream[21] = 0;
            }
            stream[22] = entity->sprite.material.textures[1].sx;
            stream[23] = tex2_flip ? - entity->sprite.material.textures[1].sy : entity->sprite.material.textures[1].sy;

            QuadBindingContext qC;
            qC.camera = camera;
            qC.mat = &entity->sprite.material;
            qC.properties = &entity->entity;
            qC.sx = sx;
            qC.sy = sy;
            qC.x = x;
            qC.y = y;

            renderer->BindQuad(realstream, qC);
            
            for (int k = 0; k < 6; k++)
            {
                assert(batchSize * 6 * 28 + k * 28 + 4 + 24 <= sizeof(bindings) / sizeof(float));
                memcpy(bindings +batchSize * 6*28 + k*28, quadData + k*4, sizeof(float)*4);
                memcpy(bindings +batchSize * 6*28 + k*28 + 4, stream, sizeof(float)*24);
            }

            batchSize++;

            if (entity->sprite.material.textures[0].index != -1)
                texture1 = entity->sprite.material.textures[0].index;
            if (entity->sprite.material.textures[1].index != -1)
                texture2 = entity->sprite.material.textures[1].index;
            if (entity->sprite.material.textures[2].index != -1)
                texture3 = entity->sprite.material.textures[2].index;
            if (entity->sprite.material.textures[3].index != -1)
                texture4 = entity->sprite.material.textures[3].index;

            if (i + 1 < spritequeryasync.result._count)
            {
                entityNext = (render_sprite_entity*)spritequeryasync.result._data + i + 1;
            }
            else
            {
                entityNext = nullptr;
            }

            bool submit = batchSize == maxBatchSize || entityNext == nullptr;
            if (!submit && entityNext)
            {
                submit |= entityNext->sprite.material.textures[0].index != texture1;
                submit |= texture2 != -1 && entityNext->sprite.material.textures[1].index != texture2;
                submit |= texture3 != -1 && entityNext->sprite.material.textures[2].index != texture3;
                submit |= texture4 != -1 && entityNext->sprite.material.textures[3].index != texture4;
                submit |= entityNext->sprite.material.vertexShader != entity->sprite.material.vertexShader
                    || entityNext->sprite.material.pixelShader != entity->sprite.material.pixelShader;
                submit |= entity->sprite.wrap != entityNext->sprite.wrap;
                submit |= entity->sprite.material.pass != entityNext->sprite.material.pass;
            }

            if (submit)
            {
                auto instance = renderer->CreateVertexBuffer(USAGE_DYNAMIC,
                    sizeof(float)*28*6*maxBatchSize,
                    bindings,
                    sizeof(float) * 28,
                    BUFFERING_DOUBLE);

                IBuffer* constantBuffer = nullptr;
                int constantBufferId = -1;
                if (config.constantDataSize > 0)
                {
                    constantBuffer = renderer->CreateConstantBuffer(USAGE_DYNAMIC, config.constantDataSize, config.constantData);
                    constantBufferId = constantBuffer->ID();
                }

                if (instance != nullptr)
                {
                    renderer->SubmitDraw(RenderOperation(instance->ID(), constantBufferId, &entity->sprite.material, TOPOLOGY_TRIANGLE,
                        entity->sprite.wrap ? SAMPLER_WRAP : SAMPLER_CLAMP, batchSize*6));

#if _DEBUG
                    drawcalls++;
                    numtriangles += 2 * batchSize;
#endif
                    renderer->RecycleBuffer(instance);
                }

                if (constantBuffer != nullptr)
                {
                    renderer->RecycleBuffer(constantBuffer);
                }

                batchSize = 0;

                texture1 = -1;
                texture2 = -1;
                texture3 = -1;
                texture4 = -1;
            }
        }

        Wait(meshqueryasyncptr);
        
        render_mesh_entity* mesh;

        //mesh rendering
        for (int i = 0; i < meshqueryasyncptr->result._count; ++i)
        {
            mesh = (render_mesh_entity*)meshqueryasyncptr->result._data + i;

            if (mesh->mesh.material.pass != pass)
                continue;
            
            if (mesh->mesh.dirty == 1)
            {
                if (mesh->mesh.meshId != -1)
                {
                    renderer->FreeVertexBuffer(mesh->mesh.meshId);
                    mesh->mesh.meshId = -1;
                }

                if (mesh->mesh.config.count > 0)
                {
                    mesh->mesh.meshId = renderer->CreateVertexBuffer(USAGE_STATIC,
                        sizeof(float) * mesh->mesh.config.count,
                        mesh->mesh.config.data,
                        mesh->mesh.config.stride,
                        BUFFERING_SINGLE)->ID();
                }

                mesh->mesh.dirty = 0;

                // Commit change to data store (possible to do it async in future)
                IslanderUpdateComponent(scene, COMPONENT_MESH, mesh->entity.id, mesh);
            }

            if (mesh->mesh.meshId == -1)
            {
                continue;
            }

            float y = mesh->transform.py / window->Height();
            float x = mesh->transform.px / window->Width();

            float sx = mesh->transform.sx;
            float sy = mesh->transform.sy;

            float stream[8];
            stream[0] = camera->GetZ() * (x - camera->GetX());
            stream[1] = camera->GetZ() * (y - camera->GetY());
            stream[2] = 1 / (1 + mesh->transform.pz) - 0.00001f;
            stream[3] = 1/camera->GetZ() * (window->Width());
            stream[4] = 1/camera->GetZ() * (window->Height());

            auto constant = renderer->CreateConstantBuffer(USAGE_DYNAMIC, sizeof(stream), stream);

            if (constant != nullptr)
            {
                renderer->SubmitDraw(RenderOperation(mesh->mesh.meshId, constant->ID(), &mesh->mesh.material, TOPOLOGY_TRIANGLE, SAMPLER_CLAMP));
                renderer->RecycleBuffer(constant);

#if _DEBUG
                drawcalls++;
                //numtriangles += buffers.at(line->buffer)->primitivecount;
#endif
            }
        }

        IslanderFreeResultSet(&meshqueryasyncptr->result);
        delete meshqueryasyncptr;

//        //line rendering
//        for (UINT i = 0; i < scene->Capacity(); i++)
//        {
//            auto ent = scene->GetEntity(i);
//
//            if (ent == NULL)
//                continue;
//
//            if (!ent->IsActive())
//                continue;
//
//            LineSprite * sprite = ent->GetComponent<LineSprite>();
//            if (sprite == NULL)
//                continue;
//
//            if (sprite->GetMaterial()->_pass != pass)
//                continue;
//
//            if (line->buffer == -1)
//            {
//                line->UpdateBuffer(renderer);
//            }
//
//            //bind shader
//
//            Material * mat = sprite->GetMaterial();
//            
//            Transform transform;
//
//            ent->GetAbsoluteTransform(transform);
//
//            float y = transform.GetPosition().GetY();
//            float x = transform.GetPosition().GetX();
//
//            float sx = transform.GetScale().GetX();
//            float sy = transform.GetScale().GetY();
//
//            FLOAT stream[16];
//            renderer->BindLine(mat, stream, ent, camera, x, y, sx, sy);
//
//            auto constant = renderer->CreateConstantBuffer(USAGE_DYNAMIC, sizeof(stream), stream);
//
//            if (constant != nullptr)
//            {
//                renderer->SubmitDraw(RenderOperation(line->buffer, constant->ID(), mat, TOPOLOGY_LINELIST, SAMPLER_CLAMP));
//                renderer->RecycleBuffer(constant);
//
//#if _DEBUG
//                drawcalls++;
//                //numtriangles += buffers.at(line->buffer)->primitivecount;
//#endif
//
//                
//            }
//        }
        
        Wait(textqueryasyncptr);

        render_text_entity* text;

        //same goes for text
        for (int i = 0; i < textqueryasyncptr->result._count; ++i)
        {
            text = (render_text_entity*)textqueryasyncptr->result._data + i;

            if (text->text.pass != pass)
                continue;

            if (text->text.dirty)
            {
                TextData* data = (TextData*)text->text.data;

                bool isTrueType = (text->text.img >> 16) & (0x1 == 1);
                if (text->text._vertid >= 0)
                {
#if ISLANDER_FREETYPE_SUPPORTED
                    if (isTrueType)
                    {
                        _freetype->FreeBuffer(renderer, text->text._vertid);
                    }
#endif
                    renderer->FreeVertexBuffer(text->text._vertid);
                }

                if (isTrueType)
                {
#if ISLANDER_FREETYPE_SUPPORTED
                    FreeTypeEngineRender render;
                    if (_freetype->RenderText(
                        &text->text,
                        &text->transform,
                        getWidth(),
                        getHeight(),
                        0,
                        renderer,
                        render))
                    {
                        text->text.img = (1 << 16) | render._texture;
                        text->text.dirty = 0;
                    }
                    else
                    {
                        text->text._vertid = -1;
                    }
#endif
                }
                else
                {
                    auto fontEngine = engine.find(data->_font);
                    if (fontEngine != engine.end())
                    {
                        if (fontEngine->second->engine.MakeText(&text->text, &text->transform, getWidth(), getHeight()) == 0)
                        {
                            text->text._vertid = -1;
                        }
                        else
                        {
                            text->text.dirty = 0;
                        }
                    }
                }

                // Commit changes the changes to: VertId, anything set in engine.MakeText
                IslanderUpdateComponent(scene, COMPONENT_TEXT, text->entity.id, text);
            }

            if (text->text._vertid < 0)
                continue;
            
            int pix = text->text.pixel;
            int vertex = text->text.vertex;
            int imgid = text->text.img & 0xff;

            float y = camera->GetZ() * text->transform.py;
            float x = camera->GetZ() * text->transform.px;
            float z = 1 / (1.5f + text->transform.pz) - 0.00001f;

            float stream[16];
            memset(&stream, 0, sizeof(stream));

            TextBindingContext tC;
            tC.properties = &text->entity;
            tC.sprite = &text->text;
            tC.sx = text->transform.sx;
            tC.sy = text->transform.sy;
            tC.x = x;
            tC.y = y;
            tC.z = z;
            tC.vertex = vertex;

            renderer->BindText(stream, tC, engine);

            auto constant = renderer->CreateConstantBuffer(USAGE_DYNAMIC, sizeof(stream), stream);

            if (constant != nullptr)
            {
                renderer->SubmitDraw(RenderOperation(text->text._vertid, constant->ID(), imgid, pix, vertex, TOPOLOGY_TRIANGLE, SAMPLER_CLAMP));
#if _DEBUG
                drawcalls++;
                //numtriangles += buffers.at(sprite->GetVertexBufferID())->primitivecount;
#endif
                renderer->RecycleBuffer(constant);
            }
        }

        IslanderFreeResultSet(&textqueryasyncptr->result);
        delete textqueryasyncptr;
    }

    IslanderFreeResultSet(&spritequeryasync.result);

    render_flags = ISLANDER_RENDER_FLAGS_CLEAR;

    return 1;
}

int Device::RenderParticleSystem(Camera3D* camera)
{
    // TODO: this appears to render to whatever render target and depth buffer
    // is currently active

    renderer->BeginDrawParticles(camera);

    // Render effects
    DrawParticleSystem(particleSystem); //particleSystem->manager->Draw();

    renderer->EndDrawParticles();

    return 1;
}

void Device::SetUISettings(IslanderUISettings* settings)
{
    uiSettings = *settings;
}

void Device::SetUITextSettings(IslanderUITextSetting* settings, int numSettings)
{
    uiTextSizes.clear();

    for (int i = 0; i < numSettings; i++)
    {
        const int id = settings[i].id;
        const int size = settings[i].size;

        uiTextSizes.insert(std::pair<int, int>(id, size));
    }
}

void SetLineVertex(float* data, int offset, float x, float y, float* colour)
{
    data[offset] = x;
    data[1 + offset] = y;
    data[2 + offset] = 0;
    data[3 + offset] = colour[0];
    data[4 + offset] = colour[1];
    data[5 + offset] = colour[2];
    data[6 + offset] = colour[3];
}

void RenderLine(CrimsonCommand* line, IRenderer* renderer, IslanderUISettings* settings, WINDOW* window)
{
    const int vertexBufferDataSize = 7 * 4;
    const int vertexBufferStride = 7;
    float vertexBufferData[vertexBufferDataSize];

    float startX = line->line.startX;
    float startY = line->line.startY;
    float endX = line->line.endX;
    float endY = line->line.endY;

    if (line->line.absolute == CRIMSON_PERCENT)
    {
        startX *= (float)window->Width();
        startY *= (float)window->Height();
        endX *= (float)window->Width();
        endY *= (float)window->Height();
    }

    // Map to screen space
    // TODO: this is probably only going to work for D3D
    float minx = std::min(startX, endX)*2 / window->Width() - 1.0f;
    float miny = -(std::min(startY, endY)*2 / window->Height() - 1.0f);
    float maxx = std::max(startX, endX)*2 / window->Width() - 1.0f;
    float maxy = -(std::max(startY, endY)*2 / window->Height() - 1.0f);

    float thicknessX = line->line.thickness / window->Width();
    float thicknessY = line->line.thickness / window->Height();

    SetLineVertex(vertexBufferData, 0, minx - thicknessX, miny - thicknessY, line->line.colour);
    SetLineVertex(vertexBufferData, vertexBufferStride, maxx + thicknessX, miny - thicknessY, line->line.colour);
    SetLineVertex(vertexBufferData, vertexBufferStride*2, minx - thicknessX, maxy + thicknessY, line->line.colour);
    SetLineVertex(vertexBufferData, vertexBufferStride*3, maxx + thicknessX, maxy + thicknessY, line->line.colour);

    auto vertexBuffer = renderer->CreateVertexBuffer(USAGE_STATIC, vertexBufferDataSize * sizeof(float), vertexBufferData, vertexBufferStride * sizeof(float), BUFFERING_SINGLE);

    const int indexBufferDataSize = 6;
    int indexBufferData[indexBufferDataSize] = {
        0, 1, 2,
        1, 2, 3
    };

    auto indexBuffer = renderer->CreateIndexBuffer(USAGE_STATIC, indexBufferDataSize * sizeof(int), indexBufferData);

    Islander::component_material material;
    material.clipping = -1;
    material.pixelShader = settings->linePixelShader;
    material.vertexShader = settings->lineVertexShader;
    material.geometryShader = -1;
    material.submaterialcount = 0;
    material.textures[0].index = -1;
    material.textures[1].index = -1;
    material.textures[2].index = -1;
    material.textures[3].index = -1;

    RasterState rasterState;
    rasterState._backface_culling = false;
    rasterState._frontface_culling = false;
    rasterState._multi_sampling = false;
    rasterState._wireframe = false;

    renderer->SubmitDraw(RenderOperation(vertexBuffer->ID(), -1, indexBuffer->ID(), &material, TOPOLOGY_TRIANGLE, SAMPLER_CLAMP, rasterState, 0, indexBufferDataSize));

    renderer->FreeIndexBuffer(indexBuffer->ID());
    renderer->FreeVertexBuffer(vertexBuffer->ID());
}

void SetIconVertex(float* data, int offset, float x, float y, float u, float v)
{
    data[offset] = x;
    data[1 + offset] = y;
    data[2 + offset] = 0;
    data[3 + offset] = u;
    data[4 + offset] = v;
}

void RenderIcon(CrimsonCommand* icon, IRenderer* renderer, IslanderUISettings* settings, WINDOW* window)
{
    const int vertexBufferDataSize = 5 * 4;
    const int vertexBufferStride = 5;
    float vertexBufferData[vertexBufferDataSize];

    float x = icon->icon.posXPercent * (float)window->Width();
    float y = icon->icon.posYPercent * (float)window->Height();
    float w = icon->icon.widthPercent * (float)window->Width();
    float h = icon->icon.heightPercent * (float)window->Height();

    // Map to screen space
    // TODO: this is probably only going to work for D3D
    float minx = (x) * 2 / window->Width() - 1.0f;
    float miny = -((y) * 2 / window->Height() - 1.0f);
    float maxx = (x + w) * 2 / window->Width() - 1.0f;
    float maxy = -((y + h) * 2 / window->Height() - 1.0f);

    SetIconVertex(vertexBufferData, 0, minx, miny, icon->icon.px, icon->icon.py);
    SetIconVertex(vertexBufferData, vertexBufferStride, maxx, miny, icon->icon.px + icon->icon.sx, icon->icon.py);
    SetIconVertex(vertexBufferData, vertexBufferStride * 2, minx, maxy, icon->icon.px, icon->icon.py + icon->icon.sy);
    SetIconVertex(vertexBufferData, vertexBufferStride * 3, maxx, maxy, icon->icon.px + icon->icon.sx, icon->icon.py + icon->icon.sy);

    auto vertexBuffer = renderer->CreateVertexBuffer(USAGE_STATIC, vertexBufferDataSize * sizeof(float), vertexBufferData, vertexBufferStride * sizeof(float), BUFFERING_SINGLE);

    const int indexBufferDataSize = 6;
    int indexBufferData[indexBufferDataSize] = {
        0, 1, 2,
        1, 2, 3
    };

    IBuffer* constantBuffer = nullptr;
    IBuffer* constantBuffer2 = nullptr;

    int pixelShader = settings->iconPixelShader;
    int vertexShader = settings->iconVertexShader;

    if (icon->icon.arrayIndex > -1)
    {
        pixelShader = settings->iconArrayPixelShader;
        vertexShader = settings->iconArrayVertexShader;

        char data[32] = {};
        std::memcpy(data, &icon->icon.arrayIndex, sizeof(int));

        constantBuffer = renderer->CreateConstantBuffer(USAGE_DYNAMIC, sizeof(data), data);
    }

    IslanderUIRenderData* renderData = reinterpret_cast<IslanderUIRenderData*>(icon->userData);
    if (renderData)
    {
        constantBuffer2 = renderer->CreateConstantBuffer(USAGE_DYNAMIC, renderData->constantDataSize, renderData->constantData);
    }

    auto indexBuffer = renderer->CreateIndexBuffer(USAGE_STATIC, indexBufferDataSize * sizeof(int), indexBufferData);

    Islander::component_material material;
    material.clipping = -1;
    material.pixelShader = pixelShader;
    material.vertexShader = vertexShader;
    material.geometryShader = -1;
    material.submaterialcount = 0;
    material.textures[0].index = icon->icon.imageId;
    material.textures[1].index = -1;
    material.textures[2].index = -1;
    material.textures[3].index = -1;

    RasterState rasterState;
    rasterState._backface_culling = false;
    rasterState._frontface_culling = false;
    rasterState._multi_sampling = false;
    rasterState._wireframe = false;

    renderer->SubmitDraw(RenderOperation(vertexBuffer->ID(),
        constantBuffer != nullptr ? constantBuffer->ID() : -1,
        constantBuffer2 != nullptr ? constantBuffer2->ID() : -1,
        indexBuffer->ID(),
        &material,
        TOPOLOGY_TRIANGLE,
        SAMPLER_CLAMP,
        rasterState,
        0,
        indexBufferDataSize));

    renderer->FreeIndexBuffer(indexBuffer->ID());
    renderer->FreeVertexBuffer(vertexBuffer->ID());

    if (constantBuffer != nullptr)
    {
        renderer->RecycleBuffer(constantBuffer);
    }

    if (constantBuffer2 != nullptr)
    {
        renderer->RecycleBuffer(constantBuffer2);
    }
}

void SetFillRectVertex(float* data, int offset, float x, float y, float* colour)
{
    data[offset] = x;
    data[1 + offset] = y;
    data[2 + offset] = 0;
    data[3 + offset] = colour[0];
    data[4 + offset] = colour[1];
    data[5 + offset] = colour[2];
    data[6 + offset] = colour[3];
}

void RenderTri(CrimsonCommand* tri, IRenderer* renderer, IslanderUISettings* settings, WINDOW* window)
{
    const int vertexBufferDataSize = 7 * 3;
    const int vertexBufferStride = 7;
    float vertexBufferData[vertexBufferDataSize];

    float x1 = tri->tri.posXPercent[0] * (float)window->Width();
    float y1 = tri->tri.posYPercent[0] * (float)window->Height();
    float x2 = tri->tri.posXPercent[1] * (float)window->Width();
    float y2 = tri->tri.posYPercent[1] * (float)window->Height();
    float x3 = tri->tri.posXPercent[2] * (float)window->Width();
    float y3 = tri->tri.posYPercent[2] * (float)window->Height();

    // Map to screen space
    // TODO: this is probably only going to work for D3D
    x1 = (x1) * 2 / window->Width() - 1.0f;
    y1 = -((y1) * 2 / window->Height() - 1.0f);
    x2 = (x2) * 2 / window->Width() - 1.0f;
    y2 = -((y2) * 2 / window->Height() - 1.0f);
    x3 = (x3) * 2 / window->Width() - 1.0f;
    y3 = -((y3) * 2 / window->Height() - 1.0f);
    
    SetFillRectVertex(vertexBufferData, 0, x1, y1, tri->tri.colour);
    SetFillRectVertex(vertexBufferData, vertexBufferStride, x2, y2, tri->tri.colour);
    SetFillRectVertex(vertexBufferData, vertexBufferStride * 2, x3, y3, tri->tri.colour);
    //SetFillRectVertex(vertexBufferData, vertexBufferStride * 3, maxx, maxy, tri->tri.colour);

    auto vertexBuffer = renderer->CreateVertexBuffer(USAGE_STATIC, vertexBufferDataSize * sizeof(float), vertexBufferData, vertexBufferStride * sizeof(float), BUFFERING_SINGLE);

    const int indexBufferDataSize = 3;
    int indexBufferData[indexBufferDataSize] = {
        0, 1, 2
    };

    auto indexBuffer = renderer->CreateIndexBuffer(USAGE_STATIC, indexBufferDataSize * sizeof(int), indexBufferData);

    Islander::component_material material;
    material.clipping = -1;
    material.pixelShader = settings->filledRectPixelShader;
    material.vertexShader = settings->filledRectVertexShader;
    material.geometryShader = -1;
    material.submaterialcount = 0;
    material.textures[0].index = -1;
    material.textures[1].index = -1;
    material.textures[2].index = -1;
    material.textures[3].index = -1;

    RasterState rasterState;
    rasterState._backface_culling = false;
    rasterState._frontface_culling = false;
    rasterState._multi_sampling = false;
    rasterState._wireframe = false;

    renderer->SubmitDraw(RenderOperation(vertexBuffer->ID(), -1, indexBuffer->ID(), &material, TOPOLOGY_TRIANGLE, SAMPLER_CLAMP, rasterState, 0, indexBufferDataSize));

    renderer->FreeIndexBuffer(indexBuffer->ID());
    renderer->FreeVertexBuffer(vertexBuffer->ID());
}

void RenderFilledRect(CrimsonCommand* rect, IRenderer* renderer, IslanderUISettings* settings, WINDOW* window)
{
    const int vertexBufferDataSize = 7 * 4;
    const int vertexBufferStride = 7;
    float vertexBufferData[vertexBufferDataSize];

    float x = rect->filledRect.posXPercent * (float)window->Width();
    float y = rect->filledRect.posYPercent * (float)window->Height();
    float w = rect->filledRect.widthPercent * (float)window->Width();
    float h = rect->filledRect.heightPercent * (float)window->Height();

    // Map to screen space
    // TODO: this is probably only going to work for D3D
    float minx = (x) * 2 / window->Width() - 1.0f;
    float miny = -((y) * 2 / window->Height() - 1.0f);
    float maxx = (x + w) * 2 / window->Width() - 1.0f;
    float maxy = -((y + h) * 2 / window->Height() - 1.0f);

    SetFillRectVertex(vertexBufferData, 0, minx, miny, rect->filledRect.colour);
    SetFillRectVertex(vertexBufferData, vertexBufferStride, maxx, miny, rect->filledRect.colour);
    SetFillRectVertex(vertexBufferData, vertexBufferStride * 2, minx, maxy, rect->filledRect.colour);
    SetFillRectVertex(vertexBufferData, vertexBufferStride * 3, maxx, maxy, rect->filledRect.colour);

    auto vertexBuffer = renderer->CreateVertexBuffer(USAGE_STATIC, vertexBufferDataSize * sizeof(float), vertexBufferData, vertexBufferStride * sizeof(float), BUFFERING_SINGLE);

    const int indexBufferDataSize = 6;
    int indexBufferData[indexBufferDataSize] = {
        0, 1, 2,
        1, 2, 3
    };

    auto indexBuffer = renderer->CreateIndexBuffer(USAGE_STATIC, indexBufferDataSize * sizeof(int), indexBufferData);

    Islander::component_material material;
    material.clipping = -1;
    material.pixelShader = settings->filledRectPixelShader;
    material.vertexShader = settings->filledRectVertexShader;
    material.geometryShader = -1;
    material.submaterialcount = 0;
    material.textures[0].index = -1;
    material.textures[1].index = -1;
    material.textures[2].index = -1;
    material.textures[3].index = -1;

    RasterState rasterState;
    rasterState._backface_culling = false;
    rasterState._frontface_culling = false;
    rasterState._multi_sampling = false;
    rasterState._wireframe = false;

    renderer->SubmitDraw(RenderOperation(vertexBuffer->ID(), -1, indexBuffer->ID(), &material, TOPOLOGY_TRIANGLE, SAMPLER_CLAMP, rasterState, 0, indexBufferDataSize));

    renderer->FreeIndexBuffer(indexBuffer->ID());
    renderer->FreeVertexBuffer(vertexBuffer->ID());
}

#if ISLANDER_FREETYPE_SUPPORTED

void InitCache(std::unordered_map<std::string, Islander::Renderer::Device::FreeTypeCacheEntry>& freeTypeCache)
{
    for (auto& it : freeTypeCache)
    {
        it.second.used = false;
    }
}

void FlushCache(IRenderer* renderer, FreeTypeEngine* freetype, std::unordered_map<std::string, Islander::Renderer::Device::FreeTypeCacheEntry>& freeTypeCache)
{
    std::vector<std::string> toFlush;

    for (auto& it : freeTypeCache)
    {
        if (!it.second.used)
        {
            renderer->FreeVertexBuffer(it.second.vertexBuffer);
            freetype->FreeBuffer(renderer, it.second.vertexBuffer);
            toFlush.push_back(it.first);
        }
    }

    for (auto& item : toFlush)
    {
        freeTypeCache.erase(item);
    }
}

void RenderText(CrimsonCommand* text,
    IRenderer* renderer,
    IslanderUISettings* settings,
    const std::unordered_map<int, int>& textSize,
    WINDOW* window,
    FreeTypeEngine* freetype,
    std::unordered_map<std::string, Islander::Renderer::Device::FreeTypeCacheEntry>& freeTypeCache)
{
    TextData data;
    std::memset(&data, 0, sizeof(data));
    data._text = text->text.text;
    data._font = settings->defaultFontName;

    Islander::component_text ctext;
    ctext._vertid = -1;
    ctext.pixel = -1;
    ctext.wrap = (text->text.flags & CRIMSON_TEXT_FLAGS_WRAP) == CRIMSON_TEXT_FLAGS_WRAP;
    ctext.dirty = false;
    ctext.img = -1;
    ctext.pass = 0;
    ctext._colour.r = text->text.colour[0];
    ctext._colour.g = text->text.colour[1];
    ctext._colour.b = text->text.colour[2];
    ctext._colour.a = text->text.colour[3];
    ctext.data = &data;

    Islander::component_transform transform;
    std::memset(&transform, 0, sizeof(transform));
    transform.px = window->Width() * text->text.posXPercent;
    transform.py = window->Height() * text->text.posYPercent;
    transform.sx = window->Width() * text->text.widthPercent;
    transform.sy = window->Height() * text->text.heightPercent;

    const float maxWidth = text->text.widthPercent * window->Width();
    const float maxHeight = text->text.heightPercent * window->Height();

    const uint32_t dpi = window->GetDPI();

    std::stringstream ss;
    ss << text->text.text << "," << int(maxWidth) << "," << int(maxHeight) << "," << text->text.fontType;
    const std::string key = ss.str();

    auto& it = freeTypeCache.find(key);
    if (it != freeTypeCache.end())
    {
        it->second.used = true;

        float drawX = transform.px * 2 / window->Width() - 1.0f;
        float drawY = -(transform.py * 2 / window->Height() - 1.0f);

        if ((text->text.flags & CRIMSON_TEXT_FLAGS_CENTERED) == CRIMSON_TEXT_FLAGS_CENTERED)
        {
            drawX = (transform.px - it->second.renderWidth / 2) * 2 / window->Width() - 1.0f;
        }

        float constantData[16] = {
            drawX,
            drawY,
            0.0f,
            0.0f,
            ctext._colour.r,
            ctext._colour.g,
            ctext._colour.b,
            ctext._colour.a,
            0, 0, 0, 0, 1
        };

        auto constant = renderer->CreateConstantBuffer(USAGE_DYNAMIC, sizeof(constantData), constantData);

        renderer->SubmitDraw(RenderOperation(it->second.vertexBuffer, constant->ID(), it->second.texture, settings->textPixelShader, settings->textVertexShader, TOPOLOGY_TRIANGLE, SAMPLER_CLAMP));

        renderer->RecycleBuffer(constant);

        return;
    }

    auto& font = textSize.find(text->text.fontType);
    if (font != textSize.end())
    {
        data.truetype_size = font->second * 64;
    }
    else
    {
        // Default to 16pt
        data.truetype_size = 16 * 64;
    }

    FreeTypeEngineRender render;
    freetype->RenderText(&ctext, &transform, window->Width(), window->Height(), dpi, renderer, render);

    float drawX = transform.px * 2 / window->Width() - 1.0f;
    float drawY = -(transform.py * 2 / window->Height() - 1.0f);

    if ((text->text.flags & CRIMSON_TEXT_FLAGS_CENTERED) == CRIMSON_TEXT_FLAGS_CENTERED)
    {
        drawX = (transform.px - render._width / 2) * 2 / window->Width() - 1.0f;
    }

    float constantData[16] = {
        drawX,
        drawY,
        0.0f,
        0.0f,
        ctext._colour.r,
        ctext._colour.g,
        ctext._colour.b,
        ctext._colour.a,
        0, 0, 0, 0, 1
    };

    auto constant = renderer->CreateConstantBuffer(USAGE_DYNAMIC, sizeof(constantData), constantData);

    renderer->SubmitDraw(RenderOperation(ctext._vertid, constant->ID(), render._texture, settings->textPixelShader, settings->textVertexShader, TOPOLOGY_TRIANGLE, SAMPLER_CLAMP));

    Device::FreeTypeCacheEntry entry;
    entry.renderWidth = render._width;
    entry.vertexBuffer = ctext._vertid;
    entry.texture = render._texture;
    freeTypeCache.insert(std::pair<std::string, Device::FreeTypeCacheEntry>(key, entry));

    //freetype->FreeBuffer(renderer, ctext._vertid);
    //renderer->FreeVertexBuffer(ctext._vertid);
    renderer->RecycleBuffer(constant);
}

#endif

int Device::RenderUI(CRIMSON_HANDLE crimson)
{
    const int maxCommandCount = 128;
    CrimsonCommand commands[maxCommandCount];

    int index = 0;

#if ISLANDER_FREETYPE_SUPPORTED
    InitCache(freeTypeCache);
#endif

    int numCommands = CrimsonGetCommandList(crimson, commands, maxCommandCount, &index);
    while (numCommands > 0)
    {
        for (int i = 0; i < numCommands; i++)
        {
            CrimsonCommand* cmd = &commands[i];
            switch (cmd->type)
            {
            case CRIMSON_COMMAND_LINE:
                RenderLine(cmd, renderer, &uiSettings, window);
                break;
            case CRIMSON_COMMAND_ICON:
                RenderIcon(cmd, renderer, &uiSettings, window);
                break;
            case CRIMSON_COMMAND_FILL_RECT:
                RenderFilledRect(cmd, renderer, &uiSettings, window);
                break;
#if ISLANDER_FREETYPE_SUPPORTED
            case CRIMSON_COMMAND_TEXT:
                RenderText(cmd, renderer, &uiSettings, uiTextSizes, window, _freetype, freeTypeCache);
#endif
                break;
            case CRIMSON_COMMAND_TRI:
                RenderTri(cmd, renderer, &uiSettings, window);
                break;
            }
        }

        numCommands = CrimsonGetCommandList(crimson, commands, maxCommandCount, &index);
    }

#if ISLANDER_FREETYPE_SUPPORTED
    FlushCache(renderer, _freetype, freeTypeCache);
#endif
    return 1;
}

char* Device::GetBytesFromTexture(int texture, int* width, int* height, int* size)
{
    return renderer->GetBytesFromTexture(texture, width, height, size);
}

void Device::CreateTextureAtlas(int sourceTexture, MaterialTexture* set, int count)
{
    renderer->CreateTextureAtlas(sourceTexture, set, count);
}

void Device::Present()
{
    renderer->Present();
}

IslanderRenderTarget Device::CreateRenderTarget(RenderTargetType type, uint32_t width, uint32_t height)
{
    return renderer->CreateRenderTarget(type, width, height);
}

void Device::ResizeRenderTarget(IslanderRenderTarget target, uint32_t width, uint32_t height)
{
    renderer->ResizeRenderTarget(target, width, height);
}

IslanderRenderTargetArray Device::CreateRenderTargetArray(RenderTargetType type, uint32_t width, uint32_t height, int dimensions)
{
    return renderer->CreateRenderTargetArray(type, width, height, dimensions);
}

void Device::SetRenderTarget(int target)
{
    current_rendertarget = target;
}

void Device::LoadFont(FontDescription& description)
{
    //description.img = resourceLoader->Load(description.filedef, RESOURCE_STORAGE_ATLAS);
    //Font* font = new Font(description, renderer);
    //engine.insert(std::pair<std::string, Font*>(description.name, font));

	std::string filename;
	FileSystem::GetFileName(description.filedef, filename);

	if (filename.length() > 4 &&
		filename[filename.length()-1] == 'f' &&
		filename[filename.length()-2] == 't' &&
		filename[filename.length()-3] == 't' &&
		filename[filename.length()-4] == '.')
	{
#if ISLANDER_FREETYPE_SUPPORTED
		_freetype->LoadFace(description.name, description.filedef);
#else
		std::stringstream ss;
		ss << "Unsupported font type " << filename;
		Islander::Logger::GetLogger()->Log(ss.str(), LOGGING_RENDERING);
#endif
	}
	else
	{
		description.img = resourceLoader->Load(description.filedef, RESOURCE_STORAGE_ATLAS);
		Font* font = new Font(description, renderer);
		engine.insert(std::pair<std::string, Font*>(description.name, font));
	}
}

void Device::SetRenderFlags(int flags)
{
    render_flags = flags;
}

int Device::ReserveTexture()
{
    return renderer->ReserveTexture();
}

int Device:: LoadTexture(void* data, int width, int height, int size)
{
    return renderer->LoadTexture(data, width, height, size);
}

int Device::LoadTexture(int texture, void* data, int width, int height, int size)
{
    return renderer->LoadTexture(texture, data, width, height, size);
}

int Device::LoadTexture(const char* filename)
{
    return renderer->LoadTexture(filename);
}

int Device::CreateCubeMapTexture(int* textureFaces, int width, int height)
{
    return renderer->CreateCubeMapTexture(textureFaces, width, height);
}

int Device::CreateTextureArray(int count, int* textures, int width, int height)
{
    return renderer->CreateTextureArray(count, textures, width, height);
}

int Device::CreateEmptyTexture(int width, int height, int type)
{
    int tex = ReserveTexture();
    renderer->CreateEmptyTexture(tex, width, height, type);
    return tex;
}

void Device::GenerateComputeTexture(int texture)
{
    renderer->CreateComputeTexture(texture);
}

int Device::LoadVertexShader(const char* filename, const char* method, SEMANTIC * semantics, int count, IslanderShaderDefineList* defines)
{
    if (renderer)
    {
        return renderer->LoadVertexShader(filename, method, semantics, count, defines);
    }

    return -1;
}

int Device::LoadPixelShader(const char* filename, const char* method, IslanderShaderDefineList* defines)
{
    if (renderer)
    {
        return renderer->LoadPixelShader(filename, method, defines);
    }

    return -1;
}

int Device::LoadGeometryShader(const char* filename, const char* method, IslanderShaderDefineList* defines)
{
    if (renderer)
    {
        return renderer->LoadGeometryShader(filename, method, defines);
    }

    return -1;
}

int Device::LoadComputeShader(const char* filename, const char* method, IslanderShaderDefineList* defines)
{
    if (renderer)
    {
        return renderer->LoadComputeShader(filename, method, defines);
    }

    return -1;
}

void Device::SetPixelShaderSamplers(int pixelShader, int numSamplers, IslanderTextureSampler* samplers)
{
    if (renderer)
    {
        renderer->SetPixelShaderSamplers(pixelShader, numSamplers, samplers);
    }
}

void Device::SetVertexShaderSamplers(int vertexShader, int numSamplers, IslanderTextureSampler* samplers)
{
    if (renderer)
    {
        renderer->SetVertexShaderSamplers(vertexShader, numSamplers, samplers);
    }
}

int Device:: GetDrawCalls() const
{
    return drawcalls;
}

int Device:: GetNumTriangles() const
{
    return numtriangles;
}

void Device:: DestroyRenderTarget(int id)
{
    renderer->ReleaseRenderTarget(id);
}

void Device:: ResizeBackBuffer()
{
    renderer->ResizeBackBuffer();
}

const int Device:: GetBufferCount() const
{
    // TODO: does nothing at the moment
    return 0;
}

void Device:: SetPasses(const PASS_LIST& list)
{
    passlist = list;
}

void Device:: ClearPasses()
{
    passlist.Clear();
    passlist.AddPass(-1, ISLANDER_RENDER_FLAGS_CLEAR, "Default Pass");
}

bool Device::GetTextureList(TextureListData& list)
{
    return renderer->GetTextureList(list);
}

void Device::GetResolutions(DisplayResolutions& list)
{
    renderer->GetDisplayResolutions(list);
}

void Device::InitializeImgui()
{
    if (renderer)
    {
        renderer->InitializeImgui();
    }
}

void Device::NewFrameImgui()
{
    if (renderer)
    {
        renderer->NewFrameImgui();
    }
}

void Device::RenderImgui()
{
    if (renderer)
    {
        renderer->RenderImgui();
    }
}

void Device::ImageImgui(int texture, float width, float height)
{
    if (renderer)
    {
        renderer->ImageImgui(texture, width, height);
    }
}

void Device::ImageImgui(int texture, float x, float y, float width, float height)
{
    if (renderer)
    {
        renderer->ImageImgui(texture, x, y, width, height);
    }
}

bool Device::ImageButtonImgui(int id, int texture, float x, float y, float sx, float sy)
{
    if (renderer)
    {
        return renderer->ImageButtonImgui(id, texture, x, y, sx, sy);
    }

    return false;
}

void Device::SetSyncInterval(int syncInterval)
{
    if (renderer)
    {
        renderer->SetSyncInterval(syncInterval);
    }
}

void Device::InitializePerformanceCounters()
{
    if (renderer)
    {
        renderer->InitializePerformanceCounters();
    }
}

void Device::BeginGPUProfilingFrame()
{
    if (renderer)
    {
        renderer->BeginGPUProfilingFrame();
    }
}

int Device::EndGPUProfilingFrame()
{
    if (renderer)
    {
        return renderer->EndGPUProfilingFrame();
    }

    return 0;
}

void Device::BeginGPUProfiling()
{
    if (renderer)
    {
        renderer->BeginGPUProfiling();
    }
}

int Device::EndGPUProfiling()
{
    if (renderer)
    {
        return renderer->EndGPUProfiling();
    }

    return 0;
}

bool Device::GetGPUProfileFrameData(int handle, IslanderGPUProfileFrameData& data)
{
    if (renderer)
    {
        return renderer->GetGPUProfileFrameData(handle, data);
    }

    return false;
}

bool Device::GetGPUProfileData(int handle, IslanderGPUProfileData& data)
{
    if (renderer)
    {
        return renderer->GetGPUProfileData(handle, data);
    }

    return false;
}

void Device::GetRendererStats(IslanderRendererStats& stats)
{
    if (renderer)
    {
        renderer->GetStats(stats);
    }
}

void Device::ProcessPolyLibrary(Islander::Model::PolygonMeshLibrary* lib)
{
    for (int id : lib->vertexBufferFree)
    {
        renderer->FreeVertexBuffer(id);
    }

    for (int id : lib->indexBufferFree)
    {
        renderer->FreeIndexBuffer(id);
    }

    lib->indexBufferFree.clear();
    lib->vertexBufferFree.clear();
}
