#include "Selection.h"
#include "Matrix.h"
#include "Component.h"
#include "Frustum.h"
#include "GJK.h"
#include "AABB.h"

using namespace Islander::Numerics;

namespace Islander
{
    void CalcRay(float fov, float ratio, float x, float y, float width, float height, float* vec3)
    {
        // Calculate picking ray from the origin.This should transformed by the inverse of the view matrix before used
        // to intersect with objects.

        float h = cosf(fov * 0.5f) / sinf(fov * 0.5f);
        float w = h / ratio;

        vec3[0] = (((2.0f * x) / width) - 1.0f) / w;
        vec3[1] = (((-2.0f * y) / height) + 1.0f) / h;
        vec3[2] = 1;
    }

    bool RayPlaneTest(float* origin, float* dir, float* planeCentre, float* planeNormal, float* pos)
    {
        float t;
        float denom = Dot(planeNormal[0], planeNormal[1], planeNormal[2], dir[0], dir[1], dir[2]);
        if (std::abs(denom) > 0.0001f)
        {
            float x = planeCentre[0] - origin[0];
            float y = planeCentre[1] - origin[1];
            float z = planeCentre[2] - origin[2];

            t = Dot(x, y, z, planeNormal[0], planeNormal[1], planeNormal[2]) / denom;
            if (t >= 0)
            {
                pos[0] = t * dir[0];
                pos[1] = t * dir[1];
                pos[2] = t * dir[2];
                return true;
            }
        }
        return false;
    }

    bool BoxSelect(ISLANDER_CAMERA camera, float minx, float miny, float maxx, float maxy, float screenWidth, float screenHeight, IslanderSelectionData* selectionData)
    {
        auto cam = (Islander::Camera3D*)camera;

        // Caclulate the view matrix
        Matrix4x4 view(Matrix4x4Type::MATRIX_4X4_ZERO);
        Matrix4x4::CreateViewMatrix(cam->pos.GetX(), cam->pos.GetY(), cam->pos.GetZ(), cam->look.GetX(), cam->look.GetY(), cam->look.GetZ(), &view);

        const float width = screenWidth;
        const float height = screenHeight;

        // Calculate the dir of the rays from to the 4 corners of the selection box
        float dir1[3];
        float dir2[3];
        float dir3[3];
        float dir4[3];
        CalcRay(cam->fovY, width / height, maxx, miny, width, height, dir1);
        CalcRay(cam->fovY, width / height, minx, miny, width, height, dir2);
        CalcRay(cam->fovY, width / height, maxx, maxy, width, height, dir3);
        CalcRay(cam->fovY, width / height, minx, maxy, width, height, dir4);

        Normalize(dir1[0], dir1[1], dir1[2], dir1[0], dir1[1], dir1[2]);
        Normalize(dir2[0], dir2[1], dir2[2], dir2[0], dir2[1], dir2[2]);
        Normalize(dir3[0], dir3[1], dir3[2], dir3[0], dir3[1], dir3[2]);
        Normalize(dir4[0], dir4[1], dir4[2], dir4[0], dir4[1], dir4[2]);

        // Calculate the corners of the frustum
        float corners[24];
        std::memset(corners, 0, sizeof(corners));

        float near_plane_pos[3] = { 0, 0, cam->nearZ };
        float far_plane_pos[3] = { 0, 0, cam->farZ };
        float dirZ[3] = { 0, 0, -1 };

        auto c1 = &corners[0];
        auto c2 = &corners[3];
        auto c3 = &corners[6];
        auto c4 = &corners[9];
        auto c5 = &corners[12];
        auto c6 = &corners[15];
        auto c7 = &corners[18];
        auto c8 = &corners[21];

        float origin[3] = { 0,0,0 };
        RayPlaneTest(origin, dir1, far_plane_pos, dirZ, c1);
        RayPlaneTest(origin, dir2, far_plane_pos, dirZ, c2);
        RayPlaneTest(origin, dir3, far_plane_pos, dirZ, c3);
        RayPlaneTest(origin, dir4, far_plane_pos, dirZ, c4);
        RayPlaneTest(origin, dir1, near_plane_pos, dirZ, c5);
        RayPlaneTest(origin, dir2, near_plane_pos, dirZ, c6);
        RayPlaneTest(origin, dir3, near_plane_pos, dirZ, c7);
        RayPlaneTest(origin, dir4, near_plane_pos, dirZ, c8);

        Frustum frustum;
        CreateFrustum(&frustum, corners);

        Matrix4x4 world(Matrix4x4Type::MATRIX_4X4_IDENTITY);
        Matrix4x4::CreateTransformationMatrix(selectionData->px,
            selectionData->py,
            selectionData->pz,
            selectionData->rx,
            selectionData->ry,
            selectionData->rz,
            selectionData->sx,
            selectionData->sy,
            selectionData->sz,
            &world);

        auto& box = selectionData->box;

        AABB aabb;
        aabb.max[0] = box.maxx;
        aabb.max[1] = box.maxy;
        aabb.max[2] = box.maxz;
        aabb.min[0] = box.minx;
        aabb.min[1] = box.miny;
        aabb.min[2] = box.minz;

        CreateTransformedAABB(world, aabb);

        float sphere[3];
        float radius;
        AABBToSphere(aabb, sphere, &radius);

        Matrix4x4::TransformVector(view, &sphere[0], &sphere[1], &sphere[2]);

        if (FrustumTest(&frustum, sphere, radius))
        {
            IslanderCollisionOBB obb_frustum;
            memcpy(obb_frustum.verticies, corners, sizeof(corners));

            IslanderCollisionOBB obb_obj;
            obb_obj.verticies[0] = box.minx; obb_obj.verticies[1] = box.miny; obb_obj.verticies[2] = box.minz;
            obb_obj.verticies[3] = box.minx; obb_obj.verticies[4] = box.miny; obb_obj.verticies[5] = box.maxz;
            obb_obj.verticies[6] = box.minx; obb_obj.verticies[7] = box.maxy; obb_obj.verticies[8] = box.minz;
            obb_obj.verticies[9] = box.maxx; obb_obj.verticies[10] = box.miny; obb_obj.verticies[11] = box.minz;
            obb_obj.verticies[12] = box.minx; obb_obj.verticies[13] = box.maxy; obb_obj.verticies[14] = box.maxz;
            obb_obj.verticies[15] = box.maxx; obb_obj.verticies[16] = box.maxy; obb_obj.verticies[17] = box.minz;
            obb_obj.verticies[18] = box.maxx; obb_obj.verticies[19] = box.miny; obb_obj.verticies[20] = box.maxz;
            obb_obj.verticies[21] = box.maxx; obb_obj.verticies[22] = box.maxy; obb_obj.verticies[23] = box.maxz;

            for (int i = 0; i < 8; i++)
            {
                Matrix4x4::TransformVector(world, &obb_obj.verticies[i*3], &obb_obj.verticies[i*3+1], &obb_obj.verticies[i*3+2]);
                Matrix4x4::TransformVector(view, &obb_obj.verticies[i*3], &obb_obj.verticies[i*3+1], &obb_obj.verticies[i*3+2]);
            }

            float normal[3];
            return Islander::Collision::GJK(obb_frustum, obb_obj, normal);
        }

        return false;
    }
}
