#include "API.h"
#include "Device.h"
#include "Window.h"
#include "FontEngine.h"
#include "ResourceLoader.h"
#include "SoundSystem.h"
#include "HardwareCursor.h"
#include "Logger.h"
#include "AudioToken.h"
#include "CollisionSystem.h"
#include "RayCast.h"
#include "NavMesh.h"
#include "Polygon.h"
#include "AnimationController.h"
#include "GamePad.h"
#include "DearImgui.h"
#include "ParticleSystem\ParticleSystem.h"
#include "Avoidance.h"
#include "NonRealisticPhysics.h"
#include "TextureExporter.h"

using namespace Islander;
using namespace Islander::Renderer;
using namespace Islander::Sound;
using namespace Islander::Collision;
using namespace Islander::Physics;
using namespace Islander::NavMesh;
using namespace Islander::Avoidance;
using namespace Islander::Numerics;
using namespace Islander::Model;

// Device management
ISLANDER_DEVICE IslanderCreateDevice()
{
	return new Device();
}

void IslanderInitializeDevice(ISLANDER_DEVICE device, ISLANDER_WINDOW window, IslanderFontDescription font)
{
	Islander::Renderer::FontDescription description;
	description.filedef = font.filedef;
	description.img = font.img;
	description.name = font.name;

	((Device*)device)->Init((WINDOW*)window, description);
}

ISLANDER_BINDINGS IslanderCreateBindings(ISLANDER_WORLD world)
{
	return CreateRenderProperty(world);
}

void IslanderRenderScene(ISLANDER_DEVICE device, ISLANDER_WORLD world, ISLANDER_CAMERA camera, ISLANDER_BINDINGS bindings)
{
	((Device*)device)->RenderScene(world, (Islander::Cameraf*)camera, bindings);
}

void IslanderRenderScene3D(ISLANDER_DEVICE device, IslanderRenderablePass* passes, int passCount, IslanderRenderable* renderables)
{
    ((Device*)device)->RenderScene3D(passes, passCount, renderables);
}

void IslanderRenderParticles(ISLANDER_DEVICE device, ISLANDER_CAMERA3D camera)
{
    ((Device*)device)->RenderParticleSystem((Islander::Camera3D*)camera);
}

int IslanderDispatchCompute(ISLANDER_DEVICE device, IslanderComputeWorkload* workload)
{
    return reinterpret_cast<Device*>(device)->DispatchCompute(workload);
}

void IslanderRenderUI(ISLANDER_DEVICE device, CRIMSON_HANDLE crimson)
{
    reinterpret_cast<Device*>(device)->RenderUI(crimson);
}

void IslanderSetUISettings(ISLANDER_DEVICE device, IslanderUISettings* settings)
{
    reinterpret_cast<Device*>(device)->SetUISettings(settings);
}

void IslanderSetUITextSettings(ISLANDER_DEVICE device, IslanderUITextSetting* settings, int numSettings)
{
    reinterpret_cast<Device*>(device)->SetUITextSettings(settings, numSettings);
}

ISLANDER_SOUND_PLAYER IslanderCreateSoundPlayer(ISLANDER_WINDOW window)
{
	return new SoundSystem(window);
}

void* IslanderPlaySound(ISLANDER_SOUND_PLAYER player, int sound, int loopCount)
{
	return ((SoundSystem*)player)->PlaySound(sound, loopCount);
}

void IslanderProcessSoundEventQueue(ISLANDER_SOUND_PLAYER player, IslanderAudioEvent* ev)
{
	return ((SoundSystem*)player)->ProcessEventQueue(/*(SoundEvent*)*/ev);
}

void IslanderCloseSoundPlayer(ISLANDER_SOUND_PLAYER player)
{
	delete (SoundSystem*) player;
}

void IslanderStopSound(ISLANDER_SOUND_PLAYER player, ISLANDER_SOUND_TOKEN token)
{
	((SoundSystem*)player)->StopSound(reinterpret_cast<Token*>(token));
}

void IslanderReleaseToken(ISLANDER_SOUND_PLAYER player, ISLANDER_SOUND_TOKEN token)
{
	((SoundSystem*)player)->ReleaseToken(reinterpret_cast<Token*>(token));
}

int IslanderCreateSoundCategory(ISLANDER_SOUND_PLAYER player)
{
	return ((SoundSystem*)player)->CreateCategory();
}

int IslanderIsSoundPlayerInitialized(ISLANDER_SOUND_PLAYER player)
{
	return ((SoundSystem*)player)->IsInitialized();
}

void IslanderSetCategoryVolume(ISLANDER_SOUND_PLAYER player, int category, float vol)
{
	((SoundSystem*)player)->SetCategoryVolume(category, vol);
}

void IslanderSetSoundVolume(ISLANDER_SOUND_PLAYER player, ISLANDER_SOUND_TOKEN token, float vol)
{
	((SoundSystem*)player)->SetSoundVolume(reinterpret_cast<Token*>(token), vol);
}

void IslanderSetSoundFrequencyRatio(ISLANDER_SOUND_PLAYER player, ISLANDER_SOUND_TOKEN token, float ratio)
{
    ((SoundSystem*)player)->SetSoundFrequencyRatio(reinterpret_cast<Token*>(token), ratio);
}

int IslanderGetTokenState(ISLANDER_SOUND_PLAYER player, ISLANDER_SOUND_TOKEN token)
{
    auto t = (reinterpret_cast<Token*>(token));
    if (((SoundSystem*)player)->IsValidToken(t))
    {
        return t->GetState();
    }

	return TOKEN_STATE_COMPLETED;
}

void IslanderSetPreferredRenderer(ISLANDER_DEVICE device, int renderer)
{
	((Device*)device)->SetPreferredRenderer(renderer);
}

int IslanderGetCurrentRenderer(ISLANDER_DEVICE device)
{
    return ((Device*)device)->GetCurrentRenderer();
}

void IslanderPresent(ISLANDER_DEVICE device)
{
	((Device*)device)->Present();
}

void IslanderGetResolutions(ISLANDER_DEVICE device, IslanderResolutions* resolution)
{
    DisplayResolutions res;
    ((Device*)device)->GetResolutions(res);

    resolution->_count = std::min(res.count, ISLANDER_MAX_RESOLUTIONS);
    for (int i = 0; i < resolution->_count; i++)
    {
        auto element = &resolution->_resolutions[i];
        element->_id = res.resolution[i].id;
        element->_width = res.resolution[i].width;
        element->_height = res.resolution[i].height;
    }
}

void IslanderResizeBackBuffer(ISLANDER_DEVICE device)
{
    ((Device*)device)->ResizeBackBuffer();
}

void IslanderSetDeviceLocalAllocator(ISLANDER_DEVICE device, ISLANDER_ALLOCATOR allocator)
{
    ((Device*)device)->SetLocalAllocator(allocator);
}

void IslanderSetSyncInterval(ISLANDER_DEVICE device, int syncInterval)
{
    ((Device*)device)->SetSyncInterval(syncInterval);
}

void IslanderGetRendererStats(ISLANDER_DEVICE device, IslanderRendererStats* stats)
{
    reinterpret_cast<Device*>(device)->GetRendererStats(*stats);
}

void IslanderProcessPolyLibrary(ISLANDER_DEVICE device, ISLANDER_POLYGON_LIBRARY lib)
{
    PolygonMeshLibrary* library = reinterpret_cast<PolygonMeshLibrary*>(lib);
    reinterpret_cast<Device*>(device)->ProcessPolyLibrary(library);
}

// Resource management
int IslanderLoadPixelShader(ISLANDER_DEVICE device, const char* filename, const char* method)
{
	return ((Device*)device)->LoadPixelShader(filename, method, nullptr);
}

int IslanderLoadPixelShaderEx(ISLANDER_DEVICE device, const char* filename, const char* method, IslanderShaderDefineList* defines)
{
    return ((Device*)device)->LoadPixelShader(filename, method, defines);
}

int IslanderLoadVertexShader(ISLANDER_DEVICE device, const char* filename, const char* method, IslanderShaderSemantic* semantics, int count)
{
	SEMANTIC* list = new SEMANTIC[count];
	for (int i = 0; i < count; i++)
	{
		list[i].desc = semantics[i]._desc;
		list[i].format = semantics[i]._format;
		list[i].stream = semantics[i]._stream;
	}

	int shader = ((Device*)device)->LoadVertexShader(filename, method, list, count, nullptr);
	
	delete[] list;

	return shader;
}

int IslanderLoadVertexShaderEx(ISLANDER_DEVICE device, const char* filename, const char* method, IslanderShaderSemantic* semantics, int count, IslanderShaderDefineList* defines)
{
    SEMANTIC* list = new SEMANTIC[count];
    for (int i = 0; i < count; i++)
    {
        list[i].desc = semantics[i]._desc;
        list[i].format = semantics[i]._format;
        list[i].stream = semantics[i]._stream;
    }

    int shader = ((Device*)device)->LoadVertexShader(filename, method, list, count, defines);

    delete[] list;

    return shader;
}

int IslanderLoadGeometryShaderEx(ISLANDER_DEVICE device, const char* filename, const char* method, IslanderShaderDefineList* defines)
{
    return ((Device*)device)->LoadGeometryShader(filename, method, defines);
}

int IslanderLoadComputeShaderEx(ISLANDER_DEVICE device, const char* filename, const char* method, IslanderShaderDefineList* defines)
{
    return ((Device*)device)->LoadComputeShader(filename, method, defines);
}

int IslanderLoadFont(ISLANDER_DEVICE device, IslanderFontDescription font)
{
	Islander::Renderer::FontDescription description;
	description.filedef = font.filedef;
	description.img = font.img;
	description.name = font.name;

	((Device*)device)->LoadFont(description);
	return description.img;
}

int IslanderLoadSound(ISLANDER_SOUND_PLAYER device, const char* filename, int category)
{
	return ((SoundSystem*)device)->LoadSound(filename, category);
}

int IslanderCreateTextureFromBytes(ISLANDER_DEVICE device, ISLANDER_TEXTURE_DATA data, int width, int height, int size)
{
	Device* d = (Device*)device;
	return d->LoadTexture(data, width, height, size);
}

void IslanderMeasureText(ISLANDER_DEVICE device, IslanderMeasuredText* output)
{
	Device* d = (Device*)device;
	d->MeasureText(output->_font, output->_text, output->_wrap, output->_size, output->_width, output->_height);
}

ISLANDER_TEXTURE_DATA IslanderGetBytesFromTexture(ISLANDER_DEVICE device, int texture, int* width, int* height, int* size)
{
	Device* d = (Device*)device;
	return d->GetBytesFromTexture(texture, width, height, size);
}

void IslanderFreeTextureData(ISLANDER_TEXTURE_DATA data)
{
	delete[] data;
}

// New resource management

ISLANDER_FILE_LIBRARY IslanderCreateFileLibrary()
{
	return new FileLibrary();
}

ISLANDER_RESOURCE_FILE IslanderGetFile(ISLANDER_FILE_LIBRARY lib, const char* name)
{
	return ((FileLibrary*)lib)->GetFile(name);
}

int IslanderAddFile(ISLANDER_FILE_LIBRARY lib, const char* path)
{
	return ((FileLibrary*)lib)->AddFile(path);
}

int IslanderAddDirectory(ISLANDER_FILE_LIBRARY lib, const char* path)
{
	return ((FileLibrary*)lib)->AddDirectory(path);
}

int IslanderAddCompressedFile(ISLANDER_FILE_LIBRARY lib, const char* path)
{
	return ((FileLibrary*)lib)->AddCompressedFile(path);
}

ISLANDER_RESOURCE IslanderCreateResource(ISLANDER_DEVICE device, ISLANDER_RESOURCE_FILE img, ISLANDER_RESOURCE_FILE def)
{
	return ((ResourceLoader2*)((Device*)device)->GetResourceLoader2())->CreateResource((ResourceFile*)img, (ResourceFile*)def);
}

ISLANDER_RESOURCE IslanderCreateResourceFromTexture(ISLANDER_DEVICE device, ISLANDER_RESOURCE_FILE res)
{
    return ((ResourceLoader2*)((Device*)device)->GetResourceLoader2())->CreateResourceFromImage((ResourceFile*)res);
}

ISLANDER_RESOURCE IslanderCreateResourceFromMesh(ISLANDER_DEVICE device, ISLANDER_RESOURCE_FILE res)
{
    return ((ResourceLoader2*)((Device*)device)->GetResourceLoader2())->CreateResourceFromMesh((ResourceFile*)res);
}

ISLANDER_RESOURCE IslanderCreateResourceFromEffect(ISLANDER_DEVICE device, ISLANDER_RESOURCE_FILE res)
{
    return ((ResourceLoader2*)((Device*)device)->GetResourceLoader2())->CreateResourceFromEffect((ResourceFile*)res);
}

int IslanderCreateCubeMapTexture(ISLANDER_DEVICE device, ISLANDER_RESOURCE* resourceArray, int width, int height)
{
    int texture = -1;

    int textures[6];
    for (int i = 0; i < 6; i++)
    {
        Resource* resource = (Resource*)resourceArray[i];
        if (resource->_state == RESOURCE_STATE_LOADED && resource->_class == RESOURCE_CLASS_TEXTURE)
        {
            textures[i] = resource->_textureData._texture;
        }
        else
        {
            return texture;
        }
    }

    texture = ((Device*)device)->CreateCubeMapTexture(textures, width, height);
    return texture;
}

int IslanderCreateTextureArray(ISLANDER_DEVICE device, int count, ISLANDER_RESOURCE* resourceArray, int width, int height)
{
    const int maxTextures = 16;
    int texture = -1;

    if (count >= maxTextures)
    {
        return texture;
    }

    int textures[maxTextures];
    for (int i = 0; i < count; i++)
    {
        Resource* resource = (Resource*)resourceArray[i];
        if (resource->_state == RESOURCE_STATE_LOADED && resource->_class == RESOURCE_CLASS_TEXTURE)
        {
            textures[i] = resource->_textureData._texture;
        }
        else
        {
            return texture;
        }
    }

    texture = ((Device*)device)->CreateTextureArray(count, textures, width, height);
    return texture;
}

void IslanderLoadResource(ISLANDER_DEVICE device, ISLANDER_RESOURCE res)
{
    ((ResourceLoader2*)((Device*)device)->GetResourceLoader2())->LoadResource((Resource*)res);
}

void IslanderReloadResource(ISLANDER_DEVICE device, ISLANDER_RESOURCE res)
{
    ((ResourceLoader2*)((Device*)device)->GetResourceLoader2())->ReloadResource((Resource*)res);
}

void IslanderUnloadResource(ISLANDER_DEVICE device, ISLANDER_RESOURCE res)
{
    ((ResourceLoader2*)((Device*)device)->GetResourceLoader2())->UnloadResource((Resource*)res);
}

void IslanderLoadResourceAsync(ISLANDER_DEVICE device, ISLANDER_RESOURCE res)
{
    ((ResourceLoader2*)((Device*)device)->GetResourceLoader2())->LoadResourceAsync((Resource*)res);
}

int IslanderGetResourceState(ISLANDER_DEVICE device, ISLANDER_RESOURCE res)
{
    Resource* resource = (Resource*)res;
    if (resource)
    {
        return int(resource->_state);
    }
    return -1;
}

int IslanderGetReferencedResourceState(ISLANDER_DEVICE device, ISLANDER_RESOURCE res)
{
    Resource* resource = (Resource*)res;
    if (resource)
    {
        int state = RESOURCE_STATE_LOADED;
        for (auto& sub : resource->subResources)
        {
            if (sub->_state == RESOURCE_STATE_UNLOADED)
            {
                state = RESOURCE_STATE_UNLOADED;
            }
            else if (sub->_state == RESOURCE_STATE_LOADED && state == RESOURCE_STATE_LOADED)
            {
                state = RESOURCE_STATE_UNLOADED;
            }
        }
        return state;
    }
    return -1;
}

void IslanderFindMaterialTexture(ISLANDER_DEVICE device, const char* name, Islander::component_texture* texture)
{
	if (texture)
	{
		component_texture_composite* tex = ((Device*)device)->GetResourceLoader2()->FindMaterialTexture(name);
		if (tex)
		{
			*texture = tex->texture;
		}
	}
}

Islander::component_texture* IslanderFindMaterialSubTexture(ISLANDER_DEVICE device, const char* name, int* tex)
{
	component_texture_composite* texture = ((Device*)device)->GetResourceLoader2()->FindMaterialTexture(name);
	if (texture)
	{
		*tex = texture->subtexturecount;
		return texture->subtexture;
	}

	*tex = 0;
	return nullptr;
}

int IslanderGetTextureSize(ISLANDER_DEVICE device, const char* name, int* w, int* h)
{
	component_texture_composite* texture = ((Device*)device)->GetResourceLoader2()->FindMaterialTexture(name);
	if (texture)
	{
		*w = texture->width;
		*h = texture->height;
		return 1;
	}

	*w = 0;
	*h = 0;
	return 0;
}

int IslanderGetTextureList(ISLANDER_DEVICE device, IslanderTexureListData* list)
{
    Device* d = ((Device*)device);

    TextureListData data;

    if (d->GetTextureList(data))
    {
        list->count = std::min(data.texture_count, ISLANDER_TEXTURE_DATA_MAX_COUNT);
        for (int i = 0; i < list->count; i++)
        {
            list->textures[i].id = data.textures[i].id;
            list->textures[i].width = data.textures[i].width;
            list->textures[i].height = data.textures[i].height;
        }

        return 1;
    }
    else
    {
        return 0;
    }
}

void IslanderGenerateSubTextures(ISLANDER_DEVICE device, const char* name, ISLANDER_SUBRESOURCE_GENERATION generationMethod, ISLANDER_SUBRESOURCE_DATA data)
{
	component_texture_composite* texture = ((Device*)device)->GetResourceLoader2()->FindMaterialTexture(name);
	if (texture)
	{
		if (generationMethod == ISLANDER_SUBRESOURCE_GENERATION_9SLICE)
		{
			Islander9SliceData* s = (Islander9SliceData*)data;

			float x[] = {
				0, s->_minX, s->_minX, s->_maxX, s->_maxX, 1, // 1st row
				0, s->_minX, s->_minX, s->_maxX, s->_maxX, 1, // 2nd row
				0, s->_minX, s->_minX, s->_maxX, s->_maxX, 1, // 3rd row
			};
			float y[] = {
				0, s->_minY, 0, s->_minY, 0, s->_minY, // 1st row
				s->_minY, s->_maxY, s->_minY, s->_maxY, s->_minY, s->_maxY, // 2nd row
				s->_maxY, 1, s->_maxY, 1, s->_maxY, 1, // 3rd row
			};

			const int slices = 9;			
			texture->subtexturecount = slices;

			for (int i = 0; i < slices; i++)
			{
				memcpy(&texture->subtexture[i], &texture->texture, sizeof(component_texture));

				float x1 = x[i * 2] * texture->texture.sx + texture->texture.px;
				float x2 = x[i * 2 + 1] * texture->texture.sx + texture->texture.px;
				float y1 = y[i * 2] * texture->texture.sy + texture->texture.py;
				float y2 = y[i * 2 + 1] * texture->texture.sy + texture->texture.py;

				texture->subtexture[i].px = x1;
				texture->subtexture[i].py = y1;
				texture->subtexture[i].sx = x2 - x1;
				texture->subtexture[i].sy = y2 - y1;
			}
		}
	}
}

void IslanderCreateReferencedResources(ISLANDER_DEVICE device, ISLANDER_RESOURCE res, ISLANDER_FILE_LIBRARY filelib)
{
    auto resource = (Resource*)res;
    auto loader = ((ResourceLoader2*)((Device*)device)->GetResourceLoader2());
    loader->CreateSubResources(resource, (FileLibrary*)filelib);
}

void IslanderLoadReferencedResources(ISLANDER_DEVICE device, ISLANDER_RESOURCE res)
{
    auto resource = (Resource*)res;
    auto loader = ((ResourceLoader2*)((Device*)device)->GetResourceLoader2());
    loader->LoadSubResources(resource);
}

void IslanderLoadReferencedResourcesAsync(ISLANDER_DEVICE device, ISLANDER_RESOURCE res)
{
    auto resource = (Resource*)res;
    auto loader = ((ResourceLoader2*)((Device*)device)->GetResourceLoader2());
    loader->LoadSubResourcesAsync(resource);
}

void IslanderReloadReferencedResources(ISLANDER_DEVICE device, ISLANDER_RESOURCE res)
{
    auto resource = (Resource*)res;
    auto loader = ((ResourceLoader2*)((Device*)device)->GetResourceLoader2());
    loader->ReloadSubResources(resource);
}

ISLANDER_POLYGON_DATA IslanderGetPolyMeshDataFromResource(ISLANDER_RESOURCE res)
{
    auto resource = (Resource*)res;
    if (resource != nullptr && resource->_class == RESOURCE_CLASS_MESH && resource->_state == RESOURCE_STATE_LOADED)
    {
        return resource->_meshData._mesh;
    }

    return nullptr;
}

int IslanderGetEffectData(ISLANDER_DEVICE device, ISLANDER_RESOURCE resource, int type, char** names, int len)
{
    auto r = (Islander::Renderer::Resource*)resource;
    auto loader = ((ResourceLoader2*)((Device*)device)->GetResourceLoader2());
    return loader->GetEffectData(r, type, names, len);
}

int IslanderCreateEmptyTexture(ISLANDER_DEVICE device, int width, int height, int type)
{
    return reinterpret_cast<Device*>(device)->CreateEmptyTexture(width, height, type);
}

void IslanderGenerateComputeTexture(ISLANDER_DEVICE device, int texture)
{
    reinterpret_cast<Device*>(device)->GenerateComputeTexture(texture);
}

int IslanderExportTexture(const char* path, unsigned char* data, int width, int height, int size)
{
    return Islander::Renderer::ExportTexture(path, data, width, height, size);
}

// Renderering
ISLANDER_PASS_LIST IslanderCreatePassList()
{
	return new PASS_LIST();
}

void IslanderSetPassList(ISLANDER_DEVICE device, ISLANDER_PASS_LIST passes)
{
	((Device*)device)->SetPasses(*(PASS_LIST*)passes);
}

void IslanderAppendPassList(ISLANDER_PASS_LIST passes, IslanderPassConfig& config)
{   
    if ((config._flags & ISLANDER_RENDER_FLAGS_MULTI_RENDER_TARGET) == ISLANDER_RENDER_FLAGS_MULTI_RENDER_TARGET)
    {
        ((PASS_LIST*)passes)->AddPass(config._multiRenderTarget, config._renderTargetCount, config._flags, "Default Pass");
    }
    else
    {
        ((PASS_LIST*)passes)->AddPass(config._renderTarget, config._flags, "Default Pass");
    }
}

void IslanderAppendNamedPassList(ISLANDER_PASS_LIST passes, const char* name, IslanderPassConfig& config)
{
    if ((config._flags & ISLANDER_RENDER_FLAGS_MULTI_RENDER_TARGET) == ISLANDER_RENDER_FLAGS_MULTI_RENDER_TARGET)
    {
        ((PASS_LIST*)passes)->AddPass(config._multiRenderTarget, config._renderTargetCount, config._flags, std::string(name));
    }
    else
    {
        ((PASS_LIST*)passes)->AddPass(config._renderTarget, config._flags, std::string(name));
    }
}

void IslanderClearPassList(ISLANDER_PASS_LIST passes)
{
    ((PASS_LIST*)passes)->Clear();
}

void IslanderCreateRenderTarget(ISLANDER_DEVICE device, int width, int height, IslanderRenderTarget* target)
{
	*target = ((Device*)device)->CreateRenderTarget(RenderTargetType::Diffuse, width, height);
}

void IslanderCreateRenderTargetOfType(ISLANDER_DEVICE device, int type, int width, int height, IslanderRenderTarget* target)
{
    *target = ((Device*)device)->CreateRenderTarget((RenderTargetType)type, width, height);
}

void IslanderCreateRenderTargetArrayOfType(ISLANDER_DEVICE device, int type, int width, int height, int dimensions, IslanderRenderTargetArray* target)
{
    *target = ((Device*)device)->CreateRenderTargetArray((RenderTargetType)type, width, height, dimensions);
}

void IslanderResizeRenderTarget(ISLANDER_DEVICE device, IslanderRenderTarget target, int width, int height)
{
    ((Device*)device)->ResizeRenderTarget(target, (uint32_t) width, (uint32_t) height);
}

void IslanderSetPassConstantData(ISLANDER_PASS_LIST passes, int pass, void* constantData, int size)
{
    ((PASS_LIST*)passes)->UpdatePassData(pass, constantData, size);
}

void IslanderInitializeGPUPerformanceCounters(ISLANDER_DEVICE device)
{
    ((Device*)device)->InitializePerformanceCounters();
}

void IslanderBeginGPUProfilingFrame(ISLANDER_DEVICE device)
{
    reinterpret_cast<Device*>(device)->BeginGPUProfilingFrame();
}

int IslanderEndGPUProfilingFrame(ISLANDER_DEVICE device)
{
    return reinterpret_cast<Device*>(device)->EndGPUProfilingFrame();
}

void IslanderBeginGPUProfiling(ISLANDER_DEVICE device)
{
    reinterpret_cast<Device*>(device)->BeginGPUProfiling();
}

int IslanderEndGPUProfiling(ISLANDER_DEVICE device)
{
    return reinterpret_cast<Device*>(device)->EndGPUProfiling();
}

bool IslanderGetGPUProfileFrameData(ISLANDER_DEVICE device, int handle, IslanderGPUProfileFrameData& data)
{
    return reinterpret_cast<Device*>(device)->GetGPUProfileFrameData(handle, data);
}

bool IslanderGetGPUProfileData(ISLANDER_DEVICE device, int handle, IslanderGPUProfileData& data)
{
    return reinterpret_cast<Device*>(device)->GetGPUProfileData(handle, data);
}

void IslanderSetPixelShaderSamplers(ISLANDER_DEVICE device, int pixelShader, int numSamplers, IslanderTextureSampler* samplers)
{
    reinterpret_cast<Device*>(device)->SetPixelShaderSamplers(pixelShader, numSamplers, samplers);
}

void IslanderSetVertexShaderSamplers(ISLANDER_DEVICE device, int vertexShader, int numSamplers, IslanderTextureSampler* samplers)
{
    reinterpret_cast<Device*>(device)->SetVertexShaderSamplers(vertexShader, numSamplers, samplers);
}

// Particles
ISLANDER_PARTICLE_SYSTEM IslanderGetParticleSystem(ISLANDER_DEVICE device)
{
    return ((Islander::Renderer::Device*)device)->GetParticleSystem();
}

void IslanderUpdateParticleSystem(ISLANDER_PARTICLE_SYSTEM system, float delta)
{
    Islander::Renderer::UpdateParticleSystem((Islander::Renderer::ParticleSystem*)system, delta);
}

int IslanderPlayParticleEffect(ISLANDER_PARTICLE_SYSTEM system, ISLANDER_RESOURCE effect, float x, float y, float z)
{
    return Islander::Renderer::PlayParticleEffect((Islander::Renderer::ParticleSystem*)system, ((Resource*)effect)->_effectData._effect, x, y, z);
}

int IslanderParticleEffectAlive(ISLANDER_PARTICLE_SYSTEM system, int id)
{
    return Islander::Renderer::IsParticleEffectAlive((Islander::Renderer::ParticleSystem*)system, id) ? 1 : 0;
}

void IslanderMoveParticleEffect(ISLANDER_PARTICLE_SYSTEM system, int effectId, float x, float y, float z)
{
    Islander::Renderer::MoveParticleEffect((Islander::Renderer::ParticleSystem*)system, effectId, x, y, z);
}

void IslanderSetLocationParticleEffect(ISLANDER_PARTICLE_SYSTEM system, int effectId, float x, float y, float z)
{
    Islander::Renderer::SetLocationParticleEffect((Islander::Renderer::ParticleSystem*)system, effectId, x, y, z);
}

void IslanderSetRotationParticleEffect(ISLANDER_PARTICLE_SYSTEM system, int effectId, float x, float y, float z)
{
    Islander::Renderer::SetRotationParticleEffect((Islander::Renderer::ParticleSystem*)system, effectId, x, y, z);
}

void IslanderSetTargetLocationParticleEffect(ISLANDER_PARTICLE_SYSTEM system, int effectId, float x, float y, float z)
{
    Islander::Renderer::SetTargetLocationParticleEffect((Islander::Renderer::ParticleSystem*)system, effectId, x, y, z);
}

void IslanderStopParticleEffect(ISLANDER_PARTICLE_SYSTEM system, int id)
{
    Islander::Renderer::StopParticleEffect((Islander::Renderer::ParticleSystem*)system, id);
}

void IslanderStopAllParticleEffects(ISLANDER_PARTICLE_SYSTEM system)
{
    Islander::Renderer::StopAllParticleEffects((Islander::Renderer::ParticleSystem*)system);
}

// Windowing
void* IslanderCreateWindow()
{
	return new WINDOW(nullptr);
}

int IslanderPumpWindow(ISLANDER_WINDOW window)
{
	return ((WINDOW*)window)->Process();
}

void IslanderSetWindowSize(ISLANDER_WINDOW window, int width, int height)
{
	((WINDOW*)window)->SetSize(width, height);
}

int IslanderIsKeyDown(ISLANDER_WINDOW window, char key)
{
	return ((WINDOW*)window)->GetInput()->keysdown[key];
}

void IslanderSetKeyUp(ISLANDER_WINDOW window, char key)
{
	((WINDOW*)window)->GetInput()->keysdown[key] = false;
}

int IslanderGetLeftMouseState(ISLANDER_WINDOW window)
{
	return (int)((WINDOW*)window)->GetInput()->lbuttondown;
}

int IslanderGetRightMouseState(ISLANDER_WINDOW window)
{
	return ((WINDOW*)window)->GetInput()->rbuttondown;
}

int IslanderGetMouseWheelState(ISLANDER_WINDOW window)
{
	return ((WINDOW*)window)->GetInput()->mouseWheel;
}

int IslanderWindowWidth(ISLANDER_WINDOW window)
{
	return ((WINDOW*)window)->Width();
}

int IslanderWindowHeight(ISLANDER_WINDOW window)
{
	return ((WINDOW*)window)->Height();
}

int IslanderMouseX(ISLANDER_WINDOW window)
{
	return ((WINDOW*)window)->GetInput()->mousepos.GetX();
}

int IslanderMouseY(ISLANDER_WINDOW window)
{
	return ((WINDOW*)window)->GetInput()->mousepos.GetY();
}

void IslanderSetMousePos(ISLANDER_WINDOW window, int x, int y)
{
    ((WINDOW*)window)->SetMousePos(x, y);
}

void IslanderGetDisplays(ISLANDER_WINDOW window, IslanderDisplays* displays)
{
    Displays d;
    ((WINDOW*)window)->GetDisplays(&d);

    displays->_count = d._count;
    for (int i = 0; i < std::min(ISLANDER_MAX_DISPLAYS, d._count); i++)
    {
        displays->_displays[i]._id = d._displays[i]._id;
        strcpy(displays->_displays[i]._name, d._displays[i]._name);
        displays->_displays[i]._len = strlen(displays->_displays[i]._name);
    }
}

void IslanderSetDisplay(ISLANDER_WINDOW window, int id)
{
    ((WINDOW*)window)->SelectDisplay(id);
}

void IslanderSetWindowIcon(ISLANDER_WINDOW window, const char* data, int width, int height)
{
    ((WINDOW*)window)->SetWindowIcon(data, width, height);
}

void IslanderSetWindowText(ISLANDER_WINDOW window, const char* text)
{
    ((WINDOW*)window)->SetTitle(text);
}

void IslanderSetWindowStyle(ISLANDER_WINDOW window, int style)
{
    ((WINDOW*)window)->SetWindowStyle((Islander::WindowStyle)style);
}

// Camera
ISLANDER_CAMERA IslanderCreateCamera()
{
	return new Cameraf;
}

void IslanderSetCameraTranslation(ISLANDER_CAMERA camera, float x, float y, float z)
{
	((Cameraf*)camera)->SetX(x);
	((Cameraf*)camera)->SetY(y);
	((Cameraf*)camera)->SetZ(z);
}

ISLANDER_CAMERA3D IslanderCreateCamera3D()
{
    auto camera = new Camera3D();
    camera->farZ = 0;
    camera->nearZ = 0;
    camera->fovY = 0;
    camera->aspect = 0;
    camera->orthographic = false;
    return camera;
}

void IslanderSetCamera3DTranslation(ISLANDER_CAMERA3D camera, float x, float y, float z)
{
    auto cam = (Camera3D*)camera;
    cam->pos.SetX(x);
    cam->pos.SetY(y);
    cam->pos.SetZ(z);
}

void IslanderSetCamera3DProjection(ISLANDER_CAMERA3D camera, float nearZ, float farZ, float fovY, float aspect)
{
    auto cam = (Camera3D*)camera;
    cam->nearZ = nearZ;
    cam->farZ = farZ;
    cam->fovY = fovY;
    cam->aspect = aspect;
    cam->orthographic = false;
}

void IslanderSetCamera3DOrthographic(ISLANDER_CAMERA3D camera, float nearZ, float farZ, float width, float height)
{
    auto cam = (Camera3D*)camera;
    cam->nearZ = nearZ;
    cam->farZ = farZ;
    cam->width = width;
    cam->height = height;
    cam->orthographic = true;
}

void IslanderSetCamera3DLookAt(ISLANDER_CAMERA3D camera, float x, float y, float z)
{
    auto cam = (Camera3D*)camera;
    cam->look.SetX(x);
    cam->look.SetY(y);
    cam->look.SetZ(z);
}

// Hardware Cursor
ISLANDER_CURSOR IslanderCreateCursor(ISLANDER_WINDOW window)
{
	return new HardwareCursor((WINDOW*)window);
}

int IslanderLoadCursor(ISLANDER_CURSOR cursor, const char* filename)
{
	return ((HardwareCursor*)cursor)->LoadCursor(filename);
}

void IslanderSetCursor(ISLANDER_CURSOR cursor, int id)
{
	((HardwareCursor*)cursor)->SetCursor(id);
}

void IslanderHideCursor(ISLANDER_CURSOR cursor)
{
	((HardwareCursor*)cursor)->HideCursor();
}

void IslanderShowCursor(ISLANDER_CURSOR cursor)
{
	((HardwareCursor*)cursor)->ShowCursor();
}

void IslanderDestroyCursor(ISLANDER_CURSOR cursor)
{
	delete (HardwareCursor*)cursor;
}

// Polygon data
ISLANDER_POLYGON_LIBRARY IslanderCreatePolyLibrary()
{
    return CreatePolyMeshLibrary();
}

ISLANDER_POLYGON_DATA IslanderAddPolyMeshData(ISLANDER_POLYGON_LIBRARY lib, float* vertexData, int32_t* indexData, int numVerts, int numIndicies, int stride, int createFlags)
{
    PolygonModel* model = new PolygonModel();
    model->animations = nullptr;
    //model->numDiffuseTextures = 0;
    //model->numNormalTextures = 0;
    model->numMaterials = 0;
    model->numMeshes = 1;
    model->numTextureChannels = 0;
    model->meshes[0].transform2 = Matrix4x4(Matrix4x4Type::MATRIX_4X4_IDENTITY);
    model->meshes[0].materialId = 0;

    AddPolyMeshData((Islander::Model::PolygonMeshLibrary*)lib, &model->meshes[0], vertexData, indexData, numVerts, numIndicies, stride, (PolygonMeshMetadata)createFlags);
    AddPolygonModel((Islander::Model::PolygonMeshLibrary*)lib, model, (PolygonMeshMetadata)createFlags);

    return model;
}

ISLANDER_POLYGON_DATA IslanderUpdateMeshData(ISLANDER_POLYGON_LIBRARY lib, ISLANDER_POLYGON_DATA polyData, float* vertexData, int32_t* indexData, int numVerts, int numIndicies, int stride, int createFlags)
{
    Islander::Model::PolygonModel* pd = (Islander::Model::PolygonModel*)polyData;
    int vertexBuffer = pd->meshes[0].vertexBuffer;
    int indexBuffer = pd->meshes[0].indexBuffer;

    UnloadEngineMesh((Islander::Model::PolygonMeshLibrary*)lib, pd, false);
    
    polyData = IslanderAddPolyMeshData(lib, vertexData, indexData, numVerts, numIndicies, stride, createFlags);

    pd = (Islander::Model::PolygonModel*)polyData;
    pd->meshes[0].indexBuffer = indexBuffer;
    pd->meshes[0].vertexBuffer = vertexBuffer;
    pd->reloaded = true;

    return polyData;
}

ISLANDER_POLYGON_DATA IslanderLoadEngineMesh(ISLANDER_POLYGON_LIBRARY lib, unsigned char* data, int count)
{
    return LoadEngineMesh((Islander::Model::PolygonMeshLibrary*)lib, data, count, false);
}

void IslanderUnloadEngineMesh(ISLANDER_POLYGON_LIBRARY lib, ISLANDER_POLYGON_DATA polyData)
{
    UnloadEngineMesh((Islander::Model::PolygonMeshLibrary*)lib, (Islander::Model::PolygonModel*)polyData, true);
}

void IslanderSetDebugBoneMesh(ISLANDER_DEVICE device, ISLANDER_POLYGON_DATA poly, int vertexShader, int pixelShader)
{
    auto d = (Device*)device;
    d->SetDebugBoneMesh((Islander::Model::PolygonModel*)poly, vertexShader, pixelShader);
}

void IslanderGetPolyMeshData(ISLANDER_POLYGON_DATA poly, IslanderPolygonData* data)
{
    auto polygon = (Islander::Model::PolygonModel*)poly;
    
    data->skinned = polygon->animations != nullptr && polygon->meshes[0].skeleton != nullptr;
    data->textureChannels = polygon->numTextureChannels;

    memcpy(data->min, polygon->min, sizeof(polygon->min));
    memcpy(data->max, polygon->max, sizeof(polygon->max));
}

int IslanderGetPolyMeshMaterials(ISLANDER_POLYGON_DATA poly)
{
    auto polygon = (Islander::Model::PolygonModel*)poly;
    return polygon->numMaterials;
}

int IslanderGetPolyMeshTextures(ISLANDER_POLYGON_DATA poly, int material, int type, char** textureNames, int len)
{
    auto polygon = (Islander::Model::PolygonModel*)poly;

    std::array<fixed_string, 4>* data;
    int count = 0;
    if (type == ISLANDER_POLYDATA_TEXTURE_DIFFUSE)
    {
        count = polygon->materials[material].numDiffuseTextures;
        data = &polygon->materials[material].diffuseTexture;
    }
    else if (type == ISLANDER_POLYDATA_TEXTURE_NORMAL)
    {
        count = polygon->materials[material].numNormalTextures;
        data = &polygon->materials[material].normalTexture;
    }

    if (textureNames != nullptr)
    {
        for (int i = 0; i < count; i++)
        {
            fixed_string& str = data->at(i);
            int strlength = std::min(len, str.len);
            memcpy(textureNames[i], str.data, strlength);
            textureNames[i][strlength] = '\0';
        }
    }

    return count;
}

void IslanderSetPolyMeshNumMaterials(ISLANDER_POLYGON_DATA poly, int numMaterials, int numTextureChannels)
{
    auto polygon = (Islander::Model::PolygonModel*)poly;
    polygon->numMaterials = numMaterials;
    polygon->numTextureChannels = numTextureChannels;
}

void IslanderSetPolyMeshNumTextures(ISLANDER_POLYGON_DATA poly, int material, int type, int numTextures)
{
    auto polygon = (Islander::Model::PolygonModel*)poly;

    if (type == ISLANDER_POLYDATA_TEXTURE_DIFFUSE)
    {
        polygon->materials[material].numDiffuseTextures = numTextures;
    }
    else if (type == ISLANDER_POLYDATA_TEXTURE_NORMAL)
    {
        polygon->materials[material].numNormalTextures = numTextures;
    }
}

void IslanderSetPolyMeshTexture(ISLANDER_POLYGON_DATA poly, int material, int type, int index, const char* textureName, int len)
{
    auto polygon = (Islander::Model::PolygonModel*)poly;

    fixed_string* data = nullptr;
    if (type == ISLANDER_POLYDATA_TEXTURE_DIFFUSE)
    {
        data = &polygon->materials[material].diffuseTexture[index];
    }
    else if (type == ISLANDER_POLYDATA_TEXTURE_NORMAL)
    {
        data = &polygon->materials[material].normalTexture[index];
    }

    if (data)
    {
        len = std::min(FIXED_STRING_LENGTH, (unsigned int)len);
        data->len = len;
        std::memcpy(data->data, textureName, len);
        data->data[len] = '\0';
    }
}

void IslanderLinkPolyMeshTextures(ISLANDER_DEVICE device, ISLANDER_POLYGON_DATA poly)
{
    auto polygon = (Islander::Model::PolygonModel*)poly;
    auto res = reinterpret_cast<Device*>(device)->GetResourceLoader2();

    for (int i = 0; i < polygon->numMaterials; i++)
    {
        for (int j = 0; j < polygon->materials[i].numDiffuseTextures; j++)
        {
            std::string name = polygon->materials[i].diffuseTexture[j].data;
            auto texture = res->FindMaterialTexture(name);
            if (texture != nullptr)
            {
                polygon->materials[i].diffuseIndex[j] = texture->texture.index;
            }
        }
    }
}

// Animations

ISLANDER_ANIMATION_SYSTEM IslanderCreateAnimationSystem()
{
    return CreateAnimationSystem();
}

void IslanderDestroyAnimationSystem(ISLANDER_ANIMATION_SYSTEM system)
{
    DestroyAnimationSystem((Islander::Model::AnimationSystem*)system);
}

int IslanderCreateAnimationController(ISLANDER_ANIMATION_SYSTEM system)
{
    return CreateAnimationController((Islander::Model::AnimationSystem*)system);
}

void IslanderDestroyAnimationController(ISLANDER_ANIMATION_SYSTEM system, int controller)
{
    DestroyAnimationController((Islander::Model::AnimationSystem*)system, controller);
}

void IslanderPlayAnimation(ISLANDER_ANIMATION_SYSTEM system, int controller, int id, int loop)
{
    PlayAnimation((Islander::Model::AnimationSystem*)system, controller, id, loop == 1);
}

void IslanderPauseAnimation(ISLANDER_ANIMATION_SYSTEM system, int controller)
{
    PauseAnimation((Islander::Model::AnimationSystem*)system, controller);
}

void IslanderResetAnimation(ISLANDER_ANIMATION_SYSTEM system, int controller)
{
    ResetAnimation((Islander::Model::AnimationSystem*)system, controller);
}

int IslanderGetAnimationId(ISLANDER_POLYGON_DATA poly, const char* name)
{
    auto polygon = (Islander::Model::PolygonModel*)poly;
    return FindAnimationId(polygon, name);
}

float IslanderGetAnimationLength(ISLANDER_POLYGON_DATA poly, int id)
{
    auto polygon = (Islander::Model::PolygonModel*)poly;
    return GetAnimationLength(polygon, id);
}

int IslanderGetAnimationCount(ISLANDER_POLYGON_DATA poly)
{
    auto polygon = (Islander::Model::PolygonModel*)poly;
    return GetAnimationCount(polygon);
}

int IslanderGetAnimationName(ISLANDER_POLYGON_DATA poly, int id, char* name)
{
    auto polygon = (Islander::Model::PolygonModel*)poly;
    return GetAnimationName(polygon, id, name);
}

float IslanderAnimationControllerElapsed(ISLANDER_ANIMATION_SYSTEM system, int controller)
{
    return ((Islander::Model::AnimationSystem*)system)->controllers[controller].animation.elapsedTime;
}

void IslanderAnimationUpdate(ISLANDER_ANIMATION_SYSTEM system, float deltaTime)
{
    RunAnimationSystem((Islander::Model::AnimationSystem*)system, deltaTime);
}

void IslanderSetAnimationLoop(ISLANDER_ANIMATION_SYSTEM system, int controller, int loop)
{
    ((Islander::Model::AnimationSystem*)system)->controllers[controller].animation.loop = loop == 1;
}

void IslanderSetAnimationSpeed(ISLANDER_ANIMATION_SYSTEM system, int controller, float speedScale)
{
    SetAnimationSpeed((Islander::Model::AnimationSystem*)system, controller, speedScale);
}

void IslanderBlendToAnimation(ISLANDER_ANIMATION_SYSTEM system, int controller, int id, float blendTime)
{
    BlendToAnimation((Islander::Model::AnimationSystem*)system, controller, id, blendTime);
}

void IslanderPlayBlendedAnimation(ISLANDER_ANIMATION_SYSTEM system, int controller, int animationId1, int animationId2, uint64_t mask, int loop1, int loop2)
{
    PlayBlendedAnimation((Islander::Model::AnimationSystem*)system, controller, animationId1, animationId2, mask, loop1 == 1, loop2 == 1);
}

void IslanderCalculateAnimationPose(ISLANDER_ANIMATION_SYSTEM system, IslanderAnimationPose* pose, float* pos)
{
    Matrix4x4 parent(Matrix4x4Type::MATRIX_4X4_IDENTITY);
    auto sys = (Model::AnimationSystem*)pose->animationSystem;

    float rot[3] = {
        pose->rot[0],
        pose->rot[1],
        pose->rot[2]
    };
    float scale[3] = {
        pose->scale[0],
        pose->scale[1],
        pose->scale[2]
    };
    float displacement[3] = {};
    auto& controller = sys->controllers[pose->controllerId];
    if ((controller.animation.isRootMotion || controller.blend.isRootMotion) && pose->polydata != nullptr)
    {
        auto parent_poly = (PolygonModel*)pose->polydata;

        CalculateRootMotion(parent_poly, sys, pose->controllerId, false, rot, scale, displacement);
    }

    Matrix4x4::CreateTransformationMatrix(
        pose->pos[0] - displacement[0],
        pose->pos[1] - displacement[1],
        pose->pos[2] - displacement[2],
        pose->rot[0],
        pose->rot[1],
        pose->rot[2],
        pose->scale[0],
        pose->scale[1],
        pose->scale[2],
        &parent);

    // Find parent bone transform if required.
    if (pose->bone != -1 && pose->polydata != nullptr)
    {
        auto parent_poly = (PolygonModel*)pose->polydata;

        CalculateTransformForPose(parent_poly, sys, pose->controllerId, pose->bone, parent);
    }

    pos[0] = 0.f;
    pos[1] = 0.f;
    pos[2] = 0.f;
    Matrix4x4::TransformVector(parent, &pos[0], &pos[1], &pos[2]);
}

void IslanderSetAnimationTimeBounds(ISLANDER_ANIMATION_SYSTEM system, int controller, float startTime, float endTime)
{
    auto sys = reinterpret_cast<Model::AnimationSystem*>(system);
    SetAnimationTimeBounds(sys, controller, startTime, endTime);
}

void IslanderSetRootMotionAnimation(ISLANDER_ANIMATION_SYSTEM system, int controller, int isRootMotion, int rootMotionFlags)
{
    auto sys = reinterpret_cast<Model::AnimationSystem*>(system);
    SetAnimationRootMotion(sys, controller, isRootMotion == ISLANDER_ANIMATION_ROOT_MOTION_ON, rootMotionFlags);
}

// Collision system
ISLANDER_COLLISION_SYSTEM IslanderCreateCollisionSystem()
{
    return CreateNonRealisticPhysicsSystem(); //CreateCollisionSystem();
}

ISLANDER_COLLISION_STATIC_MESH IslanderAddStaticMesh(ISLANDER_COLLISION_SYSTEM system, IslanderCollisionStaticMeshInfo* mesh)
{
    CollisionStaticMesh* static_mesh = nullptr;
    CollisionTransform transform;

    transform.px = mesh->px;
    transform.py = mesh->py;
    transform.pz = mesh->pz;

    transform.rx = mesh->rx;
    transform.ry = mesh->ry;
    transform.rz = mesh->rz;

    transform.sx = mesh->sx;
    transform.sy = mesh->sy;
    transform.sz = mesh->sz;

    if (mesh->heightmapCollider)
    {
        if (AddHeightmap((NonRealisticPhysicsSystem*)system, mesh->rayonly == ISLANDER_COLLISION_IS_RAY_ONLY, (Islander::Model::Heightmap*)mesh->heightmap, transform, &static_mesh))
        {
            static_mesh->trigger = mesh->trigger == 1;
        }
    }
    else if (AddStaticMesh((NonRealisticPhysicsSystem*)system, mesh->rayonly == ISLANDER_COLLISION_IS_RAY_ONLY, mesh->triangleCollider == ISLANDER_COLLISION_IS_TRIANGLE_COLLIDER,
        mesh->material, (PolygonModel*)mesh->meshData, transform, &static_mesh))
    {
        static_mesh->box.minx = mesh->box.minx;
        static_mesh->box.maxx = mesh->box.maxx;
        static_mesh->box.miny = mesh->box.miny;
        static_mesh->box.maxy = mesh->box.maxy;
        static_mesh->box.minz = mesh->box.minz;
        static_mesh->box.maxz = mesh->box.maxz;

        static_mesh->trigger = mesh->trigger == 1;
    }

    return static_mesh;
}

void IslanderUpdateStaticMesh(ISLANDER_COLLISION_SYSTEM system, ISLANDER_COLLISION_DYNAMIC_MESH mesh, IslanderCollisionStaticMeshInfo* meshInfo)
{
    UpdateStaticMesh((NonRealisticPhysicsSystem*)system, (CollisionStaticMesh*)mesh, meshInfo);
}

void IslanderOverrideStaticMeshOBB(ISLANDER_COLLISION_DYNAMIC_MESH mesh, float* verts)
{
    OverrideStaticMeshOBB((CollisionStaticMesh*)mesh, verts);
}

ISLANDER_COLLISION_DYNAMIC_MESH IslanderAddDynamicMesh(ISLANDER_COLLISION_SYSTEM system, IslanderCollisionDynamicMeshInfo* mesh)
{
    CollisionDynamicMesh* dynamic_mesh = nullptr;
    if (AddDynamicMesh((NonRealisticPhysicsSystem*)system, mesh->rayonly == ISLANDER_COLLISION_IS_RAY_ONLY, &dynamic_mesh))
    {
        dynamic_mesh->info = *mesh;
        dynamic_mesh->meshData = (Islander::Model::PolygonModel*) mesh->meshData;
        dynamic_mesh->parentMesh = (CollisionDynamicMesh*)mesh->parentMesh;
    }

    return dynamic_mesh;
}

void IslanderUpdateDynamicMesh(ISLANDER_COLLISION_DYNAMIC_MESH mesh, IslanderCollisionDynamicMeshInfo* info)
{
    auto dynamic_mesh = (CollisionDynamicMesh*)mesh;

    dynamic_mesh->info = *info;
    dynamic_mesh->meshData = (Islander::Model::PolygonModel*)info->meshData;
    dynamic_mesh->parentMesh = (CollisionDynamicMesh*)info->parentMesh;
}

void IslanderRemoveStaticMesh(ISLANDER_COLLISION_SYSTEM system, ISLANDER_COLLISION_STATIC_MESH mesh)
{
    RemoveStaticMesh((NonRealisticPhysicsSystem*)system, (CollisionStaticMesh*)mesh);
}

void IslanderRemoveDynamicMesh(ISLANDER_COLLISION_SYSTEM system, ISLANDER_COLLISION_DYNAMIC_MESH mesh)
{
    RemoveDynamicMesh((NonRealisticPhysicsSystem*)system, (CollisionDynamicMesh*)mesh);
}

void IslanderRunCollision(ISLANDER_COLLISION_SYSTEM system, float delta)
{
    RunNonRealisticPhysics((NonRealisticPhysicsSystem*)system, delta);
}

void IslanderGetCollisionInfo(ISLANDER_COLLISION_DYNAMIC_MESH mesh, IslanderCollisionData* data)
{
    auto dynamic_mesh = (CollisionDynamicMesh*)mesh;
    *data = dynamic_mesh->colData;

    //for (int i = 0; i < dynamic_mesh->collider_dynamic_count; i++)
    //{
    //    data->dynamic_mesh[i] = (ISLANDER_COLLISION_DYNAMIC_MESH*)dynamic_mesh->collider_dynamic[i];
    //}

    //for (int i = 0; i < dynamic_mesh->collider_static_count; i++)
    //{
    //    data->static_mesh[i] = (ISLANDER_COLLISION_STATIC_MESH*)dynamic_mesh->collider_static[i];
    //}

    //data->dynamic_mesh_count = dynamic_mesh->collider_dynamic_count;
    //data->static_mesh_count = dynamic_mesh->collider_static_count;

    data->px = dynamic_mesh->info.px;
    data->py = dynamic_mesh->info.py;
    data->pz = dynamic_mesh->info.pz;

    data->rx = dynamic_mesh->info.rx;
    data->ry = dynamic_mesh->info.ry;
    data->rz = dynamic_mesh->info.rz;

    data->platform = dynamic_mesh->platformMesh != nullptr;
}

void IslanderRayCast(ISLANDER_COLLISION_SYSTEM system, ISLANDER_CAMERA3D camera, float px, float py, float pz, float dx, float dy, float dz, int group, IslanderCollisionRayInfo* info)
{
    RayCast((NonRealisticPhysicsSystem*)system, (Camera3D*)camera, px, py, pz, dx, dy, dz, group, info);
}

void IslanderRayCastFine(ISLANDER_COLLISION_SYSTEM system, ISLANDER_CAMERA3D camera, float px, float py, float pz, float dx, float dy, float dz, IslanderCollisionRayInfo* info)
{
    RayCastFine((NonRealisticPhysicsSystem*)system, (Camera3D*)camera, px, py, pz, dx, dy, dz, info);
}

void IslanderRayCastHeightmap(ISLANDER_COLLISION_SYSTEM system, ISLANDER_CAMERA3D camera, float px, float py, float pz, float dx, float dy, float dz, IslanderCollisionRayInfo* info)
{
    RayCastHeightmap((NonRealisticPhysicsSystem*)system, (Camera3D*)camera, px, py, pz, dx, dy, dz, info);
}

void IslanderTransformRay(ISLANDER_CAMERA3D camera, float *px, float *py, float *pz, float *dx, float *dy, float *dz)
{
    TransformRay((Camera3D*)camera, px, py, pz, dx, dy, dz);
}

float IslanderCollisionSweep(ISLANDER_COLLISION_SYSTEM system, ISLANDER_COLLISION_STATIC_MESH mesh, float* vector)
{
    return CollisionSweep((NonRealisticPhysicsSystem*)system, (CollisionStaticMesh*)mesh, vector);
}

void IslanderCollisionOverlap(ISLANDER_COLLISION_SYSTEM system, ISLANDER_COLLISION_DYNAMIC_MESH mesh, IslanderCollisionOverlapInfo* info)
{
    return CollisionOverlap((NonRealisticPhysicsSystem*)system, (CollisionDynamicMesh*)mesh, info);
}

void IslanderCreateDebugCollisionGeometry(ISLANDER_COLLISION_SYSTEM system, IslanderCollisionGeometryData* gData)
{
    CreateDebugGeometry((NonRealisticPhysicsSystem*)system, gData);
}

void IslanderCreateDebugCollisionGeometryDynamicMesh(ISLANDER_COLLISION_SYSTEM system, ISLANDER_COLLISION_DYNAMIC_MESH mesh, IslanderCollisionGeometryData* gData)
{
    CreateDebugGeometry((NonRealisticPhysicsSystem*)system, (CollisionDynamicMesh*)mesh, gData);
}

void IslanderCollisionQuery(ISLANDER_COLLISION_SYSTEM system, IslanderCollisionQueryInfo* info)
{
    QueryPhysicsSystem((NonRealisticPhysicsSystem*)system, info);
}

// Navigation

ISLANDER_NAVIGATION IslanderCreateNavigation()
{
    return NavMeshCreateNavigation();
}

void IslanderNavAddRegion(ISLANDER_NAVIGATION nav, IslanderNavMeshRegion* region)
{
    NavMeshRegion r;
    r.polymesh = (PolygonModel*) region->polymesh;
    r.px = region->px;
    r.py = region->py;
    r.pz = region->pz;
    r.rx = region->rx;
    r.ry = region->ry;
    r.rz = region->rz;
    r.sx = region->sx;
    r.sy = region->sy;
    r.sz = region->sz;

    NavMeshAddRegion((Navigation*)nav, r);
}

void IslanderNavGenerate(ISLANDER_NAVIGATION nav, IslanderNavMeshConfig* cfg)
{
    NavMeshConfig config;
    config.ch = cfg->ch;
    config.cs = cfg->cs;
    config.detailSampleDist = cfg->detailSampleDist;
    config.detailSampleMaxError = cfg->detailSampleMaxError;
    config.maxEdgeLen = cfg->maxEdgeLen;
    config.maxSimplificationError = cfg->maxSimplificationError;
    config.maxVertsPerPoly = cfg->maxVertsPerPoly;
    config.mergeRegionArea = cfg->mergeRegionArea;
    config.minRegionArea = cfg->minRegionArea;
    config.walkableClimb = cfg->walkableClimb;
    config.walkableHeight = cfg->walkableHeight;
    config.walkableRadius = cfg->walkableRadius;
    config.walkableSlopeAngle = cfg->walkableSlopeAngle;

    NavMeshGenerate((Navigation*)nav, config);
}

void IslanderNavClearRegions(ISLANDER_NAVIGATION nav)
{
    NavMeshClearRegions((Navigation*)nav);
}

void IslanderNavDestroy(ISLANDER_NAVIGATION nav)
{
    NavMeshDestroy((Navigation*)nav);
}

void IslanderNavGetData(ISLANDER_NAVIGATION nav, IslanderNavMeshData* data)
{
    auto navigation = (Navigation*)nav;

    data->indexCount = navigation->data.indexCount;
    data->vertexCount = navigation->data.vertexCount;
    data->indexData = navigation->data.indexData;
    data->vertexData = navigation->data.vertexData;
    data->navData = navigation->data.navData;
    data->navCount = navigation->data.navCount;
}

void IslanderNavLoad(ISLANDER_NAVIGATION nav, unsigned char* data, int count)
{
    auto navigation = (Navigation*)nav;
    NavLoad(navigation, data, count);
}

int IslanderNavCalculatePath(ISLANDER_NAVIGATION nav, IslanderNavPath* path)
{
    auto navigation = (Navigation*)nav;

    return NavCalculatePath(navigation, path);
}

int IslanderNavGetClosestPoly(ISLANDER_NAVIGATION nav, IslanderNavClosestPoly* closest)
{
    auto navigation = (Navigation*)nav;

    return NavGetClosestPoly(navigation, closest) ? 1 : 0;
}

bool IslanderNavFindPointsWithinCircle(ISLANDER_NAVIGATION nav, float* pt, float radius, float* points, int max_count, int* count)
{
    auto navigation = reinterpret_cast<Navigation*>(nav);

    return FindPointsWithinCircle(navigation, pt, radius, points, max_count, count);
}

void IslanderNavSetPosition(ISLANDER_NAVIGATION nav, float* pos)
{
    auto navigation = reinterpret_cast<Navigation*>(nav);

    SetNavMeshPosition(navigation, pos);
}

int IslanderNavRayCast(ISLANDER_NAVIGATION nav, float* pos, float* end, float* t)
{
    auto navigation = reinterpret_cast<Navigation*>(nav);

    return NavRayCast(navigation, pos, end, t);
}

// Navigation avoidance
ISLANDER_AVOIDANCE_SYSTEM IslanderCreateNavigationAvoidance()
{
    return CreateAvoidance();
}

void IslanderInitAvoidance(ISLANDER_AVOIDANCE_SYSTEM avoidance, ISLANDER_NAVIGATION nav)
{
    AvoidanceManager* manager = reinterpret_cast<AvoidanceManager*>(avoidance);
    InitAvoidance(manager, reinterpret_cast<Navigation*>(nav));
}

void IslanderResetAvoidance(ISLANDER_AVOIDANCE_SYSTEM avoidance)
{
    AvoidanceManager* manager = reinterpret_cast<AvoidanceManager*>(avoidance);
    ResetAvoidance(manager);
}

ISLANDER_AVOIDANCE_AGENT IslanderAddAvoidanceAgent(ISLANDER_AVOIDANCE_SYSTEM avoidance, const IslanderAvoidanceAgentConfig& config)
{
    AvoidanceManager* manager = reinterpret_cast<AvoidanceManager*>(avoidance);
    return AddAvoidanceAgent(manager, config);
}

void IslanderRemoveAvoidanceAgent(ISLANDER_AVOIDANCE_SYSTEM avoidance, ISLANDER_AVOIDANCE_AGENT id)
{
    AvoidanceManager* manager = reinterpret_cast<AvoidanceManager*>(avoidance);
    RemoveAvoidanceAgent(manager, id);
}

void IslanderUpdateAvoidance(ISLANDER_AVOIDANCE_SYSTEM avoidance, float dt)
{
    AvoidanceManager* manager = reinterpret_cast<AvoidanceManager*>(avoidance);
    UpdateAvoidance(manager, dt);
}

void IslanderApplyAvoidanceAgentPhysics(ISLANDER_AVOIDANCE_SYSTEM avoidance, ISLANDER_AVOIDANCE_AGENT agent, float* pos)
{
    AvoidanceManager* manager = reinterpret_cast<AvoidanceManager*>(avoidance);
    ApplyAvoidanceAgentPhysics(manager, agent, pos);
}

void IslanderUpdateAvoidanceAgentDesiredVelocity(ISLANDER_AVOIDANCE_SYSTEM avoidance, ISLANDER_AVOIDANCE_AGENT agent, float* desiredVelocity)
{
    AvoidanceManager* manager = reinterpret_cast<AvoidanceManager*>(avoidance);
    UpdateAvoidanceAgentDesiredVelocity(manager, agent, desiredVelocity);
}

void IslanderClearAvoidanceAgentTarget(ISLANDER_AVOIDANCE_SYSTEM avoidance, ISLANDER_AVOIDANCE_AGENT agent)
{
    AvoidanceManager* manager = reinterpret_cast<AvoidanceManager*>(avoidance);
    ClearAvoidanceAgentTarget(manager, agent);
}

void IslanderUpdateAvoidanceAgentTarget(ISLANDER_AVOIDANCE_SYSTEM avoidance, ISLANDER_AVOIDANCE_AGENT agent, float* desiredTarget)
{
    AvoidanceManager* manager = reinterpret_cast<AvoidanceManager*>(avoidance);
    UpdateAvoidanceAgentTarget(manager, agent, desiredTarget);
}

void IslanderGetAvoidanceAgentData(ISLANDER_AVOIDANCE_SYSTEM avoidance, ISLANDER_AVOIDANCE_AGENT agent, IslanderAvoidanceAgentData* agentData)
{
    AvoidanceManager* manager = reinterpret_cast<AvoidanceManager*>(avoidance);
    GetAvoidanceAgentData(manager, agent, agentData);
}

void IslanderSetAvoidanceAgentActive(ISLANDER_AVOIDANCE_SYSTEM avoidance, ISLANDER_AVOIDANCE_AGENT agent, bool active)
{
    AvoidanceManager* manager = reinterpret_cast<AvoidanceManager*>(avoidance);
    SetAvoidanceAgentActive(manager, agent, active);
}

void IslanderUpdateAvoidanceAgentFlags(ISLANDER_AVOIDANCE_SYSTEM avoidance, ISLANDER_AVOIDANCE_AGENT agent, int flags)
{
    AvoidanceManager* manager = reinterpret_cast<AvoidanceManager*>(avoidance);
    UpdateAvoidanceAgentFlags(manager, agent, flags);
}

void IslanderAddAvoidanceType(ISLANDER_AVOIDANCE_SYSTEM avoidance, int idx, IslanderAvoidanceType* type)
{
    AvoidanceManager* manager = reinterpret_cast<AvoidanceManager*>(avoidance);
    AddAvoidanceType(manager, idx, type);
}

// Game controllers
ISLANDER_GAMEPAD_DEVICE IslanderCreateGamePadDevice()
{
    return CreateGamePadDevice();
}

void IslanderDestroyGamePadDevice(ISLANDER_GAMEPAD_DEVICE device)
{
    DestroyGamePadDevice((Islander::GamePadDevice*)device);
}

int IslanderIsGamePadSupported()
{
    return IsGamePadSupported();
}

int IslanderGamePadDetected(ISLANDER_GAMEPAD_DEVICE device)
{
    return IsGamePadDetected((Islander::GamePadDevice*)device);
}

void IslanderUpdateGamePadDevice(ISLANDER_GAMEPAD_DEVICE device)
{
    UpdateGamePadDevice((Islander::GamePadDevice*)device);
}

ISLANDER_GAMEPAD_STATE IslanderGetGamepadState(ISLANDER_GAMEPAD_DEVICE device, int gamepad)
{
    if (gamepad < 0 || gamepad >= ISLANDER_GAMEPAD_COUNT)
    {
        return nullptr;
    }

    return &((Islander::GamePadDevice*)device)->GamePads[gamepad].state;
}

void IslanderGamePadRumble(ISLANDER_GAMEPAD_DEVICE device, int gamepad, float right, float left)
{
    GamePadRumble((Islander::GamePadDevice*)device, gamepad, right, left);
}

// IMGUI

void IslanderImguiCreate(ISLANDER_DEVICE device, IslanderImguiContext* context)
{
    //Imgui::RestoreContext((ImGuiContext*)context->imgui_context, (ImGuiMemAllocFunc)context->alloc_func, (ImGuiMemFreeFunc)context->free_func, context->user_data);
    Imgui::RestoreContext(context);
    Imgui::CreateIMGUI((Device*)device);
}

void IslanderImguiRender(ISLANDER_DEVICE device, IslanderImguiContext* context)
{
    //Imgui::RestoreContext((ImGuiContext*)context->imgui_context, (ImGuiMemAllocFunc)context->alloc_func, (ImGuiMemFreeFunc)context->free_func, context->user_data);
    Imgui::RestoreContext(context);
    Imgui::Render((Device*)device);
}

void IslanderImguiNewFrame(ISLANDER_DEVICE device, IslanderImguiContext* context)
{
    //Imgui::RestoreContext((ImGuiContext*)context->imgui_context, (ImGuiMemAllocFunc)context->alloc_func, (ImGuiMemFreeFunc)context->free_func, context->user_data);
    Imgui::RestoreContext(context);
    Imgui::NewFrame((Device*)device);
}

void IslanderImguiImage(ISLANDER_DEVICE device, IslanderImguiContext* context, int texture, float width, float height)
{
    //Imgui::RestoreContext((ImGuiContext*)context->imgui_context, (ImGuiMemAllocFunc)context->alloc_func, (ImGuiMemFreeFunc)context->free_func, context->user_data);
    Imgui::RestoreContext(context);
    Imgui::Image((Device*)device, texture, width, height);
}

void IslanderImguiImagePos(ISLANDER_DEVICE device, IslanderImguiContext* context, int texture, float x, float y, float width, float height)
{
    //Imgui::RestoreContext((ImGuiContext*)context->imgui_context, (ImGuiMemAllocFunc)context->alloc_func, (ImGuiMemFreeFunc)context->free_func, context->user_data);
    Imgui::RestoreContext(context);
    Imgui::Image((Device*)device, texture, x, y, width, height);
}

int IslanderImguiImageButton(ISLANDER_DEVICE device, IslanderImguiContext* context, int id, int texture, float x, float y, float sx, float sy)
{
    //Imgui::RestoreContext((ImGuiContext*)context->imgui_context, (ImGuiMemAllocFunc)context->alloc_func, (ImGuiMemFreeFunc)context->free_func, context->user_data);
    Imgui::RestoreContext(context);
    return Imgui::ImageButton((Device*)device, id, texture, x, y, sx, sy) ? 1 : 0;
}

void IslanderImguiDestroy()
{
    
}

// Logging
void IslanderLog(const char* logMessage, int levels)
{
	Logger::GetLogger()->Log(logMessage, (LoggingChannels)levels);
}

void IslanderSetLogLevels(int levels)
{
	Logger::GetLogger()->SetLoggingFlags(levels);
}
