#include "Zloader.h"

#define MIN(a,b) a < b ? a : b

using Islander::Zloader;
using Islander::ZFile;

Zloader::Zloader(const char* filename)
{
    file = unzOpen(filename);
    GetInfo();
}

Zloader::Zloader(const std::string& filename)
{
    file = unzOpen(filename.c_str());
    GetInfo();
}

Zloader::~Zloader(void)
{
    unzClose(file);
}

void Zloader::GetInfo()
{
    if (unzGoToFirstFile(file) < 0)
        return;
    
    char filename[128];

    do
    {
        unz_file_info info;
        unzGetCurrentFileInfo(file, &info, nullptr, 0, nullptr, 0, nullptr, 0);
        
        //TODO: can we assume this is a file not a directory?

        int len = MIN(128, info.size_filename);
        unzGetCurrentFileInfo(file, nullptr, filename, len, nullptr, 0, nullptr, 0);
        filename[len] = '\0';

        ZFile f;
        f.data = nullptr;
        f.filename = filename;
        f.compressed_size = info.compressed_size;
        f.size = info.uncompressed_size;

        map.insert(std::pair<std::string, ZFile>(filename, f));

    } while (unzGoToNextFile(file) == UNZ_OK);
}

bool Zloader::Decompress(const std::string& filename, ZFile& zfile)
{
    if (unzLocateFile(file, filename.c_str(), 2) == UNZ_OK)
    {
        zfile = GetFile(filename);
        zfile.data = new char[zfile.size];

        unzOpenCurrentFile(file);

        int r = unzReadCurrentFile(file, zfile.data, zfile.size);
        //if (r >= 0)
        //{
        //	map[zfile.filename] = zfile;
        //}

        unzCloseCurrentFile(file);
        return true;
    }

    return false;
}

bool Zloader::GetFirstFile(ZFile& file)
{
    if (map.size() > 0)
    {
        it = map.begin();
        file = it->second;
        it++;
        return true;
    }
    return false;
}

bool Zloader::GetNextFile(ZFile& file)
{
    if (it != map.end())
    {
        file = it->second;
        it++;
        return true;
    }
    return false;
}

ZFile Zloader::GetFile(const std::string& filename) const
{
    return map.at(filename);
}

bool Zloader::HasFile(const std::string& filename) const
{
    return map.find(filename) != map.end();
}

int Zloader::Decompress()
{
    if (unzGoToFirstFile(file) < 0)
        return 0;

    do
    {
        unz_file_info info;
        unzGetCurrentFileInfo(file, &info, nullptr, 0, nullptr, 0, nullptr, 0);

        ZFile data;
        char* filename=new char[info.size_filename+1];
        unzGetCurrentFileInfo(file, nullptr, filename, info.size_filename, nullptr, 0, nullptr, 0);
        filename[info.size_filename]='\0';
        data.filename=filename;

        data.compressed_size = (unsigned int)info.uncompressed_size;
        data.size=(unsigned int)info.uncompressed_size;
        data.data=new char[info.uncompressed_size];

        unzOpenCurrentFile(file);

        int r = unzReadCurrentFile(file, data.data, info.uncompressed_size);
        if (r >= 0)
        {
            map.insert(std::pair<std::string, ZFile>(data.filename,data) );
        }
        
        unzCloseCurrentFile(file);

        delete[] filename;

    }while (unzGoToNextFile(file) == UNZ_OK);

    return 0;
}
