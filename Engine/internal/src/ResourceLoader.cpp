#include "ResourceLoader.h"
#include "Device.h"
#include "Zloader.h"
#include "Polygon.h"
#include "ParticleSystem\ParticleSystem.h"
#include <sstream>
#include <fstream>
#include <locale>
#include <codecvt>
#include <Logger.h>
#include <FileSystem.h>
#include <Stopwatch.h>
#include <Component.h>
#include <Material.h>
#include "RendererTypes.h"
#include "Effekseer.h"

using namespace Islander;
using namespace Islander::Renderer;

FileLibrary::FileLibrary()
	: _id(0)
{ }

int FileLibrary::AddFile(const std::string& path)
{
	if (FileSystem::Exists(path))
	{
		ResourceFile* file = new ResourceFile();
		file->_id = _id++;
		file->_path = path;
		FileSystem::GetFileName(path, file->_name);
		file->_type = RESOURCE_FILE_TYPE_FILE;
		file->_parent = nullptr;

		_resources.insert(std::pair<std::string, ResourceFile*>(file->_name, file));

		return 1;
	}

	return 0;
}

int FileLibrary::AddDirectory(const std::string& dir)
{
	std::string filename;
	auto handle = FileSystem::GetFirstFile(dir, filename);
	if (handle)
	{
		do
		{
			ResourceFile* file = new ResourceFile();
			file->_id = _id++;
			FileSystem::GetFileName(filename, file->_name);
			file->_path = dir + "/" + file->_name;
			file->_type = RESOURCE_FILE_TYPE_FILE;
			file->_parent = nullptr;

			_resources.insert(std::pair<std::string, ResourceFile*>(file->_name, file));
		} while (FileSystem::GetNextFile(handle, filename));
		
		return 1;
	}

	return 0;
}

int FileLibrary::AddCompressedFile(const std::string& path)
{
	Zloader* loader = new Zloader(path);

	ZFile zfile;
	if (loader->GetFirstFile(zfile))
	{
		do {
			ResourceFile* file = new ResourceFile();
			file->_id = _id++;
			file->_path = zfile.filename;
			FileSystem::GetFileName(zfile.filename, file->_name);
			file->_type = RESOURCE_FILE_TYPE_COMPRESSED;
			file->_parent = loader;

			_resources.insert(std::pair<std::string, ResourceFile*>(file->_name, file));

		} while (loader->GetNextFile(zfile));
		return 1;
	}

	delete loader;
	return 0;
}

ResourceFile* FileLibrary::GetFile(const std::string& name)
{
	auto it = _resources.find(name);
	if (it != _resources.end())
	{
		return it->second;
	}

	return nullptr;
}


ResourceLoader2::ResourceLoader2(Device& device)
	:
	_device(device),
	_id(0),
        _process_data(false)
{
	_thread = new std::thread(&ResourceLoader2::ProcessResourcesInternal, this);
    _meshLib = Islander::Model::CreatePolyMeshLibrary();
}

ResourceLoader2::~ResourceLoader2(void)
{
	_thread->detach();
	delete _thread;
    delete _meshLib;
}

void ResourceLoader2::ProcessResourcesInternal()
{
	// wait until signaled
	while (true)
	{
		Resource* resource;
		if (_pending.TryDequeue(&resource))
		{
			if (resource->_io->_type == RESOURCE_FILE_TYPE_FILE)
			{
                if (resource->_class == RESOURCE_CLASS_TEXTURE)
                {
                    TextureLoadInfo info;
                    auto data = LoadTexture(resource->_io->_path.c_str(), TEXTURE_LOADER_PNG, info);
                    if (data)
                    {
                        resource->_textureData._data = data;
                        resource->_textureData._info = info;
                        CompleteLoadingTexture(resource, info);
                        resource->_state = RESOURCE_STATE_LOADING_ASYNC_COMPLETED;
                        _completed.Queue(resource);
                    }
                    else
                    {
                        resource->_state = RESOURCE_STATE_UNLOADED;
                        _completed.Queue(resource);
                    }
                }
                else if (resource->_class == RESOURCE_CLASS_MESH)
                {
                    auto path = resource->_io->_path;
                    std::ifstream stream;
                    FileSystem::ReadFile(stream, path, true);
                    if (!stream.fail())
                    {
                        resource->_meshData._mesh = LoadEngineMesh(_meshLib, stream, true);
                        if (resource->_meshData._mesh)
                        {
                            resource->_state = RESOURCE_STATE_LOADING_ASYNC_COMPLETED;
                            _completed.Queue(resource);
                        }
                        else
                        {
                            resource->_state = RESOURCE_STATE_UNLOADED;
                            _completed.Queue(resource);
                        }
                    }
                    else
                    {
                        resource->_state = RESOURCE_STATE_UNLOADED;
                        _completed.Queue(resource);
                    }
                }
                else if (resource->_class == RESOURCE_CLASS_EFFECT)
                {
                    auto path = resource->_io->_path;
                    std::ifstream stream;
                    FileSystem::ReadFile(stream, path, true);
                    if (!stream.fail())
                    {
                        std::vector<char> buffer((std::istreambuf_iterator<char>(stream)), std::istreambuf_iterator<char>());
                        std::copy(buffer.begin(), buffer.end(), std::back_inserter(resource->_effectData._data));
                        resource->_state = RESOURCE_STATE_LOADING_ASYNC_COMPLETED;
                        _completed.Queue(resource);
                    }
                    else
                    {
                        resource->_state = RESOURCE_STATE_UNLOADED;
                        _completed.Queue(resource);
                    }
                }
			}
			else if (resource->_io->_type == RESOURCE_FILE_TYPE_COMPRESSED)
			{
                ZFile zfile;
                if (resource->_io->_parent->Decompress(resource->_io->_path, zfile))
                {
                    if (resource->_class == RESOURCE_CLASS_TEXTURE)
                    {
                        TextureLoadInfo info;
                        auto data = LoadTexture((char*)zfile.data, TEXTURE_LOADER_PNG, info);
                        if (data)
                        {
                            resource->_textureData._data = data;
                            resource->_textureData._info = info;
                            CompleteLoadingTexture(resource, info);
                            resource->_state = RESOURCE_STATE_LOADING_ASYNC_COMPLETED;
                            _completed.Queue(resource);

                            delete[]((char*)zfile.data);
                        }
                        else
                        {
                            resource->_state = RESOURCE_STATE_UNLOADED;
                            _completed.Queue(resource);
                        }
                    }
                    else if (resource->_class == RESOURCE_CLASS_MESH)
                    {
                        resource->_meshData._mesh = LoadEngineMesh(_meshLib, (unsigned char*)zfile.data, zfile.size, true);
                        if (resource->_meshData._mesh)
                        {
                            resource->_state = RESOURCE_STATE_LOADING_ASYNC_COMPLETED;
                            _completed.Queue(resource);
                        }
                        else
                        {
                            resource->_state = RESOURCE_STATE_UNLOADED;
                            _completed.Queue(resource);
                        }
                    }
                }
                else
                {
                    resource->_state = RESOURCE_STATE_UNLOADED;
                    _completed.Queue(resource);
                }
			}
		}
		else
		{
			// Acquire the lock and then release the lock when wait is called
			// Then wait until can process data, when the lock is then reacquired
			// Set the process flag and release the lock
			std::unique_lock<decltype(_mutex)>lock(_mutex);
			_condition.wait(lock, [this] { return _process_data; });
			_process_data = false;
		}

		std::this_thread::sleep_for(std::chrono::milliseconds(10));
	}
}

Resource* ResourceLoader2::CreateResourceFromStream(ResourceFile* image, std::istream& reader)
{
    auto resource = new Resource();
    resource->_textureData._name = image->_path;
    resource->_io = image;
    resource->_textureData._data = nullptr;
    resource->_id = _id++;
    resource->_state = RESOURCE_STATE_UNLOADED;
    resource->_textureData._texture = _device.ReserveTexture();
    resource->_class = RESOURCE_CLASS_TEXTURE;

    int size = 0; float width = 0; float height = 0;
    reader >> size;
    reader >> width;
    reader >> height;

    for (int i = 0; i < size; i++)
    {
        std::string name; int x; int y; int w; int h;
        reader >> name;

        reader >> x; reader >> y; reader >> w; reader >> h;

        auto it = _materials.find(name);
        if (it == _materials.end())
        {
            component_texture_composite* texture = new component_texture_composite();
            texture->texture.px = x / width;
            texture->texture.py = y / height;
            texture->texture.sx = w / width;
            texture->texture.sy = h / height;
            texture->width = w;
            texture->height = h;
            texture->texture.index = resource->_textureData._texture;

            _materials.insert(std::pair<std::string, component_texture_composite*>(name, texture));
        }
        else
        {
            auto& stream = Logger::GetStream(LOGGING_RESOURCES) << "Conflicting definitions for " << name;
            stream.endl();
        }
    }

    return resource;
}

Resource* ResourceLoader2::CreateResourceFromImage(ResourceFile* image)
{
    if (!image)
    {
        return nullptr;
    }

    auto resource = new Resource();
    resource->_textureData._name = image->_path;
    resource->_io = image;
    resource->_textureData._data = nullptr;
    resource->_id = _id++;
    resource->_state = RESOURCE_STATE_UNLOADED;
    resource->_textureData._texture = _device.ReserveTexture();
    resource->_class = RESOURCE_CLASS_TEXTURE;
    resource->_asyncAllowed = true;

    component_texture_composite* texture = new component_texture_composite();
    texture->texture.px = 0;
    texture->texture.py = 0;
    texture->texture.sx = 1;
    texture->texture.sy = 1;
    texture->width = 0;
    texture->height = 0;
    texture->texture.index = resource->_textureData._texture;

    std::string filename;
    FileSystem::GetFileName(image->_path, filename);

    _materials.insert(std::pair<std::string, component_texture_composite*>(filename, texture));

    return resource;
}

Resource* ResourceLoader2::CreateResourceFromMesh(ResourceFile* mesh)
{
    if (!mesh)
    {
        return nullptr;
    }

    auto resource = new Resource();
    resource->_meshData._name = mesh->_path;
    resource->_io = mesh;
    resource->_meshData._mesh = nullptr;
    resource->_id = _id++;
    resource->_state = RESOURCE_STATE_UNLOADED;
    resource->_class = RESOURCE_CLASS_MESH;
    resource->_asyncAllowed = true;

    return resource;
}

Resource* ResourceLoader2::CreateResourceFromEffect(ResourceFile* effect)
{
    if (!effect)
    {
        return nullptr;
    }

    auto resource = new Resource();
    resource->_effectData._name = effect->_path;
    resource->_io = effect;
    resource->_id = _id++;
    resource->_state = RESOURCE_STATE_UNLOADED;
    resource->_class = RESOURCE_CLASS_EFFECT;
    resource->_asyncAllowed = true;

    return resource;
}

Resource* ResourceLoader2::CreateResourceFromRawData(ResourceFile* data)
{
    if (!data)
    {
        return nullptr;
    }

    auto resource = new Resource();
    //resource->_rawData._name = data->_path;
    resource->_io = data;
    resource->_id = _id++;
    resource->_state = RESOURCE_STATE_UNLOADED;
    resource->_class = RESOURCE_CLASS_RAW_DATA;
    resource->_asyncAllowed = true;

    return resource;
}

Resource* ResourceLoader2::CreateResource(ResourceFile* image, ResourceFile* definition)
{
	if (!image || !definition)
	{
		return nullptr;
	}

	Resource* resource = nullptr;

	if (definition->_type == RESOURCE_FILE_TYPE_FILE)
	{
        std::ifstream reader;
        FileSystem::ReadFile(reader, definition->_path, false);

	    if (!reader.fail() && !reader.eof())
	    {
            resource = CreateResourceFromStream(image, reader);
            resource->_asyncAllowed = true;
	    }

	    reader.close();
	}
	else if (definition->_type == RESOURCE_FILE_TYPE_COMPRESSED)
	{
		ZFile zfile;
		if (definition->_parent->Decompress(definition->_path, zfile))
		{
            auto reader = std::istringstream((char*)zfile.data);

            resource = CreateResourceFromStream(image, reader);

            // Cannot load the resource async if they are in the same archive.
            // This is a limitation for now (due to decompression happening on multiple threads).
            // 
            resource->_asyncAllowed = definition->_parent != image->_parent;

			delete[](char*)zfile.data;
		}
	}

	return resource;
}

void ResourceLoader2::CompleteLoadingTexture(Resource* resource, TextureLoadInfo& info)
{
    std::string filename;
    FileSystem::GetFileName(resource->_io->_path, filename);
    auto found = _materials.find(filename);
    if (found != _materials.end())
    {
        found->second->texture.px = 0;
        found->second->texture.py = 0;
        found->second->texture.sx = info._width;
        found->second->texture.sy = info._height;
        found->second->width = info._width;
        found->second->height = info._height;
        found->second->texture.index = resource->_textureData._texture;
    }
}

void ResourceLoader2::LoadResource(Resource* resource)
{
	if (resource && resource->_io && resource->_state == RESOURCE_STATE_UNLOADED)
	{
		if (resource->_io->_type == RESOURCE_FILE_TYPE_FILE)
		{
            if (resource->_class == RESOURCE_CLASS_TEXTURE)
            {
                TextureLoadInfo info;
                auto data = LoadTexture(resource->_io->_path.c_str(), TEXTURE_LOADER_PNG, info);
                if (data)
                {
                    _device.LoadTexture(resource->_textureData._texture, data, info._width, info._height, info._size);
                    resource->_textureData._info = info;

                    CompleteLoadingTexture(resource, info);

                    resource->_state = RESOURCE_STATE_LOADED;
                    delete[] data;
                }
                else
                {
                    resource->_state = RESOURCE_STATE_UNLOADED;
                }
            }
            else if (resource->_class == RESOURCE_CLASS_MESH)
            {
                auto path = resource->_io->_path;
                std::ifstream stream;
                FileSystem::ReadFile(stream, path, true);
                if (!stream.fail())
                {
                    resource->_meshData._mesh = LoadEngineMesh(_meshLib, stream, false);
                    if (resource->_meshData._mesh)
                    {
                        resource->_state = RESOURCE_STATE_LOADED;
                    }
                    else
                    {
                        resource->_state = RESOURCE_STATE_UNLOADED;
                    }
                }
                else
                {
                    resource->_state = RESOURCE_STATE_UNLOADED;
                }
            }
            else if (resource->_class == RESOURCE_CLASS_EFFECT)
            {
                auto path = resource->_io->_path;
                std::ifstream stream;
                FileSystem::ReadFile(stream, path, true);
                if (!stream.fail())
                {
                    std::vector<char> buffer((std::istreambuf_iterator<char>(stream)), std::istreambuf_iterator<char>());
                    std::copy(buffer.begin(), buffer.end(), std::back_inserter(resource->_effectData._data));

                    auto particleSystem = _device.GetParticleSystem();
                    resource->_effectData._effect = LoadParticleEffect(particleSystem, resource->_effectData._data.data(), resource->_effectData._data.size());

                    resource->_state = RESOURCE_STATE_LOADED;
                }
                else
                {
                    resource->_state = RESOURCE_STATE_UNLOADED;
                }
            }
            else if (resource->_class == RESOURCE_CLASS_RAW_DATA)
            {
                auto path = resource->_io->_path;
                std::ifstream stream;
                FileSystem::ReadFile(stream, path, true);
                if (!stream.fail())
                {
                    std::vector<char> buffer((std::istreambuf_iterator<char>(stream)), std::istreambuf_iterator<char>());
                    std::copy(buffer.begin(), buffer.end(), std::back_inserter(resource->_rawData));
                    _rawData.insert(std::pair<std::string, Resource*>(resource->_io->_name, resource));

                    resource->_state = RESOURCE_STATE_LOADED;
                }
                else
                {
                    resource->_state = RESOURCE_STATE_UNLOADED;
                }
            }
		}
		else if (resource->_io->_type == RESOURCE_FILE_TYPE_COMPRESSED)
		{
            ZFile zfile;
            if (resource->_io->_parent->Decompress(resource->_io->_path, zfile))
            {
                if (resource->_class == RESOURCE_CLASS_TEXTURE)
                {
                    TextureLoadInfo info;
                    auto data = LoadTexture((char*)zfile.data, TEXTURE_LOADER_PNG, info);
                    if (data)
                    {
                        resource->_textureData._data = data;
                        resource->_textureData._info = info;
                        _device.LoadTexture(resource->_textureData._texture, data, info._width, info._height, info._size);
                        CompleteLoadingTexture(resource, info);
                        resource->_state = RESOURCE_STATE_LOADED;
                    }
                    else
                    {
                        resource->_state = RESOURCE_STATE_UNLOADED;
                    }

                    // Free the loaded data
                    delete[]((char*)zfile.data);
                }
                else if (resource->_class == RESOURCE_CLASS_MESH)
                {
                    resource->_meshData._mesh = LoadEngineMesh(_meshLib, (unsigned char*)zfile.data, zfile.size, false);
                    if (resource->_meshData._mesh)
                    {
                        resource->_state = RESOURCE_STATE_LOADED;
                        _completed.Queue(resource);
                    }
                    else
                    {
                        resource->_state = RESOURCE_STATE_UNLOADED;
                        _completed.Queue(resource);
                    }
                }
            }
            else
            {
                resource->_state = RESOURCE_STATE_UNLOADED;
            }
		}
	}
}

void ResourceLoader2::ReloadResource(Resource* resource)
{
    // This can be used for implementing hot reloading etc.

    if (resource && resource->_state == RESOURCE_STATE_LOADED)
    {
        if (resource->_io->_type == RESOURCE_FILE_TYPE_FILE)
        {
            if (resource->_class == RESOURCE_CLASS_MESH)
            {
                auto path = resource->_io->_path;
                std::ifstream stream;
                FileSystem::ReadFile(stream, path, true);
                if (!stream.fail())
                {
                    ReloadEngineMesh(_meshLib, resource->_meshData._mesh, stream);
                }
                else
                {
                    resource->_state = RESOURCE_STATE_UNLOADED;
                }
            }
        }
        else if (resource->_io->_type == RESOURCE_FILE_TYPE_COMPRESSED)
        {
            // We can't reload a compressed file right now
        }
    }
}

void ResourceLoader2::UnloadResource(Resource* resource)
{
	if (resource && resource->_state == RESOURCE_STATE_LOADED)
	{
		// TODO: delete texture
        if (resource->_class == RESOURCE_CLASS_MESH)
        {
            UnloadEngineMesh(_meshLib, resource->_meshData._mesh, true);
        }

		resource->_state = RESOURCE_STATE_UNLOADED;
	}
}

void GetUTF8FileName(const char16_t* path, std::string& filename)
{
    // Workaround since Islander handles paths in UTF8 and on windows converts them to whatever windows uses, whilst Effekseer gives us UTF16.

    std::wstring_convert<std::codecvt_utf8_utf16<wchar_t> > converter;
    std::string path_utf8 = converter.to_bytes((wchar_t*)path);

    Islander::FileSystem::GetFileName(path_utf8, filename);
}

void GetUTF8Path(const char16_t* path, std::string& path_utf8)
{
    // Workaround since Islander handles paths in UTF8 and on windows converts them to whatever windows uses, whilst Effekseer gives us UTF16.

    std::wstring_convert<std::codecvt_utf8_utf16<wchar_t> > converter;
    path_utf8 = converter.to_bytes((wchar_t*)path);
}

int ResourceLoader2::GetEffectData(Resource* resource, int type, char** names, int len)
{
    int count = 0;
    if (resource->_class == RESOURCE_CLASS_EFFECT && resource->_state == RESOURCE_STATE_LOADED)
    {
        switch (type)
        {
            case ISLANDER_EFFECT_TEXTURE_DIFFUSE:
            {
                int effectId = resource->_effectData._effect;
                auto particleSystem = _device.GetParticleSystem();
                auto effect = GetParticleEffect(particleSystem, effectId);
                count = effect->GetColorImageCount();
                if (names != nullptr)
                {
                    for (int32_t i = 0; i < count; i++)
                    {
                        auto path = effect->GetColorImagePath(i);
                        std::string filename;
                        GetUTF8Path(path, filename);
                        int strlength = min(len, filename.size());
                        memcpy(names[i], filename.c_str(), strlength);
                        names[i][strlength] = '\0';
                    }
                }
                break;
            }
            case ISLANDER_EFFECT_TEXTURE_NORMAL:
            {
                int effectId = resource->_effectData._effect;
                auto particleSystem = _device.GetParticleSystem();
                auto effect = GetParticleEffect(particleSystem, effectId);
                count = effect->GetNormalImageCount();
                if (names != nullptr)
                {
                    for (int32_t i = 0; i < count; i++)
                    {
                        auto path = effect->GetNormalImagePath(i);
                        std::string filename;
                        GetUTF8Path(path, filename);
                        int strlength = min(len, filename.size());
                        memcpy(names[i], filename.c_str(), strlength);
                        names[i][strlength] = '\0';
                    }
                }
                break;
            }
            case ISLANDER_EFFECT_MODEL:
            {
                int effectId = resource->_effectData._effect;
                auto particleSystem = _device.GetParticleSystem();
                auto effect = GetParticleEffect(particleSystem, effectId);
                count = effect->GetModelCount();
                if (names != nullptr)
                {
                    for (int32_t i = 0; i < count; i++)
                    {
                        auto path = effect->GetModelPath(i);
                        std::string filename;
                        GetUTF8Path(path, filename);
                        int strlength = min(len, filename.size());
                        memcpy(names[i], filename.c_str(), strlength);
                        names[i][strlength] = '\0';
                    }
                }
                break;
            }
        }
    }

    return count;
}

char* ResourceLoader2::GetRawData(const std::string& name, int* size)
{
    auto resource = _rawData.find(name);
    if (_rawData.end() == resource)
    {
        return nullptr;
    }

    if (size != nullptr)
    {
        *size = resource->second->_rawData.size();
    }

    return resource->second->_rawData.data();
}

void ResourceLoader2::CreateSubResources(Resource* resource, FileLibrary* lib)
{
    if (resource != nullptr && resource->_state == RESOURCE_STATE_LOADED)
    {
        auto& logger = Islander::Logger::GetStream(LOGGING_RESOURCES);

        // TODO: to unload resources effectively using reference counting on the resources.
        // Unload when the resource reaches 0 references.

        if (resource->_class == RESOURCE_CLASS_MESH)
        {
            auto mesh = resource->_meshData._mesh;
            for (int j = 0; j < mesh->numMaterials; j++)
            {
                auto& material = mesh->materials[j];

                for (int i = 0; i < material.numDiffuseTextures; i++)
                {
                    auto& texture = material.diffuseTexture[i];
                    auto composite = FindMaterialTexture(texture.data);
                    if (composite)
                    {
                        material.diffuseIndex[i] = composite->texture.index;
                    }
                    else
                    {
                        auto textureFile = lib->GetFile(texture.data);
                        if (textureFile != nullptr)
                        {
                            auto textureResource = CreateResourceFromImage(textureFile);
                            if (textureResource != nullptr)
                            {
                                material.diffuseIndex[i] = textureResource->_textureData._texture;
                                resource->subResources.push_back(textureResource);
                            }
                        }
                        else
                        {
                            logger << "Unable to find subresource " << texture.data;
                            logger.endl();
                        }
                    }
                }

                for (int i = 0; i < material.numNormalTextures; i++)
                {
                    auto& texture = material.normalTexture[i];
                    auto composite = FindMaterialTexture(texture.data);
                    if (composite)
                    {
                        material.normalIndex[i] = composite->texture.index;
                    }
                    else
                    {
                        auto textureFile = lib->GetFile(texture.data);
                        if (textureFile != nullptr)
                        {
                            auto textureResource = CreateResourceFromImage(textureFile);
                            if (textureResource != nullptr)
                            {
                                material.normalIndex[i] = textureResource->_textureData._texture;
                                resource->subResources.push_back(textureResource);
                            }
                        }
                        else
                        {
                            logger << "Unable to find subresource " << texture.data;
                            logger.endl();
                        }
                    }
                }
            }
        }
        else if (resource->_class == RESOURCE_CLASS_EFFECT)
        {
            int effectId = resource->_effectData._effect;
            auto particleSystem = _device.GetParticleSystem();
            auto effect = GetParticleEffect(particleSystem, effectId);

            int32_t numImages = effect->GetColorImageCount();
            for (int32_t i = 0; i < numImages; i++)
            {
                if (effect->GetColorImage(i) == nullptr)
                {
                    auto path = effect->GetColorImagePath(i);

                    // TODO: Here we we get the filename only (and not the path)
                    // The file library stores as a flat list.
                    std::string filename;
                    GetUTF8FileName(path, filename);

                    auto textureFile = lib->GetFile(filename);
                    if (textureFile != nullptr)
                    {
                        auto textureResource = CreateResourceFromImage(textureFile);
                        if (textureResource != nullptr)
                        {
                            resource->subResources.push_back(textureResource);
                        }
                    }
                    else
                    {
                        logger << "Unable to find subresource " << filename;
                        logger.endl();
                    }
                }
            }

            int32_t numMeshes = effect->GetModelCount();
            for (int32_t i = 0; i < numMeshes; i++)
            {
                if (effect->GetModel(i) == nullptr)
                {
                    auto path = effect->GetModelPath(i);

                    // TODO: Here we we get the filename only (and not the path)
                    // The file library stores as a flat list.
                    std::string filename;
                    GetUTF8FileName(path, filename);

                    auto modelFile = lib->GetFile(filename);
                    if (modelFile != nullptr)
                    {
                        auto modelResource = CreateResourceFromRawData(modelFile);
                        if (modelResource != nullptr)
                        {
                            resource->subResources.push_back(modelResource);
                        }
                    }
                    else
                    {
                        logger << "Unable to find subresource " << filename;
                        logger.endl();
                    }
                }
            }
        }
    }
}

void ResourceLoader2::LoadSubResources(Resource* resource)
{
    if (resource != nullptr)
    {
        for (auto& subResource : resource->subResources)
        {
            LoadResource(subResource);
        }

        if (resource->_class == RESOURCE_CLASS_EFFECT)
        {
            // Instruct effect to reload (now resources are loaded)
            int effectId = resource->_effectData._effect;
            auto particleSystem = _device.GetParticleSystem();
            auto effect = GetParticleEffect(particleSystem, effectId);
            effect->ReloadResources();
        }
    }
}

void ResourceLoader2::LoadSubResourcesAsync(Resource* resource)
{
    if (resource != nullptr)
    {
        // TODO: effect requires calling reload resources when complete - where can this be done?
        // Therefore fallback on synchronous for now...
        if (resource->_class == RESOURCE_CLASS_EFFECT)
        {
            LoadSubResources(resource);
            return;
        }

        for (auto& subResource : resource->subResources)
        {
            LoadResourceAsync(subResource);
        }
    }
}

void ResourceLoader2::ReloadSubResources(Resource* resource)
{
    if (resource != nullptr)
    {
        for (auto& subResource : resource->subResources)
        {
            LoadResource(subResource);
        }

        if (resource->_class == RESOURCE_CLASS_EFFECT)
        {
            // Instruct effect to reload (now resources are loaded)
            int effectId = resource->_effectData._effect;
            auto particleSystem = _device.GetParticleSystem();
            auto effect = GetParticleEffect(particleSystem, effectId);
            effect->ReloadResources();
        }
    }
}

void ResourceLoader2::LoadResourceAsync(Resource* resource)
{
	if (resource && resource->_state == RESOURCE_STATE_UNLOADED)
	{
        if (resource->_asyncAllowed)
        {
            resource->_state = RESOURCE_STATE_LOADING_ASYNC;
            _pending.Queue(resource);

            {
                // acquire the lock, once held allow the queue to be processed
                std::unique_lock<decltype(_mutex)>lock(_mutex);
                _process_data = true;
            }

            _condition.notify_one();
        }
        else
        {
            if (resource->_class == RESOURCE_CLASS_TEXTURE)
            {
                auto& stream = Logger::GetStream(LOGGING_RESOURCES) << "Unable to Load resource " << resource->_textureData._name << " Async, falling back";
                stream.endl();
            }
            else if (resource->_class == RESOURCE_CLASS_MESH)
            {
                auto& stream = Logger::GetStream(LOGGING_RESOURCES) << "Unable to Load resource " << resource->_meshData._name << " Async, falling back";
                stream.endl();
            }
            else if (resource->_class == RESOURCE_CLASS_EFFECT)
            {
                auto& stream = Logger::GetStream(LOGGING_RESOURCES) << "Unable to Load resource " << resource->_effectData._name << " Async, falling back";
                stream.endl();
            }

            LoadResource(resource);
        }
	}
}

void ResourceLoader2::ProcessResources()
{
	Stopwatch watch;
	watch.Start();

	Resource* resource;
	while (watch.GetElapsedMilliseconds() <= 5 && _completed.TryDequeue(&resource))
	{
		if (resource->_state == RESOURCE_STATE_LOADING_ASYNC_COMPLETED)
		{
			// Now, complete loading
            if (resource->_class == RESOURCE_CLASS_TEXTURE)
            {
                _device.LoadTexture(resource->_textureData._texture, resource->_textureData._data, resource->_textureData._info._width, resource->_textureData._info._height, resource->_textureData._info._size);
                delete[] resource->_textureData._data;
                resource->_textureData._data = nullptr;
            }
            else if (resource->_class == RESOURCE_CLASS_MESH)
            {
                CompleteLoadingEngineMesh(_meshLib, resource->_meshData._mesh);
            }
            else if (resource->_class == RESOURCE_CLASS_EFFECT)
            {
                auto particleSystem = _device.GetParticleSystem();
                resource->_effectData._effect = LoadParticleEffect(particleSystem, resource->_effectData._data.data(), resource->_effectData._data.size());
            }
			resource->_state = RESOURCE_STATE_LOADED;
		}
		else if (resource->_state == RESOURCE_STATE_UNLOADED)
		{
            auto& stream = Logger::GetStream(LOGGING_RESOURCES) << "Failed to load " << resource->_io->_path;
            stream.endl();
		}
	}
}

component_texture_composite* ResourceLoader2::FindMaterialTexture(const std::string& name)
{
	auto it = _materials.find(name);
	if (it != _materials.end())
	{
		return it->second;
	}

	return nullptr;
}


ResourceLoader::ResourceLoader(Device& device) : _device(device)
{
}

void ResourceLoader:: Decompress(const std::string& name)
{

	_loader = std::unique_ptr<Zloader>(new Zloader(name.c_str()));
	_loader->Decompress();
	
	//if (loader.Decompress() == 0)
	//	throw std::exception("Resource failed");
}

int ResourceLoader::Load(const std::string& name, ResourceStorage storage)
{
	std::stringstream texture;
	texture << name << ".png";
	std::stringstream material;
	material << name << ".mat";
	
	int tex;

	if (_loader.get() != NULL && _loader->HasFile(texture.str()) && _loader->HasFile(material.str()))
	{
		ZFile texfile =  _loader->GetFile(texture.str());
		tex = _device.LoadTexture(texfile.data, 1024, 1024, texfile.size);
		delete[] texfile.data;
		ZFile matfile = _loader->GetFile(material.str());
		LoadMaterialFiles(matfile, tex, storage);
		delete[] matfile.data;
	}
	else
	{
		tex = _device.LoadTexture(texture.str().c_str());
		if (tex != -1)
		{
            auto& stream = Logger::GetStream(LOGGING_RESOURCES) << "Loaded " << texture.str();
            stream.endl();

			LoadMaterialFiles(material.str(), tex, storage);
		}
	}
	return tex;
}


ResourceLoader::~ResourceLoader(void)
{
}

Material* ResourceLoader:: GetMaterial(const std::string& name) const
{
	return _materials.at(name);
}

Material* ResourceLoader:: GetMaterial(int32_t id) const
{
	return _lookup[id];
}

bool CompareMaterials(const Material* mat1,
					  const Material* mat2)
{
	return mat1->_texture[0]._texture == mat2->_texture[0]._texture &&
		mat1->_texture[0]._position.GetX() == mat2->_texture[0]._position.GetX() &&
		mat1->_texture[0]._position.GetY() == mat2->_texture[0]._position.GetY();
}

int32_t ResourceLoader:: Lookup(const std::string& name) const
{
	auto material = GetMaterial(name);
	for (int i = 0; i < _lookup.size(); i++)
		if (CompareMaterials(material, _lookup[i]))
			return i;
	return -1;
}

template<class t>
void ResourceLoader::ParseMaterial(t& ss, int tex, ResourceStorage storage)
{
	int size=0; float width=0; float height=0;
	ss >> size;
	ss >> width;
	ss >> height;

	MaterialTexture* textures = new MaterialTexture[size];
	std::string* names = new std::string[size];

	for (int i = 0; i < size; i++)
	{
		std::string name; int x; int y; int w; int h;
		ss >> name;

		ss >> x; ss >> y; ss >> w; ss >> h;

		textures[i]._position.SetX(x / width);
		textures[i]._position.SetY(y / height);
		textures[i]._scale.SetX(w / width);
		textures[i]._scale.SetY(h / height);
		textures[i]._originalScale.SetX(w);
		textures[i]._originalScale.SetY(h);
		textures[i]._texture = tex;

		names[i] = name;
	}

	if (storage == RESOURCE_STORAGE_ATLAS)
	{
		_device.CreateTextureAtlas(tex, textures, size);
	}

	for (int i = 0; i < size; i++)
	{
		Material* mat = new Material;
		mat->_texture[0] = textures[i];
		mat->vertexshader = _vert;
		mat->pixelshader = _pix;
		
		_materials.insert(std::pair<std::string, Material*>(names[i], mat));
		_lookup.push_back(mat);
	}

	delete[] textures;
	delete[] names;
}

void ResourceLoader::LoadMaterialFiles(const ZFile & matfile, int tex, ResourceStorage storage)
{

	std::stringstream ss;

	char * data = (char*)matfile.data;

	for (int i = 0; i < matfile.size; i++)
		ss << (data)[i];

	ss.seekp(ss.beg);

	ParseMaterial(ss, tex, storage);

}

void ResourceLoader::LoadMaterialFiles(const std::string & matfile, int tex, ResourceStorage storage)
{
	std::ifstream reader;
	FileSystem::ReadFile(reader, matfile, false);

	if (!reader.fail())
	{
		ParseMaterial(reader, tex, storage);
	}

	reader.close();

}
