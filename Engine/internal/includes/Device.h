#pragma once
#include "Window.h"
#include <vector>
#include <iostream>
#include <thread>
#include "Renderer.h"
#include "LocklessQueue.h"
#include "Crimson.h"
#include "IDevice.h"
#include "IResourceLoader.h"

struct render_mesh_entity;
struct IslanderRenderable;

namespace Islander {

    class Scene;
    class World;
    template<class t>
    class Camera;
    typedef Camera<float> Cameraf;
    class Camera3D;
    struct AnimationSystem;

    namespace Numerics
    {
        class Matrix4x4;
    }

    namespace Model
    {
        struct PolygonMeshLibrary;
        struct PolygonModel;
        struct PolygonJoint;
        struct PolygonAnimationFrameBone;
        struct PolygonAnimation;
        struct PolygonAnimations;
    }

    namespace Renderer
    {
        class Font;
        class Quad;
        class Line;
        struct FontDescription;
        class RenderSystems;
        class ResourceLoader;
        class ResourceLoader2;
        struct Query;
        struct QueryEngine;
#if ISLANDER_FREETYPE_SUPPORTED
        class FreeTypeEngine;
#endif
        struct ParticleSystem;

        typedef void*(DeviceAllocator)(int);

        class Device : public IDevice
        {
        public:
            Device(void);
            ~Device(void);
            /*
                Initializes the device and binds to specified window.
                */
            int Init(WINDOW*, FontDescription& font);
            /*
                Releases managed resources;
                */
            int Release(void);
            /*
                Renders a scene to the buffer.
            */
            int RenderScene(void* scene, Cameraf* camera, void* renderProperty);
            /*
            Renders a scene to the buffer.
            */
            int RenderScene3D(IslanderRenderablePass* renderablePasses, int renderPassCount, IslanderRenderable* renderables);
            /*
            Dispatches a compute workload.
            */
            int DispatchCompute(IslanderComputeWorkload* computeWorkload);
            /*
            Renders the particles to the buffer.
            */
            int RenderParticleSystem(Camera3D* camera);
            /*
            Renders crimson UI.
            */
            int RenderUI(CRIMSON_HANDLE crimson);
            /*
            Sets the render settings for crimson UI.
            */
            void SetUISettings(IslanderUISettings* settings);
            /*
            Sets the text render settings for crimson UI.
            */
            void SetUITextSettings(IslanderUITextSetting* settings, int numSettings);
            /*
                Loads a texture from a file and returns its id.
                */
            int LoadTexture(const char* filename);
            /*
                Loads and compiles a vertex shader and returns its id.
                */
            int LoadVertexShader(const char* filename, const char* method, SEMANTIC * semantics, int count, IslanderShaderDefineList* defines);
            /*
                Loads and compiles a pixel shader and returns its id.
            */
            int LoadPixelShader(const char* filename, const char* method, IslanderShaderDefineList* defines);
            /*
            * Loads and compiles a geometry shader and returns its id.
            */
            int LoadGeometryShader(const char* filename, const char* method, IslanderShaderDefineList* defines);
            /*
            * Loads and compiles a compute shader and returns its id.
            */
            int LoadComputeShader(const char* filename, const char* method, IslanderShaderDefineList* defines);

            void SetPixelShaderSamplers(int pixelShader, int numSamplers, IslanderTextureSampler* samplers);
            void SetVertexShaderSamplers(int vertexShader, int numSamplers, IslanderTextureSampler* samplers);

            /*
                Creates a buffer and returns its id.
                
            int CreateBuffer(float * data, int stride, int count, Usage usage);*/
            /*
                Loads a texture from a data and returns its id.
                */
            int LoadTexture(void* data, int width, int height, int size);

            int CreateEmptyTexture(int width, int height, int type);

            void GenerateComputeTexture(int texture);

            int ReserveTexture();
            
            int LoadTexture(int texture, void* data, int width, int height, int size);

            int CreateCubeMapTexture(int* textureFaces, int width, int height);
            int CreateTextureArray(int count, int* textures, int width, int height);

            const int getWidth() const{ return window->Width(); }
            const int getHeight() const{ return window->Height(); }

            //void DestroyBuffer(int id);

            const int GetBufferCount() const;

            void Present();

            void SetFullscreen(bool mode);

            int GetDrawCalls() const;
            int GetNumTriangles() const;

            IslanderRenderTarget CreateRenderTarget(RenderTargetType type, uint32_t width, uint32_t height);

            void ResizeRenderTarget(IslanderRenderTarget target, uint32_t width, uint32_t height);

            IslanderRenderTargetArray CreateRenderTargetArray(RenderTargetType type, uint32_t width, uint32_t height, int dimensions);

            void SetRenderTarget(int target = -1);

            void SetRenderFlags(int flags);

            void LoadFont(FontDescription& description);

            void SetPasses(const PASS_LIST& list);

            void ClearPasses();

            void ResizeBackBuffer();

            void DestroyRenderTarget(int id);

            void SetPreferredRenderer(int32_t type);

            int32_t GetCurrentRenderer();

            void CreateTextureAtlas(int sourceTexture, MaterialTexture* set, int count);

            inline ResourceLoader* GetResourceLoader() { return resourceLoader; };
            Islander::IResourceLoader2* GetResourceLoader2();

            void MeasureText(const std::string& name, char* text, int wrap, int size, float& width, float& height);

            char* GetBytesFromTexture(int texture, int* width, int* height, int* size);

            bool GetTextureList(TextureListData& list);

            void GetResolutions(DisplayResolutions& list);

            void SetDebugBoneMesh(Islander::Model::PolygonModel* mesh, int vertexShader, int pixelShader) { debugbonemesh = mesh; bonePixelShader = pixelShader; boneVertexShader = vertexShader; }

            void InitializeImgui();

            void NewFrameImgui();

            void RenderImgui();

            void ImageImgui(int texture, float width, float height);

            void ImageImgui(int texture, float x, float y, float width, float height);

            bool ImageButtonImgui(int id, int texture, float x, float y, float sx, float sy);

            void SetLocalAllocator(DeviceAllocator* allocator) { local_allocator = allocator; }

            void SetSyncInterval(int syncInterval);

            Islander::Renderer::ParticleSystem* GetParticleSystem() { return particleSystem; }

            void InitializePerformanceCounters();
            
            void BeginGPUProfilingFrame();

            int EndGPUProfilingFrame();

            void BeginGPUProfiling();

            int EndGPUProfiling();

            bool GetGPUProfileFrameData(int handle, IslanderGPUProfileFrameData& data);

            bool GetGPUProfileData(int handle, IslanderGPUProfileData& data);

            void GetRendererStats(IslanderRendererStats& stats);

            void ProcessPolyLibrary(Islander::Model::PolygonMeshLibrary* lib);

#if ISLANDER_FREETYPE_SUPPORTED
            struct FreeTypeCacheEntry
            {
                int vertexBuffer;
                int texture;
                int renderWidth;
                bool used;
            };
#endif

        private:

            static void TextHandler(void* userData, void* row, int e);
            static void MeshHandler(void* userData, void* row, int e);

            void RenderDebugBone(Islander::Model::PolygonJoint* joint, IslanderRenderable& mesh, const Islander::Numerics::Matrix4x4& proj, const Islander::Numerics::Matrix4x4& view, const Islander::Numerics::Matrix4x4& parent);
            void RenderSkinnedMesh(IslanderRenderable& mesh, const Islander::Numerics::Matrix4x4& proj, const Islander::Numerics::Matrix4x4& view, const PASS_CONFIG& config);

            float Interpolate(float a, float b, float t);
            //void CalculateBoneTransform(PolygonAnimationFrameBone* bone, float* pos, float* scale, glm::qua<float>& quat, float time);
            void CalculateBoneTransform(Islander::Model::PolygonAnimationFrameBone* bone, Islander::Numerics::Matrix4x4& tr, float time);
            void CalculateBoneMatrix(Islander::Numerics::Matrix4x4* boneMatrix, Islander::Model::PolygonAnimation* animation, Islander::Model::PolygonJoint* parent, Islander::Numerics::Matrix4x4* animationFrames, Islander::Numerics::Matrix4x4& transform, Islander::Numerics::Matrix4x4& inverseMeshTransform);
            //void CalculateBoneMatrixForParenting(Matrix4x4* boneMatrix, PolygonAnimation* animation, PolygonJoint* parent, Matrix4x4* animationFrames, Matrix4x4& transform, Matrix4x4& inverseMeshTransform);
            void CalculateBoneMatrixForParenting(Islander::Numerics::Matrix4x4* boneMatrix, Islander::Model::PolygonAnimation* animation, Islander::Model::PolygonJoint* parent, Islander::Numerics::Matrix4x4& transform, Islander::Numerics::Matrix4x4& inverseMeshTransform, float elapsedTime);
            //void CalculateBoneTransforms(PolygonAnimations* animation, Matrix4x4* animationFrameTransforms, float elapsedTime, AnimationSystem* system, int controllerId);

            Quad* quad;

            WINDOW* window;

            PASS_LIST passlist;

            std::unordered_map<std::string, Font*> engine;

#if ISLANDER_FREETYPE_SUPPORTED
            FreeTypeEngine* _freetype;
            std::unordered_map<std::string, FreeTypeCacheEntry> freeTypeCache;
#endif

            Line* line;

            int32_t preferred;

            IRenderer* renderer;

            RenderSystems* renderSystems;

            ResourceLoader* resourceLoader;
            ResourceLoader2* resourceLoader2;

            int current_rendertarget;

            int render_flags;

            int drawcalls;
            int numtriangles;
            
            QueryEngine* queryEngine;
            std::thread* querythread;

            Islander::Model::PolygonModel* debugbonemesh;
            int boneVertexShader;
            int bonePixelShader;

            ParticleSystem* particleSystem;
            IslanderUISettings uiSettings;
            std::unordered_map<int, int> uiTextSizes;

            DeviceAllocator* local_allocator;
        };

        /*
        Creates a render property for a particular scene.
        */
        void* CreateRenderProperty(void* scene);

    }
}





