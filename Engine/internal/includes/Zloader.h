#pragma once
#include <string>
#include <unordered_map>
extern "C"
{
#ifdef WIN32
#define ZLIB_WINAPI
#endif
#include <zlib.h>
#include <unzip.h>
}

namespace Islander
{

    struct ZFile
    {
        std::string filename;
        void* data;
        unsigned int size;
        unsigned int compressed_size;
    };


    class Zloader
    {
    public:
        Zloader() {}
        Zloader(const char* filename);
        Zloader(const std::string& filename);
        ~Zloader(void);
        int Decompress();
        bool Decompress(const std::string& filename, ZFile& zfile);
        ZFile GetFile(const std::string& filename) const;
        bool HasFile(const std::string& filename) const;
        bool GetFirstFile(ZFile& file);
        bool GetNextFile(ZFile& file);
    private:

        void GetInfo();

        std::unordered_map<std::string, ZFile> map;
        unzFile file;
        std::unordered_map<std::string, ZFile>::iterator it;
    };

}
