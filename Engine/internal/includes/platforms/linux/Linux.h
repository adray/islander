#pragma once
#include <GL/glew.h>
#include <GL/glxew.h>
#include <X11/X.h>
#include <X11/Xlib.h>
#include <GL/gl.h>
#include <GL/glx.h>
#include <GL/glu.h>

class RenderState
{
public:
	Display *display;
	Window  root;
	Window  win;
	XVisualInfo *vi;
	GLXFBConfig *config;
	Colormap cmap;
	XSetWindowAttributes swa;
 	GLXContext context;
};
