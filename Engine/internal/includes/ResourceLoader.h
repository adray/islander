#pragma once
#include <unordered_map>
#include <memory>
#include <vector>
#include <thread>
#include <mutex>
#include <condition_variable>
#include "LocklessQueue.h"
#include "TextureLoader.h"
#include "IResourceLoader.h"

namespace Islander {

struct ZFile;
class Zloader;
class Material;
struct component_texture_composite;

namespace Model
{
	struct PolygonMeshLibrary;
	struct PolygonModel;
}
	
namespace Renderer
{
	
class Device;
struct TextureLoadInfo;

enum ResourceStorage
{
	RESOURCE_STORAGE_DEFAULT,
	RESOURCE_STORAGE_ATLAS,
};

enum ResourceState
{
	RESOURCE_STATE_UNLOADED = 0,
	RESOURCE_STATE_LOADED = 1,
	RESOURCE_STATE_LOADING_ASYNC = 2,
	RESOURCE_STATE_LOADING_ASYNC_COMPLETED = 3,
};

enum ResourceFileType
{
	RESOURCE_FILE_TYPE_FILE = 0,
	RESOURCE_FILE_TYPE_COMPRESSED = 1,
};

enum ResourceClass
{
    RESOURCE_CLASS_TEXTURE = 0,
    RESOURCE_CLASS_MESH = 1,
	RESOURCE_CLASS_EFFECT = 2,
	RESOURCE_CLASS_RAW_DATA = 3,
};

class ResourceFile
{
public:
	int _id;
	ResourceFileType _type;
	std::string _name;
	std::string _path;
	Zloader* _parent;
};

struct ResourceTexture
{
	int _texture;
    std::string _name;
    unsigned char* _data;
    TextureLoadInfo _info;
};

struct ResourceMesh
{
    std::string _name;
    Islander::Model::PolygonModel* _mesh;
};

struct ResourceEffect
{
    std::string _name;
    std::vector<char> _data;
    int _effect;
};

struct Resource
{
	int _id;
	ResourceState _state;
    ResourceClass _class;
    ResourceFile* _io;
    bool _asyncAllowed;
    ResourceTexture _textureData;
    ResourceMesh _meshData;
    ResourceEffect _effectData;
	std::vector<char> _rawData;
    std::vector<Resource*> subResources;
};

class FileLibrary
{
public:

	FileLibrary();

	int AddFile(const std::string& path);
	
	int AddDirectory(const std::string& dir);
	
	int AddCompressedFile(const std::string& path);

	ResourceFile* GetFile(const std::string& name);

private:
	int _id;
	std::unordered_map<std::string, ResourceFile*> _resources;
};

class ResourceLoader2 : public Islander::IResourceLoader2
{
public:
	explicit ResourceLoader2(Device& device);
	~ResourceLoader2(void);

	Resource* CreateResource(ResourceFile* image, ResourceFile* definition);

    Resource* CreateResourceFromImage(ResourceFile* image);

    Resource* CreateResourceFromMesh(ResourceFile* image);

    Resource* CreateResourceFromEffect(ResourceFile* effect);

	Resource* CreateResourceFromRawData(ResourceFile* data);

	void LoadResource(Resource* resource);

	void ReloadResource(Resource* resource);

	void UnloadResource(Resource* resource);

	void LoadResourceAsync(Resource* resource);

	void ProcessResources();

    void CreateSubResources(Resource* resource, FileLibrary* lib);

    void LoadSubResources(Resource* resource);

	void LoadSubResourcesAsync(Resource* resource);

	void ReloadSubResources(Resource* resource);

	component_texture_composite* FindMaterialTexture(const std::string& name);

    int GetEffectData(Resource* resource, int type, char** names, int len);

	char* GetRawData(const std::string& name, int* size);

private:

	ResourceLoader2(const ResourceLoader2& other);
	ResourceLoader2& operator =(ResourceLoader2& other);

    Resource* CreateResourceFromStream(ResourceFile* image, std::istream& reader);

    void CompleteLoadingTexture(Resource* resource, TextureLoadInfo& info);

	void ProcessResourcesInternal();

	Islander::Model::PolygonMeshLibrary* _meshLib;
	Device& _device;
	std::unordered_map<std::string, component_texture_composite*> _materials;
	std::unordered_map<std::string, Resource*> _rawData;
	std::thread* _thread;
	LocklessQueue<Resource*> _pending;
	LocklessQueue<Resource*> _completed;
	std::mutex _mutex;
	std::condition_variable _condition;
	bool _process_data;
	int _id;

};

class ResourceLoader
{
public:
	ResourceLoader(Device& device);
	~ResourceLoader(void);

	Material* GetMaterial(const std::string& name) const;

	Material* GetMaterial(int32_t id) const;

	void Decompress(const std::string& name);

	int Load(const std::string& name, ResourceStorage storage = RESOURCE_STORAGE_DEFAULT);

	inline void SetDefaultShaders(int pix, int vert) { _pix = pix; _vert = vert; }

	int32_t Lookup(const std::string& name) const;

private:

	struct CachedResourceItem
	{
		int _id;
		std::string _name;
		ResourceStorage _storage;
	};

	std::unordered_map<std::string, Material*> _materials;
	std::vector<Material*> _lookup;
	std::vector<CachedResourceItem> _items;

	void LoadMaterialFiles(const std::string & matfile, int tex, ResourceStorage storage);

	void LoadMaterialFiles(const ZFile & matfile, int tex, ResourceStorage storage);

	template<class t>
	void ParseMaterial(t& stream, int tex, ResourceStorage storage);

	Device& _device;

	int _pix;
	int _vert;

	std::unique_ptr<Zloader> _loader;

};

} }
