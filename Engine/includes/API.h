#pragma once
#include "Exports.h"
#include "Component.h"
#include "..\..\Types\includes\AnimationTypes.h"
#include "..\..\Types\includes\AudioTypes.h"
#include "..\..\Types\includes\CollisionTypes.h"
#include "..\..\Types\includes\NavigationTypes.h"
#include "..\..\Types\includes\RendererTypes.h"
#include "Crimson.h"

extern "C"
{

#include "entity.h"

	// Device management
	ISLANDER_DEVICE ENGINE_EXPORT IslanderCreateDevice();
	void ENGINE_EXPORT IslanderInitializeDevice(ISLANDER_DEVICE device, ISLANDER_WINDOW window, IslanderFontDescription font);
	ISLANDER_BINDINGS ENGINE_EXPORT IslanderCreateBindings(ISLANDER_WORLD world);
    void ENGINE_EXPORT IslanderRenderScene(ISLANDER_DEVICE device, ISLANDER_WORLD world, ISLANDER_CAMERA camera, ISLANDER_BINDINGS bindings);
    void ENGINE_EXPORT IslanderRenderScene3D(ISLANDER_DEVICE device, IslanderRenderablePass* passes, int passCount, IslanderRenderable* renderables);
    void ENGINE_EXPORT IslanderRenderParticles(ISLANDER_DEVICE device, ISLANDER_CAMERA3D camera);
    int ENGINE_EXPORT IslanderDispatchCompute(ISLANDER_DEVICE device, IslanderComputeWorkload* workload);
    void ENGINE_EXPORT IslanderRenderUI(ISLANDER_DEVICE device, CRIMSON_HANDLE crimson);
    void ENGINE_EXPORT IslanderSetUISettings(ISLANDER_DEVICE device, IslanderUISettings* settings);
    void ENGINE_EXPORT IslanderSetUITextSettings(ISLANDER_DEVICE device, IslanderUITextSetting* settings, int numSettings);
	void ENGINE_EXPORT IslanderSetPreferredRenderer(ISLANDER_DEVICE device, int renderer);
	void ENGINE_EXPORT IslanderPresent(ISLANDER_DEVICE device);
    int ENGINE_EXPORT IslanderGetCurrentRenderer(ISLANDER_DEVICE device);
    void ENGINE_EXPORT IslanderGetResolutions(ISLANDER_DEVICE device, IslanderResolutions* resolution);
    void ENGINE_EXPORT IslanderResizeBackBuffer(ISLANDER_DEVICE device);
    void ENGINE_EXPORT IslanderSetDeviceLocalAllocator(ISLANDER_DEVICE device, ISLANDER_ALLOCATOR allocator);
    void ENGINE_EXPORT IslanderSetSyncInterval(ISLANDER_DEVICE device, int syncInterval);
    void ENGINE_EXPORT IslanderGetRendererStats(ISLANDER_DEVICE device, IslanderRendererStats* stats);
    void ENGINE_EXPORT IslanderProcessPolyLibrary(ISLANDER_DEVICE device, ISLANDER_POLYGON_LIBRARY lib);

	// Sound management
	ISLANDER_SOUND_PLAYER ENGINE_EXPORT IslanderCreateSoundPlayer(ISLANDER_WINDOW window);
	ISLANDER_SOUND_TOKEN ENGINE_EXPORT IslanderPlaySound(ISLANDER_SOUND_PLAYER player, int sound, int loopCount);
	void ENGINE_EXPORT IslanderProcessSoundEventQueue(ISLANDER_SOUND_PLAYER player, /*ISLANDER_SOUND_EVENT ev*/IslanderAudioEvent* ev);
	void ENGINE_EXPORT IslanderCloseSoundPlayer(ISLANDER_SOUND_PLAYER player);
	void ENGINE_EXPORT IslanderStopSound(ISLANDER_SOUND_PLAYER player, ISLANDER_SOUND_TOKEN token);
	void ENGINE_EXPORT IslanderReleaseToken(ISLANDER_SOUND_PLAYER player, ISLANDER_SOUND_TOKEN token);
	int ENGINE_EXPORT IslanderCreateSoundCategory(ISLANDER_SOUND_PLAYER player);
	int ENGINE_EXPORT IslanderIsSoundPlayerInitialized(ISLANDER_SOUND_PLAYER player);
	void ENGINE_EXPORT IslanderSetCategoryVolume(ISLANDER_SOUND_PLAYER player, int category, float vol);
    void ENGINE_EXPORT IslanderSetSoundVolume(ISLANDER_SOUND_PLAYER player, ISLANDER_SOUND_TOKEN token, float vol);
    void ENGINE_EXPORT IslanderSetSoundFrequencyRatio(ISLANDER_SOUND_PLAYER player, ISLANDER_SOUND_TOKEN token, float ratio);
	int ENGINE_EXPORT IslanderGetTokenState(ISLANDER_SOUND_PLAYER player, ISLANDER_SOUND_TOKEN token);
	
	// Resource management
    int ENGINE_EXPORT IslanderLoadPixelShader(ISLANDER_DEVICE device, const char* filename, const char* method);
    int ENGINE_EXPORT IslanderLoadPixelShaderEx(ISLANDER_DEVICE device, const char* filename, const char* method, IslanderShaderDefineList* defines);
	int ENGINE_EXPORT IslanderLoadVertexShader(ISLANDER_DEVICE device, const char* filename, const char* method, IslanderShaderSemantic* semantics, int count);
    int ENGINE_EXPORT IslanderLoadVertexShaderEx(ISLANDER_DEVICE device, const char* filename, const char* method, IslanderShaderSemantic* semantics, int count, IslanderShaderDefineList* defines);
    int ENGINE_EXPORT IslanderLoadGeometryShaderEx(ISLANDER_DEVICE device, const char* filename, const char* method, IslanderShaderDefineList* defines);
    int ENGINE_EXPORT IslanderLoadComputeShaderEx(ISLANDER_DEVICE device, const char* filename, const char* method, IslanderShaderDefineList* defines);
	int ENGINE_EXPORT IslanderLoadFont(ISLANDER_DEVICE device, IslanderFontDescription font);
	int ENGINE_EXPORT IslanderLoadSound(ISLANDER_SOUND_PLAYER device, const char* filename, int category);
	void ENGINE_EXPORT IslanderMeasureText(ISLANDER_DEVICE device, IslanderMeasuredText* output);
	ISLANDER_TEXTURE_DATA ENGINE_EXPORT IslanderGetBytesFromTexture(ISLANDER_DEVICE device, int texture, int* width, int* height, int* size);
	void ENGINE_EXPORT IslanderFreeTextureData(ISLANDER_TEXTURE_DATA data);
	int ENGINE_EXPORT IslanderCreateTextureFromBytes(ISLANDER_DEVICE device, ISLANDER_TEXTURE_DATA data, int width, int height, int size);

	// New resource management
	ISLANDER_FILE_LIBRARY ENGINE_EXPORT IslanderCreateFileLibrary();
	ISLANDER_RESOURCE_FILE ENGINE_EXPORT IslanderGetFile(ISLANDER_FILE_LIBRARY lib, const char* name);
	int ENGINE_EXPORT IslanderAddFile(ISLANDER_FILE_LIBRARY lib, const char* path);
	int ENGINE_EXPORT IslanderAddDirectory(ISLANDER_FILE_LIBRARY lib, const char* path);
	int ENGINE_EXPORT IslanderAddCompressedFile(ISLANDER_FILE_LIBRARY lib, const char* path);
	ISLANDER_RESOURCE ENGINE_EXPORT IslanderCreateResource(ISLANDER_DEVICE device, ISLANDER_RESOURCE_FILE img, ISLANDER_RESOURCE_FILE def);
    ISLANDER_RESOURCE ENGINE_EXPORT IslanderCreateResourceFromTexture(ISLANDER_DEVICE device, ISLANDER_RESOURCE_FILE res);
    ISLANDER_RESOURCE ENGINE_EXPORT IslanderCreateResourceFromMesh(ISLANDER_DEVICE device, ISLANDER_RESOURCE_FILE res);
    ISLANDER_RESOURCE ENGINE_EXPORT IslanderCreateResourceFromEffect(ISLANDER_DEVICE device, ISLANDER_RESOURCE_FILE res);
    int ENGINE_EXPORT IslanderCreateCubeMapTexture(ISLANDER_DEVICE device, ISLANDER_RESOURCE* resourceArray, int width, int height);
    int ENGINE_EXPORT IslanderCreateTextureArray(ISLANDER_DEVICE device, int count, ISLANDER_RESOURCE* resourceArray, int width, int height);
    void ENGINE_EXPORT IslanderLoadResource(ISLANDER_DEVICE device, ISLANDER_RESOURCE res);
    void ENGINE_EXPORT IslanderReloadResource(ISLANDER_DEVICE device, ISLANDER_RESOURCE res);
	void ENGINE_EXPORT IslanderUnloadResource(ISLANDER_DEVICE device, ISLANDER_RESOURCE res);
	void ENGINE_EXPORT IslanderLoadResourceAsync(ISLANDER_DEVICE device, ISLANDER_RESOURCE res);
    int ENGINE_EXPORT IslanderGetResourceState(ISLANDER_DEVICE device, ISLANDER_RESOURCE res);
    int ENGINE_EXPORT IslanderGetReferencedResourceState(ISLANDER_DEVICE device, ISLANDER_RESOURCE res);
	void ENGINE_EXPORT IslanderFindMaterialTexture(ISLANDER_DEVICE device, const char* name, Islander::component_texture* texture);
	ENGINE_EXPORT Islander::component_texture* IslanderFindMaterialSubTexture(ISLANDER_DEVICE device, char* name, int* tex);
	void ENGINE_EXPORT IslanderGenerateSubTextures(ISLANDER_DEVICE device, const char* name, ISLANDER_SUBRESOURCE_GENERATION generationMethod, ISLANDER_SUBRESOURCE_DATA data);
	int ENGINE_EXPORT IslanderGetTextureSize(ISLANDER_DEVICE device, const char* name, int* w, int* h);
    int ENGINE_EXPORT IslanderGetTextureList(ISLANDER_DEVICE device, IslanderTexureListData* list);
    void ENGINE_EXPORT IslanderCreateReferencedResources(ISLANDER_DEVICE device, ISLANDER_RESOURCE resource, ISLANDER_FILE_LIBRARY filelib);
    void ENGINE_EXPORT IslanderLoadReferencedResources(ISLANDER_DEVICE device, ISLANDER_RESOURCE resource);
    void ENGINE_EXPORT IslanderLoadReferencedResourcesAsync(ISLANDER_DEVICE device, ISLANDER_RESOURCE resource);
    void ENGINE_EXPORT IslanderReloadReferencedResources(ISLANDER_DEVICE device, ISLANDER_RESOURCE res);
    ISLANDER_POLYGON_DATA ENGINE_EXPORT IslanderGetPolyMeshDataFromResource(ISLANDER_RESOURCE res);
    int ENGINE_EXPORT IslanderGetEffectData(ISLANDER_DEVICE device, ISLANDER_RESOURCE resource, int type, char** names, int len);
    int ENGINE_EXPORT IslanderCreateEmptyTexture(ISLANDER_DEVICE device, int width, int height, int type);
    void ENGINE_EXPORT IslanderGenerateComputeTexture(ISLANDER_DEVICE device, int texture);
    int ENGINE_EXPORT IslanderExportTexture(const char* path, unsigned char* data, int width, int height, int size);

	// Rendering
	ISLANDER_PASS_LIST ENGINE_EXPORT IslanderCreatePassList();
	void ENGINE_EXPORT IslanderSetPassList(ISLANDER_DEVICE device, ISLANDER_PASS_LIST passes);
    void ENGINE_EXPORT IslanderAppendPassList(ISLANDER_PASS_LIST passes, IslanderPassConfig& config);
    void ENGINE_EXPORT IslanderAppendNamedPassList(ISLANDER_PASS_LIST passes, const char* name, IslanderPassConfig& config);
    void ENGINE_EXPORT IslanderClearPassList(ISLANDER_PASS_LIST passes);
	void ENGINE_EXPORT IslanderCreateRenderTarget(ISLANDER_DEVICE device, int width, int height, IslanderRenderTarget* target);
    void ENGINE_EXPORT IslanderCreateRenderTargetOfType(ISLANDER_DEVICE device, int type, int width, int height, IslanderRenderTarget* target);
    void ENGINE_EXPORT IslanderCreateRenderTargetArrayOfType(ISLANDER_DEVICE device, int type, int width, int height, int dimensions, IslanderRenderTargetArray* target);
    void ENGINE_EXPORT IslanderResizeRenderTarget(ISLANDER_DEVICE device, IslanderRenderTarget target, int width, int height);
    void ENGINE_EXPORT IslanderSetPassConstantData(ISLANDER_PASS_LIST passes, int pass, void* constantData, int size);
    void ENGINE_EXPORT IslanderInitializeGPUPerformanceCounters(ISLANDER_DEVICE device);
    void ENGINE_EXPORT IslanderBeginGPUProfilingFrame(ISLANDER_DEVICE device);
    int ENGINE_EXPORT IslanderEndGPUProfilingFrame(ISLANDER_DEVICE device);
    void ENGINE_EXPORT IslanderBeginGPUProfiling(ISLANDER_DEVICE device);
    int ENGINE_EXPORT IslanderEndGPUProfiling(ISLANDER_DEVICE device);
    bool ENGINE_EXPORT IslanderGetGPUProfileFrameData(ISLANDER_DEVICE device, int handle, IslanderGPUProfileFrameData& data);
    bool ENGINE_EXPORT IslanderGetGPUProfileData(ISLANDER_DEVICE device, int handle, IslanderGPUProfileData& data);
    void ENGINE_EXPORT IslanderSetPixelShaderSamplers(ISLANDER_DEVICE device, int pixelShader, int numSamplers, IslanderTextureSampler* samplers);
    void ENGINE_EXPORT IslanderSetVertexShaderSamplers(ISLANDER_DEVICE device, int vertexShader, int numSamplers, IslanderTextureSampler* samplers);

    // Particles
    ISLANDER_PARTICLE_SYSTEM ENGINE_EXPORT IslanderGetParticleSystem(ISLANDER_DEVICE device);
    void ENGINE_EXPORT IslanderUpdateParticleSystem(ISLANDER_PARTICLE_SYSTEM system, float delta);
    int ENGINE_EXPORT IslanderPlayParticleEffect(ISLANDER_PARTICLE_SYSTEM system, ISLANDER_RESOURCE effect, float x, float y, float z);
    int ENGINE_EXPORT IslanderParticleEffectAlive(ISLANDER_PARTICLE_SYSTEM system, int id);
    void ENGINE_EXPORT IslanderMoveParticleEffect(ISLANDER_PARTICLE_SYSTEM system, int effectId, float x, float y, float z);
    void ENGINE_EXPORT IslanderSetLocationParticleEffect(ISLANDER_PARTICLE_SYSTEM system, int effectId, float x, float y, float z);
    void ENGINE_EXPORT IslanderSetRotationParticleEffect(ISLANDER_PARTICLE_SYSTEM system, int effectId, float x, float y, float z);
    void ENGINE_EXPORT IslanderSetTargetLocationParticleEffect(ISLANDER_PARTICLE_SYSTEM system, int effectId, float x, float y, float z);
    void ENGINE_EXPORT IslanderStopParticleEffect(ISLANDER_PARTICLE_SYSTEM system, int id);
    void ENGINE_EXPORT IslanderStopAllParticleEffects(ISLANDER_PARTICLE_SYSTEM system);

	// Windowing
	ISLANDER_WINDOW ENGINE_EXPORT IslanderCreateWindow();
	int ENGINE_EXPORT IslanderPumpWindow(ISLANDER_WINDOW window);
	void ENGINE_EXPORT IslanderSetWindowSize(ISLANDER_WINDOW window, int width, int height);
	int ENGINE_EXPORT IslanderIsKeyDown(ISLANDER_WINDOW window, char key);
	void ENGINE_EXPORT IslanderSetKeyUp(ISLANDER_WINDOW window, char key);
	int ENGINE_EXPORT IslanderGetLeftMouseState(ISLANDER_WINDOW window);
	int ENGINE_EXPORT IslanderGetRightMouseState(ISLANDER_WINDOW window);
	int ENGINE_EXPORT IslanderGetMouseWheelState(ISLANDER_WINDOW window);
	int ENGINE_EXPORT IslanderWindowWidth(ISLANDER_WINDOW window);
	int ENGINE_EXPORT IslanderWindowHeight(ISLANDER_WINDOW window);
	int ENGINE_EXPORT IslanderMouseX(ISLANDER_WINDOW window);
	int ENGINE_EXPORT IslanderMouseY(ISLANDER_WINDOW window);
    void ENGINE_EXPORT IslanderSetMousePos(ISLANDER_WINDOW window, int x, int y);
    void ENGINE_EXPORT IslanderGetDisplays(ISLANDER_WINDOW window, IslanderDisplays* displays);
    void ENGINE_EXPORT IslanderSetDisplay(ISLANDER_WINDOW window, int id);
    void ENGINE_EXPORT IslanderSetWindowIcon(ISLANDER_WINDOW window, char* data, int width, int height);
    void ENGINE_EXPORT IslanderSetWindowText(ISLANDER_WINDOW window, const char* text);
    void ENGINE_EXPORT IslanderSetWindowStyle(ISLANDER_WINDOW window, int style);

	// Cameras
	ISLANDER_CAMERA ENGINE_EXPORT IslanderCreateCamera();
	void ENGINE_EXPORT IslanderSetCameraTranslation(ISLANDER_CAMERA camera, float x, float y, float z);
    ISLANDER_CAMERA3D ENGINE_EXPORT IslanderCreateCamera3D();
    void ENGINE_EXPORT IslanderSetCamera3DTranslation(ISLANDER_CAMERA3D camera, float x, float y, float z);
    void ENGINE_EXPORT IslanderSetCamera3DProjection(ISLANDER_CAMERA3D camera, float nearZ, float farZ, float fovY, float aspect);
    void ENGINE_EXPORT IslanderSetCamera3DOrthographic(ISLANDER_CAMERA3D camera, float nearZ, float farZ, float width, float height);
    void ENGINE_EXPORT IslanderSetCamera3DLookAt(ISLANDER_CAMERA3D camera, float x, float y, float z);

	// Hardware Cursor
	ISLANDER_CURSOR ENGINE_EXPORT IslanderCreateCursor(ISLANDER_WINDOW window);
	int ENGINE_EXPORT IslanderLoadCursor(ISLANDER_CURSOR cursor, const char* filename);
	void ENGINE_EXPORT IslanderSetCursor(ISLANDER_CURSOR cursor, int id);
	void ENGINE_EXPORT IslanderHideCursor(ISLANDER_CURSOR cursor);
	void ENGINE_EXPORT IslanderShowCursor(ISLANDER_CURSOR cursor);
    void ENGINE_EXPORT IslanderDestroyCursor(ISLANDER_CURSOR cursor);

    // Mesh-polygon data
    ISLANDER_POLYGON_LIBRARY ENGINE_EXPORT IslanderCreatePolyLibrary();
    ISLANDER_POLYGON_DATA ENGINE_EXPORT IslanderAddPolyMeshData(ISLANDER_POLYGON_LIBRARY lib, float* vertexData, int32_t* indexData, int numVerts, int numIndicies, int stride, int createFlags);
    ISLANDER_POLYGON_DATA ENGINE_EXPORT IslanderUpdateMeshData(ISLANDER_POLYGON_LIBRARY lib, ISLANDER_POLYGON_DATA polyData, float* vertexData, int32_t* indexData, int numVerts, int numIndicies, int stride, int createFlags);
    ISLANDER_POLYGON_DATA ENGINE_EXPORT IslanderLoadEngineMesh(ISLANDER_POLYGON_LIBRARY lib, unsigned char* data, int count);
    void ENGINE_EXPORT IslanderUnloadEngineMesh(ISLANDER_POLYGON_LIBRARY lib, ISLANDER_POLYGON_DATA polyData);
    void ENGINE_EXPORT IslanderSetDebugBoneMesh(ISLANDER_DEVICE device, ISLANDER_POLYGON_DATA poly, int vertexShader, int pixelShader);
    void ENGINE_EXPORT IslanderGetPolyMeshData(ISLANDER_POLYGON_DATA poly, IslanderPolygonData* data);
    int ENGINE_EXPORT IslanderGetPolyMeshMaterials(ISLANDER_POLYGON_DATA poly);
    int ENGINE_EXPORT IslanderGetPolyMeshTextures(ISLANDER_POLYGON_DATA poly, int material, int type, char** textureNames, int len);
    void ENGINE_EXPORT IslanderSetPolyMeshNumMaterials(ISLANDER_POLYGON_DATA poly, int numMaterials, int numTextureChannels);
    void ENGINE_EXPORT IslanderSetPolyMeshNumTextures(ISLANDER_POLYGON_DATA poly, int material, int type, int numTextures);
    void ENGINE_EXPORT IslanderSetPolyMeshTexture(ISLANDER_POLYGON_DATA poly, int material, int type, int index, const char* textureName, int len);
    void ENGINE_EXPORT IslanderLinkPolyMeshTextures(ISLANDER_DEVICE device, ISLANDER_POLYGON_DATA poly);

    // Animations
    ISLANDER_ANIMATION_SYSTEM ENGINE_EXPORT IslanderCreateAnimationSystem();
    void ENGINE_EXPORT IslanderDestroyAnimationSystem(ISLANDER_ANIMATION_SYSTEM system);
    int ENGINE_EXPORT IslanderCreateAnimationController(ISLANDER_ANIMATION_SYSTEM system);
    void ENGINE_EXPORT IslanderDestroyAnimationController(ISLANDER_ANIMATION_SYSTEM system, int controller);
    void ENGINE_EXPORT IslanderPlayAnimation(ISLANDER_ANIMATION_SYSTEM system, int controller, int id, int loop);
    void ENGINE_EXPORT IslanderPauseAnimation(ISLANDER_ANIMATION_SYSTEM system, int controller);
    void ENGINE_EXPORT IslanderResetAnimation(ISLANDER_ANIMATION_SYSTEM system, int controller);
    int ENGINE_EXPORT IslanderGetAnimationId(ISLANDER_POLYGON_DATA poly, const char* name);
    float ENGINE_EXPORT IslanderGetAnimationLength(ISLANDER_POLYGON_DATA poly, int id);
    void ENGINE_EXPORT IslanderAnimationUpdate(ISLANDER_ANIMATION_SYSTEM system, float deltaTime);
    void ENGINE_EXPORT IslanderSetAnimationLoop(ISLANDER_ANIMATION_SYSTEM system, int controller, int loop);
    void ENGINE_EXPORT IslanderSetAnimationSpeed(ISLANDER_ANIMATION_SYSTEM system, int controller, float speedScale);
    int ENGINE_EXPORT IslanderGetAnimationCount(ISLANDER_POLYGON_DATA poly);
    int ENGINE_EXPORT IslanderGetAnimationName(ISLANDER_POLYGON_DATA poly, int id, char* name);
    float ENGINE_EXPORT IslanderAnimationControllerElapsed(ISLANDER_ANIMATION_SYSTEM system, int controller);
    void ENGINE_EXPORT IslanderBlendToAnimation(ISLANDER_ANIMATION_SYSTEM system, int controller, int id, float blendTime);
    void ENGINE_EXPORT IslanderPlayBlendedAnimation(ISLANDER_ANIMATION_SYSTEM system, int controller, int animationId1, int animationId2, uint64_t mask, int loop1, int loop2);
    void ENGINE_EXPORT IslanderCalculateAnimationPose(ISLANDER_ANIMATION_SYSTEM system, IslanderAnimationPose* pose, float* pos);
    void ENGINE_EXPORT IslanderSetAnimationTimeBounds(ISLANDER_ANIMATION_SYSTEM system, int controller, float startTime, float endTime);
    void ENGINE_EXPORT IslanderSetRootMotionAnimation(ISLANDER_ANIMATION_SYSTEM system, int controller, int isRootMotion, int rootMotionFlags);

    // Collision
    ISLANDER_COLLISION_SYSTEM ENGINE_EXPORT IslanderCreateCollisionSystem();
    ISLANDER_COLLISION_STATIC_MESH ENGINE_EXPORT IslanderAddStaticMesh(ISLANDER_COLLISION_SYSTEM system, IslanderCollisionStaticMeshInfo* mesh);
    ISLANDER_COLLISION_DYNAMIC_MESH ENGINE_EXPORT IslanderAddDynamicMesh(ISLANDER_COLLISION_SYSTEM system, IslanderCollisionDynamicMeshInfo* mesh);
    void ENGINE_EXPORT IslanderUpdateDynamicMesh(ISLANDER_COLLISION_DYNAMIC_MESH mesh, IslanderCollisionDynamicMeshInfo* info);
    void ENGINE_EXPORT IslanderUpdateStaticMesh(ISLANDER_COLLISION_SYSTEM system, ISLANDER_COLLISION_DYNAMIC_MESH mesh, IslanderCollisionStaticMeshInfo* meshInfo);
    void ENGINE_EXPORT IslanderOverrideStaticMeshOBB(ISLANDER_COLLISION_DYNAMIC_MESH mesh, float* verts);
    void ENGINE_EXPORT IslanderRemoveStaticMesh(ISLANDER_COLLISION_SYSTEM system, ISLANDER_COLLISION_STATIC_MESH mesh);
    void ENGINE_EXPORT IslanderRemoveDynamicMesh(ISLANDER_COLLISION_SYSTEM system, ISLANDER_COLLISION_DYNAMIC_MESH mesh);
    void ENGINE_EXPORT IslanderRunCollision(ISLANDER_COLLISION_SYSTEM system, float delta);
    void ENGINE_EXPORT IslanderGetCollisionInfo(ISLANDER_COLLISION_DYNAMIC_MESH mesh, IslanderCollisionData* data);
    void ENGINE_EXPORT IslanderRayCast(ISLANDER_COLLISION_SYSTEM system, ISLANDER_CAMERA3D camera, float px, float py, float pz, float dx, float dy, float dz, int group, IslanderCollisionRayInfo* info);
    void ENGINE_EXPORT IslanderRayCastFine(ISLANDER_COLLISION_SYSTEM system, ISLANDER_CAMERA3D camera, float px, float py, float pz, float dx, float dy, float dz, IslanderCollisionRayInfo* info);
    void ENGINE_EXPORT IslanderRayCastHeightmap(ISLANDER_COLLISION_SYSTEM system, ISLANDER_CAMERA3D camera, float px, float py, float pz, float dx, float dy, float dz, IslanderCollisionRayInfo* info);
    void ENGINE_EXPORT IslanderTransformRay(ISLANDER_CAMERA3D camera, float *px, float *py, float *pz, float *dx, float *dy, float *dz);
    void ENGINE_EXPORT IslanderCreateDebugCollisionGeometry(ISLANDER_COLLISION_SYSTEM system, IslanderCollisionGeometryData* gData);
    void ENGINE_EXPORT IslanderCreateDebugCollisionGeometryDynamicMesh(ISLANDER_COLLISION_SYSTEM system, ISLANDER_COLLISION_DYNAMIC_MESH mesh, IslanderCollisionGeometryData* gData);
    float ENGINE_EXPORT IslanderCollisionSweep(ISLANDER_COLLISION_SYSTEM system, ISLANDER_COLLISION_STATIC_MESH mesh, float* vector);
    void ENGINE_EXPORT IslanderCollisionOverlap(ISLANDER_COLLISION_SYSTEM system, ISLANDER_COLLISION_DYNAMIC_MESH mesh, IslanderCollisionOverlapInfo* info);
    void ENGINE_EXPORT IslanderCollisionQuery(ISLANDER_COLLISION_SYSTEM system, IslanderCollisionQueryInfo* info);

    // Navigation
    ISLANDER_NAVIGATION ENGINE_EXPORT IslanderCreateNavigation();
    void ENGINE_EXPORT IslanderNavAddRegion(ISLANDER_NAVIGATION nav, IslanderNavMeshRegion* region);
    void ENGINE_EXPORT IslanderNavGenerate(ISLANDER_NAVIGATION nav, IslanderNavMeshConfig* cfg);
    void ENGINE_EXPORT IslanderNavClearRegions(ISLANDER_NAVIGATION nav);
    void ENGINE_EXPORT IslanderNavDestroy(ISLANDER_NAVIGATION nav);
    void ENGINE_EXPORT IslanderNavGetData(ISLANDER_NAVIGATION nav, IslanderNavMeshData* data);
    void ENGINE_EXPORT IslanderNavLoad(ISLANDER_NAVIGATION nav, unsigned char* data, int count);
    int ENGINE_EXPORT IslanderNavCalculatePath(ISLANDER_NAVIGATION nav, IslanderNavPath* path);
    int ENGINE_EXPORT IslanderNavGetClosestPoly(ISLANDER_NAVIGATION nav, IslanderNavClosestPoly* closest);
    bool ENGINE_EXPORT IslanderNavFindPointsWithinCircle(ISLANDER_NAVIGATION nav, float* pt, float radius, float* points, int max_count, int* count);
    void ENGINE_EXPORT IslanderNavSetPosition(ISLANDER_NAVIGATION nav, float* pos);
    int ENGINE_EXPORT IslanderNavRayCast(ISLANDER_NAVIGATION nav, float* pos, float* end, float* t);

    // Navigation Avoidance
    ISLANDER_AVOIDANCE_SYSTEM ENGINE_EXPORT IslanderCreateNavigationAvoidance();
    void ENGINE_EXPORT IslanderInitAvoidance(ISLANDER_AVOIDANCE_SYSTEM avoidance, ISLANDER_NAVIGATION nav);
    void ENGINE_EXPORT IslanderResetAvoidance(ISLANDER_AVOIDANCE_SYSTEM avoidance);
    ISLANDER_AVOIDANCE_AGENT ENGINE_EXPORT IslanderAddAvoidanceAgent(ISLANDER_AVOIDANCE_SYSTEM avoidance, const IslanderAvoidanceAgentConfig& config);
    void ENGINE_EXPORT IslanderRemoveAvoidanceAgent(ISLANDER_AVOIDANCE_SYSTEM avoidance, ISLANDER_AVOIDANCE_AGENT id);
    void ENGINE_EXPORT IslanderUpdateAvoidance(ISLANDER_AVOIDANCE_SYSTEM avoidance, float dt);
    void ENGINE_EXPORT IslanderApplyAvoidanceAgentPhysics(ISLANDER_AVOIDANCE_SYSTEM avoidance, ISLANDER_AVOIDANCE_AGENT agent, float* pos);
    void ENGINE_EXPORT IslanderUpdateAvoidanceAgentDesiredVelocity(ISLANDER_AVOIDANCE_SYSTEM avoidance, ISLANDER_AVOIDANCE_AGENT agent, float* desiredVelocity);
    void ENGINE_EXPORT IslanderUpdateAvoidanceAgentTarget(ISLANDER_AVOIDANCE_SYSTEM avoidance, ISLANDER_AVOIDANCE_AGENT agent, float* desiredTarget);
    void ENGINE_EXPORT IslanderClearAvoidanceAgentTarget(ISLANDER_AVOIDANCE_SYSTEM avoidance, ISLANDER_AVOIDANCE_AGENT agent);
    void ENGINE_EXPORT IslanderGetAvoidanceAgentData(ISLANDER_AVOIDANCE_SYSTEM avoidance, ISLANDER_AVOIDANCE_AGENT agent, IslanderAvoidanceAgentData* agentData);
    void ENGINE_EXPORT IslanderSetAvoidanceAgentActive(ISLANDER_AVOIDANCE_SYSTEM avoidance, ISLANDER_AVOIDANCE_AGENT agent, bool active);
    void ENGINE_EXPORT IslanderUpdateAvoidanceAgentFlags(ISLANDER_AVOIDANCE_SYSTEM avoidance, ISLANDER_AVOIDANCE_AGENT agent, int flags);
    void ENGINE_EXPORT IslanderAddAvoidanceType(ISLANDER_AVOIDANCE_SYSTEM avoidance, int idx, IslanderAvoidanceType* type);

    // Game controllers
    ISLANDER_GAMEPAD_DEVICE ENGINE_EXPORT IslanderCreateGamePadDevice();
    void ENGINE_EXPORT IslanderDestroyGamePadDevice(ISLANDER_GAMEPAD_DEVICE device);
    int ENGINE_EXPORT IslanderIsGamePadSupported();
    int ENGINE_EXPORT IslanderGamePadDetected(ISLANDER_GAMEPAD_DEVICE device);
    void ENGINE_EXPORT IslanderUpdateGamePadDevice(ISLANDER_GAMEPAD_DEVICE device);
    ISLANDER_GAMEPAD_STATE ENGINE_EXPORT IslanderGetGamepadState(ISLANDER_GAMEPAD_DEVICE device, int gamepad);
    void ENGINE_EXPORT IslanderGamePadRumble(ISLANDER_GAMEPAD_DEVICE device, int gamepad, float right, float left);

    // GUI
    void ENGINE_EXPORT IslanderImguiCreate(ISLANDER_DEVICE device, IslanderImguiContext* context);
    void ENGINE_EXPORT IslanderImguiRender(ISLANDER_DEVICE device, IslanderImguiContext* context);
    void ENGINE_EXPORT IslanderImguiNewFrame(ISLANDER_DEVICE device, IslanderImguiContext* context);
    void ENGINE_EXPORT IslanderImguiImage(ISLANDER_DEVICE device, IslanderImguiContext* context, int texture, float width, float height);
    void ENGINE_EXPORT IslanderImguiImagePos(ISLANDER_DEVICE device, IslanderImguiContext* context, int texture, float x, float y, float width, float height);
    int ENGINE_EXPORT IslanderImguiImageButton(ISLANDER_DEVICE device, IslanderImguiContext* context, int id, int texture, float x, float y, float sx, float sy);
    void ENGINE_EXPORT IslanderImguiDestroy();

	// Logging
	void ENGINE_EXPORT IslanderLog(const char* logMessage, int levels);
	void ENGINE_EXPORT IslanderSetLogLevels(int levels);
}
