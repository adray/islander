#pragma once
#include "..\..\Types\includes\CollisionTypes.h"
#include "..\..\Types\includes\RendererTypes.h"
#include "Exports.h"

namespace Islander
{
    bool ENGINE_EXPORT BoxSelect(ISLANDER_CAMERA camera, float minx, float miny, float maxx, float maxy, float screenWidth, float screenHeight, IslanderSelectionData* selectionData);
}
