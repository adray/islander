#pragma once

#ifdef Islander_Engine_EXPORTS
#ifdef _WIN32
#define ENGINE_EXPORT __declspec( dllexport )
#else
#define ENGINE_EXPORT __attribute__ ((visibility("default")))
#endif
#else
#ifdef _WIN32
#define ENGINE_EXPORT __declspec( dllimport ) 
#else
#define ENGINE_EXPORT __attribute__ ((visibility("default")))
#endif
#endif
