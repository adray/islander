#include "GamePad.h"
#include <Windows.h>
#include <Xinput.h>
#include <math.h>

#pragma comment(lib, "xinput.lib")

constexpr int CONTROLLER_TIMER_DELAY = 2000;
constexpr int MAX_THUMB_MAGNITUDE = 32767;

bool Islander::IsGamePadSupported()
{
    return true;
}

bool Islander::IsGamePadDetected(GamePadDevice* gamepad)
{
    bool detected = false;
    for (int i = 0; i < ISLANDER_GAMEPAD_COUNT; i++)
    {
        detected |= gamepad->GamePads[i].state.connected;
    }

    return detected;
}

Islander::GamePadDevice* Islander::CreateGamePadDevice()
{
    auto device = new Islander::GamePadDevice();
    device->newControllerTimer.Start();
    memset(device->GamePads, 0, sizeof(device->GamePads));
    return device;
}

void Islander::DestroyGamePadDevice(Islander::GamePadDevice* gamepad)
{
    delete gamepad;
}

void GetThumbState(float x, float y, int deadzone, float* rx, float* ry)
{
    float magnitude = sqrtf(x*x + y*y);

    if (magnitude > deadzone)
    {
        *rx = fabs(x) > deadzone ? x / SHRT_MAX : 0;
        *ry = fabs(y) > deadzone ? y / SHRT_MAX : 0;
    }
    else
    {
        *rx = 0;
        *ry = 0;
    }
}

void GetTriggerState(int v, int deadzone, float* rv)
{
    if (v > deadzone)
    {
        *rv = v / 255.0f;
    }
    else
    {
        *rv = 0;
    }
}

void Islander::UpdateGamePadDevice(GamePadDevice* gamepad)
{
    // Update state of connected gamepads.
    for (int i = 0; i < ISLANDER_GAMEPAD_COUNT; i++)
    {
        auto& g = gamepad->GamePads[i];
        if (g.state.connected)
        {
            XINPUT_STATE state;
            memset(&state, 0, sizeof(XINPUT_STATE));
            if (XInputGetState(i, &state) == ERROR_SUCCESS)
            {
                g.state.connected = true;

                if (state.dwPacketNumber > g.packetNumber)
                {
                    g.packetNumber = state.dwPacketNumber;

                    g.state.buttons[(unsigned int)ISLANDER_GAMEPAD_A] = (state.Gamepad.wButtons & XINPUT_GAMEPAD_A) == XINPUT_GAMEPAD_A;
                    g.state.buttons[(unsigned int)ISLANDER_GAMEPAD_B] = (state.Gamepad.wButtons & XINPUT_GAMEPAD_B) == XINPUT_GAMEPAD_B;
                    g.state.buttons[(unsigned int)ISLANDER_GAMEPAD_X] = (state.Gamepad.wButtons & XINPUT_GAMEPAD_X) == XINPUT_GAMEPAD_X;
                    g.state.buttons[(unsigned int)ISLANDER_GAMEPAD_Y] = (state.Gamepad.wButtons & XINPUT_GAMEPAD_Y) == XINPUT_GAMEPAD_Y;
                    g.state.buttons[(unsigned int)ISLANDER_GAMEPAD_START] = (state.Gamepad.wButtons & XINPUT_GAMEPAD_START) == XINPUT_GAMEPAD_START;
                    g.state.buttons[(unsigned int)ISLANDER_GAMEPAD_SELECT] = (state.Gamepad.wButtons & XINPUT_GAMEPAD_BACK) == XINPUT_GAMEPAD_BACK;
                    g.state.buttons[(unsigned int)ISLANDER_GAMEPAD_LEFTBUTTON] = (state.Gamepad.wButtons & XINPUT_GAMEPAD_LEFT_SHOULDER) == XINPUT_GAMEPAD_LEFT_SHOULDER;
                    g.state.buttons[(unsigned int)ISLANDER_GAMEPAD_RIGHTBUTTON] = (state.Gamepad.wButtons & XINPUT_GAMEPAD_RIGHT_SHOULDER) == XINPUT_GAMEPAD_RIGHT_SHOULDER;
                    g.state.buttons[(unsigned int)ISLANDER_GAMEPAD_DPADDOWN] = (state.Gamepad.wButtons & XINPUT_GAMEPAD_DPAD_DOWN) == XINPUT_GAMEPAD_DPAD_DOWN;
                    g.state.buttons[(unsigned int)ISLANDER_GAMEPAD_DPADUP] = (state.Gamepad.wButtons & XINPUT_GAMEPAD_DPAD_UP) == XINPUT_GAMEPAD_DPAD_UP;
                    g.state.buttons[(unsigned int)ISLANDER_GAMEPAD_DPADLEFT] = (state.Gamepad.wButtons & XINPUT_GAMEPAD_DPAD_LEFT) == XINPUT_GAMEPAD_DPAD_LEFT;
                    g.state.buttons[(unsigned int)ISLANDER_GAMEPAD_DPADRIGHT] = (state.Gamepad.wButtons & XINPUT_GAMEPAD_DPAD_RIGHT) == XINPUT_GAMEPAD_DPAD_RIGHT;
                    g.state.buttons[(unsigned int)ISLANDER_GAMEPAD_LEFTTHUMB] = (state.Gamepad.wButtons & XINPUT_GAMEPAD_LEFT_THUMB) == XINPUT_GAMEPAD_LEFT_THUMB;
                    g.state.buttons[(unsigned int)ISLANDER_GAMEPAD_RIGHTTHUMB] = (state.Gamepad.wButtons & XINPUT_GAMEPAD_RIGHT_THUMB) == XINPUT_GAMEPAD_RIGHT_THUMB;

                    GetThumbState((float)state.Gamepad.sThumbLX, (float)state.Gamepad.sThumbLY, XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE, &g.state.LX, &g.state.LY);
                    GetThumbState((float)state.Gamepad.sThumbRX, (float)state.Gamepad.sThumbRY, XINPUT_GAMEPAD_RIGHT_THUMB_DEADZONE, &g.state.RX, &g.state.RY);

                    GetTriggerState(state.Gamepad.bLeftTrigger, XINPUT_GAMEPAD_TRIGGER_THRESHOLD, &g.state.LeftTrigger);
                    GetTriggerState(state.Gamepad.bRightTrigger, XINPUT_GAMEPAD_TRIGGER_THRESHOLD, &g.state.RightTrigger);
                }
            }
            else
            {
                g.state.connected = false;
                g.packetNumber = 0;
            }
        }
    }

    // Query if new gamepads have been connected.
    if (gamepad->newControllerTimer.GetElapsedMilliseconds() >= CONTROLLER_TIMER_DELAY)
    {
        for (int i = 0; i < ISLANDER_GAMEPAD_COUNT; i++)
        {
            auto& g = gamepad->GamePads[i];
            if (!g.state.connected)
            {
                XINPUT_STATE state;
                memset(&state, 0, sizeof(XINPUT_STATE));
                if (XInputGetState(i, &state) == ERROR_SUCCESS)
                {
                    g.state.connected = true;

                    XINPUT_CAPABILITIES caps;
                    memset(&caps, 0, sizeof(caps));

                    if (XInputGetCapabilities(i, XINPUT_FLAG_GAMEPAD, &caps) == ERROR_SUCCESS)
                    {
                        g.supportsRumble = (caps.Flags & XINPUT_CAPS_FFB_SUPPORTED) == XINPUT_CAPS_FFB_SUPPORTED;
                        g.wireless = (caps.Flags & XINPUT_CAPS_WIRELESS) == XINPUT_CAPS_WIRELESS;
                    }
                }
            }
        }

        gamepad->newControllerTimer.Restart();
    }
}

void Islander::GamePadRumble(GamePadDevice* gamepad, int index, float right, float left)
{
    if (index < 0 || index >= ISLANDER_GAMEPAD_COUNT)
    {
        return;
    }

    if (gamepad->GamePads[index].state.connected &&
        gamepad->GamePads[index].supportsRumble)
    {
        XINPUT_VIBRATION vibration;
        memset(&vibration, 0, sizeof(vibration));
        vibration.wLeftMotorSpeed = (int)(left * 65535);
        vibration.wRightMotorSpeed = (int)(right * 65535);

        XInputSetState(index, &vibration);
    }
}
