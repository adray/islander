#include "GamePad.h"

bool Islander::IsGamePadSupported()
{
    return false;
}

Islander::GamePadDevice* Islander::CreateGamePadDevice()
{
    return nullptr;
}

void Islander::DestroyGamePadDevice(Islander::GamePadDevice* gamepad)
{
    // Nothing
}

bool Islander::IsGamePadDetected(Islander::GamePadDevice* gamepad)
{
    return false;
}

void Islander::UpdateGamePadDevice(Islander::GamePadDevice* gamepad)
{
    // Nothing
}

void Islander::GamePadRumble(Islander::GamePadDevice* gamepad, int index, float right, float left)
{
    //  Nothing
}
