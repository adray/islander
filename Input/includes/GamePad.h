#pragma once
#include "Stopwatch.h"
#include "RendererTypes.h"

namespace Islander
{

    struct GamePad
    {
        unsigned int packetNumber;
        bool supportsRumble;
        bool wireless;
        IslanderGamePadState state;
    };

    struct GamePadDevice
    {
        GamePad GamePads[ISLANDER_GAMEPAD_COUNT];
        Stopwatch newControllerTimer;
    };

    bool IsGamePadSupported();
    GamePadDevice* CreateGamePadDevice();
    void DestroyGamePadDevice(GamePadDevice* gamepad);
    bool IsGamePadDetected(GamePadDevice* gamepad);
    void UpdateGamePadDevice(GamePadDevice* gamepad);
    void GamePadRumble(GamePadDevice* gamepad, int index, float right, float left);
}
