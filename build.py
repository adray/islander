import subprocess
import os
import sys

islander_source = os.getcwd()

# Build libpng

# Build libpng's version of zlib dependency
os.chdir(islander_source)
os.chdir('thirdparty/lpng1637/projects/vstudio')

# libpng is sensitive, add slash to end
libpng_source = os.getcwd() + '\\'

os.chdir(libpng_source)
os.chdir('pnglibconf')
res = subprocess.call(['%MSBUILD%\msbuild.exe', 'pnglibconf.vcxproj', '/p:PlatformToolset=v140', '/p:Configuration=Release', '/p:SolutionDir=' + libpng_source], shell=True)
if res != 0:
	sys.exit(2)

os.chdir(libpng_source)
os.chdir('zlib')
res = subprocess.call(['%MSBUILD%\msbuild.exe', 'zlib.vcxproj', '/p:PlatformToolset=v140', '/p:Configuration=Release', '/p:SolutionDir=' + libpng_source], shell=True)
if res != 0:
	sys.exit(2)

os.chdir(libpng_source)
os.chdir('libpng')
res = subprocess.call(['%MSBUILD%\msbuild.exe', 'libpng.vcxproj', '/p:PlatformToolset=v140', '/p:Configuration=Release', '/p:SolutionDir=' + libpng_source], shell=True)
if res != 0:
	sys.exit(2)

# Build zlib

os.chdir(islander_source)
os.chdir('thirdparty/zlib-1.2.11/contrib/vstudio/vc14')

zlib_source = os.getcwd()

res = subprocess.call(['%MSBUILD%\msbuild.exe', 'zlibvc.vcxproj', '/p:PlatformToolset=v140', '/p:Configuration=Release'], shell=True)
if res != 0:
	sys.exit(2)

# Build glew

os.chdir(islander_source)
os.chdir('thirdparty/glew-1.10.0/build/vc14')

res = subprocess.call(['%MSBUILD%\msbuild.exe', 'glew_shared.vcxproj', '/p:PlatformToolset=v140', '/p:Configuration=Release'], shell=True)
if res != 0:
	sys.exit(2)

# Build Native projects

os.chdir(islander_source)
if not os.path.exists('build'):
	os.makedirs('build')
os.chdir('build')

res = subprocess.call(['%CMAKE%\cmake.exe',
	'-Dzlib_libs:PATH=' + islander_source + '/thirdparty//zlib-1.2.11/contrib/vstudio/vc10/x86/ZLibDllRelease',
	'-DGLEW_LIBS:PATH=' + islander_source + '/thirdparty/glew-1.10.0/lib',
	'-DLIBPNG_LIBS:PATH=' + islander_source + '/thirdparty/lpng1637/Projects/vstudio',
	'-DD3D11_LIBS:PATH=' + islander_source + '/thirdparty/d3d11/lib/x86',
	'-GVisual Studio 14 2015', islander_source],
	shell=True)
if res != 0:
	sys.exit(2)
	
res = subprocess.call(['%MSBUILD%\msbuild.exe', 'all_build.vcxproj', '/p:Configuration=Release'], shell=True)
if res != 0:
	sys.exit(2)

# Run Tests

os.chdir('bin')
os.chdir('Release')
subprocess.call(['Islander.Entity.Test.exe'])
	
os.chdir(islander_source)

# Build .NET projects

# managed
res = subprocess.call(['%MSBUILD%\msbuild.exe', './managed/islander.managed.csproj', '/p:Configuration=Release'], shell=True)
if res != 0:
	sys.exit(2)
res = subprocess.call(['%MSBUILD%\msbuild.exe', './managed/islander.managed.csproj', '/p:Configuration=Debug'], shell=True) # make linking easier..
if res != 0:
	sys.exit(2)
# ui
res = subprocess.call(['%MSBUILD%\msbuild.exe', './ui/islander.ui.csproj', '/p:Configuration=Release'], shell=True)
if res != 0:
	sys.exit(2)


# Raft
res = subprocess.call(['%MSBUILD%\msbuild.exe', './raft/raft.csproj', '/p:Configuration=Release'], shell=True)
if res != 0:
	sys.exit(2)
	
