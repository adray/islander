cmake_minimum_required(VERSION 2.8.12)

project(islander)

set (Islander_VERSION_MAJOR 0)
set (Islander_VERSION_MINOR 1)
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/bin)
#set(CMAKE_LIBRARY_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/lib)

#if(MSVC AND NOT EXISTS "${CMAKE_BINARY_DIR}/ALL_BUILD.vcxproj.user")
#	file(GENERATE
#		OUTPUT "${CMAKE_BINARY_DIR}/ALL_BUILD.vcxproj.user"
#		INPUT "${CMAKE_SOURCE_DIR}/cmake/ALL_BUILD.vcxproj.user.in")
#endif()

set(CMAKE_CXX_STANDARD 17)

if(WIN32)
	add_definitions(-DUNICODE -D_UNICODE)
endif()

if(MSVC)
	set(CMAKE_C_FLAGS_DEBUG "/DDEBUG=1 /D_DEBUG=1 ${CMAKE_C_FLAGS_DEBUG}")
	set(CMAKE_CXX_FLAGS_DEBUG "/DDEBUG=1 /D_DEBUG=1 ${CMAKE_CXX_FLAGS_DEBUG}")
endif()

add_subdirectory(Entity)
add_subdirectory(Common)
add_subdirectory(Crimson)
add_subdirectory(Numerics)
add_subdirectory(Model)
add_subdirectory(Audio)
add_subdirectory(Renderer)
add_subdirectory(AI)
add_subdirectory(Collision)
add_subdirectory(Physics)
add_subdirectory("Input")
add_subdirectory(Engine)
add_subdirectory(EntityTest)
add_subdirectory(EngineTest)

# copy dll to output directory 
file(GLOB ISLANDER_SHARED_RELEASE
 ./thirdparty//zlib-1.2.7/contrib/vstudio/vc10/x86/ZLibDllRelease/*.dll 
 ./thirdparty/glew-1.10.0/bin/release/win32/*.dll
 ./thirdparty/lpng1610/Projects/vstudio/release/*.dll)

file(COPY ${ISLANDER_SHARED_RELEASE} DESTINATION ${CMAKE_BINARY_DIR}/bin/release)

