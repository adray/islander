#include "AnimationController.h"
#include "Polygon.h"
#include "Matrix.h"
#include "glm\gtx\quaternion.hpp"
#include "Vector.h"
#include "AnimationTypes.h"
#include <limits>

using namespace Islander::Numerics;
using namespace Islander::Model;

Islander::Model::AnimationSystem* Islander::Model::CreateAnimationSystem()
{
    return new Islander::Model::AnimationSystem();
}

void Islander::Model::DestroyAnimationSystem(AnimationSystem* system)
{
    delete system;
}

void ResetController(Islander::Model::AnimationController& controller)
{
    controller.animation.animationId = 0;
    controller.animation.elapsedTime = 0.0f;
    controller.animation.lastElapsedTime = 0.0f;
    controller.animation.speedScale = 1;
    controller.animation.loop = false;
    controller.animation.mask = std::numeric_limits<uint64_t>::max();
    controller.playing = false;
    controller.state = Islander::Model::AnimationState::Normal;
    controller.alive = true;
    controller.animation.isRootMotion = false;
    controller.animation.rootMotionFlags = ISLANDER_ANIMATION_ROOT_MOTION_FLAGS_NONE;
}

int Islander::Model::CreateAnimationController(Islander::Model::AnimationSystem* system)
{
    if (system->slots.size() > 0)
    {
        int id = system->slots.back();
        system->slots.pop_back();
        ResetController(system->controllers[id]);
        return id;
    }
    else
    {
        system->controllers.emplace_back(AnimationController());
        auto it = system->controllers.rbegin();
        it->id = system->controllers.size() - 1;
        it->alive = true;
        ResetController(*it);
        return it->id;
    }
}

void Islander::Model::DestroyAnimationController(Islander::Model::AnimationSystem* system, int controller)
{
    if (controller >= 0 && controller < system->controllers.size() && system->controllers[controller].alive)
    {
        system->controllers[controller].alive = false;
        system->slots.push_back(controller);
    }
}

void Islander::Model::PlayAnimation(AnimationSystem* system, int controller, int animationId, bool loop)
{
    auto& it = system->controllers.at(controller);
    it.animation.elapsedTime = 0.0f;
    it.animation.lastElapsedTime = 0.0f;
    it.animation.loop = loop;
    it.playing = true;
    it.animation.animationId = animationId;
    it.animation.endTime = std::numeric_limits<float>::max();
    it.animation.isRootMotion = false;
    it.animation.rootMotionFlags = ISLANDER_ANIMATION_ROOT_MOTION_FLAGS_NONE;
    it.state = Islander::Model::AnimationState::Normal;
}

void Islander::Model::PauseAnimation(AnimationSystem* system, int controller)
{
    auto& it = system->controllers.at(controller);
    it.playing = false;
}

void Islander::Model::ResetAnimation(AnimationSystem* system, int controller)
{
    auto& it = system->controllers.at(controller);
    it.animation.elapsedTime = 0.0f;
    it.animation.lastElapsedTime = 0.0f;
}

void Islander::Model::SetAnimationSpeed(AnimationSystem* system, int controller, float speedScale)
{
    auto& it = system->controllers.at(controller);
    it.animation.speedScale = speedScale;
}

void Islander::Model::SetAnimationRootMotion(AnimationSystem* system, int controller, bool isRootMotion, int rootMotionFlags)
{
    auto& it = system->controllers.at(controller);
    it.animation.isRootMotion = isRootMotion;
    it.animation.rootMotionFlags = rootMotionFlags;
}

void Islander::Model::BlendToAnimation(AnimationSystem* system, int controller, int animationId, float blendTime)
{
    auto& it = system->controllers.at(controller);
    it.blend.prevAnimationId = it.animation.animationId;
    it.blend.elapsedBlendTime = 0.0f;
    it.blend.frozenTime = it.animation.elapsedTime;
    it.blend.blendTime = blendTime;
    it.blend.isRootMotion = it.animation.isRootMotion;
    it.blend.rootMotionFlags = it.animation.rootMotionFlags;
    it.blend.mask = it.animation.mask;
    it.animation.animationId = animationId;
    it.animation.elapsedTime = 0.0f;
    it.animation.lastElapsedTime = 0.0f;
    it.animation.loop = false;
    it.animation.speedScale = 1;
    it.animation.mask = std::numeric_limits<uint64_t>::max();
    it.animation.endTime = std::numeric_limits<float>::max();
    it.animation.isRootMotion = false;
    it.animation.rootMotionFlags = ISLANDER_ANIMATION_ROOT_MOTION_FLAGS_NONE;
    it.state = Islander::Model::AnimationState::BlendToAnimation;
}

void Islander::Model::PlayBlendedAnimation(AnimationSystem* system, int controller, int animationId1, int animationId2, std::uint64_t mask, bool loop1, bool loop2)
{
    // Blended animation is animation where the first animation is played with AND of the mask (which refers to bones).
    // The second animation is with the AND of the INVERSE of the mask.

    auto& it = system->controllers.at(controller);
    it.animation.elapsedTime = 0.0f;
    it.animation.lastElapsedTime = 0.0f;
    it.animation.loop = loop1;
    it.animation.loop2 = loop2;
    it.playing = true;
    it.animation.animationId = animationId1;
    it.animation.animationId2 = animationId2;
    it.animation.mask = mask;
    it.animation.endTime = std::numeric_limits<float>::max();
    it.animation.isRootMotion = false;
    it.animation.rootMotionFlags = ISLANDER_ANIMATION_ROOT_MOTION_FLAGS_NONE;
    it.state = Islander::Model::AnimationState::Blended;
}

void Islander::Model::SetAnimationTimeBounds(AnimationSystem* system, int controller, float startTime, float endTime)
{
    auto& it = system->controllers.at(controller);
    it.animation.elapsedTime = startTime;
    it.animation.endTime = endTime;
}

//void Islander::SetAnimationMask(AnimationSystem* system, int controller, std::uint64_t bones, bool masked)
//{
//    auto& it = system->controllers.at(controller);
//    if (masked)
//    {
//        it.animation.mask |= bones;
//    }
//    else
//    {
//        it.animation.mask &= ~bones;
//    }
//}

void Islander::Model::RunAnimationSystem(AnimationSystem* system, float delta)
{
    for (auto& controller : system->controllers)
    {
        if (controller.playing && controller.alive)
        {
            controller.animation.lastElapsedTime = controller.animation.elapsedTime;

            switch (controller.state)
            {
            case Islander::Model::AnimationState::BlendToAnimation:
                controller.blend.elapsedBlendTime += delta;
                if (controller.blend.elapsedBlendTime >= controller.blend.blendTime)
                {
                    controller.state = Islander::Model::AnimationState::Normal;
                }
                break;
            case Islander::Model::AnimationState::Normal:
            case Islander::Model::AnimationState::Blended:
                controller.animation.elapsedTime += delta * controller.animation.speedScale;
                break;
            }

            // Bound the elapsed to end time.
            if (controller.animation.elapsedTime > controller.animation.endTime)
            {
                controller.animation.elapsedTime = controller.animation.endTime;
            }
        }
    }
}

float Interpolate(float a, float b, float t)
{
    return a + (b - a) * t;
}

void CalculateBoneTransform(Islander::Model::PolygonAnimationFrameBone* bone, float* pos, float* scale, glm::qua<float>& quat, float time)
{
    int start = 0;
    int end = 0;

    for (int i = 1; i < bone->frames.size(); i++)
    {
        if (bone->frames.at(i).time > time)
        {
            start = i - 1;
            end = i;
            break;
        }
    }

    if (start == 0 && end == 0)
    {
        auto f = bone->frames.rbegin();
        pos[0] = f->x;
        pos[1] = f->y;
        pos[2] = f->z;

        scale[0] = f->sx;
        scale[1] = f->sy;
        scale[2] = f->sz;

        quat.w = f->q[0];
        quat.x = f->q[1];
        quat.y = f->q[2];
        quat.z = f->q[3];
    }
    else
    {
        auto& f1 = bone->frames.at(start);
        auto& f2 = bone->frames.at(end);

        float t = (time - f1.time) / (f2.time - f1.time);

        glm::qua<float> q1;
        q1.w = f1.q[0];
        q1.x = f1.q[1];
        q1.y = f1.q[2];
        q1.z = f1.q[3];

        glm::qua<float> q2;
        q2.w = f2.q[0];
        q2.x = f2.q[1];
        q2.y = f2.q[2];
        q2.z = f2.q[3];

        quat = glm::slerp(q1, q2, t);
        pos[0] = Interpolate(f1.x, f2.x, t);
        pos[1] = Interpolate(f1.y, f2.y, t);
        pos[2] = Interpolate(f1.z, f2.z, t);
        scale[0] = Interpolate(f1.sx, f2.sx, t);
        scale[1] = Interpolate(f1.sy, f2.sy, t);
        scale[2] = Interpolate(f1.sz, f2.sz, t);
    }
}

void Islander::Model::CalculateBoneMatrixForParenting(Islander::Numerics::Matrix4x4* boneMatrix, Islander::Model::PolygonAnimation* animation, Islander::Model::PolygonJoint* parent, Islander::Numerics::Matrix4x4* animationFrames, Islander::Numerics::Matrix4x4& transform, Islander::Numerics::Matrix4x4& inverseMeshTransform)
{
    Matrix4x4 t(Islander::Numerics::Matrix4x4Type::MATRIX_4X4_IDENTITY);

    // Find the animation frame associated with bone
    for (int i = 0; i < animation->boneCount; i++)
    {
        if (animation->bones[i].bone != nullptr && parent->id == animation->bones[i].bone->id)
        {
            t = animationFrames[i]; // "global transform"
            break;
        }
    }

    // For the bind pose this should come to the identity matrix (maybe not in all cases?)

    Matrix4x4::Multiply(t, transform, &t); // (parent->parent->transform * parent->transform) * t
    boneMatrix[parent->id] = t;
    Matrix4x4::Multiply(boneMatrix[parent->id], inverseMeshTransform, &boneMatrix[parent->id]);

    for (int i = 0; i < parent->childCount; i++)
    {
        CalculateBoneMatrixForParenting(boneMatrix, animation, parent->children[i], animationFrames, t, inverseMeshTransform);
    }
}

void Islander::Model::CalculateBoneTransforms(Islander::Model::PolygonAnimations* animations, Islander::Numerics::Matrix4x4* animationFrameTransforms, float elapsedTime1, float elapsedTime2, AnimationSystem* system, int controllerId)
{
    // Loop through the bones and calculate the transform for the animation frame
    // When the state is in blend mode calculate the blending between the two different animations (NLERP?)

    auto& controller = system->controllers[controllerId];
    auto anim = &animations->animations[controller.animation.animationId];

    float pos[Islander::Model::MAX_BONES * 3];
    float scale[Islander::Model::MAX_BONES * 3];
    glm::qua<float> quat[Islander::Model::MAX_BONES];

    for (int i = 0; i < anim->boneCount; i++)
    {
        animationFrameTransforms[i] = Matrix4x4(Islander::Numerics::Matrix4x4Type::MATRIX_4X4_IDENTITY);
        CalculateBoneTransform(&anim->bones[i], &pos[i * 3], &scale[i * 3], quat[i], elapsedTime1);
    }

    if (controller.state == AnimationState::BlendToAnimation)
    {
        // TODO: prevAnimationId could be -1
        auto prevAnim = &animations->animations[controller.blend.prevAnimationId];

        float t = controller.blend.elapsedBlendTime / controller.blend.blendTime;

        for (int i = 0; i < anim->boneCount; i++)
        {
            float* p = &pos[i * 3];
            float* s = &scale[i * 3];

            float prevPos[3];
            float prevScale[3];
            glm::qua<float> prevRot;

            CalculateBoneTransform(&prevAnim->bones[i], prevPos, prevScale, prevRot, controller.blend.frozenTime);

            auto rot = glm::eulerAngles(glm::lerp(prevRot, quat[i], t));

            Matrix4x4::CreateTransformationMatrix(Interpolate(prevPos[0], p[0], t),
                Interpolate(prevPos[1], p[1], t),
                Interpolate(prevPos[2], p[2], t),
                rot.x,
                rot.y,
                rot.z,
                Interpolate(prevScale[0], s[0], t),
                Interpolate(prevScale[1], s[1], t),
                Interpolate(prevScale[2], s[2], t),
                &animationFrameTransforms[i]);
        }
    }
    else if (controller.state == AnimationState::Normal)
    {
        for (int i = 0; i < anim->boneCount; i++)
        {
            float* p = &pos[i * 3];
            float* s = &scale[i * 3];
            auto rot = glm::eulerAngles(quat[i]);

            Matrix4x4::CreateTransformationMatrix(p[0],
                p[1],
                p[2],
                rot.x,
                rot.y,
                rot.z,
                s[0],
                s[1],
                s[2],
                &animationFrameTransforms[i]);
        }
    }
    else if (controller.state == AnimationState::Blended)
    {
        auto anim2 = &animations->animations[controller.animation.animationId2];
        
        float pos2[Islander::Model::MAX_BONES * 3];
        float scale2[Islander::Model::MAX_BONES * 3];
        glm::qua<float> quat2[Islander::Model::MAX_BONES];

        for (int i = 0; i < anim->boneCount; i++)
        {
            CalculateBoneTransform(&anim2->bones[i], &pos2[i * 3], &scale2[i * 3], quat2[i], elapsedTime2);
        }

        for (int i = 0; i < anim->boneCount; i++)
        {
            if ((controller.animation.mask & (1 << i)) == 1 << i)
            {
                float* p = &pos[i * 3];
                float* s = &scale[i * 3];
                auto rot = glm::eulerAngles(quat[i]);

                Matrix4x4::CreateTransformationMatrix(p[0],
                    p[1],
                    p[2],
                    rot.x,
                    rot.y,
                    rot.z,
                    s[0],
                    s[1],
                    s[2],
                    &animationFrameTransforms[i]);
            }
            else
            {
                float* p = &pos2[i * 3];
                float* s = &scale2[i * 3];
                auto rot = glm::eulerAngles(quat2[i]);

                Matrix4x4::CreateTransformationMatrix(p[0],
                    p[1],
                    p[2],
                    rot.x,
                    rot.y,
                    rot.z,
                    s[0],
                    s[1],
                    s[2],
                    &animationFrameTransforms[i]);
            }
        }
    }
}

void Islander::Model::CalculateTransformForPose(Islander::Model::PolygonModel* parent_poly, AnimationSystem* animSystem, int controllerId, int parentBone, Matrix4x4& parent)
{
    auto skeleton = parent_poly->meshes[0].skeleton;
    Matrix4x4& inverseMeshTransform = parent_poly->meshes[0].transform;

    int animId = 0;
    float elapsed = 0;
    float len = 0;
    float elapsed2 = 0;
    float len2 = 0;

    if (animSystem != nullptr && controllerId >= 0)
    {
        auto& controller = animSystem->controllers[controllerId];
        animId = controller.animation.animationId;
        elapsed = controller.animation.elapsedTime;
        len = GetAnimationLength(parent_poly, animId);

        if (controller.animation.loop)
        {
            int iterations = (int)(elapsed / len);
            elapsed = elapsed - (iterations * len);
        }
        else
        {
            elapsed = std::min(elapsed, len);
        }

        if (controller.state == AnimationState::Blended)
        {
            elapsed2 = controller.animation.elapsedTime;
            len2 = GetAnimationLength(parent_poly, controller.animation.animationId2);

            if (controller.animation.loop2)
            {
                int iterations = (int)(elapsed2 / len2);
                elapsed2 = elapsed2 - (iterations * len2);
            }
            else
            {
                elapsed2 = std::min(elapsed2, len2);
            }
        }

        auto anim = &parent_poly->animations->animations[animId];

        static Matrix4x4 boneMatrix[Islander::Model::MAX_BONES];
        static Matrix4x4 animationTransforms[Islander::Model::MAX_BONES];
        CalculateBoneTransforms(parent_poly->animations, animationTransforms, elapsed, elapsed2, animSystem, controllerId);
        CalculateBoneMatrixForParenting(&boneMatrix[0], anim, skeleton->root, animationTransforms, animationTransforms[0], inverseMeshTransform);

        //float px, py, pz, qx, qy, qz, qw;

        //Matrix4x4::Decompose(boneMatrix[mesh->mesh.parentBone], &px, &py, &pz, &qx, &qy, &qz, &qw);

        //Matrix4x4::TransformVector(parent, &px, &py, &pz);
        //parent = Matrix4x4(MATRIX_4X4_IDENTITY);
        //Matrix4x4::CreateTranslationMatrix(px, py, pz, &parent);

        Matrix4x4::Multiply(boneMatrix[parentBone], parent, &parent);
    }
}

void GetPoseTransform(Islander::Model::PolygonAnimation* anim, int bone, float elapsedTime, Matrix4x4& mat)
{
    float pos1[3] = {};
    float scale1[3] = {};
    glm::quat quat1;

    CalculateBoneTransform(&anim->bones[bone], pos1, scale1, quat1, elapsedTime);

    glm::vec3 rot1 = glm::eulerAngles(quat1);

    mat = Matrix4x4::Matrix4x4(Matrix4x4Type::MATRIX_4X4_IDENTITY);
    Matrix4x4::CreateTransformationMatrix(pos1[0],
        pos1[1],
        pos1[2],
        rot1.x,
        rot1.y,
        rot1.z,
        scale1[0],
        scale1[1],
        scale1[2],
        &mat);
}

void CalculateRootPosition(Islander::Model::PolygonModel* model, int animation, float elapsedTime, const Islander::Numerics::Matrix4x4& modelTransform, float* rootPos)
{
    auto anim = &model->animations->animations[animation];

    // Find the animation frame associated with bone
    int bone = 0;
    for (int i = 0; i < anim->boneCount; i++)
    {
        if (anim->bones[i].bone != nullptr && /*parent->id*/0 == anim->bones[i].bone->id)
        {
            bone = i; // "global transform"
            break;
        }
    }

    // Calculate the transform of the root bone of the animation at the specified time
    Islander::Numerics::Matrix4x4 mat;
    GetPoseTransform(anim, bone, elapsedTime, mat);

    Islander::Numerics::Matrix4x4 root;
    GetPoseTransform(anim, 0, elapsedTime, root);

    // Multiple the transforms by the inverse mesh transform
    Matrix4x4& inverseMeshTransform = model->meshes[0].transform;

    Matrix4x4::Multiply(mat, root, &mat);
    Matrix4x4::Multiply(mat, inverseMeshTransform, &mat);

    // Account for scaling or rotation set on the mesh.
    Matrix4x4::Multiply(mat, modelTransform, &mat);

    rootPos[0] = 0.0f;
    rootPos[1] = 0.0f;
    rootPos[2] = 0.0f;
    Islander::Numerics::Matrix4x4::TransformVector(mat, &rootPos[0], &rootPos[1], &rootPos[2]);
}

void Islander::Model::CalculateRootMotion(Islander::Model::PolygonModel* model, int animation1, int animation2, float elapsedTime1, float elapsedTime2, const float* scale, const float* rot, float* displacement)
{
    Islander::Numerics::Matrix4x4 transform = Islander::Numerics::Matrix4x4(Islander::Numerics::Matrix4x4Type::MATRIX_4X4_IDENTITY);
    Islander::Numerics::Matrix4x4::CreateTransformationMatrix(0.0f, 0.0f, 0.0f, rot[0], rot[1], rot[2], scale[0], scale[1], scale[2], &transform);

    float initialPos[3] = {};
    float newPos[3] = {};
    CalculateRootPosition(model, animation1, elapsedTime1, transform, initialPos);
    CalculateRootPosition(model, animation2, elapsedTime2, transform, newPos);

    // Then calculate the displacement
    SubVec3(newPos, initialPos, displacement);
}

void Islander::Model::CalculateRootMotion(Islander::Model::PolygonModel* model, AnimationSystem* animSystem, int controllerId, bool fromLastElapsed, const float* rot, const float* scale, float* displacement)
{
    int animId = 0;
    float elapsed = 0.0f;
    float len = 0.0f;
    float elapsed2 = 0.0f;
    float len2 = 0.0f;
    float lastElapsed = 0.0f;

    if (model && animSystem && controllerId >= 0)
    {
        auto& controller = animSystem->controllers[controllerId];
        animId = controller.animation.animationId;
        elapsed = controller.animation.elapsedTime;
        len = GetAnimationLength(model, animId);
        lastElapsed = fromLastElapsed ? controller.animation.lastElapsedTime : 0.0f;

        if (controller.animation.loop)
        {
            int iterations = (int)(elapsed / len);
            elapsed = elapsed - (iterations * len);
            if (fromLastElapsed)
            {
                lastElapsed = lastElapsed - (iterations * len);
            }
        }
        else
        {
            elapsed = std::min(elapsed, len);
            if (fromLastElapsed)
            {
                lastElapsed = std::min(lastElapsed, len);
            }
        }

        if (controller.state == AnimationState::Normal)
        {
            CalculateRootMotion(model, animId, animId, lastElapsed, elapsed, scale, rot, displacement);
        }
        else if (controller.state == AnimationState::BlendToAnimation)
        {
            Islander::Numerics::Matrix4x4 transform = Islander::Numerics::Matrix4x4(Islander::Numerics::Matrix4x4Type::MATRIX_4X4_IDENTITY);
            Islander::Numerics::Matrix4x4::CreateTransformationMatrix(0.0f, 0.0f, 0.0f, rot[0], rot[1], rot[2], scale[0], scale[1], scale[2], &transform);

            float curPos[3] = {};
            CalculateRootPosition(model, controller.animation.animationId, elapsed, transform, curPos); // current animation time

            if (fromLastElapsed)
            {
                displacement[0] = 0.0f;
                displacement[1] = 0.0f;
                displacement[2] = 0.0f;
            }
            else
            {
                float frozenPos[3] = {};
                CalculateRootPosition(model, controller.blend.prevAnimationId, controller.blend.frozenTime, transform, frozenPos); // last animation

                const float t = controller.blend.elapsedBlendTime / controller.blend.blendTime;
                displacement[0] = Interpolate(frozenPos[0], curPos[0], t);
                displacement[1] = Interpolate(frozenPos[1], curPos[1], t);
                displacement[2] = Interpolate(frozenPos[2], curPos[2], t);
            }
        }

        int rootMotionFlags = controller.animation.rootMotionFlags;
        if ((rootMotionFlags & ISLANDER_ANIMATION_ROOT_MOTION_FLAGS_DISABLE_Y) == ISLANDER_ANIMATION_ROOT_MOTION_FLAGS_DISABLE_Y)
        {
            displacement[1] = 0.0f;
        }
    }
}
