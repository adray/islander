#include "Heightmap.h"
#include "Vector.h"
#include <algorithm>
#include <stack>
#include <atomic>

#define MAX_HEIGHT 256.0f
#define VERTEX_STRIDE 20  // pos, colour, normal, tex, indices, weights, flags

bool Islander::Model::Private::PointCircleTest(float* pt, float* centre, float radius)
{
    float dt[3];
    Islander::Numerics::SubVec3(pt, centre, dt);
    return Islander::Numerics::LengthSqr(dt) < radius;
}

bool Islander::Model::Private::CircleBoxTest(float* min, float* max, float* centre, float radius)
{
    if (PointCircleTest(min, centre, radius))
    {
        return true;
    }

    if (PointCircleTest(max, centre, radius))
    {
        return true;
    }

    float p1[3] = {
        min[0],
        0.0f,
        max[2]
    };

    if (PointCircleTest(p1, centre, radius))
    {
        return true;
    }

    float p2[3] = {
        max[0],
        0.0f,
        min[2]
    };

    if (PointCircleTest(p2, centre, radius))
    {
        return true;
    }

    if (min[0] > centre[0] + radius)
    {
        return false;
    }

    if (min[2] > centre[2] + radius)
    {
        return false;
    }

    if (max[0] < centre[0] - radius)
    {
        return false;
    }

    if (max[2] < centre[2] - radius)
    {
        return false;
    }

    return true;
}

int Islander::Model::Private::GetHeightmapChunkID(int x, int z, int height)
{
    return z + x * height;
}

int Islander::Model::GetHeightmapChunkID(Heightmap* heightmap, int x, int z)
{
    const int chunkHeight = heightmap->height / heightmap->chunkHeight;
    return z + x * chunkHeight;
}

int Islander::Model::GetHeightmapChunk(Islander::Model::Heightmap* heightmap, float* pos, int& cellX, int& cellZ)
{
    float range[3] = {
        heightmap->max[0] - heightmap->min[0],
        heightmap->max[1] - heightmap->min[1],
        heightmap->max[2] - heightmap->min[2]
    };

    int numWidth = heightmap->width / heightmap->chunkWidth;
    int numHeight = heightmap->height / heightmap->chunkHeight;

    float cellSizeX = range[0] / numWidth;
    float cellSizeZ = range[2] / numHeight;

    cellX = (pos[0] - heightmap->min[0]) / cellSizeX;
    cellZ = (pos[2] - heightmap->min[2]) / cellSizeZ;

    // Inside the terrain bounds
    if (cellX < 0 || cellZ < 0 || cellX >= numWidth || cellZ >= numHeight)
    {
        return -1;
    }

    return Islander::Model::Private::GetHeightmapChunkID(cellX, cellZ, numHeight);
}

Islander::Model::Heightmap* Islander::Model::CreateHeightmapFromImageData(int width, int height, unsigned char* data)
{
    Islander::Model::Heightmap* heightmap = new Islander::Model::Heightmap();
    heightmap->height = height;
    heightmap->width = width;
    heightmap->data = data;
    heightmap->textureIndexData = nullptr;
    heightmap->textureWeightData = nullptr;
    heightmap->chunkWidth = 0;
    heightmap->chunkHeight = 0;
    heightmap->numChunks = 0;
    heightmap->chunks = nullptr;
    std::memset(&heightmap->scale, 0, sizeof(heightmap->scale));
    heightmap->chunkUpdate = nullptr;
    return heightmap;
}

void Islander::Model::SetHeightmapTextureData(Heightmap* heightmap, unsigned char* textureIndexData, unsigned char* textureWeightData)
{
    heightmap->textureIndexData = textureIndexData;
    heightmap->textureWeightData = textureWeightData;
}

void Islander::Model::SetChunkParameters(Heightmap* heightmap, int chunkWidth, int chunkHeight)
{
    heightmap->chunkWidth = chunkWidth;
    heightmap->chunkHeight = chunkHeight;
}

void SampleWeights(Islander::Model::Heightmap* heightmap, int x, int y, float* weights)
{
    unsigned char* h = &heightmap->textureWeightData[(x * heightmap->height + y) * 4];

    weights[0] = float(h[0] / 255.0f);
    weights[1] = float(h[1] / 255.0f);
    weights[2] = float(h[2] / 255.0f);
    weights[3] = float(h[3] / 255.0f);
}

void SetWeights(Islander::Model::Heightmap* heightmap, int x, int y, float* weights)
{
    unsigned char* h = &heightmap->textureWeightData[(x * heightmap->height + y) * 4];

    h[0] = weights[0] * 255.0f;
    h[1] = weights[1] * 255.0f;
    h[2] = weights[2] * 255.0f;
    h[3] = weights[3] * 255.0f;
}

void SampleIndices(Islander::Model::Heightmap* heightmap, int x, int y, int* indices)
{
    unsigned char* h = &heightmap->textureIndexData[(x * heightmap->height + y) * 4];

    indices[0] = int(h[0]);
    indices[1] = int(h[1]);
    indices[2] = int(h[2]);
    indices[3] = int(h[3]);
}

void SetIndices(Islander::Model::Heightmap* heightmap, int x, int y, int* indices)
{
    unsigned char* h = &heightmap->textureIndexData[(x * heightmap->height + y) * 4];

    h[0] = char(indices[0]);
    h[1] = char(indices[1]);
    h[2] = char(indices[2]);
    h[3] = char(indices[3]);
}

unsigned char SampleFlags(Islander::Model::Heightmap* heightmap, int x, int y)
{
    unsigned char b = heightmap->data[(x * heightmap->height + y) * 4 + 2]; // the blue channel contains the flags
    return b;
}

void SetHeightmapFlags(Islander::Model::Heightmap* heightmap, int x, int y, unsigned char flags)
{
    heightmap->data[(x * heightmap->height + y) * 4 + 2] = flags;
}

float SampleHeight(Islander::Model::Heightmap* heightmap, int x, int y)
{
    // Red and green channels are used for the height. 
    unsigned char r = heightmap->data[(x * heightmap->height + y) * 4];
    unsigned char g = heightmap->data[(x * heightmap->height + y) * 4 + 1];
    int h = r + 256 * g;
    return float(h / 255.0f);
}

void SetHeightmap(Islander::Model::Heightmap* heightmap, int x, int y, float value)
{
    int h = int(value * 255.0f);
    unsigned char r = unsigned char(h % 256);
    unsigned char g = unsigned char(h / 256);

    heightmap->data[(x * heightmap->height + y) * 4] = r;
    heightmap->data[(x * heightmap->height + y) * 4 + 1] = g;
}

float BilinearInterpolate(float* m11, float* m12, float* m21, float* m22, float x, float y)
{
    float a = (m21[0] - x) / (m21[0] - m11[0]) * m11[1] + (x - m11[0]) / (m21[0] - m11[0]) * m21[1];
    float b = (m22[0] - x) / (m22[0] - m12[0]) * m12[1] + (x - m12[0]) / (m22[0] - m12[0]) * m22[1];

    float c = (m12[2] - y) / (m12[2] - m11[2]) * a + (y - m11[2]) / (m12[2] - m11[2]) * b;
    return c;
}

bool Islander::Model::SampleHeightmap(Heightmap* heightmap, float x, float z, float* height)
{
    if (heightmap && heightmap->data)
    {
        int x_ = (x / heightmap->scale[0]) * heightmap->width;
        int z_ = (z / heightmap->scale[2]) * heightmap->height;

        if (x_ >= 0 && x_ < heightmap->width-1 &&
            z_ >= 0 && z_ < heightmap->height-1)
        {
            // Bilinear sampling

            float m11[3] = { (float)x_, SampleHeight(heightmap, x_, z_), (float)z_ };
            float m12[3] = { (float)x_, SampleHeight(heightmap, x_, z_ + 1), (float)z_ + 1 };
            float m21[3] = { (float)x_ + 1, SampleHeight(heightmap, x_ + 1, z_), (float)z_ };
            float m22[3] = { (float)x_ + 1, SampleHeight(heightmap, x_ + 1, z_ + 1), (float)z_ + 1 };

            float h = BilinearInterpolate(m11, m12, m21, m22, (x / heightmap->scale[0]) * heightmap->width, (z / heightmap->scale[2]) * heightmap->height);

            *height = heightmap->scale[1] * h;
            return true;
        }
    }

    return false;
}

void FindQuadTreeNodes(Islander::Model::HeightmapChunk* heightmapChunk, float* pos, float radius, std::vector<int>& hitQuads)
{
    if (heightmapChunk && pos && heightmapChunk->loaded)
    {
        std::stack<int> quadIds;
        for (int i = 0; i < 4; i++)
        {
            auto& quad = heightmapChunk->quadTree[i];
            if (Islander::Model::Private::CircleBoxTest(quad.min, quad.max, pos, radius))
            {
                quadIds.push(i);
            }
        }

        while (!quadIds.empty())
        {
            auto& parent = heightmapChunk->quadTree[quadIds.top()];
            quadIds.pop();
            for (int i = 0; i < parent.numNodes; i++)
            {
                auto& quad = heightmapChunk->quadTree[parent.childNodes[i]];
                if (Islander::Model::Private::CircleBoxTest(quad.min, quad.max, pos, radius))
                {
                    // Continue iterating if non-leaf node, otherwise if it is a leaf node add to hit data.
                    if (quad.numNodes > 0)
                    {
                        quadIds.push(parent.childNodes[i]);
                    }
                    else
                    {
                        hitQuads.push_back(parent.childNodes[i]);
                    }
                }
            }
        }
    }
}

int Islander::Model::FindQuadTreeNodeAtPoint(HeightmapChunk* heightmapChunk, float* pos)
{
    if (heightmapChunk && pos)
    {
        int quadId = -1;
        for (int i = 0; i < 4; i++)
        {
            auto& quad = heightmapChunk->quadTree[i];
            if (pos[0] >= quad.min[0] && /*pos[1] >= quad.min[1] &&*/ pos[2] >= quad.min[2] &&
                pos[0] <= quad.max[0] && /*pos[1] <= quad.max[1] &&*/ pos[2] <= quad.max[2])
            {
                quadId = i;
                break;
            }
        }

        while (quadId > -1 && heightmapChunk->quadTree[quadId].numNodes > 0)
        {
            auto& parent = heightmapChunk->quadTree[quadId];
            for (int i = 0; i < parent.numNodes; i++)
            {
                auto& quad = heightmapChunk->quadTree[parent.childNodes[i]];
                if (pos[0] >= quad.min[0] && /*pos[1] >= quad.min[1] &&*/ pos[2] >= quad.min[2] &&
                    pos[0] <= quad.max[0] && /*pos[1] <= quad.max[1] &&*/ pos[2] <= quad.max[2])
                {
                    quadId = parent.childNodes[i];
                    break;
                }
            }
        }

        return quadId;
    }

    return -1;
}

bool Islander::Model::HeightmapIntersect(Heightmap* heightmap, int chunkId, float* min, float* max)
{
    auto& chunk = heightmap->chunks[chunkId];
    
    return (min[0] <= chunk.max[0] && max[0] >= chunk.min[0]) &&
            (min[1] <= chunk.max[1] && max[1] >= chunk.min[1]) &&
            (min[2] <= chunk.max[2] && max[2] >= chunk.min[2]);
}

void AddQuadTreeNode(Islander::Model::HeightmapChunk* heightmapChunk, int parent, float minX, float minY, float minZ, float maxX, float maxY, float maxZ)
{
    Islander::Model::HeightmapQuad quad;
    quad.numNodes = 0;
    quad.parentNode = parent;
    quad.min[0] = minX;
    quad.min[1] = minY;
    quad.min[2] = minZ;
    quad.max[0] = maxX;
    quad.max[1] = maxY;
    quad.max[2] = maxZ;
    heightmapChunk->quadTree.push_back(quad);
}

void GenerateQuadTree(Islander::Model::Heightmap* heightmap, int chunkId)
{
    auto chunk = &heightmap->chunks[chunkId];

    // Build quad tree
    
    const float halfX = (chunk->max[0] - chunk->min[0]) / 2.0f;
    const float halfZ = (chunk->max[2] - chunk->min[2]) / 2.0f;

    const int numIterations = 4;
    int startIndex = 0;
    int endIndex = 4;

    AddQuadTreeNode(chunk, -1, chunk->min[0], /*heightmap->min[1]*/chunk->min[1], chunk->min[2], chunk->min[0] + halfX, /*heightmap->max[1]*/chunk->max[1], chunk->min[2] + halfZ);
    AddQuadTreeNode(chunk, -1, chunk->min[0] + halfX, /*heightmap->min[1]*/chunk->min[1], chunk->min[2], chunk->max[0], /*heightmap->max[1]*/chunk->max[1], chunk->min[2] + halfZ);
    AddQuadTreeNode(chunk, -1, chunk->min[0], /*heightmap->min[1]*/chunk->min[1], chunk->min[2] + halfZ, chunk->min[0] + halfX, /*heightmap->max[1]*/chunk->max[1], chunk->max[2]);
    AddQuadTreeNode(chunk, -1, chunk->min[0] + halfX, /*heightmap->min[1]*/chunk->min[1], chunk->min[2] + halfZ, chunk->max[0], /*heightmap->max[1]*/chunk->max[1], chunk->max[2]);

    for (int i = 0; i < numIterations; i++)
    {
        for (int j = startIndex; j < endIndex; j++)
        {
            // Split quad
            {
                auto& node = chunk->quadTree[j];

                node.numNodes = 4;
                node.childNodes[0] = chunk->quadTree.size();
                node.childNodes[1] = chunk->quadTree.size() + 1;
                node.childNodes[2] = chunk->quadTree.size() + 2;
                node.childNodes[3] = chunk->quadTree.size() + 3;
            }

            {
                auto nodeCopy = chunk->quadTree[j]; // intentional copy, AddQuadTreeNode may resize the array
                const float halfX = (nodeCopy.max[0] - nodeCopy.min[0]) / 2.0f;
                const float halfZ = (nodeCopy.max[2] - nodeCopy.min[2]) / 2.0f;
                AddQuadTreeNode(chunk, j, nodeCopy.min[0], nodeCopy.min[1], nodeCopy.min[2], nodeCopy.min[0] + halfX, nodeCopy.max[1], nodeCopy.min[2] + halfZ);
                AddQuadTreeNode(chunk, j, nodeCopy.min[0] + halfX, nodeCopy.min[1], nodeCopy.min[2], nodeCopy.max[0], nodeCopy.max[1], nodeCopy.min[2] + halfZ);
                AddQuadTreeNode(chunk, j, nodeCopy.min[0], nodeCopy.min[1], nodeCopy.min[2] + halfZ, nodeCopy.min[0] + halfX, nodeCopy.max[1], nodeCopy.max[2]);
                AddQuadTreeNode(chunk, j, nodeCopy.min[0] + halfX, nodeCopy.min[1], nodeCopy.min[2] + halfZ, nodeCopy.max[0], nodeCopy.max[1], nodeCopy.max[2]);
            }
        }

        startIndex = endIndex;
        endIndex = chunk->quadTree.size();
    }

    // Insert vertices into the quad tree

    int quadId = -1;
    for (int i = 0; i < chunk->numIndices; i += 3)
    {
        int i1 = chunk->indexData[i];

        if (quadId > -1) // If we are still in the same quad node, we can skip searching through the quad tree.
        {
            float* vertexPos = (float*)&chunk->vertexData[i1 * heightmap->vertexStride * sizeof(float)];
            auto& quad = chunk->quadTree[quadId];
            if (!(vertexPos[0] >= quad.min[0] && vertexPos[1] >= quad.min[1] && vertexPos[2] >= quad.min[2] &&
                vertexPos[0] <= quad.max[0] && vertexPos[1] <= quad.max[1] && vertexPos[2] <= quad.max[2]))
            {
                quadId = -1;
            }
        }
        
        if (quadId == -1)
        {
            quadId = FindQuadTreeNodeAtPoint(chunk, (float*)&chunk->vertexData[i1 * heightmap->vertexStride * sizeof(float)]);
        }

        if (quadId > -1)
        {
            auto& quad = chunk->quadTree[quadId];
            if (quad.startIndex.size() == 0)
            {
                quad.startIndex.push_back(i);
                quad.numIndices.push_back(3);
            }
            else if (quad.startIndex[quad.numIndices.size() - 1] + quad.numIndices[quad.numIndices.size() - 1] == i)
            {
                quad.numIndices[quad.numIndices.size() - 1] += 3;
            }
            else
            {
                quad.startIndex.push_back(i);
                quad.numIndices.push_back(3);
            }
        }
    }
    
    // TODO: may be not be possible..
    // Resize the leaf nodes to the height of the verts
    //for (int q = 0; q < chunk->quadTree.size(); q++)
    //{
    //    auto& quad = chunk->quadTree[q];
    //    if (quad.numNodes == 0)
    //    {
    //        float minHeight = std::numeric_limits<float>::max();
    //        float maxHeight = std::numeric_limits<float>::lowest();

    //        for (int i = 0; i < quad.numIndices.size(); i++) // triangle strips
    //        {
    //            for (int j = 0; j < quad.numIndices[i]; j += 3) // each triangle in strip
    //            {
    //                int i1 = chunk->indexData[(quad.startIndex[i] + j)] * heightmap->vertexStride * sizeof(float);
    //                int i2 = chunk->indexData[(quad.startIndex[i] + j + 1)] * heightmap->vertexStride * sizeof(float);
    //                int i3 = chunk->indexData[(quad.startIndex[i] + j + 2)] * heightmap->vertexStride * sizeof(float);

    //                float* v1 = (float*)&chunk->vertexData[i1];
    //                float* v2 = (float*)&chunk->vertexData[i2];
    //                float* v3 = (float*)&chunk->vertexData[i3];

    //                minHeight = std::min(minHeight, std::min(v1[1], std::min(v2[1], v3[1])));
    //                maxHeight = std::max(maxHeight, std::max(v1[1], std::max(v2[1], v3[1])));
    //            }
    //        }

    //        quad.min[1] = minHeight;
    //        quad.max[1] = maxHeight;
    //    }
    //}
}

void GenerateAABB(Islander::Model::Heightmap* heightmap, int chunkId)
{
    auto& chunk = heightmap->chunks[chunkId];

    chunk.min[0] = std::numeric_limits<float>::max();
    chunk.min[1] = std::numeric_limits<float>::max();
    chunk.min[2] = std::numeric_limits<float>::max();
    
    chunk.max[0] = std::numeric_limits<float>::lowest();
    chunk.max[1] = std::numeric_limits<float>::lowest();
    chunk.max[2] = std::numeric_limits<float>::lowest();

    for (int i = 0; i < chunk.numVertices; i++)
    {
        float pos[3];
        Islander::Numerics::CopyVec3((float*)(chunk.vertexData + i * heightmap->vertexStride * sizeof(float)), pos);

        chunk.min[0] = std::min(chunk.min[0], pos[0]);
        chunk.min[1] = std::min(chunk.min[1], pos[1]);
        chunk.min[2] = std::min(chunk.min[2], pos[2]);

        chunk.max[0] = std::max(chunk.max[0], pos[0]);
        chunk.max[1] = std::max(chunk.max[1], pos[1]);
        chunk.max[2] = std::max(chunk.max[2], pos[2]);
    }
}

void GenerateVertices(Islander::Model::Heightmap* heightmap, int chunkId, const float* scale)
{
    auto& chunk = heightmap->chunks[chunkId];
    for (int i = 0; i < chunk.width; i++)
    {
        for (int j = 0; j < chunk.height; j++)
        {
            //float* vertexData = (float*) &heightmap->vertexData[(i * heightmap->height + j) * heightmap->vertexStride * sizeof(float)];
            float* vertexData = (float*)&chunk.vertexData[(i * chunk.height + j) * heightmap->vertexStride * sizeof(float)];

            int x = i + chunk.x;
            int y = j + chunk.y;

            // Pos

            vertexData[0] = x * scale[0] / heightmap->width;
            vertexData[1] = SampleHeight(heightmap, x, y) * scale[1];
            vertexData[2] = y * scale[2] / heightmap->height;

            // Colour

            vertexData[3] = 0.0f;
            vertexData[4] = 0.0f;
            vertexData[5] = 0.0f;

            // Normal

            vertexData[6] = 0.0f;
            vertexData[7] = 0.0f;
            vertexData[8] = 0.0f;

            // Tex

            vertexData[9] = float(x);
            vertexData[10] = float(y);

            // Texture indicies

            int* textureIndices = (int*)&chunk.vertexData[(i * chunk.height + j) * heightmap->vertexStride * sizeof(float)];

            if (heightmap->textureIndexData)
            {
                SampleIndices(heightmap, x, y, &textureIndices[11]);
            }

            // Texture weights

            if (heightmap->textureWeightData)
            {
                SampleWeights(heightmap, x, y, &vertexData[15]);
            }

            // Flags

            int* flags = (int*)&chunk.vertexData[(i * chunk.height + j) * heightmap->vertexStride * sizeof(float)];
            flags[19] = SampleFlags(heightmap, x, y);
        }
    }
}

void GenerateIndices(Islander::Model::Heightmap* heightmap, int chunkId)
{
    int index = 0;
    auto& chunk = heightmap->chunks[chunkId];

    for (int i = 0; i < chunk.width - 1; i++)
    {
        for (int j = 0; j < chunk.height - 1; j++)
        {
            // First tri
            chunk.indexData[index++] = i * chunk.height + j;
            chunk.indexData[index++] = (i+1) * chunk.height + j + 1;
            chunk.indexData[index++] = (i+1) * chunk.height + j;

            // Second tri
            chunk.indexData[index++] = i * chunk.height + j;
            chunk.indexData[index++] = i * chunk.height + j + 1;
            chunk.indexData[index++] = (i + 1) * chunk.height + j + 1;
        }
    }
}

float* GetVertexPos(Islander::Model::HeightmapChunk* chunk, int vertexStride, int x, int y)
{
    if (x < 0 || y < 0 || x >= chunk->width || y >= chunk->height)
    {
        return nullptr;
    }

    float* vertexData = (float*)&chunk->vertexData[(x * chunk->height + y) * vertexStride * sizeof(float)];
    return vertexData;
}

void CalculateNormal(float* pt, float* p1, float* p2, float* p3, float* p4, float* normal)
{
    normal[0] = 0.0f;
    normal[1] = 0.0f;
    normal[2] = 0.0f;

    int count = 0;
    if (p1 && p3)
    {
        float t1[3]; float t2[3]; float dst[3];
        Islander::Numerics::SubVec3(p3, pt, t1);
        Islander::Numerics::SubVec3(p1, pt, t2);
        Islander::Numerics::CrossVec3(t1, t2, dst);
        Islander::Numerics::NormalizeVec3(dst);
        Islander::Numerics::AddVec3(dst, normal, normal);
        count++;
    }

    if (p1 && p4)
    {
        float t1[3]; float t2[3]; float dst[3];
        Islander::Numerics::SubVec3(p1, pt, t1);
        Islander::Numerics::SubVec3(p4, pt, t2);
        Islander::Numerics::CrossVec3(t1, t2, dst);
        Islander::Numerics::NormalizeVec3(dst);
        Islander::Numerics::AddVec3(dst, normal, normal);
        count++;
    }

    if (p2 && p3)
    {
        float t1[3]; float t2[3]; float dst[3];
        Islander::Numerics::SubVec3(p2, pt, t1);
        Islander::Numerics::SubVec3(p3, pt, t2);
        Islander::Numerics::CrossVec3(t1, t2, dst);
        Islander::Numerics::NormalizeVec3(dst);
        Islander::Numerics::AddVec3(dst, normal, normal);
        count++;
    }

    if (p2 && p4)
    {
        float t1[3]; float t2[3]; float dst[3];
        Islander::Numerics::SubVec3(p4, pt, t1);
        Islander::Numerics::SubVec3(p2, pt, t2);
        Islander::Numerics::CrossVec3(t1, t2, dst);
        Islander::Numerics::NormalizeVec3(dst);
        Islander::Numerics::AddVec3(dst, normal, normal);
        count++;
    }

    normal[0] /= count;
    normal[1] /= count;
    normal[2] /= count;

    Islander::Numerics::NormalizeVec3(normal);
}

void GenerateNormals(Islander::Model::Heightmap* heightmap, int chunkId)
{
    auto& chunk = heightmap->chunks[chunkId];

    for (int i = 0; i < chunk.width; i++)
    {
        for (int j = 0; j < chunk.height; j++)
        {
            float* p1 = GetVertexPos(&chunk, heightmap->vertexStride, i - 1, j);
            float* p2 = GetVertexPos(&chunk, heightmap->vertexStride, i + 1, j);
            float* p3 = GetVertexPos(&chunk, heightmap->vertexStride, i, j - 1);
            float* p4 = GetVertexPos(&chunk, heightmap->vertexStride, i, j + 1);
            float* pt = GetVertexPos(&chunk, heightmap->vertexStride, i, j);

            CalculateNormal(pt, p1, p2, p3, p4, &pt[6]);
        }
    }
}

void GenerateChunk(Islander::Model::Heightmap* heightmap, Islander::Model::HeightmapChunk& chunk, const int vertexStride, int i, int j)
{
    int height = j > 0 ? (heightmap->chunkHeight + 1) : heightmap->chunkHeight;
    int width = i > 0 ? (heightmap->chunkWidth + 1) : heightmap->chunkWidth;

    chunk.numVertices = width * height;
    chunk.numIndices = (height - 1) * (width - 1) * 6;

    chunk.vertexData = new char[sizeof(float) * vertexStride * chunk.numVertices];
    chunk.indexData = new int[chunk.numIndices];

    chunk.x = heightmap->chunkWidth * i - (i > 0 ? 1 : 0);
    chunk.y = heightmap->chunkHeight * j - (j > 0 ? 1 : 0);

    chunk.height = height;
    chunk.width = width;
}

void BuildCollisionBox(Islander::Model::Heightmap* heightmap)
{
    heightmap->min[0] = std::numeric_limits<float>::max();
    heightmap->min[1] = std::numeric_limits<float>::max();
    heightmap->min[2] = std::numeric_limits<float>::max();
    heightmap->max[0] = std::numeric_limits<float>::lowest();
    heightmap->max[1] = std::numeric_limits<float>::lowest();
    heightmap->max[2] = std::numeric_limits<float>::lowest();
    //for (int i = 0; i < heightmap->numChunks; i++) {
    //    auto& chunk = heightmap->chunks[i];
    //    if (chunk.loaded)
    //    {
    //        heightmap->min[0] = std::min(heightmap->min[0], chunk.min[0]);
    //        heightmap->min[1] = std::min(heightmap->min[1], chunk.min[1]);
    //        heightmap->min[2] = std::min(heightmap->min[2], chunk.min[2]);

    //        heightmap->max[0] = std::max(heightmap->max[0], chunk.max[0]);
    //        heightmap->max[1] = std::max(heightmap->max[1], chunk.max[1]);
    //        heightmap->max[2] = std::max(heightmap->max[2], chunk.max[2]);
    //    }
    //}
    for (int i = 0; i < heightmap->numChunks; i++) {
        auto& chunk = heightmap->chunks[i];
        if (chunk.loaded)
        {
            heightmap->min[1] = std::min(heightmap->min[1], chunk.min[1]);
            heightmap->max[1] = std::max(heightmap->max[1], chunk.max[1]);
        }
    }

    int height = heightmap->height + 1;
    int width = heightmap->width + 1;

    heightmap->min[0] = 0.0f;
    heightmap->min[2] = 0.0f;
    heightmap->max[0] = width * heightmap->scale[0] / heightmap->width;
    heightmap->max[2] = height * heightmap->scale[2] / heightmap->height;
}

void RebuildHeightmap(Islander::Model::Heightmap* heightmap)
{
    if (heightmap->chunks)
    {
        for (int i = 0; i < heightmap->numChunks; i++)
        {
            auto& chunk = heightmap->chunks[i];
            if (chunk.dirty && chunk.loaded)
            {
                delete[] chunk.indexData;
                delete[] chunk.vertexData;
            }
        }
    }

    if (heightmap->width > 0 && heightmap->height > 0 && heightmap->chunkHeight > 0 && heightmap->chunkWidth > 0)
    {
        const int vertexStride = VERTEX_STRIDE;
        heightmap->vertexStride = vertexStride;

        int chunkId = 0;
        for (int i = 0; i < heightmap->width / heightmap->chunkWidth; i++)
        {
            for (int j = 0; j < heightmap->height / heightmap->chunkHeight; j++)
            {
                auto& chunk = heightmap->chunks[chunkId++];

                if (chunk.dirty && chunk.loaded)
                {
                    GenerateChunk(heightmap, chunk, vertexStride, i, j);
                }
            }
        }

        for (int i = 0; i < heightmap->numChunks; i++)
        {
            if (heightmap->chunks[i].dirty && heightmap->chunks[i].loaded)
            {
                GenerateVertices(heightmap, i, heightmap->scale);
                GenerateIndices(heightmap, i);
                GenerateNormals(heightmap, i);
                GenerateAABB(heightmap, i);
            }
        }

        BuildCollisionBox(heightmap);

        for (int i = 0; i < heightmap->numChunks; i++) {
            auto& chunk = heightmap->chunks[i];
            if (chunk.dirty && chunk.loaded)
            {
                chunk.quadTree.clear();
                GenerateQuadTree(heightmap, i);
            }
        }

        for (int i = 0; i < heightmap->numChunks; i++)
        {
            heightmap->chunks[i].dirty = false;
        }
    }
}

void BuildHeightmap3(Islander::Model::Heightmap* heightmap, const float* scale, bool buildChunks)
{
    if (heightmap->chunks)
    {
        for (int i = 0; i < heightmap->numChunks; i++)
        {
            delete[] heightmap->chunks[i].indexData;
            delete[] heightmap->chunks[i].vertexData;
        }
        heightmap->numChunks = 0;

        delete[] heightmap->chunks;
        heightmap->chunks = nullptr;
    }

    Islander::Numerics::CopyVec3(scale, heightmap->scale);

    if (heightmap->width > 0 && heightmap->height > 0 && heightmap->chunkHeight > 0 && heightmap->chunkWidth > 0)
    {
        heightmap->numChunks = (heightmap->width / heightmap->chunkWidth) * (heightmap->height / heightmap->chunkHeight);
        heightmap->chunks = new Islander::Model::HeightmapChunk[heightmap->numChunks];

        const int vertexStride = VERTEX_STRIDE;
        heightmap->vertexStride = vertexStride;

        if (buildChunks)
        {
            int chunkId = 0;
            for (int i = 0; i < heightmap->width / heightmap->chunkWidth; i++)
            {
                for (int j = 0; j < heightmap->height / heightmap->chunkHeight; j++)
                {
                    auto& chunk = heightmap->chunks[chunkId++];

                    GenerateChunk(heightmap, chunk, vertexStride, i, j);

                    chunk.dirty = false;
                    chunk.loaded = true;
                }
            }

            for (int i = 0; i < heightmap->numChunks; i++)
            {
                GenerateVertices(heightmap, i, scale);
                GenerateIndices(heightmap, i);
                GenerateNormals(heightmap, i);
                GenerateAABB(heightmap, i);
            }

            BuildCollisionBox(heightmap);

            for (int i = 0; i < heightmap->numChunks; i++) {
                GenerateQuadTree(heightmap, i);
            }
        }
        else
        {
            for (int i = 0; i < heightmap->numChunks; i++) {
                auto& chunk = heightmap->chunks[i];
                chunk.dirty = false;
                chunk.loaded = false;
                chunk.vertexData = nullptr;
                chunk.indexData = nullptr;
                chunk.numIndices = 0;
                chunk.numVertices = 0;
            }
            BuildCollisionBox(heightmap);
        }
    }
}

void Islander::Model::BuildHeightmap(Islander::Model::Heightmap* heightmap, const float* scale)
{
    BuildHeightmap3(heightmap, scale, true);
}

void Islander::Model::BuildHeightmap2(Islander::Model::Heightmap* heightmap, const float* scale)
{
    BuildHeightmap3(heightmap, scale, false);
}

void Islander::Model::DestroyHeightmap(Heightmap* heightmap)
{
    if (heightmap)
    {
        for (int i = 0; i < heightmap->numChunks; i++)
        {
            delete[] heightmap->chunks[i].indexData;
            delete[] heightmap->chunks[i].vertexData;
        }
        heightmap->numChunks = 0;
        delete[] heightmap->chunks;
        heightmap->chunks = nullptr;
        delete[] heightmap->chunkUpdate;
        heightmap->chunkUpdate = nullptr;
    }
}

int Islander::Model::LoadChunk(Heightmap* heightmap, int x, int z)
{
    int chunk = -1;
    if (heightmap)
    {
        chunk = Islander::Model::Private::GetHeightmapChunkID(x, z, heightmap->height / heightmap->chunkHeight);

        if (!heightmap->chunks[chunk].loaded)
        {
            GenerateChunk(heightmap, heightmap->chunks[chunk], heightmap->vertexStride, x, z);
            heightmap->chunks[chunk].dirty = false;

            GenerateVertices(heightmap, chunk, heightmap->scale);
            GenerateIndices(heightmap, chunk);
            GenerateNormals(heightmap, chunk);
            GenerateAABB(heightmap, chunk);

            BuildCollisionBox(heightmap);
            GenerateQuadTree(heightmap, chunk);

            //std::atomic_thread_fence();
            heightmap->chunks[chunk].loaded = true;
        }
    }
    return chunk;
}

void Islander::Model::UnloadChunk(Heightmap* heightmap, int x, int z)
{
    if (heightmap)
    {
        const int id = Islander::Model::Private::GetHeightmapChunkID(x, z, heightmap->height / heightmap->chunkHeight);

        auto& chunk = heightmap->chunks[id];
        if (chunk.loaded)
        {
            chunk.quadTree.clear();
            delete[] chunk.indexData;
            delete[] chunk.vertexData;
            chunk.loaded = false;
        }
    }
}

void InvalidateHeightmap(Islander::Model::Heightmap* heightmap, float x, float z, float radius)
{
    if (heightmap)
    {
        delete[] heightmap->chunkUpdate;
        heightmap->chunkUpdate = nullptr;

        heightmap->chunkUpdate = new unsigned char[heightmap->numChunks];
        std::memset(heightmap->chunkUpdate, 0, sizeof(unsigned char) * heightmap->numChunks);

        float centre[3] = {
            x, 0.0f, z
        };

        for (int i = 0; i < heightmap->numChunks; i++)
        {
            auto& chunk = heightmap->chunks[i];
            if (chunk.loaded && Islander::Model::Private::CircleBoxTest(chunk.min, chunk.max, centre, radius))
            {
                chunk.dirty = true;
                heightmap->chunkUpdate[i] = 1;
            }
        }
    }
}

bool WithinRadius(float centreX, float centreZ, float radius, float ptX, float ptZ)
{
    float dx = ptX - centreX;
    float dz = ptZ - centreZ;

    float dist = dx * dx + dz * dz;
    return dist < radius * radius;
}

void Islander::Model::SmoothHeightmap(Heightmap* heightmap, float x, float z, float radius, float strength)
{
    if (heightmap && heightmap->data)
    {
        if (radius < 0.0f)
        {
            return;
        }

        if (strength < 0.0f || strength > 1.0f)
        {
            return;
        }

        int x_ = (x / heightmap->scale[0]) * heightmap->width;
        int z_ = (z / heightmap->scale[2]) * heightmap->height;

        int minx = ((x - radius) / heightmap->scale[0]) * heightmap->width;
        int maxx = ((x + radius) / heightmap->scale[0]) * heightmap->width;

        int minz = ((z - radius) / heightmap->scale[2]) * heightmap->height;
        int maxz = ((z + radius) / heightmap->scale[2]) * heightmap->height;

        int numSamples = 0;
        float average = 0.0f;
        for (int i = minx; i <= maxx; i++)
        {
            for (int j = minz; j <= maxz; j++)
            {
                if (i >= 0 && i < heightmap->width &&
                    j >= 0 && j < heightmap->height &&
                    WithinRadius(x, z, radius, (i / float(heightmap->width)) * heightmap->scale[0], (j / float(heightmap->height)) * heightmap->scale[2]))
                {
                    average += SampleHeight(heightmap, i, j);
                    numSamples++;
                }
            }
        }

        if (numSamples > 0)
        {
            average /= float(numSamples);

            for (int i = minx; i <= maxx; i++)
            {
                for (int j = minz; j <= maxz; j++)
                {
                    if (i >= 0 && i < heightmap->width &&
                        j >= 0 && j < heightmap->height &&
                        WithinRadius(x, z, radius, (i / float(heightmap->width)) * heightmap->scale[0], (j / float(heightmap->height)) * heightmap->scale[2]))
                    {
                        float height = SampleHeight(heightmap, i, j);
                        float delta = (average - height) * strength;

                        height += delta;
                        height = std::min(std::max(height, 0.0f), MAX_HEIGHT);
                        SetHeightmap(heightmap, i, j, height);
                    }
                }
            }

            InvalidateHeightmap(heightmap, x, z, radius);
            RebuildHeightmap(heightmap);
        }
    }
}

void Islander::Model::RaiseHeightmap(Heightmap* heightmap, float x, float z, float radius, int step)
{
    if (heightmap && heightmap->data)
    {
        if (radius < 0)
        {
            return;
        }

        int x_ = (x / heightmap->scale[0]) * heightmap->width;
        int z_ = (z / heightmap->scale[2]) * heightmap->height;

        int minx = ((x - radius) / heightmap->scale[0]) * heightmap->width;
        int maxx = ((x + radius) / heightmap->scale[0]) * heightmap->width;

        int minz = ((z - radius) / heightmap->scale[2]) * heightmap->height;
        int maxz = ((z + radius) / heightmap->scale[2]) * heightmap->height;

        for (int i = minx; i <= maxx; i++)
        {
            for (int j = minz; j <= maxz; j++)
            {
                if (i >= 0 && i < heightmap->width &&
                    j >= 0 && j < heightmap->height &&
                    WithinRadius(x, z, radius, (i / float(heightmap->width)) * heightmap->scale[0], (j / float(heightmap->height)) * heightmap->scale[2]))
                {
                    float height = SampleHeight(heightmap, i, j);
                    height += step / 255.0f;
                    height = std::min(std::max(height, 0.0f), MAX_HEIGHT);
                    SetHeightmap(heightmap, i, j, height);
                }
            }
        }

        InvalidateHeightmap(heightmap, x, z, radius);
        RebuildHeightmap(heightmap);
    }
}

void Islander::Model::LowerHeightmap(Heightmap* heightmap, float x, float z, float radius, int step)
{
    RaiseHeightmap(heightmap, x, z, radius, -step);
}

void Islander::Model::LevelHeightmap(Heightmap* heightmap, float x, float z, float radius, HeightmapEditingFlags flags)
{
    if (heightmap && heightmap->data)
    {
        if (radius < 0)
        {
            return;
        }

        bool lower = ((int)flags & (int)HeightmapEditingFlags::Level_OnlyRaise) != (int)HeightmapEditingFlags::Level_OnlyRaise;
        bool raise = ((int)flags & (int)HeightmapEditingFlags::Level_OnlyLower) != (int)HeightmapEditingFlags::Level_OnlyLower;

        int x_ = (x / heightmap->scale[0]) * heightmap->width;
        int z_ = (z / heightmap->scale[2]) * heightmap->height;

        int minx = ((x - radius) / heightmap->scale[0]) * heightmap->width;
        int maxx = ((x + radius) / heightmap->scale[0]) * heightmap->width;

        int minz = ((z - radius) / heightmap->scale[2]) * heightmap->height;
        int maxz = ((z + radius) / heightmap->scale[2]) * heightmap->height;

        if (x_ >= 0 && x_ < heightmap->width &&
            z_ >= 0 && z_ < heightmap->height)
        {
            float height = SampleHeight(heightmap, x_, z_);

            for (int i = minx; i <= maxx; i++)
            {
                for (int j = minz; j <= maxz; j++)
                {
                    if (i >= 0 && i < heightmap->width &&
                        j >= 0 && j < heightmap->height &&
                        WithinRadius(x, z, radius, (i / float(heightmap->width)) * heightmap->scale[0], (j / float(heightmap->height)) * heightmap->scale[2]))
                    {
                        float y = SampleHeight(heightmap, i, j);
                        if ((lower && y > height) || (raise && y < height))
                        {
                            SetHeightmap(heightmap, i, j, height);
                        }
                    }
                }
            }

            InvalidateHeightmap(heightmap, x, z, radius);
            RebuildHeightmap(heightmap);
        }
    }
}

template<int numWeights>
void NormalizeWeights(float* weights)
{
    float sum = 0.0f;
    for (int i = 0; i < numWeights; i++)
    {
        sum += weights[i];
    }

    for (int i = 0; i < numWeights; i++)
    {
        weights[i] /= sum;
    }
}

template<int T>
int FindWeightIndex(int indicies[4], float weights[T], int textureID)
{
    for (int i = 0; i < T; i++)
    {
        if (indicies[i] == textureID)
        {
            return i;
        }
    }

    for (int i = 0; i < T; i++)
    {
        if (weights[i] == 0.0f)
        {
            return i;
        }
    }

    return -1;
}

void Islander::Model::EraseHeightmap(Heightmap* heightmap, float x, float z, float radius)
{
    if (heightmap && heightmap->textureIndexData && heightmap->textureWeightData)
    {
        if (radius < 0)
        {
            return;
        }

        int x_ = (x / heightmap->scale[0]) * heightmap->width;
        int z_ = (z / heightmap->scale[2]) * heightmap->height;

        int minx = ((x - radius) / heightmap->scale[0]) * heightmap->width;
        int maxx = ((x + radius) / heightmap->scale[0]) * heightmap->width;

        int minz = ((z - radius) / heightmap->scale[2]) * heightmap->height;
        int maxz = ((z + radius) / heightmap->scale[2]) * heightmap->height;

        const int numWeights = 4;

        int indices[numWeights] = {};
        float weights[numWeights] = {};

        if (x_ >= 0 && x_ < heightmap->width &&
            z_ >= 0 && z_ < heightmap->height)
        {
            for (int i = minx; i <= maxx; i++)
            {
                for (int j = minz; j <= maxz; j++)
                {
                    if (i >= 0 && i < heightmap->width &&
                        j >= 0 && j < heightmap->height &&
                        WithinRadius(x, z, radius, (i / float(heightmap->width)) * heightmap->scale[0], (j / float(heightmap->height)) * heightmap->scale[2]))
                    {
                        SetIndices(heightmap, i, j, indices);
                        SetWeights(heightmap, i, j, weights);
                    }
                }
            }

            InvalidateHeightmap(heightmap, x, z, radius);
            RebuildHeightmap(heightmap);
        }
    }
}

void Islander::Model::PaintHeightmap(Heightmap* heightmap, float x, float z, float radius, float strength, int textureIndex)
{
    if (heightmap && heightmap->textureIndexData && heightmap->textureWeightData)
    {
        if (radius < 0)
        {
            return;
        }

        if (textureIndex < 0 || textureIndex > 255)
        {
            return;
        }

        int x_ = (x / heightmap->scale[0]) * heightmap->width;
        int z_ = (z / heightmap->scale[2]) * heightmap->height;

        int minx = ((x - radius) / heightmap->scale[0]) * heightmap->width;
        int maxx = ((x + radius) / heightmap->scale[0]) * heightmap->width;

        int minz = ((z - radius) / heightmap->scale[2]) * heightmap->height;
        int maxz = ((z + radius) / heightmap->scale[2]) * heightmap->height;

        const int numWeights = 4;
        const float invStrength = 1 - strength;

        if (x_ >= 0 && x_ < heightmap->width &&
            z_ >= 0 && z_ < heightmap->height)
        {
            for (int i = minx; i <= maxx; i++)
            {
                for (int j = minz; j <= maxz; j++)
                {
                    if (i >= 0 && i < heightmap->width &&
                        j >= 0 && j < heightmap->height &&
                        WithinRadius(x, z, radius, (i / float(heightmap->width)) * heightmap->scale[0], (j / float(heightmap->height)) * heightmap->scale[2]))
                    {
                        int indices[4]; float weights[4];
                        SampleIndices(heightmap, x_, z_, indices);
                        SampleWeights(heightmap, x_, z_, weights);

                        int weightIndex = FindWeightIndex<numWeights>(indices, weights, textureIndex);
                        if (weightIndex > -1)
                        {
                            weights[weightIndex] = weights[weightIndex] * invStrength + strength;
                            indices[weightIndex] = textureIndex;
                            for (int idx = 0; idx < numWeights; idx++)
                            {
                                if (idx != weightIndex)
                                {
                                    weights[idx] *= invStrength;
                                }
                            }

                            NormalizeWeights<numWeights>(weights);

                            SetIndices(heightmap, i, j, indices);
                            SetWeights(heightmap, i, j, weights);
                        }
                    }
                }
            }

            InvalidateHeightmap(heightmap, x, z, radius);
            RebuildHeightmap(heightmap);
        }
    }
}

void Islander::Model::ToggleActiveHeightmap(Heightmap* heightmap, float x, float z, float radius, bool active)
{
    if (heightmap && heightmap->data)
    {
        int x_ = (x / heightmap->scale[0]) * heightmap->width;
        int z_ = (z / heightmap->scale[2]) * heightmap->height;

        int minx = ((x - radius) / heightmap->scale[0]) * heightmap->width;
        int maxx = ((x + radius) / heightmap->scale[0]) * heightmap->width;

        int minz = ((z - radius) / heightmap->scale[2]) * heightmap->height;
        int maxz = ((z + radius) / heightmap->scale[2]) * heightmap->height;

        if (x_ >= 0 && x_ < heightmap->width &&
            z_ >= 0 && z_ < heightmap->height)
        {
            for (int i = minx; i <= maxx; i++)
            {
                for (int j = minz; j <= maxz; j++)
                {
                    if (i >= 0 && i < heightmap->width &&
                        j >= 0 && j < heightmap->height &&
                        WithinRadius(x, z, radius, (i / float(heightmap->width)) * heightmap->scale[0], (j / float(heightmap->height)) * heightmap->scale[2]))
                    {
                        SetHeightmapFlags(heightmap, i, j, active ? 0 : 1);
                    }
                }
            }

            InvalidateHeightmap(heightmap, x, z, radius);
            RebuildHeightmap(heightmap);
        }
    }
}

void GatherTrisFromQuad(Islander::Model::Heightmap* heightmap, Islander::Model::HeightmapChunk& chunk, Islander::Model::HeightmapQuad& quad, std::vector<int32_t>& indexData, float* pos, float radius)
{
    for (int i = 0; i < quad.numIndices.size(); i++)
    {
        for (int j = 0; j < quad.numIndices[i]; j += 3) // each triangle in strip
        {
            int i1 = chunk.indexData[(quad.startIndex[i] + j)] * heightmap->vertexStride * sizeof(float);
            int i2 = chunk.indexData[(quad.startIndex[i] + j + 1)] * heightmap->vertexStride * sizeof(float);
            int i3 = chunk.indexData[(quad.startIndex[i] + j + 2)] * heightmap->vertexStride * sizeof(float);

            float* v0 = (float*)&chunk.vertexData[i1];
            float* v1 = (float*)&chunk.vertexData[i2];
            float* v2 = (float*)&chunk.vertexData[i3];

            // Only add these which are within the radius

            if (WithinRadius(pos[0], pos[2], radius, v0[0], v0[2]) ||
                WithinRadius(pos[0], pos[2], radius, v1[0], v1[2]) ||
                WithinRadius(pos[0], pos[2], radius, v2[0], v2[2]))
            {
                indexData.push_back(chunk.indexData[(quad.startIndex[i] + j)]);
                indexData.push_back(chunk.indexData[(quad.startIndex[i] + j + 1)]);
                indexData.push_back(chunk.indexData[(quad.startIndex[i] + j + 2)]);
            }
        }
    }
}

int Islander::Model::GatherHeightmapTris(Heightmap* heightmap, float x, float z, float radius, HeightmapGeometryData* data, int maxDataCount)
{
    data->numIndicies = 0;
    data->indexData = nullptr;
    data->chunkId = -1;

    if (heightmap && heightmap->data)
    {
        if (radius < 0)
        {
            return -1;
        }

        // Jump to chunk containing the pos
        float pos[3] = {
            x, 0.0f, z
        };
        int cellX; int cellZ;
        int chunkId = GetHeightmapChunk(heightmap, pos, cellX, cellZ);
        if (chunkId > -1)
        {
            HeightmapChunk& chunk = heightmap->chunks[chunkId];

            int resultCount = 0;

            const int chunkWidth = heightmap->width / heightmap->chunkWidth;
            const int chunkHeight = heightmap->height / heightmap->chunkHeight;

            for (int i = -1; i <= 1; i++)
            {
                for (int j = -1; j <= 1; j++)
                {
                    int chunkX = cellX + i;
                    int chunkY = cellZ + j;

                    if (chunkX >= 0 && chunkX < chunkWidth &&
                        chunkY >= 0 && chunkY < chunkHeight)
                    {
                        int id = Islander::Model::Private::GetHeightmapChunkID(chunkX, chunkY, chunkHeight);
                        assert(id > -1);

                        HeightmapChunk& chunk_ = heightmap->chunks[id];

                        std::vector<int> quads;
                        FindQuadTreeNodes(&chunk_, pos, radius, quads);

                        if (quads.size() > 0)
                        {
                            std::vector<int32_t> indicies;
                            for (int quadId : quads)
                            {
                                auto& quad = chunk_.quadTree[quadId];
                                GatherTrisFromQuad(heightmap, chunk_, quad, indicies, pos, radius);
                            }

                            if (indicies.size() > 0)
                            {
                                data[resultCount].indexData = new int32_t[indicies.size()];
                                data[resultCount].numIndicies = indicies.size();
                                std::memcpy(data[resultCount].indexData, indicies.data(), indicies.size() * sizeof(int32_t));
                                data[resultCount].chunkId = id;

                                resultCount++;
                            }
                        }
                        if (resultCount == maxDataCount) { break; }
                    }
                }

                if (resultCount == maxDataCount) { break; }
            }

            return resultCount;
        }
    }

    return 0;
}
