#include "Polygon.h"
#include "Logger.h"
#include "ModelFormat.h"
#include "Vector.h"
#include "glm\gtx\quaternion.hpp"

#include <algorithm>
#include <cstring>
#include <stack>
#include <cfloat>

#define _USE_MATH_DEFINES
#include <math.h>

using namespace Islander::Model;
using namespace Islander::Numerics;

PolygonMeshLibrary* Islander::Model::CreatePolyMeshLibrary()
{
    PolygonMeshLibrary *lib = new PolygonMeshLibrary();
    lib->nextId = 0;
    return lib;
}

void Islander::Model::CompleteLoadingEngineMesh(PolygonMeshLibrary* lib, PolygonModel* mesh)
{
    // If the mesh has been loaded async it won't have an id associated with it yet, do this now.
    if (mesh->id == -1)
    {
        mesh->id = lib->nextId;
        lib->data.insert(std::pair<int, PolygonModel*>(mesh->id, mesh));
        lib->nextId++;
    }
}

void Islander::Model::AddPolygonModel(PolygonMeshLibrary* lib, PolygonModel* model, PolygonMeshMetadata createFlags)
{
    bool async = (createFlags & POLYGON_MESH_METADATA_ASYNC) == POLYGON_MESH_METADATA_ASYNC;

    model->min[0] = FLT_MAX;
    model->min[1] = FLT_MAX;
    model->min[2] = FLT_MAX;

    model->max[0] = FLT_MIN;
    model->max[1] = FLT_MIN;
    model->max[2] = FLT_MIN;

    for (int i = 0; i < model->numMeshes; i++)
    {
        auto mesh = &model->meshes[i];

        model->min[0] = std::min(model->min[0], mesh->min[0]);
        model->min[1] = std::min(model->min[1], mesh->min[1]);
        model->min[2] = std::min(model->min[2], mesh->min[2]);

        model->max[0] = std::max(model->max[0], mesh->max[0]);
        model->max[1] = std::max(model->max[1], mesh->max[1]);
        model->max[2] = std::max(model->max[2], mesh->max[2]);
    }

    model->id = async ? -1 : lib->nextId;

    if (!async)
    {
        lib->data.insert(std::pair<int, PolygonModel*>(model->id, model));
        lib->nextId++;
    }
}

void Islander::Model::AddPolyMeshData(PolygonMeshLibrary* lib, PolygonMesh* mesh, float* vertexData, int32_t* indexData, int numVerts, int numIndicies, int stride, PolygonMeshMetadata createFlags)
{
    mesh->metadata = createFlags;
    mesh->indexBuffer = -1;
    mesh->vertexBuffer = -1;

    if ((createFlags & POLYGON_MESH_METADATA_COPY_DATA) == POLYGON_MESH_METADATA_COPY_DATA)
    {
        mesh->indexData = new int32_t[numIndicies];
        memcpy(mesh->indexData, indexData, sizeof(int32_t) * numIndicies);

        mesh->vertexData = new float[stride * numVerts / sizeof(float)];
        memcpy(mesh->vertexData, vertexData, numVerts * stride);
    }
    else
    {
        mesh->indexData = indexData;
        mesh->vertexData = vertexData;
    }

    mesh->numVerts = numVerts;
    mesh->numIndices = numIndicies;
    mesh->stride = stride;
    mesh->posData = nullptr;

    mesh->min[0] = FLT_MAX;
    mesh->min[1] = FLT_MAX;
    mesh->min[2] = FLT_MAX;
    
    mesh->max[0] = FLT_MIN;
    mesh->max[1] = FLT_MIN;
    mesh->max[2] = FLT_MIN;

    int incr = stride / sizeof(float);

    if ((createFlags & POLYGON_MESH_METADATA_BOX_DATA) == POLYGON_MESH_METADATA_BOX_DATA)
    {
        for (int i = 0; i < numVerts * incr; i += incr)
        {
            float x = vertexData[i];
            float y = vertexData[i + 1];
            float z = vertexData[i + 2];

            Matrix4x4::TransformVector(mesh->transform2, &x, &y, &z);

            mesh->min[0] = std::min(x, mesh->min[0]);
            mesh->min[1] = std::min(y, mesh->min[1]);
            mesh->min[2] = std::min(z, mesh->min[2]);

            mesh->max[0] = std::max(x, mesh->max[0]);
            mesh->max[1] = std::max(y, mesh->max[1]);
            mesh->max[2] = std::max(z, mesh->max[2]);
        }
    }

    if ((createFlags & POLYGON_MESH_METADATA_POS_DATA) == POLYGON_MESH_METADATA_POS_DATA)
    {
        mesh->posData = new float[numVerts * 3];

        for (int i = 0; i < numVerts; i++)
        {
            mesh->posData[i * 3] = vertexData[i * incr]; // x
            mesh->posData[i * 3 + 1] = vertexData[i * incr + 1]; // y
            mesh->posData[i * 3 + 2] = vertexData[i * incr + 2]; // z
        }
    }
}

PolygonModel* LoadEngineMeshInternal(PolygonMeshLibrary* lib, PolygonModel* model, void readData(void* cxt, char* data, int size), void* cxt, bool async)
{
    bool error = false;

    Islander::ModelFormatHeader header;
    readData(cxt, (char*)&header, sizeof(Islander::ModelFormatHeader));

    if (header.version >= 0)
    {
        if (model == nullptr)
        {
            model = new PolygonModel();
        }

        Islander::ModelFormatElement element;
        int textureChannels = 0;
        int animationsCount = header.animationCount;

        PolygonMeshMetadata createFlags = (PolygonMeshMetadata)(POLYGON_MESH_METADATA_BOX_DATA | POLYGON_MESH_METADATA_POS_DATA | (async ? POLYGON_MESH_METADATA_ASYNC : POLYGON_MESH_METADATA_NONE));

        for (int modelIndex = 0; modelIndex < header.meshCount; modelIndex++)
        {
            readData(cxt, (char*)&element, sizeof(int32_t));

            if (element == Islander::ModelFormatElement::Mesh)
            {
                std::vector<Islander::ModelFormatSubMeshHeader> submeshHeaders;
                std::vector<Islander::ModelFormatSubMeshHeaderV2> submeshHeadersV2;

                Islander::ModelFormatMeshHeader meshHeader;
                readData(cxt, (char*)&meshHeader, sizeof(Islander::ModelFormatMeshHeader));

                Islander::ModelFormatMeshHeaderEx meshHeaderEx;
                if (header.version >= 1)
                {
                    readData(cxt, (char*)&meshHeaderEx, sizeof(Islander::ModelFormatMeshHeaderEx));
                }
                else
                {
                    memset((char*)&meshHeaderEx, 0, sizeof(Islander::ModelFormatMeshHeaderEx));
                }

                Matrix4x4 transform2(Matrix4x4Type::MATRIX_4X4_IDENTITY);

                if (meshHeader.version == 0)
                {
                    for (int i = 0; i < meshHeader.submeshCount; i++)
                    {
                        Islander::ModelFormatSubMeshHeader submeshHeader;
                        readData(cxt, (char*)&submeshHeader, sizeof(Islander::ModelFormatSubMeshHeader));

                        if (submeshHeader.version == 1)
                        {
                            submeshHeaders.push_back(submeshHeader);
                        }
                        else
                        {
                            // error
                            error = true;
                        }
                    }
                }
                else if (meshHeader.version == 1)
                {
                    for (int i = 0; i < meshHeader.submeshCount; i++)
                    {
                        Islander::ModelFormatSubMeshHeaderV2 submeshHeader;
                        readData(cxt, (char*)&submeshHeader, sizeof(Islander::ModelFormatSubMeshHeaderV2));

                        if (submeshHeader.version == 1)
                        {
                            submeshHeadersV2.push_back(submeshHeader);
                        }
                        else
                        {
                            // error
                            error = true;
                        }
                    }
                }
                else if (meshHeader.version == 2)
                {
                    readData(cxt, (char*)transform2.GetData(), sizeof(float)*16);

                    for (int i = 0; i < meshHeader.submeshCount; i++)
                    {
                        Islander::ModelFormatSubMeshHeaderV2 submeshHeader;
                        readData(cxt, (char*)&submeshHeader, sizeof(Islander::ModelFormatSubMeshHeaderV2));

                        if (submeshHeader.version == 1)
                        {
                            submeshHeadersV2.push_back(submeshHeader);
                        }
                        else
                        {
                            // error
                            error = true;
                        }
                    }
                }
                else
                {
                    // error
                    error = true;
                }

                if (!error)
                {
                    Islander::ModelFormatMeshAttribute attribute;
                    readData(cxt, (char*)&attribute, sizeof(int32_t));

                    float* vertices = nullptr;
                    int32_t* indices = nullptr;

                    int vertexSize = 9;
                    if ((meshHeader.vertexFlags & (int)Islander::ModelFormatVertex::BoneWeight) == (int)Islander::ModelFormatVertex::BoneWeight) { vertexSize += 8; }
                    if ((meshHeader.vertexFlags & (int)Islander::ModelFormatVertex::UV) == (int)Islander::ModelFormatVertex::UV) { vertexSize += 2; }

                    if (attribute == Islander::ModelFormatMeshAttribute::Vertices)
                    {
                        vertices = new float[meshHeader.vertexCount * vertexSize];
                        readData(cxt, (char*)vertices, meshHeader.vertexCount * sizeof(float) * vertexSize);
                    }

                    readData(cxt, (char*)&attribute, sizeof(int32_t));

                    if (attribute == Islander::ModelFormatMeshAttribute::Indicies)
                    {
                        indices = new int32_t[meshHeader.indexCount];
                        readData(cxt, (char*)indices, meshHeader.indexCount * sizeof(int32_t));
                    }

                    readData(cxt, (char*)&attribute, sizeof(int32_t));

                    PolygonSkeleton* skeleton = nullptr;

                    if (attribute == Islander::ModelFormatMeshAttribute::Bones)
                    {
                        if (meshHeader.boneCount > 0)
                        {
                            skeleton = new PolygonSkeleton();
                            skeleton->jointCount = meshHeader.boneCount;
                        }

                        for (int i = 0; i < meshHeader.boneCount; i++)
                        {
                            int32_t boneindex;
                            readData(cxt, (char*)&boneindex, sizeof(int32_t));

                            int32_t parent;
                            readData(cxt, (char*)&parent, sizeof(int32_t));

                            const int transformSize = 16;
                            float transform[transformSize];
                            readData(cxt, (char*)transform, transformSize * sizeof(float));

                            auto joint = &skeleton->joints[boneindex];

                            joint->id = boneindex;
                            joint->bound = true;
                            if (parent != -1)
                            {
                                joint->parent = &skeleton->joints[parent];
                                joint->parent->children[joint->parent->childCount] = joint;
                                joint->parent->childCount++;
                            }
                            else
                            {
                                skeleton->root = joint;
                            }
                            joint->transform = Matrix4x4(transform);
                        }
                    }

                    // To support where the old place animation count was set.
                    if (animationsCount == 0)
                    {
                        animationsCount = meshHeader.animationCount;
                    }

                    if (!error)
                    {
                        PolygonMesh* mesh = &model->meshes[model->numMeshes];
                        model->numMeshes++;

                        mesh->materialId = meshHeaderEx.materialIndex;
                        mesh->skeleton = skeleton;
                        mesh->subMeshCount = (int)submeshHeaders.size() + (int)submeshHeadersV2.size();
                        mesh->subMeshes = new PolygonSubMesh[mesh->subMeshCount];
                        mesh->transform = Matrix4x4(meshHeader.transform);
                        mesh->transform2 = transform2;

                        AddPolyMeshData(lib,
                            mesh,
                            vertices,
                            indices,
                            meshHeader.vertexCount,
                            meshHeader.indexCount,
                            vertexSize * sizeof(float),
                            createFlags);

                        for (int j = 0; j < submeshHeaders.size(); j++)
                        {
                            auto& it = submeshHeaders[j];

                            PolygonSubMesh* polySubMesh = &mesh->subMeshes[j];
                            polySubMesh->indexCount = it.indexCount;
                            polySubMesh->jointCount = it.boneCount;
                            polySubMesh->startIndex = it.indexOffset;

                            for (int i = 0; i < it.boneCount; i++)
                            {
                                polySubMesh->joints[i] = &skeleton->joints[it.bones[i]];
                            }
                        }

                        for (int j = 0; j < submeshHeadersV2.size(); j++)
                        {
                            auto& it = submeshHeadersV2[j];

                            PolygonSubMesh* polySubMesh = &mesh->subMeshes[j];
                            polySubMesh->indexCount = it.indexCount;
                            polySubMesh->jointCount = it.boneCount;
                            polySubMesh->startIndex = it.indexOffset;

                            for (int i = 0; i < it.boneCount; i++)
                            {
                                polySubMesh->joints[i] = &skeleton->joints[it.bones[i]];
                            }
                        }
                    }

                    if ((meshHeader.vertexFlags & (int)Islander::ModelFormatVertex::UV) == (int)Islander::ModelFormatVertex::UV)
                    {
                        textureChannels = 1; // supports only a single texture channel
                    }
                }
            }
        }

        if (!error)
        {
            readData(cxt, (char*)&element, sizeof(int32_t));

            PolygonAnimations* animations = new PolygonAnimations();
            animations->animationCount = 0;

            if (element == Islander::ModelFormatElement::Animation)
            {
                if (animationsCount > ANIMATION_MAX_COUNT)
                {
                    error = true;
                }
                
                for (int i = 0; i < animationsCount; i++)
                {
                    Islander::ModelFormatAnimationHeader animationHeader;
                    readData(cxt, (char*)&animationHeader, sizeof(Islander::ModelFormatAnimationHeader));

                    if (animationHeader.version == 0)
                    {
                        auto anim = &animations->animations[animations->animationCount++];
                        readData(cxt, anim->name.data, animationHeader.nameLength);
                        anim->name.len = animationHeader.nameLength;
                        anim->boneCount = animationHeader.boneCount;

                        for (int j = 0; j < animationHeader.boneCount; j++)
                        {
                            Islander::ModelFormatAnimationFrameBoneHeader animationBoneHeader;
                            readData(cxt, (char*)&animationBoneHeader, sizeof(Islander::ModelFormatAnimationFrameBoneHeader));

                            auto bone = &anim->bones[j];
                            auto skeleton = model->meshes[0].skeleton;
                            bone->bone = animationBoneHeader.bone == -1 ? nullptr : &skeleton->joints[animationBoneHeader.bone];

                            for (int k = 0; k < animationBoneHeader.frameCount; k++)
                            {
                                Islander::ModelFormatAnimationFrameHeader animationFrameBoneHeader;
                                readData(cxt, (char*)&animationFrameBoneHeader, sizeof(Islander::ModelFormatAnimationFrameHeader));

                                if (animationFrameBoneHeader.version == 0)
                                {
                                    PolygonAnimationFrame frame;
                                    frame.time = animationFrameBoneHeader.time;
                                    frame.x = animationFrameBoneHeader.x;
                                    frame.y = animationFrameBoneHeader.y;
                                    frame.z = animationFrameBoneHeader.z;
                                    frame.sx = animationFrameBoneHeader.sx;
                                    frame.sy = animationFrameBoneHeader.sy;
                                    frame.sz = animationFrameBoneHeader.sz;

                                    glm::qua<float> q;
                                    q.w = frame.q[0] = animationFrameBoneHeader.rw;
                                    q.x = frame.q[1] = animationFrameBoneHeader.rx;
                                    q.y = frame.q[2] = animationFrameBoneHeader.ry;
                                    q.z = frame.q[3] = animationFrameBoneHeader.rz;

                                    auto rot = glm::eulerAngles(q);

                                    frame.rx = rot.x;
                                    frame.ry = rot.y;
                                    frame.rz = rot.z;

                                    bone->frames.push_back(frame);
                                }
                                else
                                {
                                    error = true;
                                    break;
                                }
                            }

                            if (error)
                            {
                                break;
                            }
                        }
                    }
                    else
                    {
                        error = true;
                    }

                    if (error)
                    {
                        break;
                    }
                }
            }

            model->animations = animations;
            model->numTextureChannels = textureChannels;

            if (header.version >= 1)
            {
                readData(cxt, (char*)&element, sizeof(int32_t));

                if (element == Islander::ModelFormatElement::Material)
                {
                    Islander::ModelFormatMaterialHeader materialHeader;
                    readData(cxt, (char*)&materialHeader, sizeof(Islander::ModelFormatMaterialHeader));
                    if (materialHeader.version >= 0)
                    {
                        if (materialHeader.materialCount > model->materials.size())
                        {
                            model->numMaterials = 0;
                            error = true;
                        }

                        if (!error)
                        {
                            model->numMaterials = materialHeader.materialCount;

                            for (int i = 0; i < materialHeader.materialCount; i++)
                            {
                                Islander::ModelFormatMaterial material;
                                readData(cxt, (char*)&material, sizeof(Islander::ModelFormatMaterial));

                                model->materials[i].numDiffuseTextures = material.diffuseCount;
                                model->materials[i].numNormalTextures = material.normalCount;
                                model->materials[i].diffuseIndex.fill(-1);
                                model->materials[i].normalIndex.fill(-1);

                                for (int j = 0; j < material.diffuseCount; j++)
                                {
                                    Islander::ModelFormatTexture texture;
                                    readData(cxt, (char*)&texture, sizeof(Islander::ModelFormatTexture));

                                    memcpy(model->materials[i].diffuseTexture[j].data, texture.name, sizeof(texture.name));
                                    model->materials[i].diffuseTexture[j].len = (int)strlen(model->materials[i].diffuseTexture[j].data);
                                }

                                for (int j = 0; j < material.normalCount; j++)
                                {
                                    Islander::ModelFormatTexture texture;
                                    readData(cxt, (char*)&texture, sizeof(Islander::ModelFormatTexture));

                                    memcpy(model->materials[i].normalTexture[j].data, texture.name, sizeof(texture.name));
                                    model->materials[i].normalTexture[j].len = (int)strlen(model->materials[i].normalTexture[j].data);
                                }
                            }
                        }
                    }
                    else
                    {
                        error = true;
                    }
                }
                else
                {
                    error = true;
                }
            }
            else
            {
                model->materials[0].numDiffuseTextures = 0;
                model->materials[0].numNormalTextures = 0;
                model->numMaterials = 1;

                if (!error)
                {
                    readData(cxt, (char*)&element, sizeof(int32_t));

                    if (element == Islander::ModelFormatElement::Texture)
                    {
                        Islander::ModelFormatTextureHeader textureHeader;
                        readData(cxt, (char*)&textureHeader, sizeof(Islander::ModelFormatTextureHeader));
                        if (textureHeader.version == 0)
                        {
                            model->materials[0].numDiffuseTextures = textureHeader.diffuseCount;
                            model->materials[0].numNormalTextures = textureHeader.normalCount;
                            model->materials[0].diffuseIndex.fill(-1);
                            model->materials[0].normalIndex.fill(-1);

                            for (int i = 0; i < textureHeader.diffuseCount; i++)
                            {
                                Islander::ModelFormatTexture texture;
                                readData(cxt, (char*)&texture, sizeof(Islander::ModelFormatTexture));

                                memcpy(model->materials[0].diffuseTexture[i].data, texture.name, sizeof(texture.name));
                                model->materials[0].diffuseTexture[i].len = (int)strlen(model->materials[0].diffuseTexture[i].data);
                            }

                            for (int i = 0; i < textureHeader.normalCount; i++)
                            {
                                Islander::ModelFormatTexture texture;
                                readData(cxt, (char*)&texture, sizeof(Islander::ModelFormatTexture));

                                memcpy(model->materials[0].normalTexture[i].data, texture.name, sizeof(texture.name));
                                model->materials[0].normalTexture[i].len = (int)strlen(model->materials[0].normalTexture[i].data);
                            }
                        }
                        else
                        {
                            error = true;
                        }
                    }
                }
            }

            AddPolygonModel(lib, model, createFlags);
        }
    }
    else
    {
        // error
        error = true;
    }

    return model;
}

void ReadFromStream(void* cxt, char* data, int size)
{
    auto stream = (std::ifstream*)cxt;
    if (!stream->eof())
    {
        stream->read(data, size);
    }
}

struct MeshData
{
    unsigned char* data;
    int offset;
    int size;
};

void ReadFromArray(void* cxt, char* data, int size)
{
    auto meshData = (MeshData*)cxt;
    if (meshData->offset + size <= meshData->size)
    {
        memcpy(data, meshData->data + meshData->offset, size);
        meshData->offset += size;
    }
}

PolygonModel* Islander::Model::LoadEngineMesh(PolygonMeshLibrary* lib, std::ifstream& stream, bool async)
{
    return LoadEngineMeshInternal(lib, nullptr, ReadFromStream, (void*)&stream, async);
}

PolygonModel* Islander::Model::LoadEngineMesh(PolygonMeshLibrary* lib, unsigned char* data, int count, bool async)
{
    MeshData meshData;
    meshData.data = data;
    meshData.offset = 0;
    meshData.size = count;

    return LoadEngineMeshInternal(lib, nullptr, ReadFromArray, &meshData, async);
}

void UnloadMeshes(PolygonMeshLibrary* lib, PolygonModel* model, bool unloadBuffers)
{
    for (int i = 0; i < model->numMeshes; i++)
    {
        auto& mesh = model->meshes[i];

        if (unloadBuffers)
        {
            lib->vertexBufferFree.push_back(mesh.vertexBuffer);
            lib->indexBufferFree.push_back(mesh.indexBuffer);
        }

        delete mesh.skeleton;
        delete[] mesh.subMeshes;

        if ((mesh.metadata & POLYGON_MESH_METADATA_COPY_DATA) == POLYGON_MESH_METADATA_COPY_DATA)
        {
            delete[] mesh.vertexData;
            delete[] mesh.indexData;
        }

        if ((mesh.metadata & POLYGON_MESH_METADATA_POS_DATA) == POLYGON_MESH_METADATA_POS_DATA)
        {
            delete[] mesh.posData;
        }
    }

    //model->numDiffuseTextures = 0;
    //model->numNormalTextures = 0;
    model->numMaterials = 0;
    model->numMeshes = 0;
    model->numTextureChannels = 0;
}

void Islander::Model::UnloadEngineMesh(PolygonMeshLibrary* lib, PolygonModel* model, bool unloadBuffers)
{
    if (model)
    {
        lib->data.erase(model->id);

        UnloadMeshes(lib, model, unloadBuffers);

        delete model;
    }
}

void Islander::Model::ReloadEngineMesh(PolygonMeshLibrary* lib, PolygonModel* model, std::ifstream& stream)
{
    lib->data.erase(model->id);
    UnloadMeshes(lib, model, true);
    LoadEngineMeshInternal(lib, model, ReadFromStream, (void*)&stream, false);
    model->reloaded = true;
}

void Islander::Model::ReloadEngineMesh(PolygonMeshLibrary* lib, PolygonModel* model, unsigned char* data, int count)
{
    MeshData meshData;
    meshData.data = data;
    meshData.offset = 0;
    meshData.size = count;

    lib->data.erase(model->id);
    UnloadMeshes(lib, model, true);
    LoadEngineMeshInternal(lib, model, ReadFromArray, &meshData, false);
    model->reloaded = true;
}

int Islander::Model::FindAnimationId(PolygonModel* mesh, const char* name)
{
    for (int i = 0; i < mesh->animations->animationCount; i++)
    {
        if (strcmp(name, mesh->animations->animations[i].name.data) == 0)
        {
            return i;
        }
    }

    return -1;
}

float Islander::Model::GetAnimationLength(PolygonModel* mesh, int id)
{
    if (id >= 0 && id < mesh->animations->animationCount)
    {
        // TODO: Pre-calculate the animation length in the importer program
        auto bone = &mesh->animations->animations[id].bones[0];
        return bone->frames.rbegin()->time;
    }

    return 0;
}

int Islander::Model::GetAnimationCount(PolygonModel* mesh)
{
    return mesh->animations->animationCount;
}

int Islander::Model::GetAnimationName(PolygonModel* mesh, int id, char* name)
{
    if (id >= 0 && id < mesh->animations->animationCount)
    {
        auto animation = &mesh->animations->animations[id];

        if (name == nullptr)
        {
            return animation->name.len;
        }

        memcpy(name, animation->name.data, animation->name.len);
        return animation->name.len;
    }

    return 0;
}
