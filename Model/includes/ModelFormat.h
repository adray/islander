#pragma once
#include <stdint.h>

namespace Islander
{
#define ISLANDER_MODEL_FORMAT_SUBMESH_BONE_COUNT 20
#define ISLANDER_MODEL_FORMAT_SUBMESH_BONE_COUNT_V2 40
#define ISLANDER_MODEL_FORMAT_IMAGE_NAME_LEN 64

    enum class ModelFormatElement
    {
        None = 0,
        Mesh = 1,
        Animation = 2,
        Texture = 3,
        Material = 4
    };

    enum class ModelFormatVertex
    {
        None = 0x0,
        Pos = 0x1,
        Normal = 0x2,
        Colour = 0x4,
        BoneWeight = 0x8,
        UV = 0x16
    };

    enum class ModelFormatMeshAttribute
    {
        None = 0,
        Vertices = 1,
        Indicies = 2,
        Bones = 3
    };

    enum class ModelFormatTextureType
    {
        None = 0,
        Diffuse = 1,
        Normal = 2
    };

    struct ModelFormatHeader
    {
        int32_t version;
        int8_t meshCount;
        int8_t animationCount;
    };

    struct ModelFormatSubMeshHeader
    {
        int32_t version;
        int32_t indexCount;
        int32_t indexOffset;
        int32_t boneCount;
        int32_t bones[ISLANDER_MODEL_FORMAT_SUBMESH_BONE_COUNT];
    };

    struct ModelFormatSubMeshHeaderV2
    {
        int32_t version;
        int32_t indexCount;
        int32_t indexOffset;
        int32_t boneCount;
        int32_t bones[ISLANDER_MODEL_FORMAT_SUBMESH_BONE_COUNT_V2];
    };

    struct ModelFormatMeshHeader
    {
        int32_t version;
        int32_t vertexFlags;
        int32_t boneCount;
        int32_t submeshCount;
        int32_t vertexCount;
        int32_t indexCount;
        int32_t animationCount;
        float transform[16];
    };

    struct ModelFormatMeshHeaderEx
    {
        int32_t materialIndex;
        int32_t reserved[15]; // reserved for future use
    };

    struct ModelFormatAnimationHeader
    {
        int32_t version;
        int32_t boneCount;
        int32_t nameLength;
    };

    struct ModelFormatAnimationFrameBoneHeader
    {
        int32_t bone;
        int32_t frameCount;
    };

    struct ModelFormatAnimationFrameHeader
    {
        int32_t version;
        float time;
        float x, y, z;
        float rx, ry, rz, rw;
        float sx, sy, sz;
    };

    struct ModelFormatTextureHeader
    {
        int32_t version;
        int32_t diffuseCount;
        int32_t normalCount;
    };

    struct ModelFormatTexture
    {
        int32_t version;
        char name[ISLANDER_MODEL_FORMAT_IMAGE_NAME_LEN];
    };

    struct ModelFormatMaterialHeader
    {
        int32_t version;
        int32_t materialCount;
        int32_t reserved[4]; // reserved for future use
    };

    struct ModelFormatMaterial
    {
        int32_t version;
        int32_t diffuseCount;
        int32_t normalCount;
        int32_t reserved[4]; // reserved for future use
    };
}
