#pragma once
#include <cstdint>
#include <vector>

namespace Islander
{
    namespace Numerics
    {
        class Matrix4x4;
    }

    namespace Model
    {
        struct PolygonAnimations;
        struct PolygonAnimation;
        struct PolygonJoint;
        struct PolygonAnimationFrameBone;
        struct PolygonModel;
    }

    namespace Model
    {
        enum class AnimationState
        {
            Normal = 0,
            BlendToAnimation = 1,
            Blended = 2
        };

        struct Animation
        {
            bool loop;                  // if the current animation is looping
            int animationId;            // the id of the animation to play
            float elapsedTime;          // how much time has elapsed playing the animation
            float speedScale;           // the speed scale of the current animation
            int animationId2;           // the id of the 2nd animation to play if being blended
            bool loop2;                 // if the 2nd animation is looping
            std::uint64_t mask;         // mask for animation
            float endTime;              // optional time to end animation on
            float lastElapsedTime;      // the last elapsed time of the previous update
            bool isRootMotion;          // if the animation is a root motion animation
            int rootMotionFlags;        // root motion flags
        };

        struct AnimationBlend
        {
            int prevAnimationId;        // the previous animation id
            float blendTime;            // the time taken to blend from the previous animation to the new animation
            float elapsedBlendTime;     // the elapsed time the blend has taken
            float frozenTime;           // the time the previous animation was at when blending began
            int prevAnimationId2;       // the id of the 2nd previous animation to play if being blended
            bool isRootMotion;          // the previous animation was root motion animation
            int rootMotionFlags;        // root motion flags
            std::uint64_t mask;         // mask for the previous animation
        };

        struct AnimationController
        {
            bool alive;                 // if the controller alive (false when destroyed)
            int id;                     // id of the animation controller
            bool playing;               // if the animation controller is running
            AnimationState state;
            Animation animation;
            AnimationBlend blend;
        };

        struct AnimationSystem
        {
            std::vector<AnimationController> controllers;
            std::vector<int> slots;
        };

        AnimationSystem* CreateAnimationSystem();
        void DestroyAnimationSystem(AnimationSystem* system);
        int CreateAnimationController(AnimationSystem* system);
        void DestroyAnimationController(AnimationSystem* system, int controller);
        void PlayAnimation(AnimationSystem* system, int controller, int animationId, bool loop);
        void PauseAnimation(AnimationSystem* system, int controller);
        void ResetAnimation(AnimationSystem* system, int controller);
        void SetAnimationTimeBounds(AnimationSystem* system, int controller, float startTime, float endTime);
        void SetAnimationSpeed(AnimationSystem* system, int controller, float speedScale);
        void SetAnimationRootMotion(AnimationSystem* system, int controller, bool isRootMotion, int rootMotionFlags);
        void BlendToAnimation(AnimationSystem* system, int controller, int animationId, float blendTime);
        void PlayBlendedAnimation(AnimationSystem* system, int controller, int animationId1, int animationId2, std::uint64_t mask, bool loop1, bool loop2);
        //void SetAnimationMask(AnimationSystem* system, int controller, std::uint64_t bones, bool masked);
        void RunAnimationSystem(AnimationSystem* system, float delta);

        void CalculateBoneMatrixForParenting(Islander::Numerics::Matrix4x4* boneMatrix, Islander::Model::PolygonAnimation* animation, Islander::Model::PolygonJoint* parent, Islander::Numerics::Matrix4x4* animationFrames, Islander::Numerics::Matrix4x4& transform, Islander::Numerics::Matrix4x4& inverseMeshTransform);
        void CalculateBoneTransforms(Islander::Model::PolygonAnimations* animations, Islander::Numerics::Matrix4x4* animationFrameTransforms, float elapsedTime1, float elapsedTime2, AnimationSystem* system, int controllerId);
        void CalculateTransformForPose(Islander::Model::PolygonModel* parent_poly, AnimationSystem* animSystem, int controllerId, int parentBone, Islander::Numerics::Matrix4x4& parent);

        void CalculateRootMotion(Islander::Model::PolygonModel* model, int animation1, int animation2, float elapsedTime1, float elapsedTime2, const float* scale, const float* rot, float* displacement);
        void CalculateRootMotion(Islander::Model::PolygonModel* model, AnimationSystem* animSystem, int controllerId, bool fromLastElapsed, const float* rot, const float* scale, float* displacement);
    }
}
