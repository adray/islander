#pragma once
#include <vector>

namespace Islander
{
    namespace Model
    {
        struct HeightmapDebugQuery
        {
            int quadId;
            int chunkId;
            float velocity[3];
            bool hit;
            void* collision;
        };

        struct HeightmapDebugMoveAlongHeightmap
        {
            float vel[3];
            bool rampHandled;
            void* collision;
        };

        struct HeightmapDebugRayInfo
        {
            int quads;
            int tris;
            int iterations;
            float pos[3];
            float dir[3];
            float exitPos[3];
        };

        struct HeightmapDebugInfo
        {
            std::vector<HeightmapDebugQuery> hits;
            std::vector<HeightmapDebugMoveAlongHeightmap> moves;
            std::vector<HeightmapDebugRayInfo> ray;
        };

        struct HeightmapQuad
        {
            float min[3];
            float max[3];

            int parentNode;
            int childNodes[4];
            int numNodes;

            std::vector<int> startIndex; // list of start indices corresponding to numIndicies
            std::vector<int> numIndices; // list of number of indicies
        };

        struct HeightmapChunk
        {
            bool dirty;
            bool loaded;

            int* indexData;
            char* vertexData;

            int numVertices;
            int numIndices;

            int x; // offset x in pixels
            int y; // offset y in pixels

            int width; // width (num of vertices)
            int height; // height (num of vertices)

            float min[3]; // min AABB
            float max[3]; // max AABB

            std::vector<HeightmapQuad> quadTree;
        };

        struct Heightmap
        {
            int width;  // width in pixels of the image data
            int height; // height in pixels of the image data
            float scale[3]; // heightmap scale used to build the vertex data

            unsigned char* data;                // image data
            unsigned char* textureWeightData;   // weight image data
            unsigned char* textureIndexData;    // index image data

            HeightmapChunk* chunks;
            int numChunks;

            int chunkWidth; // width in pixels of chunk
            int chunkHeight; // height in pxiels of the chunk

            int vertexStride;

            float min[3];
            float max[3];

            unsigned char* chunkUpdate;

            HeightmapDebugInfo debugInfo;
        };

        struct HeightmapGeometryData
        {
            int32_t* indexData;
            int numIndicies;
            int chunkId;
        };

        enum class HeightmapEditingFlags
        {
            None = 0x0,

            Level_OnlyRaise = 0x1,
            Level_OnlyLower = 0x2
        };

        namespace Private
        {
            bool PointCircleTest(float* pt, float* centre, float radius);
            bool CircleBoxTest(float* min, float* max, float* centre, float radius);
            int GetHeightmapChunkID(int x, int z, int height);
        }

        int GetHeightmapChunkID(Heightmap* heightmap, int x, int z);
        int GetHeightmapChunk(Heightmap* heightmap, float* pos, int& cellX, int& cellZ);
        Heightmap* CreateHeightmapFromImageData(int width, int height, unsigned char* data);
        void SetHeightmapTextureData(Heightmap* heightmap, unsigned char* textureIndexData, unsigned char* textureWeightData);
        void SetChunkParameters(Heightmap* heightmap, int chunkWidth, int chunkHeight);
        void BuildHeightmap(Heightmap* heightmap, const float* scale);
        void BuildHeightmap2(Heightmap* heightmap, const float* scale);
        bool SampleHeightmap(Heightmap* heightmap, float x, float z, float* height);
        int FindQuadTreeNodeAtPoint(HeightmapChunk* heightmapChunk, float* pos);
        bool HeightmapIntersect(Heightmap* heightmap, int chunkId, float* min, float* max);
        void DestroyHeightmap(Heightmap* heightmap);

        // Methods for loading/unloading individual chunks

        int LoadChunk(Heightmap* heightmap, int x, int z);
        void UnloadChunk(Heightmap* heightmap, int x, int z);

        // Editing functions

        void SmoothHeightmap(Heightmap* heightmap, float x, float z, float radius, float strength);
        void RaiseHeightmap(Heightmap* heightmap, float x, float z, float radius, int step);
        void LowerHeightmap(Heightmap* heightmap, float x, float z, float radius, int step);
        void LevelHeightmap(Heightmap* heightmap, float x, float z, float radius, HeightmapEditingFlags flags);

        void EraseHeightmap(Heightmap* heightmap, float x, float z, float radius);
        void PaintHeightmap(Heightmap* heightmap, float x, float z, float radius, float strength, int textureIndex);

        // Toggles that the heightmap triangles are active or in active.
        void ToggleActiveHeightmap(Heightmap* heightmap, float x, float z, float radius, bool active);

        // Gathers the triangles which would be effected in radius around point.
        // The HeightmapGeometryData data supplied is an array of maxDataCount.
        // This data will be populated with the index data per chunk into the chunk vertex data.
        // The return value is the number of data elements written.
        int GatherHeightmapTris(Heightmap* heightmap, float x, float z, float radius, HeightmapGeometryData* data, int maxDataCount);
    }
}
