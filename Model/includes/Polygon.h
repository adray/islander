#pragma once
#include "Matrix.h"
#include <unordered_map>
#include <stdint.h>
#include <vector>
#include <array>
#include <fstream>

namespace Islander
{
    namespace Model
    {
        #define FIXED_STRING_LENGTH 512u
        struct fixed_string
        {
            char data[FIXED_STRING_LENGTH];
            int len;

            fixed_string() : len(0) {}
        };

        enum PolygonMeshMetadata
        {
            POLYGON_MESH_METADATA_NONE = 0x0,
            POLYGON_MESH_METADATA_POS_DATA = 0x1,   // Create the pos only data
            POLYGON_MESH_METADATA_BOX_DATA = 0x2,   // Create the bounding box data
            POLYGON_MESH_METADATA_COPY_DATA = 0x4,  // Copy the vertex and index data
            POLYGON_MESH_METADATA_ASYNC = 0x8       // Loading async
        };

        struct PolygonJoint
        {
            PolygonJoint* parent;
            PolygonJoint* children[4];
            int childCount;
            Islander::Numerics::Matrix4x4 transform;
            fixed_string name;
            int id;
            bool bound;

            PolygonJoint() : parent(nullptr), childCount(0), id(0), bound(false) {}
        };

        constexpr int MAX_BONES = 128;
        struct PolygonSkeleton
        {
            PolygonJoint joints[MAX_BONES];
            PolygonJoint* root;
            int jointCount;

            PolygonSkeleton() : root(nullptr), jointCount(0) {}
        };

        struct PolygonAnimationFrame
        {
            float time;
            float x, y, z;
            float rx, ry, rz;
            float sx, sy, sz;
            float q[4];
        };

        struct PolygonAnimationFrameBone
        {
            PolygonJoint* bone;
            std::vector<PolygonAnimationFrame> frames;
        };

        struct PolygonAnimation
        {
            fixed_string name;
            PolygonAnimationFrameBone bones[MAX_BONES];
            int boneCount;
        };

        constexpr int ANIMATION_MAX_COUNT = 32;
        struct PolygonAnimations
        {
            PolygonAnimation animations[ANIMATION_MAX_COUNT];
            int animationCount;
        };

        constexpr int SUBMESH_MAX_COUNT = 40;
        struct PolygonSubMesh
        {
            int startIndex;
            int indexCount;
            PolygonJoint* joints[SUBMESH_MAX_COUNT];
            int jointCount;

            PolygonSubMesh() : startIndex(0), indexCount(0), jointCount(0) {}
        };

        struct PolygonMesh
        {
            PolygonMeshMetadata metadata;
            int numVerts;
            int numIndices;
            int stride;
            float* vertexData;
            int32_t* indexData;
            float* posData;
            float min[3];
            float max[3];
            int indexBuffer;
            int vertexBuffer;
            PolygonSkeleton* skeleton;
            PolygonSubMesh* subMeshes;
            int subMeshCount;
            Islander::Numerics::Matrix4x4 transform;
            Islander::Numerics::Matrix4x4 transform2;
            int materialId;

            PolygonMesh() : skeleton(nullptr), subMeshes(nullptr), subMeshCount(0) { }
        };

        struct PolygonMaterial
        {
            std::array<fixed_string, 4> diffuseTexture;
            std::array<fixed_string, 4> normalTexture;
            std::array<int, 4> diffuseIndex;
            std::array<int, 4> normalIndex;
            int numDiffuseTextures;
            int numNormalTextures;
        };

        constexpr int MESH_MAX_COUNT = 16;

        struct PolygonModel
        {
            int id;
            int numMeshes;
            std::array<PolygonMesh, MESH_MAX_COUNT> meshes;
            float min[3];
            float max[3];
            PolygonAnimations* animations;
            std::array<PolygonMaterial, 8> materials;
            int numMaterials;
            int numTextureChannels;
            bool reloaded;

            PolygonModel() : numTextureChannels(0), numMeshes(0), reloaded(false) { }
        };

        struct PolygonMeshLibrary
        {
            int nextId;
            std::unordered_map<int, PolygonModel*> data;
            std::vector<int> vertexBufferFree;
            std::vector<int> indexBufferFree;
        };

        PolygonMeshLibrary* CreatePolyMeshLibrary();
        void AddPolyMeshData(PolygonMeshLibrary* lib, PolygonMesh* mesh, float* vertexData, int32_t* indexData, int numVerts, int numIndicies, int stride, PolygonMeshMetadata createFlags);
        //bool GetPolyMeshData(PolygonMeshLibrary* lib, int id, PolygonMesh** data);
        PolygonModel* LoadEngineMesh(PolygonMeshLibrary* lib, std::ifstream& stream, bool async);
        PolygonModel* LoadEngineMesh(PolygonMeshLibrary* lib, unsigned char* data, int count, bool async);
        void ReloadEngineMesh(PolygonMeshLibrary* lib, PolygonModel* model, std::ifstream& stream);
        void ReloadEngineMesh(PolygonMeshLibrary* lib, PolygonModel* model, unsigned char* data, int count);
        void UnloadEngineMesh(PolygonMeshLibrary* lib, PolygonModel* model, bool unloadBuffers);
        void CompleteLoadingEngineMesh(PolygonMeshLibrary* lib, PolygonModel* mesh);
        int FindAnimationId(PolygonModel* mesh, const char* name);
        float GetAnimationLength(PolygonModel* mesh, int id);
        int GetAnimationCount(PolygonModel* mesh);
        void AddPolygonModel(PolygonMeshLibrary* lib, PolygonModel* model, PolygonMeshMetadata createFlags);

        // Returns the length of name, call with name=null to get the size of name then again with name allocated to length
        int GetAnimationName(PolygonModel* mesh, int id, char* name);
    }
}
