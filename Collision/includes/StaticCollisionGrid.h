#pragma once
#include "CollisionTypes.h"
#include "CollisionPrimitives.h"
#include "CollisionEvent.h"
#include "EPA.h"
#include "Heightmap.h"
#include <vector>
#include <array>

constexpr int COLLISION_SYSTEM_GRID_COUNT = 64;

namespace Islander
{
    namespace Collision
    {
        struct CollisionStaticMesh;

        struct CollisionCell
        {
            std::vector<CollisionStaticMesh*> entries;
            IslanderCollisionBox box;
            int x;
            int z;
        };

        struct CollisionGrid
        {
            // Grid of static objects
            std::array<CollisionCell, COLLISION_SYSTEM_GRID_COUNT* COLLISION_SYSTEM_GRID_COUNT> cells;
            IslanderCollisionBox box;
            bool dirty;
        };

        void BuildCollisionGrid(Islander::Collision::CollisionGrid& grid, int static_mesh_count, Islander::Collision::CollisionStaticMesh* static_mesh);

        int QueryGridCells(Islander::Collision::CollisionGrid& grid, IslanderCollisionBox& startBox, IslanderCollisionBox& endBox, Islander::Collision::CollisionDynamicMesh* mesh, int max_hits, Islander::Collision::CollisionStaticMesh** hits);

        float SeparateObjects(const IslanderCollisionBox& box, const IslanderCollisionTriangle& tri);
        float SeparateObjects(const IslanderCollisionBox& box, const IslanderCollisionOBB& obb);

        template<typename T, int flags = 0>
        void QueryGridCells(Islander::Collision::CollisionGrid& grid, T& obj, Islander::Collision::CollisionDynamicMesh* mesh, Islander::Collision::CollisionEventList& evlist)
        {
            IslanderCollisionBox startBox;
            GetBoundingBox(obj, startBox);

            IslanderCollisionBox endBox;
            endBox.maxx = startBox.maxx;
            endBox.maxy = startBox.maxy;
            endBox.maxz = startBox.maxz;
            endBox.minx = startBox.minx;
            endBox.miny = startBox.miny;
            endBox.minz = startBox.minz;

            constexpr int max_hits = 16;
            Islander::Collision::CollisionStaticMesh* hits[max_hits];
            int hitcount = QueryGridCells(grid, startBox, endBox, mesh, max_hits, hits);
            
            float n[3];
            
            for (int i = 0; i < hitcount; i++)
            {
                auto& static_mesh = hits[i];
                static_mesh->hit = false;   // reset the flag
                bool hit = false;

                if (static_mesh->triangleCollider && static_mesh->triangles.size() > 0)
                {

                }
                else if (static_mesh->heightmapCollider)
                {

                }
                else
                {
                    if (Islander::Collision::GJK(obj, static_mesh->obb, n))
                    {
                        hit = true;
                    }
                }

                if (hit && mesh->colData.hit_count < ISLANDER_COLLISION_DATA_COUNT)
                {
                    auto& colData = mesh->colData.hits[mesh->colData.hit_count];
                    colData.flags = ISLANDER_COLLISION_HIT_FLAG_STATIC | flags;
                    CopyVec3(n, colData.normal);
                    colData.static_mesh = static_mesh;
                    colData.material = static_mesh->material;

                    mesh->colData.hit_count++;
                }
            }
        }

        template<typename T, int flags = 0>
        void QueryGridCells(Islander::Collision::CollisionGrid& grid, T& obj, float* velocity, Islander::Collision::CollisionDynamicMesh* mesh, Islander::Collision::CollisionEventList& evlist)
        {
            IslanderCollisionBox startBox;
            GetBoundingBox(obj, startBox);

            IslanderCollisionBox endBox;
            endBox.maxx = startBox.maxx + velocity[0];
            endBox.maxy = startBox.maxy + velocity[1];
            endBox.maxz = startBox.maxz + velocity[2];
            endBox.minx = startBox.minx + velocity[0];
            endBox.miny = startBox.miny + velocity[1];
            endBox.minz = startBox.minz + velocity[2];

            constexpr int max_hits = 16;
            Islander::Collision::CollisionStaticMesh* hits[max_hits];
            int hitcount = QueryGridCells(grid, startBox, endBox, mesh, max_hits, hits);

            for (int i = 0; i < hitcount; i++)
            {
                auto& static_mesh = hits[i];
                static_mesh->hit = false;   // reset the flag

                bool hit = false;
                float time = std::numeric_limits<float>::infinity();
                float normal[3];
                float triVerts[9];

                if (static_mesh->triangleCollider && static_mesh->triangles.size() > 0)
                {
                    float n[3];
                    float t_ = GJK_Swept(static_mesh->obb, obj, velocity, n);
                    if (1.0f - t_ > 0)
                    {
                        float triVerts_[9];
                        float t = TriangleMeshSweptCollision(static_mesh->triangles, obj, velocity, n, triVerts_);

                        if (1.0f - t > 0)
                        {
                            hit = true;

                            if (t < time)
                            {
                                time = t;

                                normal[0] = n[0];
                                normal[1] = n[1];
                                normal[2] = n[2];

                                std::memcpy(triVerts, triVerts_, sizeof(triVerts_));
                            }
                        }
                    }
                }
                else if (static_mesh->heightmapCollider)
                {
                    float n[3];
                    float triVerts_[9];
                    float t = HeightmapSweptCollision(static_mesh->heightmap, static_mesh->transform, obj, velocity, n, triVerts_);
                    
                    if (1.0f - t > 0)
                    {
                        hit = true;

                        if (t < time)
                        {
                            time = t;

                            normal[0] = n[0];
                            normal[1] = n[1];
                            normal[2] = n[2];

                            std::memcpy(triVerts, triVerts_, sizeof(triVerts_));
                        }
                    }
                }
                else
                {
                    float n[3];
                    float t = Islander::Collision::GJK_Swept(obj, static_mesh->obb, velocity, n);

                    if (1.0f - t > 0)
                    {
                        hit = true;

                        if (t < time)
                        {
                            time = t;

                            normal[0] = n[0];
                            normal[1] = n[1];
                            normal[2] = n[2];
                        }
                    }
                }

                if (hit)
                {
                    if (mesh->colData.static_mesh_count < ISLANDER_COLLISION_DATA_COUNT)
                    {
                        mesh->colData.static_mesh[mesh->colData.static_mesh_count++] = static_mesh;
                    }

                    if (mesh->colData.hit_count < ISLANDER_COLLISION_DATA_COUNT)
                    {
                        auto& hit = mesh->colData.hits[mesh->colData.hit_count++];
                        hit.static_mesh = static_mesh;
                        hit.flags = ISLANDER_COLLISION_HIT_FLAG_STATIC | flags;
                        CopyVec3(normal, hit.normal);
                        Islander::Numerics::CopyVec3(&triVerts[0], hit.v1);
                        Islander::Numerics::CopyVec3(&triVerts[3], hit.v2);
                        Islander::Numerics::CopyVec3(&triVerts[6], hit.v3);
                        hit.material = static_mesh->material;
                    }

                    if (!static_mesh->trigger)
                    {
                        auto& bound = std::lower_bound(evlist.collisions.begin(), evlist.collisions.end(), time, [](Islander::Collision::CollisionEvent& ev, float t) { return ev.collisionTime < t; });
                        Islander::Collision::CollisionEvent ev;
                        ev.collisionTime = time;
                        ev.mesh = static_mesh;
                        ev.dynamic = false;
                        ev.normal[0] = normal[0];
                        ev.normal[1] = normal[1];
                        ev.normal[2] = normal[2];
                        evlist.collisions.insert(bound, ev);
                    }
                }
            }
        }

        template<typename T>
        void QueryGridCellsStepUp(Islander::Collision::CollisionGrid& grid, T& obj, float* velocity, Islander::Collision::CollisionDynamicMesh* mesh, float maxStepUp, Islander::Collision::CollisionEventList& evlist)
        {
            IslanderCollisionBox startBox;
            GetBoundingBox(obj, startBox);

            IslanderCollisionBox endBox;
            endBox.maxx = startBox.maxx + velocity[0];
            endBox.maxy = startBox.maxy + velocity[1];
            endBox.maxz = startBox.maxz + velocity[2];
            endBox.minx = startBox.minx + velocity[0];
            endBox.miny = startBox.miny + velocity[1];
            endBox.minz = startBox.minz + velocity[2];

            constexpr int max_hits = 16;
            Islander::Collision::CollisionStaticMesh* hits[max_hits];
            int hitcount = QueryGridCells(grid, startBox, endBox, mesh, max_hits, hits);

            for (int i = 0; i < hitcount; i++)
            {
                auto& static_mesh = hits[i];
                static_mesh->hit = false;   // reset the flag

                bool hit = false;
                float collisionY = 0;
                bool canStepUp = false;

                //if (static_mesh->childboxcount > 0)
                //{
                //    for (int k = 0; k < static_mesh->childboxcount; k++)
                //    {
                //        float dist = SeparateObjects(endBox, static_mesh->childbox[k]);

                //        if (dist > 0 && dist < maxStepUp)
                //        {
                //            canStepUp = true;

                //            if (dist > collisionY)
                //            {
                //                collisionY = dist;
                //            }
                //        }
                //    }
                //}

                if (static_mesh->triangleCollider && static_mesh->triangles.size() > 0)
                {
                    for (auto& tri : static_mesh->triangles)
                    {
                        float dist = SeparateObjects(endBox, tri);

                        if (dist > 0 && dist < maxStepUp)
                        {
                            canStepUp = true;

                            if (dist > collisionY)
                            {
                                collisionY = dist;
                            }
                        }
                    }
                }
                else if (static_mesh->heightmapCollider)
                {
                //    float height;
                //    if (Islander::Model::SampleHeightmap(static_mesh->heightmap,
                //        (endBox.minx + endBox.maxx) / 2.0f - static_mesh->transform.px,
                //        (endBox.minz + endBox.maxz) / 2.0f - static_mesh->transform.pz, &height))
                //    {
                //        height += static_mesh->transform.py;

                //        // ?
                //    }
                    continue;
                }
                else
                {
                    float dist = SeparateObjects(endBox, static_mesh->obb);

                    if (dist > 0 && dist < maxStepUp)
                    {
                        canStepUp = true;

                        if (dist > collisionY)
                        {
                            collisionY = dist;
                        }
                    }
                }

                if (canStepUp)
                {
                    auto& bound = std::lower_bound(evlist.stepUps.begin(), evlist.stepUps.end(), collisionY, [](Islander::Collision::CollisionEvent& ev, float y) { return y < ev.collisionY; });
                    Islander::Collision::CollisionEvent ev;
                    ev.collisionY = collisionY;
                    ev.mesh = static_mesh;
                    ev.dynamic = false;
                    evlist.stepUps.insert(bound, ev);
                }
            }
        }
    }
}
