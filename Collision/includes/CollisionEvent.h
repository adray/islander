#pragma once
#include <vector>


namespace Islander
{
    namespace Collision
    {
        struct CollisionStaticMesh;
        struct CollisionDynamicMesh;

        struct CollisionEvent
        {
            union
            {
                Islander::Collision::CollisionStaticMesh* mesh;
                Islander::Collision::CollisionDynamicMesh* dynMesh;
            };
            union
            {
                float collisionTime;
                float collisionY;
            };
            float normal[3];
            bool dynamic;
        };

        struct CollisionEventList
        {
            std::vector<CollisionEvent> collisions; // sorted by collision time, earliest to latest
            std::vector<CollisionEvent> stepUps; // sorted by collisionY, highest to lowest
        };

        void CollisionListInsertAtLowerBound(CollisionEventList& evList, float collisionTime, CollisionEvent& ev);
        int ResolveCollisions(CollisionEventList& events, float& collisionY, float& collisionTime, bool& stepUp, bool& collide, float* normal);
    }
}
