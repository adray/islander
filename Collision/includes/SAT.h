#pragma once
#include "CollisionTypes.h"
#include "Vector.h"
#include <vector>

/*
SAT (Separating Axis Theorem)

This is if a line (2D) plane (3D) can be drawn between two convex shapes then they are not colliding.
It can be determined by taking the normal of the line or plane and constructing a new line from the normal.
Then projecting the vertices of both shapes onto the line and seeing if they overlap anywhere.
In many cases projecting all the vertices for each line is not needed, as there can be optimizations.

*/

namespace Islander
{
    namespace Collision
    {
        struct ConvexPolyhedra
        {
            struct Face
            {
                float point[3];
                float normal[3];
            };

            struct Edge
            {
                float pos1[3];
                float pos2[3];
                float dir[3];
            };

            //std::vector<Face> faces;
            //std::vector<Edge> edges;
            //std::vector<float> points;
            float points[24];
            Face faces[6];
            Edge edges[12];

            int numPoints;
            int numFaces;
            int numEdges;
        };

        void CreateConvexPolyhedra(const IslanderCollisionOBB& obb, ConvexPolyhedra& polyhedra);

        void CreateConvexPolyhedra(const IslanderCollisionBox& box, ConvexPolyhedra& polyhedra);

        void PopulateFace(ConvexPolyhedra::Face* face,
            float* normal,
            float px, float py, float pz);

        void PopulateEdge(ConvexPolyhedra::Edge* edge, const float* p, const float* q);

        int WhichSide(const ConvexPolyhedra& p, const float* pt, const float* normal);

        bool ConvexPolyhedraTest(const ConvexPolyhedra& p1, const ConvexPolyhedra& p2);
        
        bool SATConvexPolyhedraTest(const ConvexPolyhedra& polyhedra, const IslanderCollisionTriangle& tri);

        template<typename T>
        bool SATConvexPolyhedraTest(const T& obj, const IslanderCollisionTriangle& tri)
        {
            ConvexPolyhedra polyhedra;
            CreateConvexPolyhedra(obj, polyhedra);
            return SATConvexPolyhedraTest(polyhedra, tri);
        }

        template<typename T, typename K>
        bool SATConvexPolyhedraTest(const T& obj1, const K& obj2)
        {
            ConvexPolyhedra p1;
            ConvexPolyhedra p2;

            CreateConvexPolyhedra(obj1, p1);
            CreateConvexPolyhedra(obj2, p2);

            return ConvexPolyhedraTest(p1, p2);
        }
    }
}
