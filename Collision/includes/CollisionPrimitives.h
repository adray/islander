#pragma once
#include "CollisionTypes.h"
#include "Polygon.h"

namespace Islander
{
    namespace Model
    {
        struct Heightmap;
    }

    namespace Numerics
    {
        struct Plane2;
    }

    namespace Collision
    {
        struct CollisionTransform
        {
            // Object transform.
            float px, py, pz;
            float rx, ry, rz;
            float sx, sy, sz;
        };

        struct CollisionStaticMesh
        {
            bool active;

            // Static (not moving) object in the system.
            // Defined as one of:
            // - Array of triangles
            // - Box
            // - Heightmap

            // Size of box.
            IslanderCollisionBox box;
            IslanderCollisionBox parentBox;
            IslanderCollisionOBB obb;

            // Object transform.
            CollisionTransform transform;

            // Object is not solid, acts like a trigger.
            bool trigger;

            // Object is only for rays.
            bool rayonly;

            // If the object uses a triangle collider.
            bool triangleCollider;

            // If the object uses a heightmap collider.
            bool heightmapCollider;

            // Triangle data
            Islander::Model::PolygonModel* meshData;

            // Child-boxes
            float* vertices;
            std::vector<IslanderCollisionTriangle> triangles;

            Islander::Model::Heightmap* heightmap;

            // The material of the object.
            int material;

            // Hit data
            bool hit;
        };

        struct CollisionDynamicMesh
        {
            bool active;
            float coyoteTimeElapsed;

            IslanderCollisionDynamicMeshInfo info;

            IslanderCollisionData colData;

            // Triangle data
            Islander::Model::PolygonModel* meshData;

            // Animations.
            CollisionDynamicMesh* parentMesh;

            // The platform it is on (if any).
            ISLANDER_COLLISION_DYNAMIC_MESH platformMesh;

            // The object being pushed (if any).
            ISLANDER_COLLISION_DYNAMIC_MESH pushMesh;

            // The constraints.
            int constraints[ISLANDER_COLLISION_CONSTRAINT_COUNT];
            int constraintCount;

            // Hit data
            bool hit;
        };

        bool CollisionBoxTest(const IslanderCollisionBox& a, const IslanderCollisionBox& b);
        bool PointBoxCollision(const IslanderCollisionBox& box, float x, float y, float z);
        bool RayPlaneTest(float* origin, float* dir, Islander::Numerics::Plane2* plane, float& t);
        bool SphereTriangleCollision(float* centre, float radius, const IslanderCollisionTriangle& tri);
        bool CapsuleTriangleCollision(const IslanderCollisionCapsule& capsule, const IslanderCollisionTriangle& tri);
        void CalculateTriangleNormal(const float* v0, const float* v1, const float* v2, float* normal);

        float HeightmapSweptCollision(Islander::Model::Heightmap* heightmap, CollisionTransform& transform, const IslanderCollisionCapsule& capsule, float* velocity, float* normal, float* triVerts);
        float HeightmapSweptCollision(Islander::Model::Heightmap* heightmap, CollisionTransform& transform, const IslanderCollisionSphere& sphere, float* velocity, float* normal, float* triVerts);
        float HeightmapSweptCollision(Islander::Model::Heightmap* heightmap, CollisionTransform& transform, const IslanderCollisionBox& box, float* velocity, float* normal, float* triVerts);
        float HeightmapSweptCollision(Islander::Model::Heightmap* heightmap, CollisionTransform& transform, const IslanderCollisionOBB& obb, float* velocity, float* normal, float* triVerts);

        float TriangleMeshSweptCollision(const std::vector<IslanderCollisionTriangle>& triangles, const IslanderCollisionCapsule& capsule, float* velocity, float* normal, float* triVerts);
        float TriangleMeshSweptCollision(const std::vector<IslanderCollisionTriangle>& triangles, const IslanderCollisionSphere& sphere, float* velocity, float* normal, float* triVerts);
        float TriangleMeshSweptCollision(const std::vector<IslanderCollisionTriangle>& triangles, const IslanderCollisionBox& box, float* velocity, float* normal, float* triVerts);
        float TriangleMeshSweptCollision(const std::vector<IslanderCollisionTriangle>& triangles, const IslanderCollisionOBB& obb, float* velocity, float* normal, float* triVerts);

        void CreateTransformedAABB(float px, float py, float pz, float rx, float ry, float rz, float sx, float sy, float sz, IslanderCollisionBox& box);

        void CalculateOBB(IslanderCollisionOBB& boxStart, const IslanderCollisionDynamicMeshInfo& info, Islander::Collision::CollisionDynamicMesh* parentMesh);
        void CalculateOBB(IslanderCollisionOBB& boxStart, Islander::Collision::CollisionDynamicMesh* mesh);
        void CalculateBox(IslanderCollisionBox& boxStart, Islander::Collision::CollisionDynamicMesh* mesh);
        void CalculateSphere(IslanderCollisionSphere& sphereStart, Islander::Collision::CollisionDynamicMesh* mesh);
        void CalculateCapsule(IslanderCollisionCapsule& capsuleStart, Islander::Collision::CollisionDynamicMesh* mesh);

        float DispatchCollisionSweptTest(IslanderCollisionCapsule& collider, IslanderCollisionDynamicMeshInfo& obj2, float* velocity, float* normal, int* iterations);
        float DispatchCollisionSweptTest(IslanderCollisionSphere& collider, IslanderCollisionDynamicMeshInfo& obj2, float* velocity, float* normal, int* iterations);
        float DispatchCollisionSweptTest(IslanderCollisionBox& collider, IslanderCollisionDynamicMeshInfo& obj2, float* velocity, float* normal, int* iterations);
        float DispatchCollisionSweptTest(IslanderCollisionOBB& collider, IslanderCollisionDynamicMeshInfo& obj2, float* velocity, float* normal, int* iterations);
        bool DispatchCollisionTest(IslanderCollisionCapsule& collider, IslanderCollisionDynamicMeshInfo& obj2, float* normal);
        bool DispatchCollisionTest(IslanderCollisionSphere& collider, IslanderCollisionDynamicMeshInfo& obj2, float* normal);
        bool DispatchCollisionTest(IslanderCollisionBox& collider, IslanderCollisionDynamicMeshInfo& obj2, float* normal);
        bool DispatchCollisionTest(IslanderCollisionOBB& collider, IslanderCollisionDynamicMeshInfo& obj2, float* normal);

        bool IsAbove(const IslanderCollisionCapsule& obj, const IslanderCollisionDynamicMeshInfo& mesh);
        bool IsAbove(const IslanderCollisionBox& obj, const IslanderCollisionDynamicMeshInfo& mesh);
        float StepUpToPlatform(IslanderCollisionCapsule& obj, IslanderCollisionDynamicMeshInfo& mesh);

        static void GetBoundingBox(IslanderCollisionBox& box, IslanderCollisionBox& outBox)
        {
            outBox = box;
        }

        static void GetBoundingBox(const IslanderCollisionSphere& sphere, IslanderCollisionBox& outBox)
        {
            outBox.maxx = sphere.centreX + sphere.radius;
            outBox.minx = sphere.centreX - sphere.radius;
            outBox.maxy = sphere.centreY + sphere.radius;
            outBox.miny = sphere.centreY - sphere.radius;
            outBox.maxz = sphere.centreZ + sphere.radius;
            outBox.minz = sphere.centreZ - sphere.radius;
        }

        static void GetBoundingBox(const IslanderCollisionCapsule& capsule, IslanderCollisionBox& outBox)
        {
            outBox.maxx = capsule.centreX + capsule.radius;
            outBox.minx = capsule.centreX - capsule.radius;
            outBox.maxy = capsule.centreY + capsule.radius + capsule.height / 2;
            outBox.miny = capsule.centreY - capsule.radius - capsule.height / 2;
            outBox.maxz = capsule.centreZ + capsule.radius;
            outBox.minz = capsule.centreZ - capsule.radius;
        }

        static void GetBoundingBox(const IslanderCollisionOBB& obb, IslanderCollisionBox& outBox)
        {
            outBox.maxx = obb.verticies[0];
            outBox.minx = obb.verticies[0];
            outBox.maxy = obb.verticies[1];
            outBox.miny = obb.verticies[1];
            outBox.maxz = obb.verticies[2];
            outBox.minz = obb.verticies[2];

            for (int i = 1; i < 8; i++)
            {
                outBox.maxx = std::max(outBox.maxx, obb.verticies[i * 3]);
                outBox.minx = std::min(outBox.minx, obb.verticies[i * 3]);
                outBox.maxy = std::max(outBox.maxy, obb.verticies[i * 3 + 1]);
                outBox.miny = std::min(outBox.miny, obb.verticies[i * 3 + 1]);
                outBox.maxz = std::max(outBox.maxz, obb.verticies[i * 3 + 2]);
                outBox.minz = std::min(outBox.minz, obb.verticies[i * 3 + 2]);
            }
        }

        static void GetBoundingBox(Islander::Collision::CollisionDynamicMesh* mesh, IslanderCollisionBox& meshbox)
        {
            switch (mesh->info.collisionShape)
            {
            case ISLANDER_COLLISION_SHAPE_BOX:
                IslanderCollisionBox box = mesh->info.box;
                Islander::Collision::CalculateBox(box, mesh);
                GetBoundingBox(box, meshbox);
                break;
            case ISLANDER_COLLISION_SHAPE_CAPSULE:
                IslanderCollisionCapsule capsule = mesh->info.capsule;
                Islander::Collision::CalculateCapsule(capsule, mesh);
                GetBoundingBox(capsule, meshbox);
                break;
            case ISLANDER_COLLISION_SHAPE_ORIENTATED_BOX:
                IslanderCollisionOBB obb = mesh->info.obb;
                Islander::Collision::CalculateOBB(obb, mesh);
                GetBoundingBox(obb, meshbox);
                break;
            case ISLANDER_COLLISION_SHAPE_SPHERE:
                IslanderCollisionSphere sphere = mesh->info.sphere;
                Islander::Collision::CalculateSphere(sphere, mesh);
                GetBoundingBox(sphere, meshbox);
                break;
            default:
                // Cannot insert
                return;
            }
        }

        void ClearColliders(Islander::Collision::CollisionDynamicMesh* mesh);
    }
}
