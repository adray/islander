#pragma once
#include "GJK.h"
#include <queue>

namespace Islander
{
    namespace Collision
    {
        struct EPAEdge
        {
            int points[2];
        };

        struct EPATri
        {
            Islander::Numerics::Plane p;
            std::array<int, 3> points;
        };

        void CreatePlaneFromTriangle(EPATri& tri, std::vector<GJKVec3>& verts);

        float FindClosestFaceToOrigin(const std::vector<EPATri>& tris, float* normal, int* index);

        void CreateEPATri(EPATri& tri, std::vector<GJKVec3>& verts, int i1, int i2, int i3);

        bool RayTriTest(std::vector<GJKVec3>& verts, EPATri& tri, float* o, float* dir, float* pt, float* t);

        // Ray-triangle sweep as described in:
        // https://sourceforge.net/p/gdalgorithms/mailman/message/16750567/

        template<typename T, typename S>
        static float RayTriangleSweep(const T& obj1, const S& obj2, float* velocity, std::array<GJKVec3, 4>& points, int numPoints, float* normal, int* iterations)
        {
            std::vector<GJKVec3> verts;
            verts.push_back(points[0]);
            verts.push_back(points[1]);
            verts.push_back(points[2]);
            verts.push_back(points[3]);

            std::vector<EPATri> polytope;

            EPATri t[4];
            CreateEPATri(t[0], verts, 0, 1, 2);
            CreateEPATri(t[1], verts, 0, 2, 3);
            CreateEPATri(t[2], verts, 0, 1, 3);
            CreateEPATri(t[3], verts, 1, 2, 3);

            polytope.push_back(t[0]);
            polytope.push_back(t[1]);
            polytope.push_back(t[2]);
            polytope.push_back(t[3]);

            float o[3] = { 0,0,0 };

            const int MAX_ITERATIONS = 25;

            for (int i = 0; i < MAX_ITERATIONS; i++)
            {
                // Ray-triangle test, remove point not included in this tri and pick new point in dir of tri normal

                bool found = false;
                EPATri foundTri;
                float maxT = -std::numeric_limits<float>::max();
                float intersect[3];

                for (auto& tri : polytope)
                {
                    float pt[3];
                    float t;
                    if (RayTriTest(verts, tri, o, velocity, pt, &t) && (t > maxT || !found))
                    {
                        foundTri = tri;
                        maxT = t;
                        Islander::Numerics::CopyVec3(pt, intersect);
                        found = true;
                    }
                }

                if (!found)
                {
                    *iterations = i;
                    return 1;
                }

                float support[3];
                normal[0] = foundTri.p.x;
                normal[1] = foundTri.p.y;
                normal[2] = foundTri.p.z;
                Support_Swept(obj1, obj2, velocity, normal, support);

                // check for duplicate vertex
                for (auto& v : verts)
                {
                    float dx = v[0] - support[0];
                    float dy = v[1] - support[1];
                    float dz = v[2] - support[2];

                    if (abs(dx * dx + dy * dy + dz * dz) < 0.001f)
                    {
                        // Flip normal
                        normal[0] = -normal[0];
                        normal[1] = -normal[1];
                        normal[2] = -normal[2];

                        intersect[0] = velocity[0] - intersect[0];
                        intersect[1] = velocity[1] - intersect[1];
                        intersect[2] = velocity[2] - intersect[2];

                        // Clip intersection point to length of velocity
                        if (velocity[0] > 0)
                        {
                            intersect[0] = std::max(0.0f, intersect[0]);
                        }
                        else
                        {
                            intersect[0] = std::min(0.0f, intersect[0]);
                        }

                        if (velocity[1] > 0)
                        {
                            intersect[1] = std::max(0.0f, intersect[1]);
                        }
                        else
                        {
                            intersect[1] = std::min(0.0f, intersect[1]);
                        }

                        if (velocity[2] > 0)
                        {
                            intersect[2] = std::max(0.0f, intersect[2]);
                        }
                        else
                        {
                            intersect[2] = std::min(0.0f, intersect[2]);
                        }

                        *iterations = i;
                        const float t = Islander::Numerics::LengthSqr(intersect[0], intersect[1], intersect[2]) / Islander::Numerics::LengthSqr(velocity[0], velocity[1], velocity[2]);
                        return t;
                    }
                }

                polytope.clear();

                auto v1 = verts[foundTri.points[0]];
                auto v2 = verts[foundTri.points[1]];
                auto v3 = verts[foundTri.points[2]];
                verts.clear();
                verts.push_back(v1);
                verts.push_back(v2);
                verts.push_back(v3);

                GJKVec3 newVert;
                Islander::Numerics::CopyVec3(support, newVert);
                verts.push_back(newVert);

                CreateEPATri(t[0], verts, 0, 1, 3);
                CreateEPATri(t[1], verts, 1, 2, 3);
                CreateEPATri(t[2], verts, 0, 2, 3);
                CreateEPATri(t[3], verts, 0, 1, 2);

                polytope.push_back(t[0]);
                polytope.push_back(t[1]);
                polytope.push_back(t[2]);
                polytope.push_back(t[3]);
            }

            *iterations = MAX_ITERATIONS;
            return 0;
        }

        template<typename T>
        static void EraseSwap(std::vector<T>& vec, int index)
        {
            vec[index] = vec[vec.size() - 1];
            vec.erase(vec.begin() + vec.size() - 1);
        }

        void InsertEdge(std::vector<EPAEdge>& edges, int i1, int i2);

        //static void Validate(std::vector<EPATri> polytope, std::vector<GJKVec3>& verts)
        //{
        //    int hits = 0;
        //    for (int i = 0; i < polytope.size(); i++)
        //    {
        //        auto& tri = polytope[i];
        //        auto& v1 = verts[tri.points[0]];
        //        auto& v2 = verts[tri.points[1]];
        //        auto& v3 = verts[tri.points[2]];

        //        float centre[3] = {
        //            v1.v[0] + v2.v[0] + v2.v[0],
        //            v1.v[1] + v2.v[1] + v2.v[1],
        //            v1.v[2] + v2.v[2] + v2.v[2]
        //        };
        //        float normal[3] = { tri.p.x, tri.p.y, tri.p.z };

        //        for (int j = 0; j < polytope.size(); j++)
        //        {
        //            if (i != j)
        //            {
        //                auto& tri2 = polytope[i];
        //                float pt[3]; float t;
        //                if (RayTriTest(verts, tri2, centre, normal, pt, &t))
        //                {
        //                    hits++;
        //                    break;
        //                }
        //            }
        //        }
        //    }
        //    assert(hits == 0);
        //}

        template<typename T, typename S>
        static void EPAContactPoints(const T& box1, const S& box2, float* direction, float* pt1, float* pt2)
        {
            float negativeDirection[3];
            Islander::Numerics::NegateVec3(direction, negativeDirection);

            FindFurthestPoint(box1, direction, pt1);
            FindFurthestPoint(box2, negativeDirection, pt2);
        }

        template<typename T, typename S>
        static float EPA(const T& obj1, const S& obj2, std::array<GJKVec3, 4>& points, int numPoints, float* normal, float* outPt1, float* outPt2, int* iterations)
        {
            iterations = 0;

            // Expanding Polytope algorithm
            // Returns the penetration depth and normal e.g. the closest point on the minkowski difference to the origin.

            std::vector<GJKVec3> verts;
            verts.push_back(points[0]);
            verts.push_back(points[1]);
            verts.push_back(points[2]);
            verts.push_back(points[3]);

            std::vector<EPATri> polytope;

            EPATri t[4];
            CreateEPATri(t[0], verts, 0, 1, 2);
            CreateEPATri(t[1], verts, 0, 2, 3);
            CreateEPATri(t[2], verts, 0, 1, 3);
            CreateEPATri(t[3], verts, 1, 2, 3);

            polytope.push_back(t[0]);
            polytope.push_back(t[1]);
            polytope.push_back(t[2]);
            polytope.push_back(t[3]);

            std::vector<EPAEdge> edges;

            while (true)
            {
                //Validate(polytope, verts);

                int index;
                float dist = FindClosestFaceToOrigin(polytope, normal, &index);

                float support[3];
                Support(obj1, obj2, normal, support);

                float len = Islander::Numerics::Dot(support[0], support[1], support[2], normal[0], normal[1], normal[2]);
                if (abs(len - dist) < 0.001f)
                {
                    EPAContactPoints(obj1, obj2, normal, outPt1, outPt2);

                    // P projected onto normal is the penetration vector
                    Islander::Numerics::NegateVec3(normal, normal);

                    return len;
                }

                // Extend the polytope to P (support point)

                // remove the triangles not facing P
                // and add in triangles to include point P

                auto closestTri = polytope[index];
                InsertEdge(edges, closestTri.points[0], closestTri.points[1]);
                InsertEdge(edges, closestTri.points[1], closestTri.points[2]);
                InsertEdge(edges, closestTri.points[2], closestTri.points[0]);
                EraseSwap(polytope, index);

                int i = 0;
                while (i < polytope.size())
                {
                    auto tri = polytope[i];
                    float normal[3] = { tri.p.x, tri.p.y, tri.p.z };

                    float v[3];
                    Islander::Numerics::SubVec3(support, verts[tri.points[0]].v, v);
                    if (SameDirection(normal, v))
                    {
                        InsertEdge(edges, tri.points[0], tri.points[1]);
                        InsertEdge(edges, tri.points[1], tri.points[2]);
                        InsertEdge(edges, tri.points[2], tri.points[0]);

                        EraseSwap(polytope, i);
                    }
                    else
                    {
                        i++;
                    }
                }

                // TODO: sometimes we are hitting duplicates we shouldn't.
                // Observed this occurs when the distance to one of the the initial traingles is 0.
                bool duplicate = false;
                for (int i = 0; i < verts.size(); i++)
                {
                    float diff[3];
                    Islander::Numerics::SubVec3(support, verts[i], diff);
                    if (Islander::Numerics::LengthSqr(diff) < 0.001f)
                    {
                        duplicate = true;
                        break;
                    }
                }

                if (!duplicate)
                {
                    GJKVec3 newVert;
                    Islander::Numerics::CopyVec3(support, newVert);
                    verts.push_back(newVert);

                    for (auto& edge : edges)
                    {
                        EPATri tri;
                        CreateEPATri(tri, verts, edge.points[0], edge.points[1], verts.size() - 1);
                        polytope.push_back(tri);
                    }
                }

                edges.clear();
                iterations++;
            }
        }

        template<typename T, typename S>
        static bool GJK_EPA(const T& obj1, const S& obj2, float* normal, float* depth, float* outPt1, float* outPt2)
        {
            int iterations;
            return _GJK<T, S>(obj1, obj2, normal, depth, outPt1, outPt2, &iterations, EPA);
        }

        template<typename T, typename S>
        static float GJK_Swept(const T& obj1, const S& obj2, float* velocity, float* normal)
        {
            int iterations;
            // Use RayTriangleSweep rather than EPA since EPA implementation doesn't handle swept collisions.
            return GJK_Swept(obj1, obj2, velocity, normal, &iterations, RayTriangleSweep);
        }
        
        template<typename T, typename S>
        static float GJK_Swept(const T& obj1, const S& obj2, float* velocity, float* normal, int* iterations)
        {
            // Use RayTriangleSweep rather than EPA since EPA implementation doesn't handle swept collisions.
            return GJK_Swept(obj1, obj2, velocity, normal, iterations, RayTriangleSweep);
        }
    }
}
