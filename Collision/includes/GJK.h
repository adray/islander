#pragma once
#include "CollisionTypes.h"
#include "Vector.h"
#include "Plane.h"
#include <array>
#include <limits>
#include <vector>
#include <algorithm>

// References
// https://blog.winter.dev/2020/gjk-algorithm/
// https://www.youtube.com/watch?v=Qupqu1xe7Io
//

namespace Islander
{
    namespace Collision
    {
        struct GJKVec3
        {
            float v[3];

            operator float* () { return &v[0]; }
        };

        bool NextSimplex(std::array<GJKVec3, 4>& points, int* numPoints, float* dir);

        static bool SameDirection(float* pt1, float* pt2)
        {
            return Islander::Numerics::Dot(pt1[0], pt1[1], pt1[2], pt2[0], pt2[1], pt2[2]) > 0;
        }

        static void FindFurthestPoint(const IslanderCollisionSphere& sphere, float* direction, float* pt)
        {
            float normalized[3];

            Islander::Numerics::Normalize(direction[0], direction[1], direction[2], normalized[0], normalized[1], normalized[2]);

            pt[0] = normalized[0] * sphere.radius + sphere.centreX;
            pt[1] = normalized[1] * sphere.radius + sphere.centreY;
            pt[2] = normalized[2] * sphere.radius + sphere.centreZ;
        }

        static void FindFurthestPoint(const IslanderCollisionOBB& obb, float* direction, float* pt)
        {
            float maxDistance = std::numeric_limits<float>::lowest();

            //float verts[] = {
            //    obb.centre[0] + obb.half_size[0], obb.centre[1] + obb.half_size[1], obb.centre[2] + obb.half_size[2],
            //    obb.centre[0] - obb.half_size[0], obb.centre[1] + obb.half_size[1], obb.centre[2] + obb.half_size[2],
            //    obb.centre[0] + obb.half_size[0], obb.centre[1] - obb.half_size[1], obb.centre[2] + obb.half_size[2],
            //    obb.centre[0] + obb.half_size[0], obb.centre[1] + obb.half_size[1], obb.centre[2] - obb.half_size[2],
            //    obb.centre[0] + obb.half_size[0], obb.centre[1] - obb.half_size[1], obb.centre[2] - obb.half_size[2],
            //    obb.centre[0] - obb.half_size[0], obb.centre[1] + obb.half_size[1], obb.centre[2] - obb.half_size[2],
            //    obb.centre[0] - obb.half_size[0], obb.centre[1] - obb.half_size[1], obb.centre[2] + obb.half_size[2],
            //    obb.centre[0] - obb.half_size[0], obb.centre[1] - obb.half_size[1], obb.centre[2] - obb.half_size[2]
            //};

            //for (int i = 0; i < 8; i++)
            //{
            //    float dist = Islander::Dot(verts[i * 3], verts[i * 3 + 1], verts[i * 3 + 2], direction[0], direction[1], direction[2]);
            //    if (dist > maxDistance)
            //    {
            //        maxDistance = dist;
            //        pt[0] = verts[i * 3];
            //        pt[1] = verts[i * 3 + 1];
            //        pt[2] = verts[i * 3 + 2];
            //    }
            //}

            for (int i = 0; i < 8; i++)
            {
                float dist = Islander::Numerics::Dot(obb.verticies[i * 3], obb.verticies[i * 3 + 1], obb.verticies[i * 3 + 2], direction[0], direction[1], direction[2]);
                if (dist > maxDistance)
                {
                    maxDistance = dist;
                    pt[0] = obb.verticies[i * 3];
                    pt[1] = obb.verticies[i * 3 + 1];
                    pt[2] = obb.verticies[i * 3 + 2];
                }
            }
        }

        static void FindFurthestPoint(const IslanderCollisionBox& box, float* direction, float* pt)
        {
            float maxDistance = std::numeric_limits<float>::lowest();

            float verts[] = {
                box.maxx, box.maxy, box.maxz,
                box.minx, box.maxy, box.maxz,
                box.maxx, box.miny, box.maxz,
                box.maxx, box.maxy, box.minz,
                box.maxx, box.miny, box.minz,
                box.minx, box.miny, box.maxz,
                box.minx, box.maxy, box.minz,
                box.minx, box.miny, box.minz
            };

            for (int i = 0; i < 8; i++)
            {
                float dist = Islander::Numerics::Dot(verts[i * 3], verts[i * 3 + 1], verts[i * 3 + 2], direction[0], direction[1], direction[2]);
                if (dist > maxDistance)
                {
                    maxDistance = dist;
                    pt[0] = verts[i * 3];
                    pt[1] = verts[i * 3 + 1];
                    pt[2] = verts[i * 3 + 2];
                }
            }
        }

        static void FindFurthestPointLineSegment(float* start, float* end, float* direction, float* pt)
        {
            float d1 = Islander::Numerics::Dot(start[0], start[1], start[2], direction[0], direction[1], direction[2]);
            float d2 = Islander::Numerics::Dot(end[0], end[1], end[2], direction[0], direction[1], direction[2]);

            if (d1 > d2)
            {
                Islander::Numerics::CopyVec3(start, pt);
            }
            else
            {
                Islander::Numerics::CopyVec3(end, pt);
            }
        }

        static void FindFurthestPoint(const IslanderCollisionTriangle& triangle, float* direction, float* pt)
        {
            float maxDistance = std::numeric_limits<float>::lowest();

            for (int i = 0; i < 3; i++)
            {
                float* vert = triangle.vertices[i];

                float dist = Islander::Numerics::Dot(vert[0], vert[1], vert[2], direction[0], direction[1], direction[2]);
                if (dist > maxDistance)
                {
                    maxDistance = dist;
                    Islander::Numerics::CopyVec3(vert, pt);
                }
            }
        }

        static void FindFurthestPoint(const IslanderCollisionCapsule& capsule, float* direction, float* pt)
        {
            float normalized[3];

            Islander::Numerics::Normalize(direction[0], direction[1], direction[2], normalized[0], normalized[1], normalized[2]);

            pt[0] = normalized[0] * capsule.radius + capsule.centreX;
            pt[1] = normalized[1] * capsule.radius + capsule.centreY;
            pt[2] = normalized[2] * capsule.radius + capsule.centreZ;

            float pt2[3];

            float start[3] = { 0, -capsule.height / 2, 0 };
            float end[3] = { 0, capsule.height / 2, 0 };
            FindFurthestPointLineSegment(start, end, direction, pt2);

            Islander::Numerics::AddVec3(pt, pt2, pt);
        }

        template<typename T, typename S>
        static void Support(const T& box1, const S& box2, float* direction, float* support)
        {
            float negativeDirection[3];
            Islander::Numerics::NegateVec3(direction, negativeDirection);

            float pt1[3];
            float pt2[3];

            FindFurthestPoint(box1, direction, pt1);
            FindFurthestPoint(box2, negativeDirection, pt2);

            Islander::Numerics::SubVec3(pt1, pt2, support);
        }

        template<typename T, typename S>
        static void Support_Swept(const T& box1, const S& box2, float* velocity, float* direction, float* support)
        {
            float negativeDirection[3];
            Islander::Numerics::NegateVec3(direction, negativeDirection);

            float pt1[3];
            float pt2[3];
            float pt3[3];

            FindFurthestPoint(box1, direction, pt1);
            FindFurthestPoint(box2, negativeDirection, pt2);

            float start[3] = { 0,0,0 };
            FindFurthestPointLineSegment(start, velocity, direction, pt3);

            Islander::Numerics::AddVec3(pt1, pt3, pt1);
            Islander::Numerics::SubVec3(pt1, pt2, support);
        }

        template<typename T, typename S>
        static bool _GJK(const T& obj1, const S& obj2, float* normal, float* depth, float* outPt1, float* outPt2, int* pen_iterations,
            float (*GJKPenetration)(const T& obj1, const S& obj2, std::array<GJKVec3, 4>& points, int numPoints, float* normal, float* outPt1, float* outPt2, int* iterations))
        {
            float support[3];
            float x_dir[3] = { 1, 0, 0 }; // arbitary direction

            Support(obj1, obj2, x_dir, support);

            int numPoints = 1;
            std::array<GJKVec3, 4> points; // size of simplex is number of dimensions + 1
            Islander::Numerics::CopyVec3(support, points[0]);

            float dir[3];
            Islander::Numerics::NegateVec3(support, dir);

            const int MAX_ITERATIONS = 25;

            for (int i = 0; i < MAX_ITERATIONS; i++)
            {
                Support(obj1, obj2, dir, support);

                if (Islander::Numerics::Dot(support[0], support[1], support[2], dir[0], dir[1], dir[2]) <= 0)
                {
                    return false;
                }

                Islander::Numerics::CopyVec3(points[2], points[3]);
                Islander::Numerics::CopyVec3(points[1], points[2]);
                Islander::Numerics::CopyVec3(points[0], points[1]);
                Islander::Numerics::CopyVec3(support, points[0]);
                numPoints = std::min(numPoints + 1, 4);

                if (NextSimplex(points, &numPoints, dir))
                {
                    if (GJKPenetration != nullptr)
                    {
                        assert(numPoints == 4);
                        *depth = GJKPenetration(obj1, obj2, points, numPoints, normal, outPt1, outPt2, pen_iterations);
                    }

                    return true;
                }
            }

            return false;
        }

        template<typename T, typename S>
        static bool GJK(const T& obj1, const S& obj2, float* normal)
        {
            return _GJK<T, S>(obj1, obj2, normal, nullptr, nullptr, nullptr, nullptr, nullptr);
        }

        template<typename T, typename S>
        static float GJK_Swept(const T& obj1, const S& obj2, float* velocity, float* normal, int* pen_iterations,
            float (*GJKPenetration)(const T& obj1, const S& obj2, float* velocity, std::array<GJKVec3, 4>& points, int numPoints, float* normal, int* iteration))
        {
            float support[3];
            float x_dir[3] = { 1, 0, 0 }; // arbitary direction

            Support_Swept(obj1, obj2, velocity, x_dir, support);

            int numPoints = 1;
            std::array<GJKVec3, 4> points; // size of simplex is number of dimensions + 1
            Islander::Numerics::CopyVec3(support, points[0]);

            float dir[3];
            Islander::Numerics::NegateVec3(support, dir);

            const int MAX_ITERATIONS = 25;

            while (true)
            //for (int i = 0; i < MAX_ITERATIONS; i++)
            {
                Support_Swept(obj1, obj2, velocity, dir, support);

                if (Islander::Numerics::Dot(support[0], support[1], support[2], dir[0], dir[1], dir[2]) <= 0)
                {
                    return 1;
                }

                Islander::Numerics::CopyVec3(points[2], points[3]);
                Islander::Numerics::CopyVec3(points[1], points[2]);
                Islander::Numerics::CopyVec3(points[0], points[1]);
                Islander::Numerics::CopyVec3(support, points[0]);
                numPoints = std::min(numPoints + 1, 4);

                if (NextSimplex(points, &numPoints, dir))
                {
                    if (Islander::Numerics::LengthSqr(velocity) > 0.f)
                    {
                        //return RayTriangleSweep(obj1, obj2, velocity, points, numPoints, normal);
                        return GJKPenetration(obj1, obj2, velocity, points, numPoints, normal, pen_iterations);
                    }
                    else
                    {
                        return 0;
                    }
                }
            }

            return 0;
        }
    }
}
