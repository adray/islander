#pragma once
#include "CollisionTypes.h"
#include <array>
#include <vector>

constexpr int COLLISION_DYNAMIC_GRID_CELL_SIZE = 16;

namespace Islander
{
    namespace Collision
    {
        struct CollisionDynamicMesh;

        struct DynamicCollisionGrid
        {
            struct Cell
            {
                std::vector<CollisionDynamicMesh*> items;
                IslanderCollisionBox box;
            };

            std::array<Cell, COLLISION_DYNAMIC_GRID_CELL_SIZE * COLLISION_DYNAMIC_GRID_CELL_SIZE * COLLISION_DYNAMIC_GRID_CELL_SIZE> cells;
            IslanderCollisionBox box;
            bool dirty;
        };

        void BuildDynamicGrid(DynamicCollisionGrid& grid, IslanderCollisionBox& box, CollisionDynamicMesh* meshes, int count, int group);
        void AddItemDynamicGrid(DynamicCollisionGrid& grid, Islander::Collision::CollisionDynamicMesh* mesh);
        void ClearDynamicCollisionGrid(DynamicCollisionGrid& grid);
        int QueryDynamicGrid(DynamicCollisionGrid& grid, Islander::Collision::CollisionDynamicMesh* mesh, int max_count, Islander::Collision::CollisionDynamicMesh** hits);
    }
}
