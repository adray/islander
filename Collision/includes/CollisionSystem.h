#pragma once
#include <deque>
#include <vector>
#include <array>
#include "CollisionTypes.h"
#include "StaticCollisionGrid.h"
#include "DynamicCollisionGrid.h"
#include "CollisionPrimitives.h"

constexpr int COLLISION_SYSTEM_MAX_STATIC_ITEMS = 1024;
constexpr int COLLISION_SYSTEM_MAX_DYNAMIC_ITEMS = 128;
constexpr int COLLISION_SYSTEM_DATA_ITEMS = ISLANDER_COLLISION_DATA_COUNT;

namespace Islander
{
    class Camera3D;

    namespace Model
    {
        struct PolygonModel;
    }

    namespace Collision
    {
        template<typename T, int size>
        struct CollisionArray
        {
            T array[size];
            int count;
            std::deque<int> freeslots;

            CollisionArray() : count(0) { }
        };

        struct CollisionSystem
        {
            CollisionArray<CollisionStaticMesh, COLLISION_SYSTEM_MAX_STATIC_ITEMS> static_mesh;
            CollisionArray<CollisionDynamicMesh, COLLISION_SYSTEM_MAX_DYNAMIC_ITEMS> dynamic_mesh;
            CollisionArray<CollisionStaticMesh, COLLISION_SYSTEM_MAX_STATIC_ITEMS> static_mesh_ray;
            CollisionArray<CollisionDynamicMesh, COLLISION_SYSTEM_MAX_DYNAMIC_ITEMS> dynamic_mesh_ray;
            CollisionGrid grid;
            DynamicCollisionGrid dynGrid;
        };

        CollisionSystem* CreateCollisionSystem();
        void RunCollision(CollisionSystem* system, float delta);
        bool AddStaticMesh(CollisionSystem* system, bool rayonly, bool triangleCollider, Islander::Model::PolygonModel* meshData, const CollisionTransform& transform, CollisionStaticMesh** mesh);
        bool AddDynamicMesh(CollisionSystem* system, bool rayonly, CollisionDynamicMesh** mesh);
        void RemoveStaticMesh(CollisionSystem* system, CollisionStaticMesh* mesh);
        void RemoveDynamicMesh(CollisionSystem* system, CollisionDynamicMesh* mesh);
        void UpdateStaticMesh(CollisionSystem* system, CollisionStaticMesh* mesh, IslanderCollisionStaticMeshInfo* meshInfo);
        void CreateDebugGeometry(CollisionSystem* system, IslanderCollisionGeometryData* gData);
        void CreateDebugGeometry(CollisionSystem* system, CollisionDynamicMesh* mesh, IslanderCollisionGeometryData* gData);
    }
}
