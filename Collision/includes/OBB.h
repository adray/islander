#pragma once
#include "CollisionTypes.h"

namespace Islander
{
    namespace Numerics
    {
        class Matrix4x4;
    }

    namespace Collision
    {
        void ComputeOBB(IslanderCollisionOBB& obb, const Islander::Numerics::Matrix4x4& matrix, const float* min, const float* max);

        void ComputeOBBFromVertices(IslanderCollisionOBB& obb);
    }
}
