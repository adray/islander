#include "StaticCollisionGrid.h"
#include "Vector.h"
#include <assert.h>

using namespace Islander::Numerics;

void Islander::Collision::BuildCollisionGrid(Islander::Collision::CollisionGrid& grid, int static_mesh_count, Islander::Collision::CollisionStaticMesh* static_mesh)
{
    if (grid.dirty)
    {
        grid.dirty = false;

        for (auto& cell : grid.cells)
        {
            cell.entries.clear();
        }

        if (static_mesh_count >= 1)
        {
            // Find out the bounding volume the grid should cover.
            IslanderCollisionBox& box = grid.box;
            auto mesh = &static_mesh[0];
            IslanderCollisionBox meshbox = mesh->box;
            Islander::Collision::CreateTransformedAABB(mesh->transform.px,
                mesh->transform.py,
                mesh->transform.pz,
                mesh->transform.rx,
                mesh->transform.ry,
                mesh->transform.rz,
                mesh->transform.sx,
                mesh->transform.sy,
                mesh->transform.sz,
                meshbox);
            box.maxx = meshbox.maxx;
            box.maxy = meshbox.maxy;
            box.maxz = meshbox.maxz;
            box.minx = meshbox.minx;
            box.miny = meshbox.miny;
            box.minz = meshbox.minz;

            for (int i = 1; i < static_mesh_count; i++)
            {
                mesh = &static_mesh[i];
                meshbox = mesh->box;
                Islander::Collision::CreateTransformedAABB(mesh->transform.px,
                    mesh->transform.py,
                    mesh->transform.pz,
                    mesh->transform.rx,
                    mesh->transform.ry,
                    mesh->transform.rz,
                    mesh->transform.sx,
                    mesh->transform.sy,
                    mesh->transform.sz,
                    meshbox);
                box.maxx = std::max(box.maxx, meshbox.maxx);
                box.maxy = std::max(box.maxy, meshbox.maxy);
                box.maxz = std::max(box.maxz, meshbox.maxz);
                box.minx = std::min(box.minx, meshbox.minx);
                box.miny = std::min(box.miny, meshbox.miny);
                box.minz = std::min(box.minz, meshbox.minz);
            }

            // Divide the grid into N x N cells. This doesn't divide along the Up axis.
            float cellSizeX = (box.maxx - box.minx) / COLLISION_SYSTEM_GRID_COUNT;
            float cellSizeZ = (box.maxz - box.minz) / COLLISION_SYSTEM_GRID_COUNT;

            for (int i = 0; i < grid.cells.size(); i++)
            {
                int x = i % COLLISION_SYSTEM_GRID_COUNT;
                int z = i / COLLISION_SYSTEM_GRID_COUNT;
                auto& cell = grid.cells[i];
                cell.box.maxx = box.minx + x * cellSizeX + cellSizeX;
                cell.box.minx = box.minx + x * cellSizeX;
                cell.box.maxz = box.minz + z * cellSizeZ + cellSizeZ;
                cell.box.minz = box.minz + z * cellSizeZ;
                cell.box.miny = box.miny;
                cell.box.maxy = box.maxy;
                cell.x = x;
                cell.z = z;
            }

            // Assign objects to cells.
            for (int i = 0; i < static_mesh_count; i++)
            {
                mesh = &static_mesh[i];
                meshbox = mesh->box;
                Islander::Collision::CreateTransformedAABB(mesh->transform.px,
                    mesh->transform.py,
                    mesh->transform.pz,
                    mesh->transform.rx,
                    mesh->transform.ry,
                    mesh->transform.rz,
                    mesh->transform.sx,
                    mesh->transform.sy,
                    mesh->transform.sz,
                    meshbox);
                int minCellX = std::min(COLLISION_SYSTEM_GRID_COUNT - 1, (int)((meshbox.minx - box.minx) / cellSizeX));
                int maxCellX = std::min(COLLISION_SYSTEM_GRID_COUNT - 1, (int)((meshbox.maxx - box.minx) / cellSizeX));

                int minCellZ = std::min(COLLISION_SYSTEM_GRID_COUNT - 1, (int)((meshbox.minz - box.minz) / cellSizeZ));
                int maxCellZ = std::min(COLLISION_SYSTEM_GRID_COUNT - 1, (int)((meshbox.maxz - box.minz) / cellSizeZ));

                if (minCellX >= 0 && minCellX < COLLISION_SYSTEM_GRID_COUNT &&
                    maxCellX >= 0 && maxCellX < COLLISION_SYSTEM_GRID_COUNT &&
                    minCellZ >= 0 && minCellZ < COLLISION_SYSTEM_GRID_COUNT &&
                    maxCellZ >= 0 && maxCellZ < COLLISION_SYSTEM_GRID_COUNT)
                {

                    assert(minCellX >= 0 && minCellX < COLLISION_SYSTEM_GRID_COUNT);
                    assert(maxCellX >= 0 && maxCellX < COLLISION_SYSTEM_GRID_COUNT);
                    assert(minCellZ >= 0 && minCellZ < COLLISION_SYSTEM_GRID_COUNT);
                    assert(maxCellZ >= 0 && maxCellZ < COLLISION_SYSTEM_GRID_COUNT);

                    for (int z = minCellZ; z < maxCellZ + 1; z++)
                    {
                        for (int x = minCellX; x < maxCellX + 1; x++)
                        {
                            int index = z * COLLISION_SYSTEM_GRID_COUNT + x;
                            //assert(CollisionBoxTest(system->grid.cells[index].box, meshbox));
                            if (Islander::Collision::CollisionBoxTest(grid.cells[index].box, meshbox))
                            {
                                grid.cells[index].entries.push_back(mesh);
                            }
                        }
                    }
                }
            }
        }
    }
}

int Islander::Collision::QueryGridCells(Islander::Collision::CollisionGrid& grid, IslanderCollisionBox& startBox, IslanderCollisionBox& endBox, Islander::Collision::CollisionDynamicMesh* mesh, int max_hits, Islander::Collision::CollisionStaticMesh** hits)
{
    float cellSizeX = (grid.box.maxx - grid.box.minx) / COLLISION_SYSTEM_GRID_COUNT;
    float cellSizeZ = (grid.box.maxz - grid.box.minz) / COLLISION_SYSTEM_GRID_COUNT;

    int minCellX = std::min(
        std::min(COLLISION_SYSTEM_GRID_COUNT - 1, (int)((startBox.minx - grid.box.minx) / cellSizeX)),
        std::min(COLLISION_SYSTEM_GRID_COUNT - 1, (int)((endBox.minx - grid.box.minx) / cellSizeX))
    );
    int maxCellX = std::max(
        std::min(COLLISION_SYSTEM_GRID_COUNT - 1, (int)((startBox.maxx - grid.box.minx) / cellSizeX)),
        std::min(COLLISION_SYSTEM_GRID_COUNT - 1, (int)((endBox.maxx - grid.box.minx) / cellSizeX))
    );

    int minCellZ = std::min(
        std::min(COLLISION_SYSTEM_GRID_COUNT - 1, (int)((startBox.minz - grid.box.minz) / cellSizeZ)),
        std::min(COLLISION_SYSTEM_GRID_COUNT - 1, (int)((endBox.minz - grid.box.minz) / cellSizeZ))
    );
    int maxCellZ = std::max(
        std::min(COLLISION_SYSTEM_GRID_COUNT - 1, (int)((startBox.maxz - grid.box.minz) / cellSizeZ)),
        std::min(COLLISION_SYSTEM_GRID_COUNT - 1, (int)((endBox.maxz - grid.box.minz) / cellSizeZ))
    );

    assert(minCellX >= 0 && minCellX < COLLISION_SYSTEM_GRID_COUNT);
    assert(maxCellX >= 0 && maxCellX < COLLISION_SYSTEM_GRID_COUNT);
    assert(minCellZ >= 0 && minCellZ < COLLISION_SYSTEM_GRID_COUNT);
    assert(maxCellZ >= 0 && maxCellZ < COLLISION_SYSTEM_GRID_COUNT);

    if (minCellX < 0 || minCellX >= COLLISION_SYSTEM_GRID_COUNT) { return 0; }
    if (maxCellX < 0 && maxCellX >= COLLISION_SYSTEM_GRID_COUNT) { return 0; }
    if (minCellZ < 0 && minCellZ >= COLLISION_SYSTEM_GRID_COUNT) { return 0; }
    if (maxCellZ < 0 && maxCellZ >= COLLISION_SYSTEM_GRID_COUNT) { return 0; }

    IslanderCollisionBox broadphaseBox;
    broadphaseBox.maxx = std::max(startBox.maxx, endBox.maxx);
    broadphaseBox.maxy = std::max(startBox.maxy, endBox.maxy);
    broadphaseBox.maxz = std::max(startBox.maxz, endBox.maxz);
    broadphaseBox.minx = std::min(startBox.minx, endBox.minx);
    broadphaseBox.miny = std::min(startBox.miny, endBox.miny);
    broadphaseBox.minz = std::min(startBox.minz, endBox.minz);

    int hitcount = 0;

    for (int z = minCellZ; z < maxCellZ + 1; z++)
    {
        for (int x = minCellX; x < maxCellX + 1; x++)
        {
            int index = z * COLLISION_SYSTEM_GRID_COUNT + x;
            auto& cell = grid.cells[index];

            for (auto& static_mesh : cell.entries)
            {
                if (!static_mesh->active || static_mesh->hit)
                {
                    continue;
                }

                if (Islander::Collision::CollisionBoxTest(broadphaseBox, static_mesh->parentBox))
                {
                    assert(hitcount < max_hits);
                    if (hitcount < max_hits)
                    {
                        hits[hitcount] = static_mesh;
                        hitcount++;
                        static_mesh->hit = true; // mark as hit so we don't add duplicates
                    }
                }
            }
        }
    }

    return hitcount;
}

float Islander::Collision::SeparateObjects(const IslanderCollisionBox& box, const IslanderCollisionTriangle& tri)
{
    float n[3];
    if (!Islander::Collision::GJK(box, tri, n))
    {
        return 0;
    }

    float triangleNormal[3];
    float ab[3];
    float ac[3];
    SubVec3(tri.vertices[0], tri.vertices[1], ab);
    SubVec3(tri.vertices[0], tri.vertices[2], ac);
    CrossVec3(ab, ac, triangleNormal);
    Normalize(triangleNormal[0], triangleNormal[1], triangleNormal[2], triangleNormal[0], triangleNormal[1], triangleNormal[2]);

    const float steepness = 0.2f; // specify the maximum stepness
    if (Dot(0, 1, 0, triangleNormal[0], triangleNormal[1], triangleNormal[2]) < steepness)
    {
        return 0;
    }

    // Separate objects by pushing the box above the other box.
    return std::max(tri.vertices[0][1], std::max(tri.vertices[1][1], tri.vertices[2][1])) - box.miny + 0.0001f;
}

float Islander::Collision::SeparateObjects(const IslanderCollisionBox& box, const IslanderCollisionOBB& obb)
{
    float n[3];
    if (!Islander::Collision::GJK(box, obb, n))
    {
        return 0;
    }

    // Separate objects by pushing the box above the other box.
    float maxY = -std::numeric_limits<float>().max();
    for (int i = 0; i < 8; i++)
    {
        float y = obb.verticies[i * 3 + 1];
        maxY = std::max(y, maxY);
    }

    return maxY - box.miny + 0.0001f;
}

//float SeparateObjects(const IslanderCollisionBox& box, const IslanderCollisionBox& staticBox)
//{
//    if (!CollisionBoxTest(box, staticBox))
//    {
//        return 0;
//    }
//
//    // Separate objects by pushing the box above the other box.
//    return staticBox.maxy - box.miny + 0.0001f;
//}

//float SeparateObjects(const IslanderCollisionSphere& sphere, const IslanderCollisionBox& staticBox)
//{
//    //if (!CollisionBoxTest(box, staticBox))
//    //{
//    //    return 0;
//    //}
//
//    float n[3];
//    if (!Islander::Collision::GJK(sphere, staticBox, n))
//    {
//        return 0;
//    }
//
//    // Separate objects by pushing the sphere above the other box.
//    return staticBox.maxy - sphere.centreY - sphere.radius + 0.0001f;
//}
