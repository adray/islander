#include "CollisionSystem.h"
#include "AnimationController.h"
#include "Matrix.h"
#include "Component.h"
#include "Polygon.h"
#include "CollisionEvent.h"
#include "CollisionPrimitives.h"
#include <algorithm>
#include <cfloat>
#include <cstring>
#include <unordered_set>
#include <limits>

#define _USE_MATH_DEFINES
#include <math.h>

using namespace Islander::Numerics;

Islander::Collision::CollisionSystem* Islander::Collision::CreateCollisionSystem()
{
    auto system = new Islander::Collision::CollisionSystem();
    system->grid.dirty = false;
    return system;
}

void AddDynamicCollider(Islander::Collision::CollisionDynamicMesh* mesh, Islander::Collision::CollisionDynamicMesh* add)
{
    for (int i = 0; i < mesh->colData.dynamic_mesh_count; i++)
    {
        if (mesh->colData.dynamic_mesh[i] == add)
        {
            return;
        }
    }

    if (mesh->colData.dynamic_mesh_count < COLLISION_SYSTEM_DATA_ITEMS)
    {
        mesh->colData.dynamic_mesh[mesh->colData.dynamic_mesh_count++] = add;
    }
}

//void AddStaticCollider(Islander::Collision::CollisionDynamicMesh* mesh, Islander::Collision::CollisionStaticMesh* add)
//{
//    for (int i = 0; i < mesh->collider_static_count; i++)
//    {
//        if (mesh->collider_static[i] == add)
//        {
//            return;
//        }
//    }
//
//    if (mesh->collider_static_count < COLLISION_SYSTEM_DATA_ITEMS)
//    {
//        mesh->collider_static[mesh->collider_static_count++] = add;
//    }
//}

#if 0
// Modified from https://www.gamedev.net/articles/programming/general-and-gameplay-programming/swept-aabb-collision-detection-and-response-r3084/

float SweptCollisionBoxTest(const IslanderCollisionBox& startBox, const IslanderCollisionBox& endBox, const IslanderCollisionBox& staticBox, float& normalx, float& normaly, float& normalz)
{
    // Perform broadphase test.
    IslanderCollisionBox broadphaseBox;
    broadphaseBox.maxx = std::max(startBox.maxx, endBox.maxx);
    broadphaseBox.maxy = std::max(startBox.maxy, endBox.maxy);
    broadphaseBox.maxz = std::max(startBox.maxz, endBox.maxz);
    broadphaseBox.minx = std::min(startBox.minx, endBox.minx);
    broadphaseBox.miny = std::min(startBox.miny, endBox.miny);
    broadphaseBox.minz = std::min(startBox.minz, endBox.minz);

    if (!Islander::Collision::CollisionBoxTest(broadphaseBox, staticBox))
    {
        normalx = 0;
        normaly = 0;
        normalz = 0;
        return 1;
    }

    float xInvEntry, yInvEntry, zInvEntry;
    float xInvExit, yInvExit, zInvExit;

    float vx = endBox.maxx - startBox.maxx;
    float vy = endBox.maxy - startBox.maxy;
    float vz = endBox.maxz - startBox.maxz;

    // find the distance between the objects on the near and far sides for both x and y 
    if (vx > 0)
    {
        xInvEntry = staticBox.minx - startBox.maxx;
        xInvExit = staticBox.maxx - startBox.minx;
    }
    else
    {
        xInvEntry = staticBox.maxx - startBox.minx;
        xInvExit = staticBox.minx - startBox.maxx;
    }

    if (vy > 0)
    {
        yInvEntry = staticBox.miny - startBox.maxy;
        yInvExit = startBox.maxy - startBox.miny;
    }
    else
    {
        yInvEntry = staticBox.maxy - startBox.miny;
        yInvExit = staticBox.miny - startBox.maxy;
    }

    if (vz > 0)
    {
        zInvEntry = staticBox.minz - startBox.maxz;
        zInvExit = staticBox.maxz - startBox.minz;
    }
    else
    {
        zInvEntry = staticBox.maxz - startBox.minz;
        zInvExit = staticBox.minz - startBox.maxz;
    }

    // find time of collision and time of leaving for each axis (if statement is to prevent divide by zero) 
    float xEntry, yEntry, zEntry;
    float xExit, yExit, zExit;

    if (vx == 0.0f)
    {
        xEntry = -std::numeric_limits<float>::infinity();
        xExit = std::numeric_limits<float>::infinity();
    }
    else
    {
        xEntry = xInvEntry / vx;
        xExit = xInvExit / vx;
    }

    if (vy == 0.0f)
    {
        yEntry = -std::numeric_limits<float>::infinity();
        yExit = std::numeric_limits<float>::infinity();
    }
    else
    {
        yEntry = yInvEntry / vy;
        yExit = yInvExit / vy;
    }

    if (vz == 0.0f)
    {
        zEntry = -std::numeric_limits<float>::infinity();
        zExit = std::numeric_limits<float>::infinity();
    }
    else
    {
        zEntry = zInvEntry / vz;
        zExit = zInvExit / vz;
    }

    if (xEntry > 1.0f) xEntry = -std::numeric_limits<float>::infinity();
    if (yEntry > 1.0f) yEntry = -std::numeric_limits<float>::infinity();
    if (zEntry > 1.0f) zEntry = -std::numeric_limits<float>::infinity();

    // find the earliest/latest times of collision
    float entryTime = std::max(std::max(xEntry, yEntry), zEntry);
    float exitTime = std::min(std::min(xExit, yExit), zExit);

    // If we already colliding then the normal is the direction to separate the objects.
    if (Islander::Collision::CollisionBoxTest(startBox, staticBox))
    {
        float vx = endBox.maxx - startBox.maxx;
        float vy = endBox.maxy - startBox.maxy;
        float vz = endBox.maxz - startBox.maxz;

        //float distX =

        //normalx = (endBox.maxx - endBox.minx) / 2 + endBox.minx - ((startBox.maxx - startBox.minx) / 2 + startBox.minx);
        //normaly = (endBox.maxy - endBox.miny) / 2 + endBox.miny - ((startBox.maxy - startBox.miny) / 2 + startBox.miny);
        //normalz = (endBox.maxz - endBox.minz) / 2 + endBox.minz - ((startBox.maxz - startBox.minz) / 2 + startBox.minz);
        //Islander::Normalize(-normalx, -normaly, -normalz, normalx, normaly, normalz);

        Normalize(-vx, -vy, -vz, normalx, normaly, normalz);

        //return entryTime;
        return 0;
    }

    normalx = 0.0f;
    normaly = 0.0f;
    normalz = 0.0f;
    // if there was no collision
    if (entryTime > exitTime)
    {
        return 1.0f;
    }
    else if (xEntry < 0.0f && yEntry < 0.0f && zEntry < 0.0f)
    {
        return 1.0f;
    }
    else if (xEntry < 0.0f && yEntry < 0.0f)
    {
        if ((startBox.maxx < staticBox.minx || startBox.minx > staticBox.maxx) && (startBox.maxy < staticBox.miny || startBox.miny > staticBox.maxy))
        {
            return 1.0f;
        }
    }
    else if (xEntry < 0.0f && zEntry < 0.0f)
    {
        if ((startBox.maxx < staticBox.minx || startBox.minx > staticBox.maxx) && (startBox.maxz < staticBox.minz || startBox.minz > staticBox.maxz))
        {
            return 1.0f;
        }
    }
    else if (yEntry < 0.0f && zEntry < 0.0f)
    {
        if ((startBox.maxy < staticBox.miny || startBox.miny > staticBox.maxy) && (startBox.maxz < staticBox.minz || startBox.minz > staticBox.maxz))
        {
            return 1.0f;
        }
    }
    else if (xEntry < 0.0f)
    {
        if (startBox.maxx < staticBox.minx || startBox.minx > staticBox.maxx)
        {
            return 1.0f;
        }
    }
    else if (yEntry < 0.0f)
    {
        if (startBox.maxy < staticBox.miny || startBox.miny > staticBox.maxy)
        {
            return 1.0f;
        }
    }
    else if (zEntry < 0.0f)
    {
        if (startBox.maxz < staticBox.minz || startBox.minz > staticBox.maxz)
        {
            return 1.0f;
        }
    }
     // if there was a collision 
    {
        // calculate normal of collided surface
        if (xEntry > yEntry && xEntry > zEntry)
        {
            if (xInvEntry < 0.0f)
            {
                normalx = 1.0f;
                normaly = 0.0f;
                normalz = 0.0f;
            }
            else
            {
                normalx = -1.0f;
                normaly = 0.0f;
                normalz = 0.0f;
            }
        }
        else if (zEntry > yEntry && zEntry > xEntry)
        {
            if (zInvEntry < 0.0f)
            {
                normalx = 0.0f;
                normaly = 0.0f;
                normalz = 1.0f;
            }
            else
            {
                normalx = 0.0f;
                normaly = 0.0f;
                normalz = -1.0f;
            }
        }
        else
        {
            if (yInvEntry < 0.0f)
            {
                normalx = 0.0f;
                normaly = 1.0f;
                normalz = 0.0f;
            }
            else
            {
                normalx = 0.0f;
                normaly = -1.0f;
                normalz = 0.0f;
            }
        }

        return entryTime; // return the time of collision
    }
}
#endif

void AddVertexToOBB(IslanderCollisionOBB& obb, int index, float x, float y, float z)
{
    obb.verticies[index * 3] = x;
    obb.verticies[index * 3 + 1] = y;
    obb.verticies[index * 3 + 2] = z;
}

void CalculateOBBExtents(int* vertices, IslanderCollisionOBB& obb, float* pos)
{
    std::memset(pos, 0, sizeof(float) * 3);

    for (int i = 0; i < 4; i++)
    {
        float x = obb.verticies[vertices[i] * 3];
        float y = obb.verticies[vertices[i] * 3 + 1];
        float z = obb.verticies[vertices[i] * 3 + 2];

        pos[0] += x;
        pos[1] += y;
        pos[2] += z;
    }

    pos[0] /= 4.0f;
    pos[1] /= 4.0f;
    pos[2] /= 4.0f;
}

void CreateStaticMeshCollisionData(Islander::Collision::CollisionStaticMesh* mesh)
{
    // 1. Transform vertices

    int totalNumVerts = 0;
    int totalIndices = 0;
    for (int i = 0; i < mesh->meshData->numMeshes; i++)
    {
        totalNumVerts += mesh->meshData->meshes[i].numVerts;
        totalIndices += mesh->meshData->meshes[i].numIndices;
    }

    mesh->vertices = new float[totalNumVerts * 3];

    Matrix4x4 matrix(Matrix4x4Type::MATRIX_4X4_IDENTITY);
    Matrix4x4::CreateTransformationMatrix(
        mesh->transform.px,
        mesh->transform.py,
        mesh->transform.pz,
        mesh->transform.rx,
        mesh->transform.ry,
        mesh->transform.rz,
        mesh->transform.sx,
        mesh->transform.sy,
        mesh->transform.sz,
        &matrix);

    int vertIndex = 0;
    for (int j = 0; j < mesh->meshData->numMeshes; j++)
    {
        int initialVertIndex = vertIndex;
        int numVerts = mesh->meshData->meshes[j].numVerts * 3;
        for (int i = 0; i < numVerts; i += 3)
        {
            float v[3] =
            {
                mesh->meshData->meshes[j].posData[i],
                mesh->meshData->meshes[j].posData[i + 1],
                mesh->meshData->meshes[j].posData[i + 2]
            };
            Matrix4x4::TransformVector(matrix, &v[0], &v[1], &v[2]);
            mesh->vertices[vertIndex++] = v[0];
            mesh->vertices[vertIndex++] = v[1];
            mesh->vertices[vertIndex++] = v[2];
        }

        auto meshItem = &mesh->meshData->meshes[j];
        for (int i = 0; i < meshItem->numIndices; i += 3)
        {
            IslanderCollisionTriangle tri;
            tri.vertices[0] = &mesh->vertices[initialVertIndex + meshItem->indexData[i]*3];
            tri.vertices[1] = &mesh->vertices[initialVertIndex + meshItem->indexData[i+1]*3];
            tri.vertices[2] = &mesh->vertices[initialVertIndex + meshItem->indexData[i+2]*3];
            mesh->triangles.push_back(tri);
        }
    }

    // 2. Calculate OBB
    //float max[3];
    //float min[3];
    //Islander::CopyVec3(mesh->meshData->max, max);
    //Islander::CopyVec3(mesh->meshData->min, min);
    //Islander::Renderer::Matrix4x4::TransformVector(matrix, &max[0], &max[1], &max[2]);
    //Islander::Renderer::Matrix4x4::TransformVector(matrix, &min[0], &min[1], &min[2]);

    //mesh->obb.half_size[0] = abs(max[0] - min[0]) / 2;
    //mesh->obb.half_size[1] = abs(max[1] - min[1]) / 2;
    //mesh->obb.half_size[2] = abs(max[2] - min[2]) / 2;

    //mesh->obb.centre[0] = mesh->obb.half_size[0] + std::min(min[0], max[0]);
    //mesh->obb.centre[1] = mesh->obb.half_size[1] + std::min(min[1], max[1]);
    //mesh->obb.centre[2] = mesh->obb.half_size[2] + std::min(min[2], max[2]);

    //float half_extents[3] = {
    //    (mesh->meshData->max[0] - mesh->meshData->min[0]) / 2,
    //    (mesh->meshData->max[1] - mesh->meshData->min[1]) / 2,
    //    (mesh->meshData->max[2] - mesh->meshData->min[2]) / 2
    //};
    //float centre[3] = {
    //    mesh->meshData->min[0] + half_extents[0],
    //    mesh->meshData->min[1] + half_extents[1],
    //    mesh->meshData->min[2] + half_extents[2]
    //};
    //
    //Islander::Renderer::Matrix4x4::TransformVector(matrix, &half_extents[0], &half_extents[1], &half_extents[2], 0);
    //Islander::Renderer::Matrix4x4::TransformVector(matrix, &centre[0], &centre[1], &centre[2]);
    //Islander::CopyVec3(half_extents, mesh->obb.half_size);
    //Islander::CopyVec3(centre, mesh->obb.centre);

   
    AddVertexToOBB(mesh->obb, 0, mesh->meshData->min[0], mesh->meshData->min[1], mesh->meshData->min[2]);
    AddVertexToOBB(mesh->obb, 1, mesh->meshData->max[0], mesh->meshData->min[1], mesh->meshData->min[2]);
    AddVertexToOBB(mesh->obb, 2, mesh->meshData->min[0], mesh->meshData->max[1], mesh->meshData->min[2]);
    AddVertexToOBB(mesh->obb, 3, mesh->meshData->min[0], mesh->meshData->min[1], mesh->meshData->max[2]);
    AddVertexToOBB(mesh->obb, 4, mesh->meshData->max[0], mesh->meshData->max[1], mesh->meshData->min[2]);
    AddVertexToOBB(mesh->obb, 5, mesh->meshData->max[0], mesh->meshData->min[1], mesh->meshData->max[2]);
    AddVertexToOBB(mesh->obb, 6, mesh->meshData->min[0], mesh->meshData->max[1], mesh->meshData->max[2]);
    AddVertexToOBB(mesh->obb, 7, mesh->meshData->max[0], mesh->meshData->max[1], mesh->meshData->max[2]);   

    for (int i = 0; i < 24; i += 3)
    {
        Matrix4x4::TransformVector(matrix, &mesh->obb.verticies[i], &mesh->obb.verticies[i + 1], &mesh->obb.verticies[i + 2]);
    }

    // Centre point
    std::memset(mesh->obb.centre, 0, sizeof(mesh->obb.centre));
    for (int i = 0; i < 24; i += 3)
    {
        mesh->obb.centre[0] += mesh->obb.verticies[i];
        mesh->obb.centre[1] += mesh->obb.verticies[i + 1];
        mesh->obb.centre[2] += mesh->obb.verticies[i + 2];
    }
    mesh->obb.centre[0] /= 8.0f;
    mesh->obb.centre[1] /= 8.0f;
    mesh->obb.centre[2] /= 8.0f;

    // Planes
    int side1[4] = { 0, 1, 2, 4 }; // A
    int side2[4] = { 3, 5, 6, 7 }; // A
    int side3[4] = { 0, 2, 3, 6 }; // B
    int side4[4] = { 1, 4, 5, 7 }; // B
    int side5[4] = { 0, 1, 3, 5 }; // C
    int side6[4] = { 2, 4, 6, 7 }; // C

    CalculateOBBExtents(side1, mesh->obb, &mesh->obb.extents[0]);
    CalculateOBBExtents(side3, mesh->obb, &mesh->obb.extents[3]);
    CalculateOBBExtents(side5, mesh->obb, &mesh->obb.extents[6]);

    CalculateOBBExtents(side2, mesh->obb, &mesh->obb.extents[9]);
    CalculateOBBExtents(side4, mesh->obb, &mesh->obb.extents[12]);
    CalculateOBBExtents(side6, mesh->obb, &mesh->obb.extents[15]);

    // 3. Calculate the parent bounding box
    IslanderCollisionBox& parentBox = mesh->parentBox;
    parentBox.minx = parentBox.miny = parentBox.minz = std::numeric_limits<float>().max();
    parentBox.maxx = parentBox.maxy = parentBox.maxz = -std::numeric_limits<float>().max();
    for (int i = 0; i < vertIndex; i += 3)
    {
        auto vert = &mesh->vertices[i];

        parentBox.minx = std::min(vert[0], parentBox.minx);
        parentBox.miny = std::min(vert[1], parentBox.miny);
        parentBox.minz = std::min(vert[2], parentBox.minz);

        parentBox.maxx = std::max(vert[0], parentBox.maxx);
        parentBox.maxy = std::max(vert[1], parentBox.maxy);
        parentBox.maxz = std::max(vert[2], parentBox.maxz);
    }
}

void Islander::Collision::UpdateStaticMesh(Islander::Collision::CollisionSystem* system, Islander::Collision::CollisionStaticMesh* mesh, IslanderCollisionStaticMeshInfo* meshInfo)
{
    delete[] mesh->vertices;
    mesh->vertices = nullptr;

    mesh->meshData = (Islander::Model::PolygonModel*) meshInfo->meshData;
    mesh->rayonly = meshInfo->rayonly == 1;
    mesh->active = true;
    mesh->hit = false;
    mesh->triangleCollider = meshInfo->triangleCollider == 1;
    mesh->transform.px = meshInfo->px;
    mesh->transform.py = meshInfo->py;
    mesh->transform.pz = meshInfo->pz;
    mesh->transform.rx = meshInfo->rx;
    mesh->transform.ry = meshInfo->ry;
    mesh->transform.rz = meshInfo->rz;
    mesh->transform.sx = meshInfo->sx;
    mesh->transform.sy = meshInfo->sy;
    mesh->transform.sz = meshInfo->sz;

    if (!mesh->rayonly)
    {
        if (mesh->meshData != nullptr)
        {
            CreateStaticMeshCollisionData(mesh);
        }
        system->grid.dirty = true;
    }
}

bool AllocateStaticMesh(Islander::Collision::CollisionStaticMesh* array, int* count, std::deque<int>& freeslots, Islander::Collision::CollisionStaticMesh** mesh)
{
    if (freeslots.size() > 0)
    {
        *mesh = &array[freeslots.back()];
        freeslots.pop_back();
    }
    else if (*count < COLLISION_SYSTEM_MAX_STATIC_ITEMS)
    {
        *mesh = &array[*count];
        (*count)++;
    }
    else
    {
        return false;
    }

    return true;
}

bool Islander::Collision::AddStaticMesh(Islander::Collision::CollisionSystem* system, bool rayonly, bool triangleCollider, Islander::Model::PolygonModel* meshData, const CollisionTransform& transform, Islander::Collision::CollisionStaticMesh** mesh)
{
    if (rayonly)
    {
        if (AllocateStaticMesh(system->static_mesh_ray.array, &system->static_mesh_ray.count, system->static_mesh_ray.freeslots, mesh))
        {
            //(*mesh)->childboxcount = 0;
            //(*mesh)->childbox = nullptr;
            (*mesh)->vertices = nullptr;
            (*mesh)->rayonly = rayonly;
            (*mesh)->meshData = meshData;
            (*mesh)->transform = transform;
            (*mesh)->active = true;
            (*mesh)->hit = false;
            (*mesh)->triangleCollider = triangleCollider;
            return true;
        }
    }
    else
    {
        if (AllocateStaticMesh(system->static_mesh.array, &system->static_mesh.count, system->static_mesh.freeslots, mesh))
        {
            (*mesh)->meshData = meshData;
            (*mesh)->rayonly = rayonly;
            (*mesh)->active = true;
            (*mesh)->hit = false;
            (*mesh)->transform = transform;
            (*mesh)->triangleCollider = triangleCollider;

            if (meshData != nullptr)
            {
                CreateStaticMeshCollisionData(*mesh);
                system->grid.dirty = true;
            }
            else
            {
                //(*mesh)->childboxcount = 0;
                //(*mesh)->childbox = nullptr;
                (*mesh)->vertices = nullptr;
            }
            return true;
        }
    }

    return false;
}

bool AllocateDynamicMesh(Islander::Collision::CollisionDynamicMesh* array, int* count, std::deque<int>& freeslots, Islander::Collision::CollisionDynamicMesh** mesh)
{
    if (freeslots.size() > 0)
    {
        *mesh = &array[freeslots.back()];
        freeslots.pop_back();
    }
    else if (*count < COLLISION_SYSTEM_MAX_DYNAMIC_ITEMS)
    {
        *mesh = &array[*count];
        (*count)++;
    }
    else
    {
        return false;
    }

    return true;
}

bool Islander::Collision::AddDynamicMesh(Islander::Collision::CollisionSystem* system, bool rayonly, CollisionDynamicMesh** mesh)
{
    if (rayonly)
    {
        if (AllocateDynamicMesh(system->dynamic_mesh_ray.array, &system->dynamic_mesh_ray.count, system->dynamic_mesh_ray.freeslots, mesh))
        {
            (*mesh)->info.rayonly = rayonly;
            (*mesh)->active = true;
            (*mesh)->hit = false;
            return true;
        }
    }
    else
    {
        if (AllocateDynamicMesh(system->dynamic_mesh.array, &system->dynamic_mesh.count, system->dynamic_mesh.freeslots, mesh))
        {
            (*mesh)->info.rayonly = rayonly;
            (*mesh)->active = true;
            (*mesh)->hit = false;
            return true;
        }
    }

    return false;
}

void Islander::Collision::RemoveStaticMesh(Islander::Collision::CollisionSystem* system, Islander::Collision::CollisionStaticMesh* mesh)
{
    if (mesh->rayonly)
    {
        int idx = (mesh - &system->static_mesh_ray.array[0]);
        if (idx >= 0 && idx < system->static_mesh_ray.count && system->static_mesh_ray.array[idx].active)
        {
            //delete[] system->static_mesh_ray.array[idx].childbox;
            //system->static_mesh_ray.array[idx].childbox = nullptr;
            delete[] system->static_mesh_ray.array[idx].vertices;
            system->static_mesh_ray.array[idx].vertices = nullptr;
            system->static_mesh_ray.array[idx].active = false;
            system->static_mesh_ray.array[idx].triangles.clear();
            system->static_mesh_ray.freeslots.push_back(idx);
        }
    }
    else
    {
        int idx = (mesh - &system->static_mesh.array[0]);
        if (idx >= 0 && idx < system->static_mesh.count && system->static_mesh.array[idx].active)
        {
            //delete[] system->static_mesh.array[idx].childbox;
            //system->static_mesh.array[idx].childbox = nullptr;
            delete[] system->static_mesh.array[idx].vertices;
            system->static_mesh.array[idx].vertices = nullptr;
            system->static_mesh.array[idx].active = false;
            system->static_mesh.array[idx].triangles.clear();
            system->static_mesh.freeslots.push_back(idx);
            system->grid.dirty = true;
        }
    }
}

void Islander::Collision::RemoveDynamicMesh(Islander::Collision::CollisionSystem* system, Islander::Collision::CollisionDynamicMesh* mesh)
{
    if (mesh->info.rayonly)
    {
        int idx = (mesh - &system->dynamic_mesh_ray.array[0]);
        if (idx >= 0 && idx < system->dynamic_mesh_ray.count && system->dynamic_mesh_ray.array[idx].active)
        {
            system->dynamic_mesh_ray.array[idx].active = false;
            system->dynamic_mesh_ray.freeslots.push_back(idx);
        }
    }
    else
    {
        int idx = (mesh - &system->dynamic_mesh.array[0]);
        if (idx >= 0 && idx < system->dynamic_mesh.count && system->dynamic_mesh.array[idx].active)
        {
            system->dynamic_mesh.array[idx].active = false;
            system->dynamic_mesh.freeslots.push_back(idx);
        }
    }
}

void ClearColliders(Islander::Collision::CollisionDynamicMesh* mesh)
{
    /*for (int i = 0; i < COLLISION_SYSTEM_DATA_ITEMS; i++)
    {
        mesh->collider_static[i] = nullptr;
        mesh->collider_dynamic[i] = nullptr;
    }

    mesh->collider_dynamic_count = 0;
    mesh->collider_static_count = 0;*/

    memset(&mesh->colData, 0, sizeof(mesh->colData));
}

void BuildDynamicGrid(Islander::Collision::CollisionSystem* system)
{
    system->dynGrid.box = system->grid.box;

    Islander::Collision::ClearDynamicCollisionGrid(system->dynGrid);

    for (int i = 0; i < system->dynamic_mesh.count; i++)
    {
        if (system->dynamic_mesh.array[i].active)
        {
            Islander::Collision::AddItemDynamicGrid(system->dynGrid, &system->dynamic_mesh.array[i]);
        }
    }
}

void RunCollisionIterations(Islander::Collision::CollisionSystem* system, Islander::Collision::CollisionDynamicMesh* mesh, float delta)
{
    constexpr int MAX_ITERATIONS = ISLANDER_COLLISION_ITERATION_DATA_COUNT;

    ClearColliders(mesh);
    bool clearPusher = abs(mesh->info.vx) > 0 || abs(mesh->info.vz) > 0;

    bool completed = false;
    while (!completed && mesh->colData.iterations < MAX_ITERATIONS)
    {
        float x = mesh->info.px + mesh->info.vx;
        float y = mesh->info.py + mesh->info.vy;
        float z = mesh->info.pz + mesh->info.vz;

        float rx = mesh->info.rx + mesh->info.rdx;
        float ry = mesh->info.ry + mesh->info.rdy;
        float rz = mesh->info.rz + mesh->info.rdz;

        float velocity[3] = { mesh->info.vx, mesh->info.vy, mesh->info.vz };

        bool clearPlatform = abs(mesh->info.vy) > 0;

        float collisionTime;
        float normal[3];
        bool collide = false;

        IslanderCollisionBox boxStart = mesh->info.box;
        IslanderCollisionSphere sphereStart = mesh->info.sphere;
        IslanderCollisionCapsule capsuleStart = mesh->info.capsule;
        IslanderCollisionOBB obbStart = mesh->info.obb;

        if (mesh->info.collisionShape == ISLANDER_COLLISION_SHAPE_BOX)
        {
            Islander::Collision::CalculateBox(boxStart, mesh);

            if ((mesh->info.trigger & ISLANDER_COLLISION_IGNORE_STATIC) != ISLANDER_COLLISION_IGNORE_STATIC)
            {
                Islander::Collision::CollisionEventList evlist;
                QueryGridCells(system->grid, boxStart, velocity, mesh, evlist);

                if ((mesh->info.trigger & ISLANDER_COLLISION_IS_GHOST) != ISLANDER_COLLISION_IS_GHOST)
                {
                    if (mesh->colData.static_mesh_count > 0 && abs(mesh->info.vx) + abs(mesh->info.vz) > 0)
                    {
                        // Repeat with step up

                        QueryGridCellsStepUp(system->grid, boxStart, velocity, mesh, mesh->info.stepUpY, evlist);
                    }

                    float collisionY;
                    bool canstepup;
                    Islander::Collision::ResolveCollisions(evlist, collisionY, collisionTime, canstepup, collide, normal);
                    if (canstepup)
                    {
                        y += collisionY;
                        mesh->colData.static_mesh_count = 0;
                    }
                }
            }
        }
        else if (mesh->info.collisionShape == ISLANDER_COLLISION_SHAPE_SPHERE)
        {
            CalculateSphere(sphereStart, mesh);

            if ((mesh->info.trigger & ISLANDER_COLLISION_IGNORE_STATIC) != ISLANDER_COLLISION_IGNORE_STATIC)
            {
                Islander::Collision::CollisionEventList evlist;
                QueryGridCells(system->grid, sphereStart, velocity, mesh, evlist);
                
                if ((mesh->info.trigger & ISLANDER_COLLISION_IS_GHOST) != ISLANDER_COLLISION_IS_GHOST)
                {
                    if (mesh->colData.static_mesh_count > 0 && abs(mesh->info.vx) + abs(mesh->info.vz) > 0)
                    {
                        // Repeat with step up

                        QueryGridCellsStepUp(system->grid, sphereStart, velocity, mesh, mesh->info.stepUpY, evlist);
                    }

                    float collisionY;
                    bool canstepup;
                    Islander::Collision::ResolveCollisions(evlist, collisionY, collisionTime, canstepup, collide, normal);
                    if (canstepup)
                    {
                        y += collisionY;
                        mesh->colData.static_mesh_count = 0;
                    }
                }
            }
        }
        else if (mesh->info.collisionShape == ISLANDER_COLLISION_SHAPE_CAPSULE)
        {
            CalculateCapsule(capsuleStart, mesh);

            if ((mesh->info.trigger & ISLANDER_COLLISION_IGNORE_STATIC) != ISLANDER_COLLISION_IGNORE_STATIC)
            {
                Islander::Collision::CollisionEventList evlist;
                QueryGridCells(system->grid, capsuleStart, velocity, mesh, evlist);

                if ((mesh->info.trigger & ISLANDER_COLLISION_IS_GHOST) != ISLANDER_COLLISION_IS_GHOST)
                {
                    if (mesh->colData.static_mesh_count > 0 && abs(mesh->info.vx) + abs(mesh->info.vz) > 0)
                    {
                        // Repeat with step up

                        QueryGridCellsStepUp(system->grid, capsuleStart, velocity, mesh, mesh->info.stepUpY, evlist);
                    }

                    float collisionY;
                    bool canstepup;
                    Islander::Collision::ResolveCollisions(evlist, collisionY, collisionTime, canstepup, collide, normal);
                    if (canstepup)
                    {
                        Islander::Collision::CollisionEventList evlist2;
                        IslanderCollisionCapsule capsule = capsuleStart;
                        capsule.centreY += collisionY;
                        float vel[3] = { mesh->info.vx, 0, mesh->info.vz };
                        QueryGridCells(system->grid, capsule, vel, mesh, evlist2);
                        if (evlist2.collisions.size() == 0) // check if we collide upon step up
                        {
                            y += collisionY;
                            mesh->colData.static_mesh_count = 0;
                        }
                        else
                        {
                            // Re-resolve collisions without stepping up.
                            evlist.stepUps.clear();
                            ResolveCollisions(evlist, collisionY, collisionTime, canstepup, collide, normal);
                        }
                    }
                }
            }
        }
        else if (mesh->info.collisionShape == ISLANDER_COLLISION_SHAPE_ORIENTATED_BOX)
        {
            CalculateOBB(obbStart, mesh);

            if ((mesh->info.trigger & ISLANDER_COLLISION_IGNORE_STATIC) != ISLANDER_COLLISION_IGNORE_STATIC)
            {
                Islander::Collision::CollisionEventList evlist;
                QueryGridCells(system->grid, obbStart, velocity, mesh, evlist);

                if ((mesh->info.trigger & ISLANDER_COLLISION_IS_GHOST) != ISLANDER_COLLISION_IS_GHOST)
                {
                    if (mesh->colData.static_mesh_count > 0 && abs(mesh->info.vx) + abs(mesh->info.vz) > 0)
                    {
                        // Repeat with step up

                        QueryGridCellsStepUp(system->grid, obbStart, velocity, mesh, mesh->info.stepUpY, evlist);
                    }

                    float collisionY;
                    bool canstepup;
                    Islander::Collision::ResolveCollisions(evlist, collisionY, collisionTime, canstepup, collide, normal);
                    if (canstepup)
                    {
                        Islander::Collision::CollisionEventList evlist2;
                        IslanderCollisionOBB obb = obbStart;

                        for (int i = 1; i < 24; i += 3)
                        {
                            obb.verticies[i] += collisionY;
                        }

                        float vel[3] = { mesh->info.vx, 0, mesh->info.vz };
                        QueryGridCells(system->grid, obb, vel, mesh, evlist2);
                        if (evlist2.collisions.size() == 0) // check if we collide upon step up
                        {
                            y += collisionY;
                            mesh->colData.static_mesh_count = 0;
                        }
                        else
                        {
                            // Re-resolve collisions without stepping up.
                            evlist.stepUps.clear();
                            ResolveCollisions(evlist, collisionY, collisionTime, canstepup, collide, normal);
                        }
                    }
                }
            }
        }

        if ((mesh->info.trigger & ISLANDER_COLLISION_IGNORE_DYNAMIC) != ISLANDER_COLLISION_IGNORE_DYNAMIC)
        {
            Islander::Collision::CollisionEventList evList;
            bool isNotTrigger = (mesh->info.trigger & ISLANDER_COLLISION_IS_TRIGGER) != ISLANDER_COLLISION_IS_TRIGGER;
            //constexpr int collisionTimeEpsilon = 0.00001f;

            constexpr int max_count = 8;
            Islander::Collision::CollisionDynamicMesh* meshes[max_count];
            int count = Islander::Collision::QueryDynamicGrid(system->dynGrid, mesh, max_count, meshes);

            for (int j = 0; j < count; j++)
            {
                auto dynamic_mesh = meshes[j];
                if (dynamic_mesh != mesh)
                {
                    if (!dynamic_mesh->active || dynamic_mesh->info.collisionShape == ISLANDER_COLLISION_SHAPE_NULL)
                    {
                        continue;
                    }

                    if (mesh->platformMesh == dynamic_mesh && abs(mesh->info.vy) == 0)
                    {
                        continue;
                    }

                    if (dynamic_mesh->pushMesh == mesh)
                    {
                        continue;
                    }

                    float n[3];
                    if (mesh->info.collisionShape == ISLANDER_COLLISION_SHAPE_BOX)
                    {
                        if (abs(mesh->info.vx) + abs(mesh->info.vz) + abs(mesh->info.vy) > 0)
                        {
                            float t = Islander::Collision::DispatchCollisionSweptTest(boxStart, dynamic_mesh->info, velocity, n);
                            if (1 - t > 0)
                            {
                                AddDynamicCollider(mesh, dynamic_mesh);

                                if ((dynamic_mesh->info.trigger & ISLANDER_COLLISION_IS_TRIGGER) != ISLANDER_COLLISION_IS_TRIGGER && isNotTrigger)
                                {
                                    /*push = mesh->info.pushType == ISLANDER_COLLISION_IS_PUSHER &&
                                        dynamic_mesh->info.pushType == ISLANDER_COLLISION_IS_PUSHABLE;
                                    collide = true;
                                    collisionTime = t;
                                    normal[0] = n[0];
                                    normal[1] = n[1];
                                    normal[2] = n[2];*/

                                    Islander::Collision::CollisionEvent ev;
                                    ev.collisionTime = t;
                                    ev.dynamic = true;
                                    ev.dynMesh = dynamic_mesh;
                                    ev.normal[0] = n[0];
                                    ev.normal[1] = n[1];
                                    ev.normal[2] = n[2];
                                    CollisionListInsertAtLowerBound(evList, t, ev);
                                }
                            }
                        }
                        else if (Islander::Collision::DispatchCollisionTest(boxStart, dynamic_mesh->info, n))
                        {
                            AddDynamicCollider(mesh, dynamic_mesh);

                            //CollisionEvent ev;
                            //ev.collisionTime = collisionTimeEpsilon;
                            //ev.dynamic = true;
                            //ev.dynMesh = dynamic_mesh;
                            //ev.normal[0] = n[0];
                            //ev.normal[1] = n[1];
                            //ev.normal[2] = n[2];
                            //CollisionListInsertAtLowerBound(evList, collisionTimeEpsilon, ev);
                        }
                    }
                    else if (mesh->info.collisionShape == ISLANDER_COLLISION_SHAPE_SPHERE)
                    {
                        if (abs(mesh->info.vx) + abs(mesh->info.vz) + abs(mesh->info.vy) > 0)
                        {
                            float t = Islander::Collision::DispatchCollisionSweptTest(sphereStart, dynamic_mesh->info, velocity, n);
                            if (1 - t > 0)
                            {
                                AddDynamicCollider(mesh, dynamic_mesh);

                                if ((dynamic_mesh->info.trigger & ISLANDER_COLLISION_IS_TRIGGER) != ISLANDER_COLLISION_IS_TRIGGER && isNotTrigger)
                                {
                                    //push = mesh->info.pushType == ISLANDER_COLLISION_IS_PUSHER &&
                                    //    dynamic_mesh->info.pushType == ISLANDER_COLLISION_IS_PUSHABLE;
                                    //collide = true;
                                    //collisionTime = t;
                                    //normal[0] = n[0];
                                    //normal[1] = n[1];
                                    //normal[2] = n[2];

                                    Islander::Collision::CollisionEvent ev;
                                    ev.collisionTime = t;
                                    ev.dynamic = true;
                                    ev.dynMesh = dynamic_mesh;
                                    ev.normal[0] = n[0];
                                    ev.normal[1] = n[1];
                                    ev.normal[2] = n[2];
                                    CollisionListInsertAtLowerBound(evList, t, ev);
                                }
                            }
                        }
                        else if (Islander::Collision::DispatchCollisionTest(sphereStart, dynamic_mesh->info, n))
                        {
                            AddDynamicCollider(mesh, dynamic_mesh);

                            //CollisionEvent ev;
                            //ev.collisionTime = collisionTimeEpsilon;
                            //ev.dynamic = true;
                            //ev.dynMesh = dynamic_mesh;
                            //ev.normal[0] = n[0];
                            //ev.normal[1] = n[1];
                            //ev.normal[2] = n[2];
                            //CollisionListInsertAtLowerBound(evList, collisionTimeEpsilon, ev);
                        }
                    }
                    else if (mesh->info.collisionShape == ISLANDER_COLLISION_SHAPE_CAPSULE)
                    {
                        if (abs(mesh->info.vx) + abs(mesh->info.vz) + abs(mesh->info.vy) > 0)
                        {
                            float t = Islander::Collision::DispatchCollisionSweptTest(capsuleStart, dynamic_mesh->info, velocity, n);
                            if (1 - t > 0)
                            {
                                AddDynamicCollider(mesh, dynamic_mesh);

                                if ((dynamic_mesh->info.trigger & ISLANDER_COLLISION_IS_TRIGGER) != ISLANDER_COLLISION_IS_TRIGGER && isNotTrigger)
                                {
                                    //push = mesh->info.pushType == ISLANDER_COLLISION_IS_PUSHER &&
                                    //    dynamic_mesh->info.pushType == ISLANDER_COLLISION_IS_PUSHABLE;
                                    //collide = true;
                                    //collisionTime = t;
                                    //normal[0] = n[0];
                                    //normal[1] = n[1];
                                    //normal[2] = n[2];

                                    Islander::Collision::CollisionEvent ev;
                                    ev.collisionTime = t;
                                    ev.dynamic = true;
                                    ev.dynMesh = dynamic_mesh;
                                    ev.normal[0] = n[0];
                                    ev.normal[1] = n[1];
                                    ev.normal[2] = n[2];
                                    CollisionListInsertAtLowerBound(evList, t, ev);
                                }
                            }
                        }
                        else if (Islander::Collision::DispatchCollisionTest(capsuleStart, dynamic_mesh->info, n))
                        {
                            AddDynamicCollider(mesh, dynamic_mesh);

                            //CollisionEvent ev;
                            //ev.collisionTime = collisionTimeEpsilon;
                            //ev.dynamic = true;
                            //ev.dynMesh = dynamic_mesh;
                            //ev.normal[0] = n[0];
                            //ev.normal[1] = n[1];
                            //ev.normal[2] = n[2];
                            //CollisionListInsertAtLowerBound(evList, collisionTimeEpsilon, ev);
                        }
                    }
                    else if (mesh->info.collisionShape == ISLANDER_COLLISION_SHAPE_ORIENTATED_BOX)
                    {
                        if (abs(mesh->info.vx) + abs(mesh->info.vz) + abs(mesh->info.vy) > 0)
                        {
                            float t = Islander::Collision::DispatchCollisionSweptTest(obbStart, dynamic_mesh->info, velocity, n);
                            if (1 - t > 0)
                            {
                                AddDynamicCollider(mesh, dynamic_mesh);

                                if ((dynamic_mesh->info.trigger & ISLANDER_COLLISION_IS_TRIGGER) != ISLANDER_COLLISION_IS_TRIGGER && isNotTrigger)
                                {
                                    //push = mesh->info.pushType == ISLANDER_COLLISION_IS_PUSHER &&
                                    //    dynamic_mesh->info.pushType == ISLANDER_COLLISION_IS_PUSHABLE;
                                    //collide = true;
                                    //collisionTime = t;
                                    //normal[0] = n[0];
                                    //normal[1] = n[1];
                                    //normal[2] = n[2];

                                    Islander::Collision::CollisionEvent ev;
                                    ev.collisionTime = t;
                                    ev.dynamic = true;
                                    ev.dynMesh = dynamic_mesh;
                                    ev.normal[0] = n[0];
                                    ev.normal[1] = n[1];
                                    ev.normal[2] = n[2];
                                    CollisionListInsertAtLowerBound(evList, t, ev);
                                }
                            }
                        }
                        else if (Islander::Collision::DispatchCollisionTest(obbStart, dynamic_mesh->info, n))
                        {
                            AddDynamicCollider(mesh, dynamic_mesh);

                            //CollisionEvent ev;
                            //ev.collisionTime = collisionTimeEpsilon;
                            //ev.dynamic = true;
                            //ev.dynMesh = dynamic_mesh;
                            //ev.normal[0] = n[0];
                            //ev.normal[1] = n[1];
                            //ev.normal[2] = n[2];
                            //CollisionListInsertAtLowerBound(evList, collisionTimeEpsilon, ev);
                        }
                    }
                }
            }

            // TODO: when velocity is 0 and a collision occurs it will cause an issue
            if (evList.collisions.size() > 0)
            {
                float collisionY; bool stepUp;
                int index = ResolveCollisions(evList, collisionY, collisionTime, stepUp, collide, normal);
                if (index > -1 && evList.collisions[index].dynMesh->info.platformType == ISLANDER_COLLISION_IS_PLATFORM)
                {
                    if (abs(mesh->info.vy) > 0.0f)
                    {
                        if (mesh->info.collisionShape == ISLANDER_COLLISION_SHAPE_CAPSULE &&
                            Islander::Collision::IsAbove(capsuleStart, evList.collisions[index].dynMesh->info))
                        {
                            mesh->platformMesh = evList.collisions[index].dynMesh;
                            clearPlatform = false;
                        }
                    }
                }
                else if (index > -1 && evList.collisions[index].dynMesh->info.pushType == ISLANDER_COLLISION_IS_PUSHABLE)
                {
                    if (abs(mesh->info.vx) + abs(mesh->info.vz) > 0.0f)
                    {
                        mesh->pushMesh = evList.collisions[index].dynMesh;
                        clearPusher = false;
                    }
                }
            }
        }

        if (clearPlatform)
        {
            mesh->platformMesh = nullptr;
        }

        if (!collide)
        {
            mesh->colData.pushing = mesh->pushMesh != nullptr;
            mesh->colData.falling = false;
            mesh->colData.grounded = true;
            if (mesh->info.vy < 0.0f)
            {
                mesh->coyoteTimeElapsed += delta;

                if (mesh->coyoteTimeElapsed < mesh->info.coyoteTime)
                {
                    y = mesh->info.py;
                }
                else
                {
                    mesh->colData.falling = true;
                    mesh->colData.grounded = false;
                }
            }

            mesh->info.px = x;
            mesh->info.py = y;
            mesh->info.pz = z;
            mesh->info.rx = rx;
            mesh->info.ry = ry;
            mesh->info.rz = rz;
            completed = true;

            mesh->colData.it[mesh->colData.iterations].collide = false;
            mesh->colData.it[mesh->colData.iterations].velocity[0] = mesh->info.vx;
            mesh->colData.it[mesh->colData.iterations].velocity[1] = mesh->info.vy;
            mesh->colData.it[mesh->colData.iterations].velocity[2] = mesh->info.vz;
            mesh->colData.it[mesh->colData.iterations].pos[0] = mesh->info.px;
            mesh->colData.it[mesh->colData.iterations].pos[1] = mesh->info.py;
            mesh->colData.it[mesh->colData.iterations].pos[2] = mesh->info.pz;
        }
        else
        {
            if (mesh->info.vy < 0.0f)
            {
                mesh->coyoteTimeElapsed = 0.0f;
            }
            mesh->colData.falling = false;
            mesh->colData.pushing = mesh->pushMesh != nullptr;
            mesh->colData.grounded = true;

            mesh->colData.it[mesh->colData.iterations].collide = true;
            mesh->colData.it[mesh->colData.iterations].time = collisionTime;
            mesh->colData.it[mesh->colData.iterations].normal[0] = normal[0];
            mesh->colData.it[mesh->colData.iterations].normal[1] = normal[1];
            mesh->colData.it[mesh->colData.iterations].normal[2] = normal[2];
            mesh->colData.it[mesh->colData.iterations].velocity[0] = mesh->info.vx;
            mesh->colData.it[mesh->colData.iterations].velocity[1] = mesh->info.vy;
            mesh->colData.it[mesh->colData.iterations].velocity[2] = mesh->info.vz;
            mesh->colData.it[mesh->colData.iterations].pos[0] = mesh->info.px;
            mesh->colData.it[mesh->colData.iterations].pos[1] = mesh->info.py;
            mesh->colData.it[mesh->colData.iterations].pos[2] = mesh->info.pz;

            // Epsilon is needed since floating point isn't deterministic and where one frame a collision occurs the next frame
            // a collision may not occur if they are too close to the object they collided with in the first frame.
            const float epsilon = std::min(collisionTime, 0.01f/*0.5f*/); //0.0001f;

            constexpr float skinWidth = 0.01f;/*0.5f;*/
            float skinCollisionTimeX = mesh->info.vx == 0 ? std::numeric_limits<float>::infinity() : (abs(mesh->info.vx) * collisionTime - skinWidth) / abs(mesh->info.vx);
            float skinCollisionTimeY = mesh->info.vy == 0 ? std::numeric_limits<float>::infinity() : (abs(mesh->info.vy) * collisionTime - skinWidth) / abs(mesh->info.vy);
            float skinCollisionTimeZ = mesh->info.vz == 0 ? std::numeric_limits<float>::infinity() : (abs(mesh->info.vz) * collisionTime - skinWidth) / abs(mesh->info.vz);
            float skinCollisionTime = std::max(0.0f, std::min(skinCollisionTimeX, std::min(skinCollisionTimeY, skinCollisionTimeZ)));

            assert(collisionTime >= 0.0f && collisionTime <= 1.0f);

            if (abs(mesh->info.vy) == 0)
            {
                //mesh->info.px += collisionTime * mesh->info.vx +normal[0] * epsilon;
                //mesh->info.py += collisionTime * mesh->info.vy +normal[1] * epsilon;
                //mesh->info.pz += collisionTime * mesh->info.vz +normal[2] * epsilon;

                //mesh->info.px += collisionTime * mesh->info.vx - mesh->info.vx * epsilon;
                //mesh->info.py += collisionTime * mesh->info.vy - mesh->info.vy * epsilon;
                //mesh->info.pz += collisionTime * mesh->info.vz - mesh->info.vz * epsilon;

                collisionTime = skinCollisionTime;
                mesh->info.px += collisionTime * mesh->info.vx;
                mesh->info.py += collisionTime * mesh->info.vy;
                mesh->info.pz += collisionTime * mesh->info.vz;

                // TODO: we can allow rebounding as well

                if (mesh->pushMesh == nullptr)
                {
                    // slide 
                    float dotprod = (mesh->info.vx * normal[2] + normal[0] * mesh->info.vz) * (1 - collisionTime);
                    float vx = dotprod * normal[2]; float vz = dotprod * normal[0];

                    mesh->info.vx = vx;
                    mesh->info.vy = 0;
                    mesh->info.vz = vz;
                    completed = abs(vx) + abs(vz) < 0.00001f;
                }
                else
                {
                    mesh->info.vx = 0.0f;
                    mesh->info.vy = 0.0f;
                    mesh->info.vz = 0.0f;
                    completed = true;
                }

                //float v = Islander::LengthSqr(mesh->info.vx, mesh->info.vy, mesh->info.vz);
                //float invNormal[3] =
                //{
                //    -normal[0] * v,
                //    -normal[1] * v,
                //    -normal[2] * v
                //};
                //float dir[3] =
                //{
                //    mesh->info.vx - invNormal[0],
                //    mesh->info.vy - invNormal[1],
                //    mesh->info.vz - invNormal[2]
                //};

                //mesh->info.vx = dir[0];
                //mesh->info.vy = dir[1];
                //mesh->info.vz = dir[2];

                //completed = abs(mesh->info.vx) + abs(mesh->info.vy) + abs(mesh->info.vz) < 0.00001f;
            }
            else
            {
                //mesh->info.px += collisionTime * mesh->info.vx + normal[0] * epsilon;
                //mesh->info.py += collisionTime * mesh->info.vy + normal[1] * epsilon;
                //mesh->info.pz += collisionTime * mesh->info.vz + normal[2] * epsilon;

                //mesh->info.px += collisionTime * mesh->info.vx - mesh->info.vx * epsilon;
                //mesh->info.py += collisionTime * mesh->info.vy - mesh->info.vy * epsilon;
                //mesh->info.pz += collisionTime * mesh->info.vz - mesh->info.vz * epsilon;

                collisionTime = skinCollisionTime;
                mesh->info.px += collisionTime * mesh->info.vx;
                mesh->info.py += collisionTime * mesh->info.vy;
                mesh->info.pz += collisionTime * mesh->info.vz;

                completed = true;
            }

            mesh->info.rx = rx;
            mesh->info.ry = ry;
            mesh->info.rz = rz;

            mesh->info.rdx = 0;
            mesh->info.rdy = 0;
            mesh->info.rdz = 0;
        }

        mesh->colData.iterations++;
    }

    if (clearPusher)
    {
        mesh->pushMesh = nullptr;
    }
}

void Islander::Collision::RunCollision(Islander::Collision::CollisionSystem* system, float delta)
{
    BuildCollisionGrid(system->grid, system->static_mesh.count, system->static_mesh.array);
    BuildDynamicGrid(system);

    // platforms algo
    // 1. Move platforms, don't perform any collision detection
    // 2. Record the distance they travelled (its equal to velocity)
    // 3. Adjust any objects which are on the platform by the same value

    // Pushing algo
    // 1. Move pushable by desired amount (of pusher) with collision detection
    // 2. Record the distance travelled
    // 3. Adjust pusher to have the same velocity

    // Move platforms (no collision detection)
    for (int i = 0; i < system->dynamic_mesh.count; i++)
    {
        auto mesh = &system->dynamic_mesh.array[i];
        if (mesh->active && mesh->info.platformType == 1)
        {
            mesh->info.px += mesh->info.vx;
            mesh->info.py += mesh->info.vy;
            mesh->info.pz += mesh->info.vz;
        }
    }

    // Calculate pushable velocity
    for (int i = 0; i < system->dynamic_mesh.count; i++)
    {
        auto mesh = &system->dynamic_mesh.array[i];
        if (mesh->active && mesh->info.pushType == ISLANDER_COLLISION_IS_PUSHER &&
            mesh->pushMesh != nullptr)
        {
            auto push = reinterpret_cast<Islander::Collision::CollisionDynamicMesh*>(mesh->pushMesh);
            push->info.vx += mesh->info.vx;
            push->info.vy += mesh->info.vy;
            push->info.vz += mesh->info.vz;
        }
    }

    // Move pushables
    for (int i = 0; i < system->dynamic_mesh.count; i++)
    {
        auto mesh = &system->dynamic_mesh.array[i];
        if (mesh->active && mesh->info.pushType == ISLANDER_COLLISION_IS_PUSHABLE)
        {
            RunCollisionIterations(system, mesh, delta);
        }
    }

    // Move regular objects
    for (int i = 0; i < system->dynamic_mesh.count; i++)
    {
        auto mesh = &system->dynamic_mesh.array[i];
        if (mesh->active && mesh->info.platformType == 0 && mesh->info.pushType != ISLANDER_COLLISION_IS_PUSHABLE)
        {
            if (mesh->platformMesh != nullptr)
            {
                auto platform = reinterpret_cast<Islander::Collision::CollisionDynamicMesh*>(mesh->platformMesh);

                mesh->info.px += platform->info.vx;
                mesh->info.py += platform->info.vy;
                mesh->info.pz += platform->info.vz;

                //IslanderCollisionCapsule capsule = mesh->info.capsule;
                //CalculateCapsule(capsule, mesh);
                //IslanderCollisionBox box;
                //GetBoundingBox(capsule, box);

                //IslanderCollisionBox box2 = platform->info.box;
                //CalculateBox(box2, platform);
                //mesh->info.py += std::max(0.0f, box2.maxy - box.miny) + 0.001f; //SeparateObjects(box, platform->info.box);
            }

            RunCollisionIterations(system, mesh, delta);
        }
    }
}

void Islander::Collision::CreateDebugGeometry(CollisionSystem* system, CollisionDynamicMesh* mesh, IslanderCollisionGeometryData* gData)
{
    if (mesh->info.collisionShape == ISLANDER_COLLISION_SHAPE_CAPSULE)
    {
        IslanderCollisionCapsule capsule = mesh->info.capsule;
        CalculateCapsule(capsule, mesh);

        float top[3] = { 0, capsule.height / 2, 0 };
        float bottom[3] = { 0, -capsule.height / 2, 0 };

        const int slices = 12;
        const int stacks = 12;

        //int middle_vertex_count = 2 * segments;
        //int middle_index_count = 3 * segments;
        //int hemisphere_vertex_count = 2 * (segments - 1);
        //int hemisphere_index_count = 3 * (segments - 1);

        //gData->strideSize = 9 * sizeof(float);
        //gData->vertexCount = middle_vertex_count + hemisphere_vertex_count;
        //gData->vertexData = new float[gData->vertexCount * 9];
        //gData->indexCount = middle_index_count + hemisphere_index_count;
        //gData->indexData = new int[gData->indexCount];

        int sphere_vertex_count = (stacks - 1) * slices + 2;
        int sphere_index_count = slices * 6 + (stacks - 2) * slices * 6;

        gData->strideSize = 9 * sizeof(float);
        gData->vertexCount = sphere_vertex_count;
        gData->vertexData = new float[gData->vertexCount * 9];
        gData->indexCount = sphere_index_count;
        gData->indexData = new int[gData->indexCount];

        memset(gData->vertexData, 0, (unsigned long long)gData->vertexCount * 9 * sizeof(float));

        int vertexOffset = 0;

        gData->vertexData[0] = capsule.centreX;
        gData->vertexData[1] = capsule.centreY + capsule.radius + capsule.height / 2; // top vertex
        gData->vertexData[2] = capsule.centreZ;
        vertexOffset += 9;

        for (int j = 0; j < stacks - 1; j++)
        {
            float theta = M_PI * (j + 1) / stacks;

            for (int i = 0; i < slices; i++)
            {
                float phi = M_PI * 2 * i / slices;
                float dirX = sinf(theta) * cosf(phi);
                float dirY = cosf(theta);
                float dirZ = sinf(theta) * sinf(phi);

                float y = capsule.centreY;
                if (dirY > 0)
                {
                    y += capsule.height / 2;
                }
                else
                {
                    y -= capsule.height / 2;
                }

                gData->vertexData[vertexOffset] = capsule.centreX + dirX * capsule.radius;
                gData->vertexData[vertexOffset + 1] = y + dirY * capsule.radius;
                gData->vertexData[vertexOffset + 2] = capsule.centreZ + dirZ * capsule.radius;
                vertexOffset += 9;
            }
        }

        gData->vertexData[vertexOffset] = capsule.centreX;
        gData->vertexData[vertexOffset + 1] = capsule.centreY - capsule.radius - capsule.height / 2; // bottom
        gData->vertexData[vertexOffset + 2] = capsule.centreZ;

        int indexOffset = 0;

        // top/bottom tris
        for (int i = 0; i < slices; i++)
        {
            int i0 = i + 1;
            int i1 = (i + 1) % slices + 1;
            gData->indexData[indexOffset] = 0;
            gData->indexData[indexOffset + 1] = i1;
            gData->indexData[indexOffset + 2] = i0;

            int i2 = i + slices * (stacks - 2) + 1;
            int i3 = (i + 1) % slices + slices * (stacks - 2) + 1;
            gData->indexData[indexOffset + 3] = sphere_vertex_count - 1;
            gData->indexData[indexOffset + 4] = i2;
            gData->indexData[indexOffset + 5] = i3;
            indexOffset += 6;
        }

        // Add tris per stacks/slices
        for (int j = 0; j < stacks - 2; j++)
        {
            int j0 = j * slices + 1;
            int j1 = (j + 1) * slices + 1;

            for (int i = 0; i < slices; i++)
            {
                int i0 = j0 + i;
                int i1 = j0 + (i + 1) % slices;
                int i2 = j1 + (i + 1) % slices;
                int i3 = j1 + i;

                gData->indexData[indexOffset] = i0;
                gData->indexData[indexOffset + 1] = i1;
                gData->indexData[indexOffset + 2] = i2;
                gData->indexData[indexOffset + 3] = i1;
                gData->indexData[indexOffset + 4] = i2;
                gData->indexData[indexOffset + 5] = i3;

                indexOffset += 6;
            }
        }

        //for (int i = 0; i < segments; i++)
        //{
        //    float angle = M_PI * 2 * i / segments;
        //    float dirX = sinf(angle);
        //    float dirZ = cosf(angle);

        //    float data[18] =
        //    {
        //        bottom[0] + dirX * capsule.radius, bottom[1], bottom[2] + dirZ * capsule.radius,
        //        0, 0, 0,
        //        0, 0, 0,
        //        top[0] + dirX * capsule.radius, top[1], top[2] + dirZ * capsule.radius,
        //        0, 0, 0,
        //        0, 0, 0
        //    };

        //    memcpy(&gData->vertexData[i * 18], data, sizeof(data));

        //    gData->indexData[i * 3] = i * 2;
        //    gData->indexData[i * 3 + 1] = i * 2;
        //    gData->indexData[i * 3 + 2] = i * 2 + 1;
        //}

        //float points[segments * 3];
        //for (int i = 0; i < segments; i++)
        //{
        //    float angle = M_PI * 2 * i / segments;
        //    float dirX = sinf(angle);
        //    float dirZ = cosf(angle);

        //    points[i * 3] = bottom[0] + dirX * capsule.radius;
        //    points[i * 3 + 1] = bottom[1];// +dirX * capsule.radius;
        //    points[i * 3 + 2] = bottom[2] + dirZ * capsule.radius;
        //}

        //for (int i = 0; i < segments - 1; i++)
        //{
        //    float data[18] =
        //    {
        //        points[i * 3], points[i * 3 + 1], points[i * 3 + 2],
        //        0, 0, 0,
        //        0, 0, 0,
        //        points[i * 3 + 3], points[i * 3 + 4], points[i * 3 + 5],
        //        0, 0, 0,
        //        0, 0, 0
        //    };

        //    memcpy(&gData->vertexData[i * 18 + middle_vertex_count * 9], data, sizeof(data));

        //    gData->indexData[i * 3 + middle_index_count] = i * 2 + middle_vertex_count;
        //    gData->indexData[i * 3 + 1 + middle_index_count] = i * 2 + middle_vertex_count;
        //    gData->indexData[i * 3 + 2 + middle_index_count] = i * 2 + 1 + middle_vertex_count;
        //}
    }
    else if (mesh->info.collisionShape == ISLANDER_COLLISION_SHAPE_SPHERE)
    {
        auto sphere = mesh->info.sphere;
        CalculateSphere(sphere, mesh);

        const int slices = 12;
        const int stacks = 12;

        int sphere_vertex_count = (stacks-1) * slices + 2;
        int sphere_index_count = slices * 6 + (stacks-2)*slices *6;

        gData->strideSize = 9 * sizeof(float);
        gData->vertexCount = sphere_vertex_count;
        gData->vertexData = new float[gData->vertexCount * 9];
        gData->indexCount = sphere_index_count;
        gData->indexData = new int[gData->indexCount];

        memset(gData->vertexData, 0, (unsigned long long)gData->vertexCount * 9 * sizeof(float));

        // Sphere equations
        // x = p * sin(theta) * cos(phi)
        // y = p * sin(theta) * sin(phi)
        // z = p * cos(phi)
        // where p is radius

        int vertexOffset = 0;

        gData->vertexData[0] = sphere.centreX;
        gData->vertexData[1] = sphere.centreY + sphere.radius; // top vertex
        gData->vertexData[2] = sphere.centreZ;
        vertexOffset += 9;

        for (int j = 0; j < stacks - 1; j++)
        {
            float theta = M_PI * (j + 1) / stacks;

            for (int i = 0; i < slices; i++)
            {
                float phi = M_PI * 2 * i / slices;
                float dirX = sinf(theta) * cosf(phi);
                float dirY = cosf(theta);
                float dirZ = sinf(theta) * sinf(phi);

                gData->vertexData[vertexOffset] = sphere.centreX + dirX * sphere.radius;
                gData->vertexData[vertexOffset + 1] = sphere.centreY + dirY * sphere.radius;
                gData->vertexData[vertexOffset + 2] = sphere.centreZ + dirZ * sphere.radius;
                vertexOffset += 9;
            }
        }

        gData->vertexData[vertexOffset] = sphere.centreX;
        gData->vertexData[vertexOffset + 1] = sphere.centreY -sphere.radius; // bottom
        gData->vertexData[vertexOffset + 2] = sphere.centreZ;
        int indexOffset = 0;

        // top/bottom tris
        for (int i = 0; i < slices; i++)
        {
            int i0 = i + 1;
            int i1 = (i + 1) % slices + 1;
            gData->indexData[indexOffset] = 0;
            gData->indexData[indexOffset + 1] = i1;
            gData->indexData[indexOffset + 2] = i0;

            int i2 = i + slices * (stacks - 2) + 1;
            int i3 = (i + 1) % slices + slices * (stacks - 2) + 1;
            gData->indexData[indexOffset + 3] = sphere_vertex_count - 1;
            gData->indexData[indexOffset + 4] = i2;
            gData->indexData[indexOffset + 5] = i3;
            indexOffset += 6;
        }

        // Add tris per stacks/slices
        for (int j = 0; j < stacks - 2; j++)
        {
            int j0 = j * slices + 1;
            int j1 = (j + 1) * slices + 1;

            for (int i = 0; i < slices; i++)
            {
                int i0 = j0 + i;
                int i1 = j0 + (i + 1) % slices;
                int i2 = j1 + (i + 1) % slices;
                int i3 = j1 + i;

                gData->indexData[indexOffset] = i0;
                gData->indexData[indexOffset + 1] = i1;
                gData->indexData[indexOffset + 2] = i2;
                gData->indexData[indexOffset + 3] = i1;
                gData->indexData[indexOffset + 4] = i2;
                gData->indexData[indexOffset + 5] = i3;

                indexOffset += 6;
            }
        }
    }
    else if (mesh->info.collisionShape == ISLANDER_COLLISION_SHAPE_ORIENTATED_BOX)
    {
        auto obb = mesh->info.obb;
        CalculateOBB(obb, mesh);

        gData->indexCount = 3 * 12;
        gData->vertexCount = 8;
        gData->strideSize = 9 * sizeof(float);
        gData->vertexData = new float[8 * 9];
        gData->indexData = new int32_t[gData->indexCount];

        float data[] =
        {
            obb.verticies[0], obb.verticies[1], obb.verticies[2], 0, 0, 0, 0, 0, 0,
            obb.verticies[3], obb.verticies[4], obb.verticies[5], 0, 0, 0, 0, 0, 0,
            obb.verticies[6], obb.verticies[7], obb.verticies[8], 0, 0, 0, 0, 0, 0,
            obb.verticies[9], obb.verticies[10], obb.verticies[11], 0, 0, 0, 0, 0, 0,
            obb.verticies[12], obb.verticies[13], obb.verticies[14], 0, 0, 0, 0, 0, 0,
            obb.verticies[15], obb.verticies[16], obb.verticies[17], 0, 0, 0, 0, 0, 0,
            obb.verticies[18], obb.verticies[19], obb.verticies[20], 0, 0, 0, 0, 0, 0,
            obb.verticies[21], obb.verticies[22], obb.verticies[23], 0, 0, 0, 0, 0, 0
        };

        memcpy(gData->vertexData, data, sizeof(data));

        int32_t indexData[] =
        {
            7, 3, 2,
            7, 2, 4,
            5, 6, 0,
            5, 0, 1,
            3, 6, 0,
            3, 0, 2,
            7, 5, 1,
            7, 1, 4,
            4, 2, 0,
            4, 2, 1,
            7, 3, 5,
            6, 3, 5
        };

        memcpy(gData->indexData, indexData, sizeof(indexData));
    }
    else if (mesh->info.collisionShape == ISLANDER_COLLISION_SHAPE_BOX)
    {
        auto box = mesh->info.box;
        CalculateBox(box, mesh);

        gData->indexCount = 3 * 12;
        gData->vertexCount = 8;
        gData->strideSize = 9 * sizeof(float);
        gData->vertexData = new float[8 * 9];
        gData->indexData = new int32_t[gData->indexCount];

        float data[] =
        {
            box.maxx, box.maxy, box.maxz, 0, 0, 0, 0, 0, 0,
            box.maxx, box.miny, box.maxz, 0, 0, 0, 0, 0, 0,
            box.maxx, box.maxy, box.minz, 0, 0, 0, 0, 0, 0,
            box.minx, box.maxy, box.minz, 0, 0, 0, 0, 0, 0,
            box.maxx, box.miny, box.minz, 0, 0, 0, 0, 0, 0,
            box.minx, box.miny, box.maxz, 0, 0, 0, 0, 0, 0,
            box.minx, box.maxy, box.maxz, 0, 0, 0, 0, 0, 0,
            box.minx, box.miny, box.minz, 0, 0, 0, 0, 0, 0,
        };

        memcpy(gData->vertexData, data, sizeof(data));

        int32_t indexData[] =
        {
            7, 3, 2,
            7, 2, 4,
            5, 6, 0,
            5, 0, 1,
            3, 6, 0,
            3, 0, 2,
            7, 5, 1,
            7, 1, 4,
            4, 2, 0,
            4, 2, 1,
            7, 3, 5,
            6, 3, 5
        };

        memcpy(gData->indexData, indexData, sizeof(indexData));
    }
}

void Islander::Collision::CreateDebugGeometry(CollisionSystem* system, IslanderCollisionGeometryData* gData)
{
    //std::vector<IslanderCollisionBox> boxes;

    //for (int i = 0; i < system->static_mesh.count; i++)
    //{
    //    auto& element = system->static_mesh.array[i];
    //    if (element.active)
    //    {
    //        for (int j = 0; j < element.childboxcount; j++)
    //        {
    //            auto& childbox = element.childbox[j];
    //            boxes.push_back(childbox);
    //        }
    //    }
    //}

    //gData->indexCount = boxes.size() * 3 * 12;
    //gData->vertexCount = boxes.size() * 8;
    //gData->strideSize = 9 * sizeof(float);
    //gData->vertexData = new float[boxes.size() * 8 * 9];
    //gData->indexData = new int32_t[gData->indexCount];

    //for (int i = 0; i < boxes.size(); i++)
    //{
    //    auto& box = boxes[i];

    //    float data[] =
    //    {
    //        box.maxx, box.maxy, box.maxz, 0, 0, 0, 0, 0, 0,
    //        box.maxx, box.miny, box.maxz, 0, 0, 0, 0, 0, 0,
    //        box.maxx, box.maxy, box.minz, 0, 0, 0, 0, 0, 0,
    //        box.minx, box.maxy, box.minz, 0, 0, 0, 0, 0, 0,
    //        box.maxx, box.miny, box.minz, 0, 0, 0, 0, 0, 0,
    //        box.minx, box.miny, box.maxz, 0, 0, 0, 0, 0, 0,
    //        box.minx, box.maxy, box.maxz, 0, 0, 0, 0, 0, 0,
    //        box.minx, box.miny, box.minz, 0, 0, 0, 0, 0, 0,
    //    };

    //    memcpy(gData->vertexData + i * (sizeof(data) / sizeof(float)), data, sizeof(data));

    //    const int offset = i * 8;
    //    int32_t indexData[] =
    //    {
    //        7 + offset, 3 + offset, 2 + offset,
    //        7 + offset, 2 + offset, 4 + offset,
    //        5 + offset, 6 + offset, 0 + offset,
    //        5 + offset, 0 + offset, 1 + offset,
    //        3 + offset, 6 + offset, 0 + offset,
    //        3 + offset, 0 + offset, 2 + offset,
    //        7 + offset, 5 + offset, 1 + offset,
    //        7 + offset, 1 + offset, 4 + offset,
    //        4 + offset, 2 + offset, 0 + offset,
    //        4 + offset, 2 + offset, 1 + offset,
    //        7 + offset, 3 + offset, 5 + offset,
    //        6 + offset, 3 + offset, 5 + offset
    //    };

    //    memcpy(gData->indexData + i * (sizeof(indexData) / sizeof(int32_t)), indexData, sizeof(indexData));
    //}

    std::vector<IslanderCollisionTriangle> triangles;
    std::vector<IslanderCollisionOBB> boxes;

    for (int i = 0; i < system->static_mesh.count; i++)
    {
        auto& element = system->static_mesh.array[i];
        if (element.active)
        {
            if (element.triangleCollider)
            {
                for (int j = 0; j < element.triangles.size(); j++)
                {
                    auto& childbox = element.triangles[j];
                    triangles.push_back(childbox);
                }
            }
            else
            {
                boxes.push_back(element.obb);
            }
        }
    }

    gData->indexCount = triangles.size() * 3 + boxes.size() * 3 * 12;
    gData->vertexCount = triangles.size() * 3 + boxes.size() * 8;
    gData->strideSize = 9 * sizeof(float);
    gData->vertexData = new float[triangles.size() * 3 * 9 + boxes.size() * 8 * 9];
    gData->indexData = new int32_t[gData->indexCount];

    for (int i = 0; i < triangles.size(); i++)
    {
        auto& tri = triangles[i];

        float data[] =
        {
            tri.vertices[0][0], tri.vertices[0][1], tri.vertices[0][2], 0, 0, 0, 0, 0, 0,
            tri.vertices[1][0], tri.vertices[1][1], tri.vertices[1][2], 0, 0, 0, 0, 0, 0,
            tri.vertices[2][0], tri.vertices[2][1], tri.vertices[2][2], 0, 0, 0, 0, 0, 0,
        };

        memcpy(gData->vertexData + i * (sizeof(data) / sizeof(float)), data, sizeof(data));

        const int offset = i * 3;
        int32_t indexData[] =
        {
            0 + offset, 1 + offset, 2 + offset,
        };

        memcpy(gData->indexData + i * (sizeof(indexData) / sizeof(int32_t)), indexData, sizeof(indexData));
    }

    for (int i = 0; i < boxes.size(); i++)
    {
        auto& obb = boxes[i];

        //float data[] =
        //{
        //    obb.centre[0] + obb.half_size[0], obb.centre[1] + obb.half_size[1], obb.centre[2] + obb.half_size[2], 0, 0, 0, 0, 0, 0,
        //    obb.centre[0] - obb.half_size[0], obb.centre[1] + obb.half_size[1], obb.centre[2] + obb.half_size[2], 0, 0, 0, 0, 0, 0,
        //    obb.centre[0] + obb.half_size[0], obb.centre[1] - obb.half_size[1], obb.centre[2] + obb.half_size[2], 0, 0, 0, 0, 0, 0,
        //    obb.centre[0] + obb.half_size[0], obb.centre[1] + obb.half_size[1], obb.centre[2] - obb.half_size[2], 0, 0, 0, 0, 0, 0,
        //    obb.centre[0] + obb.half_size[0], obb.centre[1] - obb.half_size[1], obb.centre[2] - obb.half_size[2], 0, 0, 0, 0, 0, 0,
        //    obb.centre[0] - obb.half_size[0], obb.centre[1] + obb.half_size[1], obb.centre[2] - obb.half_size[2], 0, 0, 0, 0, 0, 0,
        //    obb.centre[0] - obb.half_size[0], obb.centre[1] - obb.half_size[1], obb.centre[2] + obb.half_size[2], 0, 0, 0, 0, 0, 0,
        //    obb.centre[0] - obb.half_size[0], obb.centre[1] - obb.half_size[1], obb.centre[2] - obb.half_size[2], 0, 0, 0, 0, 0, 0
        //}
        float data[] =
        {
            obb.verticies[0], obb.verticies[1], obb.verticies[2], 0, 0, 0, 0, 0, 0,
            obb.verticies[3], obb.verticies[4], obb.verticies[5], 0, 0, 0, 0, 0, 0,
            obb.verticies[6], obb.verticies[7], obb.verticies[8], 0, 0, 0, 0, 0, 0,
            obb.verticies[9], obb.verticies[10], obb.verticies[11], 0, 0, 0, 0, 0, 0,
            obb.verticies[12], obb.verticies[13], obb.verticies[14], 0, 0, 0, 0, 0, 0,
            obb.verticies[15], obb.verticies[16], obb.verticies[17], 0, 0, 0, 0, 0, 0,
            obb.verticies[18], obb.verticies[19], obb.verticies[20], 0, 0, 0, 0, 0, 0,
            obb.verticies[21], obb.verticies[22], obb.verticies[23], 0, 0, 0, 0, 0, 0
        };

        memcpy(gData->vertexData + i * (sizeof(data) / sizeof(float)) + triangles.size() * 3 * gData->strideSize, data, sizeof(data));

        const int offset = i * 8;
        int32_t indexData[] =
        {
            7 + offset, 3 + offset, 2 + offset,
            7 + offset, 2 + offset, 4 + offset,
            5 + offset, 6 + offset, 0 + offset,
            5 + offset, 0 + offset, 1 + offset,
            3 + offset, 6 + offset, 0 + offset,
            3 + offset, 0 + offset, 2 + offset,
            7 + offset, 5 + offset, 1 + offset,
            7 + offset, 1 + offset, 4 + offset,
            4 + offset, 2 + offset, 0 + offset,
            4 + offset, 2 + offset, 1 + offset,
            7 + offset, 3 + offset, 5 + offset,
            6 + offset, 3 + offset, 5 + offset
        };

        memcpy(gData->indexData + i * (sizeof(indexData) / sizeof(int32_t)) + triangles.size() * 3, indexData, sizeof(indexData));
    }
}
