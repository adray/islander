#include "CollisionPrimitives.h"
#include "EPA.h"
#include "Matrix.h"
#include "AnimationController.h"
#include "Vector.h"
#include "Plane.h"
#include "Heightmap.h"
#include "SAT.h"
#include "OBB.h"
#include <cmath>

using namespace Islander::Collision;
using namespace Islander::Numerics;


bool Islander::Collision::CollisionBoxTest(const IslanderCollisionBox& a, const IslanderCollisionBox& b)
{
    return (a.minx <= b.maxx && a.maxx >= b.minx) &&
        (a.miny <= b.maxy && a.maxy >= b.miny) &&
        (a.minz <= b.maxz && a.maxz >= b.minz);
}

bool Islander::Collision::PointBoxCollision(const IslanderCollisionBox& box, float x, float y, float z)
{
    return x >= box.minx && x <= box.maxx &&
        y >= box.miny && y <= box.maxy &&
        z >= box.minz && z <= box.maxz;
}


bool CollisionSphereTest(const IslanderCollisionSphere& a, const IslanderCollisionSphere& b)
{
    float dx = b.centreX - a.centreX;
    float dy = b.centreY - a.centreY;
    float dz = b.centreZ - a.centreZ;

    float c = Dot(dx, dy, dz, dx, dy, dz) - (a.radius + b.radius);
    return c <= 0;
}

bool QuadraticFormula(float a, float b, float c, float& r1, float& r2)
{
    float q = b * b + 4 * a * c;
    if (q >= 0)
    {
        float sq = std::sqrtf(q);
        float d = 1 / (2 * a);
        r1 = (-b + sq) * d;
        r2 = (b + sq) * d;
        return true;
    }
    else
    {
        return false;
    }
}

// Based upon http://www.realtimerendering.com/Real-Time_Rendering_4th-Collision_Detection.pdf

float SweptCollisionSphereTest(const IslanderCollisionSphere& startSphere, const IslanderCollisionSphere& endSphere, const IslanderCollisionSphere& staticSphere, float& normalx, float& normaly, float& normalz)
{
    float pt1[3] = { startSphere.centreX, startSphere.centreY, startSphere.centreZ };
    float pt2[3] = { endSphere.centreX, endSphere.centreY, endSphere.centreZ };

    float staticSphereRadius = staticSphere.radius + startSphere.radius; // assuming that End Sphere and Start Sphere both have the same radius

    float vx = pt2[0] - pt1[0];
    float vy = pt2[1] - pt1[1];
    float vz = pt2[2] - pt1[2];

    float dx = staticSphere.centreX - startSphere.centreX;
    float dy = staticSphere.centreY - startSphere.centreY;
    float dz = staticSphere.centreZ - startSphere.centreZ;

    float a = Dot(vx, vy, vz, vx, vy, vz);
    float b = 2 * Dot(vx, vy, vz, dx, dy, dz);
    float c = Dot(dx, dy, dz, dx, dy, dz) - staticSphereRadius;

    if (c <= 0)
    {
        // already overlapping
        return 0;
    }

    float r1, r2;
    if (QuadraticFormula(a, b, c, r1, r2))
    {
        float t = std::min(r1, r2);

        float pt[3] = { pt1[0] + vx * t, pt1[1] + vy * t, pt1[2] + vz * t };

        normalx = pt[0] - pt2[0];
        normaly = pt[1] - pt2[1];
        normalz = pt[2] - pt2[2];

        Normalize(normalx, normaly, normalz, normalx, normaly, normalz);
        return t;
    }

    return 1;
}

bool Islander::Collision::RayPlaneTest(float* origin, float* dir, Islander::Numerics::Plane2* plane, float& t)
{
    float denom = Dot(plane->n[0], plane->n[1], plane->n[2], dir[0], dir[1], dir[2]);
    if (std::abs(denom) > 0.0001f)
    {
        float x = plane->p[0] - origin[0];
        float y = plane->p[1] - origin[1];
        float z = plane->p[2] - origin[2];

        t = Dot(x, y, z, plane->n[0], plane->n[1], plane->n[2]) / denom;
        /*if (t >= 0)*/ return true;
    }
    return false;
}

bool PointTriangleTest(float* pt, const IslanderCollisionTriangle& tri, float* normal)
{
    float d1[3];
    float d2[3];
    float d3[3];

    SubVec3(pt, tri.vertices[0], d1);
    SubVec3(pt, tri.vertices[1], d2);
    SubVec3(pt, tri.vertices[2], d3);

    float dir01[3];
    float dir12[3];
    float dir02[3];

    SubVec3(tri.vertices[1], tri.vertices[0], dir01);
    SubVec3(tri.vertices[2], tri.vertices[1], dir12);
    SubVec3(tri.vertices[0], tri.vertices[2], dir02);

    float c1[3];
    float c2[3];
    float c3[3];

    Islander::Numerics::CrossVec3(d1, dir01, c1);
    Islander::Numerics::CrossVec3(d2, dir12, c2);
    Islander::Numerics::CrossVec3(d3, dir02, c3);

    float dot1 = DotVec3(c1, normal);
    float dot2 = DotVec3(c2, normal);
    float dot3 = DotVec3(c3, normal);

    return dot1 <= 0.0f &&
        dot2 <= 0.0f &&
        dot3 <= 0.0f;
}

void ClosestPointOnEdge(float* a, float* b, float* pt, float* closestPt)
{
    // An explaination here https://math.stackexchange.com/questions/1521128/given-a-line-and-a-point-in-3d-how-to-find-the-closest-point-on-the-line

    float AB[3];
    SubVec3(b, a, AB);

    float PA[3];
    SubVec3(pt, a, PA);

    float t = DotVec3(PA, AB) / DotVec3(AB, AB);
    t = std::min(1.0f, std::max(0.0f, t));

    closestPt[0] = a[0] + t * AB[0];
    closestPt[1] = a[1] + t * AB[1];
    closestPt[2] = a[2] + t * AB[2];
}

bool Islander::Collision::SphereTriangleCollision(float* centre, float radius, const IslanderCollisionTriangle& tri)
{
    // Credit: https://wickedengine.net/2020/04/26/capsule-collision-detection/

    // Find the plane which the triangle lies upon

    float dir1[3];
    float dir2[3];

    SubVec3(tri.vertices[0], tri.vertices[1], dir1);
    SubVec3(tri.vertices[0], tri.vertices[2], dir2);

    NormalizeVec3(dir1);
    NormalizeVec3(dir2);

    float normal[3];
    CrossVec3(dir1, dir2, normal);
    NormalizeVec3(normal);

    Islander::Numerics::Plane plane;
    Islander::Numerics::CreatePlane(&plane, tri.vertices[0], dir1, dir2);

    // Plane - sphere interest test

    float dist = Islander::Numerics::PlaneDistance(&plane, centre);
    if (std::abs(dist) > radius)
    {
        return false;
    }

    // Triangle - collision
    // Collision if:
    // -If the point projected on the plane is inside the triangle
    // -If the closest point on any of the edges is less than the radius

    float pointOnPlane[3] = {
        centre[0] - dist * normal[0],
        centre[1] - dist * normal[1],
        centre[2] - dist * normal[2],
    };

    if (PointTriangleTest(pointOnPlane, tri, normal))
    {
        return true;
    }

    float cp1[3];
    float cp2[3];
    float cp3[3];
    
    ClosestPointOnEdge(tri.vertices[0], tri.vertices[1], centre, cp1);
    ClosestPointOnEdge(tri.vertices[1], tri.vertices[2], centre, cp2);
    ClosestPointOnEdge(tri.vertices[2], tri.vertices[0], centre, cp3);

    const float radiusSquared = radius * radius;

    SubVec3(centre, cp1, cp1);
    SubVec3(centre, cp2, cp2);
    SubVec3(centre, cp3, cp3);

    bool intersects = DotVec3(cp1, cp1) < radiusSquared;
    intersects |= DotVec3(cp2, cp2) < radiusSquared;
    intersects |= DotVec3(cp3, cp3) < radiusSquared;

    return intersects;
}

bool SphereTriangleCollision(const IslanderCollisionSphere& sphere, const IslanderCollisionTriangle& tri)
{
    float centre[3] = { sphere.centreX, sphere.centreY, sphere.centreZ };
    return SphereTriangleCollision(centre, sphere.radius, tri);
}

bool Islander::Collision::CapsuleTriangleCollision(const IslanderCollisionCapsule& capsule, const IslanderCollisionTriangle& tri)
{
    // Credit: https://wickedengine.net/2020/04/26/capsule-collision-detection/

    float topSphere[3] = {
        capsule.centreX,
        capsule.centreY + capsule.height / 2.0f,
        capsule.centreZ
    };

    float bottomSphere[3] = {
        capsule.centreX,
        capsule.centreY - capsule.height / 2.0f,
        capsule.centreZ
    };

    // Find plane which triangles lies on

    float dir1[3];
    float dir2[3];

    SubVec3(tri.vertices[0], tri.vertices[1], dir1);
    SubVec3(tri.vertices[0], tri.vertices[2], dir2);

    NormalizeVec3(dir1);
    NormalizeVec3(dir2);

    float normal[3];
    CrossVec3(dir1, dir2, normal);
    NormalizeVec3(normal);

    Islander::Numerics::Plane2 plane;
    Islander::Numerics::CreatePlane(&plane, normal[0], normal[1], normal[2], tri.vertices[0][0], tri.vertices[0][1], tri.vertices[0][2]);

    // Find point where capsule centre intersects with plane along the up vector

    float up[3] = { 0.0f, 1.0f, 0.0f };

    float t;
    if (!RayPlaneTest(topSphere, up, &plane, t))
    {
        // Edge case they CAN collide if they are parallel

        float ref_point[3];
        CopyVec3(tri.vertices[0], ref_point);

        // Find point between the top sphere and bottom sphere closest to the reference point

        float closestPt[3];
        ClosestPointOnEdge(bottomSphere, topSphere, ref_point, closestPt);

        // Sphere - triangle collision
        return SphereTriangleCollision(closestPt, capsule.radius, tri);
    }

    float pt[3] = {
        t * up[0] + topSphere[0],
        t * up[1] + topSphere[1],
        t * up[2] + topSphere[2]
    };

    float ref_point[3];

    if (PointTriangleTest(pt, tri, normal))
    {
        CopyVec3(pt, ref_point);
    }
    else
    {
        float cp1[3];
        float cp2[3];
        float cp3[3];

        ClosestPointOnEdge(tri.vertices[0], tri.vertices[1], pt, cp1);
        ClosestPointOnEdge(tri.vertices[1], tri.vertices[2], pt, cp2);
        ClosestPointOnEdge(tri.vertices[2], tri.vertices[0], pt, cp3);

        SubVec3(pt, cp1, cp1);
        SubVec3(pt, cp2, cp2);
        SubVec3(pt, cp3, cp3);

        float* dir = cp1;
        float minT = LengthSqr(cp1);

        float t1 = LengthSqr(cp2);
        float t2 = LengthSqr(cp3);

        if (t1 < minT)
        {
            minT = t1;
            dir = cp2;
        }

        if (t2 < minT)
        {
            minT = t2;
            dir = cp3;
        }

        ref_point[0] = minT * dir[0] + pt[0];
        ref_point[1] = minT * dir[1] + pt[1];
        ref_point[2] = minT * dir[2] + pt[2];
    }

    // Find point between the top sphere and bottom sphere closest to the reference point

    float closestPt[3];
    ClosestPointOnEdge(bottomSphere, topSphere, ref_point, closestPt);

    // Sphere - triangle collision
    return SphereTriangleCollision(closestPt, capsule.radius, tri);
}

//int GetHeightmapChunk(Islander::Model::Heightmap* heightmap, float* pos)
//{
//    float range[3] = {
//        heightmap->max[0] - heightmap->min[0],
//        heightmap->max[1] - heightmap->min[1],
//        heightmap->max[2] - heightmap->min[2]
//    };
//
//    int numWidth = heightmap->width / heightmap->chunkWidth;
//    int numHeight = heightmap->height / heightmap->chunkHeight;
//
//    float cellSizeX = range[0] / numWidth;
//    float cellSizeZ = range[2] / numHeight;
//
//    IslanderCollisionBox box;
//    box.minx = heightmap->min[0];
//    box.miny = heightmap->min[1];
//    box.minz = heightmap->min[2];
//    box.maxx = heightmap->max[0];
//    box.maxy = heightmap->max[1];
//    box.maxz = heightmap->max[2];
//
//    if (PointBoxCollision(box, pos[0], pos[1], pos[2]))
//    {
//        // Inside the terrain bounds
//
//        int cellX = (pos[0] - heightmap->min[0]) / cellSizeX;
//        int cellZ = (pos[2] - heightmap->min[2]) / cellSizeZ;
//
//        return cellZ + cellX * numHeight;
//    }
//    else
//    {
//        return -1;
//    }
//}

void GetQuadTreeNodes(std::vector<int>& nodes, Islander::Model::HeightmapChunk* chunk, float* pos)
{
    //TODO: optimize

    if (chunk->loaded)
    {
        for (int i = -2; i <= 2; i++)
        {
            for (int j = -2; j <= 2; j++)
            {
                float samplePos[3] = {
                    pos[0] + i * 5.0f,
                    pos[1],
                    pos[2] + j * 5.0f,
                };

                int id = Islander::Model::FindQuadTreeNodeAtPoint(chunk, samplePos);

                if (id > -1)
                {
                    auto& it = std::find(nodes.begin(), nodes.end(), id);
                    if (it == nodes.end())
                    {
                        nodes.push_back(id);
                    }
                }
            }
        }
    }
}

void Islander::Collision::CalculateTriangleNormal(const float* v0, const float* v1, const float* v2, float* normal)
{
    float v0v1[3];
    SubVec3(v0, v1, v0v1);
    NormalizeVec3(v0v1);

    float v0v2[3];
    SubVec3(v0, v2, v0v2);
    NormalizeVec3(v0v2);

    Islander::Numerics::CrossVec3(v0v1, v0v2, normal);
    Islander::Numerics::NormalizeVec3(normal);
}

template<typename T>
bool HeightmapSweptCollision(Islander::Model::Heightmap* heightmap, bool(*func)(const T&, const IslanderCollisionTriangle& tri), const T& collider, float* pos, float* velocity, float* normal, float* triVerts)
{
    // Get a patch of triangles on the heightmap

    int cellX; int cellZ;
    int chunkId = GetHeightmapChunk(heightmap, pos, cellX, cellZ);

    if (chunkId > -1)
    {
        auto& chunk = heightmap->chunks[chunkId];

        std::vector<int> quadIds;
        GetQuadTreeNodes(quadIds, &chunk, pos);
        //int quadId = Islander::Model::FindQuadTreeNodeAtPoint(&chunk, pos);

        //if (quadId > -1)
        for (int quadId : quadIds)
        {
            auto& quad = chunk.quadTree[quadId];

            float n[3];
            IslanderCollisionBox box;
            box.minx = quad.min[0];
            box.miny = quad.min[1];
            box.minz = quad.min[2];
            box.maxx = quad.max[0];
            box.maxy = quad.max[1];
            box.maxz = quad.max[2];
            if (Islander::Collision::GJK(collider, box, n))
            {
                for (int i = 0; i < quad.numIndices.size(); i++)
                {
                    for (int j = 0; j < quad.numIndices[i]; j += 3) // each triangle in strip
                    {
                        int i1 = chunk.indexData[(quad.startIndex[i] + j)] * heightmap->vertexStride * sizeof(float);
                        int i2 = chunk.indexData[(quad.startIndex[i] + j + 1)] * heightmap->vertexStride * sizeof(float);
                        int i3 = chunk.indexData[(quad.startIndex[i] + j + 2)] * heightmap->vertexStride * sizeof(float);

                        float* v0 = (float*)&chunk.vertexData[i1];
                        float* v1 = (float*)&chunk.vertexData[i2];
                        float* v2 = (float*)&chunk.vertexData[i3];

                        const int flags1 = ((int*)&chunk.vertexData[i1])[19];
                        const int flags2 = ((int*)&chunk.vertexData[i2])[19];
                        const int flags3 = ((int*)&chunk.vertexData[i3])[19];

                        if (flags1 || flags2 || flags3)
                        {
                            continue;
                        }

                        IslanderCollisionTriangle tri;
                        tri.vertices[0] = v0;
                        tri.vertices[1] = v1;
                        tri.vertices[2] = v2;

                        if (func(collider, tri))
                        {
                            CalculateTriangleNormal(v0, v1, v2, normal);

                            //Islander::Model::HeightmapDebugQuery debug;
                            //debug.chunkId = chunkId;
                            //debug.quadId = quadId;
                            //debug.collision = nullptr;
                            //debug.hit = true;
                            //CopyVec3(velocity, debug.velocity);
                            //heightmap->debugInfo.hits.push_back(debug);

                            float* triVerts0 = &triVerts[0];
                            float* triVerts1 = &triVerts[3];
                            float* triVerts2 = &triVerts[6];
                            std::memcpy(triVerts0, v0, sizeof(float) * 3);
                            std::memcpy(triVerts1, v1, sizeof(float) * 3);
                            std::memcpy(triVerts2, v2, sizeof(float) * 3);

                            //return ((float)it / iterations);
                            return true;
                        }
                    }
                }
            }

            //Islander::Model::HeightmapDebugQuery debug;
            //debug.chunkId = chunkId;
            //debug.quadId = quadId;
            //debug.collision = nullptr;
            //debug.hit = false;
            //CopyVec3(velocity, debug.velocity);
            //heightmap->debugInfo.hits.push_back(debug);
        }
    }

    //return 1.0f;
    return false;
}

float Islander::Collision::HeightmapSweptCollision(Islander::Model::Heightmap* heightmap, CollisionTransform& transform, const IslanderCollisionCapsule& capsule, float* velocity, float* normal, float* triVerts)
{
    // Get a patch of triangles on the heightmap
    // Then use the capsule - triangle tests to find intersection (swept)

    const int iterations = 5;

    for (int it = 0; it < iterations; it++)
    {
        const float t = ((float)(it + 1) / iterations);

        IslanderCollisionCapsule copiedCapsule = capsule;
        copiedCapsule.centreX = copiedCapsule.centreX - transform.px + velocity[0] * t;
        copiedCapsule.centreY = copiedCapsule.centreY - transform.py + velocity[1] * t;
        copiedCapsule.centreZ = copiedCapsule.centreZ - transform.pz + velocity[2] * t;

        float dir[3];
        CopyVec3(velocity, dir);
        Islander::Numerics::NormalizeVec3(dir);

        float pos[3] = {
            copiedCapsule.centreX + dir[0] * copiedCapsule.radius,
            copiedCapsule.centreY - copiedCapsule.height / 2.0f - copiedCapsule.radius,
            copiedCapsule.centreZ + dir[2] * copiedCapsule.radius
        };

        if (::HeightmapSweptCollision<IslanderCollisionCapsule>(heightmap, CapsuleTriangleCollision, copiedCapsule, pos, velocity, normal, triVerts))
        {
            return ((float)it / iterations);
        }
    }

    return 1.0f;
}

float Islander::Collision::HeightmapSweptCollision(Islander::Model::Heightmap* heightmap, CollisionTransform& transform, const IslanderCollisionSphere& sphere, float* velocity, float* normal, float* triVerts)
{
    const int iterations = 5;

    for (int it = 0; it < iterations; it++)
    {
        const float t = ((float)(it + 1) / iterations);

        IslanderCollisionSphere copiedSphere = sphere;
        copiedSphere.centreX = copiedSphere.centreX - transform.px + velocity[0] * t;
        copiedSphere.centreY = copiedSphere.centreY - transform.py + velocity[1] * t;
        copiedSphere.centreZ = copiedSphere.centreZ - transform.pz + velocity[2] * t;

        float dir[3];
        CopyVec3(velocity, dir);
        Islander::Numerics::NormalizeVec3(dir);

        float pos[3] = {
            copiedSphere.centreX + dir[0] * copiedSphere.radius,
            copiedSphere.centreY - copiedSphere.radius,
            copiedSphere.centreZ + dir[2] * copiedSphere.radius
        };

        if (::HeightmapSweptCollision<IslanderCollisionSphere>(heightmap, ::SphereTriangleCollision, copiedSphere, pos, velocity, normal, triVerts))
        {
            return ((float)it / iterations);
        }
    }

    return 1.0f;
}

template<typename T>
bool BoxTriangleCollision(const T& box, const IslanderCollisionTriangle& tri)
{
    return Islander::Collision::SATConvexPolyhedraTest(box, tri);
}

float Islander::Collision::HeightmapSweptCollision(Islander::Model::Heightmap* heightmap, CollisionTransform& transform, const IslanderCollisionBox& box, float* velocity, float* normal, float* triVerts)
{
    const int iterations = 5;

    for (int it = 0; it < iterations; it++)
    {
        const float t = ((float)(it + 1) / iterations);

        IslanderCollisionBox copiedBox = box;
        copiedBox.maxx = copiedBox.maxx - transform.px + velocity[0] * t;
        copiedBox.maxy = copiedBox.maxy - transform.py + velocity[1] * t;
        copiedBox.maxz = copiedBox.maxz - transform.pz + velocity[2] * t;
        copiedBox.minx = copiedBox.minx - transform.px + velocity[0] * t;
        copiedBox.miny = copiedBox.miny - transform.py + velocity[1] * t;
        copiedBox.minz = copiedBox.minz - transform.pz + velocity[2] * t;

        float dir[3];
        CopyVec3(velocity, dir);
        Islander::Numerics::NormalizeVec3(dir);

        float pos[3] = {
            copiedBox.minx + (-copiedBox.minx + copiedBox.maxx) * (dir[0]+1)/2.0f,
            copiedBox.miny + (-copiedBox.miny + copiedBox.maxy) * (dir[1]+1)/2.0f,
            copiedBox.minz + (-copiedBox.minz + copiedBox.maxz) * (dir[2]+1)/2.0f
        };

        if (::HeightmapSweptCollision<IslanderCollisionBox>(heightmap, ::BoxTriangleCollision, copiedBox, pos, velocity, normal, triVerts))
        {
            return ((float)it / iterations);
        }
    }

    return 1.0f;
}

float Islander::Collision::HeightmapSweptCollision(Islander::Model::Heightmap* heightmap, CollisionTransform& transform, const IslanderCollisionOBB& obb, float* velocity, float* normal, float* triVerts)
{
    const int iterations = 5;

    for (int it = 0; it < iterations; it++)
    {
        const float t = ((float)(it + 1) / iterations);

        IslanderCollisionOBB copiedBox = obb;
        copiedBox.centre[0] = copiedBox.centre[0] - transform.px + velocity[0] * t;
        copiedBox.centre[1] = copiedBox.centre[1] - transform.py + velocity[1] * t;
        copiedBox.centre[2] = copiedBox.centre[2] - transform.pz + velocity[2] * t;

        for (int i = 0; i < 8; i++)
        {
            copiedBox.verticies[i * 3] -= transform.px + velocity[0] * t;
            copiedBox.verticies[i * 3 + 1] -= transform.py + velocity[1] * t;
            copiedBox.verticies[i * 3 + 2] -= transform.pz + velocity[2] * t;
        }

        float dir[3];
        CopyVec3(velocity, dir);
        Islander::Numerics::NormalizeVec3(dir);

        // TODO: move the sample point to end of the dir

        float pos[3] = {
            copiedBox.centre[0],
            copiedBox.centre[1],
            copiedBox.centre[2]
        };

        if (::HeightmapSweptCollision<IslanderCollisionOBB>(heightmap, ::BoxTriangleCollision, copiedBox, pos, velocity, normal, triVerts))
        {
            return ((float)it / iterations);
        }
    }

    return 1.0f;
}

float Islander::Collision::TriangleMeshSweptCollision(const std::vector<IslanderCollisionTriangle>& triangles, const IslanderCollisionCapsule& capsule, float* velocity, float* normal, float* triVerts)
{
    const int iterations = 5;

    for (int it = 0; it < iterations; it++)
    {
        const float t = ((float)(it + 1) / iterations);

        IslanderCollisionCapsule copiedCapsule = capsule;
        copiedCapsule.centreX = copiedCapsule.centreX + velocity[0] * t;
        copiedCapsule.centreY = copiedCapsule.centreY + velocity[1] * t;
        copiedCapsule.centreZ = copiedCapsule.centreZ + velocity[2] * t;

        for (auto& tri : triangles)
        {
            if (CapsuleTriangleCollision(copiedCapsule, tri))
            {
                CalculateTriangleNormal(tri.vertices[0], tri.vertices[1], tri.vertices[2], normal);

                std::memcpy(&triVerts[0], tri.vertices[0], sizeof(float) * 3);
                std::memcpy(&triVerts[3], tri.vertices[1], sizeof(float) * 3);
                std::memcpy(&triVerts[6], tri.vertices[2], sizeof(float) * 3);

                return ((float)it / iterations);
            }
        }
    }

    return 1.0f;
}

float Islander::Collision::TriangleMeshSweptCollision(const std::vector<IslanderCollisionTriangle>& triangles, const IslanderCollisionSphere& sphere, float* velocity, float* normal, float* triVerts)
{
    const int iterations = 5;

    for (int it = 0; it < iterations; it++)
    {
        const float t = ((float)(it + 1) / iterations);

        IslanderCollisionSphere copiedSphere = sphere;
        copiedSphere.centreX = copiedSphere.centreX + velocity[0] * t;
        copiedSphere.centreY = copiedSphere.centreY + velocity[1] * t;
        copiedSphere.centreZ = copiedSphere.centreZ + velocity[2] * t;

        float centre[3] = {
            copiedSphere.centreX, copiedSphere.centreY, copiedSphere.centreZ
        };

        for (auto& tri : triangles)
        {
            if (SphereTriangleCollision(centre, copiedSphere.radius, tri))
            {
                CalculateTriangleNormal(tri.vertices[0], tri.vertices[1], tri.vertices[2], normal);

                std::memcpy(&triVerts[0], tri.vertices[0], sizeof(float) * 3);
                std::memcpy(&triVerts[3], tri.vertices[1], sizeof(float) * 3);
                std::memcpy(&triVerts[6], tri.vertices[2], sizeof(float) * 3);

                return ((float)it / iterations);
            }
        }
    }

    return 1.0f;
}

float Islander::Collision::TriangleMeshSweptCollision(const std::vector<IslanderCollisionTriangle>& triangles, const IslanderCollisionBox& box, float* velocity, float* normal, float* triVerts)
{
    const int iterations = 5;
    for (int it = 0; it < iterations; it++)
    {
        const float t = ((float)(it + 1) / iterations);

        IslanderCollisionBox copiedBox = box;
        copiedBox.maxx += t * velocity[0];
        copiedBox.minx += t * velocity[0];
        copiedBox.maxy += t * velocity[1];
        copiedBox.miny += t * velocity[1];
        copiedBox.maxz += t * velocity[2];
        copiedBox.minz += t * velocity[2];

        for (auto& tri : triangles)
        {
            if (Islander::Collision::SATConvexPolyhedraTest(copiedBox, tri))
            {
                CalculateTriangleNormal(tri.vertices[0], tri.vertices[1], tri.vertices[2], normal);

                std::memcpy(&triVerts[0], tri.vertices[0], sizeof(float) * 3);
                std::memcpy(&triVerts[3], tri.vertices[1], sizeof(float) * 3);
                std::memcpy(&triVerts[6], tri.vertices[2], sizeof(float) * 3);

                return ((float)it / iterations);
            }
        }
    }

    return 1.0f;
}

float Islander::Collision::TriangleMeshSweptCollision(const std::vector<IslanderCollisionTriangle>& triangles, const IslanderCollisionOBB& obb, float* velocity, float* normal, float* triVerts)
{
    const int iterations = 5;
    for (int it = 0; it < iterations; it++)
    {
        const float t = ((float)(it + 1) / iterations);

        IslanderCollisionOBB copiedBox = obb;
        for (int i = 0; i < 8; i++)
        {
            copiedBox.verticies[i * 3] += velocity[0] * t;
            copiedBox.verticies[i * 3 + 1] += velocity[1] * t;
            copiedBox.verticies[i * 3 + 2] += velocity[2] * t;
        }

        copiedBox.centre[0] += velocity[0] * t;
        copiedBox.centre[1] += velocity[1] * t;
        copiedBox.centre[2] += velocity[2] * t;

        for (int i = 0; i < 6; i++)
        {
            copiedBox.extents[i * 3] += velocity[0] * t;
            copiedBox.extents[i * 3 + 1] += velocity[1] * t;
            copiedBox.extents[i * 3 + 2] += velocity[2] * t;
        }

        for (auto& tri : triangles)
        {
            if (Islander::Collision::SATConvexPolyhedraTest(copiedBox, tri))
            {
                CalculateTriangleNormal(tri.vertices[0], tri.vertices[1], tri.vertices[2], normal);

                std::memcpy(&triVerts[0], tri.vertices[0], sizeof(float) * 3);
                std::memcpy(&triVerts[3], tri.vertices[1], sizeof(float) * 3);
                std::memcpy(&triVerts[6], tri.vertices[2], sizeof(float) * 3);

                return ((float)it / iterations);
            }
        }
    }

    return 1.0f;
}

void CreateTransformedCapsule(const Matrix4x4 matrix, IslanderCollisionCapsule& capsule)
{
    Matrix4x4::TransformVector(matrix, &capsule.centreX, &capsule.centreY, &capsule.centreZ);
}

void CreateTransformedCapsule(float px, float py, float pz, float rx, float ry, float rz, float sx, float sy, float sz, IslanderCollisionCapsule& capsule)
{
    Matrix4x4 matrix(Matrix4x4Type::MATRIX_4X4_IDENTITY);
    Matrix4x4::CreateTransformationMatrix(
        px,
        py,
        pz,
        rx,
        ry,
        rz,
        sx,
        sy,
        sz,
        &matrix);

    CreateTransformedCapsule(matrix, capsule);
}

void CreateTransformedSphere(const Matrix4x4 matrix, IslanderCollisionSphere& sphere)
{
    Matrix4x4::TransformVector(matrix, &sphere.centreX, &sphere.centreY, &sphere.centreZ);
}

void CreateTransformedSphere(float px, float py, float pz, float rx, float ry, float rz, float sx, float sy, float sz, IslanderCollisionSphere& sphere)
{
    Matrix4x4 matrix(Matrix4x4Type::MATRIX_4X4_IDENTITY);
    Matrix4x4::CreateTransformationMatrix(
        px,
        py,
        pz,
        rx,
        ry,
        rz,
        sx,
        sy,
        sz,
        &matrix);

    CreateTransformedSphere(matrix, sphere);
}

void CreateTransformedOBB(const Matrix4x4 matrix, IslanderCollisionOBB& obb)
{
    for (int i = 0; i < 24; i += 3)
    {
        Matrix4x4::TransformVector(matrix, &obb.verticies[i], &obb.verticies[i + 1], &obb.verticies[i + 2]);
    }

    ComputeOBBFromVertices(obb);
}

void CreateTransformedOBB(float px, float py, float pz, float rx, float ry, float rz, float sx, float sy, float sz, IslanderCollisionOBB& obb)
{
    Matrix4x4 matrix(Matrix4x4Type::MATRIX_4X4_IDENTITY);
    Matrix4x4::CreateTransformationMatrix(
        px,
        py,
        pz,
        rx,
        ry,
        rz,
        sx,
        sy,
        sz,
        &matrix);

    CreateTransformedOBB(matrix, obb);
}

void CreateTransformedAABB(const Matrix4x4 matrix, IslanderCollisionBox& box)
{
    Matrix4x4::TransformVector(matrix, &box.maxx, &box.maxy, &box.maxz);
    Matrix4x4::TransformVector(matrix, &box.minx, &box.miny, &box.minz);

    IslanderCollisionBox box2;
    box2.maxx = std::max(box.maxx, box.minx);
    box2.minx = std::min(box.maxx, box.minx);
    box2.maxy = std::max(box.maxy, box.miny);
    box2.miny = std::min(box.maxy, box.miny);
    box2.maxz = std::max(box.maxz, box.minz);
    box2.minz = std::min(box.maxz, box.minz);

    box = box2;
}

void Islander::Collision::CreateTransformedAABB(float px, float py, float pz, float rx, float ry, float rz, float sx, float sy, float sz, IslanderCollisionBox& box)
{
    Matrix4x4 matrix(Matrix4x4Type::MATRIX_4X4_IDENTITY);
    Matrix4x4::CreateTransformationMatrix(
        px,
        py,
        pz,
        rx,
        ry,
        rz,
        sx,
        sy,
        sz,
        &matrix);

    Matrix4x4::TransformVector(matrix, &box.maxx, &box.maxy, &box.maxz);
    Matrix4x4::TransformVector(matrix, &box.minx, &box.miny, &box.minz);

    IslanderCollisionBox box2;
    box2.maxx = std::max(box.maxx, box.minx);
    box2.minx = std::min(box.maxx, box.minx);
    box2.maxy = std::max(box.maxy, box.miny);
    box2.miny = std::min(box.maxy, box.miny);
    box2.maxz = std::max(box.maxz, box.minz);
    box2.minz = std::min(box.maxz, box.minz);

    box = box2;
}

void Islander::Collision::CalculateOBB(IslanderCollisionOBB& boxStart, const IslanderCollisionDynamicMeshInfo& info, Islander::Collision::CollisionDynamicMesh* parentMesh)
{
    float rx = info.rx + info.rdx;
    float ry = info.ry + info.rdy;
    float rz = info.rz + info.rdz;

    if (parentMesh != nullptr && parentMesh->meshData != nullptr)
    {
        auto meshData = parentMesh->meshData;
        Islander::Model::AnimationSystem* system = (Islander::Model::AnimationSystem*)info.animationSystem;

        float displacement[3] = {};
        auto& controller = system->controllers[info.parentControllerId];
        if (controller.animation.isRootMotion || controller.blend.isRootMotion)
        {
            float rot[3] = { parentMesh->info.rx,
                parentMesh->info.ry,
                parentMesh->info.rz };
            float scale[3] = { parentMesh->info.sx,
                parentMesh->info.sy,
                parentMesh->info.sz };
            Islander::Model::CalculateRootMotion(meshData, system, info.parentControllerId, false, rot, scale, displacement);
        }

        Matrix4x4 parent(Matrix4x4Type::MATRIX_4X4_IDENTITY);
        Matrix4x4::CreateTransformationMatrix(
            parentMesh->info.px - displacement[0],
            parentMesh->info.py - displacement[1],
            parentMesh->info.pz - displacement[2],
            parentMesh->info.rx,
            parentMesh->info.ry,
            parentMesh->info.rz,
            parentMesh->info.sx,
            parentMesh->info.sy,
            parentMesh->info.sz,
            &parent);
        CalculateTransformForPose(meshData, system, info.parentControllerId, info.parentBone, parent);

        Matrix4x4 world(Matrix4x4Type::MATRIX_4X4_IDENTITY);
        Matrix4x4::CreateTransformationMatrix(
            info.px,
            info.py,
            info.pz,
            info.rx,
            info.ry,
            info.rz,
            info.sx,
            info.sy,
            info.sz,
            &world);

        Matrix4x4::Multiply(world, parent, &world);
        CreateTransformedOBB(world, boxStart);
    }
    else
    {
        CreateTransformedOBB(info.px, info.py, info.pz, rx, ry, rz, info.sx, info.sy, info.sz, boxStart);
    }
}

void Islander::Collision::CalculateOBB(IslanderCollisionOBB& boxStart, Islander::Collision::CollisionDynamicMesh* mesh)
{
    CalculateOBB(boxStart, mesh->info, mesh->parentMesh);
}

void Islander::Collision::CalculateBox(IslanderCollisionBox& boxStart, Islander::Collision::CollisionDynamicMesh* mesh)
{
    float rx = mesh->info.rx + mesh->info.rdx;
    float ry = mesh->info.ry + mesh->info.rdy;
    float rz = mesh->info.rz + mesh->info.rdz;

    if (mesh->parentMesh != nullptr && mesh->parentMesh->meshData != nullptr && mesh->info.parentBone > -1)
    {
        auto meshData = mesh->parentMesh->meshData;
        Islander::Model::AnimationSystem* system = (Islander::Model::AnimationSystem*)mesh->info.animationSystem;

        Matrix4x4 parent(Matrix4x4Type::MATRIX_4X4_IDENTITY);
        Matrix4x4::CreateTransformationMatrix(
            mesh->parentMesh->info.px,
            mesh->parentMesh->info.py,
            mesh->parentMesh->info.pz,
            mesh->parentMesh->info.rx,
            mesh->parentMesh->info.ry,
            mesh->parentMesh->info.rz,
            mesh->parentMesh->info.sx,
            mesh->parentMesh->info.sy,
            mesh->parentMesh->info.sz,
            &parent);
        CalculateTransformForPose(meshData, system, mesh->info.parentControllerId, mesh->info.parentBone, parent);

        Matrix4x4 world(Matrix4x4Type::MATRIX_4X4_IDENTITY);
        Matrix4x4::CreateTransformationMatrix(
            mesh->info.px,
            mesh->info.py,
            mesh->info.pz,
            mesh->info.rx,
            mesh->info.ry,
            mesh->info.rz,
            mesh->info.sx,
            mesh->info.sy,
            mesh->info.sz,
            &world);

        Matrix4x4::Multiply(world, parent, &world);
        CreateTransformedAABB(world, boxStart);
        //CreateTransformedAABB(world, boxEnd); // assuming a child object has no velocity of its own
        //boxEnd = boxStart;
    }
    else if (mesh->parentMesh != nullptr && mesh->parentMesh->meshData != nullptr && mesh->info.parentBone == -1)
    {
        Matrix4x4 parent(Matrix4x4Type::MATRIX_4X4_IDENTITY);
        Matrix4x4::CreateTransformationMatrix(
            mesh->parentMesh->info.px,
            mesh->parentMesh->info.py,
            mesh->parentMesh->info.pz,
            mesh->parentMesh->info.rx,
            mesh->parentMesh->info.ry,
            mesh->parentMesh->info.rz,
            mesh->parentMesh->info.sx,
            mesh->parentMesh->info.sy,
            mesh->parentMesh->info.sz,
            &parent);

        Matrix4x4 world(Matrix4x4Type::MATRIX_4X4_IDENTITY);
        Matrix4x4::CreateTransformationMatrix(
            mesh->info.px,
            mesh->info.py,
            mesh->info.pz,
            mesh->info.rx,
            mesh->info.ry,
            mesh->info.rz,
            mesh->info.sx,
            mesh->info.sy,
            mesh->info.sz,
            &world);

        Matrix4x4::Multiply(world, parent, &world);
        CreateTransformedAABB(world, boxStart);
    }
    else
    {
        CreateTransformedAABB(mesh->info.px, mesh->info.py, mesh->info.pz, /*mesh->info.rx, mesh->info.ry, mesh->info.rz,*/ rx, ry, rz, mesh->info.sx, mesh->info.sy, mesh->info.sz, boxStart);
        //CreateTransformedAABB(x, y, z, rx, ry, rz, mesh->info.sx, mesh->info.sy, mesh->info.sz, boxEnd);

        //boxEnd.maxx = boxStart.maxx + mesh->info.vx;
        //boxEnd.minx = boxStart.minx + mesh->info.vx;
        //boxEnd.maxy = boxStart.maxy + mesh->info.vy;
        //boxEnd.miny = boxStart.miny + mesh->info.vy;
        //boxEnd.maxz = boxStart.maxz + mesh->info.vz;
        //boxEnd.minz = boxStart.minz + mesh->info.vz;

        //float hx = (boxEnd.maxx - boxEnd.minx)/2;
        //float hy = (boxEnd.maxy - boxEnd.miny)/2;
        //float hz = (boxEnd.maxz - boxEnd.minz)/2;

        //float startBoxCentre[3] = { (boxStart.maxx - boxStart.minx)/2 + boxStart.minx, (boxStart.maxy - boxStart.miny)/2 + boxStart.miny, (boxStart.maxz - boxStart.minz)/2 + boxStart.minz };
        //boxStart.maxx = startBoxCentre[0] + hx;
        //boxStart.maxy = startBoxCentre[1] + hy;
        //boxStart.maxz = startBoxCentre[2] + hz;
        //boxStart.minx = startBoxCentre[0] - hx;
        //boxStart.miny = startBoxCentre[1] - hy;
        //boxStart.minz = startBoxCentre[2] - hz;

        // //TODO: remove me
        //float startVolume = (boxStart.maxx - boxStart.minx) * (boxStart.maxy - boxStart.miny) * (boxStart.maxz - boxStart.minz);
        //float endVolume = (boxEnd.maxx - boxEnd.minx)* (boxEnd.maxy - boxEnd.miny)* (boxEnd.maxz - boxEnd.minz);

        //assert(abs(startVolume - endVolume) < 0.001f);
    }
}

void Islander::Collision::CalculateSphere(IslanderCollisionSphere& sphereStart, Islander::Collision::CollisionDynamicMesh* mesh)
{
    float rx = mesh->info.rx + mesh->info.rdx;
    float ry = mesh->info.ry + mesh->info.rdy;
    float rz = mesh->info.rz + mesh->info.rdz;

    //if (mesh->parentMesh != nullptr && mesh->parentMesh->meshData != nullptr)
    //{
    //    auto meshData = mesh->parentMesh->meshData;
    //    Islander::AnimationSystem* system = (Islander::AnimationSystem*)mesh->info.animationSystem;

    //    Islander::Renderer::Matrix4x4 parent(Islander::Renderer::MATRIX_4X4_IDENTITY);
    //    Islander::Renderer::Matrix4x4::CreateTransformationMatrix(
    //        mesh->parentMesh->info.px,
    //        mesh->parentMesh->info.py,
    //        mesh->parentMesh->info.pz,
    //        mesh->parentMesh->info.rx,
    //        mesh->parentMesh->info.ry,
    //        mesh->parentMesh->info.rz,
    //        mesh->parentMesh->info.sx,
    //        mesh->parentMesh->info.sy,
    //        mesh->parentMesh->info.sz,
    //        &parent);
    //    CalculateTransformForPose(meshData, system, mesh->info.parentControllerId, mesh->info.parentBone, parent);

    //    Islander::Renderer::Matrix4x4 world(Islander::Renderer::MATRIX_4X4_IDENTITY);
    //    Islander::Renderer::Matrix4x4::CreateTransformationMatrix(
    //        mesh->info.px,
    //        mesh->info.py,
    //        mesh->info.pz,
    //        mesh->info.rx,
    //        mesh->info.ry,
    //        mesh->info.rz,
    //        mesh->info.sx,
    //        mesh->info.sy,
    //        mesh->info.sz,
    //        &world);

    //    Islander::Renderer::Matrix4x4::Multiply(world, parent, &world);
    //    CreateTransformedSphere(world, sphereStart);
    //    //CreateTransformedSphere(world, sphereEnd); // assuming a child object has no velocity of its own
    //    //sphereEnd = sphereStart;
    //}
    //else
    {
        CreateTransformedSphere(mesh->info.px, mesh->info.py, mesh->info.pz, /*mesh->info.rx, mesh->info.ry, mesh->info.rz,*/ rx, ry, rz, mesh->info.sx, mesh->info.sy, mesh->info.sz, sphereStart);
        //CreateTransformedSphere(x, y, z, rx, ry, rz, mesh->info.sx, mesh->info.sy, mesh->info.sz, sphereEnd);
    }
}

void GetTransform(Islander::Collision::CollisionDynamicMesh* mesh, Matrix4x4& world, Matrix4x4& parent)
{
    Matrix4x4::CreateTransformationMatrix(
        mesh->parentMesh->info.px,
        mesh->parentMesh->info.py,
        mesh->parentMesh->info.pz,
        mesh->parentMesh->info.rx,
        mesh->parentMesh->info.ry,
        mesh->parentMesh->info.rz,
        mesh->parentMesh->info.sx,
        mesh->parentMesh->info.sy,
        mesh->parentMesh->info.sz,
        &parent);

    auto meshData = mesh->parentMesh->meshData;
    if (mesh->info.parentBone != -1 && meshData != nullptr)
    {
        Islander::Model::AnimationSystem* system = (Islander::Model::AnimationSystem*)mesh->info.animationSystem;
        CalculateTransformForPose(meshData, system, mesh->info.parentControllerId, mesh->info.parentBone, parent);
    }

    Matrix4x4::Multiply(world, parent, &world);

    if (mesh->parentMesh->parentMesh != nullptr)
    {
        Matrix4x4 parent2(Matrix4x4Type::MATRIX_4X4_IDENTITY);

        GetTransform(mesh->parentMesh, world, parent2);
    }
}

void Islander::Collision::CalculateCapsule(IslanderCollisionCapsule& capsuleStart, Islander::Collision::CollisionDynamicMesh* mesh)
{
    float px = mesh->info.px;
    float py = mesh->info.py;
    float pz = mesh->info.pz;
    float rx = mesh->info.rx + mesh->info.rdx;
    float ry = mesh->info.ry + mesh->info.rdy;
    float rz = mesh->info.rz + mesh->info.rdz;

    //if (mesh->parentMesh != nullptr /*&& mesh->parentMesh->meshData != nullptr*/)
    //{
        //auto meshData = mesh->parentMesh->meshData;
        //Islander::AnimationSystem* system = (Islander::AnimationSystem*)mesh->info.animationSystem;

        //Islander::Renderer::Matrix4x4 parent(Islander::Renderer::MATRIX_4X4_IDENTITY);
        //Islander::Renderer::Matrix4x4::CreateTransformationMatrix(
        //    mesh->parentMesh->info.px,
        //    mesh->parentMesh->info.py,
        //    mesh->parentMesh->info.pz,
        //    mesh->parentMesh->info.rx,
        //    mesh->parentMesh->info.ry,
        //    mesh->parentMesh->info.rz,
        //    mesh->parentMesh->info.sx,
        //    mesh->parentMesh->info.sy,
        //    mesh->parentMesh->info.sz,
        //    &parent);
        //CalculateTransformForPose(meshData, system, mesh->info.parentControllerId, mesh->info.parentBone, parent);

      /*  Islander::Renderer::Matrix4x4 world(Islander::Renderer::MATRIX_4X4_IDENTITY);
        Islander::Renderer::Matrix4x4::CreateTransformationMatrix(
            mesh->info.px,
            mesh->info.py,
            mesh->info.pz,
            mesh->info.rx,
            mesh->info.ry,
            mesh->info.rz,
            mesh->info.sx,
            mesh->info.sy,
            mesh->info.sz,
            &world);*/
            //GetTransform(mesh, world, parent);

            //Islander::Renderer::Matrix4x4::Multiply(world, parent, &world);
          //  CreateTransformedCapsule(world, capsuleStart);
            //CreateTransformedSphere(world, sphereEnd); // assuming a child object has no velocity of its own
            //capsuleEnd = capsuleStart;
        //}
        //else
    {
        CreateTransformedCapsule(px, py, pz, /*mesh->info.rx, mesh->info.ry, mesh->info.rz,*/ rx, ry, rz, mesh->info.sx, mesh->info.sy, mesh->info.sz, capsuleStart);
        //CreateTransformedCapsule(x, y, z, rx, ry, rz, mesh->info.sx, mesh->info.sy, mesh->info.sz, capsuleEnd);
    }
}

float Islander::Collision::DispatchCollisionSweptTest(IslanderCollisionCapsule& collider, IslanderCollisionDynamicMeshInfo& obj2, float* velocity, float* normal, int* iterations)
{
    switch (obj2.collisionShape)
    {
    case ISLANDER_COLLISION_SHAPE_BOX: {
        IslanderCollisionBox static_box2 = obj2.box;
        CreateTransformedAABB(obj2.px, obj2.py, obj2.pz, obj2.rx, obj2.ry, obj2.rz, obj2.sx, obj2.sy, obj2.sz, static_box2);
        return Islander::Collision::GJK_Swept(collider, static_box2, velocity, normal, iterations);
    }
    case ISLANDER_COLLISION_SHAPE_SPHERE: {
        IslanderCollisionSphere static_box2 = obj2.sphere;
        CreateTransformedSphere(obj2.px, obj2.py, obj2.pz, obj2.rx, obj2.ry, obj2.rz, obj2.sx, obj2.sy, obj2.sz, static_box2);
        return Islander::Collision::GJK_Swept(collider, static_box2, velocity, normal, iterations);
    }
    case ISLANDER_COLLISION_SHAPE_CAPSULE: {
        IslanderCollisionCapsule static_box2 = obj2.capsule;
        CreateTransformedCapsule(obj2.px, obj2.py, obj2.pz, obj2.rx, obj2.ry, obj2.rz, obj2.sx, obj2.sy, obj2.sz, static_box2);
        return Islander::Collision::GJK_Swept(collider, static_box2, velocity, normal, iterations);
    }
    case ISLANDER_COLLISION_SHAPE_ORIENTATED_BOX: {
        IslanderCollisionOBB static_box2 = obj2.obb;
        CreateTransformedOBB(obj2.px, obj2.py, obj2.pz, obj2.rx, obj2.ry, obj2.rz, obj2.sx, obj2.sy, obj2.sz, static_box2);
        return Islander::Collision::GJK_Swept(collider, static_box2, velocity, normal, iterations);
    }
    }
}

float Islander::Collision::DispatchCollisionSweptTest(IslanderCollisionSphere& collider, IslanderCollisionDynamicMeshInfo& obj2, float* velocity, float* normal, int* iterations)
{
    switch (obj2.collisionShape)
    {
    case ISLANDER_COLLISION_SHAPE_BOX: {
        IslanderCollisionBox static_box2 = obj2.box;
        CreateTransformedAABB(obj2.px, obj2.py, obj2.pz, obj2.rx, obj2.ry, obj2.rz, obj2.sx, obj2.sy, obj2.sz, static_box2);
        return Islander::Collision::GJK_Swept(collider, static_box2, velocity, normal, iterations);
    }
    case ISLANDER_COLLISION_SHAPE_SPHERE: {
        IslanderCollisionSphere static_box2 = obj2.sphere;
        CreateTransformedSphere(obj2.px, obj2.py, obj2.pz, obj2.rx, obj2.ry, obj2.rz, obj2.sx, obj2.sy, obj2.sz, static_box2);
        return Islander::Collision::GJK_Swept(collider, static_box2, velocity, normal, iterations);
    }
    case ISLANDER_COLLISION_SHAPE_CAPSULE: {
        IslanderCollisionCapsule static_box2 = obj2.capsule;
        CreateTransformedCapsule(obj2.px, obj2.py, obj2.pz, obj2.rx, obj2.ry, obj2.rz, obj2.sx, obj2.sy, obj2.sz, static_box2);
        return Islander::Collision::GJK_Swept(collider, static_box2, velocity, normal, iterations);
    }
    case ISLANDER_COLLISION_SHAPE_ORIENTATED_BOX: {
        IslanderCollisionOBB static_box2 = obj2.obb;
        CreateTransformedOBB(obj2.px, obj2.py, obj2.pz, obj2.rx, obj2.ry, obj2.rz, obj2.sx, obj2.sy, obj2.sz, static_box2);
        return Islander::Collision::GJK_Swept(collider, static_box2, velocity, normal, iterations);
    }
    }
}

float Islander::Collision::DispatchCollisionSweptTest(IslanderCollisionBox& collider, IslanderCollisionDynamicMeshInfo& obj2, float* velocity, float* normal, int* iterations)
{
    switch (obj2.collisionShape)
    {
    case ISLANDER_COLLISION_SHAPE_BOX:
    {
        IslanderCollisionBox static_box2 = obj2.box;
        CreateTransformedAABB(obj2.px, obj2.py, obj2.pz, obj2.rx, obj2.ry, obj2.rz, obj2.sx, obj2.sy, obj2.sz, static_box2);
        return Islander::Collision::GJK_Swept(collider, static_box2, velocity, normal, iterations);
    }
    case ISLANDER_COLLISION_SHAPE_SPHERE: {
        IslanderCollisionSphere static_box2 = obj2.sphere;
        CreateTransformedSphere(obj2.px, obj2.py, obj2.pz, obj2.rx, obj2.ry, obj2.rz, obj2.sx, obj2.sy, obj2.sz, static_box2);
        return Islander::Collision::GJK_Swept(collider, static_box2, velocity, normal, iterations);
    }
    case ISLANDER_COLLISION_SHAPE_CAPSULE: {
        IslanderCollisionCapsule static_box2 = obj2.capsule;
        CreateTransformedCapsule(obj2.px, obj2.py, obj2.pz, obj2.rx, obj2.ry, obj2.rz, obj2.sx, obj2.sy, obj2.sz, static_box2);
        return Islander::Collision::GJK_Swept(collider, static_box2, velocity, normal, iterations);
    }
    case ISLANDER_COLLISION_SHAPE_ORIENTATED_BOX: {
        IslanderCollisionOBB static_box2 = obj2.obb;
        CreateTransformedOBB(obj2.px, obj2.py, obj2.pz, obj2.rx, obj2.ry, obj2.rz, obj2.sx, obj2.sy, obj2.sz, static_box2);
        return Islander::Collision::GJK_Swept(collider, static_box2, velocity, normal, iterations);
    }
    }
}

float Islander::Collision::DispatchCollisionSweptTest(IslanderCollisionOBB& collider, IslanderCollisionDynamicMeshInfo& obj2, float* velocity, float* normal, int* iterations)
{
    switch (obj2.collisionShape)
    {
    case ISLANDER_COLLISION_SHAPE_BOX:
    {
        IslanderCollisionBox static_box2 = obj2.box;
        CreateTransformedAABB(obj2.px, obj2.py, obj2.pz, obj2.rx, obj2.ry, obj2.rz, obj2.sx, obj2.sy, obj2.sz, static_box2);
        return Islander::Collision::GJK_Swept(collider, static_box2, velocity, normal, iterations);
    }
    case ISLANDER_COLLISION_SHAPE_SPHERE: {
        IslanderCollisionSphere static_box2 = obj2.sphere;
        CreateTransformedSphere(obj2.px, obj2.py, obj2.pz, obj2.rx, obj2.ry, obj2.rz, obj2.sx, obj2.sy, obj2.sz, static_box2);
        return Islander::Collision::GJK_Swept(collider, static_box2, velocity, normal, iterations);
    }
    case ISLANDER_COLLISION_SHAPE_CAPSULE: {
        IslanderCollisionCapsule static_box2 = obj2.capsule;
        CreateTransformedCapsule(obj2.px, obj2.py, obj2.pz, obj2.rx, obj2.ry, obj2.rz, obj2.sx, obj2.sy, obj2.sz, static_box2);
        return Islander::Collision::GJK_Swept(collider, static_box2, velocity, normal, iterations);
    }
    case ISLANDER_COLLISION_SHAPE_ORIENTATED_BOX: {
        IslanderCollisionOBB static_box2 = obj2.obb;
        //CreateTransformedOBB(obj2.px, obj2.py, obj2.pz, obj2.rx, obj2.ry, obj2.rz, obj2.sx, obj2.sy, obj2.sz, static_box2);
        CalculateOBB(static_box2, obj2, (Islander::Collision::CollisionDynamicMesh*)obj2.parentMesh);
        return Islander::Collision::GJK_Swept(collider, static_box2, velocity, normal, iterations);
    }
    }
}

bool Islander::Collision::DispatchCollisionTest(IslanderCollisionCapsule& collider, IslanderCollisionDynamicMeshInfo& obj2, float* normal)
{
    switch (obj2.collisionShape)
    {
    case ISLANDER_COLLISION_SHAPE_BOX: {
        IslanderCollisionBox static_box2 = obj2.box;
        CreateTransformedAABB(obj2.px, obj2.py, obj2.pz, obj2.rx, obj2.ry, obj2.rz, obj2.sx, obj2.sy, obj2.sz, static_box2);
        return Islander::Collision::GJK(collider, static_box2, normal);
    }
    case ISLANDER_COLLISION_SHAPE_SPHERE: {
        IslanderCollisionSphere static_box2 = obj2.sphere;
        CreateTransformedSphere(obj2.px, obj2.py, obj2.pz, obj2.rx, obj2.ry, obj2.rz, obj2.sx, obj2.sy, obj2.sz, static_box2);
        return Islander::Collision::GJK(collider, static_box2, normal);
    }
    case ISLANDER_COLLISION_SHAPE_CAPSULE: {
        IslanderCollisionCapsule static_box2 = obj2.capsule;
        CreateTransformedCapsule(obj2.px, obj2.py, obj2.pz, obj2.rx, obj2.ry, obj2.rz, obj2.sx, obj2.sy, obj2.sz, static_box2);
        return Islander::Collision::GJK(collider, static_box2, normal);
    }
    case ISLANDER_COLLISION_SHAPE_ORIENTATED_BOX: {
        IslanderCollisionOBB static_box2 = obj2.obb;
        CreateTransformedOBB(obj2.px, obj2.py, obj2.pz, obj2.rx, obj2.ry, obj2.rz, obj2.sx, obj2.sy, obj2.sz, static_box2);
        return Islander::Collision::GJK(collider, static_box2, normal);
    }
    }
}

bool Islander::Collision::DispatchCollisionTest(IslanderCollisionSphere& collider, IslanderCollisionDynamicMeshInfo& obj2, float* normal)
{
    switch (obj2.collisionShape)
    {
    case ISLANDER_COLLISION_SHAPE_BOX: {
        IslanderCollisionBox static_box2 = obj2.box;
        CreateTransformedAABB(obj2.px, obj2.py, obj2.pz, obj2.rx, obj2.ry, obj2.rz, obj2.sx, obj2.sy, obj2.sz, static_box2);
        return Islander::Collision::GJK(collider, static_box2, normal);
    }
    case ISLANDER_COLLISION_SHAPE_SPHERE: {
        IslanderCollisionSphere static_box2 = obj2.sphere;
        CreateTransformedSphere(obj2.px, obj2.py, obj2.pz, obj2.rx, obj2.ry, obj2.rz, obj2.sx, obj2.sy, obj2.sz, static_box2);
        return Islander::Collision::GJK(collider, static_box2, normal);
    }
    case ISLANDER_COLLISION_SHAPE_CAPSULE: {
        IslanderCollisionCapsule static_box2 = obj2.capsule;
        CreateTransformedCapsule(obj2.px, obj2.py, obj2.pz, obj2.rx, obj2.ry, obj2.rz, obj2.sx, obj2.sy, obj2.sz, static_box2);
        return Islander::Collision::GJK(collider, static_box2, normal);
    }
    case ISLANDER_COLLISION_SHAPE_ORIENTATED_BOX: {
        IslanderCollisionOBB static_box2 = obj2.obb;
        CreateTransformedOBB(obj2.px, obj2.py, obj2.pz, obj2.rx, obj2.ry, obj2.rz, obj2.sx, obj2.sy, obj2.sz, static_box2);
        return Islander::Collision::GJK(collider, static_box2, normal);
    }
    }
}

bool Islander::Collision::DispatchCollisionTest(IslanderCollisionBox& collider, IslanderCollisionDynamicMeshInfo& obj2, float* normal)
{
    switch (obj2.collisionShape)
    {
    case ISLANDER_COLLISION_SHAPE_BOX:
    {
        IslanderCollisionBox static_box2 = obj2.box;
        CreateTransformedAABB(obj2.px, obj2.py, obj2.pz, obj2.rx, obj2.ry, obj2.rz, obj2.sx, obj2.sy, obj2.sz, static_box2);
        return Islander::Collision::CollisionBoxTest(collider, static_box2);
    }
    case ISLANDER_COLLISION_SHAPE_SPHERE: {
        IslanderCollisionSphere static_box2 = obj2.sphere;
        CreateTransformedSphere(obj2.px, obj2.py, obj2.pz, obj2.rx, obj2.ry, obj2.rz, obj2.sx, obj2.sy, obj2.sz, static_box2);
        return Islander::Collision::GJK(collider, static_box2, normal);
    }
    case ISLANDER_COLLISION_SHAPE_CAPSULE: {
        IslanderCollisionCapsule static_box2 = obj2.capsule;
        CreateTransformedCapsule(obj2.px, obj2.py, obj2.pz, obj2.rx, obj2.ry, obj2.rz, obj2.sx, obj2.sy, obj2.sz, static_box2);
        return Islander::Collision::GJK(collider, static_box2, normal);
    }
    case ISLANDER_COLLISION_SHAPE_ORIENTATED_BOX: {
        IslanderCollisionOBB static_box2 = obj2.obb;
        CreateTransformedOBB(obj2.px, obj2.py, obj2.pz, obj2.rx, obj2.ry, obj2.rz, obj2.sx, obj2.sy, obj2.sz, static_box2);
        return Islander::Collision::GJK(collider, static_box2, normal);
    }
    }
}

bool Islander::Collision::DispatchCollisionTest(IslanderCollisionOBB& collider, IslanderCollisionDynamicMeshInfo& obj2, float* normal)
{
    switch (obj2.collisionShape)
    {
    case ISLANDER_COLLISION_SHAPE_BOX:
    {
        IslanderCollisionBox static_box2 = obj2.box;
        CreateTransformedAABB(obj2.px, obj2.py, obj2.pz, obj2.rx, obj2.ry, obj2.rz, obj2.sx, obj2.sy, obj2.sz, static_box2);
        return Islander::Collision::GJK(collider, static_box2, normal);
    }
    case ISLANDER_COLLISION_SHAPE_SPHERE: {
        IslanderCollisionSphere static_box2 = obj2.sphere;
        CreateTransformedSphere(obj2.px, obj2.py, obj2.pz, obj2.rx, obj2.ry, obj2.rz, obj2.sx, obj2.sy, obj2.sz, static_box2);
        return Islander::Collision::GJK(collider, static_box2, normal);
    }
    case ISLANDER_COLLISION_SHAPE_CAPSULE: {
        IslanderCollisionCapsule static_box2 = obj2.capsule;
        CreateTransformedCapsule(obj2.px, obj2.py, obj2.pz, obj2.rx, obj2.ry, obj2.rz, obj2.sx, obj2.sy, obj2.sz, static_box2);
        return Islander::Collision::GJK(collider, static_box2, normal);
    }
    case ISLANDER_COLLISION_SHAPE_ORIENTATED_BOX: {
        IslanderCollisionOBB static_box2 = obj2.obb;
        //CreateTransformedOBB(obj2.px, obj2.py, obj2.pz, obj2.rx, obj2.ry, obj2.rz, obj2.sx, obj2.sy, obj2.sz, static_box2);
        CalculateOBB(static_box2, obj2, (Islander::Collision::CollisionDynamicMesh*)obj2.parentMesh);
        return Islander::Collision::GJK(collider, static_box2, normal);
    }
    }
}

bool Islander::Collision::IsAbove(const IslanderCollisionCapsule& obj, const IslanderCollisionDynamicMeshInfo& mesh)
{
    float y = 0;

    switch (mesh.collisionShape)
    {
    case ISLANDER_COLLISION_SHAPE_ORIENTATED_BOX: {
        IslanderCollisionOBB static_box2 = mesh.obb;
        CalculateOBB(static_box2, mesh, (Islander::Collision::CollisionDynamicMesh*)mesh.parentMesh);
        y = static_box2.verticies[1];
        for (int i = 4; i < 24; i += 3)
        {
            y = std::max(y, static_box2.verticies[i]);
        }
        break;
    }
    case ISLANDER_COLLISION_SHAPE_BOX: {
        IslanderCollisionBox static_box2 = mesh.box;
        CreateTransformedAABB(mesh.px, mesh.py, mesh.pz, mesh.rx, mesh.ry, mesh.rz, mesh.sx, mesh.sy, mesh.sz, static_box2);
        y = static_box2.maxy;
        break;
    }
    default:
        return false;
    }

    return obj.centreY - obj.height/2.0f - obj.radius > y;
}

bool Islander::Collision::IsAbove(const IslanderCollisionBox& obj, const IslanderCollisionDynamicMeshInfo& mesh)
{
    float y = 0;

    switch (mesh.collisionShape)
    {
    case ISLANDER_COLLISION_SHAPE_ORIENTATED_BOX: {
        IslanderCollisionOBB static_box2 = mesh.obb;
        CalculateOBB(static_box2, mesh, (Islander::Collision::CollisionDynamicMesh*)mesh.parentMesh);
        y = static_box2.verticies[1];
        for (int i = 4; i < 24; i += 3)
        {
            y = std::max(y, static_box2.verticies[i]);
        }
        break;
    }
    case ISLANDER_COLLISION_SHAPE_BOX: {
        IslanderCollisionBox static_box2 = mesh.box;
        CreateTransformedAABB(mesh.px, mesh.py, mesh.pz, mesh.rx, mesh.ry, mesh.rz, mesh.sx, mesh.sy, mesh.sz, static_box2);
        y = static_box2.maxy;
        break;
    }
    default:
        return false;
    }

    return obj.miny > y;
}

float Islander::Collision::StepUpToPlatform(IslanderCollisionCapsule& obj, IslanderCollisionDynamicMeshInfo& mesh)
{
    float y = 0;

    switch (mesh.collisionShape)
    {
    case ISLANDER_COLLISION_SHAPE_ORIENTATED_BOX: {
        IslanderCollisionOBB static_box2 = mesh.obb;
        CalculateOBB(static_box2, mesh, (Islander::Collision::CollisionDynamicMesh*)mesh.parentMesh);
        y = static_box2.verticies[1];
        for (int i = 4; i < 24; i += 3)
        {
            y = std::max(y, static_box2.verticies[i]);
        }
        break;
    }
    case ISLANDER_COLLISION_SHAPE_BOX: {
        IslanderCollisionBox static_box2 = mesh.box;
        CreateTransformedAABB(mesh.px, mesh.py, mesh.pz, mesh.rx, mesh.ry, mesh.rz, mesh.sx, mesh.sy, mesh.sz, static_box2);
        y = static_box2.maxy;
        break;
    }
    default:
        return false;
    }

    return y + obj.height / 2.0f + obj.radius + 0.01f;
}

void Islander::Collision::ClearColliders(Islander::Collision::CollisionDynamicMesh* mesh)
{
    /*for (int i = 0; i < COLLISION_SYSTEM_DATA_ITEMS; i++)
    {
        mesh->collider_static[i] = nullptr;
        mesh->collider_dynamic[i] = nullptr;
    }

    mesh->collider_dynamic_count = 0;
    mesh->collider_static_count = 0;*/

    memset(&mesh->colData, 0, sizeof(mesh->colData));
}
