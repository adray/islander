#include "EPA.h"

void Islander::Collision::CreatePlaneFromTriangle(EPATri& tri, std::vector<GJKVec3>& verts)
{
    float dir1[3];
    float dir2[3];

    Islander::Numerics::SubVec3(verts[tri.points[0]], verts[tri.points[1]], dir1);
    Islander::Numerics::SubVec3(verts[tri.points[1]], verts[tri.points[2]], dir2);

    Islander::Numerics::CreatePlane(&tri.p, verts[tri.points[0]], dir1, dir2);
}

float Islander::Collision::FindClosestFaceToOrigin(const std::vector<EPATri>& tris, float* normal, int* index)
{
    float minDist = std::numeric_limits<float>::max();
    for (int i = 0; i < tris.size(); i++)
    {
        float dist = abs(tris[i].p.d); // plane ax+by+cz+d=dist where x,y,z=0, dist=0 if lies on plane

        if (dist < minDist)
        {
            minDist = dist;
            normal[0] = tris[i].p.x;
            normal[1] = tris[i].p.y;
            normal[2] = tris[i].p.z;
            *index = i;
        }
    }

    return minDist;
}

void Islander::Collision::CreateEPATri(EPATri& tri, std::vector<GJKVec3>& verts, int i1, int i2, int i3)
{
    // Maintain consistant winding order
    // Test the normal is facing away from the origin

    float* p1 = verts[i1];
    float* p2 = verts[i2];
    float* p3 = verts[i3];

    float a[3];
    float b[3];
    Islander::Numerics::SubVec3(p2, p1, a);
    Islander::Numerics::SubVec3(p3, p1, b);

    float n[3];
    Islander::Numerics::CrossVec3(a, b, n);

    float d[3];
    Islander::Numerics::NegateVec3(p1, d);

    if (SameDirection(n, d))
    {
        // Normal facing the origin invert the winding order.
        tri.points[0] = i1;
        tri.points[1] = i3;
        tri.points[2] = i2;
    }
    else
    {
        // Normal was facing away from origin, keep winding order.
        tri.points[0] = i1;
        tri.points[1] = i2;
        tri.points[2] = i3;
    }

    CreatePlaneFromTriangle(tri, verts);
}

bool Islander::Collision::RayTriTest(std::vector<GJKVec3>& verts, EPATri& tri, float* o, float* dir, float* pt, float* t)
{
    // Real-time rendering Ray/Triangle intersection 22.8

    float e1[3];
    float e2[3];

    Islander::Numerics::SubVec3(verts[tri.points[1]], verts[tri.points[0]], e1);
    Islander::Numerics::SubVec3(verts[tri.points[2]], verts[tri.points[0]], e2);

    float q[3];
    Islander::Numerics::CrossVec3(dir, e2, q);

    float a = Islander::Numerics::Dot(e1[0], e1[1], e1[2], q[0], q[1], q[2]);

    const float epsilon = 0.00001f;
    if (a > -epsilon && a < epsilon)
    {
        return false;
    }

    float f = 1 / a;

    float s[3];
    Islander::Numerics::SubVec3(o, verts[tri.points[0]], s);
    float u = f * Islander::Numerics::Dot(s[0], s[1], s[2], q[0], q[1], q[2]);
    if (u < 0)
    {
        return false;
    }

    float r[3];
    Islander::Numerics::CrossVec3(s, e1, r);
    float v = f * Islander::Numerics::Dot(dir[0], dir[1], dir[2], r[0], r[1], r[2]);
    if (v < 0 || u + v > 1)
    {
        return false;
    }

    *t = f * Islander::Numerics::Dot(e2[0], e2[1], e2[2], r[0], r[1], r[2]);
    if (*t < 0)
    {
        return false;
    }

    pt[0] = o[0] + dir[0] * *t;
    pt[1] = o[1] + dir[1] * *t;
    pt[2] = o[2] + dir[2] * *t;

    return true;
}

void Islander::Collision::InsertEdge(std::vector<EPAEdge>& edges, int i1, int i2)
{
    // Search for edge (its opposite), if doesn't exist add, otherwise remove edge.

    for (int i = 0; i < edges.size(); i++)
    {
        auto edge = edges[i];
        if ((edge.points[0] == i1 && edge.points[1] == i2) ||
            (edge.points[0] == i2 && edge.points[1] == i1))
        {
            EraseSwap(edges, i);
            return;
        }

        assert(!(edge.points[0] == i1 && edge.points[1] == i2));
        assert(!(edge.points[0] == i2 && edge.points[1] == i1));
    }

    EPAEdge e;
    e.points[0] = i1;
    e.points[1] = i2;
    edges.push_back(e);
}
