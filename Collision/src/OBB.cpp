#include "OBB.h"
#include "Matrix.h"
#include <memory>

using namespace Islander::Collision;
using namespace Islander::Numerics;

void AddVertexToOBB(IslanderCollisionOBB& obb, int index, float x, float y, float z)
{
    obb.verticies[index * 3] = x;
    obb.verticies[index * 3 + 1] = y;
    obb.verticies[index * 3 + 2] = z;
}

void AddEdgeToOBB(IslanderCollisionOBB& obb, int index, int i0, int i1)
{
    obb.edges[index * 2] = i0;
    obb.edges[index * 2 + 1] = i1;
}

void CalculateOBBExtents(int* vertices, IslanderCollisionOBB& obb, float* pos)
{
    std::memset(pos, 0, sizeof(float) * 3);

    for (int i = 0; i < 4; i++)
    {
        float x = obb.verticies[vertices[i] * 3];
        float y = obb.verticies[vertices[i] * 3 + 1];
        float z = obb.verticies[vertices[i] * 3 + 2];

        pos[0] += x;
        pos[1] += y;
        pos[2] += z;
    }

    pos[0] /= 4.0f;
    pos[1] /= 4.0f;
    pos[2] /= 4.0f;
}

void Islander::Collision::ComputeOBB(IslanderCollisionOBB& obb, const Islander::Numerics::Matrix4x4& matrix, const float* min, const float* max)
{
    AddVertexToOBB(obb, 0, min[0], min[1], min[2]);
    AddVertexToOBB(obb, 1, max[0], min[1], min[2]);
    AddVertexToOBB(obb, 2, min[0], max[1], min[2]);
    AddVertexToOBB(obb, 3, min[0], min[1], max[2]);
    AddVertexToOBB(obb, 4, max[0], max[1], min[2]);
    AddVertexToOBB(obb, 5, max[0], min[1], max[2]);
    AddVertexToOBB(obb, 6, min[0], max[1], max[2]);
    AddVertexToOBB(obb, 7, max[0], max[1], max[2]);

    for (int i = 0; i < 24; i += 3)
    {
        Matrix4x4::TransformVector(matrix, &obb.verticies[i], &obb.verticies[i + 1], &obb.verticies[i + 2]);
    }

    ComputeOBBFromVertices(obb);
}

void Islander::Collision::ComputeOBBFromVertices(IslanderCollisionOBB& obb)
{
    AddEdgeToOBB(obb, 0, 0, 1);
    AddEdgeToOBB(obb, 1, 0, 2);
    AddEdgeToOBB(obb, 2, 0, 3);
    AddEdgeToOBB(obb, 3, 1, 4);
    AddEdgeToOBB(obb, 4, 1, 5);
    AddEdgeToOBB(obb, 5, 2, 4);
    AddEdgeToOBB(obb, 6, 2, 6);
    AddEdgeToOBB(obb, 7, 3, 5);
    AddEdgeToOBB(obb, 8, 3, 6);
    AddEdgeToOBB(obb, 9, 5, 7);
    AddEdgeToOBB(obb, 10, 6, 7);
    AddEdgeToOBB(obb, 11, 4, 7);

    // Centre point
    std::memset(obb.centre, 0, sizeof(obb.centre));
    for (int i = 0; i < 24; i += 3)
    {
        obb.centre[0] += obb.verticies[i];
        obb.centre[1] += obb.verticies[i + 1];
        obb.centre[2] += obb.verticies[i + 2];
    }
    obb.centre[0] /= 8.0f;
    obb.centre[1] /= 8.0f;
    obb.centre[2] /= 8.0f;

    // Planes
    int side1[4] = { 0, 1, 2, 4 }; // A
    int side2[4] = { 3, 5, 6, 7 }; // A
    int side3[4] = { 0, 2, 3, 6 }; // B
    int side4[4] = { 1, 4, 5, 7 }; // B
    int side5[4] = { 0, 1, 3, 5 }; // C
    int side6[4] = { 2, 4, 6, 7 }; // C

    CalculateOBBExtents(side1, obb, &obb.extents[0]);
    CalculateOBBExtents(side3, obb, &obb.extents[3]);
    CalculateOBBExtents(side5, obb, &obb.extents[6]);

    CalculateOBBExtents(side2, obb, &obb.extents[9]);
    CalculateOBBExtents(side4, obb, &obb.extents[12]);
    CalculateOBBExtents(side6, obb, &obb.extents[15]);
}
