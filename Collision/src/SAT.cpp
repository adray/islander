#include "SAT.h"

using namespace Islander::Collision;


void Islander::Collision::CreateConvexPolyhedra(const IslanderCollisionOBB& obb, ConvexPolyhedra& polyhedra)
{
    ConvexPolyhedra::Face* faces = polyhedra.faces;
    for (int i = 0; i < 6; i++)
    {
        float normal[3];
        Islander::Numerics::SubVec3(&obb.extents[i * 3], obb.centre, normal);
        Islander::Numerics::NormalizeVec3(normal);

        ConvexPolyhedra::Face* face = &faces[0];
        PopulateFace(face, normal, obb.extents[i * 3], obb.extents[i * 3 + 1], obb.extents[i * 3 + 2]);
    }

    ConvexPolyhedra::Edge* edges = polyhedra.edges;
    for (int i = 0; i < 12; i++)
    {
        PopulateEdge(&edges[i], &obb.verticies[obb.edges[i * 2] * 3], &obb.verticies[obb.edges[i * 2 + 1] * 3]);
    }

    for (int i = 0; i < 3 * 8; i++)
    {
        polyhedra.points[i] = obb.verticies[i];
    }

    polyhedra.numEdges = 12;
    polyhedra.numPoints = 24;
    polyhedra.numFaces = 6;
}

void Islander::Collision::CreateConvexPolyhedra(const IslanderCollisionBox& box, ConvexPolyhedra& polyhedra)
{
    const float midX = box.minx + (box.maxx - box.minx) / 2.0f;
    const float midY = box.miny + (box.maxy - box.miny) / 2.0f;
    const float midZ = box.minz + (box.maxz - box.minz) / 2.0f;

    float up[3] = { 0.0f, 1.0f, 0.0f };
    float down[3] = { 0.0f, -1.0f, 0.0f };
    float left[3] = { 1.0f, 0.0f, 0.0f };
    float right[3] = { -1.0f, 0.0f, 0.0f };
    float back[3] = { 0.0f, 0.0f, -1.0f };
    float front[3] = { 0.0f, 0.0f, 1.0f };

    ConvexPolyhedra::Face faces[6];
    PopulateFace(&faces[0], up, midX, box.maxy, midZ);
    PopulateFace(&faces[1], down, midX, box.miny, midZ);
    PopulateFace(&faces[2], left, box.maxx, midY, midZ);
    PopulateFace(&faces[3], right, box.minx, midY, midZ);
    PopulateFace(&faces[4], back, midX, midY, box.minz);
    PopulateFace(&faces[5], front, midX, midY, box.maxz);

    for (int i = 0; i < 6; i++)
    {
        polyhedra.faces[i] = faces[i];
    }

    float points[3 * 8] = {
        box.minx, box.miny, box.minz,
        box.maxx, box.miny, box.minz,
        box.minx, box.maxy, box.minz,
        box.minx, box.miny, box.maxz,
        box.minx, box.maxy, box.maxz,
        box.maxx, box.miny, box.maxz,
        box.maxx, box.maxy, box.minz,
        box.maxx, box.maxy, box.maxz
    };

    ConvexPolyhedra::Edge* edges = polyhedra.edges;
    PopulateEdge(&edges[0], &points[0], &points[1]);
    PopulateEdge(&edges[1], &points[0], &points[2]);
    PopulateEdge(&edges[2], &points[0], &points[3]);
    PopulateEdge(&edges[3], &points[1], &points[6]);
    PopulateEdge(&edges[4], &points[2], &points[6]);
    PopulateEdge(&edges[5], &points[3], &points[5]);
    PopulateEdge(&edges[6], &points[2], &points[4]);
    PopulateEdge(&edges[7], &points[4], &points[7]);
    PopulateEdge(&edges[8], &points[3], &points[4]);
    PopulateEdge(&edges[9], &points[5], &points[7]);
    PopulateEdge(&edges[10], &points[1], &points[5]);
    PopulateEdge(&edges[11], &points[6], &points[7]);

    for (int i = 0; i < 3 * 8; i++)
    {
        polyhedra.points[i] = points[i];
    }

    polyhedra.numEdges = 12;
    polyhedra.numFaces = 6;
    polyhedra.numPoints = 24;
}


void Islander::Collision::PopulateFace(ConvexPolyhedra::Face* face,
    float* normal,
    float px, float py, float pz)
{
    std::memcpy(face->normal, normal, sizeof(face->normal));

    face->point[0] = px;
    face->point[1] = py;
    face->point[2] = pz;
}

void Islander::Collision::PopulateEdge(ConvexPolyhedra::Edge* edge, const float* p, const float* q)
{
    Islander::Numerics::CopyVec3(p, edge->pos1);
    Islander::Numerics::CopyVec3(q, edge->pos2);

    Islander::Numerics::SubVec3(p, q, edge->dir);
    Islander::Numerics::Normalize(edge->dir[0], edge->dir[1], edge->dir[2], edge->dir[0], edge->dir[1], edge->dir[2]);
}

int Islander::Collision::WhichSide(const ConvexPolyhedra& p, const float* pt, const float* normal)
{
    int positive = 0;
    int negative = 0;

    for (int i = 0; i < p.numPoints; i += 3)
    {
        float pos[3] = {
            p.points[i], p.points[i + 1], p.points[i + 2]
        };

        float dir[3];
        Islander::Numerics::SubVec3(pos, pt, dir);
        float t = Islander::Numerics::DotVec3(normal, dir);
        if (t > 0)
        {
            positive++;
        }
        else if (t < 0)
        {
            negative++;
        }

        if (positive > 0 && negative > 0)
        {
            return 0;
        }
    }

    return positive > 0 ? 1 : -1;
}

struct TriSATData
{
    float edge1[3];
    float edge2[3];
    float edge3[3];
    float normal[3];
};

void GetTriangleSATData(const IslanderCollisionTriangle& tri, TriSATData& data)
{
    Islander::Numerics::SubVec3(tri.vertices[0], tri.vertices[1], data.edge1);
    Islander::Numerics::SubVec3(tri.vertices[1], tri.vertices[2], data.edge2);
    Islander::Numerics::SubVec3(tri.vertices[2], tri.vertices[0], data.edge3);

    Islander::Numerics::NormalizeVec3(data.edge1);
    Islander::Numerics::NormalizeVec3(data.edge2);
    Islander::Numerics::NormalizeVec3(data.edge3);

    Islander::Numerics::CrossVec3(data.edge1, data.edge2, data.normal);
}

bool SATOverlapTest(const ConvexPolyhedra polyhedra, const IslanderCollisionTriangle& tri, const float* normal, const float* pos)
{
    float minT1 = std::numeric_limits<float>::max(), maxT1 = std::numeric_limits<float>::lowest();

    for (int i = 0; i < polyhedra.numPoints; i+=3)
    {
        const float pt[3] = {
            polyhedra.points[i],
            polyhedra.points[i + 1],
            polyhedra.points[i + 2]
        };

        float dir[3];
        Islander::Numerics::SubVec3(pt, pos, dir);
        float t = Islander::Numerics::DotVec3(normal, dir);

        if (t < minT1) { minT1 = t; }
        if (t > maxT1) { maxT1 = t; }
    }

    float minT2 = std::numeric_limits<float>::max(), maxT2 = std::numeric_limits<float>::lowest();

    for (int i = 0; i < 3; i++)
    {
        float* pt = tri.vertices[i];

        float dir[3];
        Islander::Numerics::SubVec3(pt, pos, dir);
        float t = Islander::Numerics::DotVec3(normal, dir);

        if (t < minT2) { minT2 = t; }
        if (t > maxT2) { maxT2 = t; }
    }

    return maxT1 >= minT2 && minT1 <= maxT2;
}

bool IsEmpty(const float* vec)
{
    return (vec[0] == 0.0f && vec[1] == 0.0f && vec[2] == 0.0f);
}

bool Islander::Collision::SATConvexPolyhedraTest(const ConvexPolyhedra& polyhedra, const IslanderCollisionTriangle& tri)
{
    /*
    1) We need to do the 4 checks for the faces, 1 from the triangle and 3 from the box.
    2) Then we need to do 3x3 checks for each combination of edges.
    We don't need to repeat for edges/faces which are parallel as they can be rolled into one check.
    */

    TriSATData triData;
    GetTriangleSATData(tri, triData);

    // Test triangle face
    if (!SATOverlapTest(polyhedra, tri, triData.normal, tri.vertices[0]))
    {
        return false;
    }

    // Test box faces
    // TODO: only need to test 3 of these.
    for (int i = 0; i < polyhedra.numFaces; i++)
    {
        auto& face = polyhedra.faces[i];
        const float* p = face.point;
        if (!SATOverlapTest(polyhedra, tri, face.normal, p))
        {
            return false;
        }
    }

    for (int i = 0; i < polyhedra.numEdges; i++)
    {
        float normal[3];
        Islander::Numerics::CrossVec3(polyhedra.edges[i].dir, triData.edge1, normal);

        if (!IsEmpty(normal) && !SATOverlapTest(polyhedra, tri, normal, polyhedra.edges[i].pos1))
        {
            return false;
        }

        Islander::Numerics::CrossVec3(polyhedra.edges[i].dir, triData.edge2, normal);

        if (!IsEmpty(normal) && !SATOverlapTest(polyhedra, tri, normal, polyhedra.edges[i].pos1))
        {
            return false;
        }

        Islander::Numerics::CrossVec3(polyhedra.edges[i].dir, triData.edge3, normal);

        if (!IsEmpty(normal) && !SATOverlapTest(polyhedra, tri, normal, polyhedra.edges[i].pos1))
        {
            return false;
        }
    }

    return true;
}

bool Islander::Collision::ConvexPolyhedraTest(const ConvexPolyhedra& p1, const ConvexPolyhedra& p2)
{
    /*    
    1) First check if there is a line of separation from the any of the faces across both colliders.
    This is done by creating a line along the face normals and projecting each of the vertices on the opposing collider.
    2) The next check is the cross product of each combination of edges, one from each collider.
    This is because a plane runs along two direction vectors. So we are constructing a plane with this combination of these.
    The cross product of these gives the plane's normal, which is used as in the previous step.
    */

    for (int i = 0; i < p1.numFaces; i++)
    {
        auto& face = p1.faces[i];
        const float* p = face.point;
        if (WhichSide(p2, p, face.normal) > 0)
        {
            return false;
        }
    }

    for (int j = 0; j < p2.numFaces; j++)
    {
        auto& face = p2.faces[j];
        const float* p = face.point;
        if (WhichSide(p1, p, face.normal) > 0)
        {
            return false;
        }
    }

    for (int i = 0; i < p1.numEdges; i++)
    {
        for (int j = 0; j < p2.numEdges; j++)
        {
            float cross[3];
            Islander::Numerics::CrossVec3(p1.edges[i].dir, p2.edges[j].dir, cross);
            if (Islander::Numerics::LengthSqr(cross) != 0.0f)
            {
                int side1 = WhichSide(p1, p1.edges[i].pos1, cross);
                if (side1 == 0)
                {
                    continue;
                }

                int side2 = WhichSide(p2, p1.edges[i].pos1, cross);
                if (side2 == 0)
                {
                    continue;
                }

                if (side1 * side2 < 0)
                {
                    return false;
                }
            }
        }
    }

    return true;
}
