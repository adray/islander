#include "GJK.h"

using namespace Islander;
using namespace Islander::Collision;
using namespace Islander::Numerics;

bool Line(std::array<GJKVec3, 4>& points, int* numPoints, float* dir)
{
    float* ptA = points[0];
    float* ptB = points[1];

    float ab[3];
    float ao[3];

    SubVec3(ptB, ptA, ab);
    NegateVec3(ptA, ao);

    if (SameDirection(ab, ao))
    {
        CrossVec3(ab, ao, dir);
        CrossVec3(dir, ab, dir);
    }
    else
    {
        CopyVec3(ptA, points[0]);
        *numPoints = 1;
        CopyVec3(ao, dir);
    }

    return false;
}

bool Triangle(std::array<GJKVec3, 4>& points, int* numPoints, float* dir)
{
    float ptA[3]; 
    float ptB[3]; 
    float ptC[3]; 
    
    CopyVec3(points[0], ptA);
    CopyVec3(points[1], ptB);
    CopyVec3(points[2], ptC);

    float ab[3];
    float ac[3];
    float ao[3];

    SubVec3(ptB, ptA, ab);
    SubVec3(ptC, ptA, ac);
    NegateVec3(ptA, ao);

    float abc[3];
    CrossVec3(ab, ac, abc);

    float temp[3];
    CrossVec3(abc, ac, temp);

    if (SameDirection(temp, ao))
    {
        if (SameDirection(ac, ao))
        {
            CopyVec3(ptA, points[0]);
            CopyVec3(ptC, points[1]);
            *numPoints = 2;

            CrossVec3(ac, ao, temp);
            CrossVec3(temp, ac, dir);
        }
        else
        {
            CopyVec3(ptA, points[0]);
            CopyVec3(ptB, points[1]);
            *numPoints = 2;

            return Line(points, numPoints, dir);
        }
    }
    else
    {
        CrossVec3(ab, abc, temp);

        if (SameDirection(temp, ao))
        {
            CopyVec3(ptA, points[0]);
            CopyVec3(ptB, points[1]);
            *numPoints = 2;

            return Line(points, numPoints, dir);
        }
        else
        {
            if (SameDirection(abc, ao))
            {
                CopyVec3(abc, dir);
            }
            else
            {
                CopyVec3(ptA, points[0]);
                CopyVec3(ptC, points[1]);
                CopyVec3(ptB, points[2]);
                *numPoints = 3;

                CopyVec3(abc, dir);
                NegateVec3(dir, dir);
            }
        }
    }

    return false;
}

bool Tetrahedron(std::array<GJKVec3, 4>& points, int* numPoints, float* dir)
{
    float ptA[3];
    float ptB[3];
    float ptC[3];
    float ptD[3];

    CopyVec3(points[0], ptA);
    CopyVec3(points[1], ptB);
    CopyVec3(points[2], ptC);
    CopyVec3(points[3], ptD);

    float ab[3];
    float ac[3];
    float ad[3];
    float ao[3];

    SubVec3(ptB, ptA, ab);
    SubVec3(ptC, ptA, ac);
    SubVec3(ptD, ptA, ad);
    NegateVec3(ptA, ao);

    float abc[3];
    float acd[3];
    float adb[3];
    CrossVec3(ab, ac, abc);
    CrossVec3(ac, ad, acd);
    CrossVec3(ad, ab, adb);

    if (SameDirection(abc, ao))
    {
        CopyVec3(ptA, points[0]);
        CopyVec3(ptB, points[1]);
        CopyVec3(ptC, points[2]);
        *numPoints = 3;

        return Triangle(points, numPoints, dir);
    }

    if (SameDirection(acd, ao))
    {
        CopyVec3(ptA, points[0]);
        CopyVec3(ptC, points[1]);
        CopyVec3(ptD, points[2]);
        *numPoints = 3;

        return Triangle(points, numPoints, dir);
    }

    if (SameDirection(adb, ao))
    {
        CopyVec3(ptA, points[0]);
        CopyVec3(ptD, points[1]);
        CopyVec3(ptB, points[2]);
        *numPoints = 3;

        return Triangle(points, numPoints, dir);
    }

    return true;
}

bool Islander::Collision::NextSimplex(std::array<GJKVec3, 4>& points, int* numPoints, float* dir)
{
    switch (*numPoints)
    {
    case 2:
        return Line(points, numPoints, dir);
    case 3:
        return Triangle(points, numPoints, dir);
    case 4:
        return Tetrahedron(points, numPoints, dir);
    }

    return false;
}

