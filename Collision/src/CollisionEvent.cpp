#include "CollisionEvent.h"
#include <algorithm>

using namespace Islander::Collision;

void Islander::Collision::CollisionListInsertAtLowerBound(CollisionEventList& evList, float collisionTime, CollisionEvent& ev)
{
    auto& bound = std::lower_bound(evList.collisions.begin(), evList.collisions.end(), collisionTime, [](CollisionEvent& ev, float t) { return ev.collisionTime < t; });
    evList.collisions.insert(bound, ev);
}

int Islander::Collision::ResolveCollisions(CollisionEventList& events, float& collisionY, float& collisionTime, bool& stepUp, bool& collide, float* normal)
{
    int index = -1;

    stepUp = false;
    collide = false;
    collisionTime = std::numeric_limits<float>::infinity();
    collisionY = 0;

    int collisions = events.collisions.size();
    for (auto& ev : events.stepUps)
    {
        for (auto& c : events.collisions)
        {
            if (!c.dynamic && ev.mesh == c.mesh)
            {
                collisions--;
            }
        }
    }

    if (collisions == 0)
    {
        if (events.stepUps.size() > 0)
        {
            // then stepup occurs
            stepUp = true;
            collisionY = events.stepUps[0].collisionY;
        }
    }
    else
    {
        collide = true;

        for (int i = 0; i < events.collisions.size(); i++)
        {
            auto& ev = events.collisions[i];
            bool collision = false;
            if (!ev.dynamic && ev.mesh != nullptr)
            {
                collision = true;
            }
            else if (ev.dynamic && ev.dynMesh != nullptr)
            {
                collision = true;
            }

            if (collision)
            {
                collisionTime = ev.collisionTime;
                normal[0] = ev.normal[0];
                normal[1] = ev.normal[1];
                normal[2] = ev.normal[2];
                index = i;
                break;
            }
        }
    }

    return index;
}
