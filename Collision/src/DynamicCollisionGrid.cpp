#include "DynamicCollisionGrid.h"
#include "CollisionPrimitives.h"
#include <assert.h>

using namespace Islander::Collision;

bool GetCell(IslanderCollisionBox& meshbox, IslanderCollisionBox& box, int* minCell, int* maxCell)
{
    float cellSizeX = (box.maxx - box.minx) / COLLISION_DYNAMIC_GRID_CELL_SIZE;
    float cellSizeY = (box.maxy - box.miny) / COLLISION_DYNAMIC_GRID_CELL_SIZE;
    float cellSizeZ = (box.maxz - box.minz) / COLLISION_DYNAMIC_GRID_CELL_SIZE;

    minCell[0] = std::min(COLLISION_DYNAMIC_GRID_CELL_SIZE - 1, (int)((meshbox.minx - box.minx) / cellSizeX));
    maxCell[0] = std::min(COLLISION_DYNAMIC_GRID_CELL_SIZE - 1, (int)((meshbox.maxx - box.minx) / cellSizeX));

    minCell[1] = std::min(COLLISION_DYNAMIC_GRID_CELL_SIZE - 1, (int)((meshbox.miny - box.miny) / cellSizeY));
    maxCell[1] = std::min(COLLISION_DYNAMIC_GRID_CELL_SIZE - 1, (int)((meshbox.maxy - box.miny) / cellSizeY));

    minCell[2] = std::min(COLLISION_DYNAMIC_GRID_CELL_SIZE - 1, (int)((meshbox.minz - box.minz) / cellSizeZ));
    maxCell[2] = std::min(COLLISION_DYNAMIC_GRID_CELL_SIZE - 1, (int)((meshbox.maxz - box.minz) / cellSizeZ));

    assert(minCell[0] >= 0 && minCell[0] < COLLISION_DYNAMIC_GRID_CELL_SIZE);
    assert(maxCell[0] >= 0 && maxCell[0] < COLLISION_DYNAMIC_GRID_CELL_SIZE);
    assert(minCell[2] >= 0 && minCell[2] < COLLISION_DYNAMIC_GRID_CELL_SIZE);
    assert(maxCell[2] >= 0 && maxCell[2] < COLLISION_DYNAMIC_GRID_CELL_SIZE);

    if (minCell[0] < 0 || minCell[0] >= COLLISION_DYNAMIC_GRID_CELL_SIZE) { return false; }
    if (maxCell[0] < 0 && maxCell[0] >= COLLISION_DYNAMIC_GRID_CELL_SIZE) { return false; }
    if (minCell[2] < 0 && minCell[2] >= COLLISION_DYNAMIC_GRID_CELL_SIZE) { return false; }
    if (maxCell[2] < 0 && maxCell[2] >= COLLISION_DYNAMIC_GRID_CELL_SIZE) { return false; }

    return true;
}

void Islander::Collision::BuildDynamicGrid(DynamicCollisionGrid& grid, IslanderCollisionBox& box, CollisionDynamicMesh* meshes, int count, int group)
{
    if (grid.dirty)
    {
        grid.dirty = false;
        grid.box = box;

        Islander::Collision::ClearDynamicCollisionGrid(grid);

        for (int i = 0; i < count; i++)
        {
            if (meshes[i].active && ((group & meshes[i].info.group) == meshes[i].info.group))
            {
                Islander::Collision::AddItemDynamicGrid(grid, &meshes[i]);
            }
        }

        float cellSizeX = (box.maxx - box.minx) / COLLISION_DYNAMIC_GRID_CELL_SIZE;
        float cellSizeY = (box.maxy - box.miny) / COLLISION_DYNAMIC_GRID_CELL_SIZE;
        float cellSizeZ = (box.maxz - box.minz) / COLLISION_DYNAMIC_GRID_CELL_SIZE;

        for (int x = 0; x < COLLISION_DYNAMIC_GRID_CELL_SIZE; x++)
        {
            for (int y = 0; y < COLLISION_DYNAMIC_GRID_CELL_SIZE; y++)
            {
                for (int z = 0; z < COLLISION_DYNAMIC_GRID_CELL_SIZE; z++)
                {
                    int index = z * COLLISION_DYNAMIC_GRID_CELL_SIZE * COLLISION_DYNAMIC_GRID_CELL_SIZE + y * COLLISION_DYNAMIC_GRID_CELL_SIZE + x;
                    if (index >= 0 && index < grid.cells.size())
                    {
                        auto& cell = grid.cells[index];
                        cell.box.maxx = cellSizeX * (x + 1) + box.minx;
                        cell.box.maxy = cellSizeY * (y + 1) + box.miny;
                        cell.box.maxz = cellSizeZ * (z + 1) + box.minz;
                        cell.box.minx = cellSizeX * (x)+box.minx;
                        cell.box.miny = cellSizeY * (y)+box.miny;
                        cell.box.minz = cellSizeZ * (z)+box.minz;
                    }
                }
            }
        }
    }
}

void Islander::Collision::AddItemDynamicGrid(DynamicCollisionGrid& grid, Islander::Collision::CollisionDynamicMesh* mesh)
{
    IslanderCollisionBox meshbox;
    GetBoundingBox(mesh, meshbox);

    IslanderCollisionBox& box = grid.box;

    int minCell[3];
    int maxCell[3];
    GetCell(meshbox, box, minCell, maxCell);

    for (int z = minCell[2]; z < maxCell[2] + 1; z++)
    {
        for (int y = minCell[1]; y < maxCell[1] + 1; y++)
        {
            for (int x = minCell[0]; x < maxCell[0] + 1; x++)
            {
                int index = z * COLLISION_DYNAMIC_GRID_CELL_SIZE * COLLISION_DYNAMIC_GRID_CELL_SIZE + y * COLLISION_DYNAMIC_GRID_CELL_SIZE + x;
                if (index >= 0 && index < grid.cells.size())
                {
                    grid.cells[index].items.push_back(mesh);
                }
            }
        }
    }
}

int Islander::Collision::QueryDynamicGrid(DynamicCollisionGrid& grid, Islander::Collision::CollisionDynamicMesh* mesh, int max_count, Islander::Collision::CollisionDynamicMesh** hits)
{
    IslanderCollisionBox meshbox;
    GetBoundingBox(mesh, meshbox);

    IslanderCollisionBox& box = grid.box;

    int minCell[3];
    int maxCell[3];
    if (!GetCell(meshbox, box, minCell, maxCell)) { return 0; }

    int count = 0;

    for (int z = minCell[2]; z < maxCell[2] + 1; z++)
    {
        for (int y = minCell[1]; y < maxCell[1] + 1; y++)
        {
            for (int x = minCell[0]; x < maxCell[0] + 1; x++)
            {
                int index = z * COLLISION_DYNAMIC_GRID_CELL_SIZE * COLLISION_DYNAMIC_GRID_CELL_SIZE + y * COLLISION_DYNAMIC_GRID_CELL_SIZE + x;
                if (index >= 0 && index < grid.cells.size())
                {
                    for (auto& item : grid.cells[index].items)
                    {
                        if (count < max_count && !item->hit)
                        {
                            hits[count] = item;
                            count++;
                            item->hit = true;
                        }
                    }
                }
            }
        }
    }

    for (int i = 0; i < count; i++)
    {
        hits[i]->hit = false;
    }

    return count;
}

void Islander::Collision::ClearDynamicCollisionGrid(DynamicCollisionGrid& grid)
{
    for (auto& cell : grid.cells)
    {
        cell.items.clear();
    }
}
