#include "TestAssert.h"

TestSingleton* TestSingleton::instance = nullptr;

TestSingleton* TestSingleton::GetInstance()
{
    if (TestSingleton::instance == nullptr)
    {
        TestSingleton::instance = new TestSingleton();
    }
    return TestSingleton::instance;
}
