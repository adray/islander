#pragma once
#include <iostream>

struct TestSingleton
{
	int assertionFailures = 0;
	int failures = 0;
	static TestSingleton* GetInstance();
private:
	static TestSingleton* instance;
};

template<typename T>
static void _assert(T value, T expected, const char* msg)
{
	if (value != expected)
	{
		TestSingleton::GetInstance()->assertionFailures++;
		if (msg)
		{
			printf(msg);
		}
		else
		{
			printf("Expected %d but was %d", expected, value);
		}
		printf("\r\n");
	}
}

static void runTest(const char* name, void (func)(void))
{
	TestSingleton::GetInstance()->assertionFailures = 0;
	func();
	if (TestSingleton::GetInstance()->assertionFailures == 0)
	{
		printf("Test %s completed successfully\r\n", name);
	}
	else
	{
		printf("Test %s failed\r\n", name);
		TestSingleton::GetInstance()->failures++;
	}
}
