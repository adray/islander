#include "EPATest.h"
#include "EPA.h"

using namespace Islander::Collision;

void EPATest1()
{
    IslanderCollisionSphere s1;
    s1.centreX = 10;
    s1.centreY = 10;
    s1.centreZ = 10;
    s1.radius = 5;

    IslanderCollisionSphere s2;
    s2.centreX = 10;
    s2.centreY = 19;
    s2.centreZ = 10;
    s2.radius = 5;

    float normal[3];
    float depth;
    float pt1[3];
    float pt2[3];
    assert(GJK_EPA(s1, s2, normal, &depth, pt1, pt2));

}

void EPATest2()
{
    IslanderCollisionBox b1;
    b1.maxx = 10;
    b1.minx = 5;
    b1.maxy = 10;
    b1.miny = 5;
    b1.maxz = 10;
    b1.minz = 5;

    IslanderCollisionBox b2;
    b2.maxx = 9;
    b2.minx = 4;
    b2.maxy = 14;
    b2.miny = 9;
    b2.maxz = 11;
    b2.minz = 6;

    float normal[3];
    float depth;
    float pt1[3];
    float pt2[3];
    assert(GJK_EPA(b1, b2, normal, &depth, pt1, pt2));

}
