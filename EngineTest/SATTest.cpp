#include "SATTest.h"
#include "SAT.h"
#include "TestAssert.h"

void SATTest1()
{
    IslanderCollisionBox box1;
    IslanderCollisionBox box2;

    box1.minx = 1.0f;
    box1.maxx = 5.0f;
    box1.miny = 1.0f;
    box1.maxy = 5.0f;
    box1.minz = 1.0f;
    box1.maxz = 5.0f;

    box2.minx = 4.0f;
    box2.maxx = 8.0f;
    box2.miny = 1.0f;
    box2.maxy = 5.0f;
    box2.minz = 1.0f;
    box2.maxz = 5.0f;

    bool intersects = Islander::Collision::SATConvexPolyhedraTest(box1, box2);
    _assert(intersects, true, "Intersects SAT failed");
}

void SATTest2()
{
    IslanderCollisionBox box1;
    IslanderCollisionBox box2;

    box1.minx = 1.0f;
    box1.maxx = 5.0f;
    box1.miny = 1.0f;
    box1.maxy = 5.0f;
    box1.minz = 1.0f;
    box1.maxz = 5.0f;

    box2.minx = 6.0f;
    box2.maxx = 10.0f;
    box2.miny = 1.0f;
    box2.maxy = 5.0f;
    box2.minz = 1.0f;
    box2.maxz = 5.0f;

    bool intersects = Islander::Collision::SATConvexPolyhedraTest(box1, box2);
    _assert(intersects, false, "Intersects SAT failed");
}

void SATBoxTriTest1()
{
    IslanderCollisionBox box;
    IslanderCollisionTriangle tri;

    box.minx = 1.0f;
    box.maxx = 5.0f;
    box.miny = 1.0f;
    box.maxy = 5.0f;
    box.minz = 1.0f;
    box.maxz = 5.0f;

    float v1[3] = { -0.5f, 2.0f, 2.0f };
    float v2[3] = { 5.5f, 2.0f, 2.0f };
    float v3[3] = { 3.0f, 6.0f, 2.0f };

    tri.vertices[0] = v1;
    tri.vertices[1] = v2;
    tri.vertices[2] = v3;

    bool intersects = Islander::Collision::SATConvexPolyhedraTest(box, tri);
    _assert(intersects, true, "Intersects SAT failed");
}

void SATBoxTriTest2()
{
    IslanderCollisionBox box;
    IslanderCollisionTriangle tri;

    box.minx = 1.0f;
    box.maxx = 5.0f;
    box.miny = 1.0f;
    box.maxy = 5.0f;
    box.minz = 1.0f;
    box.maxz = 5.0f;

    float v1[3] = { -0.5f, 6.0f, 2.0f };
    float v2[3] = { 5.5f, 6.0f, 2.0f };
    float v3[3] = { 3.0f, 8.0f, 2.0f };

    tri.vertices[0] = v1;
    tri.vertices[1] = v2;
    tri.vertices[2] = v3;

    bool intersects = Islander::Collision::SATConvexPolyhedraTest(box, tri);
    _assert(intersects, false, "Intersects SAT failed");
}

void SATBoxTriTest3()
{
    IslanderCollisionBox box;
    IslanderCollisionTriangle tri;

    box.minx = 1.0f;
    box.maxx = 5.0f;
    box.miny = 1.0f;
    box.maxy = 5.0f;
    box.minz = 1.0f;
    box.maxz = 5.0f;

    float v1[3] = { -0.5f, -6.0f, 2.0f };
    float v2[3] = { 5.5f, -6.0f, 2.0f };
    float v3[3] = { 3.0f, -8.0f, 2.0f };

    tri.vertices[0] = v1;
    tri.vertices[1] = v2;
    tri.vertices[2] = v3;

    bool intersects = Islander::Collision::SATConvexPolyhedraTest(box, tri);
    _assert(intersects, false, "Intersects SAT failed");
}

void SATBoxTriTest4() // tri inside the box
{
    IslanderCollisionBox box;
    IslanderCollisionTriangle tri;

    box.minx = 1.0f;
    box.maxx = 5.0f;
    box.miny = 1.0f;
    box.maxy = 5.0f;
    box.minz = 1.0f;
    box.maxz = 5.0f;

    float v1[3] = { 1.5f, 4.0f, 1.5f };
    float v2[3] = { 2.5f, 3.0f, 1.5f };
    float v3[3] = { 3.5f, 4.0f, 1.5f };

    tri.vertices[0] = v1;
    tri.vertices[1] = v2;
    tri.vertices[2] = v3;

    bool intersects = Islander::Collision::SATConvexPolyhedraTest(box, tri);
    _assert(intersects, true, "Intersects SAT failed");
}

void SATBoxTriTest5()
{
    IslanderCollisionBox box;
    IslanderCollisionTriangle tri;

    box.minx = 1.0f;
    box.maxx = 5.0f;
    box.miny = 1.0f;
    box.maxy = 5.0f;
    box.minz = 1.0f;
    box.maxz = 5.0f;

    float v1[3] = { 6.5f, 4.0f, -2.5f };
    float v2[3] = { 6.5f, 3.0f, 0.0f };
    float v3[3] = { 6.5f, 4.0f, 2.5f };

    tri.vertices[0] = v1;
    tri.vertices[1] = v2;
    tri.vertices[2] = v3;

    bool intersects = Islander::Collision::SATConvexPolyhedraTest(box, tri);
    _assert(intersects, false, "Intersects SAT failed");
}

//void SATOBBTriTest1()
//{
//    IslanderCollisionOBB obb;
//    IslanderCollisionTriangle tri;
//
//    float verts[24] = {
//
//    };
//
//    box.minx = 1.0f;
//    box.maxx = 5.0f;
//    box.miny = 1.0f;
//    box.maxy = 5.0f;
//    box.minz = 1.0f;
//    box.maxz = 5.0f;
//
//    float v1[3] = { 6.5f, 4.0f, -2.5f };
//    float v2[3] = { 6.5f, 3.0f, 0.0f };
//    float v3[3] = { 6.5f, 4.0f, 2.5f };
//
//    tri.vertices[0] = v1;
//    tri.vertices[1] = v2;
//    tri.vertices[2] = v3;
//
//    bool intersects = Islander::Collision::SATConvexPolyhedraTest(box, tri);
//    _assert(intersects, false, "Intersects SAT failed");
//}

void runSATTestSuite()
{
    runTest("SATTest1", SATTest1);
    runTest("SATTest2", SATTest2);
    runTest("SATBoxTriTest1", SATBoxTriTest1);
    runTest("SATBoxTriTest2", SATBoxTriTest2);
    runTest("SATBoxTriTest3", SATBoxTriTest3);
    runTest("SATBoxTriTest4", SATBoxTriTest4);
    runTest("SATBoxTriTest5", SATBoxTriTest5);
}
