#include "RayCastTest.h"
#include "TestAssert.h"
#include "RayCast.h"
#include "NonRealisticPhysics.h"
#include "OBB.h"

using namespace Islander::Physics;
using namespace Islander::Collision;

void CreateCubeMesh(Islander::Model::PolygonMeshLibrary* lib, Islander::Model::PolygonModel* model)
{
    float data[] =
    {
        1, 1, 1, 0, 0, 0, 0, 0, 0,
        1, -1, 1, 0, 0, 0, 0, 0, 0,
        1, 1, -1, 0, 0, 0, 0, 0, 0,
        -1, 1, -1, 0, 0, 0, 0, 0, 0,
        1, -1, -1, 0, 0, 0, 0, 0, 0,
        -1, -1, 1, 0, 0, 0, 0, 0, 0,
        -1, 1, 1, 0, 0, 0, 0, 0, 0,
        -1, -1, -1, 0, 0, 0, 0, 0, 0,
    };

    int32_t indexData[] =
    {
        7, 3, 2,
        7, 2, 4,
        5, 6, 0,
        5, 0, 1,
        3, 6, 0,
        3, 0, 2,
        7, 5, 1,
        7, 1, 4,
        4, 2, 0,
        4, 2, 1,
        7, 3, 5,
        6, 3, 5
    };

    model->animations = nullptr;
    //model.numDiffuseTextures = 0;
    //model.numNormalTextures = 0;
    model->numMaterials = 0;
    model->numMeshes = 1;
    model->numTextureChannels = 0;
    model->meshes[0].transform2 = Islander::Numerics::Matrix4x4(Islander::Numerics::Matrix4x4Type::MATRIX_4X4_IDENTITY);
    model->meshes[0].materialId = 0;
    
    Islander::Model::AddPolyMeshData(lib, &model->meshes[0], data, indexData, 8, 36, 9 * sizeof(float), (Islander::Model::PolygonMeshMetadata)(0x4 /* Copy the mesh data */ | 0x2 /* Generate AABB */ | 0x1));
    AddPolygonModel((Islander::Model::PolygonMeshLibrary*)lib, model, (Islander::Model::PolygonMeshMetadata)(0x4 /* Copy the mesh data */ | 0x2 /* Generate AABB */ | 0x1));
}

NonRealisticPhysicsSystem* BuildCollisionSystem(Islander::Model::PolygonModel* model)
{
    NonRealisticPhysicsSystem* physics = CreateNonRealisticPhysicsSystem();

    float staticPos[3 * 4] = {
        -10.0f, 0.0f, -10.0f,
        -10.0f, 0.0f, 10.0f,
        10.0f, 0.0f, -10.0f,
        10.0f, 0.0f, 10.0f
    };

    for (int i = 0; i < 4; i++) {
        Islander::Collision::CollisionTransform tr;
        tr.px = staticPos[i * 3];
        tr.py = staticPos[i * 3 + 1];
        tr.pz = staticPos[i * 3 + 2];
        tr.rx = 0.0f;
        tr.ry = 0.0f;
        tr.rz = 0.0f;
        tr.sx = 1.0f;
        tr.sy = 1.0f;
        tr.sz = 1.0f;

        CollisionStaticMesh* staticMesh = nullptr;
        _assert(AddStaticMesh(physics, false, false, 0, model, tr, &staticMesh), true, "Failed to add static mesh");

        staticMesh->trigger = 0;
        staticMesh->box.minx = -0.5f;
        staticMesh->box.miny = -0.5f;
        staticMesh->box.minz = -0.5f;
        staticMesh->box.maxx = 0.5f;
        staticMesh->box.maxy = 0.5f;
        staticMesh->box.maxz = 0.5f;
    }

    return physics;
}

void RayCastTest1()
{
    auto library = Islander::Model::CreatePolyMeshLibrary();
    Islander::Model::PolygonModel* model = new Islander::Model::PolygonModel;
    CreateCubeMesh(library, model);

    auto system = BuildCollisionSystem(model);

    {
        float o[3] = { 0.0f, 0.0f, 0.0f };
        float dir[3] = { 1.0f, 0.0f, 1.0f };

        IslanderCollisionRayInfo info;
        RayCast(system, nullptr, o[0], o[1], o[2], dir[0], dir[1], dir[2], ISLANDER_COLLISION_GROUP_DEFAULT, &info);

        _assert(info.static_mesh != nullptr, true, "Ray hit expected");
    }

    {
        float o[3] = { 0.0f, 0.0f, 0.0f };
        float dir[3] = { -1.0f, 0.0f, 1.0f };

        IslanderCollisionRayInfo info;
        RayCast(system, nullptr, o[0], o[1], o[2], dir[0], dir[1], dir[2], ISLANDER_COLLISION_GROUP_DEFAULT, &info);

        _assert(info.static_mesh != nullptr, true, "Ray hit expected");
    }

    {
        float o[3] = { 0.0f, 0.0f, 0.0f };
        float dir[3] = { 1.0f, 0.0f, 1.0f };

        IslanderCollisionRayInfo info;
        RayCast(system, nullptr, o[0], o[1], o[2], dir[0], dir[1], dir[2], ISLANDER_COLLISION_GROUP_DEFAULT, &info);

        _assert(info.static_mesh != nullptr, true, "Ray hit expected");
    }

    {
        float o[3] = { 0.0f, 0.0f, 0.0f };
        float dir[3] = { 1.0f, 0.0f, -1.0f };

        IslanderCollisionRayInfo info;
        RayCast(system, nullptr, o[0], o[1], o[2], dir[0], dir[1], dir[2], ISLANDER_COLLISION_GROUP_DEFAULT, &info);

        _assert(info.static_mesh != nullptr, true, "Ray hit expected");
    }

    {
        float o[3] = { 0.0f, 0.0f, 10.0f };
        float dir[3] = { 1.0f, 0.0f, 0.0f };

        IslanderCollisionRayInfo info;
        RayCast(system, nullptr, o[0], o[1], o[2], dir[0], dir[1], dir[2], ISLANDER_COLLISION_GROUP_DEFAULT, &info);

        _assert(info.static_mesh != nullptr, true, "Ray hit expected");
    }

    delete model;
}

void RayBoxTest1()
{
    IslanderCollisionBox box;
    box.minx = 2.0f;
    box.maxx = 4.0f;
    box.miny = -1.0f;
    box.maxy = 1.0f;
    box.minz = -1.0f;
    box.maxz = 1.0f;

    float dist;
    bool hit = TestRayBox(0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, box, dist);
    _assert(hit, true, "Ray hit expected");
}

void RayBoxTest2()
{
    IslanderCollisionBox box;
    box.minx = 2.0f;
    box.maxx = 4.0f;
    box.miny = -1.0f;
    box.maxy = 1.0f;
    box.minz = -1.0f;
    box.maxz = 1.0f;

    float dist;
    bool hit = TestRayBox(0.0f, 2.0f, 0.0f, 1.0f, 0.0f, 0.0f, box, dist);
    _assert(hit, false, "Ray hit not expected");
}

void RayBoxTest3()
{
    IslanderCollisionBox box;
    box.minx = 2.0f;
    box.maxx = 4.0f;
    box.miny = -1.0f;
    box.maxy = 1.0f;
    box.minz = -1.0f;
    box.maxz = 1.0f;

    float dir[3] = { 0.5f, 0.5f, 0.0f };
    Islander::Numerics::NormalizeVec3(dir);

    float dist;
    bool hit = TestRayBox(-10.0f, 0.0f, 0.0f, dir[0], dir[1], dir[2], box, dist);
    _assert(hit, false, "Ray hit not expected");
}

void RayBoxTest4()
{
    IslanderCollisionBox box;
    box.minx = 2.0f;
    box.maxx = 4.0f;
    box.miny = -1.0f;
    box.maxy = 1.0f;
    box.minz = -1.0f;
    box.maxz = 1.0f;

    float dir[3] = { 0.5f, 0.5f, 0.0f };
    Islander::Numerics::NormalizeVec3(dir);

    float dist;
    bool hit = TestRayBox(3.0f, 0.0f, 0.0f, dir[0], dir[1], dir[2], box, dist);
    _assert(hit, true, "Ray hit expected");
}

void RayBoxTest5()
{
    IslanderCollisionBox box;
    box.minx = 2.0f;
    box.maxx = 4.0f;
    box.miny = -1.0f;
    box.maxy = 1.0f;
    box.minz = -1.0f;
    box.maxz = 1.0f;

    float dist;
    bool hit = TestRayBox(0.0f, 0.0f, 0.0f, -1.0f, 0.0f, 0.0f, box, dist);
    _assert(hit, false, "Ray hit not expected");
}

void RayOBBBoxTest1()
{
    Islander::Numerics::Matrix4x4 matrix;
    Islander::Numerics::Matrix4x4::CreateTransformationMatrix(
        0.0f, 0.0f, 0.0f,
        0.0f, 0.0f, 0.0f,
        1.0f, 1.0f, 1.0f,
        &matrix);
    float min[3] = { 2.0f, -1.0f, -1.0f };
    float max[3] = { 4.0f, 1.0f, 1.0f };
    IslanderCollisionOBB obb;
    ComputeOBB(obb, matrix, min, max);

    float dist;
    bool hit = TestRayBox(0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, obb, dist);
    _assert(hit, true, "Ray hit expected");
}

void RayOBBBoxTest2()
{
    Islander::Numerics::Matrix4x4 matrix;
    Islander::Numerics::Matrix4x4::CreateTransformationMatrix(
        0.0f, 0.0f, 0.0f,
        0.0f, 0.0f, 0.0f,
        1.0f, 1.0f, 1.0f,
        &matrix);
    float min[3] = { 2.0f, -1.0f, -1.0f };
    float max[3] = { 4.0f, 1.0f, 1.0f };
    IslanderCollisionOBB obb;
    ComputeOBB(obb, matrix, min, max);

    float dist;
    bool hit = TestRayBox(0.0f, 0.0f, 0.0f, -1.0f, 0.0f, 0.0f, obb, dist);
    _assert(hit, false, "Ray hit not expected");
}

void RayOBBBoxTest3()
{
    Islander::Numerics::Matrix4x4 matrix;
    Islander::Numerics::Matrix4x4::CreateTransformationMatrix(
        0.0f, 0.0f, 0.0f,
        0.0f, 0.0f, 0.0f,
        1.0f, 1.0f, 1.0f,
        &matrix);
    float min[3] = { 2.0f, -1.0f, -1.0f };
    float max[3] = { 4.0f, 1.0f, 1.0f };
    IslanderCollisionOBB obb;
    ComputeOBB(obb, matrix, min, max);

    float dist;
    bool hit = TestRayBox(10.0f, 0.0f, 0.0f, -1.0f, 0.0f, 0.0f, obb, dist);
    _assert(hit, true, "Ray hit expected");
}

void RayOBBBoxTest4()
{
    Islander::Numerics::Matrix4x4 matrix;
    Islander::Numerics::Matrix4x4::CreateTransformationMatrix(
        0.0f, 0.0f, 0.0f,
        0.0f, 0.0f, 0.0f,
        1.0f, 1.0f, 1.0f,
        &matrix);
    float min[3] = { 2.0f, -1.0f, -1.0f };
    float max[3] = { 4.0f, 1.0f, 1.0f };
    IslanderCollisionOBB obb;
    ComputeOBB(obb, matrix, min, max);

    float dir[3] = { 0.5f, 0.5f, 0.0f };
    Islander::Numerics::NormalizeVec3(dir);

    float dist;
    bool hit = TestRayBox(0.0f, 0.0f, 0.0f, dir[0], dir[1], dir[2], obb, dist);
    _assert(hit, false, "Ray hit expected");
}

void RayOBBBoxTest5()
{
    Islander::Numerics::Matrix4x4 matrix;
    Islander::Numerics::Matrix4x4::CreateTransformationMatrix(
        0.0f, 0.0f, 0.0f,
        0.0f, 0.0f, 0.0f,
        1.0f, 1.0f, 1.0f,
        &matrix);
    float min[3] = { 2.0f, -1.0f, -1.0f };
    float max[3] = { 4.0f, 1.0f, 1.0f };
    IslanderCollisionOBB obb;
    ComputeOBB(obb, matrix, min, max);

    float dist;
    bool hit = TestRayBox(0.0f, 0.0f, 6.0f, 1.0f, 0.0f, 0.0f, obb, dist);
    _assert(hit, false, "Ray hit not expected");
}

void RayOBBBoxTest6()
{
    Islander::Numerics::Matrix4x4 matrix;
    Islander::Numerics::Matrix4x4::CreateTransformationMatrix(
        0.0f, 0.0f, 0.0f,
        0.0f, 0.0f, 0.0f,
        1.0f, 1.0f, 1.0f,
        &matrix);
    float min[3] = { 2.0f, -1.0f, -1.0f };
    float max[3] = { 4.0f, 1.0f, 1.0f };
    IslanderCollisionOBB obb;
    ComputeOBB(obb, matrix, min, max);

    float dist;
    bool hit = TestRayBox(3.0f, 5.0f, 0.0f, 0.0f, -1.0f, 0.0f, obb, dist);
    _assert(hit, true, "Ray hit expected");
}

void runRayBoxTestSuite()
{
    runTest("RayBoxTest1", RayBoxTest1);
    runTest("RayBoxTest2", RayBoxTest2);
    runTest("RayBoxTest3", RayBoxTest3);
    runTest("RayBoxTest4", RayBoxTest4);
    runTest("RayBoxTest5", RayBoxTest5);

    runTest("RayOBBBoxTest1", RayOBBBoxTest1);
    runTest("RayOBBBoxTest2", RayOBBBoxTest2);
    runTest("RayOBBBoxTest3", RayOBBBoxTest3);
    runTest("RayOBBBoxTest4", RayOBBBoxTest4);
    runTest("RayOBBBoxTest5", RayOBBBoxTest5);
    runTest("RayOBBBoxTest6", RayOBBBoxTest6);
}
