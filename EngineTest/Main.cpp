#include "Main.h"
#include "Stopwatch.h"
#include "CollisionSystem.h"
#include "Vector.h"
#include "EPA.h"
#include "EPATest.h"
#include "TestAssert.h"
#include "RayCastTest.h"
#include "CollisionPrimitivesTest.h"
#include "SATTest.h"
#include "NonRealisticPhysics.h"
#include <iostream>
#include <fstream>

#undef assert

using namespace Islander::Collision;

float BilinearInterpolate_(float* m11, float* m12, float* m21, float* m22, float x, float y)
{
    float a = (m21[0] - x) / (m21[0] - m11[0]) * m11[1] + (x - m11[0]) / (m21[0] - m11[0]) * m21[1];
    float b = (m22[0] - x) / (m22[0] - m12[0]) * m12[1] + (x - m12[0]) / (m22[0] - m12[0]) * m22[1];

    float c = (m12[2] - y) / (m12[2] - m11[2]) * a + (y - m11[2]) / (m12[2] - m11[2]) * b;
    return c;
}

void BilinearInterpolateTest()
{
    float m11[3] = { 14, 91, 20 };
    float m21[3] = { 15, 210, 20 };
    float m12[3] = { 14, 162, 21 };
    float m22[3] = { 15, 95, 21 };

    float res = BilinearInterpolate_(m11, m12, m21, m22, 14.5f, 20.2f) - 146.1f;
    _assert(std::fabs(res) < 0.001f, true, "BilinearInterpolateTest failed");
}

void TwoDynamicBoxesTest()
{
	const float delta = 1 / 60.0f;

	auto sys = Islander::Physics::CreateNonRealisticPhysicsSystem();

	CollisionDynamicMesh* dm1;
	_assert(Islander::Physics::AddDynamicMesh(sys, false, &dm1), true, "TwoDynamicBoxesTest failed");
	std::memset(dm1, 0, sizeof(CollisionDynamicMesh));
	dm1->active = true;
	dm1->info.vy = -0.5f * delta;
	dm1->info.sx = 1.0f;
	dm1->info.sy = 1.0f;
	dm1->info.sz = 1.0f;
	dm1->info.box.maxx = 1.0f;
	dm1->info.box.maxy = 1.0f;
	dm1->info.box.maxz = 1.0f;
	dm1->info.box.minx = -1.0f;
	dm1->info.box.miny = -1.0f;
	dm1->info.box.minz = -1.0f;
	dm1->info.py = 3.5f;
	
	CollisionDynamicMesh* dm2;
	_assert(Islander::Physics::AddDynamicMesh(sys, false, &dm2), true, "TwoDynamicBoxesTest failed");
	std::memset(dm2, 0, sizeof(CollisionDynamicMesh));
	dm2->active = true;
	dm2->info.vy = -0.5f * delta;
	dm2->info.sx = 1.0f;
	dm2->info.sy = 1.0f;
	dm2->info.sz = 1.0f;
	dm2->info.box.maxx = 1.0f;
	dm2->info.box.maxy = 1.0f;
	dm2->info.box.maxz = 1.0f;
	dm2->info.box.minx = -1.0f;
	dm2->info.box.miny = -1.0f;
	dm2->info.box.minz = -1.0f;
	dm2->info.py = 1.0f;

	Islander::Model::PolygonModel* model = new Islander::Model::PolygonModel();
	model->numMeshes = 0;
	model->min[0] = -10.0f;
	model->min[1] = 0.0f;
	model->min[2] = -10.0f;
	model->max[0] = 10.0f;
	model->max[1] = 0.0f;
	model->max[2] = 10.0f;

	Islander::Collision::CollisionTransform transform1;
	transform1.px = 0.0f;
	transform1.py = 0.0f;
	transform1.pz = 0.0f;
	transform1.rx = 0.0f;
	transform1.ry = 0.0f;
	transform1.rz = 0.0f;
	transform1.sx = 1.0f;
	transform1.sy = 1.0f;
	transform1.sz = 1.0f;

	CollisionStaticMesh* sm1;
	_assert(Islander::Physics::AddStaticMesh(sys, false, false, 0, model, transform1, &sm1), true, "TwoDynamicBoxesTest failed");
	sm1->parentBox.minx = model->min[0];
	sm1->parentBox.miny = model->min[1];
	sm1->parentBox.minz = model->min[2];
	sm1->parentBox.maxx = model->max[0];
	sm1->parentBox.maxy = model->max[1];
	sm1->parentBox.maxz = model->max[2];
	sm1->box.minx = model->min[0];
	sm1->box.miny = model->min[1];
	sm1->box.minz = model->min[2];
	sm1->box.maxx = model->max[0];
	sm1->box.maxy = model->max[1];
	sm1->box.maxz = model->max[2];

	Islander::Collision::CollisionTransform transform2;
	transform2.px = 0.0f;
	transform2.py = 20.0f;
	transform2.pz = 0.0f;
	transform2.rx = 0.0f;
	transform2.ry = 0.0f;
	transform2.rz = 0.0f;
	transform2.sx = 1.0f;
	transform2.sy = 1.0f;
	transform2.sz = 1.0f;

	CollisionStaticMesh* sm2;
	_assert(Islander::Physics::AddStaticMesh(sys, false, false, 0, model, transform2, &sm2), true, "TwoDynamicBoxesTest failed");
	sm2->parentBox.minx = model->min[0];
	sm2->parentBox.miny = model->min[1];
	sm2->parentBox.minz = model->min[2];
	sm2->parentBox.maxx = model->max[0];
	sm2->parentBox.maxy = model->max[1];
	sm2->parentBox.maxz = model->max[2];
	sm2->box.minx = model->min[0];
	sm2->box.miny = model->min[1];
	sm2->box.minz = model->min[2];
	sm2->box.maxx = model->max[0];
	sm2->box.maxy = model->max[1];
	sm2->box.maxz = model->max[2];

	for (int i = 0; i < 150; i++)
	{
		Islander::Physics::RunNonRealisticPhysics(sys, 1 / 60.0f);

		_assert(dm1->info.py - 1 > dm2->info.py + 1, true, "TwoDynamicBoxesTest failed");
	}
	delete sys; // there is no destroy collision system?
}

bool CircleBoxHelper(float minx, float minz, float maxx, float maxz, float centreX, float centreZ, float radius)
{
	float min[3] = {
		minx, 0.0f, minz
	};
	float max[3] = {
		maxx, 0.0f, maxz
	};
	float centre[3] = {
		centreX, 0.0f, centreZ
	};

	return Islander::Model::Private::CircleBoxTest(min, max, centre, radius);
}

void CircleTests()
{
	_assert(CircleBoxHelper(2.0f, 2.0f, 4.0f, 4.0f, 1.5f, 1.5f, 5.0f), true, "CircleTests to the left 1");
	_assert(CircleBoxHelper(2.0f, 2.0f, 4.0f, 4.0f, 1.5f, 1.5f, .2f), false, "CircleTests to the left 2");
	_assert(CircleBoxHelper(2.0f, 2.0f, 4.0f, 4.0f, 5.5f, 5.5f, 3.0f), true, "CircleTests to the right 1");
	_assert(CircleBoxHelper(2.0f, 2.0f, 4.0f, 4.0f, 5.5f, 5.5f, 5.0f), true, "CircleTests to the right 2");
	_assert(CircleBoxHelper(2.0f, 2.0f, 4.0f, 4.0f, 5.5f, 5.5f, .2f), false, "CircleTests to the right 3");
	_assert(CircleBoxHelper(2.0f, 2.0f, 4.0f, 4.0f, 3.0f, 3.5f, .2f), true, "CircleTests within the box");
	_assert(CircleBoxHelper(2.0f, 2.0f, 4.0f, 4.0f, 3.0f, 3.5f, 10.0f), true, "CircleTests around the box");
	_assert(CircleBoxHelper(2.0f, 2.0f, 4.0f, 4.0f, 3.0f, 1.5f, 2.0f), true, "CircleTests above the box");
	_assert(CircleBoxHelper(2.0f, 2.0f, 4.0f, 4.0f, 3.0f, 1.5f, 0.2f), false, "CircleTests above the box");
	_assert(CircleBoxHelper(2.0f, 2.0f, 4.0f, 4.0f, 1.8f, 1.8f, 0.3f), true, "CircleTests on corner");
}

void allTests()
{

	//runTest("PhysicsTest3", PhysicsTest3);
	//runTest("PhysicsTest2", PhysicsTest2);
	//runTest("EPATest1", EPATest1);
	//runTest("EPATest2", EPATest2);

	runTest("TwoDynamicBoxesTest", TwoDynamicBoxesTest);

	runTest("RayTriangleSweepTest", RayTriangleSweepTest);
	runTest("RayTriangleSweepTest2", RayTriangleSweepTest2);
	runTest("RayTriangleSweepTest3", RayTriangleSweepTest3);
	runTest("RayTriangleSweepTest4", RayTriangleSweepTest4);
	runTest("RayTriangleSweepTest5", RayTriangleSweepTest5);

	runTest("RayCastTest1", RayCastTest1);
    runTest("BilinearTest", BilinearInterpolateTest);

	runTest("SphereTriangleCollisionTest1", SphereTriangleCollisionTest1);
	runTest("SphereTriangleCollisionTest2", SphereTriangleCollisionTest2);
	runTest("SphereTriangleCollisionTest3", SphereTriangleCollisionTest3);
	runTest("SphereTriangleCollisionTest4", SphereTriangleCollisionTest4);

	runTest("CapsuleTriangleCollisionTest1", CapsuleTriangleCollisionTest1);
	runTest("CapsuleTriangleCollisionTest2", CapsuleTriangleCollisionTest2);
	runTest("CapsuleTriangleCollisionTest3", CapsuleTriangleCollisionTest3);
	runTest("CapsuleTriangleCollisionTest4", CapsuleTriangleCollisionTest4);
	runTest("CapsuleTriangleCollisionTest5", CapsuleTriangleCollisionTest5);
	
	runTest("CircleTests", CircleTests);

	runSATTestSuite();
	runRayBoxTestSuite();
}

int main(const char** args, int c)
{
	TestSingleton::GetInstance()->failures = 0;

	Islander::Stopwatch stopwatch;

	stopwatch.Start();
	allTests();
	stopwatch.Stop();

	if (TestSingleton::GetInstance()->failures == 0)
	{
		printf("All tests passed\r\n");
	}
	else
	{
		printf("Failed tests\r\n");
	}

	printf("Time Elapsed: %dms\r\n", stopwatch.GetElapsedMilliseconds());
	return 0;
}

void RayTriangleSweepTest()
{
	IslanderCollisionBox box1;
	box1.maxx = 5;
	box1.maxy = 10;
	box1.maxz = 2;
	box1.minx = -5;
	box1.miny = 5;
	box1.minz = -2;

	IslanderCollisionSphere sphere1;
	sphere1.centreX = 0;
	sphere1.centreY = 13;
	sphere1.centreZ = 0;
	sphere1.radius = 1;

	float vel[3] = { 0, -10, 0 };

	float normal[3];
	float t = GJK_Swept(sphere1, box1, vel, normal);

	_assert(true, abs(0.2f - t) < 0.001f, "Error t unexpected value");
}

void RayTriangleSweepTest2()
{
	IslanderCollisionBox box1;
	box1.maxx = 5;
	box1.maxy = 10;
	box1.maxz = 2;
	box1.minx = -5;
	box1.miny = 5;
	box1.minz = -2;

	IslanderCollisionCapsule capsule1;
	capsule1.centreX = 0;
	capsule1.centreY = 13;
	capsule1.centreZ = 0;
	capsule1.radius = 1;
	capsule1.height = 1;

	float vel[3] = { 0, -10, 0 };

	float normal[3];
	float t = GJK_Swept(capsule1, box1, vel, normal);

	_assert(true, abs(0.15f - t) < 0.001f, "Error t unexpected value");
}

void RayTriangleSweepTest3()
{
	IslanderCollisionBox box1;
	box1.maxx = 5;
	box1.maxy = 10;
	box1.maxz = 2;
	box1.minx = -5;
	box1.miny = 5;
	box1.minz = -2;

	IslanderCollisionCapsule capsule1;
	capsule1.centreX = 0;
	capsule1.centreY = 13;
	capsule1.centreZ = 0;
	capsule1.radius = 2;
	capsule1.height = 1;

	float vel[3] = { 0, -10, 0 };

	float normal[3];
	float t = GJK_Swept(capsule1, box1, vel, normal);

	_assert(true, abs(0.05f - t) < 0.001f, "Error t unexpected value");
}

void AABBToOBB(IslanderCollisionOBB& obb, const IslanderCollisionBox& box)
{
	//float data[] =
	//{
	//	box.maxx, box.maxy, box.maxz,
	//	box.maxx, box.miny, box.maxz,
	//	box.maxx, box.maxy, box.minz,
	//	box.minx, box.maxy, box.minz,
	//	box.maxx, box.miny, box.minz,
	//	box.minx, box.miny, box.maxz,
	//	box.minx, box.maxy, box.maxz,
	//	box.minx, box.miny, box.minz,
	//};
	float data[] = {
		box.maxx, box.maxy, box.maxz,
		box.minx, box.maxy, box.maxz,
		box.maxx, box.miny, box.maxz,
		box.maxx, box.maxy, box.minz,
		box.maxx, box.miny, box.minz,
		box.minx, box.miny, box.maxz,
		box.minx, box.maxy, box.minz,
		box.minx, box.miny, box.minz
	};
	memcpy(obb.verticies, data, sizeof(data));
}

void RayTriangleSweepTest4()
{	
	IslanderCollisionBox box1;
	box1.maxx = 5;
	box1.maxy = 10;
	box1.maxz = 2;
	box1.minx = -5;
	box1.miny = 5;
	box1.minz = -2;

	IslanderCollisionOBB box2;
	AABBToOBB(box2, box1);

	IslanderCollisionCapsule capsule1;
	capsule1.centreX = 0;
	capsule1.centreY = 13;
	capsule1.centreZ = 0;
	capsule1.radius = 2;
	capsule1.height = 1;

	float vel[3] = { 0, -10, 0 };

	float normal[3];
	float t = GJK_Swept(capsule1, box2, vel, normal);

	_assert(true, abs(0.05f - t) < 0.001f, "Error t unexpected value");
}

void RayTriangleSweepTest5()
{
	IslanderCollisionBox box1;
	box1.maxx = 5;
	box1.maxy = 10;
	box1.maxz = 2;
	box1.minx = -5;
	box1.miny = 5;
	box1.minz = -2;

	IslanderCollisionOBB box2;
	AABBToOBB(box2, box1);

	IslanderCollisionCapsule capsule1;
	capsule1.centreX = 0;
	capsule1.centreY = 13;
	capsule1.centreZ = 0;
	capsule1.radius = 2;
	capsule1.height = 1;

	float vel[3] = { 0, -1, 0 };

	float normal[3];
	int pen_iterations;
	float t = GJK_Swept(capsule1, box2, vel, normal, &pen_iterations, RayTriangleSweep<IslanderCollisionCapsule, IslanderCollisionOBB>);

	_assert(true, abs(0.5f - t) < 0.001f, "Error t unexpected value");
}

struct ContactConstraint
{
	int index;
	int index2;
	float rA[3];
	float rB[3];
	float pA[3];
	float pB[3];
	float normal[3];
	float J[12];	// the jacobian vector of [-n, cross(-rA,n), n, cross(rB,n)]
	float V[12];	// the velocity vector of [V1, W1, V2, W2]
	float inverseMass1;
	float inverseMass2;
	float totalLambda;
	float penetration;
	Islander::Numerics::Matrix3x3 inverseInteria1;
	Islander::Numerics::Matrix3x3 inverseInteria2;
};

struct RigidBody
{
	float velocity[3];
	float mass;
	Islander::Numerics::Matrix3x3 inverseInteria;
};

float ComputeContactConstraintBias(ContactConstraint& c, float dt)
{
	// Baumguarte stabilization
	// b = - (beta / dt) * d
	//
	// (for contact constraint)
	// d = (Pb - Pa) dot n 
	// 

	const float beta = 0.2f;
	float d = c.penetration;
	float b = -beta / dt * d;
	return b;
}

float ComputeEffectiveMass(ContactConstraint& c)
{
	// Meff = 1 / (J * M * transpose(J))
	// Meff = 1.0f / ( 1/mass1) + (cross(-rA,n) * I1 * cross(-rA,n)) + (1/mass2) + (cross(rB,n) * I2 * cross(rB,n))

	float meff1[3];
	Islander::Numerics::Matrix3x3::Multiply(&c.J[3], c.inverseInteria1, meff1);
	float meff2[3];
	Islander::Numerics::Matrix3x3::Multiply(&c.J[9], c.inverseInteria2, meff2);

	float k = c.inverseMass1 + Islander::Numerics::DotVec3(meff1, &c.J[3]) + c.inverseMass2 + Islander::Numerics::DotVec3(meff2, &c.J[9]);
	return 1.0f / k;
}

float ComputeContactConstraintLagrangianMultiplier(ContactConstraint& c, float dt)
{
	float bias = ComputeContactConstraintBias(c, dt); // bias

	// lambda = - (J * V + bias) * Meff
	
	float JV = 0.0f;
	for (int i = 0; i < 12; i++)
	{
		JV += c.J[i] * c.V[i];
	}

	float Meff = ComputeEffectiveMass(c);
	float lambda = -(JV + bias) * Meff;

	float oldTotalLambda = c.totalLambda;
	c.totalLambda = std::max(0.0f, c.totalLambda + lambda);
	lambda = c.totalLambda - oldTotalLambda;

	return lambda;
}

#define MAX_BODY_COUNT 10
struct PhysicsState
{
	IslanderCollisionSphere s[MAX_BODY_COUNT];
	RigidBody b[MAX_BODY_COUNT];
	IslanderCollisionBox ground;
	Islander::Numerics::Matrix3x3 groundInverseInertia;
	int sphereCount;

	ContactConstraint constraints[MAX_BODY_COUNT*5];
	int constraintCount = 0;
};

void CreateInverseInertiaMatrixSphere(float mass, float radius, Islander::Numerics::Matrix3x3* matrix)
{
	float inertia = (2.0f / 5.0f) * mass * radius * radius;

	float inverseInertia = 1.0f / inertia;

	Islander::Numerics::Matrix3x3::CreateScalingMatrix(inverseInertia, inverseInertia, inverseInertia, matrix);
}

void CreateInverseInertiaMatrixBox(float mass, IslanderCollisionBox& box, Islander::Numerics::Matrix3x3* matrix)
{
	float w = box.maxx = box.minx;
	float h = box.maxy = box.miny;
	float d = box.maxz = box.minz;

	float inertiaX = (1.0f / 12.0f) * mass * (h*h + d*d);
	float inertiaY = (1.0f / 12.0f) * mass * (w*w + d*d);
	float inertiaZ = (1.0f / 12.0f) * mass * (w*w + h*h);

	Islander::Numerics::Matrix3x3::CreateScalingMatrix(1.0f / inertiaX, 1.0f / inertiaY, 1.0f / inertiaZ, matrix);
}

void CreateContactConstraint(PhysicsState& state, int j, int k, float* normal, float* pt1, float* pt2, float depth)
{
	IslanderCollisionSphere* s = state.s;
	IslanderCollisionBox& ground = state.ground;

	float groundPos[3] =
	{
		(ground.maxx - ground.minx) / 2 + ground.minx,
		(ground.maxy - ground.miny) / 2 + ground.miny,
		(ground.maxz - ground.minz) / 2 + ground.minz
	};

	auto& con = state.constraints[state.constraintCount++];
	con.index = j;
	con.index2 = k;
	con.inverseMass1 = 1.0f / state.b[j].mass;
	con.inverseMass2 = k == -1 ? 0.0f : 1.0f / state.b[k].mass;
	con.inverseInteria1 = state.b[j].inverseInteria;
	con.inverseInteria2 = k == -1 ? state.groundInverseInertia : state.b[k].inverseInteria;
	con.totalLambda = 0.0f;
	con.penetration = depth;

	Islander::Numerics::NegateVec3(normal, normal); // TODO: may need to invert the normal inside EPA
	Islander::Numerics::CopyVec3(normal, con.normal);

	Islander::Numerics::CopyVec3(pt1, con.pA);
	Islander::Numerics::CopyVec3(pt2, con.pB);

	con.rA[0] = pt1[0] - s[j].centreX;
	con.rA[1] = pt1[1] - s[j].centreY;
	con.rA[2] = pt1[2] - s[j].centreZ;

	con.rB[0] = pt2[0] - (k == -1 ? groundPos[0] : state.s[k].centreX);
	con.rB[1] = pt2[1] - (k == -1 ? groundPos[1] : state.s[k].centreY);
	con.rB[2] = pt2[2] - (k == -1 ? groundPos[2] : state.s[k].centreZ);

	// Calculate the jacobian
	con.J[0] = -normal[0];
	con.J[1] = -normal[1];
	con.J[2] = -normal[2];

	float negrA[3];
	Islander::Numerics::NegateVec3(con.rA, negrA);
	Islander::Numerics::CrossVec3(negrA, normal, &con.J[3]);

	con.J[6] = normal[0];
	con.J[7] = normal[1];
	con.J[8] = normal[2];

	Islander::Numerics::CrossVec3(con.rB, normal, &con.J[9]);
}

void TestCollision(PhysicsState& state, int j)
{
	IslanderCollisionSphere* s = state.s;
	RigidBody* b = state.b;
	IslanderCollisionBox& ground = state.ground;

	float normal[3]; float depth; float pt1[3]; float pt2[3];
	if (GJK_EPA(s[j], ground, normal, &depth, pt1, pt2))
	{
		CreateContactConstraint(state, j, -1, normal, pt1, pt2, depth);
	}

	for (int k = 0; k < MAX_BODY_COUNT; k++)
	{
		if (k == j)
		{
			continue;
		}

		float normal[3]; float depth; float pt1[3]; float pt2[3];
		if (GJK_EPA(s[j], s[k], normal, &depth, pt1, pt2))
		{
			CreateContactConstraint(state, j, k, normal, pt1, pt2, depth);
		}
	}
}

void RunPhysics(PhysicsState& state)
{
	IslanderCollisionSphere* s = state.s;
	RigidBody* b = state.b;
	IslanderCollisionBox& ground = state.ground;

	float gravity[3] = { 0.0f, -10.0f, 0.0f };

	const int frames = 240;
	const float delta = 1.0f / 30.0f;

	for (int i = 0; i < frames; i++)
	{
		// apply gravity
		for (int j = 0; j < state.sphereCount; j++)
		{
			b[j].velocity[0] += gravity[0] * delta;
			b[j].velocity[1] += gravity[1] * delta;
			b[j].velocity[2] += gravity[2] * delta;
		}

		for (int j = 0; j < state.sphereCount; j++)
		{
			TestCollision(state, j);
		}

		const int iterations = 10;
		for (int it = 0; it < iterations; it++)
		{
			for (int j = 0; j < state.constraintCount; j++)
			{
				auto& c = state.constraints[j];

				// Calculate the V vector?
				std::memset(c.V, 0, sizeof(c.V));
				c.V[0] = b[c.index].velocity[0];
				c.V[1] = b[c.index].velocity[1];
				c.V[2] = b[c.index].velocity[2];

				if (c.index2 != -1)
				{
					c.V[6] = b[c.index2].velocity[0];
					c.V[7] = b[c.index2].velocity[1];
					c.V[8] = b[c.index2].velocity[2];
				}

				float l = ComputeContactConstraintLagrangianMultiplier(c, delta);

				// dV = inverse(M) * transpose(J) * L

				float vx = c.J[0] * l * c.inverseMass1;
				float vy = c.J[1] * l * c.inverseMass1;
				float vz = c.J[2] * l * c.inverseMass1;

				b[c.index].velocity[0] += vx;
				b[c.index].velocity[1] += vy;
				b[c.index].velocity[2] += vz;

				float w[3];
				Islander::Numerics::Matrix3x3::Multiply(&c.J[3], c.inverseInteria1, w);
				w[0] *= l;
				w[1] *= l;
				w[2] *= l;

				if (c.index2 != -1)
				{
					b[c.index2].velocity[0] += c.J[6] * l * c.inverseMass2;
					b[c.index2].velocity[1] += c.J[7] * l * c.inverseMass2;
					b[c.index2].velocity[2] += c.J[8] * l * c.inverseMass2;
				}
			}
		}

		std::cout << "Frame: " << i << std::endl;
		for (int j = 0; j < state.sphereCount; j++)
		{
			float v[3] = {
				b[j].velocity[0] * delta,
				b[j].velocity[1] * delta,
				b[j].velocity[2] * delta
			};

			s[j].centreX += v[0];
			s[j].centreY += v[1];
			s[j].centreZ += v[2];

			std::cout << "[" << s[j].centreX << "," << s[j].centreY << "," << s[j].centreZ << "]" << std::endl;
			std::cout << "[" << b[j].velocity[0] << "," << b[j].velocity[1] << "," << b[j].velocity[2] << "]" << std::endl;
		}

		state.constraintCount = 0;
	}
}

void PhysicsTest2()
{
	PhysicsState state;

	const int spheres = MAX_BODY_COUNT;
	state.sphereCount = spheres;
	IslanderCollisionSphere* s = state.s;
	for (int i = 0; i < spheres; i++)
	{
		s[i].centreX = 10;
		s[i].centreY = 10;
		s[i].centreZ = 10  + i * 10;
		s[i].radius = 5;
	}

	RigidBody* b = state.b;
	for (int i = 0; i < spheres; i++)
	{
		b[i].velocity[0] = 0.0f;
		b[i].velocity[1] = 0.0f;
		b[i].velocity[2] = 0.0f;
		b[i].mass = 1.0f;
		CreateInverseInertiaMatrixSphere(b[i].mass, s[i].radius, &b[i].inverseInteria);
	}

	IslanderCollisionBox& ground = state.ground;
	ground.maxx = 20;
	ground.minx = 0;
	ground.maxy = 2;
	ground.miny = 0;
	ground.minz = 0;
	ground.maxz = 100;
	// Ground inverse would be all zeros (if it has assumed infinite mass)
	//CreateInverseInertiaMatrixBox(std::numeric_limits<float>::max(), ground, &state.groundInverseInertia);

	RunPhysics(state);
}

void PhysicsTest3()
{
	PhysicsState state;

	const int spheres = 2;
	state.sphereCount = spheres;
	IslanderCollisionSphere* s = state.s;
	for (int i = 0; i < spheres; i++)
	{
		s[i].centreX = 10;
		s[i].centreY = 10 + i * 10;
		s[i].centreZ = 10;
		s[i].radius = 5;
	}

	RigidBody* b = state.b;
	for (int i = 0; i < spheres; i++)
	{
		b[i].velocity[0] = 0.0f;
		b[i].velocity[1] = 0.0f;
		b[i].velocity[2] = 0.0f;
		b[i].mass = 1.0f;
		CreateInverseInertiaMatrixSphere(b[i].mass, s[i].radius, &b[i].inverseInteria);
	}

	IslanderCollisionBox& ground = state.ground;
	ground.maxx = 20;
	ground.minx = 0;
	ground.maxy = 2;
	ground.miny = 0;
	ground.minz = 0;
	ground.maxz = 100;
	// Ground inverse would be all zeros (if it has assumed infinite mass)
	//CreateInverseInertiaMatrixBox(std::numeric_limits<float>::max(), ground, &state.groundInverseInertia);

	RunPhysics(state);
}

