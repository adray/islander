#include "CollisionPrimitivesTest.h"
#include "CollisionPrimitives.h"
#include "TestAssert.h"

void SphereTriangleCollisionTest1()
{
    float centre[3] = {
        0.0f, 1.0f, 0.0f
    };

    float v1[3] = {
        0.0f, 5.5f, 1.0f
    };

    float v2[3] = {
        -2.0f, 5.5f, 1.0f
    };

    float v3[3] = {
        -2.0f, 5.5f, 0.0f
    };

    IslanderCollisionTriangle tri;
    tri.vertices[0] = v1;
    tri.vertices[1] = v2;
    tri.vertices[2] = v3;

    bool intersect = Islander::Collision::SphereTriangleCollision(centre, 5.0f, tri);
    _assert(intersect, true, "Expected collision");
}

void SphereTriangleCollisionTest2()
{
    float centre[3] = {
        0.0f, 1.0f, 0.0f
    };

    float v1[3] = {
        0.0f, -3.5f, 1.0f
    };

    float v2[3] = {
        -2.0f, -3.5f, 1.0f
    };

    float v3[3] = {
        -2.0f, -3.5f, 0.0f
    };

    IslanderCollisionTriangle tri;
    tri.vertices[0] = v1;
    tri.vertices[1] = v2;
    tri.vertices[2] = v3;

    bool intersect = Islander::Collision::SphereTriangleCollision(centre, 5.0f, tri);
    _assert(intersect, true, "Expected collision");
}

void SphereTriangleCollisionTest3()
{
    float centre[3] = {
        -1.5f, 0.0f, 0.5f
    };

    float v1[3] = {
        0.0f, -4.5f, 1.0f
    };

    float v2[3] = {
        -2.0f, -4.5f, 1.0f
    };

    float v3[3] = {
        -2.0f, -4.5f, 0.0f
    };

    IslanderCollisionTriangle tri;
    tri.vertices[0] = v1;
    tri.vertices[1] = v2;
    tri.vertices[2] = v3;

    bool intersect = Islander::Collision::SphereTriangleCollision(centre, 5.0f, tri);
    _assert(intersect, true, "Expected collision");
}

void SphereTriangleCollisionTest4()
{
    float centre[3] = {
        -1.5f, 0.0f, 0.5f
    };

    float v1[3] = {
        0.0f, -4.5f, 1.0f
    };

    float v2[3] = {
        -2.0f, -4.5f, 1.0f
    };

    float v3[3] = {
        -2.0f, -4.5f, 0.0f
    };

    IslanderCollisionTriangle tri;
    tri.vertices[0] = v1;
    tri.vertices[1] = v2;
    tri.vertices[2] = v3;

    bool intersect = Islander::Collision::SphereTriangleCollision(centre, 1.0f, tri);
    _assert(intersect, false, "Expected collision");
}

void CapsuleTriangleCollisionTest1()
{
    float v1[3] = {
        0.0f, 5.5f, 1.0f
    };

    float v2[3] = {
        -2.0f, 5.5f, 1.0f
    };

    float v3[3] = {
        -2.0f, 5.5f, 0.0f
    };

    IslanderCollisionTriangle tri;
    tri.vertices[0] = v1;
    tri.vertices[1] = v2;
    tri.vertices[2] = v3;

    IslanderCollisionCapsule capsule;
    capsule.centreX = 0.0f;
    capsule.centreY = 1.0f;
    capsule.centreZ = 0.0f;
    capsule.radius = 2.0f;
    capsule.height = 6.0f;

    bool intersect = Islander::Collision::CapsuleTriangleCollision(capsule, tri);
    _assert(intersect, true, "Expected collision");
}

void CapsuleTriangleCollisionTest2()
{
    float v1[3] = {
        0.0f, 5.5f, 1.0f
    };

    float v2[3] = {
        -2.0f, 5.5f, 1.0f
    };

    float v3[3] = {
        -2.0f, 5.5f, 0.0f
    };

    IslanderCollisionTriangle tri;
    tri.vertices[0] = v1;
    tri.vertices[1] = v2;
    tri.vertices[2] = v3;

    IslanderCollisionCapsule capsule;
    capsule.centreX = 0.0f;
    capsule.centreY = 1.0f;
    capsule.centreZ = 0.0f;
    capsule.radius = 2.0f;
    capsule.height = 3.0f;

    bool intersect = Islander::Collision::CapsuleTriangleCollision(capsule, tri);
    _assert(intersect, false, "Expected collision");
}

void CapsuleTriangleCollisionTest3()
{
    float v1[3] = {
        0.0f, 5.5f, 1.0f
    };

    float v2[3] = {
        0.0f, 3.5f, 1.0f
    };

    float v3[3] = {
        0.0f, 5.5f, 0.0f
    };

    IslanderCollisionTriangle tri;
    tri.vertices[0] = v1;
    tri.vertices[1] = v2;
    tri.vertices[2] = v3;

    IslanderCollisionCapsule capsule;
    capsule.centreX = 0.0f;
    capsule.centreY = 1.0f;
    capsule.centreZ = 0.0f;
    capsule.radius = 2.0f;
    capsule.height = 3.0f;

    bool intersect = Islander::Collision::CapsuleTriangleCollision(capsule, tri);
    _assert(intersect, true, "Expected collision");
}

void CapsuleTriangleCollisionTest4()
{
    float v1[3] = {
        0.0f, -5.5f, 1.0f
    };

    float v2[3] = {
        0.0f, -3.5f, 1.0f
    };

    float v3[3] = {
        0.0f, -5.5f, 0.0f
    };

    IslanderCollisionTriangle tri;
    tri.vertices[0] = v1;
    tri.vertices[1] = v2;
    tri.vertices[2] = v3;

    IslanderCollisionCapsule capsule;
    capsule.centreX = 0.0f;
    capsule.centreY = -1.0f;
    capsule.centreZ = 0.0f;
    capsule.radius = 2.0f;
    capsule.height = 3.0f;

    bool intersect = Islander::Collision::CapsuleTriangleCollision(capsule, tri);
    _assert(intersect, true, "Expected collision");
}

void CapsuleTriangleCollisionTest5()
{
    float v1[3] = {
        -1.0f, 15.5f, 5.0f
    };

    float v2[3] = {
        -1.0f, -10.5f, 5.0f
    };

    float v3[3] = {
        -1.0f, -10.5f, -5.0f
    };

    IslanderCollisionTriangle tri;
    tri.vertices[0] = v1;
    tri.vertices[1] = v2;
    tri.vertices[2] = v3;

    IslanderCollisionCapsule capsule;
    capsule.centreX = 0.0f;
    capsule.centreY = 5.0f;
    capsule.centreZ = 0.0f;
    capsule.radius = 2.0f;
    capsule.height = 3.0f;

    bool intersect = Islander::Collision::CapsuleTriangleCollision(capsule, tri);
    _assert(intersect, true, "Expected collision");
}


