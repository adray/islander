#pragma once
#include <stdint.h>

#define ISLANDER_RENDER_TARGET_TYPE_DIFFUSE 0
#define ISLANDER_RENDER_TARGET_TYPE_DEPTH 1
#define ISLANDER_RENDER_TARGET_TYPE_OBJECT_ID 2
#define ISLANDER_TEXTURE_DATA_MAX_COUNT 128
#define ISLANDER_DISPLAY_NAME_LEN 128
#define ISLANDER_MAX_DISPLAYS 8
#define ISLANDER_MAX_RESOLUTIONS 64
#define ISLANDER_POLYDATA_TEXTURE_DIFFUSE 0
#define ISLANDER_POLYDATA_TEXTURE_NORMAL 1
#define ISLANDER_SUBRESOURCE_GENERATION_9SLICE 1

#define ISLANDER_EFFECT_TEXTURE_DIFFUSE 0
#define ISLANDER_EFFECT_TEXTURE_NORMAL 1
#define ISLANDER_EFFECT_MODEL 2

#define ISLANDER_RENDERER_TYPE_GL 0
#define ISLANDER_RENDERER_TYPE_D3D11 1

#define ISLANDER_SEMANTIC_FLOAT 0
#define ISLANDER_SEMANTIC_FLOAT2 1
#define ISLANDER_SEMANTIC_FLOAT3 2
#define ISLANDER_SEMANTIC_FLOAT4 3
#define ISLANDER_SEMANTIC_INT 4
#define ISLANDER_SEMANTIC_INT2 5
#define ISLANDER_SEMANTIC_INT3 6
#define ISLANDER_SEMANTIC_INT4 7
#define ISLANDER_SEMANTIC_COUNT 8 // number of semantics

#define ISLANDER_GAMEPAD_A 0
#define ISLANDER_GAMEPAD_B 1
#define ISLANDER_GAMEPAD_X 2
#define ISLANDER_GAMEPAD_Y 3
#define ISLANDER_GAMEPAD_START 4
#define ISLANDER_GAMEPAD_SELECT 5
#define ISLANDER_GAMEPAD_LEFTBUTTON 6
#define ISLANDER_GAMEPAD_RIGHTBUTTON 7
#define ISLANDER_GAMEPAD_DPADLEFT 8
#define ISLANDER_GAMEPAD_DPADRIGHT 9
#define ISLANDER_GAMEPAD_DPADUP 10
#define ISLANDER_GAMEPAD_DPADDOWN 11
#define ISLANDER_GAMEPAD_LEFTTHUMB 12
#define ISLANDER_GAMEPAD_RIGHTTHUMB 13
#define ISLANDER_GAMEPAD_BUTTON_COUNT 14
#define ISLANDER_GAMEPAD_COUNT 4

#define ISLANDER_RENDERABLE_MATERIAL_SLOT_NONE 0
#define ISLANDER_RENDERABLE_MATERIAL_SLOT_CUSTOM 1
#define ISLANDER_RENDERABLE_MATERIAL_SLOT_DIFFUSE 2
#define ISLANDER_RENDERABLE_MATERIAL_SLOT_NORMAL 3

#define ISLANDER_DEFINE_MAX_COUNT 8
#define ISLANDER_DEFINE_NAME_LEN 128

#define ISLANDER_MAX_RENDER_TARGETS 4

#define ISLANDER_RENDER_FLAGS_NONE 0x0
#define ISLANDER_RENDER_FLAGS_CLEAR 0x1
#define ISLANDER_RENDER_FLAGS_CLEAR_TRANSPARENT 0x2
#define ISLANDER_RENDER_FLAGS_BLEND_ADD 0x4
#define ISLANDER_RENDER_FLAGS_FRONTFACE_CULL 0x8
#define ISLANDER_RENDER_FLAGS_RETAIN_DEPTH 0x10
#define ISLANDER_RENDER_FLAGS_MULTI_RENDER_TARGET 0x20
#define ISLANDER_RENDER_FLAGS_NO_CULL 0x40
#define ISLANDER_RENDER_FLAGS_NO_DEPTH_WRITE 0x80

#define ISLANDER_TEXTURE_TYPE_NONE 0
#define ISLANDER_TEXTURE_TYPE_GREYSCALE 1

#define ISLANDER_SAMPLER_TYPE_CLAMP 0
#define ISLANDER_SAMPLER_TYPE_WRAP 1

#define ISLANDER_WINDOW_STYLE_BORDERLESS 0
#define ISLANDER_WINDOW_STYLE_BORDER 1

extern "C"
{
    typedef void* ISLANDER_DEVICE;
    typedef void* ISLANDER_BINDINGS;
    typedef void* ISLANDER_WINDOW;
    typedef void* ISLANDER_CURSOR;
    typedef void* ISLANDER_CAMERA;
    typedef void* ISLANDER_CAMERA3D;
    typedef void* ISLANDER_PASS_LIST;
    typedef int ISLANDER_SUBRESOURCE_GENERATION;
    typedef void* ISLANDER_SUBRESOURCE_DATA;
    typedef char* ISLANDER_TEXTURE_DATA;
    typedef void* ISLANDER_RESOURCE;
    typedef void* ISLANDER_RESOURCE_FILE;
    typedef void* ISLANDER_FILE_LIBRARY;
    typedef void* ISLANDER_GAMEPAD_DEVICE;
    typedef void* ISLANDER_GAMEPAD_STATE;
    typedef void* ISLANDER_MATRIX;
    typedef void* ISLANDER_ALLOCATOR(int);
    typedef void* ISLANDER_POLYGON_LIBRARY;
    typedef void* ISLANDER_PARTICLE_SYSTEM;

    struct IslanderGamePadState
    {
        bool connected;
        float RX;
        float RY;
        float LX;
        float LY;
        float LeftTrigger;
        float RightTrigger;
        bool buttons[(unsigned int)ISLANDER_GAMEPAD_BUTTON_COUNT];
    };

    struct IslanderFontDescription
    {
        char* name;
        char* filedef;
        int img;
    };

    struct IslanderShaderDefine
    {
        char name[ISLANDER_DEFINE_NAME_LEN];
        char value[ISLANDER_DEFINE_NAME_LEN];
    };

    struct IslanderShaderDefineList
    {
        int numDefines;
        IslanderShaderDefine defines[ISLANDER_DEFINE_MAX_COUNT];
    };

    struct IslanderPassConfig
    {
        union
        {
            int _renderTarget;
            int _multiRenderTarget[ISLANDER_MAX_RENDER_TARGETS];
        };
        int _renderTargetCount;
        int _flags;
    };

    struct IslanderRenderTarget
    {
        int _texture;
        int _resource;
    };

    struct IslanderRenderTargetArray
    {
        int count;
        int _texture;
        int* _resource;
    };

    struct IslanderShaderSemantic
    {
        int _format;
        uint32_t _stream;
        char* _desc;
    };

    struct Islander9SliceData
    {
        float _minX;
        float _maxX;
        float _minY;
        float _maxY;
    };

    struct IslanderMeasuredText
    {
        float _width;
        float _height;
        int _wrap;
        int _size;
        char* _font;
        char* _text;
    };

    struct IslanderTexureData
    {
        int id;
        int width;
        int height;
    };

    struct IslanderTexureListData
    {
        int count;
        IslanderTexureData textures[ISLANDER_TEXTURE_DATA_MAX_COUNT];
    };

    struct IslanderDisplay
    {
        int _id;
        char _name[ISLANDER_DISPLAY_NAME_LEN];
        int _len;
    };

    struct IslanderDisplays
    {
        int _count;
        IslanderDisplay _displays[ISLANDER_MAX_DISPLAYS];
    };

    struct IslanderResolution
    {
        int _id;
        int _width;
        int _height;
    };

    struct IslanderResolutions
    {
        int _count;
        IslanderResolution _resolutions[ISLANDER_MAX_RESOLUTIONS];
    };

    struct IslanderPolygonData
    {
        float min[3];
        float max[3];
        int skinned;
        int textureChannels;
    };

    struct IslanderImguiContext
    {
        void* imgui_context;
        void* alloc_func;
        void* free_func;
        void* user_data;
    };

    struct IslanderUITextSetting
    {
        int id;
        int size;
    };

    struct IslanderUISettings
    {
        int lineVertexShader;
        int linePixelShader;
        int iconVertexShader;
        int iconPixelShader;
        int iconArrayVertexShader;
        int iconArrayPixelShader;
        int filledRectVertexShader;
        int filledRectPixelShader;
        int textVertexShader;
        int textPixelShader;
        char* defaultFontName;
    };

    struct IslanderUIRenderData
    {
        int constantDataSize;
        void* constantData;
    };

    struct IslanderRenderableMaterial
    {
        int slot_flags[4];  // ISLANDER_RENDERABLE_MATERIAL_SLOT_*
        int slot_data[4];   // index
    };

    struct IslanderRenderableTransform
    {
        bool is_static;
        union
        {
            float mat[16];
            struct
            {
                float pos[3];
                float rot[3];
                float scale[3];
            };
        };
    };

    struct IslanderRenderableMesh
    {
        IslanderRenderableMaterial material;
        int meshId;
        int meshIndexId;
        int dirty;
        bool wireframe;
        bool debugbones;
        void* polydata;
        int controllerId;
        void* animationSystem;
        int parentEntity;
        int parentBone;
        void* constantBufferData;
        int constantBufferDataSize;
        int vertexShader;
        int pixelShader;
        int geometryShader;
    };

    struct IslanderRenderable
    {
        IslanderRenderableMesh mesh;
        IslanderRenderableTransform transform;
    };

    struct IslanderRenderablePass
    {
        int renderableBegin;
        int renderableCount;
        ISLANDER_CAMERA3D camera;
    };

    struct IslanderTextureSampler
    {
        int sampler_type; // ISLANDER_SAMPLER_TYPE_*
        float borderColor[4];
    };

    struct IslanderComputeWorkload
    {
        void* computeData;
        int computeDataNumBytes;
        int computeDataStride;
        void* constantBufferData;
        int constantBufferDataSize;
        int computeShader;
        int threadGroups[3];
        int computeTextureFlags[4];
        int computeTextures[4];
    };

    struct IslanderAnimationPose
    {
        float pos[3];
        float rot[3];
        float scale[3];
        int controllerId;
        void* animationSystem;
        int bone;
        void* polydata;
    };

    struct IslanderGPUProfileData
    {
        uint64_t startTicks;
        uint64_t endTicks;
    };

    struct IslanderGPUProfileFrameData
    {
        uint64_t frequency;
        int disjoint;
    };

    struct IslanderRendererStats
    {
        int numVertexBuffers;
        int numConstantBuffers;
        int numIndexBuffers;
    };
}
