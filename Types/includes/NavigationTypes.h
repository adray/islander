#pragma once
#include <stdint.h>
#include "CoreTypes.h"

#define ISLANDER_NAVMESH_ERROR_NO_PATH 0
#define ISLANDER_NAVMESH_SUCCESS 1
#define ISLANDER_NAVMESH_ERROR_NOT_ON_NAVMESH 2
#define ISLANDER_NAVMESH_ERROR_TARGET_NOT_ON_NAVMESH 3

extern "C"
{
    typedef void* ISLANDER_NAVIGATION;
    typedef void* ISLANDER_AVOIDANCE_SYSTEM;
    typedef int ISLANDER_AVOIDANCE_AGENT;

    struct IslanderAvoidanceAgentConfig
    {
        float pos[3];
        float collisionQueryRange;
        float height;
        float maxAcceleration;
        float maxSpeed;
        unsigned char obstacleAvoidanceType;
        float pathOptimizationRange;
        unsigned char queryFilterType;
        float radius;
        float separationWeight;
        unsigned char updateFlags;
    };

    struct IslanderAvoidanceType
    {
        float velBias;
        float weightDesVel;
        float weightCurVel;
        float weightSide;
        float weightToi;
        float horizTime;
        unsigned char gridSize;
        unsigned char adaptiveDivs;
        unsigned char adaptiveRings;
        unsigned char adaptiveDepth;
    };

    struct IslanderAvoidanceAgentData
    {
        float pos[3];
        float velocity[3];
        float target[3];
        bool moving;
        bool rotating;
    };

    struct IslanderNavMeshRegion
    {
        ISLANDER_POLYGON_DATA polymesh;

        // Object transform.
        float px, py, pz;
        float rx, ry, rz;
        float sx, sy, sz;
    };

    struct IslanderNavMeshData
    {
        float* vertexData;
        int32_t* indexData;
        unsigned char* navData;

        int vertexCount;
        int indexCount;
        int navCount;
    };

    struct IslanderNavMeshConfig
    {
        float cs; // cell size
        float ch; // cell height
        float walkableSlopeAngle;
        int walkableClimb;
        int walkableHeight;
        int walkableRadius;
        int minRegionArea;
        int mergeRegionArea;
        int maxEdgeLen;
        float maxSimplificationError;
        int maxVertsPerPoly;
        float detailSampleDist;
        float detailSampleMaxError;
    };

    struct IslanderNavPath
    {
        float sx; float sy; float sz;
        float ex; float ey; float ez;
        float nx; float ny; float nz;
    };

    struct IslanderNavClosestPoly
    {
        float sx; float sy; float sz;
        float px; float py; float pz;
    };

    struct IslanderNavRandomPointOnCircle
    {
        float radius;
        float sx; float sy; float sz;
        float px; float py; float pz;
    };
}
