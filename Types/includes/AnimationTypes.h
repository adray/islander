#pragma once

#define ISLANDER_ANIMATION_ROOT_MOTION_OFF 0
#define ISLANDER_ANIMATION_ROOT_MOTION_ON 1
#define ISLANDER_ANIMATION_ROOT_MOTION_FLAGS_NONE 0
#define ISLANDER_ANIMATION_ROOT_MOTION_FLAGS_DISABLE_Y 1    // if the character stays on the ground during the animation this flag should be set
