#pragma once

extern "C"
{
    typedef void* ISLANDER_ANIMATION_SYSTEM;
    typedef void* ISLANDER_POLYGON_DATA;
}
