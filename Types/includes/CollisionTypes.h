#pragma once
#include <stdint.h>
#include "CoreTypes.h"

#define ISLANDER_COLLISION_SHAPE_BOX 0
#define ISLANDER_COLLISION_SHAPE_SPHERE 1
#define ISLANDER_COLLISION_SHAPE_CAPSULE 2
#define ISLANDER_COLLISION_SHAPE_ORIENTATED_BOX 3
#define ISLANDER_COLLISION_SHAPE_HEIGHTMAP 4
#define ISLANDER_COLLISION_SHAPE_NULL 255

#define ISLANDER_COLLISION_IS_TRIGGER 0x1           // tests intersection against dynamic objects, but doesn't prevent moving through them
#define ISLANDER_COLLISION_IS_GHOST 0x2             // tests intersection against static objects, but doesn't prevent moving through them
#define ISLANDER_COLLISION_IGNORE_DYNAMIC 0x4       // doesn't test against dynamic
#define ISLANDER_COLLISION_IGNORE_STATIC 0x8        // doesn't test against static

#define ISLANDER_COLLISION_IS_RAY_ONLY 1            // object is only applicable for ray cast checks
#define ISLANDER_COLLISION_IS_TRIANGLE_COLLIDER 1   // uses a triangle collider
#define ISLANDER_COLLISION_IS_PUSHER 1              // object can push other objects defined as pushable
#define ISLANDER_COLLISION_IS_PUSHABLE 2            // object which can be pushed
#define ISLANDER_COLLISION_IS_PLATFORM 1            // object is a platform, which move other objects resting on it

#define ISLANDER_COLLISION_CONSTRAINT_COUNT 4
#define ISLANDER_COLLISION_CONSTRAINT_TWO_BODY 1

#define ISLANDER_COLLISION_DATA_COUNT 8
#define ISLANDER_COLLISION_ITERATION_DATA_COUNT 5
#define ISLANDER_COLLISION_OVERLAP_HIT_COUNT 8

#define ISLANDER_COLLISION_HIT_FLAG_STATIC 1
#define ISLANDER_COLLISION_HIT_FLAG_DYNAMIC 2
#define ISLANDER_COLLISION_HIT_FLAG_GRAVITY 4

#define ISLANDER_COLLISION_GROUP_DEFAULT 0xFFFFFF   // May be used a the default for the Ray cast group parameter.

#define ISLANDER_COLLISION_QUERY_FLAGS_NONE 0 
#define ISLANDER_COLLISION_QUERY_FLAGS_ITEMS 1      // Gets collision item data

extern "C"
{
    typedef void* ISLANDER_COLLISION_STATIC_MESH;
    typedef void* ISLANDER_COLLISION_DYNAMIC_MESH;
    typedef void* ISLANDER_COLLISION_SYSTEM;

    struct IslanderCollisionBox
    {
        // Axis aligned bounding box (AABB)
        float maxx, maxy, maxz;
        float minx, miny, minz;
    };

    struct IslanderCollisionSphere
    {
        float centreX, centreY, centreZ;
        float radius;
    };

    struct IslanderCollisionCapsule
    {
        float centreX, centreY, centreZ;
        float radius;
        float height;
    };

    struct IslanderCollisionTriangle
    {
        float* vertices[3];
        float normal[3];
    };

    struct IslanderCollisionOBB
    {
        float centre[3];
        float extents[18];
        float verticies[3 * 8];
        int edges[2 * 12];          // pairs of vertex indicies
    };

    struct IslanderCollisionStaticMeshInfo
    {
        // Static (not moving) object in the system.
        // Defined as one of:
        // - Array of triangles
        // - Box
        // - Heightmap

        // Size of box.
        IslanderCollisionBox box;

        // Object transform.
        float px, py, pz;
        float rx, ry, rz;
        float sx, sy, sz;

        // Object is not solid, acts like a trigger.
        int trigger;

        // Object is only for rays.
        int rayonly;

        // If the object uses a triangle collider.
        int triangleCollider;

        // If the object uses a heightmap collider.
        int heightmapCollider;

        // Triangle data
        ISLANDER_POLYGON_DATA meshData;

        // Heightmap
        void* heightmap;

        // The material type of the object.
        int material;
    };

    struct IslanderCollisionDynamicMeshInfo
    {
        // Moving object in the system
        // defined by a box.

        // Debug name.
        char* name;

        // Position of object.
        float px, py, pz;

        // Scale of object.
        float sx, sy, sz;

        // Rotation of object.
        float rx, ry, rz;

        // The type of shape specified.
        int collisionShape;

        union
        {
            // Size of box.
            IslanderCollisionBox box;

            // Or sphere.
            IslanderCollisionSphere sphere;

            // Or capsule.
            IslanderCollisionCapsule capsule;

            // Or OBB
            IslanderCollisionOBB obb;
        };

        // Value of max height of stairs can climb.
        float stepUpY;

        // Velocity of object.
        float vx, vy, vz;

        // Rotation change of object.
        float rdx, rdy, rdz;

        // Object is not solid, acts like a trigger.
        int trigger;

        // Object is only for rays.
        int rayonly;

        // Object can be pushed or can push.
        int pushType;

        // Object is a platform which objects can move on.
        int platformType;

        // Time to elapse before falling.
        float coyoteTime;

        // Group mesh belongs too (for filtering colliders).
        int group;

        // Triangle data
        ISLANDER_POLYGON_DATA meshData;

        // Animations.
        ISLANDER_ANIMATION_SYSTEM animationSystem;
        ISLANDER_COLLISION_DYNAMIC_MESH parentMesh;
        int parentControllerId;
        int parentBone;
    };

    struct IslanderCollisionCandidate
    {
        ISLANDER_COLLISION_DYNAMIC_MESH mesh;
        float t;
        int pen_iterations;
        bool tested;
        bool active;
        int shape;
    };

    struct IslanderCollisionIterationData
    {
        bool gravity;
        bool collide;
        float time;
        float normal[3];
        float velocity[3];
        float pos[3];
        IslanderCollisionCandidate candidates[8];
        int numCandidates;
    };

    struct IslanderCollisionHit
    {
        int flags;
        int material;
        float normal[3];
        union
        {
            ISLANDER_COLLISION_STATIC_MESH static_mesh;
            ISLANDER_COLLISION_DYNAMIC_MESH dynamic_mesh;
        };
        
        float v1[3]; // triangle v1
        float v2[3]; // triangle v2
        float v3[3]; // triangle v3
    };

    struct IslanderCollisionData
    {
        float px, py, pz;
        float rx, ry, rz;
        IslanderCollisionHit hits[ISLANDER_COLLISION_DATA_COUNT];
        int hit_count;
        ISLANDER_COLLISION_STATIC_MESH static_mesh[ISLANDER_COLLISION_DATA_COUNT];
        ISLANDER_COLLISION_DYNAMIC_MESH dynamic_mesh[ISLANDER_COLLISION_DATA_COUNT];
        int static_mesh_count;
        int dynamic_mesh_count;
        IslanderCollisionIterationData it[ISLANDER_COLLISION_ITERATION_DATA_COUNT*2];
        int iterations;
        bool falling;
        bool pushing;
        bool grounded;
        bool platform;
    };

    struct IslanderCollisionRayInfo
    {
        union
        {
            ISLANDER_COLLISION_DYNAMIC_MESH ignore_mesh;
            ISLANDER_COLLISION_STATIC_MESH ignore_static_mesh;
        };
        ISLANDER_COLLISION_STATIC_MESH static_mesh;
        ISLANDER_COLLISION_DYNAMIC_MESH dynamic_mesh;
        float static_hit_pos[3];
        float dynamic_hit_pos[3];
        float static_normal[3];
        float dynamic_normal[3];
    };

    struct IslanderCollisionOverlapInfo
    {
        IslanderCollisionHit hits[ISLANDER_COLLISION_OVERLAP_HIT_COUNT];
        int hit_count;
    };

    struct IslanderSelectionData
    {
        IslanderCollisionBox box;

        // Object transform.
        float px, py, pz;
        float rx, ry, rz;
        float sx, sy, sz;
    };

    struct IslanderCollisionGeometryData
    {
        float* vertexData;
        int32_t* indexData;
        int vertexCount;
        int indexCount;
        int strideSize;
    };

    struct IslanderCollisionQueryItem
    {
        bool is_static;
        void* ptr;
    };

    struct IslanderCollisionQueryInfo
    {
        int queryFlags;             // ISLANDER_COLLISION_QUERY_FLAGS
        int numStaticColliders;
        int numDynamicColliders;
        int maxStaticColliders;
        int maxDynamicColliders;
        IslanderCollisionQueryItem* colliders;
    };
}
