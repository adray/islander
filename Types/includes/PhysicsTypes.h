#pragma once
#include "CollisionTypes.h"

#define ISLANDER_PHYSICS_BODY_TYPE_STATIC 0
#define ISLANDER_PHYSICS_BODY_TYPE_KINEMATIC 1
#define ISLANDER_PHYSICS_BODY_TYPE_DYNAMIC 2

extern "C"
{
    struct IslanderPhysicsBody
    {
        uint32_t id;
        int is_character;
        int body_type;
        int shape;
        float mass;
        union
        {
            IslanderCollisionSphere sphere;
            IslanderCollisionCapsule capsule;
            IslanderCollisionBox box;
            IslanderCollisionOBB obb;
        };
        float pos[3];
        float rot[3];
        float scale[3];
        float velocity[3];
        void* character;
    };

    struct IslanderCollisionConstraintInfo
    {
        int type;
        union
        {
            ISLANDER_COLLISION_DYNAMIC_MESH mesh1;
            ISLANDER_COLLISION_DYNAMIC_MESH mesh2;
            float minDist;
            float maxDist;
        };
    };
}
