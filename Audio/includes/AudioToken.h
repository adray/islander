#pragma once
#include <string>

namespace Islander
{
	namespace Sound
	{
		enum /*class*/ TokenState
		{
			TOKEN_STATE_NONE,
			TOKEN_STATE_QUEUED,
			TOKEN_STATE_PLAYING,
            TOKEN_STATE_COMPLETING,
			TOKEN_STATE_COMPLETED,
			TOKEN_STATE_ABORTED,
		};

		enum /*class*/ EventType
		{
			SOUND_EVENT_TYPE_NONE,
			SOUND_EVENT_TYPE_BUFFER_START,
			SOUND_EVENT_TYPE_BUFFER_END,
			SOUND_EVENT_TYPE_LOOP_END,
			SOUND_EVENT_TYPE_ERROR,
            SOUND_EVENT_TYPE_STREAM_END
		};

		class Token
		{
		public:
			virtual int GetCategory() const = 0;
			virtual void SetCategory(int category) = 0;
			virtual TokenState GetState() const = 0;
            virtual int GetType() const = 0;
            virtual void SetState(const TokenState& state) = 0;
            virtual void SetState(const TokenState& state, const std::string& reason) = 0;
			virtual void SetVolume(float volume) = 0;
			virtual void SetFrequencyRatio(float ratio) = 0;
			virtual float GetVolume() const = 0;
			virtual void SetType(int type) = 0;

			virtual ~Token(){};
		};
	}
}
