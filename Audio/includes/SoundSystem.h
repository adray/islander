//
// Represents a system for loading/playing sounds.
//

#pragma once
#include <vector>
#include <cstdint>

//#ifdef Islander_Renderer_EXPORTS
//#pragma comment(lib,"xaudio2.lib")
//#pragma comment(lib,"xapobase.lib")
//#endif

struct IslanderAudioEvent;

namespace Islander
{
	namespace Sound
	{
		class SoundInterface;
		class Token;
		//struct SoundEvent;
		struct Sound;

		class SoundSystem
		{
		public:
			SoundSystem(void* window);
			~SoundSystem(void);

			struct WaveFormatHeader
			{
				char subChunkId[4];
				uint32_t subChunkSize;
				uint16_t audioFormat;
				uint16_t numChannels;
				uint32_t sampleRate;
				uint32_t bytesPerSecond;
				uint16_t blockAlign;
				uint16_t bitsPerSample;
			};

			struct WaveDataHeader
			{
				char dataChunkId[4];
				uint32_t dataSize;
			};

			struct WaveRiffHeader
			{
				char chunkId[4];
				uint32_t chunkSize;
				char format[4];
			};

			struct WaveHeaderType
			{
				WaveRiffHeader riffChunk;
				WaveFormatHeader formatChunk;
				WaveDataHeader dataChunk;
			};

			int LoadSound(const std::string& filename, int category);
			int LoadSound(char* data, int size, int category);
			Token* PlaySound(int handle, int loopCount);
			void SetOn(bool on) const;
			void ProcessEventQueue(/*Islander::Sound::SoundEvent**/IslanderAudioEvent* ev);
			void StopSound(Token* t);
			void ReleaseToken(Token* t);
			void SetCategoryVolume(int category, float vol);
			void SetSoundVolume(Token* t, float volume);
			void SetSoundFrequencyRatio(Token* t, float ratio);
			int CreateCategory();
            bool IsValidToken(Token* t);

			inline bool IsInitialized() { return m_initialized; }

		private:

			SoundInterface* _interface;

			std::vector<Sound*> m_buffers;
			bool m_initialized;
			std::vector<float> m_category_volume;

			int CreateSound(WaveHeaderType* header, const std::string& filename, char* data, int category, int offset);

		};

	}
}
