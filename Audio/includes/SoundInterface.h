#pragma once
#include <string>

struct IslanderAudioEvent;

namespace Islander
{
	namespace Sound
	{
		struct Sound;
		class SoundSystemImpl;
		class Token;

		struct SoundInfo
		{
			std::string filename;
			int bitsPerSample;
			int numChannels;
			int sampleRate;
			bool pcm;
		};

		class SoundInterface
		{
		public:
			SoundInterface();
			void ProcessEventQueue(IslanderAudioEvent* ev);
			void SetAudioOn(bool on) const;
			Islander::Sound::Token* StartAudio(Sound* sound, int loopCount);
			void EndAudio(Token* token);
			Sound* CreateSound(const SoundInfo& info, char* data, int dataSize, int category, int offset);
			void UpdateCategoryVolume(int category, float oldVolume, float newVolume);
            bool IsValidToken(Token* token);

			~SoundInterface();

		private:
			SoundSystemImpl* _impl;

			void PlayAudio(Token* token);
		};
	}
}
