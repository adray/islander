
#include <fstream>
#include <sstream>
#include <locale>
#include <codecvt>
#include <thread>
#include <atomic>
#include <cstring>

#include "SoundSystem.h"
#include "SoundInterface.h"
#include "AudioToken.h"
#include "LocklessQueue.h"
#include "FileSystem.h"
#include "Logger.h"
#include "AudioTypes.h"

#define ASYNC_SOUND_SIZE_THRESHOLD 1024 * 1024 * 2
#undef PlaySound

using namespace Islander::Sound;

SoundSystem::SoundSystem(void* window)
	: m_initialized(false)
{
	_interface = new SoundInterface();

	m_initialized = true;
}

SoundSystem::~SoundSystem(void)
{
	delete _interface;
}

void SoundSystem::ReleaseToken(Islander::Sound::Token* t)
{
	StopSound(t);
	delete t;
}

void SoundSystem::StopSound(Islander::Sound::Token* t)
{
	// TODO: the buffer may end before this has a chance to take effect
	_interface->EndAudio(t);
}

int SoundSystem::CreateSound(WaveHeaderType* header, const std::string& filename, char* data, int category, int offset)
{
	SoundInfo info;
	info.bitsPerSample = header->formatChunk.bitsPerSample;
	info.filename = filename;
	info.numChannels = header->formatChunk.numChannels;
	info.pcm = true;
	info.sampleRate = header->formatChunk.sampleRate;

	Sound* sound = _interface->CreateSound(info, data, header->dataChunk.dataSize, category, offset);
	
	m_buffers.push_back(sound);

	return m_buffers.size()-1;
}

int SoundSystem::LoadSound(char* data, int size, int category)
{
	// Read header
	char* header_data = new char[sizeof(WaveHeaderType)];
	WaveHeaderType* header = reinterpret_cast<WaveHeaderType*>(header_data);
	memcpy(header_data, data, sizeof(WaveHeaderType));

	// TODO
	//while (header->dataChunkId[0] != 'd' ||
	//	header->dataChunkId[1] != 'a' ||
	//	header->dataChunkId[2] != 't' ||
	//	header->dataChunkId[3] != 'a')
	//{
	//	// skip over
	//	stream.seekg(header->dataSize, std::ios::cur);
	//	stream.read(header->dataChunkId, sizeof(char) * 4);
	//	stream.read((char*)&header->dataSize, sizeof(unsigned long));
	//}

	// Read data
	int align = (header->formatChunk.bitsPerSample / 8) * header->formatChunk.numChannels;
	int datasize = header->riffChunk.chunkSize + (header->riffChunk.chunkSize % align);

	char* sound_data = new char[datasize];
	std::memset(sound_data, 0, datasize);
	memcpy(sound_data, data+sizeof(WaveHeaderType), sizeof(char)*header->riffChunk.chunkSize);
	header->riffChunk.chunkSize = datasize;

	return CreateSound(header, "", sound_data, category, 0);
}

int SoundSystem::LoadSound(const std::string& filename, int category)
{
	// Load sound data
	std::ifstream stream;
	FileSystem::ReadFile(stream, filename, true);

	if (stream.fail())
	{
		std::stringstream ss;
		ss << "Unable to load file: " << filename;

		Islander::Logger::GetLogger()->Log(ss.str(), LOGGING_RESOURCES);
		return -1;
	}

	// Read header
	char* data = new char[sizeof(WaveHeaderType)];
	WaveHeaderType* header = reinterpret_cast<WaveHeaderType*>(data);
	//stream.read(data, sizeof(WaveHeaderType));
	stream.read((char*)&header->riffChunk, sizeof(WaveRiffHeader));

	if (memcmp("WAVE", header->riffChunk.format, sizeof(header->riffChunk.format)) != 0)
	{
		delete[] data;
		return -1;
	}

	stream.read((char*)&header->formatChunk.subChunkId, sizeof(header->formatChunk.subChunkId));
	stream.read((char*)&header->formatChunk.subChunkSize, sizeof(header->formatChunk.subChunkSize));
	while (memcmp("fmt ", header->formatChunk.subChunkId, sizeof(header->formatChunk.subChunkId) != 0))
	{
		// skip over
		stream.seekg(header->formatChunk.subChunkSize, std::ios::cur);
		stream.read((char*)&header->formatChunk.subChunkId, sizeof(header->formatChunk.subChunkId));
		stream.read((char*)&header->formatChunk.subChunkSize, sizeof(header->formatChunk.subChunkSize));
	}

	stream.read((char*)&header->formatChunk.audioFormat, sizeof(header->formatChunk.audioFormat));
	stream.read((char*)&header->formatChunk.numChannels, sizeof(header->formatChunk.numChannels));
	stream.read((char*)&header->formatChunk.sampleRate, sizeof(header->formatChunk.sampleRate));
	stream.read((char*)&header->formatChunk.bytesPerSecond, sizeof(header->formatChunk.bytesPerSecond));
	stream.read((char*)&header->formatChunk.blockAlign, sizeof(header->formatChunk.blockAlign));
	stream.read((char*)&header->formatChunk.bitsPerSample, sizeof(header->formatChunk.bitsPerSample));
	stream.read((char*)&header->dataChunk, sizeof(WaveDataHeader));

	while (header->dataChunk.dataChunkId[0] != 'd' ||
		header->dataChunk.dataChunkId[1] != 'a' ||
		header->dataChunk.dataChunkId[2] != 't' ||
		header->dataChunk.dataChunkId[3] != 'a')
	{
		// skip over
		stream.seekg(header->dataChunk.dataSize, std::ios::cur);
		stream.read(header->dataChunk.dataChunkId, sizeof(char) * 4);
		stream.read((char*)&header->dataChunk.dataSize, sizeof(char)*4);
	}

	// Read data
	int align = (header->formatChunk.bitsPerSample / 8) * header->formatChunk.numChannels;
	header->dataChunk.dataSize -= 8;
	int alignedSize = header->dataChunk.dataSize + (header->dataChunk.dataSize % align);
	
	// If it is over the threshold stream it from disk when played
	int handle = -1;
	if (alignedSize >= ASYNC_SOUND_SIZE_THRESHOLD)
	{
		handle = CreateSound(header, filename, nullptr, category, stream.tellg());
	}
	else
	{
		char* sound_data = new char[alignedSize];
		memset(sound_data, 0, alignedSize);
		stream.read(sound_data, header->dataChunk.dataSize);
		header->dataChunk.dataSize = alignedSize;

		handle = CreateSound(header, filename, sound_data, category, stream.tellg());
		delete[] data;
	}

	stream.close();

	if (handle >= 0)
	{
		std::stringstream ss;
		ss << "Loaded: " << filename;

		Islander::Logger::GetLogger()->Log(ss.str(), LOGGING_RESOURCES);
	}
	else
	{
		std::stringstream ss;
		ss << "Unable to create/find voice for: " << filename;

		Islander::Logger::GetLogger()->Log(ss.str(), LOGGING_RESOURCES);
	}

	return handle;
}

Islander::Sound::Token* SoundSystem::PlaySound(int handle, int loopCount)
{
	auto sound = m_buffers[handle];
	
	auto token = _interface->StartAudio(sound, loopCount);
	if (token != nullptr)
	{
		auto volume = m_category_volume[token->GetCategory()];
		token->SetVolume(volume);
	}

	return token;
}

void SoundSystem::SetOn(bool on) const
{
	_interface->SetAudioOn(on);
}

void SoundSystem:: ProcessEventQueue(IslanderAudioEvent* ev)
{
	_interface->ProcessEventQueue(ev);
}

void SoundSystem::SetCategoryVolume(int category, float vol)
{
	auto oldVolume = m_category_volume[category];
	m_category_volume[category] = vol;
	_interface->UpdateCategoryVolume(category, oldVolume, vol);
}

void SoundSystem::SetSoundVolume(Islander::Sound::Token* t, float volume)
{
	t->SetVolume(volume * m_category_volume[t->GetCategory()]);
}

void SoundSystem::SetSoundFrequencyRatio(Islander::Sound::Token* t, float ratio)
{
	t->SetFrequencyRatio(ratio);
}

int SoundSystem::CreateCategory()
{	
	m_category_volume.push_back(1);
	return m_category_volume.size() - 1;
}

bool SoundSystem::IsValidToken(Token* t)
{
    return _interface->IsValidToken(t);
}
