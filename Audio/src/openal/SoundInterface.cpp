#include <vector>
#include <atomic>
#include <thread>
#include <deque>
#include <sstream>

#include "SoundInterface.h"
#include "AudioToken.h"
#include <Logger.h>
#include "LocklessQueue.h"
#include "FileSystem.h"

#include <AL/al.h>
#include <AL/alext.h>
#include <unistd.h>

#define ASYNC_DATA_BUFFER_COUNT 3
#define ASYNC_DATA_BUFFER_SIZE 65536

using namespace Islander::Sound;

namespace Islander
{
    namespace Sound
    {
		struct Sound
		{
			std::string _filename;
			unsigned char* _data;
			int _category;
			int _size;
			int _soundType;
			int _async;
			int _offset;
		};

        struct AsyncData
        {
			int _reader;
			std::atomic<int> _writer;
			int _consumed;
			int _remaining_stream_size;
			unsigned char _data[ASYNC_DATA_BUFFER_COUNT][ASYNC_DATA_BUFFER_SIZE];
			std::ifstream _stream;
			bool _completed;
        };

		struct Voice
		{
			ALuint _source;
			ALuint* _buffer;
			int _bufferCount;
			int _soundType;
			bool _available;
		};

        class OpenALToken : public Token
		{
		private:
			TokenState _state;
			int _category;
			float _volume;
            int _type;
			
		public:
			AsyncData* _asyncData;
			Voice* _voice;

			int GetCategory() const
			{
				return _category;
			}

			TokenState GetState() const
			{
				return _state;
			}

			void SetState(const TokenState& state)
			{
				_state = state;
			}
			
            void SetState(const TokenState& state, const std::string& reason)
			{
				_state = state;
			}
            
			void SetCategory(int category)
			{
				_category = category;
			}

			void SetVolume(float volume)
			{
    			alSourcef(_voice->_source, AL_GAIN, volume);

				_volume = volume;
			}

			void SetFrequencyRatio(float ratio)
			{
				// TODO
			}

			float GetVolume() const
			{
				return _volume;
			}

            void SetType(int type)
            {
                _type = type;
            }

            int GetType() const
            {
                return _type;
            }

			~OpenALToken() {}
		};

		class VoicePool
		{
			private:
				struct VoicePoolItem
				{
					int _frequency;
					int _buffer_count;
					Voice* _voice;
				};

				std::vector<VoicePoolItem> _items;

			public:
				Voice* GetVoice(int frequency, int buffer_count);
		};

		Voice* VoicePool::GetVoice(int frequency, int buffer_count)
		{
			for (int i = 0; i < _items.size(); i++)
			{
				auto item = _items[i];
				if (item._frequency == frequency &&
					item._buffer_count == buffer_count &&
					item._voice->_available)
				{
					item._voice->_available = false;
					return item._voice;
				}
			}

			ALuint source;
			alGenSources(1, &source);
			if (alGetError() != AL_NO_ERROR)
			{
				Islander::Logger::GetLogger()->Log("Audio error", LOGGING_INFO);
				return nullptr;
			}

			Voice* voice = new Voice();
			voice->_available = false;
			voice->_source = source;
			voice->_soundType = frequency;
			voice->_bufferCount = buffer_count;
			voice->_buffer = new ALuint[voice->_bufferCount];
			alGenBuffers(buffer_count, voice->_buffer);

			if (alGetError() != AL_NO_ERROR)
			{
				Islander::Logger::GetLogger()->Log("Audio error: alGenBuffers", LOGGING_INFO);
			}

			VoicePoolItem item;
			item._voice = voice;
			item._frequency = frequency;
			item._buffer_count = buffer_count;

			_items.push_back(item);

			return voice;
		}

		class SoundSystemImpl
        {
            private:

				bool _running;
				std::vector<AsyncData*> _streams;
				std::thread *_worker;
				LocklessQueue<AsyncData*> _pending;
				VoicePool _voice_pool;

				void ProcessWorkerThread();

            public:
                ALCdevice* m_device;
                ALCcontext* m_context;
				std::vector<Token*> m_tokens;

				SoundSystemImpl();
				~SoundSystemImpl();

				OpenALToken* Submit(Sound* sound);
				OpenALToken* SubmitAsync(Sound* sound);
        };

		SoundSystemImpl::SoundSystemImpl()
			: _running(true)
		{
			_worker = new std::thread(&Islander::Sound::SoundSystemImpl::ProcessWorkerThread, this);			
		}

		SoundSystemImpl::~SoundSystemImpl()
		{
			_running = false;
			_worker->join();
			delete _worker;
		}

		void Islander::Sound::SoundSystemImpl::ProcessWorkerThread()
		{
			std::deque<AsyncData*> completed;
			while (_running)
			{
				AsyncData* pending;
				while (_pending.TryDequeue(&pending))
				{
					_streams.push_back(pending);
				}

				for (auto data : _streams)
				{
					if (data->_completed)
					{
						completed.push_back(data);
						continue;
					}

					while (data->_consumed + ASYNC_DATA_BUFFER_COUNT > data->_writer && data->_remaining_stream_size > 0)
					{
						int size = std::min(data->_remaining_stream_size, ASYNC_DATA_BUFFER_SIZE);
						if (size > 0)
						{
							data->_stream.read(
								(char*)data->_data[(data->_writer + 1) % ASYNC_DATA_BUFFER_COUNT],
								size);
							data->_remaining_stream_size -= size;
							data->_writer++;
						}
					}
				}

				while (completed.size() > 0)
				{
					AsyncData* data = completed.front();
					data->_stream.close();
					_streams.erase(std::find(_streams.begin(), _streams.end(), data));
					delete data;
					completed.pop_front();
				}

				// TODO: replace with wait condition?
				std::this_thread::sleep_for(std::chrono::milliseconds(10));
			}
		}

		OpenALToken* SoundSystemImpl::SubmitAsync(Sound* sound)
		{
			Voice* voice = _voice_pool.GetVoice(sound->_soundType, 3);

			auto token = new OpenALToken();
			token->SetState(TokenState::TOKEN_STATE_PLAYING);

			AsyncData* asyncData = new AsyncData;
			asyncData->_reader = -1;
			asyncData->_writer = -1;
			asyncData->_consumed = -1;
			asyncData->_completed = false;
			asyncData->_remaining_stream_size = sound->_size;
			FileSystem::ReadFile(asyncData->_stream, sound->_filename, true);
			asyncData->_stream.seekg(sound->_offset);
			_pending.Queue(asyncData);

			token->_voice = voice;
			token->_asyncData = asyncData;

			m_tokens.push_back(token);

			alSourcePlay(voice->_source);

			if (alGetError() != AL_NO_ERROR)
			{
				Islander::Logger::GetLogger()->Log("Audio error: alSourcePlay", LOGGING_INFO);
			}

			return token;
		}

		OpenALToken* SoundSystemImpl::Submit(Sound* sound)
		{
			Voice* voice = _voice_pool.GetVoice(sound->_soundType, 1);
			
			alSourcePlay(voice->_source);
			if (alGetError() != AL_NO_ERROR)
			{
				Islander::Logger::GetLogger()->Log("Audio error", LOGGING_INFO);
				return nullptr;
			}

			auto token = new OpenALToken();
			token->_voice = voice;
			token->SetState(TOKEN_STATE_PLAYING);

			m_tokens.push_back(token);

			return token;
		}
    }
}

SoundInterface::SoundInterface()
{
    _impl = new SoundSystemImpl();

    _impl->m_device = alcOpenDevice(nullptr);

	if (_impl->m_device)
	{
		_impl->m_context = alcCreateContext(_impl->m_device,
						    nullptr);
		alcMakeContextCurrent(_impl->m_context);
	}
	else
	{
		return;
	}

	auto extension = alIsExtensionPresent("EAX2.0");

	alGetError(); // clear errors
}

SoundInterface::~SoundInterface()
{
	Logger::GetLogger()->Log("SoundSystem shutting down.", LOGGING_INFO);
	
	alcMakeContextCurrent(nullptr);
	alcDestroyContext(_impl->m_context);
	alcCloseDevice(_impl->m_device);

    delete _impl;
}

void SoundInterface::ProcessEventQueue(Islander::Sound::SoundEvent* ev)
{
	ev->_type = EventType::SOUND_EVENT_TYPE_NONE;
	ev->_token = nullptr;

	// Queue async loaded audio
	for (int i = 0; i < _impl->m_tokens.size(); i++)
	{
		auto t = (OpenALToken*) _impl->m_tokens[i];
		if (t->_asyncData != nullptr && t->GetState() != TokenState::TOKEN_STATE_COMPLETED)
		{
			ALint value = 0;
			alGetSourcei(t->_voice->_source, AL_BUFFERS_PROCESSED, &value);

			if (alGetError() != AL_NO_ERROR)
			{
				Islander::Logger::GetLogger()->Log("Audio error: alGetSourcei", LOGGING_INFO);
			}

			auto asyncData = t->_asyncData;
			
			ALuint dequeueBuffer;
			int number_deque = (int)value;
			for (int j = 0; j < number_deque; j++)
			{
				alSourceUnqueueBuffers(t->_voice->_source,
					1,
					&dequeueBuffer);

				if (alGetError() != AL_NO_ERROR)
				{
					Islander::Logger::GetLogger()->Log("Audio error: alSourceUnqueueBuffers", LOGGING_INFO);
				}
			}
			
			asyncData->_consumed += (int)value;

			while (asyncData->_writer > asyncData->_reader)
			{
				PlayAudio(t);
				t->SetState(TokenState::TOKEN_STATE_PLAYING);
			}
		}		
	}

	// Poll for completed buffers
	for (int i = 0; i < _impl->m_tokens.size(); i++)
	{
		auto t = (OpenALToken*) _impl->m_tokens[i];
		ALint value=0;
		alGetSourcei(t->_voice->_source, AL_SOURCE_STATE, &value);
		if (value == AL_STOPPED)
		{
			if (t->_asyncData == nullptr)
			{
				ev->_token = t;
				ev->_type = EventType::SOUND_EVENT_TYPE_STREAM_END;
				t->_voice->_available = true;
				_impl->m_tokens.erase(_impl->m_tokens.begin() + i);
				break;
			}
			else if (t->_asyncData->_consumed == t->_asyncData->_writer &&
				t->_asyncData->_remaining_stream_size == 0)
			{
				ev->_token = t;
				ev->_type = EventType::SOUND_EVENT_TYPE_STREAM_END;
				t->_voice->_available = true;
				_impl->m_tokens.erase(_impl->m_tokens.begin() + i);
				break;
			}
		}
	}
}

void SoundInterface::SetAudioOn(bool on) const
{

}

Islander::Sound::Token* SoundInterface::StartAudio(Sound* sound, int loopCount)
{
    int type = sound->_soundType;

	Logger::GetLogger()->Log("Starting Audio", LOGGING_INFO);

    Islander::Sound::Token* token = nullptr;

	if (sound->_async)
	{
		token = _impl->SubmitAsync(sound);
	}
	else
	{
		token = _impl->Submit(sound);
	}

	if (token != nullptr)
	{
		token->SetCategory(sound->_category);
		token->SetType(type);
	}

    return token;
}

void ClearSoundBuffers(OpenALToken* token)
{
	ALint value = 0;
	alGetSourcei(token->_voice->_source, AL_BUFFERS_QUEUED, &value);

	if (alGetError() != AL_NO_ERROR)
	{
		Islander::Logger::GetLogger()->Log("Audio error: alGetSourcei", Islander::LOGGING_INFO);
	}

	ALuint dequeueBuffer;
	int number_deque = (int)value;
	for (int j = 0; j < number_deque; j++)
	{
		alSourceUnqueueBuffers(token->_voice->_source,
			1,
			&dequeueBuffer);

		if (alGetError() != AL_NO_ERROR)
		{
			Islander::Logger::GetLogger()->Log("Audio error: alSourceUnqueueBuffers", Islander::LOGGING_INFO);
		}
	}
}

void SoundInterface::EndAudio(Token* token)
{
	auto voice = ((OpenALToken*)token)->_voice;

	alSourceStop(voice->_source);

	if (token->GetState() != TokenState::TOKEN_STATE_COMPLETED)
	{
		ClearSoundBuffers((OpenALToken*)token);
		token->SetState(TokenState::TOKEN_STATE_COMPLETED);

		auto asyncData = ((OpenALToken*)token)->_asyncData;
		if (asyncData)
		{
			asyncData->_completed = true;
			((OpenALToken*)token)->_asyncData = nullptr;
		}
	}
}

Sound* SoundInterface::CreateSound(const SoundInfo& info, char* data, int dataSize, int category, int offset)
{
	Sound* sound = new Sound();
    sound->_soundType = info.sampleRate;
	sound->_data = reinterpret_cast<unsigned char*>(data);
	sound->_category = category;
	sound->_size = dataSize;
	sound->_async = sound->_data == nullptr;
	sound->_filename = info.filename;
	sound->_offset = offset;

	return sound;
}

void SoundInterface::UpdateCategoryVolume(int category, float oldVolume, float newVolume)
{
	for (auto token : _impl->m_tokens)
	{
		if (token->GetCategory() == category)
		{
			float volume = token->GetVolume();
			token->SetVolume(volume * newVolume / oldVolume);
		}
	}
}

bool SoundInterface::IsValidToken(Token* token)
{
    auto it = std::find(_impl->m_tokens.begin(), _impl->m_tokens.end(), token);
    if (it != _impl->m_tokens.end())
    {
        return true;
    }

    return false;
}

void SoundInterface::PlayAudio(Token* token)
{
	auto asyncData = ((OpenALToken*)token)->_asyncData;
	auto ptr = asyncData->_data[(asyncData->_reader + 1) % ASYNC_DATA_BUFFER_COUNT];

	auto voice = ((OpenALToken*)token)->_voice;

	auto code1 = alGetError();
	if (code1 != AL_NO_ERROR)
	{
		Islander::Logger::GetLogger()->Log("Audio error: pre check alBufferData", LOGGING_INFO);
	}

	// Queue the buffer
	auto buf = voice->_buffer[
		(asyncData->_reader + 1) % voice->_bufferCount];
	alBufferData(buf, AL_FORMAT_STEREO16, ptr, ASYNC_DATA_BUFFER_SIZE, voice->_soundType);

	auto code = alGetError();
	if (code != AL_NO_ERROR)
	{
		Islander::Logger::GetLogger()->Log("Audio error: alBufferData", LOGGING_INFO);
	
		std::stringstream ss; ss << "Error ";
		switch (code)
		{
			case AL_OUT_OF_MEMORY:
				ss << "Out of memory";
				break;
			case AL_INVALID_VALUE:
				ss << "Invalid value";
				break;
			case AL_INVALID_ENUM:
				ss << "Invalid enum";
				break;
			case AL_INVALID_OPERATION:
				ss << "Invalid operation";
				break;
			default:
				ss << code;
				break;
		}

		Logger::GetLogger()->Log(ss.str(), LOGGING_INFO);
	}

	alSourceQueueBuffers(
		voice->_source,
		1,
		&buf
	);

	if (alGetError() != AL_NO_ERROR)
	{
		Islander::Logger::GetLogger()->Log("Audio error: alSourceQueueBuffers", LOGGING_INFO);
	}

	alSourcePlay(voice->_source);

	asyncData->_reader++;
}
