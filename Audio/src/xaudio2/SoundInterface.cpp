#include <string>
#include <atomic>
#include <fstream>
#include <XAudio2.h>
#include <locale>
#include <codecvt>
#include <thread>
#include <sstream>
#include <deque>

#include "SoundInterface.h"
#include "AudioToken.h"
#include "LocklessQueue.h"
#include "FileSystem.h"
#include "Logger.h"
#include "AudioTypes.h"

#undef PlaySound

#define ASYNC_DATA_BUFFER_COUNT 3
#define ASYNC_DATA_BUFFER_SIZE 65536

#define VOICE_COUNT 4
#define VOICE_POOL_COUNT 8

const unsigned char empty_data_block[32] = { 0 };

using namespace Islander::Sound;

namespace Islander
{
	namespace Sound
	{
		struct Sound
		{
			std::string _filename;
			unsigned char* _data;
			int _category;
			int _size;
			int _soundType;
			int _async;
			int _offset;
		};

		struct AsyncData
		{
			int _reader;
			std::atomic<int> _writer;
			int _consumed;
			int _remaining_stream_size;
			unsigned char _data[ASYNC_DATA_BUFFER_COUNT][ASYNC_DATA_BUFFER_SIZE];
			std::ifstream _stream;
			bool _completed;
		};

		class XAudioToken : public Token
		{
		private:
			TokenState _state;
			int _category;
			float _volume;
            int _type;
            bool _mute;

		public:
			IXAudio2SourceVoice* _voice;
			AsyncData* _asyncData;

			int GetCategory() const
			{
				return _category;
			}

			TokenState GetState() const
			{
				return _state;
			}

			void SetState(const TokenState& state)
			{
                if (state != _state)
                {
                    (Logger::GetLogger()->GetStream(LOGGING_DEBUG) << "XAudioToken " << this << " State changed " << _state << " -> " << state).endl();
                }

				_state = state;
			}

            void SetState(const TokenState& state, const std::string& reason)
            {
                if (state != _state)
                {
                    (Logger::GetLogger()->GetStream(LOGGING_DEBUG) << "XAudioToken " << this << " State changed " << _state << " -> " << state << " - " << reason).endl();
                }

                _state = state;
            }
			
			void SetCategory(int category)
			{
				_category = category;
			}

            void Mute(bool mute)
            {
                _mute = mute;
                _voice->SetVolume(_mute ? 0 : _volume);
            }

			void SetVolume(float volume)
			{
				_volume = max(min(volume, 1), 0);
				_voice->SetVolume(_mute ? 0 : _volume);
			}

			void SetFrequencyRatio(float ratio)
			{
				_voice->SetFrequencyRatio(ratio);
			}

			float GetVolume() const
			{
				return _volume;
			}

            void SetType(int type)
            {
                _type = type;
            }

            int GetType() const
            {
                return _type;
            }

			~XAudioToken() {}
		};

		class SoundEventSink
		{
		private:
			LocklessQueue<IslanderAudioEvent> _queue;

		public:

			void Queue(const IslanderAudioEvent& ev)
			{
				_queue.Queue(ev);
			}

			void Dequeue(IslanderAudioEvent* ev)
			{
				if (_queue.TryDequeue(ev))
				{
					switch (ev->_type)
					{
					case EventType::SOUND_EVENT_TYPE_ERROR:
						((XAudioToken*)ev->_token)->SetState(TokenState::TOKEN_STATE_ABORTED, "SOUND_EVENT_TYPE_ERROR");
						break;
					}
				}
				else
				{
					ev->_token = nullptr;
				}
			}
		};

		class DefaultCallback : public IXAudio2VoiceCallback
		{
		public:

			DefaultCallback(SoundEventSink* sink)
				: _sink(sink), _token(nullptr)
			{}

            inline void SetToken(Token* token) { _token = token; }

            inline Token* GetToken() { return _token; }

			STDMETHOD_(void, OnVoiceProcessingPassStart) (uint32_t bytesRequired) {}
			STDMETHOD_(void, OnVoiceProcessingPassEnd) () {}

			STDMETHOD_(void, OnStreamEnd) ()
            {
                SignalEvent(EventType::SOUND_EVENT_TYPE_STREAM_END);
            }

			STDMETHOD_(void, OnBufferStart) (void* pContext)
			{
				SignalEvent(EventType::SOUND_EVENT_TYPE_BUFFER_START);
			}

			STDMETHOD_(void, OnBufferEnd) (void* pContext)
			{
				SignalEvent(EventType::SOUND_EVENT_TYPE_BUFFER_END);
			}

			STDMETHOD_(void, OnLoopEnd) (void* pContext)
			{
				SignalEvent(EventType::SOUND_EVENT_TYPE_LOOP_END);
			}

			STDMETHOD_(void, OnVoiceError) (void* pContext, HRESULT error)
			{
				SignalEvent(EventType::SOUND_EVENT_TYPE_ERROR);
			}

			virtual ~DefaultCallback() {}
		private:

			SoundEventSink* _sink;
            Token* _token;

			void SignalEvent(EventType type)
			{
				/*SoundEvent*/IslanderAudioEvent ev;
                ev._token = _token;
				ev._type = type;

				_sink->Queue(ev);
			}
		};

		class VoicePool
		{
		public:

			VoicePool() : _entryCount(0)
			{}

            bool ResetState(int index, IXAudio2SourceVoice* voice)
            {
                if (index < 0 || index >= _entryCount)
                {
                    return false;
                }

                VoicePoolEntry& entry = _entries[index];

                for (int i = 0; i < VOICE_COUNT; ++i)
                {
                    if (entry._voice[i] == voice)
                    {
                        entry._state[i] = false;
                        entry._callbacks[i]->SetToken(nullptr);
                        return true;
                    }
                }

                return false;
            }

			bool Find(int index, IXAudio2SourceVoice** voice, DefaultCallback** callback)
			{
				if (index < 0 || index >= _entryCount)
				{
					return false;
				}

				VoicePoolEntry& entry = _entries[index];

				for (int i = 0; i < VOICE_COUNT; ++i)
				{
					if (!entry._state[i])
					{
                        entry._state[i] = true;
						*voice = entry._voice[i];
                        *callback = entry._callbacks[i];
                        return true;
					}
				}

				// maybe have a flag to tell it to queue it anyway?
				return false;
			}

			int Initialize(
				IXAudio2* device,
                SoundEventSink* sink,
				unsigned short bitsPerSample,
				unsigned short numChannels,
				unsigned long sampleRate,
				int format)
			{
				for (int i = 0; i < _entryCount; ++i)
				{
					VoicePoolEntry& entry = _entries[i];
					if (entry._bitsPerSample == bitsPerSample &&
						entry._format == format &&
						entry._numChannels == numChannels &&
						entry._sampleRate == sampleRate)
					{
						return i;
					}
				}

				if (_entryCount == VOICE_POOL_COUNT)
				{
					// No space to store..
					return -1;
				}

				_entries[_entryCount]._bitsPerSample = bitsPerSample;
				_entries[_entryCount]._format = format;
				_entries[_entryCount]._numChannels = numChannels;
				_entries[_entryCount]._sampleRate = sampleRate;

				WAVEFORMATEX wformat;
				wformat.cbSize = 0;
				wformat.nBlockAlign = (bitsPerSample / 8) * numChannels;
				wformat.nSamplesPerSec = sampleRate;
				wformat.nAvgBytesPerSec = wformat.nSamplesPerSec*wformat.nBlockAlign;
				wformat.nChannels = numChannels;
				wformat.wFormatTag = format;
				wformat.wBitsPerSample = bitsPerSample;

				for (int i = 0; i < VOICE_COUNT; ++i)
				{
                    _entries[_entryCount]._state[i] = false;
                    _entries[_entryCount]._callbacks[i] = new DefaultCallback(sink);
                    if (FAILED(device->CreateSourceVoice(&_entries[_entryCount]._voice[i], &wformat, 0, 2.0f, _entries[_entryCount]._callbacks[i])))
					{
						return -1;
					}
				}

				return _entryCount++;
			}

		private:

			struct VoicePoolEntry
			{
				unsigned short _bitsPerSample;
				unsigned short _numChannels;
				unsigned long _sampleRate;
				int _format;
				IXAudio2SourceVoice* _voice[VOICE_COUNT];
                bool _state[VOICE_COUNT];
                DefaultCallback* _callbacks[VOICE_COUNT];
			};

			VoicePoolEntry _entries[VOICE_POOL_COUNT];
			int _entryCount;
		};

		class SoundSystemImpl
		{
		public:
			SoundEventSink* m_sink;
			IXAudio2* m_device;
			IXAudio2MasteringVoice* m_voice;
			VoicePool* m_pool;
			LocklessQueue<AsyncData*> _pending;
			std::vector<Token*> _tokens;
            bool _shuttingDown;
            bool _completed;

			SoundSystemImpl();

			Islander::Sound::Token* SubmitBuffer(unsigned char* data, int size, int loopCount, IXAudio2SourceVoice* voice);
			Islander::Sound::Token* StreamBuffer(Islander::Sound::Sound* sound, IXAudio2SourceVoice* voice);

		private:
			
			std::vector<AsyncData*> _streams;
			std::thread *_worker;
			void ProcessWorkerThread();
		};

		Islander::Sound::Token* SoundSystemImpl::SubmitBuffer(unsigned char* data, int size, int loopCount, IXAudio2SourceVoice* voice)
		{
			Islander::Sound::XAudioToken* token = new Islander::Sound::XAudioToken();
			token->SetState(TokenState::TOKEN_STATE_PLAYING);

			XAUDIO2_BUFFER buffer;
			ZeroMemory(&buffer, sizeof(XAUDIO2_BUFFER));
			buffer.AudioBytes = size;
			buffer.pAudioData = data;
			buffer.Flags = XAUDIO2_END_OF_STREAM;
			buffer.LoopCount = loopCount;
			buffer.pContext = token;

			// Queue the buffer

			if (FAILED(voice->SubmitSourceBuffer(&buffer)))
			{
				token->SetState(TokenState::TOKEN_STATE_ABORTED, "SubmitBuffer");
			}
			else
			{
				token->_voice = voice;
				voice->Start();	
                _tokens.push_back(token);
			}

			return token;
		}

		Islander::Sound::Token* SoundSystemImpl::StreamBuffer(Islander::Sound::Sound* sound, IXAudio2SourceVoice* voice)
		{
			Islander::Sound::XAudioToken* token = new Islander::Sound::XAudioToken();
			token->SetState(TokenState::TOKEN_STATE_PLAYING);

			AsyncData* asyncData = new AsyncData;
			asyncData->_reader = -1;
			asyncData->_writer = -1;
			asyncData->_consumed = -1;
			asyncData->_completed = false;
			asyncData->_remaining_stream_size = sound->_size;
			FileSystem::ReadFile(asyncData->_stream, sound->_filename, true);
			asyncData->_stream.seekg(sound->_offset);
			_pending.Queue(asyncData);

			token->_asyncData = asyncData;
			token->_voice = voice;
			voice->Start();

			_tokens.push_back(token);

			return token;
		}

		Islander::Sound::SoundSystemImpl::SoundSystemImpl()
            : _shuttingDown(false), _completed(false)
		{
			_worker = new std::thread(&Islander::Sound::SoundSystemImpl::ProcessWorkerThread, this);
		}

		void Islander::Sound::SoundSystemImpl::ProcessWorkerThread()
		{
			std::deque<AsyncData*> completed;
			while (!_shuttingDown)
			{
				AsyncData* pending;
				while (_pending.TryDequeue(&pending))
				{
					_streams.push_back(pending);
				}

				for (auto data : _streams)
				{
					if (data->_completed)
					{
						completed.push_back(data);
						continue;
					}

					while (data->_consumed + ASYNC_DATA_BUFFER_COUNT > data->_writer && data->_remaining_stream_size > 0)
					{
						int size = min(data->_remaining_stream_size, ASYNC_DATA_BUFFER_SIZE);
						if (size > 0)
						{
							data->_stream.read(
								(char*)data->_data[(data->_writer + 1) % ASYNC_DATA_BUFFER_COUNT],
								size);
							data->_remaining_stream_size -= size;
							data->_writer++;
						}
					}
				}

				while (completed.size() > 0)
				{
					AsyncData* data = completed.front();
					data->_stream.close();
					_streams.erase(std::find(_streams.begin(), _streams.end(), data));
					delete data;
					completed.pop_front();
				}

				// TODO: replace with wait condition?
				std::this_thread::sleep_for(std::chrono::milliseconds(10));
			}

            _completed = true;
		}
	}
}

SoundInterface::SoundInterface()
{
	_impl = new SoundSystemImpl();

	CoInitializeEx(NULL, COINIT_MULTITHREADED);

	UINT32 flags = 0;
#ifdef _DEBUG
	flags = XAUDIO2_DEBUG_ENGINE;
#endif

	auto result = FAILED(XAudio2Create(&_impl->m_device, flags));

#ifdef _DEBUG
	if (result)
	{
		std::stringstream ss;
		ss << "Unable to create debug audio engine, falling back on normal";

		Islander::Logger::GetLogger()->Log(ss.str(), LOGGING_INFO);

		result = FAILED(XAudio2Create(&_impl->m_device, 0));
	}
#endif

	if (result)
	{
		std::stringstream ss;
		ss << "Unable to initialize XAudio2 (" << GetLastError << ")";

		Islander::Logger::GetLogger()->Log(ss.str(), LOGGING_INFO);
		return;
	}

	if (FAILED(_impl->m_device->CreateMasteringVoice(&_impl->m_voice)))
	{
		std::stringstream ss;
		ss << "Unable to create mastering voice (" << GetLastError << ")";

		Islander::Logger::GetLogger()->Log(ss.str(), LOGGING_INFO);
		return;
	}

#ifdef _DEBUG
	XAUDIO2_DEBUG_CONFIGURATION debug;
	debug.BreakMask = XAUDIO2_LOG_WARNINGS;
	debug.TraceMask = XAUDIO2_LOG_WARNINGS;

	_impl->m_device->SetDebugConfiguration(&debug);
#endif

	_impl->m_sink = new SoundEventSink();
	_impl->m_pool = new VoicePool();

	//m_initialized = true;
}

SoundInterface::~SoundInterface()
{
    _impl->_shuttingDown = true;
    while (!_impl->_completed) { std::this_thread::sleep_for(std::chrono::milliseconds(10)); } // spin crapily until done

	delete _impl->m_pool;
	delete _impl->m_sink;
	_impl->m_device->Release();
	CoUninitialize();
	delete _impl;
}

void SoundInterface::PlayAudio(Token* token)
{
	auto asyncData = ((XAudioToken*)token)->_asyncData;
	auto ptr = asyncData->_data[(asyncData->_reader + 1) % ASYNC_DATA_BUFFER_COUNT];

	XAUDIO2_BUFFER buffer;
	ZeroMemory(&buffer, sizeof(XAUDIO2_BUFFER));
	buffer.AudioBytes = ASYNC_DATA_BUFFER_SIZE;
	buffer.pAudioData = ptr;
	buffer.Flags = asyncData->_remaining_stream_size == 0 &&
		asyncData->_reader + 1 == asyncData->_writer ? XAUDIO2_END_OF_STREAM : 0;
	buffer.LoopCount = 0;
	buffer.pContext = token;

	// Queue the buffer

	if (FAILED(((XAudioToken*)token)->_voice->SubmitSourceBuffer(&buffer)))
	{
		token->SetState(TokenState::TOKEN_STATE_ABORTED, "SubmitSourceBuffer");
	}
    else if (buffer.Flags == XAUDIO2_END_OF_STREAM)
    {
        token->SetState(TokenState::TOKEN_STATE_COMPLETING, "XAUDIO2_END_OF_STREAM");
    }
    else
    {
        token->SetState(TokenState::TOKEN_STATE_PLAYING);
    }

	asyncData->_reader++;
}

void SoundInterface::ProcessEventQueue(IslanderAudioEvent* ev)
{
	_impl->m_sink->Dequeue(ev);

    if (ev->_token != nullptr &&
        ev->_type == EventType::SOUND_EVENT_TYPE_STREAM_END)
    {
        auto xaudio = (XAudioToken*)ev->_token;
        auto voice = xaudio->_voice;

        voice->Stop();
        voice->FlushSourceBuffers();
        xaudio->SetState(TokenState::TOKEN_STATE_COMPLETED, "SOUND_EVENT_TYPE_STREAM_END");

        if (!_impl->m_pool->ResetState(xaudio->GetType(), voice))
        {
            Islander::Logger::GetLogger()->Log("Error: Sound Interface failed to reset voice state.", LOGGING_INFO);
        }

        auto it = std::find(_impl->_tokens.begin(), _impl->_tokens.end(), ev->_token);
        if (it != _impl->_tokens.end())
        {
            _impl->_tokens.erase(it);
        }
    }

	// When buffer ends, play next part (if ready)
	else if (ev->_token != nullptr &&
		ev->_type == EventType::SOUND_EVENT_TYPE_BUFFER_END)
	{
        if (std::find(_impl->_tokens.begin(), _impl->_tokens.end(), ev->_token) != _impl->_tokens.end())
        {
			auto xaudio = (XAudioToken*)ev->_token;
            if (xaudio->GetState() != TokenState::TOKEN_STATE_ABORTED && xaudio->GetState() != TokenState::TOKEN_STATE_COMPLETED)
            {
                auto xaudio = (XAudioToken*)ev->_token;
                auto asyncData = xaudio->_asyncData;
                if (asyncData != nullptr)
                {
                    asyncData->_consumed++;
                }
            }
        }
	}

	for (auto token : _impl->_tokens)
	{
		if (token->GetState() != TokenState::TOKEN_STATE_COMPLETED && token->GetState() != TokenState::TOKEN_STATE_ABORTED)
		{
			auto asyncData = ((XAudioToken*)token)->_asyncData;
            if (asyncData != nullptr)
            {
                while (asyncData->_writer > asyncData->_reader)
                {
                    PlayAudio(token);
                }
            }
		}
	}

#if DEBUG
	XAUDIO2_PERFORMANCE_DATA perfData;
	_impl->m_device->GetPerformanceData(&perfData);
#endif
}

void SoundInterface::UpdateCategoryVolume(int category, float oldVolume, float newVolume)
{
	for (auto token : _impl->_tokens)
	{
		if (token->GetCategory() == category)
		{
			token->SetVolume(newVolume);
		}
	}
}

bool SoundInterface::IsValidToken(Token* token)
{
    auto it = std::find(_impl->_tokens.begin(), _impl->_tokens.end(), token);
    if (it != _impl->_tokens.end())
    {
        return true;
    }

    return false;
}

void SoundInterface::SetAudioOn(bool on) const
{
	on ? _impl->m_voice->SetVolume(1) : _impl->m_voice->SetVolume(0);
}

Islander::Sound::Token* SoundInterface::StartAudio(Sound* sound, int loopCount)
{
    IXAudio2SourceVoice* voice = nullptr;
    DefaultCallback* callback = nullptr;

	if (_impl->m_pool->Find(sound->_soundType, &voice, &callback))
	{
		Islander::Sound::Token *token = nullptr;
		if (sound->_async)
		{
			token = _impl->StreamBuffer(sound, voice);
		}
		else
		{
			token = _impl->SubmitBuffer(sound->_data, sound->_size, loopCount, voice);
		}

        ((XAudioToken*)token)->Mute(false);
		token->SetCategory(sound->_category);
        token->SetType(sound->_soundType);
        callback->SetToken(token);
		return token;
	}

	return nullptr;
}

void SoundInterface::EndAudio(Token* token)
{
	auto voice = ((XAudioToken*)token)->_voice;

	if (token->GetState() != TokenState::TOKEN_STATE_COMPLETED && token->GetState() != TokenState::TOKEN_STATE_ABORTED && token->GetState() != TokenState::TOKEN_STATE_COMPLETING)
	{
        auto asyncData = ((XAudioToken*)token)->_asyncData;
        if (asyncData)
        {
            // Queue a slient buffer with XAUDIO2_END_OF_STREAM flag, this will guarentee the stream will end safely.

            XAUDIO2_BUFFER buffer;
            ZeroMemory(&buffer, sizeof(XAUDIO2_BUFFER));
            buffer.AudioBytes = sizeof(empty_data_block);
            buffer.pAudioData = empty_data_block;
            buffer.Flags = XAUDIO2_END_OF_STREAM;
            buffer.LoopCount = 0;
            buffer.pContext = token;

            ((XAudioToken*)token)->Mute(true);

            // Queue the buffer

            if (FAILED(voice->SubmitSourceBuffer(&buffer)))
            {
                Logger::GetLogger()->Log("Error: Unable to submit source buffer.", LOGGING_INFO);
            }

            token->SetState(TokenState::TOKEN_STATE_ABORTED, "EndAudio");

			asyncData->_completed = true;
			((XAudioToken*)token)->_asyncData = nullptr;
		}
        else
        {
            ((XAudioToken*)token)->Mute(true);
        }
	}
}

Sound* SoundInterface::CreateSound(const SoundInfo& info, char* data, int dataSize, int category, int offset)
{
	int soundType = _impl->m_pool->Initialize(
		_impl->m_device,
		_impl->m_sink,
		info.bitsPerSample,
		info.numChannels,
		info.sampleRate,
		info.pcm ? WAVE_FORMAT_PCM : WAVE_FORMAT_UNKNOWN);

	if (soundType == -1)
	{
		return nullptr;
	}

	Sound* sound = new Sound();
	sound->_soundType = soundType;
	sound->_data = reinterpret_cast<unsigned char*>(data);
	sound->_category = category;
	sound->_size = dataSize;
	sound->_async = sound->_data == nullptr;
	sound->_filename = info.filename;
	sound->_offset = offset;

	return sound;
}
