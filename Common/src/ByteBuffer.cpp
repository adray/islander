#include "ByteBuffer.h"
#include <string>
extern "C"
{
#define ZLIB_WINAPI
#include <zlib.h>
}

using namespace Islander;

Encoder:: Encoder(unsigned char* data, int size)
{
	_inbuffer = data;
	_insize = size;
	_outbuffer = NULL;
	_outsize = 0;
}

void Encoder:: Encode(unsigned char** out, int *outsize)
{

	_outsize = _insize * 2;	//max

	_outbuffer = new unsigned char[_outsize];
		
	compress(_outbuffer, (uLongf*)&_outsize, _inbuffer, _insize);

	*out = _outbuffer;
	*outsize = _outsize;

}

void Encoder:: Decode(unsigned char** out, int* outsize)
{

	_outsize = _insize * 10;	//max

	_outbuffer = new unsigned char[_outsize];

	int code = uncompress(_outbuffer, (uLongf*)&_outsize, _inbuffer, _insize);

	if (code != Z_OK)
		return;

	*out = _outbuffer;
	*outsize = _outsize;

}

Encoder:: ~Encoder()
{
	delete[] _outbuffer;
}


ByteBuffer::ByteBuffer(unsigned char* data, unsigned int length) : _data(data), _position(0), _length(length)
{
}



void ByteBuffer:: WriteString(const std::wstring& string)
{

	int size = sizeof(wchar_t);

	WriteByte(string.size());
	for (int i = 0; i < string.size(); i++)
	{
		for (int j = 0; j < size; j++)
		{
			unsigned char o = (string[i] >> j*8) & 0xFF;
			WriteByte(o);
		}
	}

}

void ByteBuffer:: ReadString(std::wstring * string)
{
	std::wstring str=L"";
	int size = sizeof(wchar_t);

	int len = ReadByte();
	for (int i = 0; i < len; i++)
	{
		wchar_t c=0;
		for (int j = 0; j < size; j++)
		{
			auto b = ReadByte() << j*8;
			c |= b;
		}
		str.push_back(c);
	}

	*string = str;
}
