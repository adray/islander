#include "Component.h"

extern "C"
{
#include <entity.h>
}

using namespace Islander;

Event::Event(Component* source) : _source(source)
{

}

void Event::GetSource(Component& source)
{
	source = *_source;
}

Event::Event(const Event& e) : _source(e._source)
{
}
