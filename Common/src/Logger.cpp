#include "Logger.h"
#include <fstream>
#include <sstream>
#include <time.h>
#include "MachineStats.h"

using namespace Islander;

LoggerStream::LoggerStream(std::ofstream* stream) : _stream(stream)
{
}

LoggerStream::~LoggerStream(void)
{
}

Logger* Logger::_logger=NULL;

Logger::Logger(void) : _flags(LOGGING_ALL)
{
	std::stringstream ss;
	ss << "islander_log.txt";
	_logfile = ss.str();
	_stream = std::ofstream(_logfile, std::ios::out|std::ios::app);
	
    Log("Starting session", LOGGING_INFO);

	std::stringstream loginfo;
	loginfo << "Processors: " << Machine::NumberOfProcessors();
	Log(loginfo.str(), LOGGING_INFO);

    _logStream = new LoggerStream(&_stream);
    _nullLogStream = new LoggerStream(nullptr);
}

Logger::~Logger(void)
{
}

Logger* Logger:: GetLogger()
{
	if (_logger == NULL)
		_logger = new Logger();
	return Logger::_logger;
}

LoggerStream& Logger::GetStream(LoggingChannels channel)
{
    if (_logger == NULL)
        _logger = new Logger();
    if ((_logger->_flags & channel) != channel)
        return *Logger::_logger->_nullLogStream;

    return *Logger::_logger->_logStream;
}

void Logger:: Log(const std::string& message, LoggingChannels channel)
{
	if ((_flags & channel) != channel)
		return;

	if (_stream.bad())
		return;
	if (_stream.fail())
		return;

	_stream << message.c_str() << std::endl;

	_stream.flush();
}

void Logger:: SetLoggingFlags(int flags)
{
	_flags = flags;
}
