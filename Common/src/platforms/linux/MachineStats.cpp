#include "MachineStats.h"
#include <thread>
#include <unistd.h>

using namespace Islander;

int Machine::NumberOfProcessors()
{  
	return std::thread::hardware_concurrency();
}

void Machine::GetName(char* name, int length)
{
	gethostname(name, length);
}

void Machine::GetMemoryUsage(MachineMemoryInfomation& mInfo)
{
    // TODO
}

uint64_t Machine::GetProcessTicks()
{
    // TODO
	return 0;
}
