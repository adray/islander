#include "Stopwatch.h"
#include <time.h>

using namespace Islander;

namespace Islander
{
	struct StopwatchInternal
	{
		timespec spec;
	};
}

Stopwatch::Stopwatch()
{
	Init();
}


Stopwatch:: ~Stopwatch(void)
{
	delete impl;
}

Stopwatch:: Stopwatch(const Stopwatch& other)
{
	Init();
}

void Stopwatch:: Update()
{
	auto prev = impl->spec;
	clock_gettime(CLOCK_MONOTONIC, &impl->spec);
	if (isrunning)
	{
		current_elapsed += impl->spec.tv_nsec - prev.tv_nsec;
	}
}

void Stopwatch:: Init()
{
	impl = new StopwatchInternal();
	isrunning = false;
	current_elapsed = 0;
}

unsigned int Stopwatch:: GetElapsedMilliseconds()
{
	Update();
	return current_elapsed / 1000000;
}

void Stopwatch:: Start()
{
	isrunning = true;
	clock_gettime(CLOCK_MONOTONIC, &impl->spec);
}

void Stopwatch:: Stop()
{
	isrunning = false;
	clock_gettime(CLOCK_MONOTONIC, &impl->spec);
}

void Stopwatch:: Reset()
{
	current_elapsed = 0;
}

void Stopwatch:: Restart()
{
	Stop();
	Reset();
	Start();
}

bool Stopwatch:: IsRunning() const
{
	return isrunning;
}
