#include "Atomics.h"

long AtomicIncrement(volatile long* value)
{
	__sync_add_and_fetch(value, 1);
}

long AtomicDecrement(volatile long* value)
{
	__sync_sub_and_fetch(value, 1);
}

