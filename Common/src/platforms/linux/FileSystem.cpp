#include "FileSystem.h"
#include "sys/types.h"
#include "dirent.h"

using namespace Islander;

void FileSystem::AppendFile(std::ofstream& file, const PathUTF8& path, bool binary)
{
	auto flags = std::ios::app | std::ios::out;
	if (binary)
	{
		flags |= std::ios::binary;
	}
	file.open(path, flags);
}

void FileSystem::WriteFile(std::ofstream& file, const PathUTF8& path, bool binary)
{
	auto flags = std::ios::out;
	if (binary)
	{
		flags |= std::ios::binary;
	}
	file.open(path, flags);
}

void FileSystem::ReadFile(std::ifstream& file, const PathUTF8& path, bool binary)
{
	auto flags = std::ios::in;
	if (binary)
	{
		flags |= std::ios::binary;
	}
	file.open(path, flags);
}

bool FileSystem::Exists(const PathUTF8& path)
{
	return true;
}

bool FileSystem::Delete(const PathUTF8& path)
{
    return false;
}

void FileSystem::Copy(const PathUTF8& sourcePath, const PathUTF8& targetPath, bool failIfExists)
{
    // TODO
}

void FileSystem::GetFileName(const PathUTF8& path, PathUTF8& filename)
{
	filename = "";
	for (int i = path.size() - 1; i >= 0; i--)
	{
		auto c = path[i];
		if (c == '\\' || c == '/')
		{
			break;
		}

		filename = c + filename;
	}
}

void* FileSystem::GetFirstFile(const PathUTF8& dir, PathUTF8& path)
{
	DIR* h = opendir(dir.c_str());
	dirent* entry = readdir(h);

	if (entry != nullptr)
	{
		path = entry->d_name;
	}
	
	return h;
}
		
bool FileSystem::GetNextFile(void* handle, PathUTF8& path)
{
	dirent* entry = readdir((DIR*)handle);

	if (entry != nullptr)
	{
		path = entry->d_name;
		return true;
	}
	
	return false;
}

void* FileSystem::GetFirstDirectory(const PathUTF8& dir, PathUTF8& path)
{
    // TODO
    return nullptr;
}

bool FileSystem::GetNextDirectory(void* handle, PathUTF8& path)
{
    // TODO
    return false;
}

void FileSystem::GetAbsolutePath(PathUTF8& path)
{
    // TODO
}

void FileSystem::RunProcess(const PathUTF8& path, const PathUTF8& commandLineArgs)
{
    // TODO
}
