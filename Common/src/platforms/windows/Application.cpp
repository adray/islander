#include "Application.h"
#include <excpt.h>
#include <Windows.h>
#include <Dbghelp.h>

#pragma comment(lib, "Dbghelp.lib")

void DumpProcess(PEXCEPTION_POINTERS exception)
{
    // Generate dump
    HANDLE handle = GetCurrentProcess();
    if (handle)
    {
        DWORD id = GetProcessId(handle);

        HANDLE file = CreateFile(L"crash.dmp", GENERIC_WRITE, 0, nullptr, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, nullptr);
        if (file)
        {
            MINIDUMP_EXCEPTION_INFORMATION info;
            info.ThreadId = GetCurrentThreadId();
            info.ExceptionPointers = exception;
            info.ClientPointers = TRUE; // depends

            MiniDumpWriteDump(
                handle,
                id,
                file,
                MiniDumpWithFullMemory,
                &info,
                nullptr,
                nullptr);

            CloseHandle(file);
        }
    }
}

int filter(unsigned int code, struct _EXCEPTION_POINTERS* ep)
{
    return EXCEPTION_EXECUTE_HANDLER;
}

void Islander::Application::OnStart()
{
    // Empty
}

void Islander::Application::Start()
{
    PEXCEPTION_POINTERS exception;
    if (IsDebuggerPresent())
    {
        OnStart();
    }
    else
    {
        __try
        {
            OnStart();
        }
        __except (filter(GetExceptionCode(), exception = GetExceptionInformation()))
        {
            DumpProcess(exception);
        }
    }
}

Islander::Application::~Application()
{
    // Empty
}
