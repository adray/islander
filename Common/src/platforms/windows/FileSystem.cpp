#include "FileSystem.h"
#include <locale>
#include <codecvt>
#include <algorithm>

#define WIN32_MEAN_AND_LEAN
#define NOMINMAX
#include <Windows.h>

using namespace Islander;

void FileSystem::AppendFile(std::ofstream& file, const PathUTF8& path, bool binary)
{
	std::wstring_convert<std::codecvt_utf8_utf16<wchar_t> > converter;
	auto flags = std::ios::app | std::ios::out;
	if (binary)
	{
		flags |= std::ios::binary;
	}
	file.open(converter.from_bytes(path), flags);
}

void FileSystem::WriteFile(std::ofstream& file, const PathUTF8& path, bool binary)
{
	std::wstring_convert<std::codecvt_utf8_utf16<wchar_t> > converter;
	auto flags = (int)std::ios::out;
	if (binary)
	{
		flags |= std::ios::binary;
	}
	file.open(converter.from_bytes(path), flags);
}

void FileSystem::ReadFile(std::ifstream& file, const PathUTF8& path, bool binary)
{
	std::wstring_convert<std::codecvt_utf8_utf16<wchar_t> > converter;
	auto flags = (int)std::ios::in;
	if (binary)
	{
		flags |= std::ios::binary;
	}
	file.open(converter.from_bytes(path), flags);
}

bool FileSystem::Exists(const PathUTF8& path)
{
	std::wstring_convert<std::codecvt_utf8_utf16<wchar_t> > converter;
	int attribs = GetFileAttributes(converter.from_bytes(path).c_str());

	return attribs != INVALID_FILE_ATTRIBUTES && !(attribs & FILE_ATTRIBUTE_DIRECTORY);
}

bool FileSystem::Delete(const PathUTF8& path)
{
	std::wstring_convert<std::codecvt_utf8_utf16<wchar_t> > converter;
	return DeleteFile(converter.from_bytes(path).c_str());
}

void FileSystem::Copy(const PathUTF8& sourcePath, const PathUTF8& targetPath, bool failIfExists)
{
	std::wstring_convert<std::codecvt_utf8_utf16<wchar_t> > converter;
	CopyFile(converter.from_bytes(sourcePath).c_str(), converter.from_bytes(targetPath).c_str(), failIfExists);
}

void FileSystem::GetFileName(const PathUTF8& path, PathUTF8& filename)
{
	filename = "";
	for (int i = path.size() - 1; i >= 0; i--)
	{
		auto c = path[i];
		if (c == '\\' || c == '/')
		{
			break;
		}

		filename = c + filename;
	}
}

void* FileSystem::GetFirstFile(const PathUTF8& dir, PathUTF8& path)
{
	std::string search = dir + "/*";
	std::wstring_convert<std::codecvt_utf8_utf16<wchar_t> > converter;

	WIN32_FIND_DATA info;

	auto handle = FindFirstFile(converter.from_bytes(search).c_str(), &info);
	if (handle == INVALID_HANDLE_VALUE)
	{
		return nullptr;
	}

	if ((info.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) == FILE_ATTRIBUTE_DIRECTORY)
	{
		return GetNextFile(handle, path) ? handle : nullptr;
	}

	path = converter.to_bytes(info.cFileName);
	return handle;
}

bool FileSystem::GetNextFile(void* handle, PathUTF8& path)
{
	WIN32_FIND_DATA info;

	bool success = FindNextFile(handle, &info);

	if (success)
	{
		do
		{
			if ((info.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) != FILE_ATTRIBUTE_DIRECTORY)
			{
				break;
			}
		} while (success = FindNextFile(handle, &info));
	}
	
	if (!success)
	{
		FindClose(handle);
		return false;
	}

	std::wstring_convert<std::codecvt_utf8_utf16<wchar_t> > converter;
	path = converter.to_bytes(info.cFileName);
	return true;
}

void* FileSystem::GetFirstDirectory(const PathUTF8& dir, PathUTF8& path)
{
	std::string search = dir + "/*";
	std::wstring_convert<std::codecvt_utf8_utf16<wchar_t> > converter;

	WIN32_FIND_DATA info;

	auto handle = FindFirstFile(converter.from_bytes(search).c_str(), &info);
	if (handle == INVALID_HANDLE_VALUE)
	{
		return nullptr;
	}

	if ((info.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) != FILE_ATTRIBUTE_DIRECTORY)
	{
		return GetNextDirectory(handle, path) ? handle : nullptr;
	}

	path = converter.to_bytes(info.cFileName);
	return handle;
}

bool FileSystem::GetNextDirectory(void* handle, PathUTF8& path)
{
	WIN32_FIND_DATA info;

	bool success = FindNextFile(handle, &info);

	if (success)
	{
		do
		{
			if ((info.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) == FILE_ATTRIBUTE_DIRECTORY)
			{
				break;
			}
		} while (success = FindNextFile(handle, &info));
	}

	if (!success)
	{
		FindClose(handle);
		return false;
	}

	std::wstring_convert<std::codecvt_utf8_utf16<wchar_t> > converter;
	path = converter.to_bytes(info.cFileName);
	return true;
}

void FileSystem::GetAbsolutePath(PathUTF8& path)
{
	std::wstring_convert<std::codecvt_utf8_utf16<wchar_t> > converter;
	auto wpath = converter.from_bytes(path);

	// TODO: vs complains if the divide by 2 is removed
	TCHAR fullpath[MAX_PATH];
	if (/*SUCCEEDED*/(GetFullPathName(wpath.c_str(), sizeof(TCHAR) * MAX_PATH/2, fullpath, nullptr)))
	{
		path = converter.to_bytes(fullpath);
	}
}

void FileSystem::RunProcess(const PathUTF8& path, const PathUTF8& commandLineArgs)
{
	std::wstring_convert<std::codecvt_utf8_utf16<wchar_t> > converter;
	auto wpath = converter.from_bytes(path);
	auto cmd = converter.from_bytes(commandLineArgs);
	constexpr int len = 512;
	wchar_t commandLine[len];
	memcpy(commandLine, cmd.c_str(), cmd.length() * sizeof(wchar_t));
	commandLine[std::min(len - 1, (int)cmd.length())] = '\0';

	STARTUPINFO si;
	PROCESS_INFORMATION pi;
	memset(&si, 0, sizeof(si));
	memset(&pi, 0, sizeof(pi));

	CreateProcess(
		wpath.c_str(),
		commandLine,
		nullptr,
		nullptr,
		false,
		0,
		nullptr,
		nullptr,
		&si,
		&pi);

	WaitForSingleObject(pi.hProcess, 5000);

	CloseHandle(pi.hProcess);
	CloseHandle(pi.hThread);
}
