#include <Atomics.h>
#include <windows.h>

long AtomicIncrement(volatile long* value)
{
	return InterlockedIncrement(value);
}

long AtomicDecrement(volatile long* value)
{
	return InterlockedDecrement(value);
}
