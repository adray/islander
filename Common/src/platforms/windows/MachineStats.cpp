#include "MachineStats.h"

#define WINDOWS_LEAN_AND_MEAN
#include <Windows.h>

#include <Psapi.h>
#include <memory>

using namespace Islander;


int Machine::NumberOfProcessors()
{
	SYSTEM_INFO info;
	GetSystemInfo(&info);
	return info.dwNumberOfProcessors;
}

void Machine::GetName(char* name, int length)
{
	DWORD size;
	GetComputerNameA(name, (unsigned long*)&length);
}

void Machine::GetMemoryUsage(MachineMemoryInfomation& mInfo)
{
	std::memset(&mInfo, 0, sizeof(mInfo));

	int procId = GetCurrentProcessId();
	HANDLE h = OpenProcess(PROCESS_QUERY_INFORMATION, PROCESS_VM_READ, procId);
	if (h)
	{
		APP_MEMORY_INFORMATION info;
		if (GetProcessInformation(h, ProcessAppMemoryInfo, &info, sizeof(info)))
		{
			mInfo.availableCommit = info.AvailableCommit;
			mInfo.peakPrivateCommitUsage = info.PeakPrivateCommitUsage;
			mInfo.privateCommitUsage = info.PrivateCommitUsage;
			mInfo.totalCommitUsage = info.TotalCommitUsage;
		}

		PROCESS_MEMORY_COUNTERS_EX counters;
		if (GetProcessMemoryInfo(h, (PROCESS_MEMORY_COUNTERS*) &counters, sizeof(counters)))
		{
			mInfo.workingSetSize = counters.WorkingSetSize;
			mInfo.peakWorkingSetSize = counters.PeakWorkingSetSize;
			mInfo.privateUsage = counters.PrivateUsage;
		}

		CloseHandle(h);
	}
}

uint64_t Machine::GetProcessTicks()
{
	return GetTickCount64();
}
