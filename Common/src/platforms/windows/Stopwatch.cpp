#include "Stopwatch.h"
#define WIN32_LEAN_AND_MEAN
#include <windows.h>

using namespace Islander;

namespace Islander
{
	struct StopwatchInternal
	{
		LARGE_INTEGER freq;
		LARGE_INTEGER start_time;
	};
}

Stopwatch::Stopwatch(void)
{
	Init();
}

void Stopwatch::Init()
{
	impl = new StopwatchInternal();
	QueryPerformanceFrequency(&impl->freq);
	isrunning = false;
	current_elapsed = 0;
	ZeroMemory(&impl->start_time, sizeof(LARGE_INTEGER));
}

Stopwatch::Stopwatch(const Stopwatch& other)
{
	Init();

	impl->freq = other.impl->freq;
	impl->start_time = other.impl->start_time;
	isrunning = other.isrunning;
	current_elapsed = other.current_elapsed;
}

Stopwatch::~Stopwatch(void)
{
	delete impl;
}

void Stopwatch::Start()
{

	QueryPerformanceCounter(&impl->start_time);
	isrunning=true;

}

void Stopwatch::Update()
{

	if (!isrunning)
		return;

	LARGE_INTEGER current;
	QueryPerformanceCounter(&current);

	UINT elapsed = (current.LowPart - impl->start_time.LowPart) * 1000 / (float)impl->freq.QuadPart;
	current_elapsed += elapsed;

	if (elapsed >= 1)
		QueryPerformanceCounter(&impl->start_time);

}

UINT Stopwatch::GetElapsedMilliseconds()
{
	Update();
	return current_elapsed;
}

void Stopwatch::Stop()
{
	Update();
	isrunning=false;
}

void Stopwatch::Reset()
{
	current_elapsed=0;
}

bool Stopwatch::IsRunning() const
{
	return isrunning;
}

void Stopwatch::Restart()
{
	Stop();
	Reset();
	Start();
}