
#ifdef Islander_Common_EXPORTS
#ifdef _WIN32
#define COMMON_EXPORT __declspec( dllexport )
#else
#define COMMON_EXPORT __attribute__ ((visibility("default")))
#endif
#else
#if _WIN32
#define COMMON_EXPORT __declspec( dllimport )
#else
#define COMMON_EXPORT __attribute__ ((visibility("default")))
#endif
#endif
