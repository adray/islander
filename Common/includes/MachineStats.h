#pragma once
#include <cstdint>

namespace Islander
{
	struct MachineMemoryInfomation
	{
		uint64_t availableCommit;
		uint64_t privateCommitUsage;
		uint64_t peakPrivateCommitUsage;
		uint64_t totalCommitUsage;
		uint64_t workingSetSize;
		uint64_t privateUsage;
		uint64_t peakWorkingSetSize;
	};

	class Machine
	{
	public:

		static int NumberOfProcessors();

		static void GetName(char* name, int length);

		static void GetMemoryUsage(MachineMemoryInfomation& mInfo);

		static uint64_t GetProcessTicks();
	};

}
