#pragma once
#include <string>
#include <cstring>
#include <vector>
#include <memory>
#include <queue>
#include <cmath>
#include "Vector3.h"
#include "Exports.h"

#undef SendMessage

struct ISLANDER_COMPONENT_SCHEMA;
struct row_entity;
struct property_entry;

namespace Islander
{

	enum ComponentType
	{
		COMPONENT_ENTITY=0,
		COMPONENT_SPRITE=1,
		COMPONENT_TRANSFORM=2,
		COMPONENT_MESH=3,
		COMPONENT_TEXT=4
	};

	typedef struct Rect
	{
		int x, y, width, height;
	} RECTANGLE;

	enum TextDataCleanupStyle
	{
		ISLANDER_TEXT_DATA_CLEANUP_STYLE_NATIVE = 1,
		ISLANDER_TEXT_DATA_CLEANUP_STYLE_NOT_NATIVE = 2,
	};

	struct LinkElement
	{
		RECTANGLE _rect;
		int _startIndex;
		int _length;
	};

	struct TextData
	{
		const char* _text;
		RECTANGLE _rect;
		int tabs[32];
		char* _font;
		int centered;
		int cleanup;
		LinkElement _links[16];
		int _linkcount;
		int truetype_size;
	};

extern "C"
{
	struct component_texture
	{
		int index;
		int source;
		int atlasIndex;
		float px;
		float py;
		float sx;
		float sy;
	};

	struct component_texture_composite
	{
		float width;
		float height;
		int subtexturecount;
		struct component_texture texture;
		struct component_texture subtexture[16];
	};

	struct component_submaterial
	{
		struct component_texture textures[4];
	};

	struct component_material
	{
		int pass;
		char vertexShader;
		char pixelShader;
		char geometryShader;
		char submaterialcount;
		char submaterialtype;
		float clipping;
		struct component_submaterial* submaterials;
		struct component_texture textures[4];
	};

	struct component_sprite
	{
		int zindex;
		int wrap;
		struct component_material material;
	};

	struct component_mesh_config
	{
		float* data;
        int* index;
		int count;
        int index_count;
		int stride;
	};

	struct component_mesh
	{
		struct component_material material;
		struct component_mesh_config config;
		int meshId;
        int meshIndexId;
		int dirty;
        bool wireframe;
        bool debugbones;
        void* polydata;
        int controllerId;
        void* animationSystem;
        int parentEntity;
        int parentBone;
		void* constantBufferData;
		int constantBufferDataSize;
	};

	struct component_colour
	{
		float r;
		float g;
		float b;
		float a;
	};

	struct component_text
	{
		struct component_colour _colour;
		int _vertid;
		int dirty;
		int vertex;
		int pixel;
		int img;
		int pass;
		int wrap;
		void* data;
	};

	struct component_transform
	{
		float px;
		float py;
		float pz;
		float sx;
		float sy;
		float sz;
		float dx;
		float dy;
		float dz;
        float shearX;
        float shearY;
	};

    struct component_voxel_array
    {
        int32_t* data;
        int size;
    };
}

class Component;

class /*COMMON_EXPORT*/ Event
{

	Component* _source;

public:

	Event(Component* source);

	void GetSource(Component& source);

	Event(const Event& e);

	virtual ~Event(){};

};

class /*COMMON_EXPORT*/ Component
{
	std::wstring _name;
	bool _enabled;
	std::queue<std::shared_ptr<Event>>* _events;
	bool _custom;
	
public:

	Component(const std::wstring& name) : _name(name), _events(NULL), _custom(false) {}

	Component(const std::wstring& name, bool custom) : _name(name), _events(NULL), _custom(custom) {}

	virtual ~Component()
	{
	}

	bool IsCustom()
	{
		return _custom;
	}

protected:

	void SendMessage(std::shared_ptr<Event> e)
	{
		if (_events == NULL)
		{
			_events = new std::queue<std::shared_ptr<Event>>;
		}
		_events->push(std::shared_ptr<Event>(e));
	}

	void SetName(const std::wstring& name) { _name = name; }

public:

	std::wstring GetName() { return _name; }

	bool GetNextMessage(std::shared_ptr<Event>* e)
	{
		if (_events == NULL)
		{
			_events = new std::queue<std::shared_ptr<Event>>;
		}
		if (_events->size() > 0)
		{
			std::shared_ptr<Event> ev = _events->front();
			*e = ev;
			_events->pop();
			return true;
		}
		return false;
	}

};

class Camera3D
{
public:
    Vec3f pos;
    Vec3f look;
	union
	{
		float fovY;
		float width;
	};
	union
	{
		float aspect;
		float height;
	};
    float nearZ;
    float farZ;
	bool orthographic;
};

template<class t>
class Camera : public Component
{

	t _x;
	t _y;
	t _z;

public:

	Camera() : Component(L"Camera"), _x(0), _y(0), _z(1)
	{

	}

	Camera(t x, t y) : Component(L"Camera"), _x(x), _y(y)
	{

	}

	inline void Translate(t dx, t dy) { _x += dx; _y += dy; }

	inline t GetX() { return _x; }
	inline t GetY() { return _y; }
	inline t GetZ() { return _z; }

	inline void SetX(t x) { _x = x; }
	inline void SetY(t y) { _y = y; }
	inline void SetZ(t z) { _z = z; }
};

typedef Camera<int> Camerai;
typedef Camera<float> Cameraf;
}
