#pragma once
#include "Atomics.h"
#include <algorithm>

namespace Islander
{
	template<typename T>
	// Queue safe for one writer and one reader.
	class LocklessQueue
	{
		template<typename K>
		struct Section
		{
			Section<K>* _next;
			K _data[100];
			volatile long _reader;
			volatile long _writer;
			const int _length;

			Section() :
				_reader(-1),
				_writer(-1),
				_next(nullptr),
				_length(100)
			{}
		};
	public:

		LocklessQueue() :
			_size(0)
		{
			_head = new Section<T>();
			_tail = _head;
		}

		void Queue(const T& item)
		{
			_tail->_data[_tail->_writer + 1] = item;
			auto tmp = _tail;
			if (tmp->_writer + 1 == _tail->_length - 1)
			{
				auto next = new Section<T>();
				_tail = next;
				tmp->_next = next;
			}
			AtomicIncrement(&tmp->_writer);
			AtomicIncrement(&_size);
		}

		bool TryDequeue(T* item)
		{
			Ensure();
			if (_head->_reader < _head->_writer)
			{
				*item = _head->_data[_head->_reader + 1];
				AtomicDecrement(&_size);
				_head->_reader++;
				Ensure();
				return true;
			}
			return false;
		}

		~LocklessQueue()
		{
			while (_head != nullptr)
			{
				auto tmp = _head;
				_head = _head->_next;
				delete tmp;
			}
		}

		inline long Size() const
		{
			return _size;
		}

	private:

		LocklessQueue(const LocklessQueue<T>& copy);
		LocklessQueue<T>* operator =(LocklessQueue<T>& other);

		void Ensure()
		{
			if (_head->_reader + 1 == _head->_length && _head->_next != nullptr)
			{
				auto tmp = _head;
				_head = _head->_next;
				delete tmp;
			}
		}
		
		Section<T>* _head;
		Section<T>* _tail;
		volatile long _size;
		
	};
}
