#pragma once

namespace Islander
{
    class IResourceLoader2;

    class IDevice
    {
    public:
        virtual IResourceLoader2* GetResourceLoader2() = 0;

        virtual void InitializeImgui() = 0;

        virtual void NewFrameImgui() = 0;

        virtual void RenderImgui() = 0;

        virtual void ImageImgui(int texture, float width, float height) = 0;

        virtual void ImageImgui(int texture, float x, float y, float width, float height) = 0;

        virtual bool ImageButtonImgui(int id, int texture, float x, float y, float sx, float sy) = 0;

        virtual ~IDevice() {}
    };
}
