#pragma once

namespace Islander
{
    struct component_texture_composite;

    class IResourceLoader2
    {
    public:

        virtual char* GetRawData(const std::string& name, int* size) = 0;
        virtual component_texture_composite* FindMaterialTexture(const std::string& name) = 0;
        virtual ~IResourceLoader2() {}
    };
}
