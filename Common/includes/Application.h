#pragma once

namespace Islander
{
    class Application
    {
    public:
        void Start();
        virtual void OnStart();
        virtual ~Application();
    };
}
