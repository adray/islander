#pragma once
#include <string>
#include <assert.h>
#include <stdint.h>
#include <algorithm>
#include "Exports.h"

namespace Islander
{
template<class t>
class Vec2
{
	t _X, _Y;

public:
	Vec2() : _X(0), _Y(0)
	{
	}

	Vec2(t x, t y) : _X(x), _Y(y)
	{
	}

	inline void SetX(t x) { _X = x; }
	inline void SetY(t y) { _Y = y; }

	inline t GetX() const { return _X; }
	inline t GetY() const { return _Y; }

	inline float LengthXY() const
	{
		return sqrtf(_X*_X+_Y*_Y);
	}

	inline void NormalizeXY()
	{
		t len = LengthXY();
		if (len < 0.1f)
			return;
		_X /= len;
		_Y /= len;
		assert(_X*_X+_Y*_Y > 0.99f && _X*_X+_Y*_Y < 1.01f);
	}

	virtual ~Vec2() {}

};

template<class t>
class Vec3 : public Vec2<t>
{
	t _Z;

public:
	Vec3() : Vec2<t>(), _Z(0)
	{
	}

	Vec3(t x, t y, t z) : Vec2<t>(x, y), _Z(z)
	{
	}

	template<class s> 
	Vec3(s v) : Vec2<t>(v.GetX(), v.GetY()), _Z(0)
	{
	}

	inline float GetZ() const
	{
		return _Z;
	}

	inline void SetZ(float z)
	{
		_Z = z;
	}
};

typedef Vec3<int32_t> Vec3i;
typedef Vec2<int32_t> Vec2i;
typedef Vec3<float> Vec3f;
typedef Vec2<float> Vec2f;

}
