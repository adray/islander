#pragma once
#include <fstream>
#include <string>
#include "Exports.h"

namespace Islander
{

	class /*COMMON_EXPORT*/ FileSystem
	{
	public:

		typedef std::string PathUTF8;

		static void AppendFile(std::ofstream& file, const PathUTF8& path, bool binary);

		static void WriteFile(std::ofstream& file, const PathUTF8& path, bool binary);

		static void ReadFile(std::ifstream& file, const PathUTF8& path, bool binary);

		static bool Exists(const PathUTF8& path);

		static bool Delete(const PathUTF8& path);

		static void Copy(const PathUTF8& sourcePath, const PathUTF8& targetPath, bool failIfExists);

		static void GetFileName(const PathUTF8& path, PathUTF8& filename);

		static void* GetFirstFile(const PathUTF8& dir, PathUTF8& path);
		
		static bool GetNextFile(void* handle, PathUTF8& path);

		static void* GetFirstDirectory(const PathUTF8& dir, PathUTF8& path);

		static bool GetNextDirectory(void* handle, PathUTF8& path);

		static void GetAbsolutePath(PathUTF8& path);

		static void RunProcess(const PathUTF8& path, const PathUTF8& commandLineArgs);

	};
  
};
