#pragma once
#include "Exports.h"

long /*COMMON_EXPORT*/ AtomicIncrement(volatile long* value);

long /*COMMON_EXPORT*/ AtomicDecrement(volatile long* value);
