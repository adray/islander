#pragma once

#include "LocklessQueue.h"
#include <mutex>

namespace Islander
{
	template<typename T>
	// Bag safe for one writer and one reader.
	class LocklessBag
	{
	private:
		LocklessQueue<T> _queue;
		std::mutex _mutex;
		std::condition_variable _condition;
		bool _process_data = false;

	public:
		void Enqueue(const T& item)
		{
			_queue.Queue(item);

			{
				// acquire the lock, once held allow the queue to be processed
				std::unique_lock<decltype(_mutex)>lock(_mutex);
				_process_data = true;
			}

			_condition.notify_one();
		}

		// Dequeue blocks until an item is available.
		void Dequeue(T* item)
		{
			while (!_queue.TryDequeue(item))
			{
				// Acquire the lock and then release the lock when wait is called
				// Then wait until can process data, when the lock is then reacquired
				// Set the process flag and release the lock
				std::unique_lock<decltype(_mutex)>lock(_mutex);
				_condition.wait(lock, [this] { return _process_data; });
				_process_data = false;
			}
		}

		bool Empty()
		{
			return _queue.Size() == 0;
		}

		// Clear all data in the Bag
		void Drain()
		{
			while (_queue.Size() > 0)
			{
				T item;
				_queue.TryDequeue(&item);
			}
		}
	};
}
