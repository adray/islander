#pragma once
#include "Exports.h"

namespace Islander
{
	struct StopwatchInternal;

class /*COMMON_EXPORT*/ Stopwatch
{
public:
	Stopwatch(void);
	~Stopwatch(void);

	Stopwatch(const Stopwatch& other);

	unsigned int GetElapsedMilliseconds();
	void Start();
	void Stop();
	void Reset();
	void Restart();
	bool IsRunning() const;

private:
	StopwatchInternal* impl;
	bool isrunning;
	unsigned int current_elapsed;

	void Update();
	void Init();
};

}
