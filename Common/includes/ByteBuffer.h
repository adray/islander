#pragma once
#include <iostream>
#include <stdint.h>
#include "Exports.h"

namespace Islander
{

class COMMON_EXPORT Encoder
{

	uint8_t* _inbuffer;
	int32_t _insize;
	uint8_t* _outbuffer;
	int32_t _outsize;

public:
	Encoder(uint8_t* data, int32_t size);

	void Encode(uint8_t** out, int32_t* outsize);

	void Decode(uint8_t** out, int32_t* outsize);

	~Encoder();

};

class COMMON_EXPORT ByteBuffer
{

	uint8_t* _data;
	uint32_t _length;
	uint32_t _position;

public:

	ByteBuffer(uint8_t* data, uint32_t length);

	inline uint32_t BytesRemaining() { return _length - _position; }

	inline bool EndOfStream() { return _position >= _length; }

	inline void Seek(uint32_t postion) { _position = postion; }

	inline uint8_t* GetBackingBuffer() { return _data; }

	inline int32_t ReadByte()
	{

		return _data[_position++];

	}

	inline void WriteByte(uint8_t byte)
	{
		_data[_position++] = byte;
	}

	inline int32_t ReadInt()
	{

		int32_t p1 = (int32_t)_data[_position++];
		int32_t p2 = (((int32_t)_data[_position++] << 8) & 0x0000FF00 );
		int32_t p3 = (((int32_t)_data[_position++] << 16) & 0x00FF0000);
		int32_t p4 = ((int32_t)(_data[_position++] << 24) & 0xFF000000);

		return p1 | p2 | p3 | p4;

	}

	inline void WriteInt(int32_t integer)
	{

		uint8_t p1 = (uint8_t)integer;
		uint8_t p2 = (uint8_t)((integer >> 8) & 0x000000FF );
		uint8_t p3 = (uint8_t)((integer >> 16) & 0x000000FF );
		uint8_t p4 = (uint8_t)((integer >> 24) & 0x000000FF );

		_data[_position++] = p1;
		_data[_position++] = p2;
		_data[_position++] = p3;
		_data[_position++] = p4;

	}

	inline void WriteFloat(float floating)
	{
		uint32_t integer = *((uint32_t*) &floating);

		uint8_t p1 = (uint8_t)integer;
		uint8_t p2 = (uint8_t)((integer >> 8) & 0x000000FF );
		uint8_t p3 = (uint8_t)((integer >> 16) & 0x000000FF );
		uint8_t p4 = (uint8_t)((integer >> 24) & 0x000000FF );

		_data[_position++] = p1;
		_data[_position++] = p2;
		_data[_position++] = p3;
		_data[_position++] = p4;

	}

	inline float ReadFloat()
	{

		int32_t p1 = (int32_t)_data[_position++];
		int32_t p2 = (((int32_t)_data[_position++] << 8) & 0x0000FF00 );
		int32_t p3 = (((int32_t)_data[_position++] << 16) & 0x00FF0000);
		int32_t p4 = ((int32_t)(_data[_position++] << 24) & 0xFF000000);

		uint32_t integer = p1 | p2 | p3 | p4;

		float floating = *((float*)&integer);

		return floating;

	}

	void WriteString(const std::wstring& string);

	void ReadString(std::wstring * string);

};

}
