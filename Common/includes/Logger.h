#pragma once
#include <iostream>
#include <vector>
#include <fstream>
#include "Exports.h"

namespace Islander
{

enum LoggingChannels : int
{
	
	LOGGING_INFO=0,
	LOGGING_ALL=0xFF,
	LOGGING_RENDERING = 1 << 0,
	LOGGING_RESOURCES = 1 << 1,
    LOGGING_DEBUG     = 1 << 2

};

class /*COMMON_EXPORT*/ LoggerStream
{
public:
    LoggerStream(std::ofstream* stream);
    ~LoggerStream(void);

    template<typename T>
    void Log(const T& message)
    {
        if (!_stream)
            return;

        if (_stream->bad())
            return;
        if (_stream->fail())
            return;

        *_stream << message;
        _stream->flush();
    }

    void endl()
    {
        if (!_stream)
            return;

        if (_stream->bad())
            return;
        if (_stream->fail())
            return;

        *_stream << std::endl;
    }

private:
    std::ofstream* _stream;
};

template<typename T>
LoggerStream& operator<< (LoggerStream& stream, const T& message)
{
    stream.Log(message);
    return stream;
}

class /*COMMON_EXPORT*/ Logger
{
public:
	Logger(void);
	~Logger(void);

	static Logger* GetLogger();

	void Log(const std::string& message, LoggingChannels channel);
	
	void SetLoggingFlags(int flags);

    static LoggerStream& GetStream(LoggingChannels channel);

private:

	int _flags;

	std::string _logfile;
    std::ofstream _stream;
    LoggerStream* _logStream;
    LoggerStream* _nullLogStream;

	static Logger* _logger;

};

}
