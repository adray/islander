#!/usr/bin/env sh
#
#


islander_source=$PWD

# Build libpng

cd $islander_source
cd './thirdparty/lpng1637'

cp './scripts/makefile.linux' 'makefile'
make
err=$?

if [ $err != 0 ]; then
    exit $err
fi

# Build glew

cd $islander_source

cd './thirdparty/glew-2.1.0'
make
err=$?

if [ $err != 0 ]; then
    exit $err
fi

# Build zlib

cd $islander_source

cd './thirdparty/zlib-1.2.11'
cmake .
make
err=$?

if [ $err != 0 ]; then
    exit $err
fi

# Build islander

cd $islander_source

glew_libs="$islander_source/thirdparty/glew-2.1.0/lib"
libpng_libs="$islander_source/thirdparty/lpng1637"
zlib_libs="$islander_source/thirdparty/zlib-1.2.7"

cmake "-DGLEW_LIBS:PATH=$glew_libs" "-DLIBPNG_LIBS:PATH=$libpng_libs" "-Dzlib_libs:PATH=$zlib_libs" .
err=$?

if [ $err != 0 ]; then
    exit $err
fi

make
err=$?

if [[ $err != 0 ]]; then
    exit $err
fi

# Run tests

cd "$islander_source/bin"
./Islander.Entity.Test

err=$?

if [ $err != 0 ]; then
    exit $err
fi

# Build .NET projects

# managed
# build Debug to make linking of next projects easier...
cd "$islander_source/Managed"
xbuild "Islander.Managed.csproj" "/p:Configuration=Debug"

err=$?

if [ $err != 0 ]; then
    exit $err
fi

# ui
cd "$islander_source/UI"
xbuild "Islander.UI.csproj" "/p:Configuration=Release"

err=$?

if [ $err != 0 ]; then
    exit $err
fi

# Raft
cd "$islander_source/Raft"
xbuild "Raft.csproj" "/p:Configuration=Release"

err=$?

if [ $err != 0 ]; then
    exit $err
fi
