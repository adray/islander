#include "Gateway.h"
#include "Configuration.h"
#include "Connection.h"
#include "Common.h"
#include "Application.h"

using namespace Islander::NetCore;

struct LoginSession
{
	int _session;
	IConnection* _connection;
};

class Chat : public IClientNode
{
public:
	Chat(ConfigurationNode* config);

protected:
	virtual void OnMessageArrived(Message* msg, void* userData);

	virtual void OnConnected(IConnection* connection);

	virtual void OnDisconnected(IConnection* connection);

	virtual void OnSessionAborted(void* userData);
private:
	char* _name;
	char* _host;
	int _port;
};

Chat::Chat(ConfigurationNode* config)
{
	_host = config->GetString("host");
	_name = config->GetString("name");
	_port = config->GetInteger("port");

	Initialize(_host, _port);
}

void Chat::OnMessageArrived(Message* msg, void* userData)
{
	int type = msg->ReadInt();
	if (type == MESSAGE_CHAT_CREATE_ROOM || type == MESSAGE_CHAT_JOIN_ROOM)
	{
		// This session is used for subscriptions to updates.
		// The userData should not be deleted until the subscription is closed.

		LoginSession* session = reinterpret_cast<LoginSession*>(userData);

		int success = msg->ReadInt();
		
		MessageHeader header;
		header._sessionId = session->_session;
		header._messageType = MESSAGE_TYPE_DATA;
		
		Message reply;
		reply.SetHeader(&header);
		reply.WriteInt(type);
		reply.WriteInt(success);

		session->_connection->WriteMessage(reply);
	}
	else if (type == MESSAGE_CHAT_LEAVE_ROOM)
	{
		LoginSession* session = reinterpret_cast<LoginSession*>(userData);

		int success = msg->ReadInt();

		MessageHeader header;
		header._sessionId = session->_session;
		header._messageType = MESSAGE_TYPE_END;

		Message reply;
		reply.SetHeader(&header);
		reply.WriteInt(type);
		reply.WriteInt(success);

		session->_connection->WriteMessage(reply);

		session->_connection->RemoveRef();
		delete session;
	}
	else if (type == MESSAGE_CHAT_TEXT_UPDATE)
	{
		LoginSession* session = reinterpret_cast<LoginSession*>(userData);

		std::string text = msg->ReadString();

		MessageHeader header;
		header._sessionId = session->_session;
		header._messageType = MESSAGE_TYPE_DATA;

		Message reply;
		reply.SetHeader(&header);
		reply.WriteInt(type);
		reply.WriteString(text);

		session->_connection->WriteMessage(reply);
	}
	else if (type == MESSAGE_CHAT_TEXT)
	{
		LoginSession* session = reinterpret_cast<LoginSession*>(userData);

		std::string text = msg->ReadString();

		MessageHeader header;
		header._sessionId = session->_session;
		header._messageType = MESSAGE_TYPE_END;

		Message reply;
		reply.SetHeader(&header);
		reply.WriteInt(type);
		reply.WriteString(text);

		session->_connection->WriteMessage(reply);

		session->_connection->RemoveRef();
		delete session;
	}
}

void Chat::OnConnected(IConnection* connection)
{
	Application::GetLogger() << "Connected to Chat server." << ApplicationLogger::NewLine;
}

void Chat::OnDisconnected(IConnection* connection)
{
	Application::GetLogger() << "Disconnected from Chat server." << ApplicationLogger::NewLine;
}

void Chat::OnSessionAborted(void* userData)
{
	auto session = reinterpret_cast<LoginSession*>(userData);

	MessageHeader header;
	header._messageType = MESSAGE_TYPE_END;
	header._sessionId = session->_session;

	Message msg;
	msg.SetHeader(&header);
	msg.WriteInt(MESSAGE_ABORT);
	msg.WriteString("Chat server down");

	session->_connection->WriteMessage(msg);

	session->_connection->RemoveRef();
	delete session;
}

class Login : public IClientNode
{
public:
	Login(ConfigurationNode* config);

	inline int ConnectionID()
	{
		return _connectionId;
	}

protected:
	virtual void OnMessageArrived(Message* msg, void* userData);

	virtual void OnConnected(IConnection* connection);

	virtual void OnDisconnected(IConnection* connection);

	virtual void OnSessionAborted(void* userData);

private:
	char* _name;
	char* _host;
	int _port;
	int _connectionId;
};

Login::Login(ConfigurationNode* node)
	:
	_connectionId(0)
{
	_host = node->GetString("host");
	_name = node->GetString("name");
	_port = node->GetInteger("port");

	Initialize(_host, _port);
}

void Login::OnMessageArrived(Message* msg, void* userData)
{
	int type = msg->ReadInt();
	if (type == MESSAGE_DO_LOGIN)
	{
		int result = msg->ReadInt();

		LoginSession* session = reinterpret_cast<LoginSession*>(userData);

		if (result)
		{
			int id = msg->ReadInt();

			session->_connection->SetProperty(PROPERTY_LOGGED_IN, 1);
			session->_connection->SetProperty(PROPERTY_CONNECTION_STATE, _connectionId);
			session->_connection->SetProperty(PROPERTY_CONNECTION_ID, id);
		}

		MessageHeader header;
		header._messageType = MESSAGE_TYPE_END;
		header._sessionId = session->_session;

		Message reply;
		reply.SetHeader(&header);
		reply.WriteInt(type);
		reply.WriteInt(result);
		session->_connection->WriteMessage(reply);

		session->_connection->RemoveRef();
		delete session;
	}
	else if (type == MESSAGE_DO_LOGOFF)
	{
		int result = msg->ReadInt();

		LoginSession* session = reinterpret_cast<LoginSession*>(userData);

		if (result)
		{
			session->_connection->SetProperty(PROPERTY_LOGGED_IN, 0);
			session->_connection->SetProperty(PROPERTY_CONNECTION_ID, -1);
		}

		MessageHeader header;
		header._messageType = MESSAGE_TYPE_END;
		header._sessionId = session->_session;

		Message reply;
		reply.SetHeader(&header);
		reply.WriteInt(type);
		reply.WriteInt(result);
		session->_connection->WriteMessage(reply);

		session->_connection->RemoveRef();
		delete session;
	}
}

void Login::OnConnected(IConnection* connection)
{
	// Connected to login server
	_connectionId++;
	Application::GetLogger() << "Connected to Login server." << ApplicationLogger::NewLine;
}

void Login::OnDisconnected(IConnection* connection)
{
	// Disconnected from login server
	Application::GetLogger() << "Disconnected from Login server." << ApplicationLogger::NewLine;
}

void Login::OnSessionAborted(void* userData)
{	
	auto session = reinterpret_cast<LoginSession*>(userData);

	MessageHeader header;
	header._messageType = MESSAGE_TYPE_END;
	header._sessionId = session->_session;

	Message msg;
	msg.SetHeader(&header);
	msg.WriteInt(MESSAGE_ABORT);
	msg.WriteString("Logon server down");

	session->_connection->WriteMessage(msg);

	session->_connection->RemoveRef();
	delete session;
}

class Gateway : public IServerNode
{
public:

	Gateway(std::unique_ptr<ConfigurationNode>& config);

	virtual void OnMessageArrived(IConnection* connection, Message* msg);

	virtual void OnConnected(IConnection* connection);

	virtual void OnDisconnected(IConnection* connection);

private:
	std::unique_ptr<ConfigurationNode> _config;
	Login* login;
	Chat* chat;

	int port;
	int version;
};

Gateway::Gateway(std::unique_ptr<ConfigurationNode>& config)
	: _config(config.get()), chat(nullptr), login(nullptr)
{
	port = config->GetInteger("port");
	version = config->GetInteger("version");
	auto caps = config->GetNode("caps");
	auto services = config->GetNodeList("services");

	for (int i = 0; i < services->Count(); ++i)
	{
		auto serviceConfig = services->GetNode(i);

		char* name = serviceConfig->GetString("name");
		if (strcmp(name, "login") == 0)
		{
			login = new Login(serviceConfig);
			AddClientNode(login);
		}
		else if (strcmp(name, "chat") == 0)
		{
			chat = new Chat(serviceConfig);
			AddClientNode(chat);
		}
	}

	bool ok = true;

	if (!login)
	{
		Application::GetLogger() << "Startup Error: Login server not configured." << ApplicationLogger::NewLine;
		ok = false;
	}

	if (!chat)
	{
		Application::GetLogger() << "Startup Error: Chat server not configured." << ApplicationLogger::NewLine;
		ok = false;
	}

	if (ok)
	{
		Initialize(port);
	}
}

void Gateway::OnMessageArrived(IConnection* connection, Message* msg)
{
	auto name = msg->ReadInt();
	if (name == MESSAGE_GET_SERVER_STATS)
	{
		Message reply;
		reply.SetHeader(msg->GetHeader());
		reply.WriteInt(MESSAGE_GET_SERVER_STATS);
		reply.WriteInt(version);
		connection->WriteMessage(reply);
	}
	else if (name == MESSAGE_DO_LOGIN)
	{
		Message forward;
		forward.WriteInt(name);
		forward.WriteString(msg->ReadString());

		LoginSession* session = new LoginSession();
		session->_connection = connection;
		session->_session = msg->GetHeader()->_sessionId;
		connection->AddRef();

		login->WriteMessage(forward, session);
	}
	else if (name == MESSAGE_DO_LOGOFF)
	{
		bool validated = connection->GetProperty(PROPERTY_LOGGED_IN) == 1 &&
			connection->GetProperty(PROPERTY_CONNECTION_STATE) == login->ConnectionID() &&
			login->IsConnected();

		if (validated)
		{
			Message forward;
			forward.WriteInt(name);
			forward.WriteInt(connection->GetProperty(PROPERTY_CONNECTION_ID));

			LoginSession* session = new LoginSession();
			session->_connection = connection;
			session->_session = msg->GetHeader()->_sessionId;
			connection->AddRef();

			login->WriteMessage(forward, session);
		}
		else
		{
			MessageHeader header;
			header._messageType = MESSAGE_TYPE_END;
			header._sessionId = msg->GetHeader()->_sessionId;

			Message reply;
			reply.SetHeader(&header);
			reply.WriteInt(name);
			reply.WriteInt(0);
			connection->WriteMessage(reply);
		}
	}
	else if (name == MESSAGE_CHAT_CREATE_ROOM ||
		name == MESSAGE_CHAT_JOIN_ROOM ||
		name == MESSAGE_CHAT_LEAVE_ROOM ||
		name == MESSAGE_CHAT_TEXT)
	{
		bool validated = connection->GetProperty(PROPERTY_LOGGED_IN) == 1 &&
			connection->GetProperty(PROPERTY_CONNECTION_STATE) == login->ConnectionID() &&
			login->IsConnected();

		if (validated)
		{
			Message forward;
			forward.WriteInt(name);
			forward.WriteInt(connection->GetProperty(PROPERTY_CONNECTION_ID));

			if (name == MESSAGE_CHAT_TEXT)
			{
				forward.WriteString(msg->ReadString());
			}
			else if (name == MESSAGE_CHAT_JOIN_ROOM)
			{
				forward.WriteInt(msg->ReadInt());
			}

			LoginSession* session = new LoginSession();
			session->_connection = connection;
			session->_session = msg->GetHeader()->_sessionId;
			connection->AddRef();

			chat->WriteMessage(forward, session);
		}
		else
		{
			MessageHeader header;
			header._messageType = MESSAGE_TYPE_END;
			header._sessionId = msg->GetHeader()->_sessionId;

			Message reply;
			reply.SetHeader(&header);
			reply.WriteInt(name);
			reply.WriteInt(0);
			connection->WriteMessage(reply);
		}
	}
}

void Gateway::OnConnected(IConnection* connection)
{
	connection->SetProperty(PROPERTY_LOGGED_IN, 0);
	connection->SetProperty(PROPERTY_CONNECTION_STATE, -1);
	connection->SetProperty(PROPERTY_CONNECTION_ID, -1);
}

void Gateway::OnDisconnected(IConnection* connection)
{
	if (connection->GetProperty(PROPERTY_LOGGED_IN) == 1)
	{
		Message autologOff;
		autologOff.WriteInt(MESSAGE_DO_LOGOFF);
		autologOff.WriteInt(connection->GetProperty(PROPERTY_CONNECTION_ID));
		
		login->WriteMessage(autologOff, nullptr);
	}
}

int main(int c, char** args)
{
	auto config = Configuration::Load("config.xml");

	if (config)
	{
		auto gateway = new Gateway(config);
		Application::Run(gateway);
	}
}
