#include "Login.h"
#include "Configuration.h"
#include "Connection.h"
#include "Common.h"
#include "Application.h"
#include "sqlite3.h"
#include <sstream>

using namespace Islander::NetCore;

class Login : public IServerNode
{
public:

	Login(ConfigurationNode* node);

	void OnMessageArrived(IConnection* connection, Message* msg);

	void OnConnected(IConnection* connection);

	void OnDisconnected(IConnection* connection);

private:
	sqlite3* _db;
};

Login::Login(ConfigurationNode* node)
{
	int port = node->GetInteger("port");
	std::string database = node->GetString("database");

	Initialize(port);

	if (sqlite3_open(database.c_str(), &_db))
	{
		// No database availble
		_db = nullptr;
	}
	else
	{
		// Everyone is logged out
		std::stringstream ss;
		ss << "UPDATE users SET status=0";
		char* errormsg;
		int result;
		if (sqlite3_exec(_db, ss.str().c_str(), nullptr, &result, &errormsg) <= 0 && errormsg)
		{
			Application::GetLogger() << "Error: " << errormsg << ApplicationLogger::NewLine;
		}
	}
}

struct UserData
{
	bool _available;
	int _id;
};

int DoLogin(void* userData, int c, char** colData, char** colName)
{
	UserData* result = (UserData*)userData;

	if (c > 0)
	{
		result->_available = true;
		result->_id = 0; //TODO
	}

	return 1;
}

void Login::OnMessageArrived(IConnection* connection, Message* msg)
{
	int type = msg->ReadInt();
	if (type == MESSAGE_DO_LOGIN)
	{
		MessageHeader header;
		header._sessionId = msg->GetHeader()->_sessionId;
		header._messageType = MESSAGE_TYPE_END;

		Message reply;
		reply.SetHeader(&header);
		reply.WriteInt(MESSAGE_DO_LOGIN);
	
		// Careful with SQL injection here
		std::string username = msg->ReadString();
		std::stringstream ss;
		ss << "SELECT * FROM users WHERE username='"
			<< username
			<< "' AND status=0;";

		UserData result;
		char* errormsg;
		if (sqlite3_exec(_db, ss.str().c_str(), DoLogin, &result, &errormsg) > 0 && result._available)
		{
			reply.WriteInt(1);
			reply.WriteInt(result._id);

			std::stringstream ss;
			ss << "UPDATE users SET status=1 WHERE username='"
				<< username
				<< "';";
			sqlite3_exec(_db, ss.str().c_str(), nullptr, &result, &errormsg);
		}
		else
		{
			reply.WriteInt(0);
		}

		connection->WriteMessage(reply);
	}
	else if (type == MESSAGE_DO_LOGOFF)
	{
		MessageHeader header;
		header._sessionId = msg->GetHeader()->_sessionId;
		header._messageType = MESSAGE_TYPE_END;

		int id = msg->ReadInt();

		Message reply;
		reply.SetHeader(&header);
		reply.WriteInt(MESSAGE_DO_LOGOFF);
		reply.WriteInt(1);

		int result = 0;
		char* errormsg;
		std::stringstream ss;
		ss << "UPDATE users SET status=0 WHERE id='"
			<< id
			<< "';";
		sqlite3_exec(_db, ss.str().c_str(), nullptr, &result, &errormsg);
	}
}

void Login::OnConnected(IConnection* connection)
{
}

void Login::OnDisconnected(IConnection* connection)
{
	// TODO: reset users to logged out if they were registered via this connection
}

int main(int c, char** args)
{
	auto config = Configuration::Load("login.xml");

	if (config)
	{
		Login* t = new Login(config.get());
		Application::Run(t);
	}
}
