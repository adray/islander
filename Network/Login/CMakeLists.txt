project(islander_netcore)

set(CMAKE_AUTOMOC TRUE)

include_directories("..\\NetCore\\includes")
include_directories("..\\Common")
include_directories("${THIRDPARTY}\\sqlite")

set(islander_net_HEADERS
	Login.h)
	
set(islander_net_SOURCES
	Login.cpp)

source_group("login\\Source Files" FILES ${islander_net_SOURCES})
source_group("login\\Header Files" FILES ${islander_net_HEADERS})

add_executable(Islander.Login ${islander_net_SOURCES} ${islander_net_HEADERS})

target_link_libraries(Islander.Login Islander.NetCore)

target_link_libraries(Islander.Login debug ${sql_libs}\\Debug\\sqlite3.lib)
target_link_libraries(Islander.Login optimized ${sql_libs}\\Release\\sqlite3.lib)
