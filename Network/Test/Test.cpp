#include "Test.h"
#include "Configuration.h"
#include "Connection.h"
#include "Application.h"
#include "Common.h"
#include "Stopwatch.h"

using namespace Islander::NetCore;

class LoginStressTest : public IClientNode
{
public:
	void SetUsername(char* username);

	void Start();

protected:
	void OnMessageArrived(Message* msg, void* userData);

	void OnConnected(IConnection* connection);

	void OnDisconnected(IConnection* connection);

	void OnSessionAborted(void* userData);

private:
	char* _username;
	int _outstanding;
};

void LoginStressTest::SetUsername(char* username)
{
	_username = username;
}

void LoginStressTest::Start()
{
	_outstanding = 0;
	int totalSent = 0;
	int totalTime = 0;

	Message login;
	login.WriteInt(MESSAGE_DO_LOGIN);
	login.WriteString(_username);

	Islander::Stopwatch watch;
	watch.Start();

	while (true)
	{
		for (int i = 0; i < 3; ++i)
		{
			WriteMessage(login, nullptr);
			_outstanding++;
			totalSent++;
		}

		if (watch.GetElapsedMilliseconds() >= 5000)
		{
			totalTime += watch.GetElapsedMilliseconds();
			watch.Restart();
			Application::GetLogger() << "Outstanding count: " << _outstanding << ApplicationLogger::NewLine;
			Application::GetLogger() << "Total sent: " << totalSent << ApplicationLogger::NewLine;
			Application::GetLogger() << "Requests per second: " << totalSent/(totalTime/1000) << ApplicationLogger::NewLine;
		}

		Process();
	}
}

void LoginStressTest::OnMessageArrived(Message* msg, void* userData)
{
	_outstanding--;
}

void LoginStressTest::OnConnected(IConnection* connection)
{}

void LoginStressTest::OnDisconnected(IConnection* connection)
{}

void LoginStressTest::OnSessionAborted(void* userData)
{}

class Test : public IClientNode
{
public:
	void SetUsername(char* username);

protected:
	void OnMessageArrived(Message* msg, void* userData);

	void OnConnected(IConnection* connection);

	void OnDisconnected(IConnection* connection);

	void OnSessionAborted(void* userData);

private:
	char* _username;
};

void Test::SetUsername(char* username)
{
	_username = username;
}

void Test::OnMessageArrived(Message* msg, void* userData)
{
	int type = msg->ReadInt();
	if (type == MESSAGE_GET_SERVER_STATS)
	{
		int version = msg->ReadInt();
		Application::GetLogger() << "Server Version: " << version << ApplicationLogger::NewLine;

		Application::GetLogger() << "Requesting logon as " << _username << ApplicationLogger::NewLine;
		Message login;
		login.WriteInt(MESSAGE_DO_LOGIN);
		login.WriteString(_username);
		WriteMessage(login, nullptr);
	}
	else if (type == MESSAGE_DO_LOGIN)
	{
		int result = msg->ReadInt();
		if (result == 0)
		{
			Application::GetLogger() << "Logon denied" << ApplicationLogger::NewLine;
		}
		else if (result == 1)
		{
			Application::GetLogger() << "Logon approved" << ApplicationLogger::NewLine;

			Message createRoom;
			createRoom.WriteInt(MESSAGE_CHAT_CREATE_ROOM);
			WriteMessage(createRoom, nullptr);
		}
	}
	else if (type == MESSAGE_CHAT_CREATE_ROOM)
	{
		int result = msg->ReadInt();
		if (result == 0)
		{
			Application::GetLogger() << "User not logged in" << ApplicationLogger::NewLine;
		}
		else
		{
			Application::GetLogger() << "Room successfully created" << ApplicationLogger::NewLine;

			Message send;
			send.WriteInt(MESSAGE_CHAT_TEXT);
			send.WriteString("hello, world!");
			WriteMessage(send, nullptr);
		}
	}
	else if (type == MESSAGE_CHAT_TEXT_UPDATE)
	{
		std::string text = msg->ReadString();
		Application::GetLogger() << "Message: " << text << ApplicationLogger::NewLine;
	}
	else if (type == MESSAGE_ABORT)
	{
		auto abort = msg->ReadString();
		Application::GetLogger() << "Abort: " << abort << ApplicationLogger::NewLine;
	}
}

void Test::OnSessionAborted(void* userData)
{

}

void Test::OnConnected(IConnection* connection)
{
	Application::GetLogger() << "Requesting server info." << ApplicationLogger::NewLine;
	Message msg;
	msg.WriteInt(MESSAGE_GET_SERVER_STATS);
	WriteMessage(msg, nullptr);
}

void Test::OnDisconnected(IConnection* connection)
{

}

int main(int c, char** args)
{
	auto config = Configuration::Load("test.xml");

	if (config)
	{
		int port = config->GetInteger("port");
		char* host = config->GetString("host");
		char* username = config->GetString("username");

		Test* t = new Test();
		t->SetUsername(username);
		t->Initialize(std::string(host), port);
		Application::Run(t);

		//LoginStressTest* t = new LoginStressTest();
		//t->SetUsername(username);
		//t->Initialize(std::string(host), port);
		//t->Start();
	}
}

