#include "Configuration.h"
#include "Application.h"
#include <rapidxml.hpp>
#include <fstream>
#include <sstream>

using namespace Islander::NetCore;


void ConfigurationNodeList:: AddNode(ConfigurationNode* node)
{
	_nodes.push_back(node);
}

ConfigurationNode* ConfigurationNodeList::GetNode(int index)
{
	return _nodes[index];
}

int ConfigurationNodeList::Count()
{
	return _nodes.size();
}

char* ConfigurationNode::CopyString(const char* str)
{
	int len = strlen(str);
	char* n = new char[len + 1];
	memcpy_s(n, len, str, len);
	n[len] = 0;
	_pool.push_back(n);
	return n;
}

char* ConfigurationNode::GetString(const char* name) const
{
	auto it = _strings.find(const_cast<char*>(name));
	if (it != _strings.end())
	{
		return it->second;
	}
	return nullptr;
}

int ConfigurationNode::GetInteger(const char* name) const
{
	auto it = _integers.find(const_cast<char*>(name));
	if (it != _integers.end())
	{
		return it->second;
	}
	return 0;
}

float ConfigurationNode::GetFloat(const char* name) const
{
	auto it = _floats.find(const_cast<char*>(name));
	if (it != _floats.end())
	{
		return it->second;
	}
	return 0;
}

ConfigurationNode* ConfigurationNode::GetNode(const char* name) const
{
	auto it = _nodes.find(const_cast<char*>(name));
	if (it != _nodes.end())
	{
		return it->second;
	}
	return nullptr;
}

ConfigurationNodeList* ConfigurationNode::GetNodeList(const char* name) const
{
	auto it = _nodelist.find(const_cast<char*>(name));
	if (it != _nodelist.end())
	{
		return it->second;
	}
	return nullptr;
}

void ConfigurationNode::AddFloat(const char* name, float fp)
{
	char* n = CopyString(name);

	_floats.insert(std::make_pair(n, fp));
}

void ConfigurationNode::AddInteger(const char* name, int integer)
{
	char* n = CopyString(name);

	_integers.insert(std::make_pair(n, integer));
}

void ConfigurationNode::AddNode(const char* name, ConfigurationNode* node)
{
	char* n = CopyString(name);

	_nodes.insert(std::make_pair(n, node));

	_nodePool.push_back(node);
}

void ConfigurationNode::AddString(const char* name, char* string)
{
	char* n = CopyString(name);
	char* s = CopyString(string);

	_strings.insert(std::make_pair(n, s));
}

void ConfigurationNode::AddNodeList(const char* name, ConfigurationNodeList* list)
{
	char* n = CopyString(name);

	_nodelist.insert(std::make_pair(n, list));

	_nodeListPool.push_back(list);
}

ConfigurationNode::~ConfigurationNode()
{
	for (auto ptr : _pool)
	{
		delete ptr;
	}

	for (auto ptr : _nodePool)
	{
		delete ptr;
	}

	for (auto ptr : _nodeListPool)
	{
		delete ptr;
	}

	_pool.clear();
	_pool.swap(_pool);
	_nodes.clear();
	_strings.clear();
	_integers.clear();
	_floats.clear();
}

static void ReadNode(const rapidxml::xml_node<char>* node, ConfigurationNode* config);

static void ReadNodeList(const rapidxml::xml_node<char>* node, ConfigurationNodeList* config)
{
	rapidxml::xml_node<char>* child = node->first_node();
	while (child)
	{
		if (strcmp(child->name(), "node") == 0)
		{
			ConfigurationNode* childConfig = new ConfigurationNode();
			ReadNode(child, childConfig);
			config->AddNode(childConfig);
		}

		child = child->next_sibling();
	}
}

void ReadNode(const rapidxml::xml_node<char>* node, ConfigurationNode* config)
{
	rapidxml::xml_node<char>* child = node->first_node();
	while (child)
	{
		if (strcmp(child->name(), "node") == 0)
		{
			auto nameNode = child->first_attribute("name");
			if (nameNode)
			{
				ConfigurationNode* childConfig = new ConfigurationNode();
				ReadNode(child, childConfig);
				config->AddNode(nameNode->value(), childConfig);
			}
		}
		else if (strcmp(child->name(), "nodelist") == 0)
		{
			auto nameNode = child->first_attribute("name");
			if (nameNode)
			{
				ConfigurationNodeList* childConfig = new ConfigurationNodeList();
				ReadNodeList(child, childConfig);
				config->AddNodeList(nameNode->value(), childConfig);
			}
		}
		else if (strcmp(child->name(), "string") == 0)
		{
			auto nameNode = child->first_attribute("name");
			auto valueNode = child->first_attribute("value");
			if (nameNode && valueNode)
			{
				config->AddString(nameNode->value(), valueNode->value());
			}
		}
		else if (strcmp(child->name(), "integer") == 0)
		{
			auto nameNode = child->first_attribute("name");
			auto valueNode = child->first_attribute("value");
			if (nameNode && valueNode)
			{
				auto value = valueNode->value();
				config->AddInteger(nameNode->value(), strtol(value, nullptr, 10));
			}
		}
		else if (strcmp(child->name(), "float") == 0)
		{
			auto nameNode = child->first_attribute("name");
			auto valueNode = child->first_attribute("value");
			if (nameNode && valueNode)
			{
				auto value = valueNode->value();
				config->AddFloat(nameNode->value(), (float)strtod(value, nullptr));
			}
		}

		child = child->next_sibling();
	}
}

std::unique_ptr<ConfigurationNode> Configuration::Load(const char* filename)
{
	std::fstream stream(filename);
	if (!stream.bad() && !stream.fail())
	{
		std::unique_ptr<ConfigurationNode> node(new ConfigurationNode());

		std::stringstream ss;
		ss << stream.rdbuf();

		std::string contents = ss.str();

		char* xml = const_cast<char*>(contents.c_str());
		stream.close();

		rapidxml::xml_document<char> doc;
		doc.parse<0>(xml);

		ReadNode(doc.first_node(), node.get());

		Application::GetLogger()
			<< "Configuration " << filename << " read successfully." << ApplicationLogger::NewLine;

		return node;
	}

	Application::GetLogger() << "Configuration " << filename << " could not be read." << ApplicationLogger::NewLine;

	return nullptr;
}
