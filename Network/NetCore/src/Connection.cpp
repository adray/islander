#include <Common.h>
#include "Connection.h"
#include "Application.h"
#include "enet\enet.h"
#include "LocklessQueue.h"
#include "Stopwatch.h"

using namespace Islander::NetCore;


Message::Message()
	:
	_data(&_staticData),
	_size(256),
	_position(sizeof(MessageHeader))
{}

Message::Message(void* data, int length)
	:
	_size(length),
	_position(sizeof(MessageHeader))
{
	_data = new char[length];
	memcpy_s(_data, length, data, length);
}

Message::~Message()
{
	if (_data != &_staticData)
	{
		delete _data;
		_data = nullptr;
	}
}

void Message::SetHeader(MessageHeader* header)
{
	MessageHeader* data = (MessageHeader*)_data;
	memcpy_s(data, sizeof(MessageHeader), header, sizeof(MessageHeader));
}

MessageHeader* Message::GetHeader()
{
	if (_size >= sizeof(MessageHeader))
	{
		return (MessageHeader*)_data;
	}

	return nullptr;
}

void Message::Allocate(int min)
{
	if (_data != &_staticData)
	{
		delete _data;
		int size = min + 64;
		_data = new char[size];
		_size = size;
	}
}

void Message::WriteInt(int value)
{
	if (_position + sizeof(int) >= _size)
	{
		Allocate(_position + sizeof(int));
	}

	int* data = (int*)((char*)_data + _position);
	*data = value;
	_position += sizeof(int);
}

void Message::WriteFloat(float value)
{
	if (_position + sizeof(float) >= _size)
	{
		Allocate(_position + sizeof(float));
	}

	float* data = (float*)((char*)_data + _position);
	*data = value;
	_position += sizeof(float);
}

void Message::WriteString(const std::string& value)
{
	int len = sizeof(int) + value.length();
	if (_position + len >= _size)
	{
		Allocate(_position + len);
	}

	int* data = (int*)((char*)_data + _position);
	*data = value.length();
	_position += sizeof(int);

	for (int i = 0; i < value.length(); ++i)
	{
		((char*)_data)[_position++] = value[i];
	}
}

std::string Message::ReadString() const
{
	if (_position + sizeof(int) > _size)
	{
		return "";
	}

	int* len = (int*)((char*)_data + _position);
	_position += sizeof(int);

	if (_position + *len*sizeof(char) > _size)
	{
		return "";
	}

	char* str = new char[*len + 1];
	memcpy_s(str, *len, (char*)_data + _position, *len);
	str[*len] = 0;

	std::string string(str);
	delete str;

	return string;
}

int Message::ReadInt() const
{
	if (_position + sizeof(int) <= _size)
	{
		int* data = (int*)((char*)_data + _position);
		_position += sizeof(int);
		return *data;
	}

	return 0;
}

float Message::ReadFloat() const
{
	if (_position + sizeof(float) <= _size)
	{
		float* data = (float*)((char*)_data + _position);
		_position += sizeof(float);
		return *data;
	}

	return 0;
}

class Connection : public IConnection
{
public:
	Connection(ENetPeer* peer);
	Connection(const std::string& host, int port);

	bool WriteMessage(const Message& msg);

	void Dispatch(IClientNode* node);

private:

	ENetPeer* _peer;
	ENetHost* _host;
	ENetAddress _addr;
	Islander::Stopwatch _watch;
};

Connection::Connection(ENetPeer* peer)
	: _peer(peer)
{
}

Connection::Connection(const std::string& host, int port)
{
	Application::GetLogger() << "Connecting to " << host << " on port " << port << ApplicationLogger::NewLine;

	_addr.port = port;
	enet_address_set_host(&_addr, (char*)host.c_str());
	
	_host = enet_host_create(nullptr, 1, 2, 0, 0);
	_peer = enet_host_connect(_host, &_addr, 2, 0);
}

bool Connection::WriteMessage(const Message& msg)
{
	if (_peer->state == ENET_PEER_STATE_DISCONNECTED ||
		_peer->state == ENET_PEER_STATE_ZOMBIE)
	{
		return false;
	}
	
	ENetPacket* packet = enet_packet_create(msg.Blob(), msg.Size(), ENET_PACKET_FLAG_RELIABLE);
	return enet_peer_send(_peer, 0, packet) == 0;
}

void Connection::Dispatch(IClientNode* node)
{
	if (_peer->state == ENET_PEER_STATE_DISCONNECTED)
	{
		if (!_watch.IsRunning())
		{
			_watch.Restart();
		}
		else if (_watch.GetElapsedMilliseconds() >= 5000)
		{
			_watch.Restart();
			_peer = enet_host_connect(_host, &_addr, 2, 0);
		}
	}

	ENetEvent ev;
	for (int i = 0; i < 10; ++i)
	{
		if (enet_host_service(_host, &ev, 1) > 0)
		{
			switch (ev.type)
			{
			case ENET_EVENT_TYPE_CONNECT:
			{
				node->Connected();
			}
			break;
			case ENET_EVENT_TYPE_DISCONNECT:
				node->Disconnected();
				delete ev.peer->data;
				break;
			case ENET_EVENT_TYPE_RECEIVE:
			{
				Message msg(ev.packet->data, ev.packet->dataLength);
				node->MessageArrived(&msg);
				enet_packet_destroy(ev.packet);
			}
			break;
			}
		}
	}
}

class ServerNode
{
public:

	ServerNode();

	void Initialize(int port);

	void Dispatch(IServerNode* node);
private:
	ENetHost* _host;
};

ServerNode::ServerNode()
{
	enet_initialize();
}

void ServerNode::Initialize(int port)
{
	Application::GetLogger() << "Server intializing on port " << port << ApplicationLogger::NewLine;

	ENetAddress addr;
	addr.port = port;
	addr.host = ENET_HOST_ANY;

	_host = enet_host_create(&addr, 1024, 2, 0, 0);

	if (_host)
	{
		Application::GetLogger() << "Server successfully initialized." << ApplicationLogger::NewLine;
	}
	else
	{
		Application::GetLogger() << "Server failed to initialize." << ApplicationLogger::NewLine;
	}
}

void ServerNode::Dispatch(IServerNode* node)
{
	ENetEvent ev;
	for (int i = 0; i < 10; ++i)
	{
		if (enet_host_service(_host, &ev, 1) > 0)
		{
			switch (ev.type)
			{
			case ENET_EVENT_TYPE_CONNECT:
				{
					auto conn = new Connection(ev.peer);
					conn->AddRef();
					ev.peer->data = conn;
					node->OnConnected(conn);
				}
				break;
			case ENET_EVENT_TYPE_DISCONNECT:
				{
					auto conn = reinterpret_cast<IConnection*>(ev.peer->data);
					node->OnDisconnected(conn);
					conn->RemoveRef();
				}
				break;
			case ENET_EVENT_TYPE_RECEIVE:
				{
					Message msg(ev.packet->data, ev.packet->dataLength);
					node->OnMessageArrived(
						reinterpret_cast<IConnection*>(ev.peer->data),
						&msg);
					enet_packet_destroy(ev.packet);
				}
				break;
			}
		}
	}
}

IConnection::IConnection()
	:
	_refCount(0)
{ }

void IConnection:: SetProperty(int property, int value)
{
	auto it = _properties.find(property);
	if (it != _properties.end())
	{
		_properties[property] = value;
	}
	else
	{
		_properties.insert(std::make_pair(property, value));
	}
}

int IConnection:: GetProperty(int property)
{
	auto it = _properties.find(property);
	if (it != _properties.end())
	{
		return it->second;
	}

	return -1;
}

void IConnection::AddRef()
{
	++_refCount;
}

void IConnection::RemoveRef()
{
	if (--_refCount == 0)
	{
		delete this;
	}
}

IClientNode::IClientNode()
	:
	_connected(false)
{
	_session.reserve(100);
	enet_initialize();
}

void IClientNode::Initialize(const std::string& host, int port)
{
	_node = new Connection(host, port);
}

void IClientNode::Connected()
{
	_connected = true;
	OnConnected(_node);
}

void IClientNode::Disconnected()
{
	_connected = false;

	for (auto session : _session)
	{
		if (session)
		{
			OnSessionAborted(session->_userData);
		}
		delete session;
	}

	_session.clear();
	_session.swap(_session);
	_queue.swap(std::queue<int>());

	OnDisconnected(_node);
}

bool IClientNode::IsConnected()
{
	return _connected;
}

void IClientNode::MessageArrived(Message* msg)
{
	MessageHeader* header = msg->GetHeader();
	
	if (header->_sessionId >= 0 && header->_sessionId < _session.size())
	{
		Session* session = _session[header->_sessionId];

		OnMessageArrived(msg, session->_userData);

		if (header->_sessionId == MESSAGE_TYPE_END)
		{
			_queue.push(session->_sessionId);
			_session[session->_sessionId] = nullptr;
			delete session;
		}
	}
}

void IClientNode::WriteMessage(Message& msg, void* userData)
{
	Session* session = new Session();
	session->_userData = userData;

	if (_queue.size() > 0)
	{
		session->_sessionId = _queue.front();
		_queue.pop();
		_session[session->_sessionId] = session;
	}
	else
	{
		session->_sessionId = _session.size();
		_session.push_back(session);
	}
	
	MessageHeader header;
	header._messageType = MESSAGE_TYPE_OPEN;
	header._sessionId = session->_sessionId;
	msg.SetHeader(&header);

	if (!_node->WriteMessage(msg))
	{
		OnSessionAborted(session->_userData);
		_queue.push(session->_sessionId);
		delete session;
	}
}

void IClientNode::Process()
{
	_node->Dispatch(this);
}

IClientNode::~IClientNode()
{
	for (auto session : _session)
	{
		delete session;
	}

	delete _node;
	_node = nullptr;
}

IServerNode::IServerNode()
	: _node(new ServerNode()), _initialized(false)
{
}

IServerNode::~IServerNode()
{
}

void IServerNode::Initialize(int port)
{
	_node->Initialize(port);
	_initialized = true;
}

void IServerNode::Process()
{
	if (_initialized)
	{
		_node->Dispatch(this);

		for (auto node : _nodelist)
		{
			node->Process();
		}
	}
}

void IServerNode::AddClientNode(IClientNode* node)
{
	_nodelist.push_back(node);
}
