#include "Application.h"
#include "Connection.h"
#include <iostream>

using namespace Islander::NetCore;

const std::string& ApplicationLogger::NewLine = "\r\n";

ApplicationLogger& ApplicationLogger:: operator<<(float value)
{
	std::cout << value;
	return *this;
}

ApplicationLogger& ApplicationLogger:: operator<<(int value)
{
	std::cout << value;
	return *this;
}

ApplicationLogger& ApplicationLogger:: operator<<(const std::string& value)
{
	std::cout << value;
	return *this;
}

ApplicationLogger Application::_logger;

void Application::Run(IServerNode* node)
{
	while (true)
	{
		node->Process();
	}
}

void Application::Run(IClientNode* node)
{
	while (true)
	{
		node->Process();
	}
}

void Application::Log(const std::string& msg)
{
	std::cout << msg << std::endl;
}

void Application::SetTimer(APPLICATION_TIMER_PROC proc, void* userData, int milliseconds)
{
	// TODO
}
