#pragma once
#include "Exports.h"
#include <memory>
#include <unordered_map>

namespace Islander
{
	namespace NetCore
	{
		class ConfigurationNode;

		class ISLANDER_NETCORE_EXPORT ConfigurationNodeList
		{
		public:
			void AddNode(ConfigurationNode* node);
			ConfigurationNode* GetNode(int index);
			int Count();
		private:
			std::vector<ConfigurationNode*> _nodes;
		};

		class ISLANDER_NETCORE_EXPORT ConfigurationNode
		{
		public:
			ConfigurationNodeList* GetNodeList(const char* name) const;
			ConfigurationNode* GetNode(const char* name) const;
			char* GetString(const char* name) const;
			int GetInteger(const char* name) const;
			float GetFloat(const char* name) const;

			void AddNode(const char* name, ConfigurationNode* node);
			void AddString(const char* name, char* string);
			void AddInteger(const char* name, int integer);
			void AddFloat(const char* name, float fp);
			void AddNodeList(const char* name, ConfigurationNodeList* list);

			~ConfigurationNode();
		private:

			struct Hash_Func{
				//BKDR hash algorithm
				int operator()(char * str)const
				{
					const int seed = 131;
					int hash = 0;
					while (*str)
					{
						hash = (hash * seed) + (*str);
						str++;
					}

					return hash & (0x7FFFFFFF);
				}
			};

			struct Equal_Func : public std::binary_function<char*, char*, bool>
			{
				bool operator()(const char* x, const char* y) const
				{
					return strcmp(x, y) == 0;
				}
			};

			std::unordered_map<char*, ConfigurationNodeList*, Hash_Func, Equal_Func> _nodelist;
			std::unordered_map<char*, ConfigurationNode*, Hash_Func, Equal_Func> _nodes;
			std::unordered_map<char*, char*, Hash_Func, Equal_Func> _strings;
			std::unordered_map<char*, int, Hash_Func, Equal_Func> _integers;
			std::unordered_map<char*, float, Hash_Func, Equal_Func> _floats;
			std::vector<char*> _pool;
			std::vector<ConfigurationNode*> _nodePool;
			std::vector<ConfigurationNodeList*> _nodeListPool;

			char* CopyString(const char* str);
		};

		class ISLANDER_NETCORE_EXPORT Configuration
		{
		public:
			static std::unique_ptr<ConfigurationNode> Load(const char* filename);
		};

	}
}
