#pragma once
#include "Exports.h"
#include <string>

namespace Islander
{
	namespace NetCore
	{
		class IServerNode;
		class IClientNode;
		
		typedef void(*APPLICATION_TIMER_PROC)(void*);

		class ISLANDER_NETCORE_EXPORT ApplicationLogger
		{
		public:
			ApplicationLogger& operator<<(float value);
			ApplicationLogger& operator<<(int value);
			ApplicationLogger& operator<<(const std::string& value);

			static const std::string& NewLine;
		};

		class ISLANDER_NETCORE_EXPORT Application
		{
		public:

			static void SetTimer(APPLICATION_TIMER_PROC proc, void* userData, int milliseconds);
			static void Run(IServerNode* node);
			static void Run(IClientNode* node);
			static void Log(const std::string& msg);
			static ApplicationLogger& GetLogger()
			{
				return _logger;
			}
		private:
			static ApplicationLogger _logger;
		};
	}
}