#pragma once
#include "Exports.h"
#include <string>
#include <queue>
#include <unordered_map>

class ServerNode;
class ClientNode;
class Connection;

namespace Islander
{
	namespace NetCore
	{
		struct MessageHeader
		{
			int _sessionId;
			int _messageType;
		};

		class ISLANDER_NETCORE_EXPORT Message
		{
		public:

			Message();

			Message(void* data, int length);

			~Message();

			void SetHeader(MessageHeader* header);

			MessageHeader* GetHeader();

			void WriteInt(int value);

			void WriteFloat(float value);

			void WriteString(const std::string& value);

			std::string ReadString() const;

			int ReadInt() const;

			float ReadFloat() const;

			inline void* Blob() const
			{
				return _data;
			}

			inline int Size() const
			{
				return _position;
			}

		private:

			void Allocate(int min);

			char _staticData[256];
			void* _data;
			int _size;
			mutable int _position;
		};
		
		class ISLANDER_NETCORE_EXPORT IConnection
		{
		public:

			IConnection();

			virtual ~IConnection(){}

			virtual bool WriteMessage(const Message& msg) = 0;			

			void SetProperty(int property, int value);

			int GetProperty(int property);

			void AddRef();

			void RemoveRef();

		private:
			std::unordered_map<int, int> _properties;
			int _refCount;
		};

		class ISLANDER_NETCORE_EXPORT IClientNode
		{
		public:

			IClientNode();

			void Initialize(const std::string& host, int port);

			void Process();
						
			void WriteMessage(Message& msg, void* userData);

			void MessageArrived(Message* msg);

			void Connected();

			void Disconnected();

			bool IsConnected();

			virtual ~IClientNode();

		protected:
			virtual void OnMessageArrived(Message* msg, void* userData) = 0;

			virtual void OnConnected(IConnection* connection) = 0;

			virtual void OnDisconnected(IConnection* connection) = 0;

			virtual void OnSessionAborted(void* userData) = 0;

		private:

			struct Session
			{
				int _sessionId;
				void* _userData;
			};

			Connection* _node;
			bool _connected;
			std::vector<Session*> _session;
			std::queue<int> _queue;
		};

		class ISLANDER_NETCORE_EXPORT IServerNode
		{
		public:

			IServerNode();

			void Initialize(int port);

			void Process();
						
			void AddClientNode(IClientNode* node);

			virtual void OnMessageArrived(IConnection* connection, Message* msg) = 0;
			
			virtual void OnConnected(IConnection* connection) = 0;

			virtual void OnDisconnected(IConnection* connection) = 0;

			virtual ~IServerNode();

		private:
			ServerNode* _node;
			std::vector<IClientNode*> _nodelist;
			bool _initialized;
		};

	}
}
