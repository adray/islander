﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace Islander.Wizard
{
    public static class Extensions
    {
        public static void AddAttribute(this XmlNode node, string name, string value)
        {
            var attribute = node.OwnerDocument.CreateAttribute(name);
            attribute.Value = value;
            node.Attributes.Append(attribute);
        }

        public static void AppendTextElement(this XmlNode node, string name, string value)
        {
            var element = node.OwnerDocument.CreateElement(name, node.NamespaceURI);
            element.InnerText = value;
            node.AppendChild(element);
        }

        public static string Fetch(this XmlNode node, string name, FetchFlags flags)
        {
            var child = node.SelectSingleNode(name);
            string value = string.Empty;

            if (child != null)
            {
                var def = child.Attributes["Default"];
                if (def != null)
                {
                    value = def.Value;
                }

                string attribute = flags == FetchFlags.Debug ? "Debug" : "Release";

                var type = child.Attributes[attribute];
                if (type != null)
                {
                    value = type.Value;
                }
            }

            return value;
        }

        public static List<String> FetchAll(this XmlNode node, string name, FetchFlags flags)
        {
            var child = node.SelectNodes(name);
            List<string> values = new List<string>();

            if (child != null)
            {
                foreach (XmlNode childNode in child)
                {
                    string value = string.Empty;
                    var def = childNode.Attributes["Default"];
                    if (def != null)
                    {
                        value = def.Value;
                    }

                    string attribute = flags == FetchFlags.Debug ? "Debug" : "Release";

                    var type = childNode.Attributes[attribute];
                    if (type != null)
                    {
                        value = type.Value;
                    }
                    values.Add(value);
                }
            }

            return values;
        }
    }

    public enum FetchFlags
    {
        Debug,
        Release,
    }

    public class GenerationResult
    {
        public bool Success
        {
            get
            {
                return this.Errors.Count == 0;
            }
        }

        public List<String> Errors { get; set; }

        public GenerationResult()
        {
            this.Errors = new List<string>();
        }
    }

    public class Generator
    {
        public GenerationResult Generate(ProjectConfig config)
        {
            GenerationResult result = new GenerationResult();
            
            XmlDocument doc = new XmlDocument();

            XmlNode decl = doc.CreateXmlDeclaration("1.0", "UTF-8", null);
            doc.AppendChild(decl);

            XmlNode project = doc.CreateElement("Project", "http://schemas.microsoft.com/developer/msbuild/2003");
            project.AddAttribute("ToolsVersion", "12.0");
            project.AddAttribute("DefaultTargets", "Build");

            doc.AppendChild(project);

            XmlNode import = doc.CreateElement("Import", project.NamespaceURI);
            import.AddAttribute("Project", @"$(MSBuildExtensionsPath)\$(MSBuildToolsVersion)\Microsoft.Common.props");
            import.AddAttribute("Condition", @"Exists('$(MSBuildExtensionsPath)\$(MSBuildToolsVersion)\Microsoft.Common.props')");

            project.AppendChild(import);

            XmlNode projectSettings = doc.CreateElement("PropertyGroup", project.NamespaceURI);
            projectSettings.AppendTextElement("ProjectGuid", string.Format("{{{0}}}", config.Guid.ToString()));
            projectSettings.AppendTextElement("OutputType", config.Output);
            projectSettings.AppendTextElement("AppDesignerFolder", "Properties");
            projectSettings.AppendTextElement("RootNamespace", config.Namespace);
            projectSettings.AppendTextElement("AssemblyName", config.Namespace);
            projectSettings.AppendTextElement("TargetFrameworkVersion", "v4.0");
            projectSettings.AppendTextElement("FileAlignment", "512");
            projectSettings.AppendTextElement("Platform", "x86");

            if (config.OutputType == FetchFlags.Debug)
            {
                projectSettings.AppendTextElement("Configuration", "Debug");
            }
            else
            {
                projectSettings.AppendTextElement("Configuration", "Release");
            }

            project.AppendChild(projectSettings);

            string constants = "TRACE";

            XmlNode outputSettings = doc.CreateElement("PropertyGroup", project.NamespaceURI);
            if (config.OutputType == FetchFlags.Debug)
            {
                outputSettings.AppendTextElement("DebugSymbols", "true");
                outputSettings.AppendTextElement("DebugType", "full");
                outputSettings.AppendTextElement("Optimize", "false");
                outputSettings.AppendTextElement("BaseIntermediateOutputPath", System.IO.Path.Combine(System.IO.Directory.GetCurrentDirectory(), @"obj\Debug"));
                constants = "DEBUG; " + constants;
            }
            else
            {
                outputSettings.AppendTextElement("DebugType", "pdbonly");
                outputSettings.AppendTextElement("Optimize", "true");
                outputSettings.AppendTextElement("BaseIntermediateOutputPath", System.IO.Path.Combine(System.IO.Directory.GetCurrentDirectory(), @"obj\Release"));
            }

            outputSettings.AppendTextElement("OutputPath", config.BuildDirectory);
            outputSettings.AppendTextElement("DefineConstants", constants);
            outputSettings.AppendTextElement("ErrorReport", "prompt");
            outputSettings.AppendTextElement("WarningLevel", "4");
            outputSettings.AppendTextElement("PlatformTarget", "x86");

            if (!string.IsNullOrWhiteSpace(config.AllowUnsafeCode))
            {
                outputSettings.AppendTextElement("AllowUnsafeBlocks", config.AllowUnsafeCode);
            }

            project.AppendChild(outputSettings);

            if (!string.IsNullOrWhiteSpace(config.Icon))
            {
                XmlNode iconSettings = doc.CreateElement("PropertyGroup", project.NamespaceURI);
                iconSettings.AppendTextElement("ApplicationIcon", config.Icon);
                project.AppendChild(iconSettings);
            }

            XmlNode references = doc.CreateElement("ItemGroup", project.NamespaceURI);

            foreach (var reference in config.References)
            {
                string full = System.IO.Path.GetFileName(reference);
                string includePath = System.IO.Path.GetFileNameWithoutExtension(reference);
                var include = doc.CreateElement("Reference", project.NamespaceURI);
                include.AddAttribute("Include", includePath);
                if (full != reference)
                {
                    include.AppendTextElement("HintPath", reference);
                }
                references.AppendChild(include);
            }

            project.AppendChild(references);

            XmlNode sources = doc.CreateElement("ItemGroup", project.NamespaceURI);

            foreach (var source in config.SourceFiles)
            {
                var include = doc.CreateElement("Compile", project.NamespaceURI);
                include.AddAttribute("Include", source);
                sources.AppendChild(include);
            }

            project.AppendChild(sources);

            if (!string.IsNullOrWhiteSpace(config.Icon))
            {
                XmlNode itemGroup = doc.CreateElement("ItemGroup", project.NamespaceURI);
                XmlNode embedRes = doc.CreateElement("EmbeddedResource", project.NamespaceURI);
                embedRes.AddAttribute("Include", config.Icon);
                itemGroup.AppendChild(embedRes);
                project.AppendChild(itemGroup);
            }

            XmlNode tools = doc.CreateElement("Import", project.NamespaceURI);
            tools.AddAttribute("Project", @"$(MSBuildToolsPath)\Microsoft.CSharp.targets");
            project.AppendChild(tools);

            try
            {
                doc.Save(config.OutputFile);
            }
            catch (Exception)
            {
                result.Errors.Add("Error outputting to " + config.OutputFile);
            }

            return result;
        }
    }
}
