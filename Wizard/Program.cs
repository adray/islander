﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Islander.Wizard
{
    class Program
    {
        static void Main(string[] args)
        {
            List<string> keys = new List<string>();
            List<string> values = new List<string>();
            for (int i = 0; i < args.Length; i++)
            {
                if (args[i].StartsWith("/p:"))
                {
                    string[] condition = args[i].Substring(3).Split('=');
                    if (condition.Length == 2)
                    {
                        keys.Add(condition[0]);
                        values.Add(condition[1]);
                    }
                }
            }

            Importer importer = null;
            if (keys.Count > 0)
            {
                Console.WriteLine("Using config from arguments");
                importer = Importer.FromArgs(keys, values);
            }
            else
            {
                Console.WriteLine("Importing from " + System.Environment.CurrentDirectory);
                importer = Importer.FromFile(System.Environment.CurrentDirectory);
            }

            Console.WriteLine("Detecting configurations");
            var imports = importer.Import(System.Environment.CurrentDirectory);

            Console.WriteLine("Detected " + imports.Count + " configurations");

            Generator generator = new Generator();
            foreach (var import in imports)
            {
                Console.WriteLine("Generating " + import.OutputType);
                GenerationResult result = generator.Generate(import);
                if (result.Success)
                {
                    Console.WriteLine("Generation success");
                }
                else
                {
                    Console.WriteLine("Generation failure");
                }
            }
        }
    }
}
