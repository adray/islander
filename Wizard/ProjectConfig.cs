﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Islander.Wizard
{
    public class ProjectConfig
    {
        public string BuildDirectory { get; set; }
        public List<string> References { get; set; }
        public string OutputFile { get; set; }
        public List<string> SourceFiles { get; set; }
        public Guid Guid { get; set; }
        public string Output { get; set; }
        public FetchFlags OutputType { get; set; }
        public string Namespace { get; set; }
        public string AllowUnsafeCode { get; set; }
        public string Icon { get; set; }
    }
}
