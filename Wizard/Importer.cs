﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace Islander.Wizard
{
    public class Importer
    {
        private class Variable
        {
            public String Key { get; set; }
            public String Value { get; set; }
        }

        private List<Variable> variables = new List<Variable>();

        public static Importer FromArgs(List<string> keys, List<string> values)
        {
            Importer importer = new Importer();

            for (int i = 0; i < keys.Count; i++)
            {
                string value = values[i].Replace("\"", string.Empty);

                Console.WriteLine("Detected variable: " + keys[i] + ":" + value);
                importer.variables.Add(new Variable()
                    {
                        Key = keys[i],
                        Value = value,
                    });
            }

            return importer;
        }

        public static Importer FromFile(string directory)
        {
            string wizardFile = System.IO.Path.Combine(directory, "wizard.config");
            string wizardConfig = null;
            if (System.IO.File.Exists(wizardFile))
            {
                try
                {
                    using (System.IO.StreamReader reader = new System.IO.StreamReader(wizardFile))
                    {
                        wizardConfig = reader.ReadToEnd();
                    }
                }
                catch (Exception)
                {
                }
            }
            else
            {
                Console.WriteLine("Failed to read " + wizardFile);
            }

            var importer = new Importer();
            if (wizardConfig != null)
            {
                try
                {
                    XmlDocument doc = new XmlDocument();
                    doc.LoadXml(wizardConfig);

                    var config = doc.SelectSingleNode("Config");
                    if (config != null)
                    {
                        var list = config.SelectNodes("Variable");
                        foreach (XmlNode variable in list)
                        {
                            var key = variable.Attributes["Key"];
                            if (key != null)
                            {
                                var val = variable.Attributes["Value"];
                                if (val != null)
                                {
                                    Console.WriteLine("Detected variable: " + key.Value + ":" + val.Value);
                                    importer.variables.Add(new Variable()
                                    {
                                        Key = key.Value,
                                        Value = val.Value,
                                    });
                                }
                            }
                        }
                    }
                }
                catch (Exception)
                {
                }
            }

            return importer;
        }

        public List<ProjectConfig> Import(string directory)
        {
            List<string> candidates = new List<string>();
            int version = (int)Environment.OSVersion.Platform;
            if (version == 4 ||
                version == 6 ||
                version == 128)
            {
                candidates.Add(System.IO.Path.Combine(directory, "wizard-unix"));
            }
            else
            {
                candidates.Add(System.IO.Path.Combine(directory, "wizard-win"));
            }

            candidates.Add(System.IO.Path.Combine(directory, "wizard"));

            List<ProjectConfig> configs = new List<ProjectConfig>();

            string wizardConfig = null;
            foreach (string wizardFile in candidates)
            {
                if (System.IO.File.Exists(wizardFile))
                {
                    try
                    {
                        using (System.IO.StreamReader reader = new System.IO.StreamReader(wizardFile))
                        {
                            wizardConfig = reader.ReadToEnd();
                        }
                        break;
                    }
                    catch (Exception)
                    {
                        Console.WriteLine("Error reading file");
                    }
                }
                else
                {
                    Console.WriteLine("Failed to read " + wizardFile);
                }
            }

            if (wizardConfig != null)
            {
                foreach (var @var in this.variables)
                {
                    wizardConfig = wizardConfig.Replace(string.Format("$({0})", @var.Key), @var.Value);
                }

                try
                {
                    XmlDocument doc = new XmlDocument();
                    doc.LoadXml(wizardConfig);

                    var project = doc.SelectSingleNode("Project");
                    if (project != null)
                    {
                        ProjectConfig debug = new ProjectConfig();
                        debug.BuildDirectory = project.Fetch("BuildDirectory", FetchFlags.Debug);
                        debug.Guid = new Guid(project.Fetch("Guid", FetchFlags.Debug));
                        debug.Namespace = project.Fetch("Namespace", FetchFlags.Debug);
                        debug.Output = project.Fetch("Output", FetchFlags.Debug);
                        debug.References = project.FetchAll("Reference", FetchFlags.Debug);
                        debug.SourceFiles = project.FetchAll("Source", FetchFlags.Debug);
                        debug.AllowUnsafeCode = project.Fetch("AllowUnsafeCode", FetchFlags.Debug);
                        debug.Icon = project.Fetch("Icon", FetchFlags.Debug);
                        debug.OutputFile = debug.Namespace + "_DEBUG.csproj";
                        configs.Add(debug);

                        ProjectConfig release = new ProjectConfig();
                        release.BuildDirectory = project.Fetch("BuildDirectory", FetchFlags.Release);
                        release.Guid = new Guid(project.Fetch("Guid", FetchFlags.Release));
                        release.Namespace = project.Fetch("Namespace", FetchFlags.Release);
                        release.Output = project.Fetch("Output", FetchFlags.Release);
                        release.References = project.FetchAll("Reference", FetchFlags.Release);
                        release.SourceFiles = project.FetchAll("Source", FetchFlags.Release);
                        release.OutputFile = debug.Namespace + "_RELEASE.csproj";
                        release.AllowUnsafeCode = project.Fetch("AllowUnsafeCode", FetchFlags.Release);
                        release.Icon = project.Fetch("Icon", FetchFlags.Release);
                        release.OutputType = FetchFlags.Release;
                        configs.Add(release);
                    }

                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }

            return configs;
        }
    }
}
