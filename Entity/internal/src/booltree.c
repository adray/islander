#include "booltree.h"
#include "tree.h"
#include <stdlib.h>

struct bool_tree* BoolTreeCreate()
{
	struct bool_tree* tree = malloc(sizeof(struct bool_tree));
	tree->_false = TreeCreate();
	tree->_true = TreeCreate();
	return tree;
}

void BoolTreeInsert(struct bool_tree* tree, int key, int id, void* data)
{
	if (key == 0)
	{
		TreeInsert(tree->_false, (TREE_KEY)id, data);
	}
	else
	{
		TreeInsert(tree->_true, (TREE_KEY)id, data);
	}
}

void BoolTreeUpdate(struct bool_tree* tree, int key, int id, void* data)
{
	if (key == 0)
	{
		TreeUpdate(tree->_false, (TREE_KEY)id, data);
	}
	else
	{
		TreeUpdate(tree->_true, (TREE_KEY)id, data);
	}
}

void BoolTreeDelete(struct bool_tree* tree, int key, int id)
{
	if (key == 0)
	{
		TreeDelete(tree->_false, (TREE_KEY)id);
	}
	else
	{
		TreeDelete(tree->_true, (TREE_KEY)id);
	}
}

void BoolTreeDestroy(struct bool_tree* tree)
{
	free(tree->_false);
	free(tree->_true);
	free(tree);
}
