#include "entity.h"
#include <stdlib.h>
#include <string.h>
#include "schema.h"
#include "arraylist.h"
#include "iterator.h"
#include "table.h"
#include "tree.h"
#include "query.h"
#include "booltree.h"
#include "hash.h"

//========================================
// WORLD, COMPONENT and ENTITY
//========================================

#define TABLE_ENTITY 0
#define TABLE_PROPERTIES 1

struct struct_world
{
	int _entity_count;
	struct array_list* _tables;
};

struct struct_stats
{
	struct array_list* _tables;
};

void ArrayListAppend(struct array_list* list, void* data)
{
	char* dst;
	AddArrayListItem(list);
	dst = (char*)list->_data + (list->_index-1) * list->_element_size;
	memmove(dst, data, list->_element_size);
}

char* ArrayListGetItem(struct array_list* list, int index)
{
	return (char*)list->_data + index * list->_element_size;
}

void IslanderDestroyWorld(ISLANDER_WORLD world)
{
	int i;
	struct struct_table* t;
	struct struct_world *w = world;
	
	for (i = 0; i < w->_tables->_index; ++i)
	{
		t = ArrayListGetItem(w->_tables, i);

		TableDestroy(t);
	}

	DestroyArrayList(w->_tables);

	free(w);
}

ISLANDER_WORLD IslanderCreateWorld()
{
	struct struct_world *world = (struct struct_world*)malloc(sizeof(struct struct_world));
	struct struct_table* entity = TableCreate("entity", sizeof(struct row_entity), "id", 0, sizeof(int));
	int active = TableAddColumn(entity, "active", sizeof(int), sizeof(int));

	world->_entity_count = 0;
	world->_tables = CreateArrayList(8, sizeof(struct struct_table));
	
	ArrayListAppend(world->_tables, entity);

	IslanderAddComponentIndex(world, TABLE_ENTITY, INDEX_TYPE_FILTER_BOOL, active);

	// Free tables (do not free the interior pointers)
	free(entity);

	return world;
}

int DeserializeInt(char* data, int* index)
{
	int integer;
	memcpy(&integer, data + *index, sizeof(int));
	*index += sizeof(int);
	return integer;
}

void IslanderDeserialize(ISLANDER_WORLD world, ISLANDER_SERIALIZATION_DATA data, int count)
{
	struct struct_world *w = world;
	int index = 0;
	int table_count;
	int row_count;
	int i, j, k;

	w->_entity_count = DeserializeInt(data, &index);

	table_count = DeserializeInt(data, &index);
	for (i = 0; i < table_count; ++i)
	{
		int size=0;
		char* row;
		struct struct_table* table = ArrayListGetItem(w->_tables, i);
		row_count = DeserializeInt(data, &index);
		for (k = 0; k < table->_schema._index; ++k)
		{
			size += table->_schema._column[k]._width;
		}
		
		row = malloc(size);		
		
		// Apply the defaults.
		memmove(row, table->_default_row, size);

		for (j = 0; j < row_count; ++j)
		{
			for (k = 0; k < table->_schema._index; ++k)
			{
				memcpy(row + table->_schema._column[k]._offset, (char*)data + index, table->_schema._column[k]._width);
				index += table->_schema._column[k]._width;
			}

			TableAddRow(table, row);
		}

		free(row);
	}
}

void SerializeInt(struct array_list* array, int i)
{
	memcpy((char*)array->_data + array->_index, &i, sizeof(int));
	array->_index += sizeof(int);

	if (array->_index + sizeof(int) >= array->_count)
	{
		ResizeArrayList(array, array->_count * 2);
	}
}

ISLANDER_SERIALIZATION_DATA IslanderSerialize(ISLANDER_WORLD world, int* count)
{
	ISLANDER_SERIALIZATION_DATA serial;
	struct array_list* array;
	struct struct_world *w = world;
	struct struct_table* table;
	int i, j, k;

	array = CreateArrayList(1000, sizeof(char));

	// Write number of entities
	SerializeInt(array, w->_entity_count);

	// Write number of tables
	SerializeInt(array, w->_tables->_index);

	for (i = 0; i < w->_tables->_index; ++i)
	{
		table = (struct struct_table*)w->_tables->_data + i;

		// Write number of rows
		SerializeInt(array, table->_data->_index);

		for (j = 0; j < table->_data->_index; ++j)
		{
			char* data = ArrayListGetItem(table->_data, j);
			for (k = 0; k < table->_schema._index; ++k)
			{
				struct struct_column col = table->_schema._column[k];
				if (array->_index + col._width >= array->_count)
				{
					ResizeArrayList(array, array->_count * 2);
				}
				memcpy((char*)array->_data + array->_index, data + col._offset, col._width);
				array->_index += col._width;
			}
		}
	}

	*count = array->_index;
	serial = array->_data;
	free(array);
	return serial;
}

ISLANDER_QUERY IslanderCompileQuery(ISLANDER_WORLD world, const char* query)
{
	struct struct_world *w = world;
	char error[128];
	return CompileQuery(query, w->_tables, error, 128);
}

void IslanderExecuteQuery(ISLANDER_WORLD world, ISLANDER_QUERY query, struct ISLANDER_RESULT_SET* result)
{
	struct struct_world *w = world;
	struct struct_relation* r = ExecuteQuery(w->_tables, (struct struct_query*) query);
	if (r)
	{
		// If cached make sure not to free the result data
		if (r->_cache_entry)
		{
			result->_cached = 1;
			result->_count = r->_data->_index;
			result->_data = r->_data->_data;
			result->_element_size = r->_data->_element_size;
			result->_cache_entry = r->_cache_entry;
		}
		else
		{
			result->_cached = 0;
			result->_count = r->_data->_index;
			result->_data = r->_data->_data;
			result->_element_size = r->_data->_element_size;
			result->_cache_entry = 0;
			free(r->_data); // we don't need the container anymore
		}
	}
	free(r);
}

void IslanderFreeResultSet(struct ISLANDER_RESULT_SET* result)
{
	struct struct_query_cache_entry* entry;

	if (result->_cached == 0)
	{
	    free(result->_data);
	}
	else if (result->_cache_entry != 0)
	{
		entry = ((struct struct_query_cache_entry*) result->_cache_entry);
		entry->_flags = QUERY_CACHE_FLAG_FREE;
	}
}

int IslanderAddComponentTable(ISLANDER_WORLD world, struct ISLANDER_COMPONENT_SCHEMA* schema)
{
	struct struct_world *w = world;
	struct struct_table *t = TableCreate(schema->_name, schema->_row_size, schema->_primaryKey, schema->_priOffset, schema->_priWidth);

	ArrayListAppend(w->_tables, t);
	free(t);
	return w->_tables->_index-1;
}

int IslanderGetComponentTable(ISLANDER_WORLD world, const char* name)
{
	struct struct_world *w = world;
	struct struct_table* table;
	int i;

	for (i = 0; i < w->_tables->_index; ++i)
	{
		table = (struct struct_table*)w->_tables->_data + i;
		if (strcmp(table->_name, name) == 0)
		{
			return i;
		}
	}

	return -1;
}

void IslanderSetDefault(ISLANDER_WORLD world, int component, int offset, char* data, int width)
{
	struct struct_world *w = world;
	struct struct_table* table = (struct struct_table*)w->_tables->_data + component;
	TableSetDefault(table, offset, data, width);
}

int IslanderAddComponentColumn(ISLANDER_WORLD world, int component, const char* name, int offset, int width)
{
	struct struct_world *w = world;
	struct struct_table* table = (struct struct_table*)w->_tables->_data + component;
	return TableAddColumn(table, name, offset, width);
}

void IslanderAddComponentIndex(ISLANDER_WORLD world, int component, int index_type, int key)
{
	struct struct_world *w = world;
	struct struct_table* table = (struct struct_table*)w->_tables->_data + component;
	struct struct_base_index* index;

	if (index_type == INDEX_TYPE_SORTED)
	{
		index = malloc(sizeof(struct struct_sorted_index));
		index->_filtered = 0;
		index->_sorted = 1;
		index->_ptr = TreeCreate();
	}
	else if (index_type == INDEX_TYPE_FILTER_BOOL)
	{
		index = malloc(sizeof(struct struct_bool_index));
		index->_filtered = 1;
		index->_sorted = 0;
		index->_ptr = BoolTreeCreate();
	}
	else if (index_type == INDEX_TYPE_FILTERED)
	{
		index = malloc(sizeof(struct struct_non_unique_index));
		index->_filtered = 1;
		index->_sorted = 0;
		index->_ptr = 0; // TODO: hash table presumably
	}

	index->_type = index_type;
	index->_key = key;

	TableAddIndex(table, index);
	free(index); // just freeing the container
}

ISLANDER_ENTITY IslanderCreateEntity(ISLANDER_WORLD world)
{
	struct struct_world* w = (struct struct_world*)world;

	struct struct_table* table = (struct struct_table*) ArrayListGetItem(w->_tables, TABLE_ENTITY);

	struct row_entity row;

	row.id = w->_entity_count++;
	row.flags = 1;
    row.count = 0;

	TableAddRow(table, &row);

	return row.id;
}

void IslanderFreeEntity(ISLANDER_WORLD world, ISLANDER_ENTITY entity)
{
	struct struct_world* w = (struct struct_world*)world;

	struct struct_table* table;

	int i;

	for (i = 0; i < w->_tables->_index; ++i)
	{
		table = (struct struct_table*)ArrayListGetItem(w->_tables, i);

		TableRemoveRow(table, entity);
	}

	// TODO: likely want to remove this from other tables
}

ISLANDER_BOOLEAN IslanderIsEntityActive(ISLANDER_WORLD world, ISLANDER_ENTITY entity)
{
	struct struct_world* w = (struct struct_world*)world;

	struct struct_table* table = (struct struct_table*)ArrayListGetItem(w->_tables, TABLE_ENTITY);

	struct struct_sorted_index* index = (struct struct_sorted_index*) ArrayListGetItem(table->_indexes, 0);

	struct row_entity* row = TreeLookup(index->_tree, (TREE_KEY)entity);

	return (row->flags & 0x1) == 1;
}

void IslanderSetEntityActive(ISLANDER_WORLD world, ISLANDER_ENTITY entity, ISLANDER_BOOLEAN active)
{
	struct struct_world* w = (struct struct_world*)world;

	struct struct_table* table = (struct struct_table*)ArrayListGetItem(w->_tables, TABLE_ENTITY);

	struct struct_sorted_index* index = (struct struct_sorted_index*) ArrayListGetItem(table->_indexes, 0);

	struct row_entity* row = TreeLookup(index->_tree, (TREE_KEY)entity);

	struct row_entity updated;
	memmove(&updated, row, sizeof(struct row_entity));

	updated.flags = active;

	TableUpdateRow(table, updated.id, &updated);
}

void IslanderAddComponent(ISLANDER_WORLD w, int type, void* row)
{
	struct struct_world* world = w;

	struct struct_table* table =(struct struct_table*) ArrayListGetItem(world->_tables, type);

	TableAddRow(table, row);
}

void IslanderGetComponent(ISLANDER_WORLD w, int type, int id, void* row)
{
	struct struct_world* world = w;

	struct struct_table* table = (struct struct_table*) ArrayListGetItem(world->_tables, type);

	TableGetRow(table, id, row);
}

void IslanderUpdateComponent(ISLANDER_WORLD w, int type, int id, void* row)
{
	struct struct_world* world = w;

	struct struct_table* table = (struct struct_table*) ArrayListGetItem(world->_tables, type);

	TableUpdateRow(table, id, row);
}


void IslanderUpdateComponentPartial(ISLANDER_WORLD w, int type, int id, void* row, int offset, int size)
{
    struct struct_world* world = w;

    struct struct_table* table = (struct struct_table*) ArrayListGetItem(world->_tables, type);

    TableUpdateRowPartial(table, id, row, offset, size);
}

void IslanderRemoveComponent(ISLANDER_WORLD w, int type, int id)
{
	struct struct_world* world = w;

	struct struct_table* table = (struct struct_table*) ArrayListGetItem(world->_tables, type);

	TableRemoveRow(table, id);
}

ISLANDER_STATS IslanderGatherStats(ISLANDER_WORLD world, int* count)
{
	struct struct_world* w = (struct struct_world*)world;

	struct struct_table* table;

	int i;

	struct ISLANDER_INFO_BLOCK* block;
	
	struct struct_stats* stats = malloc(sizeof(struct struct_stats));
	
	stats->_tables = (struct array_list*) CreateArrayList(w->_tables->_index, sizeof(struct ISLANDER_INFO_BLOCK));

	for (i = 0; i < w->_tables->_index; ++i)
	{
		table =(struct struct_table*) ArrayListGetItem(w->_tables, i);
		block =(struct ISLANDER_INFO_BLOCK*) ArrayListGetItem(stats->_tables, i);

		block->_id = i;

		TableGatherStats(table, block);
	}

	*count = w->_tables->_index;
	stats->_tables->_index = *count;
	return (ISLANDER_STATS) stats;
}

void IslanderGetStatBlock(ISLANDER_STATS stats, int index, struct ISLANDER_INFO_BLOCK* block )
{
	struct struct_stats* s = (struct struct_stats*)stats;
	if (index >= 0 && index < s->_tables->_index)
	{
		*block = *(struct ISLANDER_INFO_BLOCK*)ArrayListGetItem(s->_tables, index);
	}
}

void IslanderFreeStats(ISLANDER_STATS stats)
{
	struct struct_stats* s = (struct struct_stats*)stats;
	DestroyArrayList(s->_tables);
	free(s);
}

void IslanderAddComponentHandler(ISLANDER_WORLD w, int type, ISLANDER_ROW_CHANGED handler, void* userData)
{
	struct struct_world* world = w;

	struct struct_table* table = (struct struct_table*) ArrayListGetItem(world->_tables, type);

	TableAddHandler(table, handler, userData);
}

// I don't want these to be globals anymore than you do..
int _propertyIndex = 0;
struct struct_hash* _properties;

static void InitializeComponentProperties()
{
	_properties = HashCreate();
	_propertyIndex = 1;
}

int IslanderRegisterComponentProperty(const char* name)
{
	int index;

	if (!_propertyIndex)
	{
		InitializeComponentProperties();
	}

	index = _propertyIndex++;
	HashInsert(_properties, name, (void*)index);
	return index;
}

int IslanderGetComponentProperty(const char* name)
{
	if (!_propertyIndex)
	{
		InitializeComponentProperties();
	}

	// Zero implies no entry found
	return (int)HashFind(_properties, name);
}

// Wrapper to allocate memory
ISLANDER_POINTER IslanderAllocate(int numBytes)
{
	return malloc(numBytes);
}

// Wrapper to free memory
void IslanderFree(void* ptr)
{
	free(ptr);
}
