#include "cache.h"
#include "arraylist.h"

int CacheEntry(
	struct struct_query_list_cache* cache,
	struct array_list* list,
	struct struct_query_cache_entry** found)
{
	int i;

	if (cache->_index == QUERY_CACHE_SIZE)
	{
		int leastUsedIndex = -1;
		int leastUsedCount = INT32_MAX;

		// Discard least used

		for (i = 0; i < cache->_index; ++i)
		{
			struct struct_query_cache_entry *entry = &cache->_arrays[i];
			if (entry->_flags == QUERY_CACHE_FLAG_FREE)
			{
				if (leastUsedIndex == -1 || entry->_uses < leastUsedCount)
				{
					leastUsedIndex = i;
					leastUsedCount = entry->_uses;
				}
			}
		}

		if (leastUsedIndex == -1)
		{
			// Unable to cache the item as all the slots are in use

			return 0;
		}
		else
		{
			struct struct_query_cache_entry *entry = &cache->_arrays[leastUsedIndex];

			DestroyArrayList(entry->_item);

			entry->_item = list;
			entry->_uses = 0;
			entry->_flags = QUERY_CACHE_FLAG_FREE;

			*found = entry;

			return 1;
		}
	}
	else
	{
		struct struct_query_cache_entry* entry = &cache->_arrays[cache->_index];

		entry->_item = list;
		entry->_uses = 0;
		entry->_flags = QUERY_CACHE_FLAG_FREE;

		if (found) *found = entry;

		cache->_index++;

		return 1;
	}
}

int SearchCache(
	struct struct_query_list_cache* cache,
	int element_size,
	int count,
	struct struct_query_cache_entry** found)
{
	int i;
	int bestIndex = -1;
	int bestCount = INT32_MAX;
	int sizeRequired = element_size * count;

	for (i = 0; i < cache->_index; ++i)
	{
		struct struct_query_cache_entry *entry = &cache->_arrays[i];
		if (entry->_flags == QUERY_CACHE_FLAG_FREE)
		{
			int size = entry->_item->_element_size*entry->_item->_count;
			if (size >= sizeRequired && (bestIndex == -1 || size < bestCount))
			{
				bestIndex = i;
				bestCount = size;
			}
		}
	}

	if (bestIndex == -1)
	{
		return 0;
	}
	else
	{
		*found = &cache->_arrays[bestIndex];
		(*found)->_item->_index = 0;
		(*found)->_item->_count = bestCount / element_size;
		(*found)->_item->_element_size = element_size;
		(*found)->_flags = QUERY_CACHE_FLAG_IN_USE;
		(*found)->_uses++;
		return 1;
	}
}
