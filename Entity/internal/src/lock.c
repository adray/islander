#include "lock.h"

#ifdef _WIN32
#include <windows.h>

#define ATOMIC_INC InterlockedIncrement
#define ATOMIC_DEC InterlockedDecrement
#define ATOMIC_ADD InterlockedExchangeAdd
//#define ATOMIC_CMP_EXG InterlockedCompareExchange

#else

int ATOMIC_INC(int* val)
{
	return __sync_add_and_fetch(val, 1);
}

int ATOMIC_DEC(int* val)
{
	return __sync_add_and_fetch(val, -1);
}

#define ATOMIC_ADD __sync_fetch_and_add

#endif

#define LOCK_OPEN 0x01000000

struct Lock
{
	int _flags;
};

RW_LOCK CreateLock()
{
	struct Lock* lock = malloc(sizeof(struct Lock));

	lock->_flags = LOCK_OPEN;

	return lock;
}

int TryAcquireWriterLock(RW_LOCK lock)
{
	struct Lock* _lock = lock;
	
	if (ATOMIC_ADD(&_lock->_flags, -LOCK_OPEN) == LOCK_OPEN)
	{
		return 1;
	}

	ATOMIC_ADD(&_lock->_flags, LOCK_OPEN);
	return 0;
}

int TryAcquireReaderLock(RW_LOCK lock)
{
	struct Lock* _lock = lock;	
	
	if (ATOMIC_DEC(&_lock->_flags) >= 0)
	{
		return 1;
	}

	ATOMIC_INC(&_lock->_flags);

	return 0;
}

void ReleaseWriterLock(RW_LOCK lock)
{
	struct Lock* _lock = lock;

	ATOMIC_ADD(&_lock->_flags, LOCK_OPEN);
}

void ReleaseReaderLock(RW_LOCK lock)
{
	struct Lock* _lock = lock;

	ATOMIC_INC(&_lock->_flags);
}

void DestroyLock(RW_LOCK lock)
{
	free((struct Lock*)lock);
}
