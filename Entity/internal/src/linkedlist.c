#include "linkedlist.h"
#include "iterator.h"
#include <stdlib.h>

//========================================
// SEGMENTED LINKED LIST
//========================================

struct node_element* CreateNodeElement()
{
	struct node_element* element = (struct node_element*)malloc(sizeof(struct node_element));

	element->_count = 1000;
	element->_index = 0;
	element->_next = 0;

	return element;
}

void AppendList(struct node_list * list)
{
	struct node_element* element = CreateNodeElement();

	if (list->_last != 0)
	{
		list->_last->_next = element;
	}

	list->_last = element;
}

void CreateList(struct node_list * list)
{
	list->_last = 0;
	AppendList(list);
	list->_first = list->_last;
}

char* LinkedListAddEntry(struct node_list* list, struct node_entry* entry)
{
	int index;

	if (list->_last->_index >= list->_last->_count)
	{
		AppendList(list);
	}

	index = list->_last->_index++;

	list->_last->_data[index] = *entry;
	return list->_last->_data[index]._payload;
}

struct node_entry* LinkedListGetEntryAt(struct node_list* list, int index)
{
	struct node_element* elem = list->_first;

	while (index >= 0)
	{
		if (index > elem->_index)
		{
			index -= elem->_index;
			elem = elem->_next;
		}
		else
		{
			return &elem->_data[index];
		}
	}

	return 0;
}
