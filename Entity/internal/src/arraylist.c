#include "arraylist.h"
#include "iterator.h"
#include <stdlib.h>

//========================================
// ARRAY LIST
//========================================

struct array_list* CreateArrayList(int initial_size, int element_size)
{
	struct array_list* list = malloc(sizeof(struct array_list));
	list->_data = malloc(initial_size * element_size);
	list->_count = initial_size;
	list->_index = 0;
	list->_element_size = element_size;
	return list;
}

void DestroyArrayList(struct array_list* list)
{
	free(list->_data);
	free(list);
}

void ResizeArrayList(struct array_list* list, int new_size)
{
	list->_data = realloc(list->_data, new_size * list->_element_size);
	list->_count = new_size;
}

void AddArrayListItem(struct array_list* list)
{
	if (list->_index + 1 > list->_count)
	{
		ResizeArrayList(list, list->_count * 2);
	}
	list->_index++;
}

