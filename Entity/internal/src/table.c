#include <string.h>
#include <stdlib.h>
#include "table.h"
#include "tree.h"
#include "arraylist.h"
#include "booltree.h"
#include "lock.h"

#define min(A, B) A < B ? A : B
#define max(A, B) A > B ? A : B 

static void OnTableRowChanged(struct struct_table* table, void* row, int e)
{
	int i;
	struct array_list* list = table->_callback;

	for (i = 0; i < list->_index; ++i)
	{
		struct struct_handler* call = (char*)list->_data + list->_element_size * i;
		(*call->_call)(call->_userData, row, e);
	}
}

void TableAddIndex(struct struct_table* table, struct struct_base_index* index)
{
	char* dst;
	AddArrayListItem(table->_indexes);
	dst = (char*)table->_indexes->_data + table->_indexes->_element_size * (table->_indexes->_index-1);
	memmove(dst, index, table->_indexes->_element_size);
}

void TableDestroy(struct struct_table* table)
{
	DestroyLock(table->_lock);
	DestroyArrayList(table->_callback);
	DestroyArrayList(table->_indexes);
    DestroyArrayList(table->_data);
    DestroyArrayList(table->_free_rows);

    // TODO: fixme
	//free(table->_default_row);
 //   free(table);
}

struct struct_table* TableCreate(const char* name, int element_size, const char* primaryKey, int offset, int width)
{
	struct struct_sorted_index index;
	struct struct_table* table = malloc(sizeof(struct struct_table));
	table->_data = CreateArrayList(1024, element_size);
	table->_indexes = CreateArrayList(8, sizeof(struct struct_base_index));
	table->_callback = CreateArrayList(4, sizeof(struct struct_handler));
	table->_free_rows = CreateArrayList(128, sizeof(int32_t));
	table->_lock = CreateLock();
	table->_row_count = 0;
	table->_default_row = malloc(element_size);
	memset(table->_default_row, 0, element_size);

	strncpy(table->_name, name, sizeof(char) * min(TABLE_NAME_LENGTH, (strlen(name) + 1)));

	table->_schema._primarykey = 0;
	table->_schema._index = 0;

	TableAddColumn(table, primaryKey, offset, width);

	index._key = 0;
	index._sorted = 1;
	index._filted = 0;
	index._type = INDEX_TYPE_SORTED;
	index._tree = TreeCreate();

	TableAddIndex(table, (struct struct_base_index*) &index);
	return table;
}

void TableAcquireReaderLock(struct struct_table* table)
{
	while (!TryAcquireReaderLock(table->_lock)) {};
}

void TableAcquireWriterLock(struct struct_table* table)
{
	while (!TryAcquireWriterLock(table->_lock)) {};
}

void TableReleaseReaderLock(struct struct_table* table)
{
	ReleaseReaderLock(table->_lock);
}

void TableReleaseWriterLock(struct struct_table* table)
{
	ReleaseWriterLock(table->_lock);
}

int TableAddColumn(struct struct_table* table, const char* name, int offset, int width)
{
	int index = table->_schema._index++;
	
	strncpy(table->_schema._column[index]._name, name, sizeof(char) * min(TABLE_NAME_LENGTH, (strlen(name) + 1)));

	table->_schema._column[index]._offset = offset;
	table->_schema._column[index]._width = width;

	return index;
}

void TableSetPrimaryKey(struct struct_table* table, const char* name)
{
	int i;
	for (i = 0; i < table->_schema._index; ++i)
	{
		if (strcmp(table->_schema._column[i]._name, name) == 0)
		{
			table->_schema._primarykey = i;
			return;
		}
	}
}

static TREE_KEY GetCell(void* row, struct struct_column* col)
{
	TREE_KEY key = 0;
	memmove(&key, (char*)row + col->_offset, min(sizeof(TREE_KEY), col->_width));
	return key;
}

static TREE_KEY GetPrimaryCell(void* row, struct struct_table_schema* schema)
{
	return GetCell(row, &schema->_column[schema->_primarykey]);
}

static void SetCell(void* row, struct struct_column* col, void* value)
{
	memmove((char*)row + col->_offset, value, col->_width);
}

static void SetPrimaryCell(void* row, struct struct_table_schema* schema, void* value)
{
	SetCell(row, &schema->_column[schema->_primarykey], value);
}

static TREE_KEY GetRowID(void* row)
{
	// TODO
	return (TREE_KEY)*(int*)row;
}

static void SetRowID(int id, void* row)
{
	// TODO
	*(int*)row = id;
}

static struct struct_tree* BuildIndexedTree(struct struct_table* table)
{
	int i;
	TREE_KEY id;
	struct struct_row* row;
	struct struct_tree* tree = TreeCreate();
	int element_size = table->_data->_element_size;

	for (i = 0; i < table->_data->_index; ++i)
	{
		row =(struct struct_row*) ((char*)table->_data->_data + element_size * i);
		id = GetPrimaryCell(row, &table->_schema);
		if (id >= 0)
		{
			TreeInsert(tree, id, row);
		}
	}
	return tree;
}

static struct bool_tree* BuildBoolTree(struct struct_table* table, struct struct_column* col)
{
	int i;
	TREE_KEY id;
	TREE_KEY key;
	struct struct_row* row;
	struct bool_tree* tree = BoolTreeCreate();
	int element_size = table->_data->_element_size;

	for (i = 0; i < table->_data->_index; ++i)
	{
		row = (char*)table->_data->_data + element_size * i;
		id = GetPrimaryCell(row, &table->_schema);
		key = GetCell(row, col);
		if (id >= 0)
		{
			BoolTreeInsert(tree, key, id, row);
		}
	}
	return tree;
}

static void UpdateSortedIndex(struct struct_sorted_index* index, TREE_KEY key, void* row)
{
	TreeInsert(index->_tree, key, row);
}

static void UpdateFilteredBoolIndex(struct struct_bool_index* index, int id, int key, void* row)
{
	BoolTreeInsert(index->_tree, key, id, row);
}

void TableSetDefault(struct struct_table* table, int offset, char* data, int width)
{
	memmove(table->_default_row + offset, data, width);
}

int TableAddRow(struct struct_table* table, void* row)
{
	int i;
	struct struct_base_index* index;
	struct array_list* list = table->_data;
	void* previous = list->_data;
	char* begin;
	
	if (table->_free_rows->_index > 0)
	{
		int pos = *(int*)table->_free_rows->_data;
		if (pos >= 0)
		{
			begin = (char*)list->_data + pos;
			memcpy(table->_free_rows->_data,
				(char*)table->_free_rows->_data + (table->_free_rows->_index-1) * table->_free_rows->_element_size,
				sizeof(int32_t));
			table->_free_rows->_index--;
		}
		else
		{
			return 0;
		}
	}
	else
	{
		AddArrayListItem(table->_data);	
		begin = (char*)table->_data->_data + list->_element_size * (list->_index-1);
	}
	
	memmove(begin, row, list->_element_size);
	table->_row_count++;
	
	// Update indexes
	if (table->_data->_data != previous)
	{
		// Has rellocated in different place, therefore all index pointers are invalid		
		for (i = 0; i < table->_indexes->_index; ++i)
		{
			index = (struct struct_base_index*)table->_indexes->_data + i;

			switch (index->_type)
			{
			case INDEX_TYPE_SORTED:
				{
					// TODO: might be faster to clear it
					TreeDestroy(((struct struct_sorted_index*)index)->_tree);

					((struct struct_sorted_index*)index)->_tree = BuildIndexedTree(table);
				}
				break;
			case INDEX_TYPE_FILTER_BOOL:
				{
					BoolTreeDestroy(((struct struct_bool_index*)index)->_tree);

					((struct struct_bool_index*)index)->_tree = BuildBoolTree(table, &table->_schema._column[index->_key]);
				}
				break;
			}
		}
	}
	else
	{
		for (i = 0; i < table->_indexes->_index; ++i)
		{
			index = (struct struct_base_index*)table->_indexes->_data + i;

			switch (index->_type)
			{
			case INDEX_TYPE_SORTED:
				UpdateSortedIndex((struct struct_sorted_index*)index, GetPrimaryCell(row, &table->_schema), begin);
				break;
			case INDEX_TYPE_FILTER_BOOL:
				UpdateFilteredBoolIndex((struct struct_bool_index*)index,
					GetPrimaryCell(row, &table->_schema),
					GetCell(row, &table->_schema._column[index->_key]),
					begin);
				break;
			}
		}
	}

	OnTableRowChanged(table, row, ISLANDER_ROW_EVENT_ADDED);

	return 1;
}

int TableGetRow(struct struct_table* table, int id, void* row)
{
	struct struct_sorted_index* index;
	void* original=0;
	
	TableAcquireReaderLock(table);

	index = table->_indexes->_data;
	original = TreeLookup(index->_tree, id);	

	TableReleaseReaderLock(table);

	if (original)
	{
		memmove(row, original, table->_data->_element_size);
		return 1;
	}

	return 0;
}

// NOTE: Modifying the underlying table data then calling TableUpdateRow
// with the modified data will fail to update the indexes as there
// is know way to know how to map from the original data.

void TableUpdateRow(struct struct_table* table, int id, void* row)
{
    TableUpdateRowPartial(table, id, row, 0, table->_data->_element_size);
}

void TableUpdateRowPartial(struct struct_table* table, int id, void* row, int offset, int size)
{
    struct struct_sorted_index* index;
    void* original;
    TREE_KEY primarykey;

    TableAcquireWriterLock(table);

    if (offset > table->_schema._primarykey)
    {
        primarykey = id;
    }
    else if (offset == 0 && size > table->_schema._primarykey + sizeof(int))
    {
        primarykey = GetPrimaryCell(row, &table->_schema);
        if (primarykey != id)
        {
            TableReleaseWriterLock(table);
            return;
        }
    }
    else
    {
        TableReleaseWriterLock(table);
        return;
    }

    index = table->_indexes->_data;
    original = TreeLookup(index->_tree, primarykey);
    if (original)
    {
        // If the row column which index is keyed then remove item and added it back in

        int i;
        int keyoffset;
        int width;
        struct struct_base_index* data;
        char* newRowKey;

        for (i = 0; i < table->_indexes->_index; ++i)
        {
            data = (struct struct_base_index*)table->_indexes->_data + i;
            keyoffset = table->_schema._column[data->_key]._offset;
            width = table->_schema._column[data->_key]._width;

            if (offset > keyoffset)
            {
                newRowKey = (char*)original + keyoffset;
            }
            else
            {
                newRowKey = (char*)row + keyoffset - offset;
            }

            if (!!memcmp((char*)original + keyoffset, // !! hints the compiler that only the result != 0 matters
                newRowKey,
                width))
            {
                switch (data->_type)
                {
                case INDEX_TYPE_SORTED:
                {
                    struct struct_sorted_index* sorted = (struct struct_sorted_index*)data;
                    TreeDelete(sorted->_tree, GetCell(original, &table->_schema._column[data->_key]));
                    // Insert pointer to where the data to row, which will be updated
                    TreeInsert(sorted->_tree, id, original);
                }
                break;
                case INDEX_TYPE_FILTER_BOOL:
                {
                    struct struct_bool_index* boolean = (struct struct_bool_index*)data;
                    BoolTreeDelete(boolean->_tree,
                        GetCell(original, &table->_schema._column[data->_key]),
                        GetPrimaryCell(original, &table->_schema));
                    // Insert pointer to where the data to row, which will be updated
                    BoolTreeInsert(boolean->_tree,
                        *(int*)newRowKey,
                        GetPrimaryCell(row, &table->_schema),
                        original);
                }
                break;
                }
            }
        }

        memmove((char*)original + offset, row, size);

        TableReleaseWriterLock(table);

        OnTableRowChanged(table, original, ISLANDER_ROW_EVENT_UPDATED);
    }
    else
    {
        TableReleaseWriterLock(table);
    }
}

void TableRemoveRow(struct struct_table* table, int id)
{
	int deleted = -1;
	struct struct_base_index* index;
	struct struct_sorted_index* sorted = (struct struct_sorted_index*)table->_indexes->_data;
	void* row = TreeLookup(sorted->_tree, (TREE_KEY)id);
	int i;

	if (row)
	{
		// Add to table for reuse
		char* ptr;
		int pos = ((char*)row - (char*)table->_data->_data);

		AddArrayListItem(table->_free_rows);
		ptr = (char*)table->_free_rows->_data + (table->_free_rows->_index-1) * table->_free_rows->_element_size;
		memmove(ptr, &pos, sizeof(int32_t));

		table->_row_count--;

		// Run callback early as the data will be "removed" after this
		OnTableRowChanged(table, row, ISLANDER_ROW_EVENT_REMOVED);

		// Mark row as removed
		SetPrimaryCell(row, &table->_schema, &deleted);

		// Update indexes
		for (i = 0; i < table->_indexes->_index; ++i)
		{
			index = i + (struct struct_base_index*)table->_indexes->_data;

			switch (index->_type)
			{
			case INDEX_TYPE_SORTED:
				TreeDelete(((struct struct_sorted_index*)index)->_tree, id);
				break;
			case INDEX_TYPE_FILTER_BOOL:
				BoolTreeDelete(((struct struct_bool_index*)index)->_tree,
					GetCell(row, &table->_schema._column[index->_key]),
					id);
				break;
			}
		}
	}
}

int TableRowCount(struct struct_table* table)
{
	return table->_row_count;
}

void TableAddHandler(struct struct_table* table, ISLANDER_ROW_CHANGED row, void* userData)
{
	char* ptr;
	struct struct_handler handler;

	AddArrayListItem(table->_callback);
	ptr = (char*)table->_callback->_data + (table->_callback->_index-1) * table->_callback->_element_size;

	handler._userData = userData;
	handler._call = row;
	memmove(ptr, &handler, table->_callback->_element_size);
}

void TableGatherStats(struct struct_table* table, struct ISLANDER_INFO_BLOCK* block)
{
	int i;
	int mem_usage = 0;
	struct struct_base_index* index;
	
	block->_free_rows = table->_free_rows->_index;
	block->_used_rows = table->_data->_index;
	block->_max_rows = table->_data->_count;

	mem_usage += table->_data->_count * table->_data->_element_size;
	mem_usage += table->_free_rows->_index * table->_free_rows->_element_size;

	for (i = 0; i < table->_indexes->_index; ++i)
	{
		index = i + (struct struct_base_index*)table->_indexes->_data;

	       	switch (index->_type)
		{
		case INDEX_TYPE_SORTED:
			mem_usage += TreeQueryMemoryUsage(((struct struct_sorted_index*)index)->_tree);
			break;
		case INDEX_TYPE_FILTER_BOOL:
			mem_usage += TreeQueryMemoryUsage(((struct struct_bool_index*)index)->_tree->_false);mem_usage += TreeQueryMemoryUsage(((struct struct_bool_index*)index)->_tree->_true);
			break;
		}
	}
		
	block->_mem_usage = mem_usage;
}

static void AppendRelation(struct struct_relation* relation, char* src)
{
	char* dst;

	AddArrayListItem(relation->_data);
	dst = (char*)relation->_data->_data + relation->_data->_element_size * (relation->_data->_index-1);
	memmove(dst, src, relation->_data->_element_size);
}

static void BuildSortedRelation(struct struct_tree* tree, struct struct_relation* relation)
{
	int i;
	char* src;
	if (tree->isLeaf)
	{
		for (i = 0; i < tree->numKeys; ++i)
		{
			src = (char*)tree->values[i];
			AppendRelation(relation, src);
		}
	}
	else
	{
		for (i = 0; i < tree->numKeys; ++i)
		{
			BuildSortedRelation(tree->childNodes[i], relation);
			src = (char*)tree->values[i];
			AppendRelation(relation, src);
		}
		BuildSortedRelation(tree->childNodes[i], relation);
	}
}

struct struct_relation* TableBuildRelationSorted(struct struct_table* table, int key)
{
	// Check if there is a matching index
	struct struct_relation* relation;
	struct array_list* list = table->_indexes;
	int i;
	struct struct_tree* tree;

	for (i = 0; i < list->_index; ++i)
	{
		struct struct_base_index* index = (struct struct_base_index*)list->_data + i;
		if (index->_key == key && index->_sorted == 1)
		{
			struct struct_sorted_index* sortedIndex = index;
			relation = malloc(sizeof(struct struct_relation));
			relation->_data = CreateArrayList(table->_row_count, table->_data->_element_size);
			BuildSortedRelation(sortedIndex->_tree, relation);			

			return relation;
		}
	}

	// Otherwise linear scan
	
	tree = BuildIndexedTree(table);
	relation = malloc(sizeof(struct struct_relation));
	relation->_data = CreateArrayList(table->_row_count, table->_data->_element_size);
	BuildSortedRelation(tree, relation);
	TreeDestroy(tree);

	return relation;
}

struct struct_relation* TableBuildRelationFiltered(struct struct_table* table, int key, void* value)
{
	// TODO: possible speedup if keep record of the contiguous chunks, then just copy these over

	// Just all the items which are not deleted

	void* row;
	int i;
	TREE_KEY id;
	int element_size = table->_data->_element_size;

	struct struct_tree* tree = BuildIndexedTree(table);
	struct struct_relation* relation = malloc(sizeof(struct struct_relation));
	relation->_data = CreateArrayList(table->_row_count, table->_data->_element_size);

	for (i = 0; i < table->_data->_index; ++i)
	{
		row = (char*)table->_data->_data + element_size * i;
		id = GetRowID(row);
		if (id >= 0)
		{
			//TODO: actually filter it
			char* dst;
			AddArrayListItem(relation->_data);
			dst = (char*)relation->_data->_data + relation->_data->_element_size * (relation->_data->_index-1);
			memmove(dst, row, relation->_data->_element_size);
		}
	}

	return relation;
}
