#include "hash.h"
#include <stdlib.h>
#include <string.h>

static int HashString(const char* string)
{
	int hash = 5381;
	int c;

	while (c = *string++)
	{
		hash = ((hash << 5) + hash) + c;
	}

	return hash;
}

struct struct_hash* HashCreate()
{
	int i;
	struct struct_hash* hash = malloc(sizeof(struct struct_hash));
	hash->_count = 0;
	
	// TODO: Probably should not access tree internals like this
	for (i = 0; i < HASH_ENTRIES; ++i)
	{
		hash->_entries[i].isLeaf = 1;
		hash->_entries[i].numKeys = 0;
	}

	return hash;
}

void HashInsert(struct struct_hash* hash, const char* key, void* value)
{
	int index = HashString(key);
	int mod = abs(index) % HASH_ENTRIES;

	TreeInsert(&hash->_entries[mod], index, value);
	++hash->_count;
}

void HashRemove(struct struct_hash* hash, const char* key, void* value)
{
	int index = HashString(key);
	int mod = abs(index) % HASH_ENTRIES;

	TreeDelete(&hash->_entries[mod], index);
	--hash->_count;
}

void* HashFind(struct struct_hash* hash, const char* key)
{
	int index = HashString(key);
	int mod = abs(index) % HASH_ENTRIES;

	return TreeLookup(&hash->_entries[mod], index);
}

void HashDestroy(struct struct_hash* hash)
{
	// yup totally a hack
	int i;
	int j;
	for (i = 0; i < HASH_ENTRIES; ++i)
	{
		if (!hash->_entries[i].isLeaf)
		{
			for (j = 0; j < hash->_entries[i].numKeys; ++j)
			{
				TreeDestroy(hash->_entries[i].childNodes[j]);
			}			
		}
	}

	free(hash);
}
