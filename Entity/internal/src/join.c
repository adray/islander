#include "join.h"
#include "table.h"
#include "arraylist.h"
#include "cache.h"
#include "tree.h"
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#define min(A, B) A < B ? A : B
#define max(A, B) A > B ? A : B 

static int GetKey(struct struct_relation* relation)
{
	return *(int*)((char*)relation->_data->_data + relation->_position * relation->_data->_element_size);
}

static void AppendRelation(struct struct_relation* relation, char* src1, char* src2, int size)
{
	char* dst=0;

	AddArrayListItem(relation->_data);
	dst = (char*)relation->_data->_data + relation->_data->_element_size * (relation->_data->_index-1);
	memmove(dst, src1, size);
	memmove(dst + size, src2, relation->_data->_element_size - size);
}

struct struct_relation* JoinRelations(struct struct_query_list_cache* cache, struct struct_relation* first, struct struct_relation* second)
{
	int firstKey;
	int secondKey;
	struct struct_query_cache_entry* entry;
	struct struct_relation* relation = malloc(sizeof(struct struct_relation));

	if (cache && SearchCache(cache, first->_data->_element_size + second->_data->_element_size,
		min(first->_data->_index, second->_data->_index), &entry))
	{
		relation->_data = entry->_item;
		relation->_cache_entry = entry;
	}
	else
	{
		relation->_data = CreateArrayList(min(first->_data->_index, second->_data->_index),
			first->_data->_element_size + second->_data->_element_size);
		relation->_cache_entry = 0;
	}

	first->_position = 0;
	second->_position = 0;

	while (first->_position < first->_data->_index && second->_position < second->_data->_index)
	{
		firstKey = GetKey(first);
		secondKey = GetKey(second);
		if (firstKey == secondKey)
		{
			AppendRelation(relation,
				(char*)first->_data->_data + first->_data->_element_size * first->_position,
				(char*)second->_data->_data + second->_data->_element_size * second->_position,
				first->_data->_element_size);
			first->_position++;
			second->_position++;
		}
		else if (firstKey < secondKey)
		{
			first->_position++;
		}
		else
		{
			second->_position++;
		}
	}

	return relation;
}

struct struct_relation* JoinTreeToTree(struct struct_query_list_cache* cache, struct struct_tree* first, struct struct_tree* second, int first_size, int second_size)
{
    TREE_KEY firstKey;
    TREE_KEY secondKey;
    struct struct_query_cache_entry* entry;
    struct struct_relation* relation = malloc(sizeof(struct struct_relation));
    int firstStackPos = 0;
    int secondStackPos = 0;
    struct struct_tree_stack_item stack1[4];
    struct struct_tree_stack_item stack2[4];
    void* d1;
    void* d2;
    int res1;
    int res2;

    if (cache && SearchCache(cache, first_size + second_size,
        1000, &entry))
    {
        relation->_data = entry->_item;
        relation->_cache_entry = entry;
    }
    else
    {
        relation->_data = CreateArrayList(1000,
            first_size + second_size);
        relation->_cache_entry = 0;
    }

    stack1[0].pos = 0;
    stack1[0].tree = first;
    stack2[0].pos = 0;
    stack2[0].tree = second;
    InitWalkTree(stack1, &firstStackPos);
    InitWalkTree(stack2, &secondStackPos);

    res1 = WalkTree(stack1, &firstStackPos, &d1, &firstKey);
    res2 = WalkTree(stack2, &secondStackPos, &d2, &secondKey);

    while (res1 && res2)
    {
        if (firstKey == secondKey)
        {
            assert(d1 && d2);
            AppendRelation(relation,
                d1,
                d2,
                first_size);
            res1 = WalkTree(stack1, &firstStackPos, &d1, &firstKey);
            res2 = WalkTree(stack2, &secondStackPos, &d2, &secondKey);
        }
        else if (firstKey < secondKey)
        {
            res1 = WalkTree(stack1, &firstStackPos, &d1, &firstKey);
        }
        else
        {
            res2 = WalkTree(stack2, &secondStackPos, &d2, &secondKey);
        }
    }

    return relation;
}

struct struct_relation* JoinTreeToRelation(struct struct_query_list_cache* cache, struct struct_tree* first, struct struct_relation* second, int first_size, int reversed)
{
    TREE_KEY firstKey=0;
    TREE_KEY secondKey;
    struct struct_query_cache_entry* entry;
    struct struct_relation* relation = malloc(sizeof(struct struct_relation));
    int firstStackPos = 0;
    struct struct_tree_stack_item stack1[4];
    void* d1=0;
    int res1;

    if (cache && SearchCache(cache, first_size + second->_data->_element_size,
        1000, &entry))
    {
        relation->_data = entry->_item;
        relation->_cache_entry = entry;
    }
    else
    {
        relation->_data = CreateArrayList(1000,
            first_size + second->_data->_element_size);
        relation->_cache_entry = 0;
    }

    stack1[0].pos = 0;
    stack1[0].tree = first;
    InitWalkTree(stack1, &firstStackPos);

    res1 = WalkTree(stack1, &firstStackPos, &d1, &firstKey);

    second->_position = 0;
    secondKey = GetKey(second);

    if (!reversed)
    {
        while (d1 && res1 && second->_position < second->_data->_index)
        {
            if (firstKey == secondKey)
            {
                assert(d1);
                AppendRelation(relation,
                    d1,
                    (char*)second->_data->_data + second->_data->_element_size * second->_position,
                    first_size);
                res1 = WalkTree(stack1, &firstStackPos, &d1, &firstKey);
                second->_position++;
                secondKey = GetKey(second);
            }
            else if (firstKey < secondKey)
            {
                res1 = WalkTree(stack1, &firstStackPos, &d1, &firstKey);
            }
            else
            {
                second->_position++;
                secondKey = GetKey(second);
            }
        }
    }
    else
    {
        while (d1 && res1 && second->_position < second->_data->_index)
        {
            if (firstKey == secondKey)
            {
                assert(d1);
                AppendRelation(relation,
                    (char*)second->_data->_data + second->_data->_element_size * second->_position,
                    d1,
                    second->_data->_element_size);
                res1 = WalkTree(stack1, &firstStackPos, &d1, &firstKey);
                second->_position++;
                secondKey = GetKey(second);
            }
            else if (firstKey < secondKey)
            {
                res1 = WalkTree(stack1, &firstStackPos, &d1, &firstKey);
            }
            else
            {
                second->_position++;
                secondKey = GetKey(second);
            }
        }
    }

    return relation;
}
