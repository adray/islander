#include "query.h"
#include "arraylist.h"
#include "table.h"
#include "tree.h"
#include "booltree.h"
#include "join.h"
#include <string.h>
#include <stdlib.h>

#define min(A, B) A < B ? A : B
#define max(A, B) A > B ? A : B 

#define QUERY_ITEM_NONE 0
#define QUERY_ITEM_TREE 1
#define QUERY_ITEM_RELATION 2

struct struct_query_item
{
    int type;
    int element_size;
    struct struct_tree* tree;
    struct struct_relation* relation;
};

static void AppendRelation(struct struct_relation* relation, char* src)
{
	char* dst;
	AddArrayListItem(relation->_data);
	dst = (char*)relation->_data->_data + relation->_data->_element_size * (relation->_data->_index-1);
	memmove(dst, src, relation->_data->_element_size);
}

static void BuildFilteredRelation(struct struct_tree* tree, struct struct_relation* relation, int offset, int element_size, void* data)
{
	int i;
	char* src;
	if (tree->isLeaf)
	{
		for (i = 0; i < tree->numKeys; ++i)
		{
			src = (char*)tree->values[i];
			if (memcmp(src + offset, data, element_size) == 0)
			{
				AppendRelation(relation, src);
			}
		}
	}
	else
	{
		for (i = 0; i < tree->numKeys; ++i)
		{
			BuildFilteredRelation(tree->childNodes[i], relation, offset, element_size, data);
			src = (char*)tree->values[i];
			if (memcmp(src + offset, data, element_size) == 0)
			{
				AppendRelation(relation, src);
			}
		}
		BuildFilteredRelation(tree->childNodes[i], relation, offset, element_size, data);
	}
}

static struct struct_relation* Filter(struct struct_tree* tree, int row_count, int row_width, int offset, int element_size, void* data)
{
	struct struct_relation* relation;

	relation = malloc(sizeof(struct struct_relation));
	relation->_data = CreateArrayList(row_count, row_width);
	relation->_cache_entry = 0;
	BuildFilteredRelation(tree, relation, offset, element_size, data);

	return relation;
}

static void BuildSortedRelation(struct struct_tree* tree, struct struct_relation* relation)
{
	int i;
	char* src;
	if (tree->isLeaf)
	{
		for (i = 0; i < tree->numKeys; ++i)
		{
			src = (char*)tree->values[i];
			AppendRelation(relation, src);
		}
	}
	else
	{
		for (i = 0; i < tree->numKeys; ++i)
		{
			BuildSortedRelation(tree->childNodes[i], relation);
			src = (char*)tree->values[i];
			AppendRelation(relation, src);
		}
		BuildSortedRelation(tree->childNodes[i], relation);
	}
}

//static struct struct_tree* BuildIndexedTree(struct struct_bool_index* index, int filter, int element_size)
//{
//	int i;
//	TREE_KEY id;
//	struct struct_row* row;
//	struct struct_tree* tree = TreeCreate();
//
//	for (i = 0; i < table->_data->_index; ++i)
//	{
//		row = (char*)table->_data->_data + element_size * i;
//		id = GetRowID(row);
//		if (id >= 0)
//		{
//			TreeInsert(tree, id, row);
//		}
//	}
//	return tree;
//}

struct struct_relation* ExecuteQuery(struct array_list* domain, struct struct_query* query)
{
    struct struct_query_item items[2];
	struct struct_table* table = 0;
	struct struct_base_index* index = 0;
	int i;
	int j;

    items[0].type = QUERY_ITEM_NONE;
    items[1].type = QUERY_ITEM_NONE;
    items[0].tree = 0;
    items[1].tree = 0;
    items[0].relation = 0;
    items[1].relation = 0;
    items[0].element_size = 0;
    items[1].element_size = 0;

	// TODO: if someone tries to update 2 tables atomically, 
	// this will either deadlock or work on an inconsistant view of the data
	for (i = 0; i < query->_tableCount; ++i)
	{
		table = (struct struct_table*)domain->_data + i;
		TableAcquireReaderLock(table);
	}

	for (i = 0; i < query->_count; ++i)
	{
		switch (query->_ins[i]._opcode)
		{
		case OPCODE_PUSH_INDEX:
			{
				table = (struct struct_table*)domain->_data + query->_ins[i]._r1;
				index = (struct struct_base_index*)table->_indexes->_data + query->_ins[i]._r2;
			}
			break;
		case OPCODE_SORT_FILTER_BOOL:
			{
				// Sort and filter an index which is of type BoolTree

				struct struct_tree* tree;
                struct struct_query_item* item;

				if (query->_ins[i]._r1 == 0)
				{
					tree = ((struct struct_bool_index*)index)->_tree->_false;
				}
				else
				{
					tree = ((struct struct_bool_index*)index)->_tree->_true;
				}

                item = &items[query->_ins[i]._r4];
                item->type = QUERY_ITEM_TREE;
                item->tree = tree;
                item->element_size = table->_data->_element_size;
			}
			break;
		case OPCODE_SORT_FILTERED:
			{
                struct struct_query_item* item;

                item = &items[query->_ins[i]._r4];
                item->type = QUERY_ITEM_TREE;
                item->tree = ((struct struct_sorted_index*)index)->_tree;
                item->element_size = table->_data->_element_size;
			}
			break;
		case OPCODE_FILTER:
			{
				struct struct_sorted_index* sorted = (struct struct_sorted_index*)index;
				items[query->_ins[i]._r4].relation = Filter(sorted->_tree,
					table->_row_count,
					table->_data->_element_size,
					query->_ins[i]._r1,
					query->_ins[i]._r2,
					query->_ins[i]._r3);
                items[query->_ins[i]._r4].type = QUERY_ITEM_RELATION;
			}
			break;
		case OPCODE_MERGE:
			{
                struct struct_relation* temp;
                
                if (items[0].type == QUERY_ITEM_RELATION && items[1].type == QUERY_ITEM_RELATION)
                {
                    temp = JoinRelations(&query->_cache, items[0].relation, items[1].relation);
                }
                else if (items[0].type == QUERY_ITEM_TREE && items[1].type == QUERY_ITEM_TREE)
                {
                    temp = JoinTreeToTree(&query->_cache, items[0].tree, items[1].tree, items[0].element_size, items[1].element_size);
                }
                else if (items[0].type == QUERY_ITEM_RELATION && items[1].type == QUERY_ITEM_TREE)
                {
                    temp = JoinTreeToRelation(&query->_cache, items[1].tree, items[0].relation, items[1].element_size, 1);
                }
                else if (items[0].type == QUERY_ITEM_TREE && items[1].type == QUERY_ITEM_RELATION)
                {
                    temp = JoinTreeToRelation(&query->_cache, items[0].tree, items[1].relation, items[0].element_size, 0);
                }

                if (items[0].type == QUERY_ITEM_RELATION)
                {
                    if (items[0].relation->_cache_entry)
                    {
                        ((struct struct_query_cache_entry*)items[0].relation->_cache_entry)->_flags = QUERY_CACHE_FLAG_FREE;
                    }
                    else
                    {
                        struct struct_query_cache_entry* found = 0;
                        if (!CacheEntry(&query->_cache, items[0].relation->_data, &found))
                        {
                            items[0].relation->_cache_entry = found;
                            free(items[0].relation->_data->_data);
                            free(items[0].relation->_data);
                        }
                    }

                    free(items[0].relation);
                    items[0].relation = 0;
                }

                if (items[1].type == QUERY_ITEM_RELATION)
                {
                    if (items[1].relation->_cache_entry)
                    {
                        ((struct struct_query_cache_entry*)items[1].relation->_cache_entry)->_flags = QUERY_CACHE_FLAG_FREE;
                    }
                    else
                    {
                        struct struct_query_cache_entry* found = 0;
                        if (!CacheEntry(&query->_cache, items[1].relation->_data, &found))
                        {
                            items[1].relation->_cache_entry = found;
                            free(items[1].relation->_data->_data);
                            free(items[1].relation->_data);
                        }
                    }

                    free(items[1].relation);
                    items[1].relation = 0;
                }

				items[0].relation = temp;
                items[0].type = QUERY_ITEM_RELATION;
			}
			break;
		case OPCODE_SORT_RELATION:
			{
				struct struct_relation* temp = malloc(sizeof(struct struct_relation));
                struct struct_tree* tree = 0;
				struct struct_relation* r = 0;
				TREE_KEY key = 0;
				int id = 0;
				struct struct_query_cache_entry* entry;

                if (items[0].type == QUERY_ITEM_RELATION)
                {
                    tree = TreeCreate();
                    r = items[0].relation;

                    if (SearchCache(&query->_cache, r->_data->_element_size, r->_data->_index, &entry))
                    {
                        temp->_data = entry->_item;
                        temp->_cache_entry = entry;
                    }
                    else
                    {
                        temp->_data = CreateArrayList(r->_data->_index, r->_data->_element_size);
                        temp->_cache_entry = 0;
                    }

                    for (j = 0; j < r->_data->_index; ++j)
                    {
                        memmove(&key, (char*)r->_data->_data + j * r->_data->_element_size + query->_ins[i]._r1, query->_ins[i]._r2);
                        memmove(&id, (char*)r->_data->_data + j * r->_data->_element_size, sizeof(int));
                        key = (key << 32) | id;
                        TreeInsert(tree, key, (char*)r->_data->_data + j * r->_data->_element_size);
                    }
                }
                else
                {
                    struct struct_tree_stack_item stack[4];
                    int stack_pos = 0;
                    void* row = 0;
                    TREE_KEY key = 0;

                    tree = TreeCreate();

                    stack[0].pos = stack_pos;
                    stack[0].tree = items[0].tree;
                    InitWalkTree(stack, &stack_pos);

                    while (WalkTree(stack, &stack_pos, &row, &key))
                    {
                        memmove(&key, (char*)row + query->_ins[i]._r1, query->_ins[i]._r2);
                        memmove(&id, (char*)row, sizeof(int));
                        key = (key << 32) | id;
                        TreeInsert(tree, key, row);
                    }

                    if (SearchCache(&query->_cache, items[0].element_size, 1024, &entry))
                    {
                        temp->_data = entry->_item;
                        temp->_cache_entry = entry;
                    }
                    else
                    {
                        temp->_data = CreateArrayList(1024, items[0].element_size);
                        temp->_cache_entry = 0;
                    }
                }

				BuildSortedRelation(tree, temp);
				items[query->_ins[i]._r4].relation= temp;
                items[query->_ins[i]._r4].type = QUERY_ITEM_RELATION;
                TreeDestroy(tree);

                if (r)
                {
                    if (r->_cache_entry)
                    {
                        ((struct struct_query_cache_entry*)r->_cache_entry)->_flags = QUERY_CACHE_FLAG_FREE;
                    }
                    else
                    {
                        struct struct_query_cache_entry* found = 0;
                        if (!CacheEntry(&query->_cache, r->_data, &found))
                        {
                            r->_cache_entry = found;
                            free(r->_data->_data);
                            free(r->_data);
                        }
                    }

                    free(r);
                }
			}
			break;
		}
	}

	for (i = 0; i < query->_tableCount; ++i)
	{
		table = (struct struct_table*)domain->_data + i;
		TableReleaseReaderLock(table);
	}

    if (items[1].type == QUERY_ITEM_RELATION)
    {
        if (items[1].relation && !items[1].relation->_cache_entry)
        {
            free(items[1].relation->_data->_data);
            free(items[1].relation->_data);
        }
        free(items[1].relation);
    }

    if (items[0].type == QUERY_ITEM_TREE)
    {
        struct struct_query_cache_entry* entry;
        struct struct_relation* relation = malloc(sizeof(struct struct_relation));

        // Sort an index which is of type Tree

        if (SearchCache(&query->_cache, items[0].element_size, 1000, &entry))
        {
            relation->_data = entry->_item;
            relation->_cache_entry = entry;
        }
        else
        {
            relation->_data = CreateArrayList(1000, items[0].element_size);
            relation->_cache_entry = 0;
        }

        BuildSortedRelation(items[0].tree, relation);
        
        return relation;
    }

	return items[0].relation;
}

static int LookupKey(const struct array_list* domain, int table, const char* keystring)
{
	int i;
	struct struct_table* t = (struct struct_table*)domain->_data + table;

	for (i = 0; i < t->_schema._index; ++i)
	{
		if (strcmp(t->_schema._column[i]._name, keystring) == 0)
		{
			return i;
		}
	}

	return -1;
}

static int LookupTable(const struct array_list* domain, const char* string)
{
	int i;
	struct struct_table* table;

	for (i = 0; i < domain->_index; ++i)
	{
		table = (struct struct_table*)domain->_data + i;
		if (strcmp(table->_name, string) == 0)
		{
			return i;
		}
	}

	return -1;
}

#define TOKEN_WHERE 0
#define TOKEN_SELECT 1
#define TOKEN_ORDER 2
#define TOKEN_GROUP 3
#define TOKEN_BY 4
#define TOKEN_STRING 5
#define TOKEN_COMMA 6
#define TOKEN_EQUALS 7
#define TOKEN_DOT 8

static int ReadNextToken(const char* query, int len, int* index, char* string, char* delim)
{
	const char* tokens = ".=,";
	int i;
	int j = 0;
	int match = 0;
	int advance = 0;

	while (*index <= len)
	{
		if (j > 0) // remove empty entries
		{
			string[j] = '\0';
			
			if (strcmp(string, ",") == 0)
			{
				return TOKEN_COMMA;
			}
			else if (strcmp(string, ".") == 0)
			{
				return TOKEN_DOT;
			}
			else if (strcmp(string, "=") == 0)
			{
				return TOKEN_EQUALS;
			}
			
			for (i = 0; i < strlen(delim); ++i)
			{
				if (delim[i] == query[*index])
				{
					match = 1;
					advance = 1;
					break;
				}
			}

			for (i = 0; i < strlen(tokens); ++i)
			{
				if (tokens[i] == query[*index])
				{
					match = 1;
					advance = 0;
					break;
				}
			}
			
			if (match || *index == len)
			{
				if (advance)
				{
					while (match)
					{
						++*index;
						match = 0;

						for (i = 0; i < strlen(delim); ++i)
						{
							if (delim[i] == query[*index])
							{
								match = 1;
								break;
							}
						}
					}
				}

				if (strcmp(string, "SELECT") == 0)
				{
					return TOKEN_SELECT;
				}
				else if (strcmp(string, "WHERE") == 0)
				{
					return TOKEN_WHERE;
				}
				else if (strcmp(string, "GROUP") == 0)
				{
					return TOKEN_GROUP;
				}
				else if (strcmp(string, "ORDER") == 0)
				{
					return TOKEN_ORDER;
				}
				else if (strcmp(string, "BY") == 0)
				{
					return TOKEN_BY;
				}
				else
				{
					return TOKEN_STRING;
				}
			}
		}

		string[j++] = query[*index];
		++*index;
	}

	return -1;
}

static void Trim(char* string)
{
	int i = 0;
	int len = strlen(string);
	
	while (string[i] == ' ') { ++i; }

	if (i > 0)
	{
		memmove(string, string + i, len - i + 1);
	}

	i = len - i;
	while (string[i] == ' ')
	{
		string[i] = '\0';
		--i;
	}
}

struct struct_select
{
	char tables[8];
	int tablecount;
};

static int ReadSelectClause(const struct array_list* domain, const char* query, int len, int* index, struct struct_select* select)
{
	int prev;
	int token;
	char string[128];

	select->tablecount = 0;

	while (*index < len)
	{
		prev = *index;
		// Split the tables by the empty string
		token = ReadNextToken(query, len, index, string, " ");

		if (token == TOKEN_STRING)
		{
			// token should be table name
			Trim(string);
			select->tables[select->tablecount] = LookupTable(domain, string);

			if (select->tables[select->tablecount] == -1)
			{
				return 0;
			}

			++select->tablecount;
			
			prev = *index;
			token = ReadNextToken(query, len, index, string, " ");
			if (token == TOKEN_COMMA)
			{
				continue;
			}
			else
			{
				break;
			}
		}
		else if (select->tablecount == 0)
		{
			return 0;
		}
	}

	*index = prev;
	return 1;
}

struct struct_where
{
	int table;
	int key;
	int operator;
	int value;
};

static int ReadWhereClause(const struct array_list* domain, const char* query, int len, int* index, struct struct_where* where)
{
	int token;
	char string[128];

	token = ReadNextToken(query, len, index, string, " ");

	if (token != TOKEN_STRING)
		return 0;

	where->table = LookupTable(domain, string);

	token = ReadNextToken(query, len, index, string, " ");

	if (token != TOKEN_DOT)
		return 0;

	token = ReadNextToken(query, len, index, string, " ");

	if (token != TOKEN_STRING)
		return 0;

	where->key = LookupKey(domain, where->table, string);

	token = ReadNextToken(query, len, index, string, " ");

	if (token != TOKEN_EQUALS)
		return 0;

	where->operator = TOKEN_EQUALS;

	token = ReadNextToken(query, len, index, string, " ");

	if (token != TOKEN_STRING)
		return 0;

	where->value = strtol(string, 0, 10);

	return 1;
}

struct struct_order
{
	char name[128];
	int operator;
};

static int ReadOrderClause(const struct array_list* domain, const char* query, int len, int* index, struct struct_order* order)
{
	int token;
	char string[128];

	token = ReadNextToken(query, len, index, string, " ");

	// Only valid option now
	if (token == TOKEN_BY)
	{
		token = ReadNextToken(query, len, index, string, " ");

		if (token == TOKEN_STRING)
		{
			order->operator = TOKEN_BY;
			strcpy(order->name, string);
			return 1;
		}
	}

	return 0;
}

struct struct_group
{
	char name[128];
	int operator;
};

static int ReadGroupClause(const struct array_list* domain, const char* query, int len, int* index, struct struct_group* group)
{
	int token;
	char string[128];

	token = ReadNextToken(query, len, index, string, " ");

	// Only valid option now
	if (token == TOKEN_BY)
	{
		token = ReadNextToken(query, len, index, string, " ");

		if (token == TOKEN_STRING)
		{
			group->operator = TOKEN_BY;
			strcpy(group->name, string);
			return 1;
		}
	}

	return 0;
}

static void SetError(char* in, char* out, int errorLen)
{
	strncpy(out, in, min(errorLen, strlen(in)+1));
}

void EmitPushIndex(struct struct_query_opcode* ins, int table, int index)
{
	ins->_opcode = OPCODE_PUSH_INDEX;
	ins->_r1 = table;
	ins->_r2 = index;
}

void EmitFilterBool(struct struct_query_opcode* ins, int value, int result)
{
	ins->_opcode = OPCODE_SORT_FILTER_BOOL;
	ins->_r1 = value;
	ins->_r4 = result;
}

void EmitMerge(struct struct_query_opcode* ins)
{
	ins->_opcode = OPCODE_MERGE;
}

void EmitFilter(struct struct_query_opcode* ins, int offset, int width, void* data, int result)
{
	ins->_opcode = OPCODE_FILTER;
	ins->_r1 = offset;
	ins->_r2 = width;
	memcpy(ins->_r3, data, width);
	ins->_r4 = result;
}

void EmitSort(struct struct_query_opcode* ins, int result)
{
	ins->_opcode = OPCODE_SORT_FILTERED;
	ins->_r4 = result;
}

void EmitSortRelation(struct struct_query_opcode* ins, int offset, int size, int result)
{
	ins->_opcode = OPCODE_SORT_RELATION;
	ins->_r1 = offset;
	ins->_r2 = size;
	ins->_r4 = result;
}

int EmitQuery(struct array_list* domain,
	struct struct_query* query,
	struct struct_where* where,
	struct struct_select* select,
	struct struct_group* group,
	struct struct_order* order)
{
	int success;
	int i;
	struct struct_table* table;

	query->_count = 0;
	query->_tableCount = select->tablecount;
	memcpy(query->_tables, select->tables, sizeof(select->tables));

	// Search for index which matches the filter query
	success = 0;
	table = (struct struct_table*)domain->_data + where->table;
	for (i = 0; i < table->_indexes->_index; ++i)
	{
		struct struct_base_index* index = (struct struct_base_index*) table->_indexes->_data + i;
		if (index->_key == where->key && index->_filtered)
		{
			EmitPushIndex(&query->_ins[query->_count++], where->table, i);

            if (index->_type == INDEX_TYPE_FILTER_BOOL)
			{
				// TODO: always emit in the first register?
				EmitFilterBool(&query->_ins[query->_count++], where->value, 0);
			}

			success = 1;
			break;
		}
	}

	if (!success)
		return 0;

	if (select->tablecount >= 2)
	{
		// Merge remaining tables
		success = 0;

		for (i = 0; i < select->tablecount; ++i)
		{
			if (i != where->table)
			{
				// TODO: just emitting the index by id, i.e. 0
				EmitPushIndex(&query->_ins[query->_count++], select->tables[i], 0);
				EmitSort(&query->_ins[query->_count++], 1);
				EmitMerge(&query->_ins[query->_count++]);
			}
		}
	}

	if (order->operator >= 0)
	{
		int j;
		int key;
		int width;
		int offset = 0;

		for (i = 0; i < select->tablecount; ++i)
		{
			table = (struct struct_table*)domain->_data + select->tables[i];
			key = LookupKey(domain, select->tables[i], order->name);
			if (key >= 0)
			{
				width = table->_schema._column[key]._width;
				offset += table->_schema._column[key]._offset;
				break;
			}
			else
			{
				offset += table->_data->_element_size;
			}
		}
		
		if (key == -1)
		{
			return 0;
		}
		else
		{
			EmitSortRelation(&query->_ins[query->_count++], offset, width, 0);
		}
	}

	return 1;
}

struct struct_query* CompileQuery(const char* query, struct array_list* domain, char* error, int errorLen)
{
	struct struct_query* compiled = malloc(sizeof(struct struct_query)); // TODO: we are leaking memory if the compilied query returns 0
	int i = 0;
	int len = strlen(query);
	char current[32];
	int token;
	struct struct_select select;
	struct struct_where where;
	struct struct_group group;
	struct struct_order order;

	compiled->_cache._index = 0;
	select.tablecount = 0;
	where.key = -1;
	where.table = -1;
	group.operator = -1;
	order.operator = -1;

	while (i < len)
	{
		token = ReadNextToken(query, len, &i, current, " ");
		
		switch (token)
		{
		case TOKEN_SELECT:
			{
				if (ReadSelectClause(domain, query, len, &i, &select) == 0)
				{
					SetError("Invalid SELECT clause", error, errorLen);
					return 0;
				}
			}
			break;
		case TOKEN_WHERE:
			{
				if (ReadWhereClause(domain, query, len, &i, &where) == 0)
				{
					SetError("Invalid WHERE clause", error, errorLen);
					return 0;
				}
			}
			break;
		case TOKEN_GROUP:
			{
				if (ReadGroupClause(domain, query, len, &i, &group) == 0)
				{
					SetError("Invalid GROUP clause", error, errorLen);
					return 0;
				}
			}
			break;
		case TOKEN_ORDER:
		{
			if (ReadOrderClause(domain, query, len, &i, &order) == 0)
			{
				SetError("Invalid ORDER clause", error, errorLen);
				return 0;
			}
		}
		break;
		default:
			{
				SetError("Encountered unexpected token", error, errorLen);
				return 0;
			}
			break;
		}
	}

	if (select.tablecount == 0)
	{
		SetError("No tables selected in the query", error, errorLen);
		return 0;
	}

	if (where.key < 0)
	{
		SetError("No key selected for where clause", error, errorLen);
		return 0;
	}

	if (where.table < 0)
	{
		SetError("No table set for where clause", error, errorLen);
		return 0;
	}

	if (select.tablecount >= 2 && group.operator < 0)
	{
		SetError("No grouping clause provided, but multiple tables selected", error, errorLen);
		return 0;
	}

	if (EmitQuery(domain, compiled, &where, &select, &group, &order) == 0)
	{
		SetError("Error generating opcode", error, errorLen);
		return 0;
	}

	return compiled;
}




