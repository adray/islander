#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include "tree.h"

struct struct_tree* TreeCreate()
{
	struct struct_tree* tree = malloc(sizeof(struct struct_tree));
	tree->isLeaf = 1;
	tree->numKeys = 0;
	return tree;
}

// Binary search to find the position of key in the tree
int SearchKey(struct struct_tree* tree, TREE_KEY key)
{
	int lo;
	int mid;
	int hi;

	lo = -1;
	hi = tree->numKeys;

	while (lo + 1 < hi)
	{
		mid = (lo + hi) / 2;
		if (tree->keys[mid] == key)
		{
			return mid;
		}
		else if (tree->keys[mid] < key)
		{
			lo = mid;
		}
		else
		{
			hi = mid;
		}
	}

	return hi;
}

void TreeDestroy(struct struct_tree* tree)
{
	if (!tree->isLeaf)
	{
		int i;
		for (i = 0; i < tree->numKeys+1; ++i)
		{
			TreeDestroy(tree->childNodes[i]);
		}
	}

	free(tree);
}

struct struct_tree* TreeInsertInternal(struct struct_tree* tree, TREE_KEY key, void* value, TREE_KEY* median, void** medianvalue)
{
	struct struct_tree* next;
	int pos = SearchKey(tree, key);
	int mid;
	void* midvalue;

	if (pos < tree->numKeys && tree->keys[pos] == key)
	{
		return 0;
	}

	if (tree->isLeaf)
	{
		memmove(&tree->keys[pos + 1], &tree->keys[pos], sizeof(TREE_KEY) * (tree->numKeys - pos));
		memmove(&tree->values[pos + 1], &tree->values[pos], sizeof(void*) * (tree->numKeys - pos));
		tree->keys[pos] = key;
		tree->values[pos] = value;
		tree->numKeys++;
	}
	else
	{
		next = TreeInsertInternal(tree->childNodes[pos], key, value, median, &midvalue);

		if (next)
		{
			memmove(&tree->keys[pos + 1], &tree->keys[pos], sizeof(TREE_KEY) * (tree->numKeys - pos));
			memmove(&tree->values[pos + 1], &tree->values[pos], sizeof(void*) * (tree->numKeys - pos));
			memmove(&tree->childNodes[pos + 2], &tree->childNodes[pos + 1], sizeof(struct struct_tree*) * (tree->numKeys - pos));

			tree->keys[pos] = *median;
			tree->values[pos] = midvalue;
			tree->childNodes[pos + 1] = next;
			tree->numKeys++;
		}
	}

	if (tree->numKeys >= TREE_KEY_COUNT)
	{
		mid = tree->numKeys / 2;
		*median = tree->keys[mid];
		*medianvalue = tree->values[mid];

		next = malloc(sizeof(struct struct_tree));
		next->numKeys = tree->numKeys - mid - 1;
		next->isLeaf = tree->isLeaf;
		
		memmove(next->keys, &tree->keys[mid + 1], sizeof(TREE_KEY) * next->numKeys);
		memmove(next->values, &tree->values[mid + 1], sizeof(void*) * next->numKeys);
		if (!next->isLeaf)
		{
			memmove(&next->childNodes, &tree->childNodes[mid + 1], sizeof(struct struct_tree*) * (next->numKeys + 1));
		}

		tree->numKeys = mid;
		return next;
	}

	return 0;
}

void TreeInsert(struct struct_tree* tree, TREE_KEY key, void* value)
{
	TREE_KEY median;
	void* medianvalue;
	struct struct_tree* left;
	struct struct_tree* right = TreeInsertInternal(tree, key, value, &median, &medianvalue);

	if (right)
	{
		left = malloc(sizeof(struct struct_tree));

		memmove(left, tree, sizeof(struct struct_tree));

		tree->isLeaf = 0;
		tree->numKeys = 1;
		tree->keys[0] = median;
		tree->values[0] = medianvalue;
		tree->childNodes[0] = left;
		tree->childNodes[1] = right;
	}
}

void* TreeLookup(struct struct_tree* tree, TREE_KEY key)
{
	int pos;

	if (tree->numKeys == 0)
		return 0;

	pos = SearchKey(tree, key);

	if (tree->isLeaf)
	{
		if (tree->numKeys > pos && tree->keys[pos] == key)
		{
			return tree->values[pos];
		}
	}
	else
	{
		if (tree->keys[pos] == key)
		{
			return tree->values[pos];
		}
		else
		{
			return TreeLookup(tree->childNodes[pos], key);
		}
	}

	return 0;
}

void TreeDelete(struct struct_tree* tree, TREE_KEY key)
{
	int pos = SearchKey(tree, key);

	if (tree->isLeaf)
	{
        if (tree->numKeys >= pos && tree->keys[pos] == key)
        {
            tree->numKeys--;
            assert(tree->numKeys >= 0);
            memmove(&tree->keys[pos], &tree->keys[pos + 1], sizeof(TREE_KEY) * (tree->numKeys - pos));
            memmove(&tree->values[pos], &tree->values[pos + 1], sizeof(void*) * (tree->numKeys - pos));
        }
	}
	else
	{
		if (tree->numKeys > pos && tree->keys[pos] == key)
		{
			if (tree->childNodes[pos + 1]->numKeys > 0)
			{
				// Promote the first element in the subsequent node
				tree->keys[pos] = tree->childNodes[pos + 1]->keys[0];
				tree->values[pos] = tree->childNodes[pos + 1]->values[0];

				tree->childNodes[pos + 1]->numKeys--;
                assert(tree->childNodes[pos + 1]->numKeys >= 0);
				memmove(&tree->childNodes[pos + 1]->keys[0], &tree->childNodes[pos + 1]->keys[1], sizeof(TREE_KEY) * tree->childNodes[pos + 1]->numKeys);
                memmove(&tree->childNodes[pos + 1]->values[0], &tree->childNodes[pos + 1]->values[1], sizeof(void*) * tree->childNodes[pos + 1]->numKeys);
                
                // TODO: copy the childNodes
			}
			else if (tree->childNodes[pos]->numKeys > 0)
			{
				// Promote the last element in the previous node
				int keys = tree->childNodes[pos]->numKeys;
				tree->keys[pos] = tree->childNodes[pos]->keys[keys-1];
                tree->values[pos] = tree->childNodes[pos]->values[keys - 1];

                if (tree->childNodes[pos]->isLeaf)
                {
                    tree->childNodes[pos]->numKeys--;
                    assert(tree->childNodes[pos]->numKeys >= 0);
                }
                else
                {
                    // TODO: Promote the first element 
                }
			}
            else if (tree->numKeys > pos + 1)
            {
                // Promote sibling
                memmove(&tree->keys[pos], &tree->keys[pos + 1], sizeof(TREE_KEY) * (tree->numKeys - pos - 1));
                memmove(&tree->values[pos], &tree->values[pos + 1], sizeof(void*) * (tree->numKeys - pos - 1));
                memmove(&tree->childNodes[pos], &tree->childNodes[pos + 1], sizeof(struct struct_tree*) * (tree->numKeys - pos));

                tree->numKeys--;
                assert(tree->numKeys >= 0);
            }
            else
            {
                // This is the only element in the (sub-tree).
                tree->numKeys--;
                assert(tree->numKeys >= 0);
            }
		}
		else
		{
			TreeDelete(tree->childNodes[pos], key);
		}
	}
}

void TreeUpdate(struct struct_tree* tree, TREE_KEY key, void* new_value)
{
	int pos;

	if (tree->numKeys == 0)
		return;

	pos = SearchKey(tree, key);

	if (tree->isLeaf)
	{
		if (tree->keys[pos] == key)
		{
			tree->values[pos] = new_value;
		}
	}
	else
	{
		if (tree->keys[pos] == key)
		{
			tree->values[pos] = new_value;
		}
		else
		{
			TreeUpdate(tree->childNodes[pos], key, new_value);
		}
	}
}

static int TreeQueryMemoryUsageCore(struct struct_tree* tree, int usage)
{
	int i;
	usage += sizeof(struct struct_tree);
	if (!tree->isLeaf)
	{
		for (i = 0; i < tree->numKeys+1; i++)
		{
			usage += TreeQueryMemoryUsageCore(tree->childNodes[i], 0);
		}
	}
	return usage;
}

int TreeQueryMemoryUsage(struct struct_tree* tree)
{
	return TreeQueryMemoryUsageCore(tree, 0);	
}

void InitWalkTree(struct struct_tree_stack_item* stack, int* stack_pos)
{
    struct struct_tree_stack_item* tree = &stack[*stack_pos];
    while (!tree->tree->isLeaf)
    {
        *stack_pos = *stack_pos + 1;

        stack[*stack_pos].pos = 0;
        stack[*stack_pos].tree = tree->tree->childNodes[0];
        tree = &stack[*stack_pos];
    }
}

int WalkTree(struct struct_tree_stack_item* stack, int* stack_pos, void** data, TREE_KEY* key)
{
    if (*stack_pos < 0) { return 0; }

    struct struct_tree_stack_item* tree = &stack[*stack_pos];
    int hasData = 0;
    if (tree->tree->isLeaf)
    {
        if (tree->pos < tree->tree->numKeys)
        {
            *data = tree->tree->values[tree->pos];
            *key = tree->tree->keys[tree->pos];
            tree->pos = tree->pos + 1;
            hasData = 1;
        }

        if (tree->pos >= tree->tree->numKeys)
        {
            *stack_pos = *stack_pos - 1;
            if (*stack_pos >= 0)
            {
                stack[*stack_pos].pos++;
                if (!hasData)
                {
                    return WalkTree(stack, stack_pos, data, key);
                }
            }
            else if (!hasData)
            {
                return 0;
            }
        }
    }
    else
    {
        if (tree->pos <= tree->tree->numKeys)
        {
            *data = tree->tree->values[tree->pos - 1];
            *key = tree->tree->keys[tree->pos - 1];
            stack[*stack_pos + 1].pos = 0;
            stack[*stack_pos + 1].tree = tree->tree->childNodes[tree->pos];
            *stack_pos = *stack_pos + 1;
        }
        else
        {
            *stack_pos = *stack_pos - 1;
            if (*stack_pos >= 0)
            {
                stack[*stack_pos].pos++;
            }
            return WalkTree(stack, stack_pos, data, key);
        }
    }

    return 1;
}
