

#ifndef H_ARRAY_LIST
#define H_ARRAY_LIST

#include "types.h"

struct array_list
{
	short _element_size;
	int _index;
	int _count;
	void* _data;
};

struct array_list_iterator
{
	int _index;
	struct array_list* _list;
	ISLANDER_BOOLEAN(*_iterate)(struct base_iterator* it);
};

struct array_list* CreateArrayList(int initial_size, int element_size);

void DestroyArrayList(struct array_list* list);

void ResizeArrayList(struct array_list* list, int new_size);

void AddArrayListItem(struct array_list* list);

#endif
