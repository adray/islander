
#ifndef H_JOIN
#define H_JOIN

struct struct_relation;
struct struct_query_list_cache;
struct struct_tree;

struct struct_relation* JoinRelations(struct struct_query_list_cache* cache, struct struct_relation* first, struct struct_relation* second);
struct struct_relation* JoinTreeToTree(struct struct_query_list_cache* cache, struct struct_tree* first, struct struct_tree* second, int first_size, int second_size);
struct struct_relation* JoinTreeToRelation(struct struct_query_list_cache* cache, struct struct_tree* first, struct struct_relation* second, int first_size, int reversed);

#endif
