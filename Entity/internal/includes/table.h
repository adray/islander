
#ifndef H_TABLE
#define H_TABLE

#include "types.h"

#define TABLE_MAX_COLUMNS 32
#define TABLE_NAME_LENGTH 128

struct array_list;
struct struct_tree;
struct bool_tree;

struct struct_column
{
	short _offset;
	short _width;
	char _annotation;
	char _name[TABLE_NAME_LENGTH];
};

struct struct_table_schema
{
	int _index;
	int _primarykey;
	struct struct_column _column[TABLE_MAX_COLUMNS];
};

struct struct_handler
{
	ISLANDER_ROW_CHANGED _call;
	void* _userData;
};

struct struct_table
{
	int _row_count;
	char _name[TABLE_NAME_LENGTH];
	char* _default_row;
	void* _lock;
	struct array_list* _free_rows;
	struct array_list* _data;
	struct array_list* _indexes;
	struct array_list* _callback;
	struct struct_table_schema _schema;
};

#define INDEX_TYPE_SORTED 0
#define INDEX_TYPE_FILTER_BOOL 1
#define INDEX_TYPE_FILTERED 2

struct struct_base_index
{
	char _sorted;
	char _filtered;
	char _key;
	char _type;
	void* _ptr;
};

struct struct_sorted_index
{
	char _sorted;
	char _filted;
	char _key;
	char _type;
	struct struct_tree* _tree;
};

struct struct_bool_index
{
	char _sorted;
	char _filtered;
	char _key;
	char _type;
	struct bool_tree* _tree;
};

struct struct_non_unique_index
{
	char _sorted;
	char _filted;
	char _key;
	char _type;
	void* _ptr; //TODO
};

struct struct_relation
{
	int _position;
	struct array_list* _data;
	void* _cache_entry;
};

struct struct_table* TableCreate(const char* name, int element_size, const char* primaryKey, int offset, int width);

void TableDestroy(struct struct_table* table);

void TableAcquireReaderLock(struct struct_table* table);

void TableAcquireWriterLock(struct struct_table* table);

void TableReleaseReaderLock(struct struct_table* table);

void TableReleaseWriterLock(struct struct_table* table);

int TableAddColumn(struct struct_table* table, const char* name, int offset, int width);

void TableSetPrimaryKey(struct struct_table* table, const char* name);

void TableSetDefault(struct struct_table* table, int offset, char* data, int width);

int TableAddRow(struct struct_table* table, void* row);

int TableGetRow(struct struct_table* table, int id, void* row);

void TableUpdateRow(struct struct_table* table, int id, void* row);

void TableUpdateRowPartial(struct struct_table* table, int id, void* row, int offset, int size);

void TableRemoveRow(struct struct_table* table, int id);

int TableRowCount(struct struct_table* table);

void TableAddIndex(struct struct_table* table, struct struct_base_index* index);

void TableAddHandler(struct struct_table* table, ISLANDER_ROW_CHANGED row, void* userData);

void TableGatherStats(struct struct_table* table, struct ISLANDER_INFO_BLOCK* block);

struct struct_relation* TableBuildRelationSorted(struct struct_table* table, int key);

struct struct_relation* TableBuildRelationFiltered(struct struct_table* table, int key, void* value);

#endif
