
#ifndef H_HASH
#define H_HASH

#include "tree.h"

#define HASH_ENTRIES 128

struct struct_hash
{
	int _count;
	struct struct_tree _entries[HASH_ENTRIES];
};

struct struct_hash* HashCreate();

void HashInsert(struct struct_hash* hash, const char* key, void* value);

void HashRemove(struct struct_hash* hash, const char* key, void* value);

void* HashFind(struct struct_hash* hash, const char* key);

void HashDestroy(struct struct_hash* hash);

#endif
