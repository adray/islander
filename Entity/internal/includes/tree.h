
#ifndef H_TREE
#define H_TREE

#include <stdint.h>

#define TREE_KEY_COUNT 1024

typedef int64_t TREE_KEY;

struct struct_tree
{
	int isLeaf;
	int numKeys;
	TREE_KEY keys[TREE_KEY_COUNT];
	void* values[TREE_KEY_COUNT];
	struct struct_tree* childNodes[TREE_KEY_COUNT + 1];
};

struct struct_tree_stack_item
{
    int pos;
    struct struct_tree* tree;
};

struct struct_tree* TreeCreate();

void TreeDestroy(struct struct_tree* tree);

void TreeInsert(struct struct_tree* tree, TREE_KEY key, void* value);

void* TreeLookup(struct struct_tree* tree, TREE_KEY key);

void TreeDelete(struct struct_tree* tree, TREE_KEY key);

void TreeUpdate(struct struct_tree* tree, TREE_KEY key, void* new_value);

int TreeQueryMemoryUsage(struct struct_tree* tree);

void InitWalkTree(struct struct_tree_stack_item* stack, int* stack_pos);

int WalkTree(struct struct_tree_stack_item* stack, int* stack_pos, void** data, TREE_KEY* key);

#endif
