#ifndef H_LOCK
#define H_LOCK

#define RW_LOCK void*

RW_LOCK CreateLock();

int TryAcquireWriterLock(RW_LOCK lock);

int TryAcquireReaderLock(RW_LOCK lock);

void ReleaseWriterLock(RW_LOCK lock);

void ReleaseReaderLock(RW_LOCK lock);

void DestroyLock(RW_LOCK lock);

#endif
