

#ifndef ITERATOR_H
#define ITERATOR_H

#include "types.h"

struct base_iterator
{
	int _index;
	void* _current;
	ISLANDER_BOOLEAN(*_iterate)(struct base_iterator* it);
};

#endif _ITERATOR_H
