

#ifndef H_BOOLTREE
#define H_BOOLTREE

struct struct_tree;

struct bool_tree
{
	struct struct_tree* _false;
	struct struct_tree* _true;
};

struct bool_tree* BoolTreeCreate();

void BoolTreeInsert(struct bool_tree* tree, int key, int id, void* data);

void BoolTreeUpdate(struct bool_tree* tree, int key, int id, void* data);

void BoolTreeDelete(struct bool_tree* tree, int key, int id);

void BoolTreeDestroy(struct bool_tree* tree);

#endif
