
#ifndef H_LINKED_LIST
#define H_LINKED_LIST

#include "types.h"

struct node_entry
{
	char _new;
	char _type;
	short _id;
	char _payload[12];
};

struct node_element
{
	short _count;
	short _index;
	int _freed;
	struct node_entry _data[1000];
	struct node_element* _next;
};

struct node_list
{
	struct node_element* _first;
	struct node_element* _last;
};

struct node_bookmark
{
	char _new;
	char _type;
	short _id;
	int _index;
	struct node_element* _element;
	struct node_bookmark** _self;
};

struct node_element_iterator
{
	int _index;
	struct node_element* _next;
	ISLANDER_BOOLEAN(*_iterate)(struct base_iterator* it);
};

void CreateList(struct node_list * list);

void AppendList(struct node_list * list);

char* LinkedListAddEntry(struct node_list* list, struct node_entry* entry);

struct node_entry* LinkedListGetEntryAt(struct node_list* list, int index);

#endif