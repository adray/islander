#ifndef H_CACHE
#define H_CACHE

#include <stdint.h>

struct array_list;

#define QUERY_CACHE_SIZE 12

#define QUERY_CACHE_FLAG_FREE 0
#define QUERY_CACHE_FLAG_IN_USE 1

struct struct_query_cache_entry
{
	int16_t _flags;
	int16_t _uses;
	struct array_list* _item;
};

struct struct_query_list_cache
{
	int32_t _index;
	struct struct_query_cache_entry _arrays[QUERY_CACHE_SIZE];
};

int CacheEntry(
struct struct_query_list_cache* cache,
struct array_list* list,
struct struct_query_cache_entry** found);

int SearchCache(
struct struct_query_list_cache* cache,
	int element_size,
	int count,
struct struct_query_cache_entry** found);

#endif
