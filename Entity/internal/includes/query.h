
#ifndef H_QUERY
#define H_QUERY

#include <stdint.h>
#include "cache.h"

struct struct_relation;
struct array_list;

#define MAX_OPCODE_STRING 128

#define OPCODE_PUSH_INDEX 0
#define OPCODE_SORT_FILTER_BOOL 1
#define OPCODE_SORT_FILTERED 2
#define OPCODE_FILTER 3
#define OPCODE_MERGE 4
#define OPCODE_SORT_RELATION 5

struct struct_query_opcode
{
	int8_t _opcode;
	int32_t _r1;
	int32_t _r2;
	int8_t _r3[MAX_OPCODE_STRING];
	int32_t _r4;
};

struct struct_query
{
	int16_t _count;
	int16_t _tableCount;
	struct struct_query_opcode _ins[16];
	int8_t _tables[8];
	struct struct_query_list_cache _cache;
};

struct struct_relation* ExecuteQuery(struct array_list* domain, struct struct_query* query);

struct struct_query* CompileQuery(const char* query, struct array_list* domain, char* error, int errorLen);

#endif
