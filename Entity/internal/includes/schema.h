
#ifndef SCHEMA_H
#define SCHEMA_H

#include "schema_type.h"
#include "linkedlist.h"

struct struct_schema_entry
{
	ISLANDER_SCHEMA_TYPE _type;
	ISLANDER_SCHEMA_NAME _name;
};

struct struct_schema
{
	struct node_list _entries;
};

struct struct_schema_library
{
	struct node_list _schema;
};

#endif
