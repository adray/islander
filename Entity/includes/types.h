

#ifndef H_TYPES
#define H_TYPES

typedef void* ISLANDER_WORLD;
typedef int ISLANDER_ENTITY;
typedef void* ISLANDER_COMPONENT;
typedef int ISLANDER_BOOLEAN;
typedef void* ISLANDER_QUERY;
typedef void* ISLANDER_POINTER;
typedef void* ISLANDER_SERIALIZATION_DATA;
typedef void* ISLANDER_STATS;

typedef void(*ISLANDER_ROW_CHANGED)(void* userData, void* row, int e);
#define ISLANDER_ROW_EVENT_ADDED 0
#define ISLANDER_ROW_EVENT_UPDATED 1
#define ISLANDER_ROW_EVENT_REMOVED 2

#define ISLANDER_COLUMN_INTEGER 1
#define ISLANDER_COLUMN_FLOAT 2
#define ISLANDER_COLUMN_POINTER 3
#define ISLANDER_COLUMN_BYTE 4
#define ISLANDER_COLUMN_STRUCTURE 5

struct ISLANDER_RESULT_SET
{
	int _count;
	void* _data;
	int _element_size;
	int _cached;
	void* _cache_entry;
};

struct ISLANDER_INFO_BLOCK
{
	int _id;
	int _free_rows;
	int _used_rows;
	int _max_rows;
	int _mem_usage;
};

#endif
