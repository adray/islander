
#ifndef H_ENTITY
#define H_ENTITY

#ifdef Islander_Entity_EXPORTS
#ifdef _WIN32
#define ENTITY_EXPORT __declspec( dllexport )
#else
#define ENTITY_EXPORT __attribute__ ((visibility("default")))
#endif
#else
#ifdef _WIN32
#define ENTITY_EXPORT __declspec( dllimport )
#else
#define ENTITY_EXPORT __attribute__ ((visibility("default")))
#endif
#endif

#include "types.h"
#include "schema_type.h"

struct ISLANDER_COMPONENT_SCHEMA
{
	char* _name;
	int _row_size;
	char* _primaryKey;
	int _priOffset;
	int _priWidth;
};

struct property_entry
{
	int key;
	float data[4];
};

struct row_entity
{
	int id;
	int flags;
	int count;
	struct property_entry entries[8];
};

// Create a new world object
ISLANDER_WORLD ENTITY_EXPORT IslanderCreateWorld();

// Destroy world object
void ENTITY_EXPORT IslanderDestroyWorld(ISLANDER_WORLD world);

// Serialize the object
ISLANDER_SERIALIZATION_DATA ENTITY_EXPORT IslanderSerialize(ISLANDER_WORLD world, int* count);

// Deserialize the object
void ENTITY_EXPORT IslanderDeserialize(ISLANDER_WORLD world, ISLANDER_SERIALIZATION_DATA data, int count);

// Set default value at offset
void ENTITY_EXPORT IslanderSetDefault(ISLANDER_WORLD world, int component, int offset, char* data, int width);

// Creates a new well known component type
int ENTITY_EXPORT IslanderAddComponentTable(ISLANDER_WORLD world, struct ISLANDER_COMPONENT_SCHEMA* schema);

// Gets the table for a well known component type
int ENTITY_EXPORT IslanderGetComponentTable(ISLANDER_WORLD world, const char* name);

// Add a column to a well know component type
int ENTITY_EXPORT IslanderAddComponentColumn(ISLANDER_WORLD world, int component, const char* name, int offset, int width);

// Add index to a well known component type
void ENTITY_EXPORT IslanderAddComponentIndex(ISLANDER_WORLD world, int component, int index_type, int key);

// Compiles a query against a world object
ISLANDER_QUERY ENTITY_EXPORT IslanderCompileQuery(ISLANDER_WORLD world, const char* query);

// Executes a query against a world object
void ENTITY_EXPORT IslanderExecuteQuery(ISLANDER_WORLD world, ISLANDER_QUERY query, struct ISLANDER_RESULT_SET* result);

// Free the result of the query
void ENTITY_EXPORT IslanderFreeResultSet(struct ISLANDER_RESULT_SET* result);

// Creates a new entity
ISLANDER_ENTITY ENTITY_EXPORT IslanderCreateEntity(ISLANDER_WORLD world);

// Frees an existing entity
void ENTITY_EXPORT IslanderFreeEntity(ISLANDER_WORLD world, ISLANDER_ENTITY entity);

// Determines if an entity is active
ISLANDER_BOOLEAN ENTITY_EXPORT IslanderIsEntityActive(ISLANDER_WORLD world, ISLANDER_ENTITY entity);

// Sets the active state of an entity
void ENTITY_EXPORT IslanderSetEntityActive(ISLANDER_WORLD world, ISLANDER_ENTITY entity, ISLANDER_BOOLEAN active);

// Adds a new component object to the table
void ENTITY_EXPORT IslanderAddComponent(ISLANDER_WORLD w, int type, void* row);

// Gets a component entry
void ENTITY_EXPORT IslanderGetComponent(ISLANDER_WORLD w, int type, int id, void* row);

// Updates a component entry
void ENTITY_EXPORT IslanderUpdateComponent(ISLANDER_WORLD w, int type, int id, void* row);

// Update a component entry (partial)
void ENTITY_EXPORT IslanderUpdateComponentPartial(ISLANDER_WORLD w, int type, int id, void* row, int offset, int size);

// Removes a component from an entity
void ENTITY_EXPORT IslanderRemoveComponent(ISLANDER_WORLD w, int type, int id);

// Gather entity stats
ISLANDER_STATS ENTITY_EXPORT IslanderGatherStats(ISLANDER_WORLD world, int* count);

// Grab an item from the stats
void ENTITY_EXPORT IslanderGetStatBlock(ISLANDER_STATS stats, int index, struct ISLANDER_INFO_BLOCK* block);

void ENTITY_EXPORT IslanderFreeStats(ISLANDER_STATS stats);

// Adds a handler which is triggered when the table for the component changes
void ENTITY_EXPORT IslanderAddComponentHandler(ISLANDER_WORLD w, int type, ISLANDER_ROW_CHANGED handler, void* userData);

// Register a component property.
int ENTITY_EXPORT IslanderRegisterComponentProperty(const char* name);

// Gets a component property.
int ENTITY_EXPORT IslanderGetComponentProperty(const char* name);

// Wrapper to allocate memory
ISLANDER_POINTER ENTITY_EXPORT IslanderAllocate(int numBytes);

// Wrapper to free memory
void ENTITY_EXPORT IslanderFree(void* ptr);

#endif
