========================================================================
    Islander Entity Overview
========================================================================

The entity library is the base for islander as shown below.

-------------------
	 MANAGED
-------------------
	 RENDERER
-------------------
     COMMON
-------------------
     ENTITY
-------------------

It operates very much like a database where the entity and
components are tables with rows of elements. The database
is contained entirely in memory and provides no persistancy
or recovery options and therefore should not be used in
place of an actual database. Creating an entity is simply
adding a new row to the tables. The system also provides
functionallity for querying the database and returning a
set of results. 

The goal of the library is to provide
querying of data in a continous chunk
which can be processed without taking too many cache misses.

Queries can be defined in a pseudo sql syntax, which allow grouping
and sorting data. An example of the syntax is the following:
"SELECT entity, transform WHERE entity.active=1 GROUP BY id"
This returns the combined result of the entities and transforms
which have the same id, filtering out those which are not active.

