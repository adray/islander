
#ifndef H_TEST
#define H_TEST

int main(const char** args, int c);

void assert(int value, int expected, const char* msg);

void TreeInsertTest(void);

void TreeBulkInsertTest(void);

void TreeSplitTest(void);

void TreeLookupTest(void);

void TreeUpdateTest(void);

void TreeDeleteTest(void);

void TreeBigDeleteTest(void);

void TreeDeleteReverseTest(void);

void TreeDeleteReverse2Test(void);

void TableInsertTest(void);

void TableBulkInsertTest(void);

void TableGetRowTest(void);

void TableUpdateTest(void);

void TableRemoveTest(void);

void TableHandlerTest(void);

void TableBuildRelationTest(void);

void TableQueryTest(void);

void TableLinearScanTest(void);

void TableJoinTest(void);

void IndexUpdateTest(void);

void QueryCompilerTest(void);

void QueryEmitTest(void);

void EntityCreateTest(void);

void EntityBulkCreateTest(void);

void EntityQueryCompileTest(void);

void HashTableTest(void);

void SerializationTest(void);

void CacheTest(void);

void GatherStatsTest(void);

void RowPartialUpdateTest(void);

#endif
