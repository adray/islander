#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "Test.h"
#include "tree.h"
#include "table.h"
#include "entity.h"
#include "arraylist.h"
#include "join.h"
#include "query.h"
#include "booltree.h"
#include "hash.h"
#include "cache.h"

#ifdef _WIN32
#include <Windows.h>
int GetTime()
{
	SYSTEMTIME time;
	GetSystemTime(&time);
	return time.wMilliseconds + time.wSecond * 1000 + time.wMinute * 60 * 1000 + time.wHour * 60 * 1000 * 60;
}

#else

// TODO: just a stub, anyway the windows implementation looks broken e.g. midnight
int GetTime()
{
	return 0;
}

#endif

int assertionFailures;
int failures;

void runTest(const char* name, void (func)(void))
{
	assertionFailures = 0;
	func();
	if (assertionFailures == 0)
	{
		printf("Test %s completed successfully\r\n", name);
	}
	else
	{
		printf("Test %s failed\r\n", name);
		failures++;
	}
}

void allTests()
{
	runTest("TreeInsertTest", TreeInsertTest);
	runTest("TreeBulkInsertTest", TreeBulkInsertTest);
	runTest("TreeSplitTest", TreeSplitTest);
	runTest("TreeLookupTest", TreeLookupTest);
	runTest("TreeUpdateTest", TreeUpdateTest);
	runTest("TreeDeleteTest", TreeDeleteTest);
    runTest("TreeBigDeleteTest", TreeBigDeleteTest);
    runTest("TreeDeleteReverseTest", TreeDeleteReverseTest);
    runTest("TreeDeleteReverse2Test", TreeDeleteReverse2Test);
	runTest("TableInsertTest", TableInsertTest);
	runTest("TableBulkInsertTest", TableBulkInsertTest);
	runTest("TableUpdateTest", TableUpdateTest);
	runTest("TableGetRowTest", TableGetRowTest);
	runTest("TableRemoveTest", TableRemoveTest);
	runTest("TableHandlerTest", TableHandlerTest);
	runTest("TableBuildRelationTest", TableBuildRelationTest);
	runTest("TableQueryTest", TableQueryTest);
	runTest("TableLinearScanTest", TableLinearScanTest);
	runTest("TableJoinTest", TableJoinTest);
	runTest("IndexUpdateTest", IndexUpdateTest);
	runTest("QueryCompilerTest", QueryCompilerTest);
	runTest("QueryEmitTest", QueryEmitTest);
	runTest("EntityCreateTest", EntityCreateTest);
    runTest("EntityBulkCreateTest", EntityBulkCreateTest);
	runTest("EntityQueryCompileTest", EntityQueryCompileTest);
	runTest("HashTableTest", HashTableTest);
	runTest("SerializationTest", SerializationTest);
	runTest("CacheTest", CacheTest);
    runTest("GatherStatsTest", GatherStatsTest);
    runTest("RowPartialUpdateTest", RowPartialUpdateTest);
}

int main(const char** args, int c)
{
	int diff;
	int start;
	failures = 0;

	start = GetTime();
	allTests();
	diff = GetTime() - start;
	
	if (failures == 0)
	{
		printf("All tests passed\r\n");
	}
	else
	{
		printf("Failed tests\r\n");
	}

	printf("Time Elapsed: %d\r\n", diff);
	return 0;
}

void assert(int value, int expected, const char* msg)
{
	if (value != expected)
	{
		assertionFailures++;
		if (msg)
		{
			printf(msg);
		}
		else
		{
			printf("Expected %d but was %d", expected, value);
		}
		printf("\r\n");
	}
}

void TreeInsertTest(void)
{
	TREE_KEY a = 1;
	TREE_KEY b = 2;
	TREE_KEY c = 3;

	struct struct_tree* tree = TreeCreate();
	
	TreeInsert(tree, 5, &a);
	TreeInsert(tree, 7, &b);
	TreeInsert(tree, 2, &c);

	// Assert entries
	assert(tree->numKeys, 3, 0);
	
	// Assert Keys
	assert(tree->keys[0], 2, 0);
	assert(tree->keys[1], 5, 0);
	assert(tree->keys[2], 7, 0);

	// Assert values
	assert(*(int*)tree->values[0], 3, 0);
	assert(*(int*)tree->values[1], 1, 0);
	assert(*(int*)tree->values[2], 2, 0);

	TreeDestroy(tree);
}

void TreeBulkInsertTest(void)
{
	int i = 0;
	struct struct_tree* tree = TreeCreate();

	for (i = 0; i < 1000; i++)
	{
		TreeInsert(tree, 1000-i, (char*)i);
	}

	for (i = 0; i < 1000; i++)
	{
		int k = 1000 - i;
		int v = (int)TreeLookup(tree, (TREE_KEY)k);
		assert(v, i, 0);
	}
}

void TreeSplitTest(void)
{
	struct struct_tree* tree = TreeCreate();
	TREE_KEY i;
	int array[TREE_KEY_COUNT];

	for (i = 0; i < TREE_KEY_COUNT; i++)
	{
		array[i] = i;
		TreeInsert(tree, i, &array[i]);
	}

	// There should be a single key for the 2 child nodes
	assert(tree->numKeys, 1, 0);
	assert(tree->isLeaf, 0, 0);
	assert(*(int*)tree->values[0], array[TREE_KEY_COUNT / 2], 0);

	assert(tree->childNodes[0]->numKeys, TREE_KEY_COUNT / 2, 0);
	assert(tree->childNodes[1]->numKeys, (TREE_KEY_COUNT / 2) - 1, 0);

	for (i = 0; i < tree->childNodes[0]->numKeys; i++)
	{
		assert(*(int*)tree->childNodes[0]->values[i], array[i], 0);
	}

	for (i = 0; i < tree->childNodes[1]->numKeys; i++)
	{
		assert(*(int*)tree->childNodes[1]->values[i], array[i + 1 + tree->childNodes[0]->numKeys], 0);
	}

	TreeDestroy(tree);
}

void TreeLookupTest(void)
{
	struct struct_tree* tree = TreeCreate();
	TREE_KEY i;
	int array[TREE_KEY_COUNT];

	for (i = 0; i < TREE_KEY_COUNT; i++)
	{
		array[i] = i;
		TreeInsert(tree, i, &array[i]);
	}

	assert(*(int*)TreeLookup(tree, 0), array[0], 0);
	assert(*(int*)TreeLookup(tree, TREE_KEY_COUNT / 2), array[TREE_KEY_COUNT / 2], 0);
	assert(*(int*)TreeLookup(tree, (TREE_KEY_COUNT / 2)+1), array[(TREE_KEY_COUNT / 2)+1], 0);
	assert((int)TreeLookup(tree, TREE_KEY_COUNT), 0, 0);
	assert((int)TreeLookup(tree, -1), 0, 0);

	TreeDestroy(tree);
}

void TreeUpdateTest(void)
{
	struct struct_tree* tree = TreeCreate();
	TREE_KEY i;
	int array[TREE_KEY_COUNT];

	for (i = 0; i < TREE_KEY_COUNT; i++)
	{
		array[i] = i;
		TreeInsert(tree, i, &array[i]);
	}

	TreeUpdate(tree, 0, &array[1]);
	assert(*(int*)TreeLookup(tree, 0), array[1], 0);

	TreeUpdate(tree, TREE_KEY_COUNT/2, &array[0]);
	assert(*(int*)TreeLookup(tree, TREE_KEY_COUNT / 2), array[0], 0);
}

void TreeDeleteTest(void)
{
	struct struct_tree* tree = TreeCreate();
	TREE_KEY i;
	int array[TREE_KEY_COUNT];

	for (i = 0; i < TREE_KEY_COUNT; i++)
	{
		array[i] = i;
		TreeInsert(tree, i, &array[i]);
	}

	TreeDelete(tree, 0);
	assert(tree->childNodes[0]->numKeys, TREE_KEY_COUNT / 2 - 1, 0);
	assert(tree->childNodes[0]->keys[0], 1, 0);
	assert(*(int*)tree->childNodes[0]->values[0], array[1], 0);
	assert(tree->childNodes[0]->keys[tree->childNodes[0]->numKeys], (TREE_KEY_COUNT / 2) - 1, 0);

	TreeDelete(tree, TREE_KEY_COUNT / 2);
	assert(tree->numKeys, 1, 0);
	assert(tree->childNodes[1]->numKeys, TREE_KEY_COUNT / 2 - 2, 0);

	// TODO: expect more cases which will not work
}

void ValidateNodes(TREE_KEY pos, struct struct_tree* tree, int* array, int* additional)
{
	int i, j;
	TREE_KEY key = 0;
	for (i = 0; i < TREE_KEY_COUNT; i++)
	{
		if (key > pos)
		{
			int result = *(int*)TreeLookup(tree, key);
			assert(result, array[i], 0);
		}
		key++;

		for (j = 0; j < 25; j++)
		{
			int result = *(int*)TreeLookup(tree, key++);
			assert(result, additional[j], 0);
		}
	}
}

void TreeBigDeleteTest(void)
{
	struct struct_tree* tree = TreeCreate();
	TREE_KEY i;
	TREE_KEY j;
	TREE_KEY key;
	int array[TREE_KEY_COUNT];
	int additional[TREE_KEY_COUNT];

	key = 0;
	for (i = 0; i < TREE_KEY_COUNT; i++)
	{
		array[i] = i;
		TreeInsert(tree, key++, &array[i]);

		for (j = 0; j < 25; j++)
		{
			additional[j] = j;
			TreeInsert(tree, key++, &additional[j]);
		}
	}

	// Assert the struture is built correctly
	key = 0;
	for (i = 0; i < TREE_KEY_COUNT; i++)
	{
		int result = *(int*)TreeLookup(tree, key++);
		assert(result, array[i], 0);
		ValidateNodes(key, tree, array, additional);

		for (j = 0; j < 25; j++)
		{
			result = *(int*)TreeLookup(tree, key++);
			assert(result, additional[j], 0);
		}
	}

	// Now delete, and check that the structure survives
	key = 0;
	for (i = 0; i < TREE_KEY_COUNT; i++)
	{
		int result = *(int*)TreeLookup(tree, key);
		assert(result, array[i], 0);
		TreeDelete(tree, key++);
		ValidateNodes(key, tree, array, additional);

		for (j = 0; j < 25; j++)
		{
			result = *(int*)TreeLookup(tree, key++);
			assert(result, additional[j], 0);
		}
	}
}

void TreeDeleteReverseTest(void)
{
    struct struct_tree* tree = TreeCreate();
    TREE_KEY i;
    TREE_KEY key;
    int value = 42;

    key = 0;
    for (i = 1024 * 5 - 1; i >= 0; i--)
    {
        TreeInsert(tree, i, (void*)&value);
    }

    for (i = -100; i < 1024 * 5; i++)
    {
        TreeDelete(tree, i);
    }
    assert(tree->numKeys, 0, 0);
}

void TreeDeleteReverse2Test(void)
{
    struct struct_tree* tree = TreeCreate();
    TREE_KEY i;
    TREE_KEY key;
    int value = 42;

    key = 0;
    for (i = 0; i < 1024; i++)
    {
        TreeInsert(tree, i * 1000, (void*)&value);
    }

    for (i = 1; i < 600; i++)
    {
        TreeInsert(tree, i, (void*)&value);
    }

    for (i = 0; i < 2000; i++)
    {
        TreeDelete(tree, i);
    }
}

struct struct_row
{
	int id;
	int col1;
	int col2;
};

void TableInsertTest(void)
{
	struct struct_table* table = TableCreate("table", sizeof(struct struct_row), "id", 0, sizeof(int));
	struct struct_row *testrow;
	
	struct struct_row row1;
	struct struct_row row2;

	row1.id = 0;
	row1.col1 = 5;
	row1.col2 = 3;
	TableAddRow(table, &row1);

	testrow = table->_data->_data;
	assert(testrow->id, row1.id, 0);
	assert(testrow->col1, row1.col1, 0);

	row2.id = 1;
	row2.col1 = 6;
	row2.col2 = 2;
	TableAddRow(table, &row2);

	testrow = (struct struct_row*)table->_data->_data + 1;
	assert(testrow->id, row2.id, 0);
	assert(testrow->col1, row2.col1, 0);
}

void TableBulkInsertTest(void)
{
	struct struct_table* table = TableCreate("table", sizeof(struct struct_row), "id", 0, sizeof(int));
	struct struct_row *testrow;
	struct struct_row row;
	int i;

	for (i = 0; i < 100000; i++)
	{
		row.id = i;
		row.col1 = i % 6;
		row.col2 = i % 4 + 5;
		TableAddRow(table, &row);

		testrow = (struct struct_row*)table->_data->_data + i;
		assert(testrow->id, row.id, 0);
		assert(testrow->col1, row.col1, 0);
	}
}

void TableUpdateTest(void)
{
	struct struct_table* table = TableCreate("table", sizeof(struct struct_row), "id", 0, sizeof(int));
	struct struct_row *testrow;
	struct struct_row row;

	// Add row
	row.id = 0;
	row.col1 = 3;
	row.col2 = 4;
	TableAddRow(table, &row);

	// Update row
	row.col1 = 4;
	row.col2 = 7;
	TableUpdateRow(table, row.id, &row);

	testrow = (struct struct_row*)table->_data->_data;
	assert(testrow->id, row.id, 0);
	assert(testrow->col1, row.col1, 0);
	assert(testrow->col2, row.col2, 0);
}

void TableGetRowTest(void)
{
	struct struct_table* table = TableCreate("table", sizeof(struct struct_row), "id", 0, sizeof(int));
	struct struct_row row;
	struct struct_row *testrow;
	int i;

	for (i = 0; i < 1000; i++)
	{
		row.id = i;
		row.col1 = i % 6;
		row.col2 = i % 4 + 5;
		TableAddRow(table, &row);

		testrow = (struct struct_row*)table->_data->_data + i;
		assert(testrow->id, row.id, 0);
		assert(testrow->col1, row.col1, 0);
	}

	i = TableGetRow(table, 5, &row);
	assert(i, 1, 0);
	assert(row.id, 5, 0);
	assert(row.col1, 5 % 6, 0);
	assert(row.col2, 5 % 4 + 5, 0);

	i = TableGetRow(table, 9000, &row);
	assert(i, 0, 0);
}

void TableRemoveTest(void)
{
	struct struct_table* table = TableCreate("table", sizeof(struct struct_row), "id", 0, sizeof(int));
	struct struct_row *testrow;
	struct struct_row row;
	int i;

	for (i = 0; i < 1000; i++)
	{
		row.id = i;
		row.col1 = i % 6;
		row.col2 = i % 4 + 5;
		TableAddRow(table, &row);

		testrow = (struct struct_row*)table->_data->_data + i;
		assert(testrow->id, row.id, 0);
		assert(testrow->col1, row.col1, 0);
	}

	assert(table->_row_count, 1000, 0);

	TableRemoveRow(table, 8);
	assert(table->_row_count, 999, 0);

	TableRemoveRow(table, 99);
	assert(table->_row_count, 998, 0);

	assert(table->_free_rows->_index, 2, 0);
	assert(table->_free_rows->_element_size, sizeof(int), 0);
	assert(table->_free_rows->_count, 128, 0);
	assert(*(int*)table->_free_rows->_data, sizeof(row) * 8, "Free row insert failure");

	row.id = 10000;
	row.col1 = 4;
	row.col2 = 5;
	assert(TableAddRow(table, &row), 1, 0);

	row.id = 10001;
	row.col1 = 3;
	row.col2 = 2;
	assert(TableAddRow(table, &row), 1, 0);

	assert(table->_free_rows->_index, 0, 0);

	assert(table->_row_count, 1000, 0);
	assert(table->_data->_index, 1000, 0);

	testrow = (struct struct_row*)table->_data->_data + 8;
	assert(testrow->id, 10000, 0);
	assert(testrow->col1, 4, 0);
	assert(testrow->col2, 5, 0);

	testrow = (struct struct_row*)table->_data->_data + 99;
	assert(testrow->id, 10001, 0);
	assert(testrow->col1, 3, 0);
	assert(testrow->col2, 2, 0);
}

void Handler(void* userData, void* row, int e)
{
	struct struct_row* rowptr = row;
	assert(rowptr->id, 0, 0);
	assert(rowptr->col1, 2, 0);
	assert(rowptr->col2, 4, 0);
	assert(e, 0, 0);
	assert(*(int*)userData, 5, 0);
}

void TableHandlerTest(void)
{
	int userData = 5;
	struct struct_table* table = TableCreate("table", sizeof(struct struct_row), "id", 0, sizeof(int));
	struct struct_row row;

	TableAddHandler(table, &Handler, &userData);

	row.id = 0;
	row.col1 = 2;
	row.col2 = 4;
	TableAddRow(table, &row);
}

void TableBuildRelationTest(void)
{
	struct struct_table* table = TableCreate("table", sizeof(struct struct_row), "id", 0, sizeof(int));
	int i;
	struct struct_row row;
	struct struct_row* testrow;
	struct struct_relation* relation;

	for (i = 0; i < 1000; i++)
	{
		row.id = 999-i;
		row.col1 = i % 6;
		row.col2 = i % 4 + 5;
		TableAddRow(table, &row);
	}

	relation = TableBuildRelationSorted(table, 0);

	for (i = 0; i < 1000; i++)
	{
		testrow = (struct struct_row*)relation->_data->_data + i;
		assert(testrow->id, i, 0);
	}
}

struct struct_joined_row
{
	int id1;
	int col1;
	int col2;
	int id2;
	int col3;
	int col4;
};

void TableJoinTest(void)
{
	struct struct_table* table1 = TableCreate("table", sizeof(struct struct_row), "id", 0, sizeof(int));
	struct struct_table* table2 = TableCreate("table", sizeof(struct struct_row), "id", 0, sizeof(int));
	
	int i;
	struct struct_row row;
	struct struct_joined_row* testrow;
	struct struct_relation* relation1;
	struct struct_relation* relation2;
	struct struct_relation* joined;

	for (i = 0; i < 100000; i++)
	{
		row.id = 100000 - (i + 1);
		row.col1 = i % 6;
		row.col2 = i % 4 + 5;
		TableAddRow(table1, &row);
	}

	for (i = 0; i < 100000; i++)
	{
		row.id = i;
		row.col1 = i % 3;
		row.col2 = i % 7 + 2;
		TableAddRow(table2, &row);
	}

	relation1 = TableBuildRelationSorted(table1, 0);
	relation2 = TableBuildRelationSorted(table2, 0);

	joined = JoinRelations(0, relation1, relation2);

	for (i = 0; i < 100000; i++)
	{
		testrow = (struct struct_joined_row*)joined->_data->_data + i;
		assert(testrow->id1, i, 0);
		assert(testrow->id2, i, 0);
	}
}

void TableQueryTest(void)
{
	int i;
	struct struct_row row;
	struct struct_row* testrow;
	struct struct_relation* relation;
	struct struct_query query;
	struct array_list domain;

	struct struct_table* table = TableCreate("table", sizeof(struct struct_row), "id", 0, sizeof(int));

	domain._count = 1;
	domain._data = table;
	domain._element_size = sizeof(struct struct_table);
	domain._index = 1;
	
	for (i = 0; i < 10000; i++)
	{
		row.id = i;
		row.col1 = i % 10;
		row.col2 = i % 4 + 5;
		TableAddRow(table, &row);
	}

	query._count = 2;
	query._ins[0]._opcode = OPCODE_PUSH_INDEX;
	query._ins[0]._r1 = 0;
	query._ins[0]._r2 = 0;

	query._ins[1]._opcode = OPCODE_FILTER;
	query._ins[1]._r1 = 4;
	query._ins[1]._r2 = 4;
	query._ins[1]._r4 = 0;
	
	query._tableCount = 0;

	i = 0;
	memcpy(query._ins[1]._r3, &i, sizeof(int));

	relation = ExecuteQuery(&domain, &query);

	for (i = 0; i < relation->_data->_index; i++)
	{
		testrow = (struct struct_row*)relation->_data->_data + i;
		assert(testrow->col1, 0, 0);
	}
}

void TableLinearScanTest(void)
{
	struct struct_table* table = TableCreate("table", sizeof(struct struct_row), "id", 0, sizeof(int));

	int i;
	struct struct_row row;
	struct struct_row* testrow;
	struct struct_relation* relation;

	for (i = 0; i < 10000; i++)
	{
		row.id = i;
		row.col1 = i % 6;
		row.col2 = i % 4 + 5;
		TableAddRow(table, &row);
	}

	relation = TableBuildRelationSorted(table, 1);

	for (i = 0; i < 10000; i++)
	{
		testrow = (struct struct_row*)relation->_data->_data + i;
		assert(testrow->id, i, 0);
	}
}

void IndexUpdateTest(void)
{
	struct struct_table* table = TableCreate("table", sizeof(struct struct_row), "id", 0, sizeof(int));

	int i;
	struct struct_row row;
	struct struct_row* testrow;
	struct struct_bool_index index;
	struct struct_query query;
	struct struct_relation* relation;
	struct array_list domain;
	int indexUpdated = 0;

	domain._count = 1;
	domain._data = table;
	domain._element_size = sizeof(struct struct_table);
	domain._index = 1;

	index._filtered = 1;
	index._key = TableAddColumn(table, "IsActive", sizeof(int), sizeof(int));
	index._sorted = 0;
	index._tree = BoolTreeCreate();
	index._type = INDEX_TYPE_FILTER_BOOL;
	
	TableAddIndex(table, &index);

	for (i = 0; i < 10000; i++)
	{
		row.id = i;
		row.col1 = i % 2;
		row.col2 = i % 4 + 5;
		TableAddRow(table, &row);
	}

	row.id = 0;
	row.col1 = 1;
	row.col2 = 8;
	TableUpdateRow(table, 0, &row);

	query._count = 2;
	query._ins[0]._opcode = OPCODE_PUSH_INDEX;
	query._ins[0]._r1 = 0;
	query._ins[0]._r2 = 1;

	query._ins[1]._opcode = OPCODE_SORT_FILTER_BOOL;
	query._ins[1]._r1 = 1;
	query._ins[1]._r4 = 0;

	query._tableCount = 0;
	query._cache._index = 0;

	relation = ExecuteQuery(&domain, &query);
		
	for (i = 0; i < relation->_data->_index; ++i)
	{
		testrow = (struct struct_row*) relation->_data->_data + i;
		assert(testrow->col1, 1, 0);

		if (testrow->id == 0 && i == 0)
		{
			indexUpdated = 1;
		}
	}

	assert(indexUpdated, 1, 0);
}

void QueryCompilerTest(void)
{
	struct array_list* domain = CreateArrayList(4, sizeof(struct struct_table));
	struct struct_table* entity;
	struct struct_table* transform;
	struct struct_bool_index index;
	int result;

	char error[64];
	CompileQuery("SELECT WHERE", 0, error, 64);

	assert(strcmp(error, "Invalid SELECT clause"), 0, 0);

	entity = TableCreate("entity", sizeof(struct struct_row), "id", 0, sizeof(int));
	TableAddColumn(entity, "active", sizeof(int), sizeof(int));

	index._filtered = 1;
	index._sorted = 0;
	index._type = INDEX_TYPE_FILTER_BOOL;
	index._tree = BoolTreeCreate();
	index._key = 1;
	TableAddIndex(entity, &index);
	memcpy(domain->_data, entity, sizeof(struct struct_table));
	
	transform = TableCreate("transform", sizeof(struct struct_row), "id", 0, sizeof(int));
	memcpy((struct struct_table*)domain->_data + 1, transform, sizeof(struct struct_table));

	domain->_index = 2;

	// Check can parse query
	result = CompileQuery("SELECT entity ,transform WHERE entity .active=1 GROUP BY id", domain, error, 64) != 0;
	assert(result, 1, 0);

	// Check query is rejected in this case
	result = CompileQuery("SELECT entity ,,  transform WHERE entity .active=1", domain, error, 64) != 0;
	assert(result, 0, 0);
}

void QueryEmitTest(void)
{
	struct array_list* domain = CreateArrayList(4, sizeof(struct struct_table));
	struct struct_table* entity;
	struct struct_table* transform;
	struct struct_bool_index index;
	struct struct_query* result;
	struct struct_relation* relation;
	struct struct_row row;
	struct struct_joined_row* joined;

	int i;

	char error[64];
	entity = TableCreate("entity", sizeof(struct struct_row), "id", 0, sizeof(int));
	TableAddColumn(entity, "active", sizeof(int), sizeof(int));

	index._filtered = 1;
	index._sorted = 0;
	index._type = INDEX_TYPE_FILTER_BOOL;
	index._tree = BoolTreeCreate();
	index._key = 1;
	TableAddIndex(entity, &index);

	transform = TableCreate("transform", sizeof(struct struct_row), "id", 0, sizeof(int));

	for (i = 0; i < 1000; ++i)
	{
		row.id = i;
		row.col1 = i % 2;
		row.col2 = i;
		TableAddRow(entity, &row);

		row.id = i;
		row.col1 = 5;
		row.col2 = 6;
		TableAddRow(transform, &row);
	}

	memcpy(domain->_data, entity, sizeof(struct struct_table));
	memcpy((struct struct_table*)domain->_data + 1, transform, sizeof(struct struct_table));

	domain->_index = 2;

	result = CompileQuery("SELECT entity, transform WHERE entity.active=1 GROUP BY id", domain, error, 64);
	
	relation = ExecuteQuery(domain, result);

	assert(relation->_data->_index, 500, 0);

	for (i = 0; i < relation->_data->_index; ++i)
	{
		joined = (struct struct_joined_row*) relation->_data->_data + i;
		assert(joined->col1, 1, 0);
		assert(joined->id1 == joined->id2, 1, 0);
	}
}

struct row_transform
{
	int id;
	float x;
	float y;
	float z;
};

void EntityCreateTest(void)
{
	ISLANDER_WORLD world = IslanderCreateWorld();
	int transform;
	struct ISLANDER_COMPONENT_SCHEMA schema;
	int id;
	struct row_transform t;

	schema._name = "transform";
	schema._row_size = sizeof(struct row_transform);
	schema._primaryKey = "id";
	schema._priOffset = 0;
	schema._priWidth = sizeof(int);
	
	transform = IslanderAddComponentTable(world, &schema);

	id = IslanderCreateEntity(world);

	t.id = id;
	t.x = 1000;
	t.y = 2000;
	t.z = 5;

	IslanderAddComponent(world, transform, &t);

	t.x = 1200;
	IslanderUpdateComponent(world, transform, t.id, &t);
}

int bulkCreateCount = 0;
void DummmyHandler(void* userData, void* row, int e)
{
	bulkCreateCount++;
}

void EntityBulkCreateTest(void)
{
	struct ISLANDER_COMPONENT_SCHEMA schema;
	int transform;
	int id;
	int i;
	struct row_transform t;
	ISLANDER_WORLD world = IslanderCreateWorld();
	
	schema._name = "transform";
	schema._row_size = sizeof(struct row_transform);
	schema._primaryKey = "id";
	schema._priOffset = 0;
	schema._priWidth = sizeof(int);

	transform = IslanderAddComponentTable(world, &schema);

    bulkCreateCount = 0;

	IslanderAddComponentHandler(world, transform, DummmyHandler, 0);
	IslanderAddComponentHandler(world, transform, DummmyHandler, 0);

	for (i = 0; i < 1000000; i++)
	{
		id = IslanderCreateEntity(world);
		t.id = id;
		t.x = 1000;
		t.y = 2000;
		t.z = 5;

		IslanderAddComponent(world, transform, &t);
	}

	assert(bulkCreateCount, 1000000 * 2, 0);
}

struct struct_texture
{
	float px;
	float py;
	float sx;
	float sy;
	int texture;
	int source;
	int atlasIndex;
};

struct row_sprite
{
	int id;
	int zindex;
	int wrap;
	struct struct_texture textures[4];
};

struct row_render_entity
{
	int id;
	int active;
    int count;
    struct property_entry entries[8];
	struct row_transform transform;
	struct row_sprite sprite;
};

void EntityQueryCompileTest(void)
{
	struct ISLANDER_COMPONENT_SCHEMA schema;
	int transform;
	int sprite;
	int id;
	int i;
	struct row_transform t;
	struct row_sprite s;
	struct row_render_entity* e;
	ISLANDER_QUERY query;
	struct ISLANDER_RESULT_SET result;
	int lastIndex = -1000;
	ISLANDER_WORLD world = IslanderCreateWorld();

	schema._name = "transform";
	schema._row_size = sizeof(struct row_transform);
	schema._primaryKey = "id";
	schema._priOffset = 0;
	schema._priWidth = sizeof(int);

	transform = IslanderAddComponentTable(world, &schema);

	schema._name = "sprite";
	schema._row_size = sizeof(struct row_sprite);
	schema._primaryKey = "id";
	schema._priOffset = 0;
	schema._priWidth = sizeof(int);

	sprite = IslanderAddComponentTable(world, &schema);
	IslanderAddComponentColumn(world, sprite, "index", sizeof(int), sizeof(int));

	query = IslanderCompileQuery(world, "SELECT entity, transform, sprite WHERE entity.active=1 GROUP BY id ORDER BY index");
	assert(query != 0, 1, 0);
	
	for (i = 0; i < 1000; i++)
	{
		id = IslanderCreateEntity(world);
		if (i % 2 == 0)
			IslanderSetEntityActive(world, id, 0);
		t.id = id;
		t.x = 1000;
		t.y = 2000;
		t.z = 5;

		IslanderAddComponent(world, transform, &t);

		s.id = id;
		s.zindex = (i % 7) * 8;

		IslanderAddComponent(world, sprite, &s);
	}

	IslanderExecuteQuery(world, query, &result);

	assert(result._count, 500, 0);
	for (i = 0; i < result._count; ++i)
	{
		e = (struct row_render_entity*)result._data + i;
		assert(e->active, 1, 0);
		assert(e->sprite.zindex >= lastIndex, 1, 0);
		lastIndex = e->sprite.zindex;
	}
}

void HashTableTest(void)
{
	struct struct_hash* hash = HashCreate();
	
	HashInsert(hash, "TESTING", 129);

	assert(HashFind(hash, "TESTING"), 129, 0);
}


void SerializationTest(void)
{
	struct ISLANDER_RESULT_SET result;
	ISLANDER_QUERY query;
	int count;
	struct ISLANDER_COMPONENT_SCHEMA schema;
	int transform;
	int sprite;
	ISLANDER_SERIALIZATION_DATA data;
	ISLANDER_WORLD world[2];
	int i;
	int id;
	struct row_transform t;
	struct row_sprite s;
	struct row_render_entity* e;

	for (i = 0; i < 2; ++i)
	{
		world[i] = IslanderCreateWorld();

		schema._name = "transform";
		schema._row_size = sizeof(struct row_transform);
		schema._primaryKey = "id";
		schema._priOffset = 0;
		schema._priWidth = sizeof(int);

		transform = IslanderAddComponentTable(world[i], &schema);
		IslanderAddComponentColumn(world[i], transform, "index", 0, sizeof(int));
		IslanderAddComponentColumn(world[i], transform, "x", sizeof(int), sizeof(float));
		IslanderAddComponentColumn(world[i], transform, "y", sizeof(int) + sizeof(float), sizeof(float));
		IslanderAddComponentColumn(world[i], transform, "z", sizeof(int) + sizeof(float) * 2, sizeof(float));

		schema._name = "sprite";
		schema._row_size = sizeof(struct row_sprite);
		schema._primaryKey = "id";
		schema._priOffset = 0;
		schema._priWidth = sizeof(int);

		sprite = IslanderAddComponentTable(world[i], &schema);
		IslanderAddComponentColumn(world[i], sprite, "index", 0, sizeof(int));
		IslanderAddComponentColumn(world[i], sprite, "zindex", sizeof(int), sizeof(int));
		IslanderAddComponentColumn(world[i], sprite, "wrap", 2 * sizeof(int), sizeof(int));
		IslanderAddComponentColumn(world[i], sprite, "textures", 3 * sizeof(int), sizeof(struct struct_texture) * 4);
	}

	for (i = 0; i < 1000; i++)
	{
		id = IslanderCreateEntity(world[0]);
		if (i % 2 == 0)
			IslanderSetEntityActive(world[0], id, 0);
		t.id = id;
		t.x = 1000;
		t.y = 2000;
		t.z = 5;

		IslanderAddComponent(world[0], transform, &t);

		s.id = id;
		s.zindex = (i % 7) * 8;

		IslanderAddComponent(world[0], sprite, &s);
	}

	data = IslanderSerialize(world[0], &count);


	IslanderDeserialize(world[1], data, count);

	query = IslanderCompileQuery(world[1], "SELECT entity, transform, sprite WHERE entity.active=1 GROUP BY id");
	IslanderExecuteQuery(world[1], query, &result);

	for (i = 0; i < result._count; i++)
	{
		e = (struct row_render_entity*)result._data + i;
		assert(1, e->active, 0);
		assert(1000, e->transform.x, 0);
		assert(2000, e->transform.y, 0);
		assert(5, e->transform.z, 0);
		assert((e->id % 7) * 8, e->sprite.zindex, 0);
	}
}

void CacheTest(void)
{
	int i;
	struct struct_query_list_cache cache;
	cache._index = 0;

	for (i = 0; i < 500; ++i)
	{
		struct array_list* list = CreateArrayList(1000, 10);
		struct struct_query_cache_entry* entry;
		if (!CacheEntry(&cache, list, &entry))
		{
			DestroyArrayList(list);
		}
	}

	for (i = 0; i < 500; ++i)
	{
		struct struct_query_cache_entry* entry;
		if (SearchCache(&cache, 80, 40, &entry) && (i % 20) == 0)
		{
			entry->_flags = QUERY_CACHE_FLAG_FREE;
		}
	}
}

void GatherStatsTest(void)
{
	int transform;
	int sprite;
	int id;
	int i;
	int count;
	struct row_transform t;
	struct row_sprite s;
	struct row_render_entity* e;
	ISLANDER_QUERY query;
	struct ISLANDER_RESULT_SET result;
	int lastIndex = -1000;
	ISLANDER_WORLD world = IslanderCreateWorld();
	struct ISLANDER_COMPONENT_SCHEMA schema;
	ISLANDER_STATS stats;
	
	schema._name = "transform";
	schema._row_size = sizeof(struct row_transform);
	schema._primaryKey = "id";
	schema._priOffset = 0;
	schema._priWidth = sizeof(int);

	transform = IslanderAddComponentTable(world, &schema);

	schema._name = "sprite";
	schema._row_size = sizeof(struct row_sprite);
	schema._primaryKey = "id";
	schema._priOffset = 0;
	schema._priWidth = sizeof(int);

	sprite = IslanderAddComponentTable(world, &schema);

	for (i = 0; i < 1000; i++)
	{
		id = IslanderCreateEntity(world);
		if (i % 2 == 0)
			IslanderSetEntityActive(world, id, 0);
		t.id = id;
		t.x = 1000;
		t.y = 2000;
		t.z = 5;

		IslanderAddComponent(world, transform, &t);

		s.id = id;
		s.zindex = (i % 7) * 8;

		IslanderAddComponent(world, sprite, &s);
	}

	stats = IslanderGatherStats(world, &count);
    assert(count, 3, 0);

	for (i = 0; i < count; i++)
	{
		struct ISLANDER_INFO_BLOCK block;
		IslanderGetStatBlock(stats, i, &block);
		assert(block._id, i, 0);
		printf("Used: %d Free: %d Max: %d Mem: %d bytes\n",
		       block._used_rows, block._free_rows,
		       block._max_rows, block._mem_usage);
	}
}

struct large_row_transform
{
    int x;
    int y;
    int z;
};

struct large_row_sprite
{
    int sprite;
    int material;
};

struct large_row
{
    int id;
    struct large_row_transform transform;
    struct large_row_sprite sprite;
};

void RowPartialUpdateTest(void)
{
    ISLANDER_WORLD world = IslanderCreateWorld();
    struct ISLANDER_COMPONENT_SCHEMA schema;
    struct large_row t;
    int table_id;
    int id;
    int i;

    schema._name = "large_row";
    schema._row_size = sizeof(struct large_row);
    schema._primaryKey = "id";
    schema._priOffset = 0;
    schema._priWidth = sizeof(int);

    table_id = IslanderAddComponentTable(world, &schema);

    for (i = 0; i < 1000; i++)
    {
        id = IslanderCreateEntity(world);
        if (i % 2 == 0)
            IslanderSetEntityActive(world, id, 0);
        t.id = id;
        t.transform.x = 1000;
        t.transform.y = 2000;
        t.transform.z = 5;
        t.sprite.material = 8;
        t.sprite.sprite = 10;

        IslanderAddComponent(world, table_id, &t);
    }

    for (i = 0; i < 1000; i++)
    {
        IslanderGetComponent(world, table_id, i, &t);

        t.sprite.sprite = 12;

        IslanderUpdateComponentPartial(world, table_id, i, &t.sprite, sizeof(struct large_row) - sizeof(struct large_row_sprite), sizeof(struct large_row_sprite));
    }

    for (i = 0; i < 1000; i++)
    {
        IslanderGetComponent(world, table_id, i, &t);

        assert(t.id, i, 0);
        assert(t.sprite.sprite, 12, 0);
    }
}
