﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;
using System.Collections.Generic;

namespace UtilitiesTest
{
    [TestClass]
    public class SerializerTest
    {
        [Serializable]
        private class TestInstance
        {
            public int a;
            public int b;
            public string k;
            public static int c;

            public TestInstance()
            {
                c++;
            }
        }

        [TestMethod]
        public void SkipConstructor()
        {
            TestInstance i = new TestInstance();
            i.a = 3;
            i.b = 4;
            i.k = "sre";

            var ti = SerializeAndDeserialize<TestInstance>(i);

            // Test the static variable is the same, such the constructor is not called
            Assert.AreEqual(TestInstance.c, 1);
            Assert.IsNotNull(ti);
            Assert.AreEqual(3, ti.a);
            Assert.AreEqual(4, ti.b);
        }

        [Serializable]
        private class EventClass
        {
            public event EventHandler Handler;
            public int k = 0;

            public void Target(object o, EventArgs e)
            {
                k++;
            }

            public void Trigger()
            {
                if (Handler != null)
                {
                    Handler(this, EventArgs.Empty);
                }
            }
        }

        [TestMethod]
        public void Events()
        {
            EventClass c = new EventClass();
            c.Handler += c.Target;

            var e = SerializeAndDeserialize<EventClass>(c);

            e.Trigger();
            Assert.AreEqual(1, e.k);
        }

        [Serializable]
        private class RClass
        {
            public RClass node;
        }

        [TestMethod]
        public void Recursive()
        {
            var r = new RClass();
            r.node = new RClass();
            r.node.node = r;

            var e = SerializeAndDeserialize<RClass>(r);
            Assert.AreEqual(e, e.node.node);
        }

        [Serializable]
        class SerializeMe
        {
            public System.Collections.Generic.IEnumerator<int> current;

            public System.Collections.Generic.IEnumerable<int> DoThing(int a, int b)
            {
                int c = a + b;
                yield return c;

                yield return c + 1;

                yield return c + 2;
            }
        }

        [TestMethod]
        public void YieldReturn()
        {
            var s = new SerializeMe();
            s.current = s.DoThing(2, 4).GetEnumerator();
            s.current.MoveNext();
            s.current.MoveNext();

            var x = SerializeAndDeserialize<SerializeMe>(s);
            Assert.AreEqual(7, x.current.Current);
        }

        private T SerializeAndDeserialize<T>(T o)
        {
            var manager = new Islander.Managed.SerializationManager();
            
            var mem = new System.IO.MemoryStream();

            manager.Serialize(mem, o);
            mem.Position = 0;
            return (T)manager.Deserialize(mem);
        }

        [TestMethod]
        public void List()
        {
            var d = new System.Collections.Generic.List<int>()
            {
                3,
                1,
                4,
                5,
            };


            var x = SerializeAndDeserialize<System.Collections.Generic.List<int>>(d);

            Assert.AreEqual(3, x[0]);
            Assert.AreEqual(1, x[1]);
            Assert.AreEqual(4, x[2]);
            Assert.AreEqual(5, x[3]);
        }

        [TestMethod]
        public void MultiArray()
        {
            //int[,] a2 = new int[,] { { 9, 4, 2 }, { 2, 4, 5 }, };
            int[, ,] a2 = new int[,,] {
                { { 9, 4, 2 }, { 2, 4, 5 }, },
                { { 2, 2, 3}, { 2, 2, 4}}
            };

            var x2 = SerializeAndDeserialize<int[, ,]>(a2);

            Assert.AreEqual(a2.Length, x2.Length);
            Assert.AreEqual(a2[0, 0, 0], x2[0, 0, 0]);
            Assert.AreEqual(a2[1, 1, 1], x2[1, 1, 1]);
        }

        [TestMethod]
        public void Array()
        {
            int[] a1 = new int[] { 1, 2, 5, 32, 5 };

            var x1 = SerializeAndDeserialize<int[]>(a1);

            Assert.AreEqual(a1.Length, x1.Length);
            Assert.AreEqual(a1[0], x1[0]);

            int[][] a3 = new int[][] { new int[] { 0, 4, 6 }, new int[] { 3, 3, 54, 5 } };

            var x3 = SerializeAndDeserialize<int[][]>(a3);

            Assert.AreEqual(a3.Length, x3.Length);
            Assert.AreEqual(a3[0].Length, x3[0].Length);

            System.Collections.Generic.List<string>[] a4 =
                new System.Collections.Generic.List<string>[]
                {
                    new System.Collections.Generic.List<string>()
                    {
                        "foo", "bar",
                    },
                    new System.Collections.Generic.List<string>()
                    {
                        "potato", "today",
                    },                    
                };

            var x4 = SerializeAndDeserialize<System.Collections.Generic.List<string>[]>(a4);

            Assert.AreEqual(a4.Length, x4.Length);
            Assert.AreEqual(a4[0].Count, x4[0].Count);
            Assert.AreEqual(a4[0][0], x4[0][0]);
        }

        [TestMethod]
        public void Dictionary()
        {
            var d = new System.Collections.Generic.Dictionary<string, int>()
            {
                {"a", 0},
                {"b", 2},
                {"aa", 3},
                {"ww", 3},
            };


            var x = SerializeAndDeserialize<System.Collections.Generic.Dictionary<string, int>>(d);

            Assert.AreEqual(d["a"], x["a"]);
            Assert.AreEqual(d["ww"], x["ww"]);
        }

        [Serializable]
        private class GetterSetter
        {
            public int Val { get; set; }
        }

        [TestMethod]
        public void GetSet()
        {
            var g = new GetterSetter();
            g.Val = 100;

            var x = SerializeAndDeserialize<GetterSetter>(g);

            Assert.AreEqual(g.Val, x.Val);
        }

        [TestMethod]
        public void Meta()
        {
            var g = new GetterSetter();
            var p = g.GetType().GetProperty("Val");

            var x = SerializeAndDeserialize<System.Reflection.PropertyInfo>(p);

            Assert.AreEqual(x.Name, p.Name);
        }

        [TestMethod]
        public void Anon()
        {
            int j = 5;
            EventClass e = new EventClass();
            e.Handler += (o, ev) =>
                {
                    j++;
                    Assert.AreEqual(6, j);
                };

            var x = SerializeAndDeserialize<EventClass>(e);
            j = 0;

            // It can get tricky here, when it is deserialized
            // a new j is created.
            x.Trigger();
            Assert.AreEqual(0, j);
        }

        [Serializable]
        private class NullClass
        {
            public int? c = 5;
        }

        [TestMethod]
        public void Null()
        {
            var n = new NullClass();
            n.c = null;

            var x = SerializeAndDeserialize<NullClass>(n);
            Assert.AreEqual(null, n.c);
        }

        [Serializable]
        private class GameState
        {
            public Islander.Managed.IWorld world;
            public List<Islander.Managed.IEntity> entity
                = new List<Islander.Managed.IEntity>();
        }

        [Serializable]
        private class CustomComponent : Islander.Managed.IManagedComponent
        {
            public float opacity;

            public CustomComponent()
                : base("Custom")
            { }
            
            public void Update()
            {
                this.OnPropertyChanged("Opacity");
            }

            public float Opacity
            {
                get
                {
                    return this.opacity;
                }
            }
        }

        [TestMethod]
        public void Entity()
        {
            GameState s = new GameState();
            s.world = Islander.Managed.WorldBuilder.CreateWorld();
            for (int i = 0; i < 100; i++)
            {
                var e = s.world.CreateEntity();
                e.Active = i % 9 == 0;
                var t = e.AddComponent<Islander.Managed.ITransform>();
                t.Position = new Islander.Managed.Vec3<float>(i, 0, 0);
                var o = e.AddComponent<CustomComponent>();
                o.opacity = i;
                o.Update();
                s.entity.Add(e);
            }
            
            var x = SerializeAndDeserialize<GameState>(s);
            for (int i = 0; i < 100; i++)
            {
                var e = x.entity[i];
                Assert.AreEqual(i % 9 == 0, e.Active);
                var t = e.GetComponent<Islander.Managed.ITransform>();
                Assert.AreEqual(i, (int)t.Position.X);
                var o = e.GetComponent<CustomComponent>();
                Assert.AreEqual(i, (int)o.opacity);
            }
        }
    }
}
