#pragma once
#include <math.h>

namespace Islander
{
    namespace Numerics
    {
        enum class Matrix3x3Type
        {
            MATRIX_3X3_IDENTITY,
            MATRIX_3X3_ZERO
        };

        enum class Matrix4x4Type
        {
            MATRIX_4X4_IDENTITY,
            MATRIX_4X4_ZERO
        };

        class Matrix3x3
        {
        public:

            Matrix3x3();
            Matrix3x3(const Matrix3x3Type identity);
            Matrix3x3(float* data);

            static void CreateTranslationMatrix(float x, float y, Matrix3x3* mat);
            static void CreateRotationMatrix(float x, float y, Matrix3x3* mat);
            static void CreateScalingMatrix(float x, float y, Matrix3x3* mat);
            static void CreateScalingMatrix(float x, float y, float z, Matrix3x3* mat);
            static void CreateShearMatrix(float x, float y, Matrix3x3* mat);

            static void Multiply(const Matrix3x3& left, const Matrix3x3& right, Matrix3x3* mat);
            static void Multiply(float* vec, const Matrix3x3& mat, float* outVec);

            float& operator()(int row, int col);
            float operator()(int row, int col) const;

        private:
            float data[9];
        };

        class Matrix4x4
        {
        public:

            Matrix4x4();
            //Matrix4x4(const Matrix4x4& other);
            Matrix4x4(const Matrix4x4Type identity);
            Matrix4x4(double in[16]);
            Matrix4x4(float in[16]);

            //Matrix4x4& operator=(const Matrix4x4& other);

            static void CreateProjectionMatrix(float fovY, float aspect, float nearZ, float farZ, Matrix4x4* mat);
            static void CreateOrthographicMatrix(float width, float height, float nearZ, float farZ, Matrix4x4* mat);
            static void CreateViewMatrix(float posX, float posY, float posZ, float lookX, float lookY, float lookZ, Matrix4x4* mat);
            static void CreateInverseViewMatrix(float posX, float posY, float posZ, float lookX, float lookY, float lookZ, Matrix4x4* mat);
            static void CreateTransformationMatrix(float posX, float posY, float posZ, float dirX, float dirY, float dirZ, float scaleX, float scaleY, float scaleZ, Matrix4x4* mat);
            static void CreateRotationXMatrix(float angle, Matrix4x4* mat);
            static void CreateRotationYMatrix(float angle, Matrix4x4* mat);
            static void CreateRotationZMatrix(float angle, Matrix4x4* mat);
            static void CreateTranslationMatrix(float x, float y, float z, Matrix4x4* mat);
            static void CreateScaleMatrix(float x, float y, float z, Matrix4x4* mat);
            static void Multiply(const Matrix4x4& left, const Matrix4x4& right, Matrix4x4* mat);
            static void TransformVector(const Matrix4x4& mat, float* x, float* y, float* z);
            static void TransformVector(const Matrix4x4& mat, float* x, float* y, float* z, float w);
            static void TransformVector(const Matrix4x4& mat, float* x, float* y, float* z, float* w);
            static void Invert(const Matrix4x4& inmat, Matrix4x4* mat);
            static void Transpose(const Matrix4x4& inmat, Matrix4x4* mat);
            static void ClearScale(Matrix4x4& mat);
            static void ClearTranslation(Matrix4x4& mat);
            static void Decompose(const Matrix4x4& mat, float* px, float* py, float* pz, float* qx, float* qy, float* qz, float* qw);

            float& operator()(int row, int col);
            float operator()(int row, int col) const;

            const float* GetData() const { return data; }

        private:
            float data[16];
        };
    }
}
