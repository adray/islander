#pragma once

namespace Islander
{
    namespace Numerics
    {
        struct Plane
        {
            float x, y, z, d;
        };

        void CreatePlane(Plane* plane, const float* pos, const float* dir1, const float* dir2);
        void CreatePlane(Plane* plane, float* pos, float* c1, float* c2, float* c3, float* c4);
        float PlaneDistance(Plane* plane, float* pos);

        struct Plane2
        {
            float n[3];
            float p[3];
            float d;
        };

        void CreatePlane(Plane2* p, float nx, float ny, float nz, float px, float py, float pz);
    }
}
