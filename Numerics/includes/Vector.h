#pragma once
#include <string>
#include <assert.h>
#include <stdint.h>
#include <algorithm>

namespace Islander
{
	namespace Numerics
	{
		static void Normalize(float inX, float inY, float inZ, float& outX, float& outY, float& outZ)
		{
			float length = sqrtf(inX * inX + inY * inY + inZ * inZ);
			if (length > 0)
			{
				outX = inX / length;
				outY = inY / length;
				outZ = inZ / length;
			}
		}

		static void NormalizeVec3(float* vec)
		{
			Normalize(vec[0], vec[1], vec[2], vec[0], vec[1], vec[2]);
		}

		static void Cross(
			float X1, float Y1, float Z1,
			float X2, float Y2, float Z2,
			float& outX, float& outY, float& outZ)
		{
			float x = Y1 * Z2 - Z1 * Y2;
			float y = Z1 * X2 - X1 * Z2;
			float z = X1 * Y2 - Y1 * X2;

			outX = x;
			outY = y;
			outZ = z;
		}

		static float Dot(
			float X1, float Y1, float Z1,
			float X2, float Y2, float Z2)
		{
			return X1 * X2 + Y1 * Y2 + Z1 * Z2;
		}

		static float DotVec3(
			const float* V1,
			const float* V2)
		{
			return V1[0] * V2[0] + V1[1] * V2[1] + V1[2] * V2[2];
		}

		static float DotVec2(
			float* V1,
			float* V2)
		{
			return V1[0] * V2[0] + V1[1] * V2[1];
		}

		static void Reflect(float* inVelocity, float* normal, float* outVelocity)
		{
			// r = v - 2 * (v . n) * n
			float dot = 2 * DotVec3(inVelocity, normal);
			outVelocity[0] = inVelocity[0] - dot * normal[0];
			outVelocity[1] = inVelocity[1] - dot * normal[1];
			outVelocity[2] = inVelocity[2] - dot * normal[2];
		}

		static void Reflect2D(float* inVelocity, float* normal, float* outVelocity)
		{
			// r = v - 2 * (v . n) * n
			float dot = 2 * DotVec2(inVelocity, normal);
			outVelocity[0] = inVelocity[0] - dot * normal[0];
			outVelocity[1] = inVelocity[1] - dot * normal[1];
		}

		static float LengthSqr(float inX, float inY, float inZ)
		{
			return sqrtf(inX * inX + inY * inY + inZ * inZ);
		}

		static float LengthSqr(const float* inVec)
		{
			return sqrtf(inVec[0] * inVec[0] + inVec[1] * inVec[1] + inVec[2] * inVec[2]);
		}

		template<typename T>
		static void CopyVec4(const T* src, T* dst)
		{
			dst[0] = src[0];
			dst[1] = src[1];
			dst[2] = src[2];
			dst[3] = src[3];
		}

		static void CopyVec3(const float* src, float* dst)
		{
			dst[0] = src[0];
			dst[1] = src[1];
			dst[2] = src[2];
		}

		static void NegateVec3(const float* src, float* dst)
		{
			dst[0] = -src[0];
			dst[1] = -src[1];
			dst[2] = -src[2];
		}

		static void SubVec3(const float* v1, const float* v2, float* dst)
		{
			dst[0] = v1[0] - v2[0];
			dst[1] = v1[1] - v2[1];
			dst[2] = v1[2] - v2[2];
		}

		static void AddVec3(const float* v1, const float* v2, float* dst)
		{
			dst[0] = v1[0] + v2[0];
			dst[1] = v1[1] + v2[1];
			dst[2] = v1[2] + v2[2];
		}

		static void CrossVec3(const float* v1, const float* v2, float* dst)
		{
			Cross(v1[0], v1[1], v1[2], v2[0], v2[1], v2[2], dst[0], dst[1], dst[2]);
		}
	}
}
