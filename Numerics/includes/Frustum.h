#pragma once
#include "Plane.h"

namespace Islander
{
    namespace Numerics
    {
        constexpr int FRUSTUM_PLANE_COUNT = 6;

        struct Frustum
        {
            Plane planes[FRUSTUM_PLANE_COUNT];
        };

        void CalculateFrustumCorners(float fov, float ratio, float nearplane, float farplane, float Corners[24]);
        void CreateFrustum(Frustum* frustum, float* Corners);
        void CreateFrustum(Frustum* frustum, float fov, float ratio, float nearplane, float farplane);
        void CreateFrustumOrthographic(Frustum* frustum, float width, float height, float nearplane, float farplane);
        bool FrustumTest(Frustum* frustum, float* pos, float radius);
    }
}
