#pragma once

namespace Islander
{
    namespace Numerics
    {
        struct AABB
        {
            float min[3];
            float max[3];
        };

        class Matrix4x4;

        void CreateTransformedAABB(float px, float py, float pz, float rx, float ry, float rz, float sx, float sy, float sz, AABB& box);

        void CreateTransformedAABB(const Matrix4x4& matrix, AABB& box);

        bool CollisionBoxTest(const AABB& a, const AABB& b);

        bool PointBoxCollision(const AABB& box, float x, float y, float z);

        void AABBToSphere(const AABB& box, float* centre, float* radius);

        void AABBToCapsule(const AABB& box, float* centre, float* radius, float* height);
    }
}
