#include "Plane.h"
#include "Vector.h"

using namespace Islander;
using namespace Islander::Numerics;

constexpr int X = 0;
constexpr int Y = 1;
constexpr int Z = 2;

void Islander::Numerics::CreatePlane(Plane* plane, const float* pos, const float* dir1, const float* dir2)
{
    float x, y, z;
    Cross(dir1[X], dir1[Y], dir1[Z], dir2[X], dir2[Y], dir2[Z], x, y, z);
    Normalize(x, y, z, x, y, z);

    plane->d = -Dot(x, y, z, pos[X], pos[Y], pos[Z]);
    plane->x = x;
    plane->y = y;
    plane->z = z;
}

void Islander::Numerics::CreatePlane(Plane* plane, float* pos, float* c1, float* c2, float* c3, float* c4)
{
    float dir1[3] = { c1[X] - c2[X], c1[Y] - c2[Y], c1[Z] - c2[Z] };
    float dir2[3] = { c3[X] - c4[X], c3[Y] - c4[Y], c3[Z] - c4[Z] };

    CreatePlane(plane, pos, dir1, dir2);
}

float Islander::Numerics::PlaneDistance(Plane* plane, float* pos)
{
    return Dot(plane->x, plane->y, plane->z, pos[X], pos[Y], pos[Z]) + plane->d;
}

void Islander::Numerics::CreatePlane(Plane2* p, float nx, float ny, float nz,
    float px, float py, float pz)
{
    p->n[0] = nx;
    p->n[1] = ny;
    p->n[2] = nz;
    p->p[0] = px;
    p->p[1] = py;
    p->p[2] = pz;

    p->d = -Dot(px, py, pz, p->n[0], p->n[1], p->n[2]);
}
