#include "AABB.h"
#include "Matrix.h"
#include <algorithm>
#include <assert.h>

using namespace Islander;
using namespace Islander::Numerics;

void Islander::Numerics::CreateTransformedAABB(float px, float py, float pz, float rx, float ry, float rz, float sx, float sy, float sz, AABB& box)
{
    Islander::Numerics::Matrix4x4 matrix(Islander::Numerics::Matrix4x4Type::MATRIX_4X4_IDENTITY);
    Islander::Numerics::Matrix4x4::CreateTransformationMatrix(
        px,
        py,
        pz,
        rx,
        ry,
        rz,
        sx,
        sy,
        sz,
        &matrix);

    Islander::Numerics::Matrix4x4::TransformVector(matrix, &box.max[0], &box.max[1], &box.max[2]);
    Islander::Numerics::Matrix4x4::TransformVector(matrix, &box.min[0], &box.min[1], &box.min[2]);

    AABB box2;
    box2.max[0] = std::max(box.max[0], box.min[0]);
    box2.min[0] = std::min(box.max[0], box.min[0]);
    box2.max[1] = std::max(box.max[1], box.min[1]);
    box2.min[1] = std::min(box.max[1], box.min[1]);
    box2.max[2] = std::max(box.max[2], box.min[2]);
    box2.min[2] = std::min(box.max[2], box.min[2]);

    box = box2;
}

void Islander::Numerics::CreateTransformedAABB(const Islander::Numerics::Matrix4x4& matrix, AABB& box)
{
    Islander::Numerics::Matrix4x4::TransformVector(matrix, &box.max[0], &box.max[1], &box.max[2]);
    Islander::Numerics::Matrix4x4::TransformVector(matrix, &box.min[0], &box.min[1], &box.min[2]);

    AABB box2;
    box2.max[0] = std::max(box.max[0], box.min[0]);
    box2.min[0] = std::min(box.max[0], box.min[0]);
    box2.max[1] = std::max(box.max[1], box.min[1]);
    box2.min[1] = std::min(box.max[1], box.min[1]);
    box2.max[2] = std::max(box.max[2], box.min[2]);
    box2.min[2] = std::min(box.max[2], box.min[2]);

    box = box2;
}

bool Islander::Numerics::CollisionBoxTest(const AABB& a, const AABB& b)
{
    return (a.min[0] <= b.max[0] && a.max[0] >= b.min[0]) &&
        (a.min[1] <= b.max[1] && a.max[1] >= b.min[1]) &&
        (a.min[2] <= b.max[2] && a.max[2] >= b.min[2]);
}

bool Islander::Numerics::PointBoxCollision(const AABB& box, float x, float y, float z)
{
    return x >= box.min[0] && x <= box.max[0] &&
        y >= box.min[1] && y <= box.max[1] &&
        z >= box.min[2] && z <= box.max[2];
}

void Islander::Numerics::AABBToSphere(const AABB& box, float* centre, float* radius)
{
    assert(box.max[0] >= box.min[0]);
    assert(box.max[1] >= box.min[1]);
    assert(box.max[2] >= box.min[2]);

    float radiusX = (box.max[0] - box.min[0]) / 2;
    float radiusY = (box.max[1] - box.min[1]) / 2;
    float radiusZ = (box.max[2] - box.min[2]) / 2;
    float sqr = radiusX * radiusX + radiusY * radiusY + radiusZ * radiusZ;
    *radius = sqrtf(sqr);

    centre[0] = box.min[0] + radiusX;
    centre[1] = box.min[1] + radiusY;
    centre[2] = box.min[2] + radiusZ;
}

void Islander::Numerics::AABBToCapsule(const AABB& box, float* centre, float* radius, float* height)
{
    assert(box.max[0] >= box.min[0]);
    assert(box.max[1] >= box.min[1]);
    assert(box.max[2] >= box.min[2]);

    float radiusX = (box.max[0] - box.min[0]) / 2;
    float radiusY = 0;// (box.max[1] - box.min[1]) / 2;
    float radiusZ = (box.max[2] - box.min[2]) / 2;
    float sqr = radiusX * radiusX + radiusY * radiusY * radiusZ * radiusZ;
    *radius = sqrtf(sqr);

    centre[0] = box.min[0] + radiusX;
    centre[1] = box.min[1] + radiusY;
    centre[2] = box.min[2] + radiusZ;
}
