#include "Matrix.h"
#include "Vector.h"
#include <string.h>
#include <algorithm>

using namespace Islander::Numerics;

Matrix3x3::Matrix3x3()
{
    std::fill_n(data, 9, 0.0f);
}

Matrix3x3::Matrix3x3(const Matrix3x3Type identity)
{
	switch (identity)
	{
    case Matrix3x3Type::MATRIX_3X3_IDENTITY:
		{
			std::fill_n(data, 9, 0.0f);
			data[0] = 1;
			data[4] = 1;
			data[8] = 1;
			break;
        }
    default:
        std::fill_n(data, 9, 0.0f);
        break;
	}
}

Matrix3x3::Matrix3x3(float* ndata)
{
    std::memcpy(data, ndata, sizeof(data));
}

void Matrix3x3::CreateTranslationMatrix(float x, float y, Matrix3x3* mat)
{
	(*mat)(2,0) = x;
	(*mat)(2,1) = y;
}

void Matrix3x3::CreateRotationMatrix(float x, float y, Matrix3x3* mat)
{
	(*mat)(0,0) = y;
	(*mat)(1,0) = x;
	(*mat)(0,1) = -x;
	(*mat)(1,1) = y;
}

void Matrix3x3::CreateScalingMatrix(float x, float y, Matrix3x3* mat)
{
	(*mat)(0,0) = x;
	(*mat)(1,1) = y;
}

void Matrix3x3::CreateScalingMatrix(float x, float y, float z, Matrix3x3* mat)
{
    (*mat)(0, 0) = x;
    (*mat)(1, 1) = y;
    (*mat)(2, 2) = z;
}

void Matrix3x3::CreateShearMatrix(float x, float y, Matrix3x3* mat)
{
    (*mat)(1, 0) = x;
    (*mat)(0, 1) = y;
}

float& Matrix3x3:: operator()(int row, int col)
{
	return data[row + col * 3];
}

float Matrix3x3:: operator()(int row, int col) const
{
	return data[row + col * 3];
}

void Matrix3x3::Multiply(const Matrix3x3& left, const Matrix3x3& right, Matrix3x3* mat)
{
	Matrix3x3 out;

	out(0, 0) = left(0, 0) * right(0, 0) + left(1, 0) * right(0, 1) + left(2, 0) * right(0, 2);
	out(1, 0) = left(0, 0) * right(1, 0) + left(1, 0) * right(1, 1) + left(2, 0) * right(1, 2);
	out(2, 0) = left(0, 0) * right(2, 0) + left(1, 0) * right(2, 1) + left(2, 0) * right(2, 2);
	out(0, 1) = left(0, 1) * right(0, 0) + left(1, 1) * right(0, 1) + left(2, 1) * right(0, 2);
	out(1, 1) = left(0, 1) * right(1, 0) + left(1, 1) * right(1, 1) + left(2, 1) * right(1, 2);
	out(2, 1) = left(0, 1) * right(2, 0) + left(1, 1) * right(2, 1) + left(2, 1) * right(2, 2);
	out(0, 2) = left(0, 2) * right(0, 0) + left(1, 2) * right(0, 1) + left(2, 2) * right(0, 2);
	out(1, 2) = left(0, 2) * right(1, 0) + left(1, 2) * right(1, 1) + left(2, 2) * right(1, 2);
	out(2, 2) = left(0, 2) * right(2, 0) + left(1, 2) * right(2, 1) + left(2, 2) * right(2, 2);

	memcpy(mat->data, out.data, sizeof(float) * 9);
}

void Matrix3x3::Multiply(float* vec, const Matrix3x3& mat, float* outVec)
{
    float x = vec[0];
    float y = vec[1];
    float z = vec[2];

    float _x = mat(0, 0) * x + mat(1, 0) * y + mat(2, 0) * z;
    float _y = mat(0, 1) * x + mat(1, 1) * y + mat(2, 1) * z;
    float _z = mat(0, 2) * x + mat(1, 2) * y + mat(2, 2) * z;

    outVec[0] = _x;
    outVec[1] = _y;
    outVec[2] = _z;
}

void Matrix4x4::CreateProjectionMatrix(float fovY, float aspect, float nearZ, float farZ, Matrix4x4* mat)
{
    float h, w, Q;

    h = (float)cosf(fovY*0.5f) / (float)sinf(fovY*0.5f);
    w = h / aspect;
    Q = farZ / (farZ - nearZ);

    (*mat)(0, 0) = w;
    (*mat)(1, 1) = h;
    (*mat)(2, 2) = Q;
    (*mat)(3, 2) = -Q*nearZ;
    (*mat)(2, 3) = 1;
}

void Matrix4x4::CreateOrthographicMatrix(float width, float height, float nearZ, float farZ, Matrix4x4* mat)
{
    float Q = 1 / (farZ - nearZ);

    (*mat)(0, 0) = 2/width;
    (*mat)(1, 1) = 2/height;
    (*mat)(2, 2) = Q;
    (*mat)(3, 2) = -Q * nearZ;
    (*mat)(3, 3) = 1;
}

void Matrix4x4::CreateViewMatrix(float posX, float posY, float posZ, float lookX, float lookY, float lookZ, Matrix4x4* mat)
{
    //zaxis = normal(At - Eye)
    //xaxis = normal(cross(Up, zaxis))
    //yaxis = cross(zaxis, xaxis)

    //xaxis.x           yaxis.x           zaxis.x          0
    //xaxis.y           yaxis.y           zaxis.y          0
    //xaxis.z           yaxis.z           zaxis.z          0
    //- dot(xaxis, eye) - dot(yaxis, eye) - dot(zaxis, eye)  1

    float zAxisX = lookX - posX;
    float zAxisY = lookY - posY;
    float zAxisZ = lookZ - posZ;

    Normalize(zAxisX, zAxisY, zAxisZ, zAxisX, zAxisY, zAxisZ);

    if (zAxisX == 0 && abs(abs(zAxisY) - 1) < 0.001f && zAxisZ == 0)
    {
        float xAxisX;
        float xAxisY;
        float xAxisZ;

        Cross(0, 0, 1, zAxisX, zAxisY, zAxisZ, xAxisX, xAxisY, xAxisZ);

        Normalize(xAxisX, xAxisY, xAxisZ, xAxisX, xAxisY, xAxisZ);

        float yAxisX;
        float yAxisY;
        float yAxisZ;

        Cross(zAxisX, zAxisY, zAxisZ, xAxisX, xAxisY, xAxisZ, yAxisX, yAxisY, yAxisZ);

        (*mat)(0, 0) = xAxisX;
        (*mat)(0, 1) = yAxisX;
        (*mat)(0, 2) = zAxisX;

        (*mat)(1, 0) = xAxisY;
        (*mat)(1, 1) = yAxisY;
        (*mat)(1, 2) = zAxisY;

        (*mat)(2, 0) = xAxisZ;
        (*mat)(2, 1) = yAxisZ;
        (*mat)(2, 2) = zAxisZ;

        (*mat)(3, 0) = -Dot(xAxisX, xAxisY, xAxisZ, posX, posY, posZ);
        (*mat)(3, 1) = -Dot(yAxisX, yAxisY, yAxisZ, posX, posY, posZ);
        (*mat)(3, 2) = -Dot(zAxisX, zAxisY, zAxisZ, posX, posY, posZ);

        (*mat)(3, 3) = 1;
    }
    else
    {
        float xAxisX;
        float xAxisY;
        float xAxisZ;

        Cross(0, 1, 0, zAxisX, zAxisY, zAxisZ, xAxisX, xAxisY, xAxisZ);

        Normalize(xAxisX, xAxisY, xAxisZ, xAxisX, xAxisY, xAxisZ);

        float yAxisX;
        float yAxisY;
        float yAxisZ;

        Cross(zAxisX, zAxisY, zAxisZ, xAxisX, xAxisY, xAxisZ, yAxisX, yAxisY, yAxisZ);

        (*mat)(0, 0) = xAxisX;
        (*mat)(0, 1) = yAxisX;
        (*mat)(0, 2) = zAxisX;

        (*mat)(1, 0) = xAxisY;
        (*mat)(1, 1) = yAxisY;
        (*mat)(1, 2) = zAxisY;

        (*mat)(2, 0) = xAxisZ;
        (*mat)(2, 1) = yAxisZ;
        (*mat)(2, 2) = zAxisZ;

        (*mat)(3, 0) = -Dot(xAxisX, xAxisY, xAxisZ, posX, posY, posZ);
        (*mat)(3, 1) = -Dot(yAxisX, yAxisY, yAxisZ, posX, posY, posZ);
        (*mat)(3, 2) = -Dot(zAxisX, zAxisY, zAxisZ, posX, posY, posZ);

        (*mat)(3, 3) = 1;
    }
}

void Matrix4x4::CreateInverseViewMatrix(float posX, float posY, float posZ, float lookX, float lookY, float lookZ, Matrix4x4* mat)
{
    Matrix4x4 view(Matrix4x4Type::MATRIX_4X4_ZERO);
    CreateViewMatrix(posX, posY, posZ, lookX, lookY, lookZ, &view);

    *mat = Matrix4x4(Matrix4x4Type::MATRIX_4X4_ZERO);
    (*mat)(1, 0) = view(0, 1);
    (*mat)(2, 0) = view(0, 2);
    (*mat)(2, 1) = view(1, 2);
    (*mat)(1, 1) = view(1, 1);
    (*mat)(3, 0) = posX;
    (*mat)(3, 1) = posY;
    (*mat)(3, 2) = posZ;
    (*mat)(3, 3) = 1;
    (*mat)(0, 2) = view(2, 0); //1;
    (*mat)(2, 2) = view(2, 2);
    (*mat)(0, 1) = view(1, 0);
    (*mat)(0, 0) = view(0, 0);
    (*mat)(1, 2) = view(2, 1);
}

void Matrix4x4::CreateTransformationMatrix(float posX, float posY, float posZ, float dirX, float dirY, float dirZ, float scaleX, float scaleY, float scaleZ, Matrix4x4* mat)
{
    Matrix4x4 x(Matrix4x4Type::MATRIX_4X4_IDENTITY);
    Matrix4x4 y(Matrix4x4Type::MATRIX_4X4_IDENTITY);
    Matrix4x4 z(Matrix4x4Type::MATRIX_4X4_IDENTITY);
    Matrix4x4 t(Matrix4x4Type::MATRIX_4X4_IDENTITY);

    CreateRotationXMatrix(dirX, &x);
    CreateRotationYMatrix(dirY, &y);
    CreateRotationZMatrix(dirZ, &z);
    CreateTranslationMatrix(posX, posY, posZ, &t);
    CreateScaleMatrix(scaleX, scaleY, scaleZ, mat);

    Multiply(*mat, x, mat);
    Multiply(*mat, y, mat);
    Multiply(*mat, z, mat);
    Multiply(*mat, t, mat);
}

void Matrix4x4:: CreateScaleMatrix(float x, float y, float z, Matrix4x4* mat)
{
    (*mat)(0, 0) = x;
    (*mat)(1, 1) = y;
    (*mat)(2, 2) = z;
    (*mat)(3, 3) = 1;
}

void Matrix4x4::CreateRotationXMatrix(float angle, Matrix4x4* mat)
{
    //(*mat)(1, 1) = (float)cos(angle);
    //(*mat)(1, 2) = (float)-sin(angle);
    //(*mat)(2, 1) = (float)sin(angle);
    //(*mat)(2, 2) = (float)cos(angle);

    (*mat)(1, 1) = (float)cos(angle);
    (*mat)(2, 1) = (float)-sin(angle);
    (*mat)(1, 2) = (float)sin(angle);
    (*mat)(2, 2) = (float)cos(angle);
}

void Matrix4x4::CreateRotationYMatrix(float angle, Matrix4x4* mat)
{
    //(*mat)(0, 0) = (float)cos(angle);
    //(*mat)(0, 2) = (float)sin(angle);
    //(*mat)(2, 0) = (float)-sin(angle);
    //(*mat)(2, 2) = (float)cos(angle);

    (*mat)(0, 0) = (float)cos(angle);
    (*mat)(2, 0) = (float)sin(angle);
    (*mat)(0, 2) = (float)-sin(angle);
    (*mat)(2, 2) = (float)cos(angle);
}

void Matrix4x4::CreateRotationZMatrix(float angle, Matrix4x4* mat)
{
    //(*mat)(0, 0) = (float)cos(angle);
    //(*mat)(0, 1) = (float)-sin(angle);
    //(*mat)(1, 0) = (float)sin(angle);
    //(*mat)(1, 1) = (float)cos(angle);

    (*mat)(0, 0) = (float)cos(angle);
    (*mat)(1, 0) = (float)-sin(angle);
    (*mat)(0, 1) = (float)sin(angle);
    (*mat)(1, 1) = (float)cos(angle);
}

void Matrix4x4::CreateTranslationMatrix(float x, float y, float z, Matrix4x4* mat)
{
    (*mat)(3, 0) = x;
    (*mat)(3, 1) = y;
    (*mat)(3, 2) = z;
}

void Matrix4x4::Multiply(const Matrix4x4& left, const Matrix4x4& right, Matrix4x4* mat)
{
    Matrix4x4 out;

    (out)(0, 0) = left(0, 0) * right(0, 0) + left(0, 1) * right(1, 0) + left(0, 2) * right(2, 0) + left(0, 3) * right(3, 0);
    (out)(0, 1) = left(0, 0) * right(0, 1) + left(0, 1) * right(1, 1) + left(0, 2) * right(2, 1) + left(0, 3) * right(3, 1);
    (out)(0, 2) = left(0, 0) * right(0, 2) + left(0, 1) * right(1, 2) + left(0, 2) * right(2, 2) + left(0, 3) * right(3, 2);
    (out)(0, 3) = left(0, 0) * right(0, 3) + left(0, 1) * right(1, 3) + left(0, 2) * right(2, 3) + left(0, 3) * right(3, 3);

    (out)(1, 0) = left(1, 0) * right(0, 0) + left(1, 1) * right(1, 0) + left(1, 2) * right(2, 0) + left(1, 3) * right(3, 0);
    (out)(1, 1) = left(1, 0) * right(0, 1) + left(1, 1) * right(1, 1) + left(1, 2) * right(2, 1) + left(1, 3) * right(3, 1);
    (out)(1, 2) = left(1, 0) * right(0, 2) + left(1, 1) * right(1, 2) + left(1, 2) * right(2, 2) + left(1, 3) * right(3, 2);
    (out)(1, 3) = left(1, 0) * right(0, 3) + left(1, 1) * right(1, 3) + left(1, 2) * right(2, 3) + left(1, 3) * right(3, 3);

    (out)(2, 0) = left(2, 0) * right(0, 0) + left(2, 1) * right(1, 0) + left(2, 2) * right(2, 0) + left(2, 3) * right(3, 0);
    (out)(2, 1) = left(2, 0) * right(0, 1) + left(2, 1) * right(1, 1) + left(2, 2) * right(2, 1) + left(2, 3) * right(3, 1);
    (out)(2, 2) = left(2, 0) * right(0, 2) + left(2, 1) * right(1, 2) + left(2, 2) * right(2, 2) + left(2, 3) * right(3, 2);
    (out)(2, 3) = left(2, 0) * right(0, 3) + left(2, 1) * right(1, 3) + left(2, 2) * right(2, 3) + left(2, 3) * right(3, 3);

    (out)(3, 0) = left(3, 0) * right(0, 0) + left(3, 1) * right(1, 0) + left(3, 2) * right(2, 0) + left(3, 3) * right(3, 0);
    (out)(3, 1) = left(3, 0) * right(0, 1) + left(3, 1) * right(1, 1) + left(3, 2) * right(2, 1) + left(3, 3) * right(3, 1);
    (out)(3, 2) = left(3, 0) * right(0, 2) + left(3, 1) * right(1, 2) + left(3, 2) * right(2, 2) + left(3, 3) * right(3, 2);
    (out)(3, 3) = left(3, 0) * right(0, 3) + left(3, 1) * right(1, 3) + left(3, 2) * right(2, 3) + left(3, 3) * right(3, 3);

    memcpy(mat->data, out.data, sizeof(float) * 16);
}

void Matrix4x4::TransformVector(const Matrix4x4& mat, float* x, float* y, float* z)
{
    float _x = mat(0, 0) * *x + mat(1, 0) * *y + mat(2, 0) * *z + mat(3, 0);
    float _y = mat(0, 1) * *x + mat(1, 1) * *y + mat(2, 1) * *z + mat(3, 1);
    float _z = mat(0, 2) * *x + mat(1, 2) * *y + mat(2, 2) * *z + mat(3, 2);

    *x = _x;
    *y = _y;
    *z = _z;
}

void Matrix4x4::TransformVector(const Matrix4x4& mat, float* x, float* y, float* z, float w)
{
    float _x = mat(0, 0) * *x + mat(1, 0) * *y + mat(2, 0) * *z + mat(3, 0) * w;
    float _y = mat(0, 1) * *x + mat(1, 1) * *y + mat(2, 1) * *z + mat(3, 1) * w;
    float _z = mat(0, 2) * *x + mat(1, 2) * *y + mat(2, 2) * *z + mat(3, 2) * w;

    *x = _x;
    *y = _y;
    *z = _z;
}

void Matrix4x4::TransformVector(const Matrix4x4& mat, float* x, float* y, float* z, float* w)
{
    float _x = mat(0, 0) * *x + mat(1, 0) * *y + mat(2, 0) * *z + mat(3, 0) * *w;
    float _y = mat(0, 1) * *x + mat(1, 1) * *y + mat(2, 1) * *z + mat(3, 1) * *w;
    float _z = mat(0, 2) * *x + mat(1, 2) * *y + mat(2, 2) * *z + mat(3, 2) * *w;
    float _w = mat(0, 3) * *x + mat(1, 3) * *y + mat(2, 3) * *z + mat(3, 3) * *w;

    *x = _x;
    *y = _y;
    *z = _z;
    *w = _w;
}

void Matrix4x4::Invert(const Matrix4x4& inmat, Matrix4x4* mat)
{
    if (inmat(0, 0) == 0)
    {
        return;
    }

    // Use LU decomposition

    // AX = I

    // LU = A
    // Lyi = Ii
    // Uxi = yi

    Matrix4x4 l(Matrix4x4Type::MATRIX_4X4_IDENTITY);
    Matrix4x4 u(Matrix4x4Type::MATRIX_4X4_ZERO);

    u(0, 0) = inmat(0, 0); //g
    u(0, 1) = inmat(0, 1); //h
    u(0, 2) = inmat(0, 2); //i
    u(0, 3) = inmat(0, 3); //j

    l(1, 0) = inmat(1, 0) / u(0, 0); //a
    l(2, 0) = inmat(2, 0) / u(0, 0); //b
    l(3, 0) = inmat(3, 0) / u(0, 0); //d

    u(1, 1) = inmat(1, 1) - l(1, 0) * u(0, 1); //k

    //Assert(u(1, 1) != 0);

    l(2, 1) = (inmat(2, 1) - l(2, 0) * u(0, 1)) / u(1, 1); //c
    l(3, 1) = (inmat(3, 1) - l(3, 0) * u(0, 1)) / u(1, 1); //e

    u(1, 2) = inmat(1, 2) - l(1, 0) * u(0, 2); //l
    u(1, 3) = inmat(1, 3) - l(1, 0) * u(0, 3); //m

    u(2, 2) = inmat(2, 2) - l(2, 0) * u(0, 2) - l(2, 1) * u(1, 2); //o
    u(2, 3) = inmat(2, 3) - l(2, 0) * u(0, 3) - l(2, 1) * u(1, 3); //p

    //Assert(matrix(2, 2) != 0);

    l(3, 2) = (inmat(3, 2) - l(3, 0) * u(0, 2) - l(3, 1) * u(1, 2)) / u(2, 2); //f

    u(3, 3) = inmat(3, 3) - l(3, 0) * u(0, 3) - l(3, 1) * u(1, 3) - l(3, 2) * u(2, 3); //q -dj -em - fp

    // Find inverse by back propagation

    Matrix4x4 y(Matrix4x4Type::MATRIX_4X4_ZERO);
    Matrix4x4 I(Matrix4x4Type::MATRIX_4X4_IDENTITY);

    // Solve Ly = b

    for (int i = 0; i < 4; i++)
    {
        y.data[i] = I.data[i * 4]; //xi
        y.data[i + 4] = I.data[i * 4 + 1] - l(1, 0) * y.data[i]; //yi
        y.data[i + 8] = I.data[i * 4 + 2] - l(2, 0) * y.data[i] - l(2, 1) * y.data[i + 4];    //zi
        y.data[i + 12] = I.data[i * 4 + 3] - l(3, 0) * y.data[i] - l(3, 1) * y.data[i + 4] - l(3, 2) * y.data[i + 8]; //wi
    }

//#if DEBUG
//
//        Matrix4x4 res = l * y;
//
//        for (int i = 0; i < 16; i++)
//        {
//            Assert(Math.Abs(res.data[i] - I.data[i]) < 0.01f, "Ly!=b\n" + res.ToString());
//        }
//
//#endif

    // Solve Ux = y

    for (int i = 0; i < 4; i++)
    {
        (*mat).data[i * 4 + 3] = y.data[i + 12] / u(3, 3);
        (*mat).data[i * 4 + 2] = (y.data[i + 8] - u(2, 3) * (*mat).data[i * 4 + 3]) / u(2, 2);
        (*mat).data[i * 4 + 1] = (y.data[i + 4] - u(1, 2) * (*mat).data[i * 4 + 2] - u(1, 3) * (*mat).data[i * 4 + 3]) / u(1, 1);
        (*mat).data[i * 4] = (y.data[i] - u(0, 1) * (*mat).data[i * 4 + 1]
            - u(0, 2) * (*mat).data[i + 8] - u(0, 3) * (*mat).data[i * 4 + 3]) / u(0, 0);
    }

//#if DEBUG
//
//        y.Transpose();
//        res = u * inverse;
//
//        for (int i = 0; i < 16; i++)
//        {
//            Assert(Math.Abs(res.data[i] - y.data[i]) < 0.01f, "Ux!=y\n got:" +
//                res.ToString() + "\n wanted:" + y.ToString());
//        }
//
//#endif
}

void Matrix4x4::Transpose(const Matrix4x4& inmat, Matrix4x4* mat)
{
    Matrix4x4 ref(inmat);

    for (int i = 0; i < 16; i++)
    {
        (*mat)(i / 4, i % 4) = ref(i % 4, i / 4);
    }
}

void Matrix4x4::ClearScale(Matrix4x4& mat)
{
    //mat(0, 0) = 1;
    //mat(1, 1) = 1;
    //mat(2, 2) = 1;

    float sx = (float)sqrtf(mat(0, 0) * mat(0, 0) + mat(0, 1) * mat(0, 1) + mat(0, 2) * mat(0, 2));
    float sy = (float)sqrtf(mat(1, 0) * mat(1, 0) + mat(1, 1) * mat(1, 1) + mat(1, 2) * mat(1, 2));
    float sz = (float)sqrtf(mat(2, 0) * mat(2, 0) + mat(2, 1) * mat(2, 1) + mat(2, 2) * mat(2, 2));

    Matrix4x4 copy(Matrix4x4Type::MATRIX_4X4_ZERO);
    Matrix4x4::CreateScaleMatrix(1 / sx, 1 / sy, 1 / sz, &copy);
    Matrix4x4::Multiply(mat, copy, &mat);
}

void Matrix4x4::ClearTranslation(Matrix4x4& mat)
{
    mat(3, 0) = 0;
    mat(3, 1) = 0;
    mat(3, 2) = 0;
}

void Matrix4x4::Decompose(const Matrix4x4& mat, float* px, float* py, float* pz, float* qx, float* qy, float* qz, float* qw)
{
    *px = mat(3, 0);
    *py = mat(3, 1);
    *pz = mat(3, 2);

    float sx = (float)sqrtf(mat(0, 0) * mat(0, 0) + mat(0, 1) * mat(0, 1) + mat(0, 2) * mat(0, 2));
    float sy = (float)sqrtf(mat(1, 0) * mat(1, 0) + mat(1, 1) * mat(1, 1) + mat(1, 2) * mat(1, 2));
    float sz = (float)sqrtf(mat(2, 0) * mat(2, 0) + mat(2, 1) * mat(2, 1) + mat(2, 2) * mat(2, 2));

    Matrix4x4 copy(Matrix4x4Type::MATRIX_4X4_ZERO);
    Matrix4x4::CreateScaleMatrix(1/sx, 1/sy, 1/sz, &copy);
    Matrix4x4::Multiply(mat, copy, &copy);
    Matrix4x4::Transpose(copy, &copy);

    float t = copy(0,0) + copy(1,1) + copy(2,2) + 1.0f;
    if (t > 1e-4f) {
        float s = 0.5f / sqrtf(t);
        *qw = 0.25f / s;
        *qx = (copy(2,1) - copy(1,2)) * s;
        *qy = (copy(0,2) - copy(2,0)) * s;
        *qz = (copy(1,0) - copy(0,1)) * s;
    }
    else if (copy(0,0) > copy(1,1) && copy(0,0) > copy(2,2)) {
        float s = sqrtf(1.0f + copy(0,0) - copy(1,1) - copy(2,2)) * 2.0f; // S=4*qx 
        *qx = 0.25f * s;
        *qy = (copy(0,1) + copy(1,0)) / s;
        *qz = (copy(0,2) + copy(2,0)) / s;
        *qw = (copy(2,1) - copy(1,2)) / s;
    }
    else if (copy(1,1) > copy(2,2)) {
        float s = sqrtf(1.0f + copy(1,1) - copy(0,0) - copy(2,2)) * 2.0f; // S=4*qy
        *qx = (copy(0,1) + copy(1,0)) / s;
        *qy = 0.25f * s;
        *qz = (copy(1,2) + copy(2,1)) / s;
        *qw = (copy(0,2) - copy(2,0)) / s;
    }
    else {
        float s = sqrtf(1.0f + copy(2,2) - copy(0,0) - copy(1,1)) * 2.0f; // S=4*qz
        *qx = (copy(0,2) + copy(2,0)) / s;
        *qy = (copy(1,2) + copy(2,1)) / s;
        *qz = 0.25f * s;
        *qw = (copy(1,0) - copy(0,1)) / s;
    }
}

float& Matrix4x4:: operator()(int row, int col)
{
    return data[row + col * 4];
}

float Matrix4x4:: operator()(int row, int col) const
{
    return data[row + col * 4];
}

Matrix4x4::Matrix4x4()
{
    std::fill_n(data, 16, 0.0f);
}

Matrix4x4::Matrix4x4(const Matrix4x4Type identity)
{
    switch (identity)
    {
        case Matrix4x4Type::MATRIX_4X4_IDENTITY:
        {
            std::fill_n(data, 16, 0.0f);
            data[0] = 1;
            data[5] = 1;
            data[10] = 1;
            data[15] = 1;
            break;
        }
        case Matrix4x4Type::MATRIX_4X4_ZERO:
        default:
        {
            std::fill_n(data, 16, 0.0f);
            break;
        }
    }
}

//Matrix4x4::Matrix4x4(const Matrix4x4& other)
//{
//    memcpy(data, other.data, sizeof(float) * 16);
//}

Matrix4x4::Matrix4x4(double in[16])
{
    for (int i = 0; i < 16; i++)
    {
        data[i] = (float)in[i];
    }
}

Matrix4x4::Matrix4x4(float in[16])
{
    for (int i = 0; i < 16; i++)
    {
        data[i] = (float)in[i];
    }
}

//Matrix4x4& Matrix4x4::operator=(const Matrix4x4& other)
//{
//    memcpy(data, other.data, sizeof(float) * 16);
//    return *this;
//}
