#include "Frustum.h"
#include "Vector.h"

using namespace Islander;
using namespace Islander::Numerics;

constexpr int X = 0;
constexpr int Y = 1;
constexpr int Z = 2;
constexpr int W = 3;

bool Islander::Numerics::FrustumTest(Frustum* frustum, float* pos, float radius)
{
    for (int i = 0; i < FRUSTUM_PLANE_COUNT; i++)
    {
        if (PlaneDistance(&frustum->planes[i], pos) + radius < 0)
        {
            return false;
        }
    }

    return true;
}

void SetVec(float* vec, float x, float y, float z)
{
    vec[0] = x;
    vec[1] = y;
    vec[2] = z;
}

void Islander::Numerics::CreateFrustum(Frustum* frustum, float* Corners)
{
    CreatePlane(&frustum->planes[0], &Corners[0], &Corners[2 * 3], &Corners[0], &Corners[1 * 3], &Corners[0]); // far
    CreatePlane(&frustum->planes[1], &Corners[4 * 3], &Corners[5 * 3], &Corners[4 * 3], &Corners[6 * 3], &Corners[4 * 3]); // near

    CreatePlane(&frustum->planes[2], &Corners[0], &Corners[1 * 3], &Corners[0], &Corners[4 * 3], &Corners[0]); // top
    CreatePlane(&frustum->planes[3], &Corners[3 * 3], &Corners[2 * 3], &Corners[3 * 3], &Corners[7 * 3], &Corners[3 * 3]); // bottom

    CreatePlane(&frustum->planes[4], &Corners[1 * 3], &Corners[3 * 3], &Corners[1 * 3], &Corners[5 * 3], &Corners[1 * 3]); // left
    CreatePlane(&frustum->planes[5], &Corners[0], &Corners[4 * 3], &Corners[0], &Corners[2 * 3], &Corners[0]); // right
}

void Islander::Numerics::CalculateFrustumCorners(float fov, float ratio, float nearplane, float farplane, float Corners[24])
{
    //camera view direction
    float dir[] = { 0, 0, 1 };

    float halfnearheight = (float)tanf(fov / 2) * nearplane;
    float halfnearwidth = halfnearheight * ratio;

    float halffarheight = (float)tanf(fov / 2) * farplane;
    float halffarwidth = halffarheight * ratio;

    float centre[3 * 2] =
    {
        dir[X] * nearplane, dir[Y] * nearplane, dir[Z] * nearplane,
        dir[X] * farplane, dir[Y] * farplane, dir[Z] * farplane
    };

    SetVec(Corners, centre[3] + halffarwidth, centre[4] + halffarheight, centre[5]);
    SetVec(&Corners[3], centre[3] - halffarwidth, centre[4] + halffarheight, centre[5]);
    SetVec(&Corners[6], centre[3] + halffarwidth, centre[4] - halffarheight, centre[5]);
    SetVec(&Corners[9], centre[3] - halffarwidth, centre[4] - halffarheight, centre[5]);

    SetVec(&Corners[12], centre[0] + halfnearwidth, centre[1] + halfnearheight, centre[2]);
    SetVec(&Corners[15], centre[0] - halfnearwidth, centre[1] + halfnearheight, centre[2]);
    SetVec(&Corners[18], centre[0] + halfnearwidth, centre[1] - halfnearheight, centre[2]);
    SetVec(&Corners[21], centre[0] - halfnearwidth, centre[1] - halfnearheight, centre[2]);
}

void Islander::Numerics::CreateFrustum(Frustum* frustum, float fov, float ratio, float nearplane, float farplane)
{
    //camera view direction
    float dir[] = { 0, 0, 1 };

    float halfnearheight = (float)tanf(fov / 2) * nearplane;
    float halfnearwidth = halfnearheight * ratio;

    float halffarheight = (float)tanf(fov / 2) * farplane;
    float halffarwidth = halffarheight * ratio;

    float Corners[8 * 3];

    float centre[3 * 2] = 
    {
        dir[X] * nearplane, dir[Y] * nearplane, dir[Z] * nearplane,
        dir[X] * farplane, dir[Y] * farplane, dir[Z] * farplane
    };

    SetVec(Corners, centre[3] + halffarwidth, centre[4] + halffarheight, centre[5]);
    SetVec(&Corners[3], centre[3] - halffarwidth, centre[4] + halffarheight, centre[5]);
    SetVec(&Corners[6], centre[3] + halffarwidth, centre[4] - halffarheight, centre[5]);
    SetVec(&Corners[9], centre[3] - halffarwidth, centre[4] - halffarheight, centre[5]);
    
    SetVec(&Corners[12], centre[0] + halfnearwidth, centre[1] + halfnearheight, centre[2]);
    SetVec(&Corners[15], centre[0] - halfnearwidth, centre[1] + halfnearheight, centre[2]);
    SetVec(&Corners[18], centre[0] + halfnearwidth, centre[1] - halfnearheight, centre[2]);
    SetVec(&Corners[21], centre[0] - halfnearwidth, centre[1] - halfnearheight, centre[2]);

    CreatePlane(&frustum->planes[0], &Corners[0], &Corners[2*3], &Corners[0], &Corners[1*3], &Corners[0]); // far
    CreatePlane(&frustum->planes[1], &Corners[4 * 3], &Corners[5 * 3], &Corners[4 * 3], &Corners[6 * 3], &Corners[4 * 3]); // near
    
    CreatePlane(&frustum->planes[2], &Corners[0], &Corners[1 * 3], &Corners[0], &Corners[4 * 3], &Corners[0]); // top
    CreatePlane(&frustum->planes[3], &Corners[3 * 3], &Corners[2 * 3], &Corners[3 * 3], &Corners[7 * 3], &Corners[3 * 3]); // bottom

    CreatePlane(&frustum->planes[4], &Corners[1 * 3], &Corners[3 * 3], &Corners[1 * 3], &Corners[5 * 3], &Corners[1 * 3]); // left
    CreatePlane(&frustum->planes[5], &Corners[0], &Corners[4 * 3], &Corners[0], &Corners[2 * 3], &Corners[0]); // right
}

void Islander::Numerics::CreateFrustumOrthographic(Frustum* frustum, float width, float height, float nearplane, float farplane)
{
    //camera view direction
    float dir[] = { 0, 0, 1 };

    float halfnearheight = height / 2;
    float halffarheight = height / 2;
    float halfnearwidth = width / 2;
    float halffarwidth = width / 2;

    float Corners[8 * 3];

    float centre[3 * 2] =
    {
        dir[X] * nearplane, dir[Y] * nearplane, dir[Z] * nearplane,
        dir[X] * farplane, dir[Y] * farplane, dir[Z] * farplane
    };

    SetVec(Corners, centre[3] + halffarwidth, centre[4] + halffarheight, centre[5]);
    SetVec(&Corners[3], centre[3] - halffarwidth, centre[4] + halffarheight, centre[5]);
    SetVec(&Corners[6], centre[3] + halffarwidth, centre[4] - halffarheight, centre[5]);
    SetVec(&Corners[9], centre[3] - halffarwidth, centre[4] - halffarheight, centre[5]);

    SetVec(&Corners[12], centre[0] + halfnearwidth, centre[1] + halfnearheight, centre[2]);
    SetVec(&Corners[15], centre[0] - halfnearwidth, centre[1] + halfnearheight, centre[2]);
    SetVec(&Corners[18], centre[0] + halfnearwidth, centre[1] - halfnearheight, centre[2]);
    SetVec(&Corners[21], centre[0] - halfnearwidth, centre[1] - halfnearheight, centre[2]);

    CreatePlane(&frustum->planes[0], &Corners[0], &Corners[2 * 3], &Corners[0], &Corners[1 * 3], &Corners[0]); // far
    CreatePlane(&frustum->planes[1], &Corners[4 * 3], &Corners[5 * 3], &Corners[4 * 3], &Corners[6 * 3], &Corners[4 * 3]); // near

    CreatePlane(&frustum->planes[2], &Corners[0], &Corners[1 * 3], &Corners[0], &Corners[4 * 3], &Corners[0]); // top
    CreatePlane(&frustum->planes[3], &Corners[3 * 3], &Corners[2 * 3], &Corners[3 * 3], &Corners[7 * 3], &Corners[3 * 3]); // bottom

    CreatePlane(&frustum->planes[4], &Corners[1 * 3], &Corners[3 * 3], &Corners[1 * 3], &Corners[5 * 3], &Corners[1 * 3]); // left
    CreatePlane(&frustum->planes[5], &Corners[0], &Corners[4 * 3], &Corners[0], &Corners[2 * 3], &Corners[0]); // right
}
