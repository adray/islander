#pragma once
#include <stdint.h>
#include <string>
#include <queue>
#include <unordered_map>
#include "Vector3.h"
#include "Keys.h"

namespace Islander
{
    enum class WindowStyle
    {
        Borderless = 0,
        Border = 1
    };

    struct Input
    {
        bool control;
        bool shift;
        bool alt;
        std::unordered_map<int, Keys> keymap;
        bool keysdown[KEY_COUNT];
        std::queue<Vec2i> mouse_input;
        Vec2i mousepos;
        Vec2i initial_pos;
        MouseInput rbuttondown;
        MouseInput lbuttondown;
        int mouseWheelDelta;
        int mouseWheel;

        Input();
    };

#define DISPLAY_NAME_LEN 128
#define MAX_DISPLAYS 8

    struct Display
    {
        int _id;
        char _name[DISPLAY_NAME_LEN];
        Vec2i _pos;
        Vec2i _size;
    };

    struct Displays
    {
        int _count;
        Display _displays[MAX_DISPLAYS];
    };

    class InternalWindow;

    struct WINDOW
    {
        WINDOW(void* parent);

        int Process();

        int Process(uint32_t& msg);

        Input* GetInput();

        void Destroy();

        void SetSize(int32_t width, int32_t height);

        int32_t Width() const;

        int32_t Height() const;

        void SetMousePos(int x, int y);

        void SetTitle(const std::string& title);

        void* Handle() const;

        void SetFullscreen(bool fullscreen);

        void GetDisplays(Displays* displays);

        void SelectDisplay(int id);

        void SetWindowIcon(const char* data, int32_t width, int32_t height);

        int SetWindowStyle(WindowStyle style);

        uint32_t GetDPI();

    private:

        Islander::InternalWindow* _impl;

        WINDOW(const WINDOW& window) = default;

    };
}
