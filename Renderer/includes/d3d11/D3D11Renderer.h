#pragma once
#include "d3d11\D3D11Buffer.h"
#include "d3d11\D3D11Shader.h"
#include "d3d11\D3D11Texture.h"
#include "Renderer.h"
#include "Collections.h"
#include "Effekseer.h"
#include <stack>

#pragma comment(lib, "dxguid.lib")
#pragma comment(lib, "d3d11.lib")
#pragma comment(lib, "dxgi.lib")
#pragma comment(lib, "d3d10.lib")

struct ID3D11DeviceContext;
struct IDXGISwapChain;
struct ID3D11Device;
struct ID3D11Texture2D;
struct ID3D11DepthStencilState;
struct ID3D11DepthStencilView;
struct ID3D11RasterizerState;
struct ID3D11RenderTargetView;
struct ID3D11ShaderResourceView;
struct ID3D11SamplerState;
struct ID3D11PixelShader;
struct ID3D10Blob;
struct ID3D11BlendState;
struct ID3D11Counter;
struct ID3D11Query;

namespace Islander
{
	class WINDOW;
	class Material;
	class MaterialTexture;

	namespace Renderer
	{
		class FontDescription;

        template<typename T>
        class DX
        {
        public:
            DX(T* value) : _value(value) {}
            DX() : _value(nullptr) {}
            DX(const DX<T>& other) : _value(other._value)
            {
                if (_value)
                {
                    _value->AddRef();
                }
            }
            DX<T>& operator=(const DX<T>& other)
            {
                if (_value)
                {
                    _value->Release();
                }
                _value = other._value;
                if (_value)
                {
                    _value->AddRef();
                }
                return *this;
            }
            DX<T>& operator=(T* other)
            {
                if (_value)
                {
                    _value->Release();
                }
                _value = other;
				if (_value)
				{
					_value->AddRef();
				}
                return *this;
            }
            T* Value()
            {
                return _value;
            }
            operator T*() { return _value; }
            operator T**() { return &_value; }
            ~DX()
            {
                if (_value)
                {
                    _value->Release();
                    _value = nullptr;
                }
            }
        private:
            T* _value;
        };

        struct D3d11RasterState
        {
            RasterState _state;
            DX<ID3D11RasterizerState> _rasterstate;
        };

		struct D3d11RenderTargetItem
		{
			ID3D11RenderTargetView* render_target;
			ID3D11DepthStencilView* depthview;
			ID3D11DepthStencilState* depthstate;
			ID3D11DepthStencilState* depthstate_no_write;
			RenderTargetType format;
			int width;
			int height;
		};

        struct D3d11Profile
        {
            DX<ID3D11Query> start;
            DX<ID3D11Query> end;
        };

        // Holds the current bound resources.
        struct D3d11Cache
        {
            D3d11VertexShader* vertexShader;
            D3d11PixelShader* pixelShader;

            D3d11Cache() : vertexShader(nullptr), pixelShader(nullptr) {}
        };
		
		class D3d11Renderer : public IRenderer
		{
		public:

			D3d11Renderer();

			virtual void Init(WINDOW *hWindow, FontDescription& font);

			void GetStats(IslanderRendererStats& stats);

			void BeginLightPass();
			
			IBuffer* CreateVertexBuffer(Usage flags, int sizeInBytes, void* data, int stride, GraphicsBuffering buffering);

			IBuffer* CreateConstantBuffer(Usage flags, int sizeInBytes, void* data);

            IBuffer* CreateIndexBuffer(Usage flags, int sizeInBytes, void* data);

			IBuffer* CreateStructuredBuffer(Usage flags, int sizeInBytes, int stride, void* data);

			void RecycleBuffer(IBuffer* buffer);

			void FreeVertexBuffer(int handle);

			void FreeConstantBuffer(int handle);

            void FreeIndexBuffer(int handle);

			void FreeStructuredBuffer(int handle);

			void SubmitDraw(const RenderOperation& op);
			
			void SubmitDrawInstanced(const InstancedRenderOperation& op, int count);

			void DispatchCompute(const ComputeOperation& op);

			void BeginPass(const PASS_CONFIG& config, int pass, int render_flags);

			void Release();

			int LoadPixelShader(const char* filename, const char* method, IslanderShaderDefineList* defines);

			int LoadVertexShader(const char* filename, const char* method, SEMANTIC * semantics, int count, IslanderShaderDefineList* defines);

			int LoadGeometryShader(const char* filename, const char* method, IslanderShaderDefineList* defines);

			int LoadComputeShader(const char* filename, const char* method, IslanderShaderDefineList* _defines);

			void SetPixelShaderSamplers(int pixelShader, int numSamplers, IslanderTextureSampler* samplers);

			void SetVertexShaderSamplers(int vertexShader, int numSamplers, IslanderTextureSampler* samplers);

			int LoadTexture(const char* filename);

			int LoadTexture(void* data, int width, int height, int size);

			int LoadTexture(int texture, void* data, int width, int height, int size);

			int CreateTextureArray(int numTextures, int* textures_, int width, int height);

			int CreateCubeMapTexture(int* textureFaces, int width, int height);
			
			int ReserveTexture();

			int CreateEmptyTexture(int texture, int width, int height, int type);
	
			void CopyToTexture(int texture, void* data, int width, int height, int offsetX, int offsetY);

			void ClearTexture(int texture, int width, int height, int offsetX, int offsetY);

			void CreateComputeTexture(int texture);

			void ResizeBackBuffer();

			void ReleaseRenderTarget(int handle);

			IslanderRenderTarget CreateRenderTarget(RenderTargetType type, uint32_t width, uint32_t height);

            void ResizeRenderTarget(IslanderRenderTarget target, uint32_t width, uint32_t height);

			IslanderRenderTargetArray CreateRenderTargetArray(RenderTargetType type, uint32_t width, uint32_t height, uint32_t dimensions);

			void Present();

			void SetFullscreen(bool mode);

			void BindQuad(float* stream, const QuadBindingContext& context);

			void BindLine(float* stream, const LineBindingContext& context);

			void BindText(float* stream, const TextBindingContext& context, const std::unordered_map<std::string, Font*>& engine);

			inline int32_t RendererType() const { return ISLANDER_RENDERER_TYPE_D3D11; }

			int CreateTextureAtlas(int sourceTexture, MaterialTexture* set, int count);

			char* GetBytesFromTexture(int texture, int* width, int* height, int* size);

            bool GetTextureList(TextureListData& list);

            void GetDisplayResolutions(DisplayResolutions& list);

			bool RequiresFlipping(int id) const { return false; }

			void InitializeImgui();

			void NewFrameImgui();

			void RenderImgui();

			void ImageImgui(int texture, float width, float height);

			void ImageImgui(int texture, float x, float y, float width, float height);

			bool ImageButtonImgui(int id, int texture, float x, float y, float sx, float sy);

			void SetSyncInterval(int syncInterval);

            ParticleRenderer* GetParticleRenderer();

            void BeginDrawParticles(Camera3D* camera);

            void EndDrawParticles();

			void InitializePerformanceCounters();

            void BeginGPUProfilingFrame();
            
            int EndGPUProfilingFrame();

            void BeginGPUProfiling();

            int EndGPUProfiling();

            bool GetGPUProfileFrameData(int handle, IslanderGPUProfileFrameData& data);

            bool GetGPUProfileData(int handle, IslanderGPUProfileData& data);

		private:
			
			// Forbid
			D3d11Renderer(const D3d11Renderer& other);
			const D3d11Renderer& operator=(D3d11Renderer& other);
			
			int CreateRenderTarget(RenderTargetType type, uint32_t width, uint32_t height, ID3D11Texture2D ** texture, ID3D11RenderTargetView ** view, int * tex);
			int CreateRenderTarget(uint32_t width, uint32_t height, ID3D11Texture2D * rendertarget, ID3D11RenderTargetView ** view);
			int CreateDepthStencilTarget(uint32_t width, uint32_t height, ID3D11Texture2D ** rendertarget, ID3D11DepthStencilState ** state,
				ID3D11DepthStencilState** state_no_write,
				ID3D11DepthStencilView ** depthview);
            int CreateRasterState(D3d11RasterState& rasterState);
			void ReportShaderError(ID3D10Blob * error);
            void SelectRasterState(const RasterState& rasterState);

			int ProcessTexture(ID3D11ShaderResourceView* texture);
			
			BindType BindShader(int pixelshader, int vertexshader, int geometryshader);

			void BindTexture(int slot, int texture);

			int CreateSampler(bool wrap);

			ID3D11ShaderResourceView * LoadTextureInternal(void* data, int size, int width, int height, bool greyscale, bool genMipMaps);
            int LoadTexture(int tex, void* data, int width, int height, int size, bool greyscale);

			WINDOW * window;
            ParticleRenderer* particleRenderer;

			ID3D11DeviceContext * context;
			IDXGISwapChain * swapchain;
			ID3D11Device * device;
			ID3D11Texture2D * backbuffer;
			ID3D11Texture2D * depthstenciltexture;
			ID3D11DepthStencilState* depthstate;
			ID3D11DepthStencilState * depthstate_no_write;
			ID3D11DepthStencilView * depthview;
            ID3D11RasterizerState * currentrasterstate;
            RasterState currentRasterStateParams;
            std::vector<D3d11RasterState> rasterStateList;
			ID3D11RenderTargetView * rendertargetview;

			ID3D11Texture2D * tex_lightmap;
			ID3D11RenderTargetView * view_lightmap;

            D3d11Cache cache;
			
			HandleCollection<D3d11Texture> textures;
			HandleCollection<ID3D11SamplerState*> samplers;
			HandleCollection<D3d11RenderTargetItem> rendertargets;
			HandleCollection<D3d11PixelShader*> pixelshaders;
			HandleCollection<D3d11VertexShader*> vertexshaders;
			HandleCollection<D3d11GeometryShader*> geometryshaders;
			HandleCollection<D3d11ComputeShader*> computeshaders;

			KeyedAllocationBuffer<D3d11ConstantBuffer*> constantbuffers;
			KeyedAllocationBuffer<D3d11StructuredBuffer*> structuredbuffers;
            KeyedAllocationBuffer<D3d11VertexBuffer*> vertexbuffers;
            KeyedAllocationBuffer<D3d11IndexBuffer*> indexbuffers;
			
			ID3D11BlendState * blendstate;
			ID3D11BlendState * blendstate2;

			std::vector<ID3D11Counter*> counters;
            std::stack<D3d11Profile> profile;
            HandleCollection<D3d11Profile> profileItems;
            DX<ID3D11Query> disjoint;
            HandleCollection<DX<ID3D11Query>> disjointItems;

			//DX<ID3D11Query> computeQuery;

			int texture_lightmap;
			int current_rendertarget;

			float colour[4];
			char hardware_desc[128];

            DisplayResolutions display_resolutions;
			int _syncInterval;
		};
	}
}
