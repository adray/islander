#pragma once
#include "Renderer.h"
#include "GraphicsBuffer.h"

struct ID3D11Buffer;
struct ID3D11Device;
struct ID3D11DeviceContext;
struct ID3D11ShaderResourceView;

namespace Islander
{
	namespace Renderer
	{
		enum class BindType
		{
			None = 0,
			Vertex = 1,
			Pixel = 2,
			Compute = 4,
			Geometry = 8
		};

		class D3d11StructuredBuffer : public IBuffer
		{
		public:
			D3d11StructuredBuffer(ID3D11Device* device, int stride);

			virtual void Allocate(int sizeInBytes, void* data, Usage usage);

			virtual void Fill(int offset, void* data);

			virtual void Release();

			virtual ~D3d11StructuredBuffer();

			void Bind(int index, BindType type);

			inline void SetID(int id) { _id = id; }

			inline int ID() const { return _id; }

			inline bool Available() const { return _allocated; }

			inline int Size() const { return _bytes; }

			inline Usage GetUsage() const { return _usage; }

		private:

			// Forbid
			D3d11StructuredBuffer(const D3d11StructuredBuffer& other);
			const D3d11StructuredBuffer& operator=(D3d11StructuredBuffer& other);

			Usage _usage;
			int _id;
			int _allocated;
			int _bytes;
			int _stride;
			ID3D11Buffer* _buffer;
			ID3D11Device* _device;
			ID3D11DeviceContext* _context;
			ID3D11ShaderResourceView* _shaderResourceView;
		};

		class D3d11ConstantBuffer : public IBuffer
		{
		public:

			D3d11ConstantBuffer(ID3D11Device* device);

			virtual void Allocate(int sizeInBytes, void* data, Usage usage);

			virtual void Fill(int offset, void* data);

			virtual void Release();

			virtual ~D3d11ConstantBuffer();

			void Bind(int index, BindType type);

			inline void SetID(int id) { _id = id; }

			inline int ID() const { return _id; }

			inline bool Available() const { return _allocated; }

			inline int Size() const { return _bytes; }

			inline Usage GetUsage() const { return _usage; }

		private:

			// Forbid
			D3d11ConstantBuffer(const D3d11ConstantBuffer& other);
			const D3d11ConstantBuffer& operator=(D3d11ConstantBuffer& other);

			Usage _usage;
			int _id;
			int _allocated;
			int _bytes;
			ID3D11Buffer* _buffer;
			ID3D11Device* _device;
			ID3D11DeviceContext* _context;
		};

        class D3d11IndexBuffer : public IBuffer
        {
        public:

            D3d11IndexBuffer(ID3D11Device* device, GraphicsBuffering buffering);

            virtual void Allocate(int sizeInBytes, void* data, Usage usage);

            virtual void Fill(int offset, void* data);

            virtual void Release();

            virtual ~D3d11IndexBuffer();

            void Bind(int index);

            inline void SetID(int id) { _id = id; }

            inline int ID() const { return _id; }

            inline bool Available() const { return _allocated; }

            inline int Size() const { return _bytes; }

            inline Usage GetUsage() const { return _usage; }

        private:

            // Forbid
            D3d11IndexBuffer(const D3d11IndexBuffer& other);
            const D3d11IndexBuffer& operator=(D3d11IndexBuffer& other);

            Usage _usage;
            int _id;
            int _allocated;
            int _bytes;
            int _index;
            GraphicsBuffering _buffering;
            ID3D11Buffer* _buffer[2];
            ID3D11Device* _device;
            ID3D11DeviceContext* _context;
        };

		class D3d11VertexBuffer : public IBuffer
		{
		public:
			
			D3d11VertexBuffer(ID3D11Device* device, int stride, GraphicsBuffering buffering);

			virtual void Allocate(int sizeInBytes, void* data, Usage usage);

			virtual void Fill(int offset, void* data);

			virtual void Release();

			virtual ~D3d11VertexBuffer();

			void Bind(int index);

			inline void SetID(int id) { _id = id; }

			inline int ID() const { return _id; }

			inline bool Available() const { return _allocated; }

			inline int Size() const { return _bytes; }

			inline Usage GetUsage() const { return _usage; }

			inline int GetStride() const { return _stride; }

			void Swap();

		private:
			
			// Forbid
			D3d11VertexBuffer(const D3d11VertexBuffer& other);
			const D3d11VertexBuffer& operator=(D3d11VertexBuffer& other);

			Usage _usage;
			int _id;
			int _allocated;
			int _bytes;
			int _stride;
			int _index;
			GraphicsBuffering _buffering;
			ID3D11Buffer* _buffer[2];
			ID3D11Device* _device;
			ID3D11DeviceContext* _context;

		};
	}
}
