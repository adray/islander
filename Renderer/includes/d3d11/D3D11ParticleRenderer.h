#pragma once
#include "Collections.h"

struct ID3D11DeviceContext;
struct ID3D11Device;
struct ID3D11ShaderResourceView;

namespace Islander
{
    class Camera3D;

    namespace Renderer
    {
        struct ParticleRenderer;
        struct D3d11Texture;

        ParticleRenderer* CreateD3d11ParticleRenderer(ID3D11DeviceContext* d3D11DeviceContext, ID3D11Device* d3D11Device,
            HandleCollection<D3d11Texture>* textures);
        void D3d11BeginDrawParticles(ParticleRenderer* renderer, Camera3D* camera);
        void D3d11EndDrawParticles(Islander::Renderer::ParticleRenderer* renderer);
    }
}
