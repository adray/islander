#pragma once

struct ID3D11ShaderResourceView;
struct ID3D11UnorderedAccessView;
struct ID3D11Device;

namespace Islander
{
    namespace Renderer
    {
        struct D3d11Texture
        {
            ID3D11ShaderResourceView* srv;
            ID3D11UnorderedAccessView* uav;
        };

        void CreateTextureUAV(D3d11Texture& texture, ID3D11Device* device);
    }
}
