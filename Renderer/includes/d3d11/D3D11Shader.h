#pragma once
#include "GraphicsShader.h"
#include <string>
#include <vector>

struct ID3D11VertexShader;
struct ID3D11PixelShader;
struct ID3D11GeometryShader;
struct ID3D11ComputeShader;
struct ID3D11InputLayout;
struct ID3D11ShaderReflection;
struct D3D11_INPUT_ELEMENT_DESC;
struct ID3D11Device;
struct ID3D10Blob;

namespace Islander
{
	namespace Renderer
	{
		struct SEMANTIC;
		class Attribute;

		class D3d11VertexShader : public GraphicsShader
		{
		public:

			D3d11VertexShader(ID3D11Device* device, ID3D10Blob* blob, SEMANTIC * semantics, int count);

			virtual void Release();
			
			virtual void Bind();

			inline bool HasErrors() { return _errors.size() > 0; }

			void GetErrors(std::vector<std::wstring>& errors);

			void Link(const std::vector<Attribute>& attributes);

			inline const std::vector<Attribute>& GetAttributes() {
				return _attributes;
			};

		private:
			ID3D11Device* _device;
			ID3D11VertexShader* _shader;
			ID3D11InputLayout* _layout;
			ID3D11ShaderReflection* _reflection;
			D3D11_INPUT_ELEMENT_DESC* _layoutDescription;
			std::vector<std::wstring> _errors;
			std::vector<Attribute> _attributes;
		};

		class D3d11PixelShader : public GraphicsShader
		{
		public:

			D3d11PixelShader(ID3D11Device* device, ID3D10Blob* blob);

			virtual void Release();

			virtual void Bind();

		private:
			ID3D11Device* _device;
			ID3D11PixelShader* _shader;
		};

		class D3d11GeometryShader : public GraphicsShader
		{
		public:

			D3d11GeometryShader(ID3D11Device* device, ID3D10Blob* blob);

			virtual void Release();

			virtual void Bind();

		private:
			ID3D11Device* _device;
			ID3D11GeometryShader* _shader;
		};

		class D3d11ComputeShader : public GraphicsShader
		{
		public:

			D3d11ComputeShader(ID3D11Device* device, ID3D10Blob* blob);

			virtual void Release();

			virtual void Bind();

		private:
			ID3D11Device* _device;
			ID3D11ComputeShader* _shader;
		};
	}
}
