#pragma once
#include <array>
#include <vector>
#include <stdint.h>
#include "AABB.h"

namespace Islander
{
    namespace Renderer
    {
        // Defines a grid of cells where objects can reside in one or more cells.
        // The grid can be used for broad phase culling.

        constexpr int RENDER_GRID_COUNT = 8;
        constexpr int RENDER_GRID_BIT_FLAG_BYTES = RENDER_GRID_COUNT * RENDER_GRID_COUNT * RENDER_GRID_COUNT / 8;

        struct RenderCell
        {
            Islander::Numerics::AABB box;
            int x;
            int y;
            int z;

            float centre[3];
            float radius;
        };

        struct RenderGridEntity
        {
            uint8_t* bitflags; // bit flags which the entity is in of size RENDER_GRID_BIT_FLAG_BYTES.
            float pos[3];
            float scale[3];
            float rotation[3];
            Islander::Numerics::AABB box;
        };

        struct RenderGrid
        {
            // Grid of static objects
            std::array<RenderCell, RENDER_GRID_COUNT * RENDER_GRID_COUNT * RENDER_GRID_COUNT> cells;
            Islander::Numerics::AABB box;
            std::vector<RenderGridEntity> entities;
            uint8_t bitflags[RENDER_GRID_BIT_FLAG_BYTES]; // bit flags of the cells active. Of size RENDER_GRID_BIT_FLAG_BYTES.
            bool dirty;
        };

        void RenderGridInitialize(RenderGrid& grid);
        void RenderGridAddEntity(RenderGrid& grid, const RenderGridEntity& entity);
        void RenderGridUpdateEntity(RenderGrid& grid, const RenderGridEntity& entity);
        void RenderGridResizeGrid(RenderGrid& grid);
        void RenderGridCullGrid(RenderGrid& grid, void* camera);
        void RenderGridClearGrid(RenderGrid& grid);
        bool RenderGridCullEntity(RenderGrid& grid, const RenderGridEntity& entity);
    }
}
