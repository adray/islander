#pragma once
#include <string>

namespace Islander
{
    namespace Renderer
    {
        int ExportTexture(const std::string& path, unsigned char* data, int width, int height, int size);
    }
}
