#pragma once
#include <memory>
#include <algorithm>

namespace Islander
{
	namespace Renderer
	{
		class GraphicsShader
		{
		public:

			GraphicsShader() : _numSamplers(0) {}

			virtual void Release()=0;

			virtual ~GraphicsShader()
			{
			}

			static constexpr int MAX_SAMPLERS = 4;

			void SetSamplers(int numSamplers, int* samplers)
			{
				_numSamplers = numSamplers;
				std::memcpy(_samplers, samplers, sizeof(int) * std::min(MAX_SAMPLERS, numSamplers));
			}

			int NumSamplers() const { return _numSamplers; }

			const int* const Samplers() const { return _samplers; }

		private:
			int _numSamplers;
			int _samplers[MAX_SAMPLERS];
		};
	}
}

