#pragma once
//#include "imgui.h"
#include "RendererTypes.h"

namespace Islander
{
    class IDevice;

    namespace Imgui
    {
        //void RestoreContext(ImGuiContext* context, ImGuiMemAllocFunc alloc_func, ImGuiMemFreeFunc free_func, void* user_data);
        void RestoreContext(IslanderImguiContext* context);

        void CreateIMGUI(Islander::IDevice* device);

        void NewFrame(Islander::IDevice* device);

        void Render(Islander::IDevice* device);

        void Image(Islander::IDevice* device, int texture, float width, float height);

        void Image(Islander::IDevice* device, int texture, float x, float y, float width, float height);

        bool ImageButton(Islander::IDevice* device, int id, int texture, float x, float y, float sx, float sy);
    }
}
