#pragma once

namespace Islander
{
	namespace Renderer
	{
		class IRenderer;

		class Quad
		{
		public:
			Quad(void);
			~Quad(void);

			int UpdateBuffer(IRenderer* renderer);

			int buffer;

		private:
			float * data;

			// purposely not implemented
			Quad(const Quad& quad);
			const Quad& operator=(Quad& other);
		};

		class Line
		{
		public:
			Line(void);
			~Line(void);

			int UpdateBuffer(IRenderer* renderer);

			int buffer;
		private:
			float * data;

			// purposely not implemented
			Line(const Line& line);
			const Line& operator=(Line& other);
		};

	}
}

