#pragma once
#include <string>
#include <vector>

namespace Islander
{
    namespace Renderer
    {
        enum TokenType
        {
            TOKEN_TYPE_NONE,
            TOKEN_TYPE_TEXT,
            TOKEN_TYPE_SPACE,
            TOKEN_TYPE_BOLD_BEGIN,
            TOKEN_TYPE_BOLD_END,
            TOKEN_TYPE_NEWLINE,
            TOKEN_TYPE_TAB,
            TOKEN_TYPE_STRIKE_BEGIN,
            TOKEN_TYPE_STRIKE_END,
            TOKEN_TYPE_LINK_BEGIN,
            TOKEN_TYPE_LINK_END
        };

        struct Token
        {
            TokenType _type;
            std::string _text;
            int _index;
            bool linebreak;
        };

        class TextTokenizer
        {
            public:
                /*
                <b>Bold Text</b>
                < b >Bold Text</ b>
                < b Bold Text</>
                <bbb Bold Text ></b>
                */

                static void GetTokens(const std::string& text, std::vector<Token>& tokens);
            private:

                enum SymbolType
                {
                    SYMBOL_TYPE_NONE,
                    SYMBOL_TYPE_TEXT,
                    SYMBOL_TYPE_SPACE,
                    SYMBOL_TYPE_ANGLE_LEFT,
                    SYMBOL_TYPE_ANGLE_RIGHT,
                    SYMBOL_TYPE_NEWLINE,
                    SYMBOL_TYPE_FORWARD_SLASH,
                    SYMBOL_TYPE_TAB,
                    SYMBOL_TYPE_EOF
                };

                static TextTokenizer::SymbolType GetNextSymbol(const std::string& text, int* i);
        };
    }
}
