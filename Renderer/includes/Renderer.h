#pragma once
#include <vector>
#include <cstdint>
#include <unordered_map>
#include <string>
#include <assert.h>
#include "RendererTypes.h"

//struct row_property;
struct row_entity;

constexpr int TEXTURE_LIST_MAX_COUNT = 128;
constexpr int RESOLUTION_LIST_MAX_COUNT = ISLANDER_MAX_RESOLUTIONS;

namespace Islander
{
    struct WINDOW;
    class Entity;
    class Material;
    class TextSprite;
    class MaterialTexture;
    class Camera3D;

    template<typename T>
    class Camera;

    typedef Camera<float> Cameraf;

    struct component_material;
    struct component_text;

    namespace Renderer
    {
        struct FontDescription;
        class Font;
        class IBuffer;
        struct ParticleRenderer;
        
        enum GraphicsBuffering
        {
            BUFFERING_SINGLE,
            BUFFERING_DOUBLE,
        };

        enum Usage
        {
            USAGE_STATIC,
            USAGE_DYNAMIC,
            USAGE_DEFAULT
        };

        enum class RenderTargetType
        {
            Diffuse = 0,
            Depth = 1,
            ObjectID = 2
        };

        struct SEMANTIC
        {
            int32_t format;
            uint32_t stream;
            char* desc;

            SEMANTIC() : stream(0), desc(nullptr)
            {
            }

        };

        enum AttributeClass
        {
            ATTRIBUTE_CLASS_TRANSFORM=0,
            ATTRIBUTE_CLASS_TEXTURE0,
            ATTRIBUTE_CLASS_TEXTURE1,
            ATTRIBUTE_CLASS_TEXTURE2,
            ATTRIBUTE_CLASS_TEXTURE3,
            ATTRIBUTE_CLASS_CUSTOM,
            ATTRIBUTE_CLASS_FONT_COLOUR,
            ATTRIBUTE_CLASS_MESH_TRANSFORM,
            ATTRIBUTE_CLASS_WINDOW_WIDTH,
            ATTRIBUTE_CLASS_WINDOW_HEIGHT
        };

        class Attribute
        {
        public:

            Attribute(int32_t format,
                AttributeClass type,
                int propertyType,
                std::string parameter) : _format(format), _type(type), _propertyType(propertyType), _parameter(parameter), _offset(-1)
            {
            }

            AttributeClass GetType() const { return _type; }

            int GetPropertyType() const { return _propertyType; }

            int32_t GetFormat() const { return _format; }

            std::string GetParameter() const { return _parameter; }

            int GetOffset() const { return _offset; }

            void SetOffset(int offset) { _offset = offset; }

            void SetIndex(int index) { _index = index; }

            int GetIndex() { return _index; }

        private:
            int32_t _format;
            AttributeClass _type;
            int _propertyType;
            std::string _parameter;
            int _offset;
            int _index;
        };

        struct PASS_CONFIG
        {
            static const int RENDER_TARGET_MAX_COUNT = 4;

            std::string _passName;
            int _passID;
            int _renderTarget[RENDER_TARGET_MAX_COUNT];
            int _renderTargetCount;
            int _flags;
            void* constantData;
            int constantDataSize;
        };

        struct PASS_LIST
        {
        private:

            std::vector<PASS_CONFIG> _passes;
            int _passCount;

        public:

            PASS_LIST() : _passCount(0)	{ }

            void AddPass(int renderTarget, int flags, const std::string& name)
            {
                PASS_CONFIG pass;
                pass._passID = _passCount++;
                pass._renderTarget[0] = renderTarget;
                pass._renderTargetCount = 1;
                pass._flags = flags;
                pass.constantData = nullptr;
                pass.constantDataSize = 0;
                pass._passName = name;
                _passes.push_back(pass);
            }

            void AddPass(const int* renderTarget, int renderTargetCount, int flags, const std::string& name)
            {
                PASS_CONFIG pass;
                pass._passID = _passCount++;
                std::memcpy(pass._renderTarget, renderTarget, sizeof(int) * renderTargetCount);
                pass._renderTargetCount = renderTargetCount;
                pass._flags = flags;
                pass.constantData = nullptr;
                pass.constantDataSize = 0;
                pass._passName = name;
                _passes.push_back(pass);
            }

            void UpdatePassData(int id, void* constantData, int size)
            {
                if (id < _passes.size())
                {
                    assert(size % (sizeof(float) * 4) == 0);
                    if (size % (sizeof(float) * 4) == 0)
                    {
                        auto& pass = _passes[id];
                        pass.constantData = constantData;
                        pass.constantDataSize = size;
                    }
                }
            }

            void Clear()
            {
                _passes.clear();
                _passes.swap(_passes);
                _passCount = 0;
            }

            bool FetchPass(int id, PASS_CONFIG* pass) const
            {
                if (id < _passes.size())
                {
                    *pass = _passes[id];
                    return true;
                }
                return false;
            }

            int Count() const
            {
                return _passCount;
            }

            void Append(const PASS_LIST& list)
            {
                for (int i = 0; i < list._passCount; ++i)
                {
                    AddPass(&list._passes[i]._renderTarget[0],
                        list._passes[i]._renderTargetCount,
                        list._passes[i]._flags,
                        list._passes[i]._passName);
                }
            }
        };
        
        enum SamplerType
        {
            SAMPLER_CLAMP = 0,
            SAMPLER_WRAP = 1,
        };

        enum Topology
        {
            TOPOLOGY_TRIANGLE,
            TOPOLOGY_LINELIST,
        };

        struct RasterState
        {
            bool _multi_sampling;
            bool _backface_culling;
            bool _frontface_culling;
            bool _wireframe;

            RasterState() :
                _multi_sampling(false),
                _backface_culling(false),
                _frontface_culling(false),
                _wireframe(false)
            { }

            bool Equals(const RasterState& other) const
            {
                return other._backface_culling == _backface_culling &&
                    other._frontface_culling == _frontface_culling &&
                    other._multi_sampling == _multi_sampling &&
                    other._wireframe == _wireframe;
            }
        };

        class RenderOperation
        {
        public:

            RenderOperation(int vertexBuffer, int constantBuffer, component_material* material, Topology topology, SamplerType sampler);
            RenderOperation(int vertexBuffer, int constantBuffer, component_material* material, Topology topology, SamplerType sampler, int vertexCount);
            RenderOperation(int vertexBuffer, int constantBuffer, int texture, int pixelshader, int vertexshader, Topology topology, SamplerType sampler);
            RenderOperation(int vertexBuffer, int constantBuffer, int indexBuffer, component_material* material, Topology topology, SamplerType sampler, RasterState rasterState, int indexOffset, int indexCount);
            RenderOperation(int vertexBuffer, int constantBuffer, int constantBuffer2, int indexBuffer, component_material* material, Topology topology, SamplerType sampler, RasterState rasterState, int indexOffset, int indexCount);

            inline int GetVertexCount() const { return _vertexCount; }
            inline int GetVertexBuffer() const { return _vertexbuffer; }
            inline int GetConstantBuffer() const { return _constantBuffer; }
            inline int GetConstantBuffer2() const { return _constantBuffer2; }
            inline int GetIndexBuffer() const { return _indexBuffer; }
            inline int GetTextures(int index) const { return _texture[index]; }
            inline Topology GetTopology() const { return _topology; }
            inline int GetVertexShader() const { return _vertexshader; }
            inline int GetPixelShader() const { return _pixelshader; }
            inline int GetGeometryShader() const { return _geometryShader; }
            inline SamplerType GetSampler() const { return _sampler; }
            inline RasterState GetRasterState() const { return _raster_state; }
            inline int GetIndexOffset() const { return _indexOffset; }
            inline int GetIndexCount() const { return _indexCount; }

        private:
            int _vertexCount;
            int _indexOffset;
            int _indexCount;
            int _vertexbuffer;
            int _constantBuffer;
            int _constantBuffer2;
            int _indexBuffer;
            int _vertexshader;
            int _pixelshader;
            int _geometryShader;
            int _texture[4];
            Topology _topology;
            SamplerType _sampler;
            RasterState _raster_state;
        };

        class InstancedRenderOperation
        {
        public:

            InstancedRenderOperation(int textures[4], int vertexBuffer[4], int constantBuffer[4], int pixelshader, int vertexshader, Topology topology, SamplerType sampler);

            inline int GetVertexBuffer(int index) const { return _vertexbuffer[index]; }
            inline int GetConstantBuffer(int index) const { return _constantBuffer[index]; }
            inline int GetTextures(int index) const { return _texture[index]; }
            inline int GetVertexShader() const { return _vertexshader; }
            inline int GetPixelShader() const { return _pixelshader; }
            inline Topology GetTopology() const { return _topology; }
            inline SamplerType GetSampler() const { return _sampler; }

        private:
            int _texture[4];
            int _vertexshader;
            int _pixelshader;
            int _vertexbuffer[4];
            int _constantBuffer[4];
            Topology _topology;
            SamplerType _sampler;
        };

        class ComputeOperation
        {
        public:

            ComputeOperation(int computeshader, int threadGroups[3], int constantBuffer, int textures[4], int computeBuffer);

            inline int GetComputeShader() const { return _computeshader; }
            inline const int* ThreadGroups() const { return _threadGroups; }
            inline int GetConstantBuffer() const { return _constantBuffer; }
            inline const int* Textures() const { return _textures; }
            inline int GetComputeBuffer() const { return _computeBuffer; }

        private:
            int _computeshader;
            int _threadGroups[3];
            int _constantBuffer;
            int _computeBuffer;
            int _textures[4];
        };

        struct TextBindingContext
        {
            int vertex;
            float x;
            float y;
            float z;
            float sx;
            float sy;
            row_entity* properties;
            component_text* sprite;
        };

        struct QuadBindingContext
        {
            float x;
            float y;
            float sx;
            float sy;
            row_entity* properties;
            component_material* mat;
            Cameraf* camera;
        };

        struct LineBindingContext
        {
            float x;
            float y;
            float sx;
            float sy;
            row_entity* properties;
            component_material* mat;
            Cameraf* camera;
        };

        struct TextureData
        {
            int id;
            int width;
            int height;
        };

        struct TextureListData
        {
            int texture_count;
            TextureData textures[TEXTURE_LIST_MAX_COUNT];
        };

        struct DisplayResolution
        {
            int id;
            int width;
            int height;
        };

        struct DisplayResolutions
        {
            int count;
            DisplayResolution resolution[RESOLUTION_LIST_MAX_COUNT];
        };

        class IRenderer
        {
        public:
            
            virtual void Init(WINDOW* _window, FontDescription& font) = 0;

            virtual void GetStats(IslanderRendererStats& stats) = 0;

            virtual IBuffer* CreateVertexBuffer(Usage flags, int sizeInBytes, void* data, int stride, GraphicsBuffering buffering) = 0;

            virtual IBuffer* CreateConstantBuffer(Usage flags, int sizeInBytes, void* data) = 0;

            virtual IBuffer* CreateIndexBuffer(Usage flags, int sizeInBytes, void* data) = 0;

            virtual IBuffer* CreateStructuredBuffer(Usage flags, int sizeInBytes, int stride, void* data) = 0;

            virtual void RecycleBuffer(IBuffer* buffer) = 0;

            virtual void FreeVertexBuffer(int handle) = 0;

            virtual void FreeConstantBuffer(int handle) = 0;

            virtual void FreeIndexBuffer(int handle) = 0;

            virtual void FreeStructuredBuffer(int handle) = 0;

            virtual void SubmitDraw(const RenderOperation& op) = 0;

            virtual void SubmitDrawInstanced(const InstancedRenderOperation& op, int count) = 0;

            virtual void DispatchCompute(const ComputeOperation& op) = 0;

            virtual void BeginLightPass() = 0;

            virtual void BeginPass(const PASS_CONFIG& config, int pass, int render_flags) = 0;

            virtual void Release() = 0;

            virtual int LoadPixelShader(const char* filename, const char* method, IslanderShaderDefineList* defines) = 0;

            virtual int LoadVertexShader(const char* filename, const char* method, SEMANTIC * semantics, int count, IslanderShaderDefineList* defines) = 0;

            virtual int LoadGeometryShader(const char* filename, const char* method, IslanderShaderDefineList* defines) = 0;

            virtual int LoadComputeShader(const char* filename, const char* method, IslanderShaderDefineList* _defines) = 0;

            virtual void SetPixelShaderSamplers(int pixelShader, int numSamplers, IslanderTextureSampler* samplers) = 0;

            virtual void SetVertexShaderSamplers(int vertexShader, int numSamplers, IslanderTextureSampler* samplers) = 0;

            virtual int LoadTexture(const char* filename) = 0;

            virtual int LoadTexture(void* data, int width, int height, int size) = 0;

            virtual int LoadTexture(int texture, void* data, int width, int height, int size) = 0;
            
            virtual int CreateTextureArray(int numTextures, int* textures_, int width, int height) = 0;

            virtual int CreateCubeMapTexture(int* textureFaces, int width, int height) = 0;

            virtual int ReserveTexture() = 0;

            virtual int CreateEmptyTexture(int texture, int width, int height, int type) = 0;

            virtual void ClearTexture(int texture, int width, int height, int offsetX, int offsetY) = 0;

            virtual void CopyToTexture(int texture, void* data, int width, int height, int offsetX, int offsetY) = 0;

            virtual void CreateComputeTexture(int texture) = 0;

            virtual int CreateTextureAtlas(int sourceTexture, MaterialTexture* set, int count) = 0;

            virtual void ResizeBackBuffer() = 0;

            virtual	void ReleaseRenderTarget(int handle) = 0;

            virtual IslanderRenderTarget CreateRenderTarget(RenderTargetType type, uint32_t width, uint32_t height) = 0;

            virtual void ResizeRenderTarget(IslanderRenderTarget target, uint32_t width, uint32_t height) = 0;

            virtual IslanderRenderTargetArray CreateRenderTargetArray(RenderTargetType type, uint32_t width, uint32_t height, uint32_t dimensions) = 0;

            virtual void Present() = 0;

            virtual void SetFullscreen(bool mode) = 0;

            virtual void BindQuad(float* stream, const QuadBindingContext& context) = 0;

            virtual void BindLine(float* stream, const LineBindingContext& context) = 0;

            virtual void BindText(float* stream, const TextBindingContext& context, const std::unordered_map<std::string, Font*>& engine) = 0;
            
            virtual int32_t RendererType() const = 0;

            virtual char* GetBytesFromTexture(int texture, int* width, int* height, int* size) = 0;

            virtual bool GetTextureList(TextureListData& list) = 0;

            virtual void GetDisplayResolutions(DisplayResolutions& list) = 0;

            virtual bool RequiresFlipping(int id) const = 0;

            virtual void InitializeImgui() = 0;

            virtual void NewFrameImgui() = 0;

            virtual void RenderImgui() = 0;

            virtual void ImageImgui(int texture, float width, float height) = 0;

            virtual void ImageImgui(int texture, float x, float y, float width, float height) = 0;

            virtual bool ImageButtonImgui(int id, int texture, float x, float y, float sx, float sy) = 0;

            virtual void SetSyncInterval(int syncInterval) = 0;

            virtual ParticleRenderer* GetParticleRenderer() = 0;

            virtual void BeginDrawParticles(Camera3D* camera) = 0;

            virtual void EndDrawParticles() = 0;

            virtual void InitializePerformanceCounters() = 0;

            virtual void BeginGPUProfilingFrame() = 0;

            virtual int EndGPUProfilingFrame() = 0;

            virtual void BeginGPUProfiling() = 0;

            virtual int EndGPUProfiling() = 0;

            virtual bool GetGPUProfileFrameData(int handle, IslanderGPUProfileFrameData& data) = 0;

            virtual bool GetGPUProfileData(int handle, IslanderGPUProfileData& data) = 0;

            virtual ~IRenderer()
            {
            }
        };

        class ShaderDefinitionParser
        {
        public:
            void Parse(const std::string& input, std::vector<Attribute>& attributes);

        private:
            
            AttributeClass MatchAttributeClass(const std::string& property);
            int MatchProperty(const std::string& property);
        };

        void GetFileNameWithoutExtension(const std::string& in, std::string& out);

        void GetFileNameExtension(const std::string& in, std::string& out);
    }
}
