#pragma once
#include "Effekseer.h"


namespace Islander
{
    class IDevice;

    namespace Renderer
    {
        struct ParticleRenderer
        {
            IDevice* device;
            void* renderer;
            ::Effekseer::SpriteRendererRef spriteRenderer;
            ::Effekseer::RibbonRendererRef ribbonRenderer;
            ::Effekseer::RingRendererRef ringRenderer;
            ::Effekseer::ModelRendererRef modelRenderer;
            ::Effekseer::TrackRendererRef trackRenderer;
            ::Effekseer::TextureLoaderRef textureLoader;
            ::Effekseer::ModelLoaderRef modelLoader;
            ::Effekseer::MaterialLoaderRef materialLoader;
            float elapsedTime;
        };
    }
}
