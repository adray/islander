#pragma once

namespace Effekseer
{
    template <typename T>
    class RefPtr;

    class Effect;

    using EffectRef = RefPtr<Effect>;
}

namespace Islander
{
    class IDevice;

    namespace Renderer
    {
        struct ParticleRenderer;
        struct ParticleSystem;

        ParticleSystem* CreateParticleSystem(ParticleRenderer* particleRenderer, IDevice* device);
        int LoadParticleEffect(ParticleSystem* system, void* data, int size);
        int PlayParticleEffect(ParticleSystem* system, int effectId, float x, float y, float z);
        void StopParticleEffect(Islander::Renderer::ParticleSystem* system, int id);
        void StopAllParticleEffects(Islander::Renderer::ParticleSystem* system);
        bool IsParticleEffectAlive(Islander::Renderer::ParticleSystem* system, int id);
        ::Effekseer::EffectRef GetParticleEffect(ParticleSystem* system, int id);
        void MoveParticleEffect(ParticleSystem* system, int id, float x, float y, float z);
        void SetLocationParticleEffect(ParticleSystem* system, int id, float x, float y, float z);
        void SetRotationParticleEffect(ParticleSystem* system, int id, float x, float y, float z);
        void SetTargetLocationParticleEffect(ParticleSystem* system, int id, float x, float y, float z);
        void UpdateParticleSystem(ParticleSystem* system, float deltaTime);
        void DrawParticleSystem(ParticleSystem* system);
        void DestroyParticleSystem(ParticleSystem* system);
    }
}
