#pragma once
#include <iostream>
#include <Component.h>
#include "Material.h"

namespace Islander
{
	namespace Renderer
	{
		class IRenderer;

		struct FontDescription
		{
			std::string name;
			std::string filedef;
			int img;
		};

		struct FontCharacter
		{
			MaterialTexture texture;
			RECTANGLE rect;
		};

		class FontEngine
		{
		public:
			FontEngine(FontDescription& font, IRenderer* renderer);
			~FontEngine(void);

			int MakeText(component_text* data, component_transform* transform, int width, int height);

			void MeasureText(component_text* data, component_transform* transform);

			inline int NewLine() { return newline; }

		private:
			FontCharacter characters[200];
			int newline;
			int space;
			IRenderer* _renderer;
			bool _supportsAtlas;
		};

		class Font
		{
		public:

			Font(FontDescription& desc, IRenderer* renderer);

			FontDescription desc;
			FontEngine engine;
		};
	}
}
