#pragma once

#include <vector>
#include <string>

namespace Islander
{
	struct WINDOW;

	namespace Renderer
	{	
		class HardwareCursor
		{
		public:

			HardwareCursor(WINDOW* window);

			int LoadCursor(const std::string& path);

			void SetCursor(int type);

			void ShowCursor();

			void HideCursor();

			virtual ~HardwareCursor();

		private:
			std::vector<void*> _images;
			WINDOW* _window;
		};
	}
}
