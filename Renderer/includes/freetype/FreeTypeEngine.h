#pragma once
#include <string>
#include <Component.h>

namespace Islander
{
    namespace Renderer
    {
        class FreeTypeEngineImpl;
        class IRenderer;

        struct FreeTypeEngineRender
        {
            int _texture;
            int _px;
            int _py;
            int _width;
            int _height;
        };

        class FreeTypeEngine
        {
            public:
                FreeTypeEngine();
                ~FreeTypeEngine();

                void LoadFace(const std::string& name, const std::string& filename); 

                bool RenderText(
                    component_text* text,
                    component_transform* transform,
                    int windowWidth, int windowHeight,
                    uint32_t dpi,
                    IRenderer* renderer,
                    FreeTypeEngineRender& render);

                void Clear(IRenderer* renderer);

                void FreeBuffer(IRenderer* renderer, int buffer);

                void MeasureText(
                    component_text* text,
                    component_transform* transform,
                    int windowWidth,
                    int windowHeight,
                    uint32_t dpi);

            private:
                FreeTypeEngineImpl* _impl;
        };
    }
}
