#pragma once
#include <vector>

namespace Islander
{
	namespace Renderer
	{
		class IRenderer;

		class RenderSystems
		{
		public:
			void GetRenderers(std::vector<IRenderer*>& systems);

		private:
			std::vector<IRenderer*> _systems;
		};
	}
}
