#pragma once

namespace Islander
{
	struct WINDOW;

	namespace Renderer
	{
		class InternalOpenGLContext;

		class OpenGLContext
		{
		public:

			OpenGLContext();

			int InitializeGL(WINDOW* window);

			void DeactivateGL(WINDOW* window);

			void Present(WINDOW* window);

		private:

			InternalOpenGLContext* _impl;

		};
	}
}
