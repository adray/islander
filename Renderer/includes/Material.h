#pragma once
#include "Vector3.h"

namespace Islander
{

	enum TextureSource
	{
		TEXTURE_SRC_DEFAULT,
		TEXTURE_SRC_ATLAS,
	};

	class MaterialTexture
	{
	public:
		int _texture;
		int _atlasIndex;
		Vec2f _position;
		Vec2f _scale;
		TextureSource _source;
		Vec2f _originalScale;
		bool _flip;

		MaterialTexture() : _texture(-1), _source(TEXTURE_SRC_DEFAULT), _atlasIndex(-1), _flip(false) {}
	};

	class SubMaterial
	{
	public:
		MaterialTexture _texture[4];
	};

	class Material
	{
	public:
		Material(void);
		~Material(void);

		MaterialTexture _texture[4];

		int pixelshader;
		int vertexshader;

		float threshold;
		float clipping;

		int lightmap;
		int _pass;

		SubMaterial _submaterials[16];
		int _submaterialcount;
		int _submaterialtype;
	};

}
