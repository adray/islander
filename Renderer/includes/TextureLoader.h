#pragma once
#include <string>

namespace Islander
{
	namespace Renderer
	{
		enum TextureLoaderFlags
		{
			TEXTURE_LOADER_NONE = 0,
			TEXTURE_LOADER_PNG = 1,
			TEXTURE_LOADER_FLIP_Y = 64,
		};

		struct TextureLoadInfo
		{
			int _size;
			int _width;
			int _height;
		};

		unsigned char* LoadTexture(const char* filename, int flags, TextureLoadInfo& info);
        unsigned char* LoadTexture(char* data, int flags, TextureLoadInfo& info);
	}
}
