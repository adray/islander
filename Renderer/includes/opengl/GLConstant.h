#pragma once
#include "Renderer.h"

namespace Islander
{
	namespace Renderer
	{
		struct GLConstant
		{
			int32_t _format;
			int _offset;
			int _index;

			GLConstant(int32_t format, int offset, int index) :
				_format(format),
				_offset(offset),
				_index(index)
			{}
		};
	}
}
