#pragma once
#include <string>
#include <vector>

namespace Islander
{
	namespace Renderer
	{		
		struct GLShaderTag
		{
			std::string _type;
			std::string _variable;
			std::string _binding;
			std::string _attribute;
		};

		struct GLShaderUniform
		{
			std::string _name;
			std::string _type;
			int _pos;
		};

		struct GLShaderInput
		{
			std::string _name;
			std::string _type;
			int _pos;
		};

		class GLShaderParser
		{
		public:
			GLShaderParser(const std::string& input);

			void Parse();

			inline bool Success() const { return _success; }

			inline const std::string& Output() const { return _output; }

			inline const std::vector<GLShaderInput>& Inputs() const { return _inputs; }

			inline const std::vector<GLShaderUniform>& Uniforms() const { return _uniform; }

			inline const std::vector<GLShaderTag>& Tags() const { return _tags; }

		private:
			
			enum GLShaderToken
			{
				GL_SHADER_TOKEN_TAG_START,
				GL_SHADER_TOKEN_TAG_END,
				GL_SHADER_TOKEN_STRING,
				GL_SHADER_TOKEN_SQUARE_BRACKET_START,
				GL_SHADER_TOKEN_SQUARE_BRACKET_END,
				GL_SHADER_TOKEN_BRACKET_START,
				GL_SHADER_TOKEN_BRACKET_END,
				GL_SHADER_TOKEN_BRACES_START,
				GL_SHADER_TOKEN_BRACES_END,
				GL_SHADER_TOKEN_UNIFORM,
				GL_SHADER_TOKEN_SEMI_COLON,
				GL_SHADER_TOKEN_IN,
				GL_SHADER_TOKEN_EQUALS,
				GL_SHADER_TOKEN_COLON
			};

			struct ShaderToken
			{
				GLShaderToken _token;
				std::string _item;
			};

			bool ParseTag(int& pos);
			bool ParseUniform(int& pos);
			bool ParseInput(int& pos);
			GLShaderToken GetToken(const std::string& item);

			bool _success;
			std::string _input;
			std::string _output;
			std::vector<ShaderToken> _tokens;
			std::vector<GLShaderTag> _tags;
			std::vector<GLShaderUniform> _uniform;
			std::vector<GLShaderInput> _inputs;
		};
	}
}
