#pragma once
#include "Renderer.h"
#include "GLConstant.h"
#include "GLShaderParser.h"
#include <vector>
#include <GL/glew.h>
#include <GL/gl.h>
#include <GL/glu.h>

namespace Islander
{
	namespace Renderer
	{
		class GLVertexBuffer;
		class GLConstantBuffer;
		class GLVertexShader;
		class GLPixelShader;

		class GPUProgramLayout
		{
		public:
			GPUProgramLayout(const std::vector<GLShaderInput>& inputs, const std::vector<GLShaderTag>& tags, const std::vector<GLShaderUniform>& uniforms, SEMANTIC* semantics, int count);

			inline std::vector<Attribute>& GetAttributes() { return _attributes; }

			inline SEMANTIC GetSemantic(int index)
                        {
                                if (index < 0 || index >= _count) throw;
                                return _semantics[index];
                        }

			inline const std::vector<GLShaderInput>& Inputs() const { return _inputs; }

			inline const std::vector<GLShaderUniform>& Uniforms() const { return _uniforms; }

			inline const std::vector<GLShaderTag>& Tags() const { return _tags; }

			inline int GetCount() { return _count; }

			~GPUProgramLayout();

		private:
			std::vector<GLShaderTag> _tags;
			std::vector<GLShaderUniform> _uniforms;
			std::vector<GLShaderInput> _inputs;
			std::vector<Attribute> _attributes;
			int _count;
			SEMANTIC* _semantics;
		};

		class GPUProgram
		{
		public:
			GPUProgram(GLVertexShader* vertexShader, GLPixelShader* fragmentShader);

			void Bind(GLVertexBuffer* buffer, GLConstantBuffer* constant);
			void Bind(GLVertexBuffer** buffer, int count);
			GLint GetUniformLocation(const char* uniform);
			GLint GetAttributeLocation(const char* attribute);

			GLuint GetSampler(int index);
			void Validate();

		private:
			void Link(GPUProgramLayout* vertexLayout, GPUProgramLayout* pixelLayout);

			GLuint _program;
			GLuint _vao;
			GLint _posAttrib;
			GPUProgramLayout* _layout;

			std::vector<GLuint> _samplers;
			std::vector<GLConstant> _constants;
		};
	}
}
