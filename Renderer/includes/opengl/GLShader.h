#pragma once
#include "GraphicsShader.h"
#include <vector>
#include <string>
#include <GL/glew.h>
#include <GL/gl.h>
#include <GL/glu.h>

namespace Islander
{
	namespace Renderer
	{
		class GPUProgramLayout;

		class GLVertexShader : public GraphicsShader
		{
		public:

			GLVertexShader(const char* source, GPUProgramLayout* layout);

			void Bind(GLuint program);

			void Release();

			inline bool HasErrors() const { return _errors.size() > 0; }

			inline GLuint GetHandle() const { return _handle; }

			inline GPUProgramLayout* GetLayout() { return _layout; }

			void GetErrors(std::vector<std::string>& errors);

			~GLVertexShader();

		private:
			GLuint _handle;
			std::vector<std::string> _errors;
			GPUProgramLayout* _layout;
		};

		class GLPixelShader : public GraphicsShader
		{
		public:

			GLPixelShader(const char* source, GPUProgramLayout* layout);

			void Bind(GLuint program);

			void Release();

			inline bool HasErrors() const { return _errors.size() > 0; }

			inline GLuint GetHandle() const { return _handle; }
			
			inline GPUProgramLayout* GetLayout() { return _layout; }

			void GetErrors(std::vector<std::string>& errors);

			~GLPixelShader();

		private:
			GLuint _handle;
			GPUProgramLayout* _layout;
			std::vector<std::string> _errors;
		};
	}
}
