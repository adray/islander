#pragma once
#include "Renderer.h"
#include "Collections.h"
#include <unordered_map>
#include <unordered_set>

namespace Islander
{
	struct WINDOW;

	namespace Renderer
	{
		class FontDescription;
		class GLVertexBuffer;
		class GLVertexShader;
		class GLPixelShader;
		class GLConstantBuffer;
		class IBuffer;
		class OpenGLContext;
		class GPUProgram;
		class GPUProgramLayout;

		class GLRenderer : public IRenderer
		{
		public:

			void Init(WINDOW* _window, FontDescription& font);

			void GetStats(IslanderRendererStats& stats);

			IBuffer* CreateVertexBuffer(Usage flags, int sizeInBytes, void* data, int stride, GraphicsBuffering buffering);

			IBuffer* CreateConstantBuffer(Usage flags, int sizeInBytes, void* data);

            IBuffer* CreateIndexBuffer(Usage flags, int sizeInBytes, void* data);

			IBuffer* CreateStructuredBuffer(Usage flags, int sizeInBytes, int stride, void* data);

			void RecycleBuffer(IBuffer* buffer);

			void FreeVertexBuffer(int handle);

			void FreeConstantBuffer(int handle);

            void FreeIndexBuffer(int handle);

			void FreeStructuredBuffer(int handle);

			void SubmitDraw(const RenderOperation& op);

			void SubmitDrawInstanced(const InstancedRenderOperation& op, int count);

			void DispatchCompute(const ComputeOperation& op);

			void BeginLightPass();

			void BeginPass(const PASS_CONFIG& config, int pass, int render_flags);

			void Release();

			int LoadPixelShader(const char* filename, const char* method, IslanderShaderDefineList* _defines);

			int LoadVertexShader(const char* filename, const char* method, SEMANTIC * semantics, int count, IslanderShaderDefineList* defines);

			int LoadGeometryShader(const char* filename, const char* method, IslanderShaderDefineList* defines);

			int LoadComputeShader(const char* filename, const char* method, IslanderShaderDefineList* _defines);

			void SetPixelShaderSamplers(int pixelShader, int numSamplers, IslanderTextureSampler* samplers);

			void SetVertexShaderSamplers(int vertexShader, int numSamplers, IslanderTextureSampler* samplers);

			int LoadTexture(const char* filename);

			int LoadTexture(void* data, int width, int height, int size);

			int LoadTexture(int texture, void* data, int width, int height, int size);

			void CopyToTexture(int texture, void* data, int width, int height, int offsetX, int offsetY);

            void ClearTexture(int texture, int width, int height, int offsetX, int offsetY);

			int CreateEmptyTexture(int texture, int width, int height, int type);

			void CreateComputeTexture(int texture);

			int CreateTextureArray(int numTextures, int* textures_, int width, int height);

			int CreateCubeMapTexture(int* textureFaces, int width, int height);

			int ReserveTexture();

			void ResizeBackBuffer();

			void ReleaseRenderTarget(int handle);

			IslanderRenderTarget CreateRenderTarget(RenderTargetType type, uint32_t width, uint32_t height);

            void ResizeRenderTarget(IslanderRenderTarget target, uint32_t width, uint32_t height);

			IslanderRenderTargetArray CreateRenderTargetArray(RenderTargetType type, uint32_t width, uint32_t height, uint32_t dimensions);

			int CreateTextureAtlas(int sourceTexture, MaterialTexture* set, int count);

			void Present();

			void SetFullscreen(bool mode);

			void BindQuad(float* stream, const QuadBindingContext& context);

			void BindLine(float* stream, const LineBindingContext& context);

			void BindText(float* stream, const TextBindingContext& context, const std::unordered_map<std::string, Font*>& engine);

			inline int32_t RendererType() const { return ISLANDER_RENDERER_TYPE_GL; }

			char* GetBytesFromTexture(int texture, int* width, int* height, int* size);

            bool GetTextureList(TextureListData& list);

            void GetDisplayResolutions(DisplayResolutions& list);

			bool RequiresFlipping(int id) const;

			void InitializeImgui();

			void NewFrameImgui();

			void RenderImgui();

			void ImageImgui(int texture, float width, float height);

			void ImageImgui(int texture, float x, float y, float width, float height);

			bool ImageButtonImgui(int id, int texture, float x, float y, float sx, float sy);

			void SetSyncInterval(int syncInterval);

            ParticleRenderer* GetParticleRenderer();

            void BeginDrawParticles(Camera3D* camera);
            
            void EndDrawParticles();

            void InitializePerformanceCounters();
            
            void BeginGPUProfilingFrame();

            int EndGPUProfilingFrame();

            void BeginGPUProfiling();

            int EndGPUProfiling();

            bool GetGPUProfileFrameData(int handle, IslanderGPUProfileFrameData& data);

            bool GetGPUProfileData(int handle, IslanderGPUProfileData& data);

		private:

			GPUProgram* GetGPUProgram(GLVertexShader* vertexShader, GLPixelShader* fragmentShader);

			WINDOW* window;
			OpenGLContext* _context;
			
			HandleCollection<GLPixelShader*> pixelshaders;
			HandleCollection<GLVertexShader*> vertexshaders;
			HandleCollection<int> textures;
			std::unordered_set<int> flip_textures;
			std::unordered_map<int, GPUProgram*> _programs;

			KeyedAllocationBuffer<GLVertexBuffer*> _vertexbuffers;
			KeyedAllocationBuffer<GLConstantBuffer*> _uniformbuffers;
		};

	}
}
