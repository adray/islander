#pragma once
#include "GraphicsBuffer.h"
#include "GLConstant.h"
#include <GL/glew.h>
#include <GL/gl.h>
#include <GL/glu.h>

namespace Islander
{
	namespace Renderer
	{
		class GPUProgramLayout;

		class GLVertexBuffer : public IBuffer
		{
		public:
			
			GLVertexBuffer(int stride, GraphicsBuffering buffering);

			void Allocate(int sizeInBytes, void* data, Usage usage);

			void Fill(int offset, void* data);

			void Bind(GPUProgramLayout* layout);

			void Release();

			int ID() const { return _id; }

			int Size() const { return _sizeInBytes; }

			void SetID(int id) { _id = id; }

			int GetStride() const { return _stride; }

			Usage GetUsage() const { return _usage; }

			void Swap();

			virtual ~GLVertexBuffer();

		private:
			GraphicsBuffering _buffering;
			int _index;
			GLuint _buffer[2];
			int _id;
			int _stride;
			Usage _usage;
			int _sizeInBytes;
		};

		class GLConstantBuffer : public IBuffer
		{
		public:

			void Allocate(int sizeInBytes, void* data, Usage usage);

			void Fill(int offset, void* data);

			void Bind(const std::vector<GLConstant>& constants);

			void Release();

			int ID() const { return _id; }

			int Size() const { return _sizeInBytes; }

			void SetID(int id) { _id = id; }

			Usage GetUsage() const { return _usage; }

			virtual ~GLConstantBuffer();

		private:
			float* _buffer;
			int _id;
			Usage _usage;
			int _sizeInBytes;
		};

	}
}
