#pragma once
#include "Renderer.h"

namespace Islander
{
	namespace Renderer
	{
		class IBuffer
		{
		public:

			virtual void Allocate(int sizeInBytes, void* data, Usage usage) = 0;

			virtual void Fill(int offset, void* data) = 0;

			virtual void Release() = 0;

			virtual int ID() const = 0;

			virtual ~IBuffer()
			{
			}
		};

	}
}

