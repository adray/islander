#pragma once
#include <unordered_map>
#include <queue>
#include <vector>
#include <algorithm>
#include <assert.h>

namespace Islander
{
	namespace Renderer
	{
		// Collection where each item is interchangeable.
		template <typename T>
		class AllocationBuffer
		{
		public:

			bool Allocate(T& val)
			{
				if (!_unused.empty())
				{
					val = _unused.front();
					_unused.pop();
					_used.push_back(val);
					return true;
				}
				return false;
			}

			void AllocateNew(const T& val, bool used)
			{
				if (used)
				{
					_used.push_back(val);
				}
				else
				{
					_unused.push(val);
				}
			}

			void Release(const T& val)
			{
				auto it = std::find(_used.begin(), _used.end(), val);
				if (it != _used.end())
				{
					_used.erase(it);
					_unused.push(val);
				}
			}

			void Remove(const T& val)
			{
				auto it = std::find(_used.begin(), _used.end(), val);
				if (it != _used.end())
				{
					_used.erase(it);
				}
				else
				{
					// Could be in unused
					throw;
				}
			}

		private:
			std::vector<T> _used;
			std::queue<T> _unused;
		};
		
		// Collection where the key is generated as a unique value. 
		template<typename T>
		class HandleCollection
		{
		public:

			int Add(const T& val)
			{
				int id = 0;
				if (_free.size() == 0)
				{
					id = _array.size();
					_array.push_back(val);
				}
				else
				{
					id = _free.back();
					_free.pop_back();
					_array[id] = val;
				}

				return id;
			}

			void Replace(int handle, const T& val)
			{
				assert(handle < _array.size());
				_array[handle] = val;
			}

			int Reserve()
			{
				_array.push_back(T());
				return _array.size() - 1;
			}
			
			void Remove(int handle)
			{
				assert(handle < _array.size());
				_array[handle] = T();
				_free.push_back(handle);
			}

			T& Fetch(int handle)
			{
				return _array[handle];
			}

            int GetCount()
            {
                return _array.size();
            }

			int NumAllocated()
			{
				return _array.size() - _free.size();
			}

		private:
			std::vector<T> _array;
			std::deque<int> _free;
		};
		
		// Collection where each item with the same key is interchangeable.
		template<typename T>
		class KeyedAllocationBuffer
		{
		public:

			bool TryAllocate(int key, T& val)
			{
				auto it = _buffer.find(key);
				if (it != _buffer.end())
				{
					auto buffer = _buffer.at(key);
					if (buffer->Allocate(val))
					{		
						return true;
					}
				}

				return false;
			}

			int AllocateNew(int key, const T& val)
			{
				auto it = _buffer.find(key);
				if (it != _buffer.end())
				{
					auto buffer = _buffer.at(key);
					buffer->AllocateNew(val, true);
					return _handles.Add(val);
				}

				auto allocation = new AllocationBuffer<T>();
				_buffer.insert(std::make_pair(key, allocation));
				allocation->AllocateNew(val, true);
				return _handles.Add(val);
			}

			void Release(int key, int handle)
			{
				auto val = _handles.Fetch(handle);
				auto it = _buffer.find(key);
				if (it != _buffer.end())
				{
					auto buffer = _buffer.at(key);
					buffer->Release(val);
				}
			}

			void Remove(int key, int handle)
			{
				auto val = _handles.Fetch(handle);
				auto it = _buffer.find(key);
				if (it != _buffer.end())
				{
					auto buffer = _buffer.at(key);
					buffer->Remove(val);
					_handles.Remove(handle);
				}
			}

			int Count()
			{
				return _handles.NumAllocated();
			}

			T Fetch(int handle)
			{
				return _handles.Fetch(handle);
			}
			
		private:
			std::unordered_map<int, AllocationBuffer<T>*> _buffer;
			HandleCollection<T> _handles;
		};
	}
}
