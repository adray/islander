#include "Quad.h"
#include "Renderer.h"
#include "GraphicsBuffer.h"

using namespace Islander;
using namespace Islander::Renderer;

Quad::Quad(void)
	:
	buffer(-1)
{
	data = new float[24];
	for (int i = 0; i < 1; i++)
	{
		data[0+24*i] = -1;
		data[1+24*i] = 1;
		data[2+24*i] = 0;
		data[3+24*i] = 0;

		data[4+24*i] = 1;
		data[5+24*i] = 1;
		data[6+24*i] = 1;
		data[7+24*i] = 0;

		data[8+24*i] = -1;
		data[9+24*i] = -1;
		data[10+24*i] = 0;
		data[11+24*i] = 1;
	
	
		data[12+24*i] = -1;
		data[13+24*i] = -1;
		data[14+24*i] = 0;
		data[15+24*i] = 1;

		data[16+24*i] = 1;
		data[17+24*i] = 1;
		data[18+24*i] = 1;
		data[19+24*i] = 0;

		data[20+24*i] = 1;
		data[21+24*i] = -1;
		data[22+24*i] = 1;
		data[23+24*i] = 1;
	}
}

Quad::~Quad(void)
{
	delete[] data;
	data = nullptr;
}

int Quad::UpdateBuffer(IRenderer * renderer)
{
	if (buffer >= 0)
		return 0;

	buffer = renderer->CreateVertexBuffer(USAGE_STATIC, sizeof(float) * 24, data, sizeof(float) * 4, BUFFERING_SINGLE)->ID();

	return 0;
}

Line::Line()
{
	data = new float[2];
	for (int i = 0; i < 1; i++)
	{
		data[0+2*i] = 0;
		data[1+2*i] =1;
	}

	buffer = -1;
}

Line::~Line()
{
	delete[] data;
	data = nullptr;
}

int Line::UpdateBuffer(IRenderer * renderer)
{
	if (buffer >= 0)
		return 0;

	buffer = renderer->CreateVertexBuffer(USAGE_STATIC, sizeof(float) * 2, data, sizeof(float), BUFFERING_SINGLE)->ID();

	return 0;
}
