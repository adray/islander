#include "TextTokenizer.h"

using namespace Islander;
using namespace Islander::Renderer;

void TextTokenizer::GetTokens(const std::string& text, std::vector<Token>& tokens)
{
    Token token;
	int index = 0;
	while (index < text.length())
	{
		std::string word;
        int startIndex = index;
		auto t = GetNextSymbol(text, &index);

		switch (t)
		{
		case SYMBOL_TYPE_NONE:
			// what?
			break;
		case SYMBOL_TYPE_TEXT:
		{
			word += text[index - 1];
			t = GetNextSymbol(text, &index);
			while (t == SYMBOL_TYPE_TEXT)
			{
				word += text[index - 1];
				t = GetNextSymbol(text, &index);
			}

			if (t != SYMBOL_TYPE_EOF)
			{
				index--;
			}

			token._type = TOKEN_TYPE_TEXT;
		}
		break;
		case SYMBOL_TYPE_SPACE:
			token._type = TOKEN_TYPE_SPACE;
			break;
		case SYMBOL_TYPE_ANGLE_LEFT:
		{
			int temp = index;
			bool success = false;
			auto next = GetNextSymbol(text, &temp);
			if (next == SYMBOL_TYPE_TEXT &&
				text[temp - 1] == 'b')
			{
				if (GetNextSymbol(text, &temp) == SYMBOL_TYPE_ANGLE_RIGHT)
				{
					success = true;
					word = "<b>";
					index = temp;
					token._type = TOKEN_TYPE_BOLD_BEGIN;
				}
			}
            else if (next == SYMBOL_TYPE_TEXT &&
                text[temp - 1] == 'l')
            {
                if (GetNextSymbol(text, &temp) == SYMBOL_TYPE_ANGLE_RIGHT)
                {
                    success = true;
                    word = "<l>";
                    index = temp;
                    token._type = TOKEN_TYPE_LINK_BEGIN;
                }
            }
            else if (next == SYMBOL_TYPE_TEXT &&
                text[temp - 1] == 's')
            {
                if (GetNextSymbol(text, &temp) == SYMBOL_TYPE_ANGLE_RIGHT)
                {
                    success = true;
                    word = "<s>";
                    index = temp;
                    token._type = TOKEN_TYPE_STRIKE_BEGIN;
                }
            }
			else if (next == SYMBOL_TYPE_FORWARD_SLASH)
			{
				next = GetNextSymbol(text, &temp);
				if (next == SYMBOL_TYPE_TEXT &&
					text[temp - 1] == 'b')
				{
					if (GetNextSymbol(text, &temp) == SYMBOL_TYPE_ANGLE_RIGHT)
					{
						success = true;
						word = "</b>";
						index = temp;
						token._type = TOKEN_TYPE_BOLD_END;
					}
				}
                else if (next == SYMBOL_TYPE_TEXT &&
                    text[temp - 1] == 'l')
                {
                    if (GetNextSymbol(text, &temp) == SYMBOL_TYPE_ANGLE_RIGHT)
                    {
                        success = true;
                        word = "</l>";
                        index = temp;
                        token._type = TOKEN_TYPE_LINK_END;
                    }
                }
                else if (next == SYMBOL_TYPE_TEXT &&
                    text[temp - 1] == 's')
                {
                    if (GetNextSymbol(text, &temp) == SYMBOL_TYPE_ANGLE_RIGHT)
                    {
                        success = true;
                        word = "</s>";
                        index = temp;
                        token._type = TOKEN_TYPE_STRIKE_END;
                    }
                }
			}

			if (!success)
			{
				word = "<";
				token._type = TOKEN_TYPE_TEXT;
			}
		}
		break;
		case SYMBOL_TYPE_ANGLE_RIGHT:
			word = ">";
			token._type = TOKEN_TYPE_TEXT;
			break;
		case SYMBOL_TYPE_NEWLINE:
			token._type = TOKEN_TYPE_NEWLINE;
			break;
		case SYMBOL_TYPE_FORWARD_SLASH:
			word = "/";
			token._type = TOKEN_TYPE_TEXT;
			break;
		case SYMBOL_TYPE_TAB:
			token._type = TOKEN_TYPE_TAB;
			break;
		default:
			break;
		}

		token._text = word;
        token._index = startIndex;

		tokens.push_back(token);
	}
}

TextTokenizer::SymbolType TextTokenizer::GetNextSymbol(const std::string& text, int* i)
{
	if (*i >= text.length())
	{
		return SYMBOL_TYPE_EOF;
	}

	char c = text[*i];
	++*i;

	if (c == ' ')
	{
		return SYMBOL_TYPE_SPACE;
	}
	else if (c == '\n')
	{
		return SYMBOL_TYPE_NEWLINE;
	}
	else if (c == '\t')
	{
		return SYMBOL_TYPE_TAB;
	}
	else if (c == '/')
	{
		return SYMBOL_TYPE_FORWARD_SLASH;
	}
	else if (c == '<')
	{
		return SYMBOL_TYPE_ANGLE_LEFT;
	}
	else if (c == '>')
	{
		return SYMBOL_TYPE_ANGLE_RIGHT;
	}
	else if (c >= 0 && c < 200)
	{
		return SYMBOL_TYPE_TEXT;
	}

	return SYMBOL_TYPE_NONE;
}
