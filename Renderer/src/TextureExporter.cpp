#include "TextureExporter.h"
#include <fstream>
#include <png.h>

namespace Islander
{
    namespace Renderer
    {
        void write_data(png_structp png_ptr, png_bytep data, png_size_t length)
        {
            std::ofstream* stream = (std::ofstream*)png_get_io_ptr(png_ptr);

            stream->write((char*)data, length);
        }

        int ExportTexture(const std::string& path, unsigned char* data, int width, int height, int size)
        {
            png_structp png = png_create_write_struct(PNG_LIBPNG_VER_STRING, (png_voidp)nullptr, nullptr, nullptr);

            if (!png)
            {
                return 0;
            }

            png_infop info_ptr = png_create_info_struct(png);

            if (!info_ptr)
            {
                png_destroy_write_struct(&png, (png_infopp)nullptr);
                return 0;
            }

            std::ofstream file;
            file.open(path.c_str(), std::ios_base::binary);
            if (file.good())
            {
                png_set_write_fn(png, &file, write_data, nullptr);

                png_set_IHDR(png, info_ptr, width, height, 8/*RGBA*/, 6/*RGBA*/, PNG_INTERLACE_ADAM7, PNG_COMPRESSION_TYPE_DEFAULT, PNG_FILTER_TYPE_DEFAULT);

                const int row_size = width;// (size / 4) / height;

                png_byte** row_pointers = new png_byte * [height];
                for (int i = 0; i < height; i++)
                {
                    row_pointers[i] = &data[row_size * i * 4];
                }

                png_set_rows(png, info_ptr, row_pointers);
                png_write_png(png, info_ptr, PNG_TRANSFORM_IDENTITY, nullptr);

                png_write_end(png, info_ptr);

                delete[] row_pointers;
            }

            png_destroy_write_struct(&png, (png_infopp)nullptr);
            return 1;
        }

    }
}
