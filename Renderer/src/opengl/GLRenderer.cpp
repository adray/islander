#include "opengl/GLRenderer.h"
#include "Window.h"
#include "Component.h"
#include "opengl/GLBuffer.h"
#include "opengl/GLShader.h"
#include "opengl/GLGPUProgram.h"
#include "OpenGL.h"
#include "TextureLoader.h"
#include "Logger.h"
#include "opengl/GLShaderParser.h"

#include <fstream>
#include <sstream>
#include <GL/glew.h>
#include <GL/gl.h>
#include <GL/glu.h>
extern "C"
{
#include <png.h>
#include <entity.h>
}
#include <locale>
#include <codecvt>

using namespace Islander;
using namespace Islander::Renderer;


void GLRenderer::Init(WINDOW* _window, FontDescription& font)
{
	window = _window;

	_context = new OpenGLContext();
	_context->InitializeGL(window);
}

void GLRenderer::GetStats(IslanderRendererStats& stats)
{
	// TODO
	stats.numConstantBuffers = 0;
	stats.numIndexBuffers = 0;
	stats.numVertexBuffers = 0;
}

void GLRenderer::Release()
{
	_context->DeactivateGL(window);
}

static bool PushBinding(property_entry* entry, Attribute& attribute, int offset, float* stream)
{
	if (attribute.GetFormat() == ISLANDER_SEMANTIC_FLOAT4)
	{
		stream[offset] = entry->data[0];
		stream[offset + 1] = entry->data[1];
		stream[offset + 2] = entry->data[2];
		stream[offset + 3] = entry->data[3];
		return true;
	}
	else if (attribute.GetFormat() == ISLANDER_SEMANTIC_FLOAT3)
	{
		stream[offset] = entry->data[0];
		stream[offset + 1] = entry->data[1];
		stream[offset + 2] = entry->data[2];
		return true;
	}
	else if (attribute.GetFormat() == ISLANDER_SEMANTIC_FLOAT2)
	{
		stream[offset] = entry->data[0];
		stream[offset + 1] = entry->data[1];
		return true;
	}
	else if (attribute.GetFormat() == ISLANDER_SEMANTIC_FLOAT)
	{
		stream[offset] = entry->data[0];
		return true;
	}
	return false;
}

static void Bind(row_entity* entities, Attribute& attribute, int offset, float* stream)
{
	for (int i = 0; i < entities->count; ++i)
	{
		if (entities->entries[i].key == attribute.GetPropertyType())
		{
			PushBinding(&entities->entries[i], attribute, offset, stream);
		}
	}
}

void GLRenderer::BindLine(float* stream, const LineBindingContext& context)
{
	// TODO: Implement
}

void GLRenderer::BindText(float* stream, const TextBindingContext& context, const std::unordered_map<std::string, Font*>& engine)
{
	auto vertexShader = vertexshaders.Fetch(context.vertex);
	auto pixelShader = pixelshaders.Fetch(context.sprite->pixel);
	GetGPUProgram(vertexShader, pixelShader); //ensure gpu program links attributes
	for (auto& attribute : vertexShader->GetLayout()->GetAttributes())
	{
		if (attribute.GetOffset() == -1)
			continue;

		switch (attribute.GetType())
		{
		case ATTRIBUTE_CLASS_CUSTOM:
			{
				Bind(context.properties, attribute, attribute.GetOffset(), stream);
			}
			break;
		case ATTRIBUTE_CLASS_TRANSFORM:
			{
				auto data = (TextData*)context.sprite->data;
				int index = attribute.GetOffset();
				stream[index] = ((data->centered ? ((float)-data->_rect.width) : -context.sx) + data->_rect.x + context.x + window->Width()) / (window->Width()) - 1;
				stream[index + 1] = 1 - ((data->_rect.y - context.y + window->Height() - context.sy) / (window->Height()));
				stream[index + 2] = context.z;
				stream[index + 3] = 1;
			}
			break;
		case ATTRIBUTE_CLASS_FONT_COLOUR:
			{
				int index = attribute.GetOffset();
				stream[index] = context.sprite->_colour.r;
				stream[index + 1] = context.sprite->_colour.g;
				stream[index + 2] = context.sprite->_colour.b;
				stream[index + 3] = context.sprite->_colour.a;
			}
			break;
		}
	}
}

void GLRenderer::BindQuad(float* stream, const QuadBindingContext& context)
{
	auto vertexShader = vertexshaders.Fetch(context.mat->vertexShader);
	auto pixelShader = pixelshaders.Fetch(context.mat->pixelShader);
	GetGPUProgram(vertexShader, pixelShader); //ensure gpu program links attributes
	for (auto& attribute : vertexShader->GetLayout()->GetAttributes())
	{
		if (attribute.GetOffset() == -1)
			continue;

		if (attribute.GetType() == ATTRIBUTE_CLASS_CUSTOM)
		{
			Bind(context.properties, attribute, attribute.GetOffset(), stream);
		}
		else if (attribute.GetType() == ATTRIBUTE_CLASS_TRANSFORM)
		{
			int index = attribute.GetOffset();
			stream[index] = context.x - context.camera->GetX();
			stream[index + 1] = context.y - context.camera->GetY();
			stream[index + 2] = context.sx / window->Width();
			stream[index + 3] = context.sy / window->Height();
		}
		else if (attribute.GetType() >= ATTRIBUTE_CLASS_TEXTURE0 && attribute.GetType() <= ATTRIBUTE_CLASS_TEXTURE3)
		{
			int index = attribute.GetType() - ATTRIBUTE_CLASS_TEXTURE0;
			int offset = attribute.GetOffset();
			stream[offset] = context.mat->textures[index].px;
			stream[offset + 1] = context.mat->textures[index].py;
			stream[offset + 2] = context.mat->textures[index].sx;
			stream[offset + 3] = context.mat->textures[index].sy;
		}
		//else if (attribute.GetType() == ATTRIBUTE_CLASS_MESH_TRANSFORM)
		//{
		//	int index = attribute.GetOffset();
		//	stream[index] = context.camera->GetZ() * (context.x - context.camera->GetX());
		//	stream[index + 1] = context.camera->GetZ() * (context.y - context.camera->GetY());
		//}
		else if (attribute.GetType() == ATTRIBUTE_CLASS_WINDOW_WIDTH)
		{
			int index = attribute.GetOffset();
			stream[index] = 1 / context.camera->GetZ() * (window->Width());
		}
		else if (attribute.GetType() == ATTRIBUTE_CLASS_WINDOW_HEIGHT)
		{
			int index = attribute.GetOffset();
			stream[index] = 1 / context.camera->GetZ() * (window->Height());
		}
	}
}

int textureBindings[4] = { GL_TEXTURE0, GL_TEXTURE1, GL_TEXTURE2, GL_TEXTURE3 };

void BindTexture(int index, GLuint texture)
{
	glActiveTexture(textureBindings[index]);
	glBindTexture(GL_TEXTURE_2D, texture);
	int error = glGetError();
}

void GLRenderer::BeginPass(const PASS_CONFIG& config, int pass, int render_flags)
{
	// TODO: support multiple render targets
	if (config._renderTarget[0] == -1)
	{
		glBindFramebuffer(GL_FRAMEBUFFER, 0);
	}
	else
	{
		glBindFramebuffer(GL_FRAMEBUFFER, config._renderTarget[0]);
	}

	if ((config._flags & ISLANDER_RENDER_FLAGS_CLEAR) == ISLANDER_RENDER_FLAGS_CLEAR)
	{
		glClearColor(1.0f, 0.0f, 0.0f, 1.0f);
		//glClearDepth(0);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		glEnable(GL_DEPTH_TEST);
		//glDepthRange(1.0f, 0.0f);
		glDepthFunc(GL_LEQUAL);
	}
	else if ((config._flags & ISLANDER_RENDER_FLAGS_CLEAR_TRANSPARENT) == ISLANDER_RENDER_FLAGS_CLEAR_TRANSPARENT)
	{
		glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
		//glClearDepth(0);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		glEnable(GL_DEPTH_TEST);
		glDepthFunc(GL_LEQUAL);
		//glDepthRange(1.0f, 0.0f);
	}
	else
	{
		//glClearDepth(0);
		glClear(GL_DEPTH_BUFFER_BIT);
		glDepthFunc(GL_LEQUAL);
		//glDepthRange(1.0f, 0.0f);
	}

	// TODO
	glViewport(0, 0, window->Width(), window->Height());

	if ((config._flags & ISLANDER_RENDER_FLAGS_BLEND_ADD) == ISLANDER_RENDER_FLAGS_BLEND_ADD)
	{
		glBlendEquationSeparate(GL_FUNC_ADD, GL_FUNC_ADD);
		glBlendFuncSeparate(GL_ONE, GL_ONE,
			            GL_ONE, GL_ONE_MINUS_SRC_ALPHA);
	}
	else
	{
		glBlendEquationSeparate(GL_FUNC_ADD, GL_FUNC_ADD);
	        glBlendFuncSeparate(GL_ONE, GL_ONE_MINUS_SRC_ALPHA,
				    GL_ONE, GL_ONE_MINUS_SRC_ALPHA);
	}
}

void GLRenderer::BeginLightPass()
{

}

GPUProgram* GLRenderer::GetGPUProgram(GLVertexShader* vertexShader, GLPixelShader* fragmentShader)
{
	int key = vertexShader->GetHandle() | (fragmentShader->GetHandle() << 8);

	GPUProgram* out = nullptr;
	if (_programs.find(key) != _programs.end())
	{
		out = _programs.at(key);
		return out;
	}

	out = new GPUProgram(vertexShader, fragmentShader);

	_programs.insert(std::make_pair(key, out));

	return out;
}

void GLRenderer::SubmitDraw(const RenderOperation& op)
{
	// Setup render states
	glDisable(GL_CULL_FACE);

	glEnable(GL_BLEND);
	
	// Bind shader program
	auto pixelshader = pixelshaders.Fetch(op.GetPixelShader());
	auto vertexshader = vertexshaders.Fetch(op.GetVertexShader());

	auto vertexbuffer = _vertexbuffers.Fetch(op.GetVertexBuffer());
	GLConstantBuffer* constant = nullptr;
	if (op.GetConstantBuffer() != -1)
	{
		constant = _uniformbuffers.Fetch(op.GetConstantBuffer());
	}

	auto program = GetGPUProgram(vertexshader, pixelshader);
	program->Bind(vertexbuffer, constant);

	// Bind textures
	int textureCount = 0;
	for (int i = 0; i < 4; i++)
	{
		if (op.GetTextures(i) != -1)
		{
			GLint tex = program->GetSampler(i);
			if (tex != UINT_MAX)
			{
				glUniform1i(tex, i);
				int texture = textures.Fetch(op.GetTextures(i));
				BindTexture(i, texture);
				textureCount++;
			}
		}
	}

	int maxVertexCount = vertexbuffer->Size() / vertexbuffer->GetStride();
	int vertexCount = op.GetVertexCount();

	int topology = GL_TRIANGLES;
	if (op.GetTopology() == TOPOLOGY_LINELIST)
	{
		topology = GL_LINES;
	}

	glDrawArrays(topology, 0, vertexCount == 0 ? maxVertexCount : vertexCount);
	
	vertexbuffer->Swap();

	// Clear textures
	glBindTexture(GL_TEXTURE_2D, 0);
	
	// Clear vertex array
	glBindVertexArray(0);
}

void GLRenderer::SubmitDrawInstanced(const InstancedRenderOperation& op, int count)
{
	// Setup render states
	glDisable(GL_CULL_FACE);

	glEnable(GL_BLEND);
		
	// Bind shader program
	auto pixelshader = pixelshaders.Fetch(op.GetPixelShader());
	auto vertexshader = vertexshaders.Fetch(op.GetVertexShader());
	
	// Bind vertex buffers
	int vertexCount = 0;
	int bufferCount = 0;
	GLVertexBuffer* buffers[4];
	for (int i = 0; i < 4; i++)
	{
		if (op.GetVertexBuffer(i) != -1)
		{
			auto vertexbuffer = _vertexbuffers.Fetch(op.GetVertexBuffer(i));
			buffers[bufferCount++] = vertexbuffer;

			if (i == 0)
			{
				vertexCount = vertexbuffer->Size() / vertexbuffer->GetStride();
			}
		}
	}

	auto program = GetGPUProgram(vertexshader, pixelshader);
	program->Bind(buffers, bufferCount);

	// Bind textures
	int textureCount = 0;
	for (int i = 0; i < 4; i++)
	{
		if (op.GetTextures(i) != -1)
		{
			GLint tex = program->GetSampler(i);
			if (tex != UINT_MAX)
			{
				glUniform1i(tex, i);
				int texture = textures.Fetch(op.GetTextures(i));
				BindTexture(i, texture);
				textureCount++;
			}
		}
	}

	int topology = GL_TRIANGLES;
	if (op.GetTopology() == TOPOLOGY_LINELIST)
	{
		topology = GL_LINES;
	}

	glDrawArraysInstanced(topology, 0, vertexCount, count);

	for (int i = 0; i < 4; i++)
	{
		if (op.GetVertexBuffer(i) != -1)
		{
			auto vertexbuffer = _vertexbuffers.Fetch(op.GetVertexBuffer(i));
			vertexbuffer->Swap();
		}
	}
}

void GLRenderer::DispatchCompute(const ComputeOperation& op)
{
}

char* GLRenderer::GetBytesFromTexture(int texture, int* width, int* height, int* size)
{
	// TODO: complete
	return 0;
}

int GLRenderer::CreateTextureArray(int numTextures, int* textures_, int width, int height)
{
	return -1;
}

int GLRenderer::CreateCubeMapTexture(int* textureFaces, int width, int height)
{
	return -1;
}

int GLRenderer::ReserveTexture()
{
	return textures.Reserve();
}

int GLRenderer::LoadTexture(const char* filename)
{
	TextureLoadInfo info;
	unsigned char* data = ::LoadTexture(filename, TEXTURE_LOADER_PNG, info);

	if (!data)
	{
		return -1;
	}
	
	GLuint tex;
	glGenTextures(1, &tex);
	
	glBindTexture(GL_TEXTURE_2D, tex);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, info._width, info._height, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);
        	
	glGenerateMipmap(GL_TEXTURE_2D);
	
	glBindTexture(GL_TEXTURE_2D, 0);
	
	delete[] data;

	return textures.Add(tex);
}

int GLRenderer::LoadVertexShader(const char* filename, const char* method, SEMANTIC * semantics, int count, IslanderShaderDefineList* defines)
{
	std::ifstream stream(filename);

	std::string s((std::istreambuf_iterator<char>(stream)), std::istreambuf_iterator<char>());

	stream.close();

	GLShaderParser parser(s);
	parser.Parse();
	if (!parser.Success())
	{
		std::stringstream ss;
		ss << "Error parsing shader " << filename << std::endl;
		Islander::Logger::GetLogger()->Log(ss.str(), LOGGING_RENDERING);
		return -1;
	}

	const std::string& out = parser.Output();
	const char* vertexSource = out.c_str();

	auto shader = new GLVertexShader(vertexSource, new GPUProgramLayout(parser.Inputs(), parser.Tags(), parser.Uniforms(), semantics, count));

	if (shader->HasErrors())
	{
		std::vector<std::string> errors;
		shader->GetErrors(errors);

		std::stringstream ss;
		ss << "Error compiling shader " << filename << std::endl;

		for (auto& error : errors)
		{
			ss << error << std::endl;
		}

		Islander::Logger::GetLogger()->Log(ss.str(), LOGGING_RENDERING);
		delete shader;
		return -1;
	}
	
	return vertexshaders.Add(shader);
}

int GLRenderer::LoadPixelShader(const char* filename, const char* method, IslanderShaderDefineList* _defines)
{
	std::ifstream stream(filename);

	std::string s((std::istreambuf_iterator<char>(stream)), std::istreambuf_iterator<char>());

	stream.close();

	GLShaderParser parser(s);
	parser.Parse();
	if (!parser.Success())
	{
		std::stringstream ss;
		ss << "Error parsing shader " << filename << std::endl;
		Islander::Logger::GetLogger()->Log(ss.str(), LOGGING_RENDERING);
		return -1;
	}

	const std::string& out = parser.Output();
	const char* pixelSource = out.c_str();

	auto shader = new GLPixelShader(pixelSource, new GPUProgramLayout(parser.Inputs(), parser.Tags(), parser.Uniforms(), nullptr, 0));

	if (shader->HasErrors())
	{
		std::vector<std::string> errors;
		shader->GetErrors(errors);

		std::stringstream ss;
		ss << "Error compiling shader " << filename << std::endl;

		for (auto& error : errors)
		{
			ss << error << std::endl;
		}

		Islander::Logger::GetLogger()->Log(ss.str(), LOGGING_RENDERING);
		delete shader;
		return -1;
	}

	return pixelshaders.Add(shader);
}

int GLRenderer::LoadGeometryShader(const char* filename, const char* method, IslanderShaderDefineList* defines)
{
	return -1;
}

int GLRenderer::LoadComputeShader(const char* filename, const char* method, IslanderShaderDefineList* _defines)
{
	return -1;
}

void GLRenderer::SetPixelShaderSamplers(int pixelShader, int numSamplers, IslanderTextureSampler* samplers)
{
	// TODO
}

void GLRenderer::SetVertexShaderSamplers(int vertexShader, int numSamplers, IslanderTextureSampler* samplers)
{
	// TODO
}

IBuffer* GLRenderer::CreateVertexBuffer(Usage flags, int sizeInBytes, void* data, int stride, GraphicsBuffering buffering)
{
	int key = (flags & 0xFF) | (sizeInBytes << 4) | (stride << 20);

	GLVertexBuffer* buffer;
	if (_vertexbuffers.TryAllocate(key, buffer))
	{
		buffer->Fill(0, data);
	}
	else
	{
		buffer = new GLVertexBuffer(stride, buffering);
		buffer->Allocate(sizeInBytes, data, flags);
		int id = _vertexbuffers.AllocateNew(key, buffer);
		buffer->SetID(id);
	}

	return buffer;
}

IBuffer* GLRenderer::CreateConstantBuffer(Usage usage, int sizeInBytes, void* data)
{
	int key = (usage & 0xFF) | (sizeInBytes << 4);

	GLConstantBuffer* buffer;
	if (_uniformbuffers.TryAllocate(key, buffer))
	{
		buffer->Fill(0, data);
	}
	else
	{
		buffer = new GLConstantBuffer();
		buffer->Allocate(sizeInBytes, data, usage);
		int id = _uniformbuffers.AllocateNew(key, buffer);
		buffer->SetID(id);
	}

	return buffer;
}

IBuffer* GLRenderer::CreateIndexBuffer(Usage flags, int sizeInBytes, void* data)
{
    return nullptr;
}

IBuffer* GLRenderer::CreateStructuredBuffer(Usage flags, int sizeInBytes, int stride, void* data)
{
	return nullptr;
}

int GLRenderer::CreateEmptyTexture(int texture, int width, int height, int type)
{
	GLuint tex;
	glGenTextures(1, &tex);

	glBindTexture(GL_TEXTURE_2D, tex);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	std::vector<GLubyte> emptyData(width * height, 0);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_R8, width, height, 0, GL_RED, GL_UNSIGNED_BYTE, &emptyData[0]);

	glBindTexture(GL_TEXTURE_2D, 0);

	textures.Replace(texture, tex);
	return texture;
}

void GLRenderer::CreateComputeTexture(int texture)
{
	// TODO
}

void GLRenderer::ClearTexture(int texture, int width, int height, int offsetX, int offsetY)
{
	auto tex = textures.Fetch(texture);

	glBindTexture(GL_TEXTURE_2D, tex);

	std::vector<GLubyte> emptyData(width * height, 0);
	glTexSubImage2D(GL_TEXTURE_2D, 0, offsetX, offsetY, width, height, GL_RED, GL_UNSIGNED_BYTE, &emptyData[0]);

	glBindTexture(GL_TEXTURE_2D, 0);
}

void GLRenderer::CopyToTexture(int texture, void* data, int width, int height, int offsetX, int offsetY)
{
	auto tex = textures.Fetch(texture);

	glBindTexture(GL_TEXTURE_2D, tex);

	glTexSubImage2D(
		GL_TEXTURE_2D,
		0,
		offsetX,
		offsetY,
		width,
		height,
		GL_RED, // target format
		GL_UNSIGNED_BYTE,
		data
	);

    auto error = glGetError();

	// TODO: we don't want to generate mip-maps from successive calls to this
	glGenerateMipmap(GL_TEXTURE_2D);	
	glBindTexture(GL_TEXTURE_2D, 0);
}

int GLRenderer::LoadTexture(int texture, void* data, int width, int height, int size)
{
	if (!data || size <= 0)
	{
		return -1;
	}

	GLuint tex;
	glGenTextures(1, &tex);

	glBindTexture(GL_TEXTURE_2D, tex);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);

	glGenerateMipmap(GL_TEXTURE_2D);

	glBindTexture(GL_TEXTURE_2D, 0);

	textures.Replace(texture, tex);
	return texture;
}

int GLRenderer::LoadTexture(void* data, int width, int height, int size)
{
	int texture = ReserveTexture();

	return LoadTexture(texture, data, width, height, size);
}

void GLRenderer::FreeVertexBuffer(int handle)
{
	auto vertex = _vertexbuffers.Fetch(handle);
	if (vertex)
	{
		int key = (vertex->GetUsage() & 0xFF) | (vertex->Size() << 4) | (vertex->GetStride() << 20);
		_vertexbuffers.Remove(key, vertex->ID());
		vertex->Release();
	}
	delete vertex;
}

void GLRenderer::FreeConstantBuffer(int handle)
{
	auto constant = _uniformbuffers.Fetch(handle);
	if (constant)
	{
		int key = (constant->GetUsage() & 0xFF) | (constant->Size() << 4);
		_uniformbuffers.Remove(key, constant->ID());
		constant->Release();
	}
	delete constant;
}

void GLRenderer::FreeIndexBuffer(int handle)
{
	// TODO
}

void GLRenderer::FreeStructuredBuffer(int handle)
{
	// TODO
}

void GLRenderer::RecycleBuffer(IBuffer* buffer)
{
	auto vertex = dynamic_cast<GLVertexBuffer*>(buffer);
	if (vertex)
	{
		int key = (vertex->GetUsage() & 0xFF) | (vertex->Size() << 4) | (vertex->GetStride() << 20);
		_vertexbuffers.Release(key, vertex->ID());
	}

	auto constant = dynamic_cast<GLConstantBuffer*>(buffer);
	if (constant)
	{
		int key = (constant->GetUsage() & 0xFF) | (constant->Size() << 4);
		_uniformbuffers.Release(key, constant->ID());
	}
}

void GLRenderer::Present()
{
	_context->Present(window);
}

void GLRenderer::ResizeBackBuffer()
{

}

void GLRenderer::ReleaseRenderTarget(int handle)
{

}

IslanderRenderTarget GLRenderer::CreateRenderTarget(RenderTargetType type, uint32_t width, uint32_t height)
{
	GLuint id = 0;
	glGenFramebuffers(1, &id);
	glBindFramebuffer(GL_FRAMEBUFFER, id);

	GLuint texture = 0;
	glGenTextures(1, &texture);
	glBindTexture(GL_TEXTURE_2D, texture);
	
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, 0);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	
	GLuint depthrenderbuffer;
	glGenRenderbuffers(1, &depthrenderbuffer);
	glBindRenderbuffer(GL_RENDERBUFFER, depthrenderbuffer);
	glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT, width, height);
	glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthrenderbuffer);

	glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, texture, 0);
		
	GLenum buffers[1] = { GL_COLOR_ATTACHMENT0 };
	glDrawBuffers(1, buffers);

	IslanderRenderTarget target;
	if (glCheckFramebufferStatus(GL_FRAMEBUFFER) == GL_FRAMEBUFFER_COMPLETE)
	{
		target._resource = id;
		target._texture = textures.Add(texture);
		flip_textures.insert(target._texture);
	}
	else
	{
		Logger::GetLogger()->Log("Failed to create Framebuffer", LOGGING_RENDERING);
	}

	return target;
}

void GLRenderer::ResizeRenderTarget(IslanderRenderTarget target, uint32_t width, uint32_t height)
{

}

IslanderRenderTargetArray GLRenderer::CreateRenderTargetArray(RenderTargetType type, uint32_t width, uint32_t height, uint32_t dimensions)
{
	IslanderRenderTargetArray target;
	target.count = 0;
	target._resource = nullptr;
	target._texture = -1;

	return target;
}

bool GLRenderer:: RequiresFlipping(int id) const
{
	return flip_textures.find(id) != flip_textures.end();
}

void GLRenderer::SetFullscreen(bool mode)
{
	
}

int GLRenderer::CreateTextureAtlas(int sourceTexture, MaterialTexture* set, int count)
{
	return -1;
}

bool GLRenderer::GetTextureList(TextureListData& list)
{
    int count = textures.GetCount();
    list.texture_count = count;
    for (int i = 0; i < count; i++)
    {
        TextureData* data = &list.textures[i];
        data->id = i;
        // Unknown at this time since they are not stored anywhere
        data->width = 0;
        data->height = 0;
    }

    return true;
}

void GLRenderer::GetDisplayResolutions(DisplayResolutions& list)
{

}

void GLRenderer::InitializeImgui()
{

}

void GLRenderer::NewFrameImgui()
{

}

void GLRenderer::RenderImgui()
{

}

void GLRenderer::ImageImgui(int texture, float width, float height)
{

}

void GLRenderer::ImageImgui(int texture, float x, float y, float width, float height)
{

}

bool GLRenderer::ImageButtonImgui(int id, int texture, float x, float y, float sx, float sy)
{
	return false;
}

void GLRenderer::SetSyncInterval(int syncInterval)
{

}

ParticleRenderer* GLRenderer::GetParticleRenderer()
{
    return nullptr;
}

void GLRenderer::BeginDrawParticles(Camera3D* camera)
{

}

void GLRenderer::EndDrawParticles()
{

}

void GLRenderer::InitializePerformanceCounters()
{

}

void GLRenderer::BeginGPUProfilingFrame()
{
    
}

int GLRenderer::EndGPUProfilingFrame()
{
    return 0;
}

void GLRenderer::BeginGPUProfiling()
{

}

int GLRenderer::EndGPUProfiling()
{
    return 0;
}

bool GLRenderer::GetGPUProfileFrameData(int handle, IslanderGPUProfileFrameData& data)
{
    return false;
}

bool GLRenderer::GetGPUProfileData(int handle, IslanderGPUProfileData& data)
{
    return false;
}
