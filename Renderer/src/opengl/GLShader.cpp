#include "opengl/GLShader.h"
#include "opengl/GLGPUProgram.h"
#include <iterator>

using namespace Islander::Renderer;


GLVertexShader::GLVertexShader(const char* source, GPUProgramLayout* layout)
	: 
	_handle(0),
	_layout(layout)
{
	_handle = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(_handle, 1, &source, NULL);
	glCompileShader(_handle);

	GLint status;
	glGetShaderiv(_handle, GL_COMPILE_STATUS, &status);

	if (status != GL_TRUE)
	{
		char buffer[512];
		glGetShaderInfoLog(_handle, 512, NULL, buffer);
		_errors.push_back(std::string(buffer));
	}
}

void GLVertexShader::Bind(GLuint program)
{
	glAttachShader(program, _handle);

	//res = glGetError();
}

void GLVertexShader::Release()
{
	if (_handle != 0)
	{
		glDeleteShader(_handle);
		delete _layout;
		_layout = nullptr;
	}
}

void GLVertexShader::GetErrors(std::vector<std::string>& errors)
{
	std::copy(_errors.begin(), _errors.end(), std::back_inserter(errors));
}

GLVertexShader::~GLVertexShader()
{
	Release();
}


GLPixelShader::GLPixelShader(const char* source, GPUProgramLayout* layout)
	:
	_handle(0),
	_layout(layout)
{
	_handle = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(_handle, 1, &source, NULL);
	glCompileShader(_handle);

	GLint status;
	glGetShaderiv(_handle, GL_COMPILE_STATUS, &status);

	if (status != GL_TRUE)
	{
		char buffer[512];
		glGetShaderInfoLog(_handle, 512, NULL, buffer);
		_errors.push_back(std::string(buffer));
	}
}

void GLPixelShader::Bind(GLuint program)
{
	glAttachShader(program, _handle);
}

void GLPixelShader::Release()
{
	if (_handle != 0)
	{
		glDeleteShader(_handle);
		_handle = 0;
		delete _layout;
		_layout = 0;
	}
}

void GLPixelShader::GetErrors(std::vector<std::string>& errors)
{
	std::copy(_errors.begin(), _errors.end(), std::back_inserter(errors));
}

GLPixelShader::~GLPixelShader()
{
	Release();
}

