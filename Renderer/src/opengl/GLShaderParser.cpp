#include <opengl/GLShaderParser.h>

using namespace Islander::Renderer;

GLShaderParser::GLShaderParser(const std::string& input)
	: _input(input), _success(false)
{
}

GLShaderParser::GLShaderToken GLShaderParser::GetToken(const std::string& item)
{
        if (item == "in")
	{
		return GL_SHADER_TOKEN_IN;
	}
	else if (item == "]")
	{
		return GL_SHADER_TOKEN_SQUARE_BRACKET_END;
	}
	else if (item == "[")
	{
		return GL_SHADER_TOKEN_SQUARE_BRACKET_START;
	}
	else if (item == ">")
	{
		return GL_SHADER_TOKEN_TAG_END;
	}
	else if (item == "<@Bind")
	{
		return GL_SHADER_TOKEN_TAG_START;
	}
	else if (item == "uniform")
	{
		return GL_SHADER_TOKEN_UNIFORM;
	}
	else if (item == ";")
	{
		return GL_SHADER_TOKEN_SEMI_COLON;
	}
	else if (item == ":")
	{
		return GL_SHADER_TOKEN_COLON;
	}
	else if (item == "{")
	{
		return GL_SHADER_TOKEN_BRACES_START;
	}
	else if (item == "}")
	{
		return GL_SHADER_TOKEN_BRACES_END;
	}
	else if (item == "(")
	{
		return GL_SHADER_TOKEN_BRACKET_START;
	}
	else if (item == ")")
	{
		return GL_SHADER_TOKEN_BRACKET_END;
	}
	else if (item == "=")
	{
		return GL_SHADER_TOKEN_EQUALS;
	}

	return GL_SHADER_TOKEN_STRING;
}

bool GLShaderParser::ParseUniform(int& pos)
{
	auto& shaderType = _tokens[pos++];
	if (shaderType._token != GL_SHADER_TOKEN_STRING)
		return false;

	auto& name = _tokens[pos++];
	if (shaderType._token != GL_SHADER_TOKEN_STRING)
		return false;

	GLShaderUniform uniform;
	uniform._pos = _uniform.size();
	uniform._name = name._item;
	uniform._type = shaderType._item;
	_uniform.push_back(uniform);
	return true;
}

bool GLShaderParser::ParseInput(int& pos)
{
	auto& shaderType = _tokens[pos++];
	if (shaderType._token != GL_SHADER_TOKEN_STRING)
		return false;

	auto& name = _tokens[pos++];
	if (shaderType._token != GL_SHADER_TOKEN_STRING)
		return false;

	GLShaderInput input;
	input._pos = _inputs.size();
	input._name = name._item;
	input._type = shaderType._item;
	_inputs.push_back(input);
	return true;
}

bool GLShaderParser::ParseTag(int& pos)
{
	GLShaderTag tag;

	auto& start = _tokens[pos++];
	if (start._token != GL_SHADER_TOKEN_SQUARE_BRACKET_START)
		return false;

	auto& binding = _tokens[pos++];
	if (binding._token != GL_SHADER_TOKEN_STRING)
		return false;

	tag._binding = binding._item;

	auto& colon = _tokens[pos++];
	if (colon._token != GL_SHADER_TOKEN_COLON)
		return false;

	if (_tokens[pos]._token == GL_SHADER_TOKEN_STRING)
	{
		auto& attribute = _tokens[pos++];
		if (attribute._token != GL_SHADER_TOKEN_STRING)
			return false;

		tag._attribute = attribute._item;
	}

	auto& end = _tokens[pos++];
	if (end._token != GL_SHADER_TOKEN_SQUARE_BRACKET_END)
		return false;

	auto& endtag = _tokens[pos++];
	if (endtag._token != GL_SHADER_TOKEN_TAG_END)
		return false;
	
	int lookahead = pos;
	auto next = _tokens[lookahead++];

	// Lookahead to get the remaining without advancing the pos
	if (next._token == GL_SHADER_TOKEN_STRING)
	{
		if (next._item != "layout")
			return false;

		auto& bracketStart = _tokens[lookahead++];
		if (bracketStart._token != GL_SHADER_TOKEN_BRACKET_START)
			return false;

		auto& location = _tokens[lookahead++];
		if (location._token != GL_SHADER_TOKEN_STRING)
			return false;

		auto& equals = _tokens[lookahead++];
		if (equals._token != GL_SHADER_TOKEN_EQUALS)
			return false;

		auto& position = _tokens[lookahead++];
		if (position._token != GL_SHADER_TOKEN_STRING)
			return false;

		auto& bracketEnd = _tokens[lookahead++];
		if (bracketEnd._token != GL_SHADER_TOKEN_BRACKET_END)
			return false;

		next = _tokens[lookahead++];
	}

	if (next._token == GL_SHADER_TOKEN_IN || next._token == GL_SHADER_TOKEN_UNIFORM)
	{
		auto& shaderType = _tokens[lookahead++];
		if (shaderType._token != GL_SHADER_TOKEN_STRING)
			return false;
		
		auto& name = _tokens[lookahead++];
		if (name._token != GL_SHADER_TOKEN_STRING)
			return false;

		tag._variable = name._item;
		tag._type = shaderType._item;
	}
	
	_tags.push_back(tag);
	return true;
}

void GLShaderParser::Parse()
{
	_tokens.clear();
	_tokens.swap(_tokens);
	_tags.clear();
	_tags.swap(_tags);
	_success = false;

	_output.clear();
	_output.swap(_output);
	
	// Generate tokens

	bool ignore = false;
	std::string item;
	for (int i = 0; i < _input.length(); ++i)
	{
		auto c = _input[i];

		if (isspace(c) && item.length() > 0)
		{
			ShaderToken token;
			token._item = item;
			token._token = GetToken(item);
			_tokens.push_back(token);

			if (!ignore)
				_output += item;

			item = "";
		}
		else if (!isspace(c))
		{
			item += c;
						
			bool commit = false;
			bool space = false;

			if (i + 1 == _input.length())
			{
				commit = true;
			}
			else
			{
				GLShaderToken cur = GetToken(std::string(item));
				if (cur != GL_SHADER_TOKEN_STRING &&
					item.size() == 1)
				{
					commit = true;
				}

				auto& n = _input[i + 1];
				if (isspace(n))
				{
					commit = true;
				}
				else
				{
					GLShaderToken next = GetToken(std::string(&n, 1));
					if (next != GL_SHADER_TOKEN_STRING)
					{
						commit = true;
					}
				}
			}

			if (commit)
			{
				ShaderToken token;
				token._item = item;
				token._token = GetToken(item);
				_tokens.push_back(token);

				if (token._token == GL_SHADER_TOKEN_TAG_START)
					ignore = true;
				
				if (!ignore)
					_output += item;
				
				if (token._token == GL_SHADER_TOKEN_TAG_END)
					ignore = false;

				item = "";
			}
		}
		else
		{
			if (!ignore)
				_output += c;
		}
	}

	// Parse tokens

	int pos = 0;
	while (pos < _tokens.size())
	{
		auto& t = _tokens[pos++];
		switch (t._token)
		{
		case GL_SHADER_TOKEN_TAG_START:
			if (!ParseTag(pos))
				return;
			break;
		case GL_SHADER_TOKEN_UNIFORM:
			if (!ParseUniform(pos))
				return;
			break;
		case GL_SHADER_TOKEN_IN:
			if (!ParseInput(pos))
				return;
			break;
		}
	}

	_success = true;
}
