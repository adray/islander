#include "opengl/GLBuffer.h"
#include "opengl/GLGPUProgram.h"
#include <string.h>

using namespace Islander::Renderer;

GLVertexBuffer::GLVertexBuffer(int stride, GraphicsBuffering buffering)
	:
	_usage(USAGE_STATIC),
	_stride(stride),
	_id(-1),
	_sizeInBytes(0),
	_buffering(buffering),
	_index(0)
{
	_buffer[0] = 0;
	_buffer[1] = 0;
}

void GLVertexBuffer::Allocate(int sizeInBytes, void* data, Usage usage)
{
	_sizeInBytes = sizeInBytes;
	_usage = usage;

	glGenBuffers(_buffering == BUFFERING_SINGLE ? 1 : 2, _buffer);

	Fill(0, data);
}

void GLVertexBuffer::Fill(int offset, void* data)
{
	GLint uses[] = { GL_STATIC_DRAW, GL_DYNAMIC_DRAW, };

	glBindBuffer(GL_ARRAY_BUFFER, _buffer[_index]);
	glBufferData(GL_ARRAY_BUFFER, _sizeInBytes, data, uses[_usage]);
}

void GLVertexBuffer::Bind(GPUProgramLayout* layout)
{
	glBindBuffer(GL_ARRAY_BUFFER, _buffer[_index]);
}

void GLVertexBuffer::Swap()
{
	if (_buffering == BUFFERING_DOUBLE)
	{
		_index = (_index + 1) % 2;
	}
}

void GLVertexBuffer::Release()
{
	glDeleteBuffers(_buffering == BUFFERING_SINGLE ? 1 : 2, _buffer);
	_buffer[0] = 0;
	_buffer[1] = 0;
}

GLVertexBuffer::~GLVertexBuffer()
{
	Release();
}


void GLConstantBuffer::Allocate(int sizeInBytes, void* data, Usage usage)
{	
	_buffer = new float[sizeInBytes / sizeof(float)];
	memcpy(_buffer, data, sizeInBytes);
	_usage = usage;
	_sizeInBytes = sizeInBytes;
}

void GLConstantBuffer::Fill(int offset, void* data)
{
	memcpy(_buffer, data, _sizeInBytes);
}

void GLConstantBuffer::Bind(const std::vector<GLConstant>& constants)
{
	for (auto& attribute : constants)
	{
		int i = attribute._offset;
		switch (attribute._format)
		{
		case ISLANDER_SEMANTIC_FLOAT:
			glUniform1f(attribute._index, _buffer[i]);
			break;
		case ISLANDER_SEMANTIC_FLOAT2:
			glUniform2f(attribute._index,
				_buffer[i],
				_buffer[i + 1]);
			break;
		case ISLANDER_SEMANTIC_FLOAT3:
			glUniform3f(attribute._index,
				_buffer[i],
				_buffer[i + 1],
				_buffer[i + 2]);
			break;
		case ISLANDER_SEMANTIC_FLOAT4:
			glUniform4f(attribute._index,
				_buffer[i],
				_buffer[i + 1],
				_buffer[i + 2],
				_buffer[i + 3]);
			break;
		}
	}
}

void GLConstantBuffer::Release()
{
	delete[] _buffer;
	_buffer = nullptr;
}

GLConstantBuffer::~GLConstantBuffer()
{
	Release();
}
