#include "opengl/GLGPUProgram.h"
#include "opengl/GLShader.h"
#include "opengl/GLBuffer.h"
#include <Logger.h>
#include <string>
#include <unordered_map>
#include <sstream>
#include <limits.h>
#include <algorithm>

extern "C"
{
#include <entity.h>
}

using namespace Islander::Renderer;

AttributeClass MatchAttributeClass(const std::string& property)
{
	if (property == "Transform")
	{
		return ATTRIBUTE_CLASS_TRANSFORM;
	}
	else if (property == "Texture0")
	{
		return ATTRIBUTE_CLASS_TEXTURE0;
	}
	else if (property == "Texture1")
	{
		return ATTRIBUTE_CLASS_TEXTURE1;
	}
	else if (property == "Texture2")
	{
		return ATTRIBUTE_CLASS_TEXTURE2;
	}
	else if (property == "Texture3")
	{
		return ATTRIBUTE_CLASS_TEXTURE3;
	}
	else if (property == "TextColour")
	{
		return ATTRIBUTE_CLASS_FONT_COLOUR;
	}
	else if (property == "Component")
	{
		return ATTRIBUTE_CLASS_CUSTOM;
	}
	else if (property == "MeshTransform")
	{
		return ATTRIBUTE_CLASS_MESH_TRANSFORM;
	}
	else if (property == "WindowWidth")
	{
		return ATTRIBUTE_CLASS_WINDOW_WIDTH;
	}
	else if (property == "WindowHeight")
	{
		return ATTRIBUTE_CLASS_WINDOW_HEIGHT;
	}

	// Assume custom
	return ATTRIBUTE_CLASS_CUSTOM;
}

int MatchProperty(const std::string& property)
{
	int index = IslanderGetComponentProperty(property.c_str());
	if (!index)
	{
		index = IslanderRegisterComponentProperty(property.c_str());
	}

	return index;
}

GPUProgramLayout::GPUProgramLayout(const std::vector<GLShaderInput>& inputs, const std::vector<GLShaderTag>& tags, const std::vector<GLShaderUniform>& uniforms, SEMANTIC* semantics, int count)
	:
	_inputs(inputs),
	_tags(tags),
	_uniforms(uniforms),
	_count(count)
{
	_semantics = new SEMANTIC[count];
	for (int i = 0; i < _count; i++)
	{
		_semantics[i] = semantics[i];
	}

	for (auto& in : tags)
	{
		int32_t format;
		if (in._type == "float")
			format = ISLANDER_SEMANTIC_FLOAT;
		else if (in._type == "vec2")
			format = ISLANDER_SEMANTIC_FLOAT2;
		else if (in._type == "vec3")
			format = ISLANDER_SEMANTIC_FLOAT3;
		else if (in._type == "vec4")
			format = ISLANDER_SEMANTIC_FLOAT4;

		_attributes.push_back(Attribute(format, MatchAttributeClass(in._binding), MatchProperty(in._attribute), in._variable));
	}
}

GPUProgramLayout::~GPUProgramLayout()
{
	delete[] _semantics;
	_semantics = 0;
}

GPUProgram::GPUProgram(GLVertexShader* vertexShader, GLPixelShader* fragmentShader)
{
	// Create program
	_program = glCreateProgram();

	auto res = glGetError();

	vertexShader->Bind(_program);
	fragmentShader->Bind(_program);

	res = glGetError();

	glBindFragDataLocation(_program, 0, "outColor");

	res = glGetError();

	glLinkProgram(_program);

	res = glGetError();
	
	_posAttrib = GetAttributeLocation("position");

	// Initialize vao
	glGenVertexArrays(1, &_vao);
	glBindVertexArray(_vao);

	Link(vertexShader->GetLayout(), fragmentShader->GetLayout());

	Validate();
}

void GPUProgram::Validate()
{
	glValidateProgram(_program);

	GLint validated;
	glGetProgramiv(_program, GL_VALIDATE_STATUS, &validated);
	if (validated == GL_FALSE)
	{
		GLint length;
		glGetProgramiv(_program, GL_INFO_LOG_LENGTH, &length);
		char* info = new char[length];
		glGetProgramInfoLog(_program, length, 0, info);

		std::stringstream ss;
		ss << "Program could not be validated." << std::endl << info;
		Islander::Logger::GetLogger()->Log(ss.str(), LOGGING_RENDERING);
	}
}

void GPUProgram::Bind(GLVertexBuffer* buffer, GLConstantBuffer* constant)
{
	glUseProgram(_program);

	glBindVertexArray(_vao);

	if (constant)
	{
		constant->Bind(_constants);
	}
	buffer->Bind(_layout);

	//glEnableVertexAttribArray(_posAttrib);
	//glVertexAttribPointer(_posAttrib, 4, GL_FLOAT, GL_FALSE, 0, 0);

	GLint attributeCount;
	glGetProgramiv(_program, GL_ACTIVE_ATTRIBUTES, &attributeCount);

	int offset = 0;
	for (int j = 0; j < _layout->GetCount(); j++)
	{
		auto semantic = _layout->GetSemantic(j);

		if (semantic.stream == 0)
		{
			int size = 0;
			switch (semantic.format)
			{
			case ISLANDER_SEMANTIC_FLOAT:
				size = 1;
				break;
			case ISLANDER_SEMANTIC_FLOAT2:
				size = 2;
				break;
			case ISLANDER_SEMANTIC_FLOAT3:
				size = 3;
				break;
			case ISLANDER_SEMANTIC_FLOAT4:
				size = 4;
				break;
			}

			glEnableVertexAttribArray(j);
			glVertexAttribPointer(j, size, GL_FLOAT, GL_FALSE, buffer->GetStride(), (GLvoid*)offset);
			glVertexAttribDivisor(j, 0);

			offset += size * sizeof(float);
		}
	}
}

void GPUProgram::Bind(GLVertexBuffer** buffer, int count)
{
	glUseProgram(_program);

	GLint MaxVertexAttribs;
	glGetIntegerv(GL_MAX_VERTEX_ATTRIBS, &MaxVertexAttribs);

	glBindVertexArray(_vao);
	for (int i = 0; i < count; i++)
	{
		auto current = buffer[i];
		current->Bind(_layout);

		int offset = 0;

		if (i == 0)
		{
			GLint attributeCount;
			glGetProgramiv(_program, GL_ACTIVE_ATTRIBUTES, &attributeCount);

			for (int j = 0; j < _layout->GetCount(); j++)
			{
				auto semantic = _layout->GetSemantic(j);

				if (semantic.stream == i)
				{
					int size = 0;
					switch (semantic.format)
					{
					case ISLANDER_SEMANTIC_FLOAT:
						size = 1;
						break;
					case ISLANDER_SEMANTIC_FLOAT2:
						size = 2;
						break;
					case ISLANDER_SEMANTIC_FLOAT3:
						size = 3;
						break;
					case ISLANDER_SEMANTIC_FLOAT4:
						size = 4;
						break;
					}

					glEnableVertexAttribArray(j);
					glVertexAttribPointer(j, size, GL_FLOAT, GL_FALSE, current->GetStride(), (GLvoid*)offset);
					glVertexAttribDivisor(j, 0);

					offset += size*sizeof(float);
				}
			}
		}
		else
		{
			GLint attributeCount;
			glGetProgramiv(_program, GL_ACTIVE_ATTRIBUTES, &attributeCount);

			int offset = 0;
			for (int j = 0; j < _layout->GetCount(); j++)
			{
				auto semantic = _layout->GetSemantic(j);

				if (semantic.stream == i)
				{
					glEnableVertexAttribArray(j);

					int size = 0;
					switch (semantic.format)
					{
					case ISLANDER_SEMANTIC_FLOAT:
						size = 1;
						break;
					case ISLANDER_SEMANTIC_FLOAT2:
						size = 2;
						break;
					case ISLANDER_SEMANTIC_FLOAT3:
						size = 3;
						break;
					case ISLANDER_SEMANTIC_FLOAT4:
						size = 4;
						break;
					}

					glVertexAttribPointer(j, size, GL_FLOAT, GL_FALSE, current->GetStride(), (GLvoid*)(offset * sizeof(float)));
					glVertexAttribDivisor(j, 1);

					offset += size;
				}
			}
		}
	}
}

void GPUProgram::Link(GPUProgramLayout* vertexLayout, GPUProgramLayout* pixelLayout)
{
	_layout = vertexLayout;
	
	std::unordered_map<std::string, int> offsetMap;
	std::unordered_map<std::string, int> indexMap;

	GLint attributeCount;
	glGetProgramiv(_program, GL_ACTIVE_ATTRIBUTES, &attributeCount);

    const int indexCount = 16;
	std::string indices[indexCount];
	
	int attributeOffset = 0;
	int stream = 0;
    for (int i = 0; i < attributeCount; i++)
	{
		char name[20];
		GLsizei length;
		GLint size;
		GLenum type;
		glGetActiveAttrib(_program, i, sizeof(name) / sizeof(char), &length, &size, &type, name);

		int index = GetAttributeLocation(name);
		if (index >= 0 && index < indexCount)
		{
			indices[index] = std::string(name);
		}
        else
        {
            Logger::GetLogger()->Log("GL Link Error: Exceeding maximum number of layout locations.", LOGGING_RENDERING);
        }
	}

	for (int i = 0; i < std::min(10, vertexLayout->GetCount()); i++)
	{
		SEMANTIC sementic = vertexLayout->GetSemantic(i);
		if (sementic.stream > stream)
		{
			stream = sementic.stream;
			attributeOffset = 0;
		}

		auto& name = indices[i];
		offsetMap.insert(std::make_pair(std::string(name), attributeOffset));
		indexMap.insert(std::make_pair(std::string(name), i));

		if (sementic.format == ISLANDER_SEMANTIC_FLOAT)
		{
			attributeOffset++;
		}
		else if (sementic.format == ISLANDER_SEMANTIC_FLOAT2)
		{
			attributeOffset += 2;
		}
		else if (sementic.format == ISLANDER_SEMANTIC_FLOAT3)
		{
			attributeOffset += 3;
		}
		else if (sementic.format == ISLANDER_SEMANTIC_FLOAT4)
		{
			attributeOffset += 4;
		}
	}

	GLint constantCount;
	glGetProgramiv(_program, GL_ACTIVE_UNIFORMS, &constantCount);

	int offset = 0;
	for (int i = 0; i < constantCount; i++)
	{
		char name[20];
		GLsizei length;
		GLint size;
		GLenum type;
		glGetActiveUniform(_program, i, sizeof(name) / sizeof(char), &length, &size, &type, name);
		
		offsetMap.insert(std::make_pair(std::string(name), offset));
		indexMap.insert(std::make_pair(std::string(name), i));

		if (type == GL_FLOAT)
		{
			_constants.push_back(GLConstant(ISLANDER_SEMANTIC_FLOAT, offset, i));
			offset++;
		}
		else if (type == GL_FLOAT_VEC2)
		{
			_constants.push_back(GLConstant(ISLANDER_SEMANTIC_FLOAT2, offset, i));
			offset += 2;
		}
		else if (type == GL_FLOAT_VEC3)
		{
			_constants.push_back(GLConstant(ISLANDER_SEMANTIC_FLOAT3, offset, i));
			offset += 3;
		}
		else if (type == GL_FLOAT_VEC4)
		{
			_constants.push_back(GLConstant(ISLANDER_SEMANTIC_FLOAT4, offset, i));
			offset += 4;
		}
	}

	for (auto& uniform : pixelLayout->Uniforms())
	{
		if (uniform._type == "sampler2D")
		{
			_samplers.push_back(GetUniformLocation(uniform._name.c_str()));
		}
	}

	for (auto& attribute : vertexLayout->GetAttributes())
	{
		if (offsetMap.find(attribute.GetParameter()) != offsetMap.end())
		{
			attribute.SetOffset(offsetMap[attribute.GetParameter()]);
			attribute.SetIndex(indexMap[attribute.GetParameter()]);
		}
	}
}

GLuint GPUProgram::GetSampler(int index)
{
	int size = _samplers.size();
	if (index < 0 || size <= index)
	{
		return UINT_MAX;
	}

	return _samplers[index];
}

GLint GPUProgram::GetUniformLocation(const char* uniform)
{
	return glGetUniformLocation(_program, uniform);
}

GLint GPUProgram::GetAttributeLocation(const char* attribute)
{
	return glGetAttribLocation(_program, attribute);
}
