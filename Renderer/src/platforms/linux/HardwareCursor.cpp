#include "HardwareCursor.h"
#include "platforms/linux/Linux.h"
#include "Window.h"
#include "Logger.h"
#include <sstream>
#include "TextureLoader.h"
#include <X11/Xcursor/Xcursor.h>

using namespace Islander::Renderer;

HardwareCursor::HardwareCursor(WINDOW* window)
	: _window(window)
{
	auto state = reinterpret_cast<RenderState*>(_window->Handle());

	// Create empty cursor
	char nullData[] = { 0 };
	auto map = XCreateBitmapFromData(state->display,
					 DefaultRootWindow(state->display),
					 nullData,
					 1, 1);
	
	XColor c;
	c.red = c.green = c.blue = 0;
	auto cursor = XCreatePixmapCursor(state->display, map, map, &c, &c, 0, 0);
	_images.push_back(new Cursor(cursor));
}

int HardwareCursor::LoadCursor(const std::string& path)
{
	unsigned char* data;
	TextureLoadInfo info;
	if ((data = LoadTexture(path.c_str(), 0, info)) == 0)
	{
		Logger::GetLogger()->Log("Failed to load cursor",
					 LOGGING_INFO);
		return 0;
	}

	auto image = XcursorImageCreate(info._width, info._height);
	image->xhot = 0;
	image->yhot = 0;
	for (int i = 0; i < info._height; i++)
	{
		for (int k = 0; k < info._width*4; k+=4)
		{
			int r = data[k+info._width*4*i];
		    int g = data[k+1+info._width*4*i];
		    int b = data[k+2+info._width*4*i];
		    int a = data[k+3+info._width*4*i];
		    
			image->pixels[i*info._width+k/4] = b | (g << 8) | (r << 16) | (a << 24);
		}
	}

	auto state = reinterpret_cast<RenderState*>(_window->Handle());

	Cursor cursorX = XcursorImageLoadCursor(state->display, image);

	assert(cursorX);

	XcursorImageDestroy(image);

	std::stringstream ss;
	ss << "Loaded cursor: " << path;
	Logger::GetLogger()->Log(ss.str(), LOGGING_INFO);

	delete[] data;
	
	_images.push_back(new Cursor(cursorX));
	return _images.size()-1;
}

void HardwareCursor::SetCursor(int type)
{
	auto state = reinterpret_cast<RenderState*>(_window->Handle());
	
	XDefineCursor(state->display, state->root, *(Cursor*)_images[type]);
}

void HardwareCursor::ShowCursor()
{
	auto state = reinterpret_cast<RenderState*>(_window->Handle());
	
	XUndefineCursor(state->display, state->root);
}

void HardwareCursor::HideCursor()
{
	SetCursor(0);
}

HardwareCursor::~HardwareCursor()
{
	Logger::GetLogger()->Log("Destroying cursor", LOGGING_INFO);
	//SetCursor(0);

	// TODO: free all the cursors in images list?

	auto state = reinterpret_cast<RenderState*>(_window->Handle());
	XDestroyWindow(state->display, state->win);
	XCloseDisplay(state->display);
	//XFlush(state->display);
}

