#include "OpenGL.h"
#include "platforms/linux/Linux.h"
#include <Logger.h>
#include "Window.h"

using namespace Islander;
using namespace Islander::Renderer;

namespace Islander
{
	namespace Renderer
	{
//		typedef  InternalOpenGLContext RenderState;
	}
}

OpenGLContext::OpenGLContext()
{
	
}

int OpenGLContext::InitializeGL(WINDOW* window)
{
        auto version = glGetString(GL_VERSION);
	Logger::GetLogger()->Log((char*)version,
				 LOGGING_INFO);

	return 1;
}

void OpenGLContext::DeactivateGL(WINDOW* window)
{
	
}

void OpenGLContext::Present(WINDOW* window)
{
	auto state = static_cast<RenderState*>(window->Handle());
	glXSwapBuffers(state->display, state->win);
}

