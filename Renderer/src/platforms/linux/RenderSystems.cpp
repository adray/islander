#include "RenderSystems.h"
#include "opengl/GLRenderer.h"

using namespace Islander::Renderer;

void RenderSystems:: GetRenderers(std::vector<IRenderer*>& systems)
{
	systems.push_back(new GLRenderer());
}
