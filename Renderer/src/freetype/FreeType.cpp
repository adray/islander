#include <sstream>
#include <Logger.h>
#include <unordered_map>
#include <unordered_set>
#include <assert.h>
#include "FreeTypeEngine.h"
#include "Renderer.h"
#include "TextTokenizer.h"
#include "GraphicsBuffer.h"
#include "Material.h"
#include <ft2build.h>
#include FT_FREETYPE_H

using namespace Islander;
using namespace Islander::Renderer;

#define TEXTURES_MAX_COUNT 16

namespace Islander
{
    namespace Renderer
    {
        struct FreeTypeTextureSlot
        {
            int _px;
            int _py;
            int _vertexBuffer;
        };

        struct FreeTypeTexture
        {
            int _texture;
            int _px;
            int _py;
            int _slotWidth;
            int _slotHeight;
            std::vector<FreeTypeTextureSlot*> _slots;            
        };

        struct FreeTypeTextSegment
        {
            RECTANGLE _rect;
            bool bold;
            bool strike;
            bool link;
            bool newline;
        };

        class FreeTypeCache
        {
        public:

            struct CacheItem
            {
                int64_t key;
                FT_Pos horiAdvance;
                FT_Int bitmap_top;
                FT_Int bitmap_left;
                unsigned int bitmap_width;
                unsigned int bitmap_rows;
                unsigned char * buffer;
            };

            FreeTypeCache()
            {
                _buffer = new unsigned char[bufferLength];
                _buffer_offset = 0;
            }

            void SetSize(int size)
            {
                _size = size;
            }

            void Add(CacheItem& item)
            {
                _cache.insert(std::pair<int64_t, CacheItem>(item.key, item));
            }

            void Add(char ch, FT_Face face, CacheItem& item)
            {
                item.key = _size | (int64_t)ch << 32;
                item.bitmap_left = face->glyph->bitmap_left;
                item.bitmap_rows = face->glyph->bitmap.rows;
                item.bitmap_top = face->glyph->bitmap_top;
                item.bitmap_width = face->glyph->bitmap.width;
                item.horiAdvance = face->glyph->metrics.horiAdvance;

                int length = item.bitmap_rows * item.bitmap_width;
                if (_buffer_offset + length >= bufferLength)
                {
                    item.buffer = new unsigned char[length];
                    memcpy(item.buffer, face->glyph->bitmap.buffer, length);
                }
                else
                {
                    item.buffer = _buffer + _buffer_offset;
                    memcpy(item.buffer, face->glyph->bitmap.buffer, length);
                    _buffer_offset += length;
                }
                Add(item);
            }

            bool Get(char ch, CacheItem& item)
            {
                int64_t key = _size | (int64_t)ch << 32;
                if (_cache.find(key) != _cache.end())
                {
                    item = _cache[key];
                    return true;
                }

                return false;
            }

        private:
            std::unordered_map<int64_t, CacheItem> _cache;
            int _size;
            unsigned char * _buffer;
            int _buffer_offset;
            const int bufferLength = 1024 * 2 * 20;
        };

        class FreeTypeEngineImpl
        {
            private:
                FT_Library _library;
                std::unordered_map<std::string, FT_Face> _faces; // not the best key...
                std::vector<FreeTypeTexture*> _texture;
                std::unordered_map<FT_Face, FreeTypeCache*> _faceCache;
                FreeTypeCache* _cache;
                uint32_t _dpi;

                static const int TextureWidth;
                static const int TextureHeight;

                bool FindTexture(FreeTypeTexture** foundTexture, FreeTypeTextureSlot** foundSlot, int width, int height);
                bool GrowSlotArray(FreeTypeTexture* textureItem, FreeTypeTextureSlot** foundSlot);
                void MeasureText(std::vector<Token>& tokens, FT_Face face, int& width, int& height, int wrapwidth, bool wrap);
                void MeasureSequence(FT_Face face, const std::string& text, int& width);
                void MeasureSpace(FT_Face face, int& width);

            public:
                FreeTypeEngineImpl();
                void LoadFace(const std::string& name, const std::string& filename);
                bool RenderText(
                    component_text* text,
                    component_transform* transform,
                    int windowWidth, int windowHeight,
                    uint32_t dpi,
                    IRenderer* renderer,
                    FreeTypeEngineRender& render);
                void MeasureText(
                    component_text* text,
                    component_transform* transform,
                    int windowWidth,
                    int windowHeight,
                    uint32_t dpi);
                void RenderSequence(FT_Face face, IRenderer* renderer,
                    const std::string& text, int tex, int* px, int* py);
                void InsertSpace(FT_Face face, int* px);
                void BuildBufferData(component_text* text, IRenderer* renderer, const std::vector<FreeTypeTextSegment>& segments, int width, int height, int startX, int startY);
                void Clear(IRenderer* renderer);
                void FreeBuffer(IRenderer* renderer, int buffer);
        };
    }
}

FreeTypeEngineImpl::FreeTypeEngineImpl()
{
    auto error = FT_Init_FreeType(&_library);
    if (error)
    {
        std::stringstream ss;
        ss << "(Freetype) Unable to initialize";
        Islander::Logger::GetLogger()->Log(ss.str(), LOGGING_RENDERING);
    }
}

const int FreeTypeEngineImpl::TextureWidth = 1024 * 2;
const int FreeTypeEngineImpl::TextureHeight = 1024 * 2;

void FreeTypeEngineImpl::LoadFace(const std::string& name, const std::string& filename)
{
    FT_Face face;
    int error = FT_New_Face(_library,
                     filename.c_str(),
                     0,
                     &face);
    if (error)
    {
        std::stringstream ss;
        ss << "(Freetype) Unable to load " << filename;
        Islander::Logger::GetLogger()->Log(ss.str(), LOGGING_RENDERING);
    }
    else
    {
        std::stringstream ss;
        ss << "(Freetype) Loaded " << filename;
        Islander::Logger::GetLogger()->Log(ss.str(), LOGGING_RENDERING);

        _faces.insert(std::pair<std::string, FT_Face>(name, face));
        _faceCache.insert(std::pair<FT_Face, FreeTypeCache*>(face, new FreeTypeCache()));
    }
}

void FreeTypeEngineImpl::FreeBuffer(IRenderer* renderer, int buffer)
{
    for (auto texture : _texture)
    {
        bool done = false;
        for (auto slot : texture->_slots)
        {
            if (slot->_vertexBuffer == buffer)
            {
                slot->_vertexBuffer = -1;
                done = true;
                break;
            }
        }

        if (done) { break; }
    }
}

void FreeTypeEngineImpl::Clear(IRenderer* renderer)
{
    // TODO
    //_px = 0;
    //_py = 0;

    //renderer->ClearTexture(_texture, TextureWidth, TextureHeight);
}

bool FreeTypeEngineImpl::GrowSlotArray(FreeTypeTexture* textureItem, FreeTypeTextureSlot** foundSlot)
{
    if (textureItem->_px + textureItem->_slotWidth <= TextureWidth &&
        textureItem->_py + textureItem->_slotHeight <= TextureHeight)
    {
        *foundSlot = new FreeTypeTextureSlot();
        (*foundSlot)->_vertexBuffer = -1;
        (*foundSlot)->_px = textureItem->_px;
        (*foundSlot)->_py = textureItem->_py;

        textureItem->_px += textureItem->_slotWidth;
        if (textureItem->_px == TextureWidth)
        {
            textureItem->_px = 0;
            textureItem->_py += textureItem->_slotHeight;
        }

        textureItem->_slots.push_back(*foundSlot);

        auto stream = Islander::Logger::GetStream(LOGGING_DEBUG);
        stream << "(Freetype) allocated texture slot @ " << (*foundSlot)->_px << " " << (*foundSlot)->_py << " height " << textureItem->_slotHeight;
        stream.endl();

        return true;
    }

    return false;
}

static int GetSlotHeight(int max)
{
    int size = 1024*2; // TODO: some things are trying to allocate slot size greater than 1024*2
    if (max < 32) size = 32;
    else if (max < 64) size = 64;
    else if (max < 128) size = 128;
    else if (max < 256) size = 256;
    else if (max < 512) size = 512;
    else if (max < 1024) size = 1024;
    return size;
}

bool FreeTypeEngineImpl::FindTexture(FreeTypeTexture** foundTexture, FreeTypeTextureSlot** foundSlot, int width, int height)
{
    for (auto textureItem : _texture)
    {
        if (textureItem->_slotHeight > height &&
            textureItem->_slotWidth > width)
        {
            // Search for existing unallocated slot
            for (auto slot : textureItem->_slots)
            {
                if (slot->_vertexBuffer == -1)
                {
                    *foundSlot = slot;
                    *foundTexture = textureItem;
                    return true;
                }
            }

            if (GrowSlotArray(textureItem, foundSlot))
            {
                *foundTexture = textureItem;                
                return true;
            }
        }
    }

    for (auto textureItem : _texture)
    {
        bool usedSlots = false;
        for (auto slot : textureItem->_slots)
        {
            usedSlots |= (slot->_vertexBuffer != -1);
        }

        if (!usedSlots)
        {
            textureItem->_px = 0;
            textureItem->_py = 0;
            textureItem->_slotWidth = TextureWidth;
            textureItem->_slotHeight = GetSlotHeight(height);
            textureItem->_slots.clear();
            textureItem->_slots.swap(textureItem->_slots);
            if (GrowSlotArray(textureItem, foundSlot))
            {
                *foundTexture = textureItem;
                return true;
            }
        }
    }

    return false;
}

void FreeTypeEngineImpl::MeasureSequence(FT_Face face,
    const std::string& text, int& width)
{
    FT_UInt previous = 0;

    bool use_kerning = FT_HAS_KERNING(face);

    for (auto& ch : text)
    {
        auto index = FT_Get_Char_Index(face, ch);

        FreeTypeCache::CacheItem item;

        if (index && !_cache->Get(ch, item))
        {
            if (FT_Load_Glyph(face, index, FT_LOAD_RENDER))
            {
                previous = 0;
                continue;
            }

            _cache->Add(ch, face, item);
        }

        if (use_kerning && previous)
        {
            FT_Vector delta;

            FT_Get_Kerning(face, previous, index, FT_KERNING_DEFAULT, &delta);

            width += delta.x >> 6; // delta is 1/64th of pixel
        }

        width += item.horiAdvance >> 6; // 1/64th pixel

        previous = index;
    }
}

void FreeTypeEngineImpl::MeasureSpace(FT_Face face, int& width)
{
    const int space = 32;
    auto index = FT_Get_Char_Index(face, space);
    FT_Load_Glyph(face, index, FT_LOAD_DEFAULT);
    width += face->glyph->metrics.horiAdvance >> 6; // 1/64th of pixel
}

void FreeTypeEngineImpl::MeasureText(std::vector<Token>& tokens, FT_Face face, int& width, int& height, int wrapwidth, bool wrap)
{
    int newlinesize =  face->size->metrics.height >> 6; // 1/64th of pixel
    int maxwidth=0;
    width=0;
    height=0;
    for (int i = 0; i < tokens.size(); i++)
    {
        int sequence_width = 0;
        auto& token = tokens[i];
        token.linebreak = false;
        switch (token._type)
        {
            case TOKEN_TYPE_TEXT:
                MeasureSequence(face, token._text, sequence_width);
                if (wrap && width + sequence_width > wrapwidth)
                {
                    height += newlinesize;
                    maxwidth = std::max(width, maxwidth);
                    width = 0;
                    token.linebreak = true;
                }
                width += sequence_width;
                break;
            case TOKEN_TYPE_NEWLINE:
                height += newlinesize;
                maxwidth = std::max(width, maxwidth);
                width = 0;
                break;
            case TOKEN_TYPE_SPACE:
                MeasureSpace(face, width);
                break;
        }
    }

    width = std::max(width, maxwidth);
    height += newlinesize;
}

void FreeTypeEngineImpl::MeasureText(
    component_text* text,
    component_transform* transform,
    int windowWidth,
    int windowHeight,
    uint32_t dpi)
{
    TextData* data = (TextData*)text->data;

    auto face = _faces.find(data->_font);
    if (face == _faces.end()) return;

   	auto len = strlen(data->_text);
	auto& text_data = data->_text;
	
	if (len <= 0) return;

    _cache = _faceCache[face->second];
    _cache->SetSize(data->truetype_size);

	int wrapwidth = 0;
	if (transform && text->wrap)
	{
        wrapwidth = transform->sx;
	}

    std::vector<Token> tokens;
	TextTokenizer::GetTokens(text_data, tokens);

    //FT_Set_Char_Size(face->second, 0, data->truetype_size, windowWidth, windowHeight);
    FT_Set_Char_Size(face->second, 0, data->truetype_size, dpi, dpi);

    int calculatedWidth = 0;
    int calculatedHeight = 0;
    MeasureText(tokens, face->second, calculatedWidth, calculatedHeight, wrapwidth, text->wrap && transform);

    data->_rect.width = calculatedWidth;
    data->_rect.height = calculatedHeight;
    data->_rect.x = 0;
    data->_rect.y = 0;
}

static void AddTextSegment(std::vector<FreeTypeTextSegment>& segments, int startX, int startY, int px, int py, bool bold, bool link, bool strike, bool newline, int newlinesize)
{
    if (segments.size() == 0)
    {
        FreeTypeTextSegment segment;
        segment.bold = bold;
        segment.link = link;
        segment.strike = strike;
        segment._rect.width = px - startX;
        segment._rect.height = newlinesize;
        segment._rect.x = startX;
        segment._rect.y = startY;
        segment.newline = newline;
        segments.push_back(segment);
    }
    else
    {
        FreeTypeTextSegment last = segments.at(segments.size() - 1);

        FreeTypeTextSegment segment;
        segment.bold = bold;
        segment.link = link;
        segment.strike = strike;

        int x = last.newline ? startX : last._rect.x + last._rect.width;
        int y = last.newline ? last._rect.y + newlinesize : last._rect.y;
        
        if (newline)
        {
            segment._rect.width = px - x;
            segment._rect.height = newlinesize;
            segment._rect.x = x;
            segment._rect.y = y;
            segment.newline = true;
        }
        else
        {
            segment._rect.width = px - x;
            segment._rect.height = newlinesize;
            segment._rect.x = x;
            segment._rect.y = y;
            segment.newline = false;
        }

        segments.push_back(segment);
    }
}

static void AddLinkElement(TextData* data, const FreeTypeTextSegment& segment, int linkstart, int strPos, int startX, int startY)
{
    auto& element = data->_links[data->_linkcount];
    element._rect.x = segment._rect.x - startX;
    element._rect.y = -segment._rect.y + startY;
    element._rect.width = segment._rect.width;
    element._rect.height = segment._rect.height;
    element._startIndex = linkstart;
    element._length = strPos - linkstart;
    data->_linkcount++;
}

static void BackfillLinkElementsText(TextData* data, int linkstart)
{
    for (int i = linkstart; i < data->_linkcount - 1; i++)
    {
        auto& element = data->_links[i];
        element._length = data->_links[data->_linkcount - 1]._length;
    }
}

bool FreeTypeEngineImpl::RenderText(
    component_text* text,
    component_transform* transform,
    int windowWidth, int windowHeight,
    uint32_t dpi,
    IRenderer* renderer,
    FreeTypeEngineRender& render)
{
	TextData* data = (TextData*)text->data;

    auto face = _faces.find(data->_font);
    if (face == _faces.end()) return false;

    _cache = _faceCache[face->second];
    _cache->SetSize(data->truetype_size);

   	auto len = strlen(data->_text);
	auto& text_data = data->_text;
	
	if (len <= 0) return false;

	int wrapwidth = 0;
	if (transform && text->wrap)
	{
        wrapwidth = transform->sx;
	}

	std::vector<Token> tokens;
	TextTokenizer::GetTokens(text_data, tokens);

    //FT_Set_Char_Size(face->second, 0, data->truetype_size, windowWidth, windowHeight);
    FT_Set_Char_Size(face->second, 0, data->truetype_size, dpi, dpi);

    int calculatedWidth = 0;
    int calculatedHeight = 0;
    MeasureText(tokens, face->second, calculatedWidth, calculatedHeight, wrapwidth, text->wrap && transform);

    data->_rect.width = calculatedWidth;
    data->_rect.height = calculatedHeight;
    data->_rect.x = 0;
    data->_rect.y = 0;

    data->_linkcount = 0;

    auto& stream = Islander::Logger::GetStream(LOGGING_RENDERING);

    FreeTypeTexture* texture = nullptr;
    FreeTypeTextureSlot* slot = nullptr;
    if (!FindTexture(&texture, &slot, data->_rect.width, data->_rect.height))
    {
        if (_texture.size() == TEXTURES_MAX_COUNT)
        {
            stream << "(Freetype) unable to allocate additional textures (" << _texture.size() << ")";
            stream.endl();

            // Limit the number of textures
            return false;
        }

        if (data->_rect.width >= TextureWidth)
        {
            stream << "(Freetype) unable to allocate texture slot too large (" << data->_rect.width << "x" << data->_rect.height << ")";
            stream.endl();
            return false;
        }

        // Initialize texture
        texture = new FreeTypeTexture();
        texture->_texture = renderer->ReserveTexture();
        texture->_px = texture->_py = 0;
        renderer->CreateEmptyTexture(texture->_texture, TextureWidth, TextureHeight, ISLANDER_TEXTURE_TYPE_GREYSCALE);
        _texture.push_back(texture);

        // Choose slot size - in general text is longer horizontally
        int max = data->_rect.height;
        int size = GetSlotHeight(max);
        texture->_slotWidth = TextureWidth;
        texture->_slotHeight = size;

        auto debug = Islander::Logger::GetStream(LOGGING_DEBUG);
        debug << "(Freetype) allocated texture size " << TextureWidth << "x" << TextureHeight << " @ id " << texture->_texture << " Slot height: " << size;
        debug.endl();

        if (!GrowSlotArray(texture, &slot))
        {
            stream << "(Freetype) error allocating initial slot";
            stream.endl();
            return false;
        }
    }

    assert(texture);
    assert(slot);

    // clear slot
    renderer->ClearTexture(texture->_texture, texture->_slotWidth, texture->_slotHeight, slot->_px, slot->_py);

    std::vector<FreeTypeTextSegment> segments;

    int startX = slot->_px;
    int startY = slot->_py;
    int px = startX;
    int py = startY;
    int maxpx = 0;

    bool bold = false;
    bool link = false;
    bool strike = false;

    int linkstart = 0;
    int linkindex = 0;
    int newlinesize = face->second->size->metrics.height >> 6;

    for (int i = 0; i < tokens.size(); i++)
    {
        auto& token = tokens[i];
        switch (token._type)
        {
            case TOKEN_TYPE_TEXT:
                if (token.linebreak)
                {
                    py += newlinesize;
                    AddTextSegment(segments, startX, startY, px, py, bold, link, strike, true, newlinesize);
                    if (link) { AddLinkElement(data, segments.at(segments.size() - 1), linkstart, token._index, startX, startY); }
                    maxpx = std::max(px, maxpx);
                    px = startX;
                }

                RenderSequence(face->second, renderer, token._text, texture->_texture, &px, &py);
                break;
            case TOKEN_TYPE_NEWLINE:
                py += newlinesize;
                AddTextSegment(segments, startX, startY, px, py, bold, link, strike, true, newlinesize);
                if (link) { AddLinkElement(data, segments.at(segments.size() - 1), linkstart, token._index, startX, startY); }
                maxpx = std::max(px, maxpx);
                px = startX;
                break;
            case TOKEN_TYPE_SPACE:
                InsertSpace(face->second, &px);
                break;
            case TOKEN_TYPE_BOLD_BEGIN:
                AddTextSegment(segments, startX, startY, px, py, bold, link, strike, false, newlinesize);
                bold = true;
                break;
            case TOKEN_TYPE_BOLD_END:
                AddTextSegment(segments, startX, startY, px, py, bold, link, strike, false, newlinesize);
                bold = false;
                break;
            case TOKEN_TYPE_LINK_BEGIN:
                AddTextSegment(segments, startX, startY, px, py, bold, link, strike, false, newlinesize);
                linkindex = data->_linkcount;
                linkstart = token._index + token._text.size();
                link = true;
                break;
            case TOKEN_TYPE_LINK_END:
                AddTextSegment(segments, startX, startY, px, py, bold, link, strike, false, newlinesize);
                AddLinkElement(data, segments.at(segments.size() - 1), linkstart, token._index, startX, startY);
                BackfillLinkElementsText(data, linkindex);
                link = false;
                break;
            case TOKEN_TYPE_STRIKE_BEGIN:
                strike = true;
                break;
            case TOKEN_TYPE_STRIKE_END:
                AddTextSegment(segments, startX, startY, px, py, bold, link, strike, false, newlinesize);
                strike = false;
                break;
        }
    }

    AddTextSegment(segments, startX, startY, px, py + newlinesize, bold, link, strike, true, newlinesize);
    maxpx = std::max(px, maxpx);

    int textureWidth = maxpx - startX;
    int textureHeight = py - startY + newlinesize; // 1/64th of pixel;

    render._texture = texture->_texture;
    render._px = startX;
    render._py = startY;
    render._width = textureWidth;
    render._height = textureHeight;

    data->_rect.width = textureWidth;
    data->_rect.height = textureHeight;
    data->_rect.x = 0;
    data->_rect.y = 0;

    BuildBufferData(text, renderer, segments, windowWidth/2, windowHeight/2, startX, startY);

    slot->_vertexBuffer = text->_vertid;

    return true;
}

static void AppendBuffer(float* buffer, float value, int& position)
{
	buffer[position++] = value;
}

void FreeTypeEngineImpl::BuildBufferData(
    component_text* text,
    IRenderer* renderer,
    const std::vector<FreeTypeTextSegment>& segments,
    int width, int height, int startX, int startY)
{
    float * buffer;
    int bufferLength = 0;
    int stride = 0;

    int x =0; int y=0;
    bool bold = false;

    TextData* data = (TextData*)text->data;
    Rect& rect = data->_rect;

    int position=0;

    /*
    bool supportsAtlas = false;
    if (supportsAtlas)
    {
        bufferLength = supportsAtlas ? 36 : 30;
        stride = 6;
        buffer = new float[bufferLength];

        MaterialTexture tex;
        tex._atlasIndex = -1;
        tex._position.SetX(startX / 1024.0f);
        tex._position.SetY(startY / 1024.0f);
        tex._scale.SetX(rect.width / 1024.0f);
        tex._scale.SetY(rect.height / 1024.0f);

        AppendBuffer(buffer, x / (float)width, position);
        AppendBuffer(buffer, y / (float)height, position);
        AppendBuffer(buffer, 0, position);
        AppendBuffer(buffer, 0, position);
        AppendBuffer(buffer, tex._atlasIndex, position);
        AppendBuffer(buffer, bold ? 1: 0, position);

        AppendBuffer(buffer, x / (float)width, position);
        AppendBuffer(buffer, (y - rect.height) / (float)height, position);
        AppendBuffer(buffer, 0, position);
        AppendBuffer(buffer, tex._scale.GetY(), position);
        AppendBuffer(buffer, tex._atlasIndex, position);
        AppendBuffer(buffer, bold ? 1 : 0, position);

        AppendBuffer(buffer, (rect.width + x) / (float)width, position);
        AppendBuffer(buffer, y / (float)height, position);
        AppendBuffer(buffer, tex._scale.GetX(), position);
        AppendBuffer(buffer, 0, position);
        AppendBuffer(buffer, tex._atlasIndex, position);
        AppendBuffer(buffer, bold ? 1 : 0, position);

        AppendBuffer(buffer, (rect.width + x) / (float)width, position);
        AppendBuffer(buffer, y / (float)height, position);
        AppendBuffer(buffer, tex._scale.GetX(), position);
        AppendBuffer(buffer, 0, position);
        AppendBuffer(buffer, tex._atlasIndex, position);
        AppendBuffer(buffer, bold ? 1 : 0, position);

        AppendBuffer(buffer, x / (float)width, position);
        AppendBuffer(buffer, (-rect.height + y) / (float)height, position);
        AppendBuffer(buffer, 0, position);
        AppendBuffer(buffer, tex._scale.GetY(), position);
        AppendBuffer(buffer, tex._atlasIndex, position);
        AppendBuffer(buffer, bold ? 1 : 0, position);

        AppendBuffer(buffer, (rect.width + x) / (float)width, position);
        AppendBuffer(buffer, (y - rect.height) / (float)height, position);
        AppendBuffer(buffer, tex._scale.GetX(), position);
        AppendBuffer(buffer, tex._scale.GetY(), position);
        AppendBuffer(buffer, tex._atlasIndex, position);
        AppendBuffer(buffer, bold ? 1 : 0, position);
    }
    else*/
    {
        int count = 0;
        for (auto& segment : segments)
        {
            count++;
            if (segment.strike)
            {
                count++;
            }
        }

        bufferLength = 30 * count;
        stride = 5;
        buffer = new float[bufferLength];

        for (auto& segment : segments)
        {
            MaterialTexture tex;
            tex._atlasIndex = -1;
            tex._position.SetX(segment._rect.x / float(TextureWidth));
            tex._position.SetY(segment._rect.y / float(TextureHeight));
            tex._scale.SetX(segment._rect.width / float(TextureWidth));
            tex._scale.SetY(segment._rect.height / float(TextureHeight));

            int px = segment._rect.x-startX;
            int py = -segment._rect.y + startY;

            float textStyle = (segment.bold ? 1 : 0) + (segment.link ? 4 : 0);

            AppendBuffer(buffer, px / (float)width, position);
            AppendBuffer(buffer, py / (float)height, position);
            AppendBuffer(buffer, tex._position.GetX(), position);
            AppendBuffer(buffer, tex._position.GetY(), position);
            AppendBuffer(buffer, textStyle, position);

            AppendBuffer(buffer, px / (float)width, position);
            AppendBuffer(buffer, (py - segment._rect.height) / (float)height, position);
            AppendBuffer(buffer, tex._position.GetX(), position);
            AppendBuffer(buffer, tex._scale.GetY() + tex._position.GetY(), position);
            AppendBuffer(buffer, textStyle, position);

            AppendBuffer(buffer, (segment._rect.width + px) / (float)width, position);
            AppendBuffer(buffer, py / (float)height, position);
            AppendBuffer(buffer, tex._position.GetX() + tex._scale.GetX(), position);
            AppendBuffer(buffer, tex._position.GetY(), position);
            AppendBuffer(buffer, textStyle, position);

            AppendBuffer(buffer, (segment._rect.width + px) / (float)width, position);
            AppendBuffer(buffer, py / (float)height, position);
            AppendBuffer(buffer, tex._position.GetX() + tex._scale.GetX(), position);
            AppendBuffer(buffer, tex._position.GetY(), position);
            AppendBuffer(buffer, textStyle, position);

            AppendBuffer(buffer, px / (float)width, position);
            AppendBuffer(buffer, (-segment._rect.height + py) / (float)height, position);
            AppendBuffer(buffer, tex._position.GetX(), position);
            AppendBuffer(buffer, tex._position.GetY() + tex._scale.GetY(), position);
            AppendBuffer(buffer, textStyle, position);

            AppendBuffer(buffer, (segment._rect.width + px) / (float)width, position);
            AppendBuffer(buffer, (py - segment._rect.height) / (float)height, position);
            AppendBuffer(buffer, tex._position.GetX() + tex._scale.GetX(), position);
            AppendBuffer(buffer, tex._position.GetY() + tex._scale.GetY(), position);
            AppendBuffer(buffer, textStyle, position);

            if (segment.strike)
            {
                const float strike = 2;
                const float thickness = 1.0f;
                AppendBuffer(buffer, px / (float)width, position);
                AppendBuffer(buffer, (py - segment._rect.height / 2 - thickness) / (float)height, position);
                AppendBuffer(buffer, 0, position);
                AppendBuffer(buffer, 0, position);
                AppendBuffer(buffer, strike, position);

                AppendBuffer(buffer, px / (float)width, position);
                AppendBuffer(buffer, (py - segment._rect.height / 2 + thickness) / (float)height, position);
                AppendBuffer(buffer, 0, position);
                AppendBuffer(buffer, 0, position);
                AppendBuffer(buffer, strike, position);

                AppendBuffer(buffer, (segment._rect.width + px) / (float)width, position);
                AppendBuffer(buffer, (py - segment._rect.height / 2 - thickness) / (float)height, position);
                AppendBuffer(buffer, 0, position);
                AppendBuffer(buffer, 0, position);
                AppendBuffer(buffer, strike, position);

                AppendBuffer(buffer, (segment._rect.width + px) / (float)width, position);
                AppendBuffer(buffer, (py - segment._rect.height / 2 - thickness) / (float)height, position);
                AppendBuffer(buffer, 0, position);
                AppendBuffer(buffer, 0, position);
                AppendBuffer(buffer, strike, position);

                AppendBuffer(buffer, px / (float)width, position);
                AppendBuffer(buffer, (py - segment._rect.height / 2 + thickness) / (float)height, position);
                AppendBuffer(buffer, 0, position);
                AppendBuffer(buffer, 0, position);
                AppendBuffer(buffer, strike, position);

                AppendBuffer(buffer, (segment._rect.width + px) / (float)width, position);
                AppendBuffer(buffer, (py - segment._rect.height / 2 + thickness) / (float)height, position);
                AppendBuffer(buffer, 0, position);
                AppendBuffer(buffer, 0, position);
                AppendBuffer(buffer, strike, position);
            }
        }
    }

    text->_vertid = renderer->CreateVertexBuffer(
        USAGE_STATIC, bufferLength * sizeof(float),
        buffer, stride * sizeof(float), BUFFERING_SINGLE)->ID();

    delete[] buffer;
}

void FreeTypeEngineImpl::InsertSpace(FT_Face face, int* px)
{
    const int space = 32;
    auto index = FT_Get_Char_Index(face, space);
    FT_Load_Glyph(face, index, FT_LOAD_DEFAULT);
    *px += face->glyph->metrics.horiAdvance >> 6; // 1/64th of pixel
}

void FreeTypeEngineImpl::RenderSequence(FT_Face face, IRenderer* renderer,
    const std::string& text, int tex, int* px, int* py)
{
    FT_UInt previous = 0;

    bool use_kerning = FT_HAS_KERNING(face);

    int height = (face->size->metrics.ascender - face->size->metrics.descender) >> 6; //face->size->metrics.height >> 6; // 1/64th of pixel

    for (auto& ch : text)
    {
        auto index = FT_Get_Char_Index(face, ch);

        FreeTypeCache::CacheItem item;

        if (index && !_cache->Get(ch, item))
        {
            if (FT_Load_Glyph(face, index, FT_LOAD_RENDER))
            {
                previous = 0;
                continue;
            }

            _cache->Add(ch, face, item);
        }

        if (use_kerning && previous)
        {
            FT_Vector delta;

            FT_Get_Kerning(face, previous, index, FT_KERNING_DEFAULT, &delta);

            *px += delta.x >> 6; // delta is 1/64th of pixel
        }

        renderer->CopyToTexture(
            tex,
            item.buffer,
            item.bitmap_width,
            item.bitmap_rows,
            *px + item.bitmap_left,
            *py + height/*(glyph->metrics.horiBearingY >> 6)*/ - item.bitmap_top);

        *px += item.horiAdvance >> 6; // 1/64th pixel

        previous = index;
    }
}

FreeTypeEngine::FreeTypeEngine()
{
    _impl = new FreeTypeEngineImpl();
}

FreeTypeEngine::~FreeTypeEngine()
{
    delete _impl;
    _impl = nullptr;
}

void FreeTypeEngine::LoadFace(const std::string& name, const std::string& filename)
{
    _impl->LoadFace(name, filename);
}

bool FreeTypeEngine::RenderText(
    component_text* text,
    component_transform* transform,
    int windowWidth, int windowHeight,
    uint32_t dpi,
    IRenderer* renderer,
    FreeTypeEngineRender& render)
{
    return _impl->RenderText(text, transform, windowWidth, windowHeight, dpi, renderer, render);
}

void FreeTypeEngine::Clear(IRenderer* renderer)
{
    _impl->Clear(renderer);
}

void FreeTypeEngine::FreeBuffer(IRenderer* renderer, int buffer)
{
    _impl->FreeBuffer(renderer, buffer);
}

void FreeTypeEngine::MeasureText(
    component_text* text,
    component_transform* transform,
    int windowWidth,
    int windowHeight,
    uint32_t dpi)
{
    _impl->MeasureText(text, transform, windowWidth, windowHeight, dpi);
}
