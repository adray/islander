#include "DearImgui.h"
#include "IDevice.h"
#include "imgui.h"

using namespace Islander::Imgui;

//void Islander::Imgui::RestoreContext(ImGuiContext* context, ImGuiMemAllocFunc alloc_func, ImGuiMemFreeFunc free_func, void* user_data)
//{
//    ImGui::SetCurrentContext(context);
//    ImGui::SetAllocatorFunctions(alloc_func, free_func, user_data);
//}

void Islander::Imgui::RestoreContext(IslanderImguiContext* context)
{
    ImGui::SetCurrentContext((ImGuiContext*)context->imgui_context);
    ImGui::SetAllocatorFunctions((ImGuiMemAllocFunc)context->alloc_func, (ImGuiMemFreeFunc)context->free_func, context->user_data);
}

void Islander::Imgui::CreateIMGUI(Islander::IDevice* device)
{
    // Setup Platform/Renderer backends
    device->InitializeImgui();
}

void Islander::Imgui::NewFrame(Islander::IDevice* device)
{
    device->NewFrameImgui();
}

void Islander::Imgui::Render(Islander::IDevice* device)
{
    device->RenderImgui();
}

void Islander::Imgui::Image(Islander::IDevice* device, int texture, float width, float height)
{
    device->ImageImgui(texture, width, height);
}

void Islander::Imgui::Image(Islander::IDevice* device, int texture, float x, float y, float width, float height)
{
    device->ImageImgui(texture, x, y, width, height);
}

bool Islander::Imgui::ImageButton(Islander::IDevice* device, int id, int texture, float x, float y, float sx, float sy)
{
    return device->ImageButtonImgui(id, texture, x, y, sx, sy);
}

