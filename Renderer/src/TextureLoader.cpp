#include "TextureLoader.h"
#include <png.h>
#include <fstream>
#include <string.h>
#include <Logger.h>
#include <sstream>

using namespace Islander::Renderer;

namespace Islander
{
	namespace Renderer
	{
        void user_read_data(png_structp png_ptr, png_bytep data, png_size_t length)
        {
            char *stream = (char*)png_get_io_ptr(png_ptr);

            auto io_state = png_get_io_state(png_ptr);

            memcpy(data, stream, length);

            stream += length;
            png_set_read_fn(png_ptr, stream, user_read_data);
        }

		void user_read_data_stream(png_structp png_ptr, png_bytep data, png_size_t length)
		{
			std::istream *stream = (std::istream*)png_get_io_ptr(png_ptr);

			//auto io_state = png_get_io_state(png_ptr);

			stream->read((char*)data, length);
        }

       unsigned char* LoadTexture(char* stream, int flags, TextureLoadInfo& info, png_rw_ptr read_data_fn)
       {
			png_structp png_ptr = png_create_read_struct(PNG_LIBPNG_VER_STRING, (png_voidp)nullptr, nullptr, nullptr);

			if (!png_ptr)
				return nullptr;

			png_infop info_ptr = png_create_info_struct(png_ptr);

			if (!info_ptr)
			{
				png_destroy_read_struct(&png_ptr, (png_infopp)nullptr, (png_infopp)nullptr);
				return nullptr;
			}

			png_set_read_fn(png_ptr, stream, read_data_fn);

			png_read_png(png_ptr, info_ptr, PNG_TRANSFORM_IDENTITY, nullptr);

			png_bytep* row_pointers = png_get_rows(png_ptr, info_ptr);
			//png_size_t rowBytes = png_get_rowbytes(png_ptr, info_ptr);

			int bitdepth = png_get_bit_depth(png_ptr, info_ptr);
			int colourType = png_get_color_type(png_ptr, info_ptr);

			info._width = png_get_image_width(png_ptr, info_ptr);
			info._height = png_get_image_height(png_ptr, info_ptr);

			info._size = info._width*info._height * 4;

            // Colour Type bit flags
            // --------------------
            // 2 - Colour (R,G,B)
            // 4 - Alpha

			unsigned char * data = new unsigned char[info._size];
			if (colourType == 6)
			{
				if (bitdepth == 8)
				{
					for (int i = 0; i < info._height; i++)
					{
						memcpy(data + info._width* i * 4, row_pointers[i], info._width*sizeof(unsigned char) * 4);
					}
				}
				else if (bitdepth == 16)
				{
					for (int i = 0; i < info._height; i++)
					{
						for (int j = 0; j < info._width; j++)
						{
							int index = i * info._width * 4 + 4 * j;
							data[index] = row_pointers[i][8 * j];
							data[index + 1] = row_pointers[i][8 * j + 2];
							data[index + 2] = row_pointers[i][8 * j + 4];
							data[index + 3] = row_pointers[i][8 * j + 6];
						}
					}
				}
			}
            else if (colourType == 2)
            {
                if (bitdepth == 8)
                {
                    for (int i = 0; i < info._height; i++)
                    {
                        for (int j = 0; j < info._width; j++)
                        {
                            int index = i * info._width * 4 + 4 * j;

                            data[index] = row_pointers[i][3 * j];
                            data[index + 1] = row_pointers[i][3 * j + 1];
                            data[index + 2] = row_pointers[i][3 * j + 2];
                            data[index + 3] = 255;
                        }
                    }
                }
            }

			if ((flags & TEXTURE_LOADER_FLIP_Y) == TEXTURE_LOADER_FLIP_Y)
			{
				unsigned char* temp = new unsigned char[info._width * sizeof(unsigned char) * 4];
				int width = info._width * sizeof(unsigned char) * 4;
				for (int i = 0; i < info._height/2; i++)
				{
					memcpy(temp, data + info._width* i * 4, width);
					memcpy(data + info._width* i * 4, data + (info._height - i - 1) * info._width * 4, width);
					memcpy(data + (info._height - i - 1) * info._width * 4, temp, width);
				}
				delete[] temp;
			}

			png_destroy_read_struct(&png_ptr, &info_ptr, (png_infopp)nullptr);

			return data;
		}

       unsigned char* LoadTexture(char* data, int flags, TextureLoadInfo& info)
       {
           return LoadTexture(data, flags, info, user_read_data);
       }

       unsigned char* LoadTexture(const char* filename, int flags, TextureLoadInfo& info)
       {
           std::ifstream stream(filename, std::ios::in | std::ios::binary);
           if (stream.fail())
           {
               std::stringstream ss;
               ss << "Stream failed to load: "
                   << filename;
               Logger::GetLogger()->Log(ss.str(),
                   LOGGING_RESOURCES);
               return nullptr;
           }

           auto data = LoadTexture((char*)&stream, flags, info, user_read_data_stream);
           stream.close();
           return data;
       }
	}
}
