//#include "StdAfx.h"
#include "FontEngine.h"
#include "Renderer.h"
#include "GraphicsBuffer.h"
#include "TextTokenizer.h"
#include <Material.h>
#include <FileSystem.h>
#include <iostream>
#include <fstream>
#include <vector>
#include <sstream>
#include <Logger.h>

using namespace Islander::Renderer;
using namespace Islander;

FontEngine::FontEngine(FontDescription& font, IRenderer* renderer)
	:
	_renderer(renderer)
{
	//parse file

	memset(&characters, 0, sizeof(FontCharacter) * 200);

	std::stringstream builder;
	builder << font.filedef << ".bin";

	std::ifstream file;
	FileSystem::ReadFile(file, builder.str(), true);

	file.seekg(0, std::ios::end);
	int bytelength = file.tellg();

	file.seekg(0);

	newline = 0;
	space = 0;

	std::vector<int> indices;

	for (int i = 0; i < bytelength; i+=sizeof(char)+4*sizeof(int))
	{
		int pos;
		file.read((char*)&pos, sizeof(int));

		if (pos >= 0 && pos < 200)
		{
			RECTANGLE& rect = characters[pos].rect;

			file.read((char*)&rect.x, sizeof(int));
			file.read((char*)&rect.y, sizeof(int));
			file.read((char*)&rect.width, sizeof(int));
			file.read((char*)&rect.height, sizeof(int));

			newline = std::max(rect.height, newline);
			space = std::max(rect.width, space);

			indices.push_back(pos);
		}
		else
		{
			file.seekg(sizeof(int) * 4, std::ios::cur);
		}
	}

	newline *= 1.2f;
	space *= 0.8f;

	file.close();

	MaterialTexture* textureSet = new MaterialTexture[indices.size()];
	for (int i = 0; i < indices.size(); i++)
	{
		int index = indices[i];
		MaterialTexture& texture = textureSet[i];
		RECTANGLE& rect = characters[index].rect;
		texture._position.SetX(rect.x / 1024.0f);
		texture._position.SetY(rect.y / 1024.0f);
		texture._scale.SetX(rect.width / 1024.0f);
		texture._scale.SetY(rect.height / 1024.0f);
	}

	int atlasImg = _renderer->CreateTextureAtlas(font.img, textureSet, indices.size());
	_supportsAtlas = atlasImg != -1;
	if (_supportsAtlas)
	{
		font.img = atlasImg;
	}

	for (int i = 0; i < indices.size(); i++)
	{
		int index = indices[i];
		characters[index].texture = textureSet[i];
	}

	delete[] textureSet;
}

FontEngine::~FontEngine(void)
{
}

/*enum TokenType
{
	TOKEN_TYPE_NONE,
	TOKEN_TYPE_TEXT,
	TOKEN_TYPE_SPACE,
	TOKEN_TYPE_BOLD_BEGIN,
	TOKEN_TYPE_BOLD_END,
	TOKEN_TYPE_STRIKE_BEGIN,
	TOKEN_TYPE_STRIKE_END,
	TOKEN_TYPE_LINK_BEGIN,
	TOKEN_TYPE_LINK_END,
	TOKEN_TYPE_NEWLINE,
	TOKEN_TYPE_TAB
};

enum SymbolType
{
	SYMBOL_TYPE_NONE,
	SYMBOL_TYPE_TEXT,
	SYMBOL_TYPE_SPACE,
	SYMBOL_TYPE_ANGLE_LEFT,
	SYMBOL_TYPE_ANGLE_RIGHT,
	SYMBOL_TYPE_NEWLINE,
	SYMBOL_TYPE_FORWARD_SLASH,
	SYMBOL_TYPE_TAB,
	SYMBOL_TYPE_EOF
};*/

/*
<b>Bold Text</b>
< b >Bold Text</ b>
< b Bold Text</>
<bbb Bold Text ></b>
*/

/*struct Token
{
	TokenType _type;
	std::string _text;
	int _index;
};

SymbolType GetNextSymbol(const std::string& text, int* i)
{
	if (*i >= text.length())
	{
		return SYMBOL_TYPE_EOF;
	}

	char c = text[*i];
	++*i;

	if (c == ' ')
	{
		return SYMBOL_TYPE_SPACE;
	}
	else if (c == '\n')
	{
		return SYMBOL_TYPE_NEWLINE;
	}
	else if (c == '\t')
	{
		return SYMBOL_TYPE_TAB;
	}
	else if (c == '/')
	{
		return SYMBOL_TYPE_FORWARD_SLASH;
	}
	else if (c == '<')
	{
		return SYMBOL_TYPE_ANGLE_LEFT;
	}
	else if (c == '>')
	{
		return SYMBOL_TYPE_ANGLE_RIGHT;
	}
	else if (c >= 0 && c < 200)
	{
		return SYMBOL_TYPE_TEXT;
	}

	return SYMBOL_TYPE_NONE;
}

void ParseText(const std::string& text, std::vector<Token>& tokens)
{
	Token token;
	int index = 0;
	while (index < text.length())
	{
		std::string word;
		int startIndex = index;
		auto t = GetNextSymbol(text, &index);

		switch (t)
		{
		case SYMBOL_TYPE_NONE:
			// what?
			break;
		case SYMBOL_TYPE_TEXT:
		{
			word += text[index - 1];
			t = GetNextSymbol(text, &index);
			while (t == SYMBOL_TYPE_TEXT)
			{
				word += text[index - 1];
				t = GetNextSymbol(text, &index);
			}

			if (t != SYMBOL_TYPE_EOF)
			{
				index--;
			}

			token._type = TOKEN_TYPE_TEXT;
		}
		break;
		case SYMBOL_TYPE_SPACE:
			token._type = TOKEN_TYPE_SPACE;
			break;
		case SYMBOL_TYPE_ANGLE_LEFT:
		{
			int temp = index;
			bool success = false;
			auto next = GetNextSymbol(text, &temp);
			if (next == SYMBOL_TYPE_TEXT &&
				text[temp - 1] == 'b')
			{
				if (GetNextSymbol(text, &temp) == SYMBOL_TYPE_ANGLE_RIGHT)
				{
					success = true;
					word = "<b>";
					index = temp;
					token._type = TOKEN_TYPE_BOLD_BEGIN;
				}
			}
			else if (next == SYMBOL_TYPE_TEXT &&
				text[temp - 1] == 's')
			{
				if (GetNextSymbol(text, &temp) == SYMBOL_TYPE_ANGLE_RIGHT)
				{
					success = true;
					word = "<s>";
					index = temp;
					token._type = TOKEN_TYPE_STRIKE_BEGIN;
				}
			}
			else if (next == SYMBOL_TYPE_TEXT &&
				text[temp - 1] == 'l')
			{
				if (GetNextSymbol(text, &temp) == SYMBOL_TYPE_ANGLE_RIGHT)
				{
					success = true;
					word = "<l>";
					index = temp;
					token._type = TOKEN_TYPE_LINK_BEGIN;
				}
			}
			else if (next == SYMBOL_TYPE_FORWARD_SLASH)
			{
				next = GetNextSymbol(text, &temp);
				if (next == SYMBOL_TYPE_TEXT &&
					text[temp - 1] == 'b')
				{
					if (GetNextSymbol(text, &temp) == SYMBOL_TYPE_ANGLE_RIGHT)
					{
						success = true;
						word = "</b>";
						index = temp;
						token._type = TOKEN_TYPE_BOLD_END;
					}
				}
				else if (next == SYMBOL_TYPE_TEXT &&
					text[temp - 1] == 's')
				{
					if (GetNextSymbol(text, &temp) == SYMBOL_TYPE_ANGLE_RIGHT)
					{
						success = true;
						word = "</s>";
						index = temp;
						token._type = TOKEN_TYPE_STRIKE_END;
					}
				}
				else if (next == SYMBOL_TYPE_TEXT &&
					text[temp - 1] == 'l')
				{
					if (GetNextSymbol(text, &temp) == SYMBOL_TYPE_ANGLE_RIGHT)
					{
						success = true;
						word = "</l>";
						index = temp;
						token._type = TOKEN_TYPE_LINK_END;
					}
				}
			}

			if (!success)
			{
				word = "<";
				token._type = TOKEN_TYPE_TEXT;
			}
		}
		break;
		case SYMBOL_TYPE_ANGLE_RIGHT:
			word = ">";
			token._type = TOKEN_TYPE_TEXT;
			break;
		case SYMBOL_TYPE_NEWLINE:
			token._type = TOKEN_TYPE_NEWLINE;
			break;
		case SYMBOL_TYPE_FORWARD_SLASH:
			word = "/";
			token._type = TOKEN_TYPE_TEXT;
			break;
		case SYMBOL_TYPE_TAB:
			token._type = TOKEN_TYPE_TAB;
			break;
		default:
			break;
		}

		token._text = word;
		token._index = startIndex;

		tokens.push_back(token);
	}
}*/

void MeasureWords(const std::vector<Token>& tokens, FontCharacter* rects, std::vector<float>& widths)
{
	for (int i = 0; i < tokens.size(); i++)
	{
		auto& tok = tokens[i];

		if (tok._type == TOKEN_TYPE_TEXT)
		{
			float width = 0;
			for (int i = 0; i < tok._text.length(); i++)
			{
				auto c = tok._text[i];
				if (c >= 0 && c < 200)
				{
					width += rects[c].rect.width + 5;
				}
			}
			widths.push_back(width);
		}
	}
}

void FontEngine::MeasureText(component_text* data, component_transform* transform)
{
	auto textsprite = (TextData*)data->data;
	auto len = strlen(textsprite->_text);
	auto& text = textsprite->_text;
	
    if (len <= 0)
    {
        Rect& rect = textsprite->_rect;
        rect.x = 0;
        rect.y = 0;
        rect.width = 0;
        rect.height = 0;
        return;
    }

	int wrapwidth = 0;
	if (transform && data->wrap)
	{
		wrapwidth = transform->sx;
	}

	std::vector<Token> tokens;
	TextTokenizer::GetTokens(text, tokens);

	std::vector<float> wordWidths;
	MeasureWords(tokens, characters, wordWidths);

	int wordcount = 0;

	//create a list of quads with correct texcoords
	int x =0; int y=0;
	int maxwidth=0;
	for (auto& token : tokens)
	{
		switch (token._type)
		{
			case TOKEN_TYPE_SPACE:
			{
				x += space;
				if (wrapwidth > 0 && wordcount + 1 < wordWidths.size() && wordWidths[wordcount + 1] + x > wrapwidth)
				{
					y -= newline; maxwidth = std::max(x, maxwidth); x = 0;
				}
				wordcount++;
				continue;
			}
			case TOKEN_TYPE_NEWLINE:
				y -= newline; maxwidth = std::max(x, maxwidth); x = 0; wordcount++;
				continue;
			case TOKEN_TYPE_TAB:
			{
				int tabpos = 0; int tabcount = 0;
				while (x > tabpos && tabcount < 30) { tabpos += textsprite->tabs[tabcount++]; }
				x = tabpos;
				continue;
			}
			case TOKEN_TYPE_TEXT:
			{
				for (int i = 0; i < token._text.length(); i++)
				{
					if (token._text[i] >= 0 && token._text[i] < 200)
					{
						RECTANGLE& rect = characters[token._text[i]].rect;

						x += rect.width + 5;
					}
				}
			}
		}
	}

	Rect& rect = textsprite->_rect;
	rect.x=0;
	rect.y=0;
	rect.width=std::max(x,maxwidth);
	rect.height=-y+newline;
}

void AppendBuffer(std::vector<float>& buffer, float value, int& position)
{
	buffer.push_back(value);
	position++;
}

int FontEngine::MakeText(component_text* data, component_transform* transform, int width, int height)
{
	auto extra = (TextData*)data->data;

	auto len = strlen(extra->_text);
	auto& text = extra->_text;
	
	if (len <= 0)
		return 0;

	int wrapwidth = 0;
	if (transform && data->wrap)
	{
		wrapwidth = transform->sx;
	}

	width /= 2;
	height /= 2;

	std::vector<Token> tokens;
	TextTokenizer::GetTokens(text, tokens);

	std::vector<float> wordWidths;
	MeasureWords(tokens, characters, wordWidths);

	int wordcount = 0;
	bool bold = false;
	bool strike = false;
	bool link = false;

	int count=0;
	std::vector<float> buffer;
	int bufferLength = _supportsAtlas ? 36 : 30;

	extra->_linkcount = 0;

	//create a list of quads with correct texcoords
	int position = 0;
	int x =0; int y=0;
	int maxwidth=0;
	for (auto& token : tokens)
	{
		switch (token._type)
		{
			case TOKEN_TYPE_SPACE:
			{
				x += space;
				if (wrapwidth > 0 && wordcount + 1 < wordWidths.size() && wordWidths[wordcount + 1] + x > wrapwidth)
				{
					y -= newline; maxwidth = std::max(x, maxwidth); x = 0;

					if (link)
					{
						extra->_linkcount++;
						extra->_links[extra->_linkcount]._startIndex = extra->_links[extra->_linkcount-1]._startIndex;
						auto& rect = extra->_links[extra->_linkcount]._rect;
						rect.width = rect.height = 0;
						rect.x = x; rect.y = y;
					}
				}
				wordcount++;
				continue;
			}
			case TOKEN_TYPE_NEWLINE:
				y -= newline; maxwidth = std::max(x, maxwidth); x = 0; wordcount++;
				continue;
			case TOKEN_TYPE_TAB:
			{
				int tabpos = 0; int tabcount = 0;
				while (x > tabpos && tabcount < 30) { tabpos += extra->tabs[tabcount++]; }
				x = tabpos;
				continue;
			}
			case TOKEN_TYPE_BOLD_BEGIN:
				bold = true;
				continue;
			case TOKEN_TYPE_BOLD_END:
				bold = false;
				continue;
			case TOKEN_TYPE_STRIKE_BEGIN:
				strike = true;
				continue;
			case TOKEN_TYPE_STRIKE_END:
				strike = false;
				continue;
			case TOKEN_TYPE_LINK_BEGIN:
			{
				extra->_links[extra->_linkcount]._startIndex = token._index + token._text.size();
				auto& rect = extra->_links[extra->_linkcount]._rect;
				rect.width = rect.height = 0;
				rect.x = x; rect.y = y;
				link = true;
				continue;
			}
			case TOKEN_TYPE_LINK_END:
			{
				int startIndex = extra->_links[extra->_linkcount]._startIndex;
				extra->_links[extra->_linkcount]._length = token._index - extra->_links[extra->_linkcount]._startIndex;

				for (int i = extra->_linkcount - 1; i >= 0 && extra->_links[i]._startIndex == startIndex; i--)
				{
					extra->_links[i]._length = extra->_links[extra->_linkcount]._length;
				}

				extra->_linkcount++;
				link = false;
				continue;
			}
			case TOKEN_TYPE_TEXT:
			{
				for (int i = 0; i < token._text.length(); i++)
				{
					if (!(token._text[i] >= 0 && token._text[i] < 200))
					{
						continue;
					}

					FontCharacter& character = characters[token._text[i]];
					RECTANGLE& rect = character.rect;
					MaterialTexture& tex = character.texture;
					
					float textStyle = (bold ? 1 : 0) + (link ? 4 : 0);

					if (_supportsAtlas)
					{
						AppendBuffer(buffer, x / (float)width, position);
						AppendBuffer(buffer, y / (float)height, position);
						AppendBuffer(buffer, 0, position);
						AppendBuffer(buffer, 0, position);
						AppendBuffer(buffer, tex._atlasIndex, position);
						AppendBuffer(buffer, textStyle, position);

						AppendBuffer(buffer, x / (float)width, position);
						AppendBuffer(buffer, (y - rect.height) / (float)height, position);
						AppendBuffer(buffer, 0, position);
						AppendBuffer(buffer, tex._scale.GetY(), position);
						AppendBuffer(buffer, tex._atlasIndex, position);
						AppendBuffer(buffer, textStyle, position);

						AppendBuffer(buffer, (rect.width + x) / (float)width, position);
						AppendBuffer(buffer, y / (float)height, position);
						AppendBuffer(buffer, tex._scale.GetX(), position);
						AppendBuffer(buffer, 0, position);
						AppendBuffer(buffer, tex._atlasIndex, position);
						AppendBuffer(buffer, textStyle, position);

						AppendBuffer(buffer, (rect.width + x) / (float)width, position);
						AppendBuffer(buffer, y / (float)height, position);
						AppendBuffer(buffer, tex._scale.GetX(), position);
						AppendBuffer(buffer, 0, position);
						AppendBuffer(buffer, tex._atlasIndex, position);
						AppendBuffer(buffer, textStyle, position);

						AppendBuffer(buffer, x / (float)width, position);
						AppendBuffer(buffer, (-rect.height + y) / (float)height, position);
						AppendBuffer(buffer, 0, position);
						AppendBuffer(buffer, tex._scale.GetY(), position);
						AppendBuffer(buffer, tex._atlasIndex, position);
						AppendBuffer(buffer, textStyle, position);

						AppendBuffer(buffer, (rect.width + x) / (float)width, position);
						AppendBuffer(buffer, (y - rect.height) / (float)height, position);
						AppendBuffer(buffer, tex._scale.GetX(), position);
						AppendBuffer(buffer, tex._scale.GetY(), position);
						AppendBuffer(buffer, tex._atlasIndex, position);
						AppendBuffer(buffer, textStyle, position);
					}
					else
					{
						AppendBuffer(buffer, x / (float)width, position);
						AppendBuffer(buffer, y / (float)height, position);
						AppendBuffer(buffer, tex._position.GetX(), position);
						AppendBuffer(buffer, tex._position.GetY(), position);
						AppendBuffer(buffer, textStyle, position);

						AppendBuffer(buffer, x / (float)width, position);
						AppendBuffer(buffer, (y - rect.height) / (float)height, position);
						AppendBuffer(buffer, tex._position.GetX(), position);
						AppendBuffer(buffer, tex._scale.GetY() + tex._position.GetY(), position);
						AppendBuffer(buffer, textStyle, position);

						AppendBuffer(buffer, (rect.width + x) / (float)width, position);
						AppendBuffer(buffer, y / (float)height, position);
						AppendBuffer(buffer, tex._position.GetX() + tex._scale.GetX(), position);
						AppendBuffer(buffer, tex._position.GetY(), position);
						AppendBuffer(buffer, textStyle, position);

						AppendBuffer(buffer, (rect.width + x) / (float)width, position);
						AppendBuffer(buffer, y / (float)height, position);
						AppendBuffer(buffer, tex._position.GetX() + tex._scale.GetX(), position);
						AppendBuffer(buffer, tex._position.GetY(), position);
						AppendBuffer(buffer, textStyle, position);

						AppendBuffer(buffer, x / (float)width, position);
						AppendBuffer(buffer, (-rect.height + y) / (float)height, position);
						AppendBuffer(buffer, tex._position.GetX(), position);
						AppendBuffer(buffer, tex._position.GetY() + tex._scale.GetY(), position);
						AppendBuffer(buffer, textStyle, position);

						AppendBuffer(buffer, (rect.width + x) / (float)width, position);
						AppendBuffer(buffer, (y - rect.height) / (float)height, position);
						AppendBuffer(buffer, tex._position.GetX() + tex._scale.GetX(), position);
						AppendBuffer(buffer, tex._position.GetY() + tex._scale.GetY(), position);
						AppendBuffer(buffer, textStyle, position);
					}

					if (strike)
					{
						if (_supportsAtlas)
						{
							AppendBuffer(buffer, x / (float)width, position);
							AppendBuffer(buffer, (y - rect.height * 0.45f) / (float)height, position);
							AppendBuffer(buffer, 0, position);
							AppendBuffer(buffer, 0, position);
							AppendBuffer(buffer, tex._atlasIndex, position);
							AppendBuffer(buffer, 2 | (bold ? 1 : 0), position);

							AppendBuffer(buffer, x / (float)width, position);
							AppendBuffer(buffer, (y - rect.height * 0.55f) / (float)height, position);
							AppendBuffer(buffer, 0, position);
							AppendBuffer(buffer, tex._scale.GetY(), position);
							AppendBuffer(buffer, tex._atlasIndex, position);
							AppendBuffer(buffer, 2 | (bold ? 1 : 0), position);

							AppendBuffer(buffer, (rect.width + x) / (float)width, position);
							AppendBuffer(buffer, (y - rect.height * 0.45f) / (float)height, position);
							AppendBuffer(buffer, tex._scale.GetX(), position);
							AppendBuffer(buffer, 0, position);
							AppendBuffer(buffer, tex._atlasIndex, position);
							AppendBuffer(buffer, 2 | (bold ? 1 : 0), position);

							AppendBuffer(buffer, (rect.width + x) / (float)width, position);
							AppendBuffer(buffer, (y - rect.height * 0.45f) / (float)height, position);
							AppendBuffer(buffer, tex._scale.GetX(), position);
							AppendBuffer(buffer, 0, position);
							AppendBuffer(buffer, tex._atlasIndex, position);
							AppendBuffer(buffer, 2 | (bold ? 1 : 0), position);

							AppendBuffer(buffer, x / (float)width, position);
							AppendBuffer(buffer, (-rect.height* 0.55f + y ) / (float)height, position);
							AppendBuffer(buffer, 0, position);
							AppendBuffer(buffer, tex._scale.GetY(), position);
							AppendBuffer(buffer, tex._atlasIndex, position);
							AppendBuffer(buffer, 2 | (bold ? 1 : 0), position);

							AppendBuffer(buffer, (rect.width + x) / (float)width, position);
							AppendBuffer(buffer, (y - rect.height*0.55f) / (float)height, position);
							AppendBuffer(buffer, tex._scale.GetX(), position);
							AppendBuffer(buffer, tex._scale.GetY(), position);
							AppendBuffer(buffer, tex._atlasIndex, position);
							AppendBuffer(buffer, 2 | (bold ? 1 : 0), position);
						}
						else
						{
							AppendBuffer(buffer, x / (float)width, position);
							AppendBuffer(buffer, (y - rect.height*0.45f) / (float)height, position);
							AppendBuffer(buffer, tex._position.GetX(), position);
							AppendBuffer(buffer, tex._position.GetY(), position);
							AppendBuffer(buffer, 2 | (bold ? 1 : 0), position);

							AppendBuffer(buffer, x / (float)width, position);
							AppendBuffer(buffer, (y - rect.height*0.55f) / (float)height, position);
							AppendBuffer(buffer, tex._position.GetX(), position);
							AppendBuffer(buffer, tex._scale.GetY() + tex._position.GetY(), position);
							AppendBuffer(buffer, 2 | (bold ? 1 : 0), position);

							AppendBuffer(buffer, (rect.width + x) / (float)width, position);
							AppendBuffer(buffer, (y - rect.height*0.45f) / (float)height, position);
							AppendBuffer(buffer, tex._position.GetX() + tex._scale.GetX(), position);
							AppendBuffer(buffer, tex._position.GetY(), position);
							AppendBuffer(buffer, 2 | (bold ? 1 : 0), position);

							AppendBuffer(buffer, (rect.width + x) / (float)width, position);
							AppendBuffer(buffer, (y - rect.height*0.45f) / (float)height, position);
							AppendBuffer(buffer, tex._position.GetX() + tex._scale.GetX(), position);
							AppendBuffer(buffer, tex._position.GetY(), position);
							AppendBuffer(buffer, 2 | (bold ? 1 : 0), position);

							AppendBuffer(buffer, x / (float)width, position);
							AppendBuffer(buffer, (-rect.height*0.55f + y) / (float)height, position);
							AppendBuffer(buffer, tex._position.GetX(), position);
							AppendBuffer(buffer, tex._position.GetY() + tex._scale.GetY(), position);
							AppendBuffer(buffer, 2 | (bold ? 1 : 0), position);

							AppendBuffer(buffer, (rect.width + x) / (float)width, position);
							AppendBuffer(buffer, (y - rect.height*0.55f) / (float)height, position);
							AppendBuffer(buffer, tex._position.GetX() + tex._scale.GetX(), position);
							AppendBuffer(buffer, tex._position.GetY() + tex._scale.GetY(), position);
							AppendBuffer(buffer, 2 | (bold ? 1 : 0), position);
						}
					}

					if (link)
					{
						auto& linkRect = extra->_links[extra->_linkcount]._rect;
						linkRect.width += rect.width + 5;
						linkRect.height = std::max(linkRect.height, rect.height); // TODO: this assumes it spans a single line
					}

					x += rect.width + 5;
					count++;
				}
				break;
			}
		}
	}
	
	if (count==0)
	{
		return 0;
	}

	int bufferid = _renderer->CreateVertexBuffer(USAGE_STATIC, buffer.size() * sizeof(float), buffer.data(), bufferLength / 6 * sizeof(float), BUFFERING_SINGLE)->ID();
	if (bufferid != -1)
	{
		data->_vertid = bufferid;

		Rect& rect = extra->_rect;
		rect.x = 0;
		rect.y = 0;
		rect.width = std::max(x, maxwidth);
		rect.height = -y + newline;

		return len;
	}

	return 0;
}

Font::Font(FontDescription& description, IRenderer* renderer)
	:
	desc(description),
	engine(description, renderer)
{}
