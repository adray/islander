#include "ParticleSystem\ParticleSystem.h"
#include "ParticleSystem\ParticleRenderer.h"
#include "IResourceLoader.h"
#include "Effekseer.h"

using namespace Islander::Renderer;

namespace Islander
{
    namespace Renderer
    {
        struct ParticleSystem
        {
            ::Effekseer::RefPtr<Effekseer::Manager> manager;
            ParticleRenderer* renderer;
            std::vector<::Effekseer::EffectRef> effects;
        };
    }
}

ParticleSystem* Islander::Renderer::CreateParticleSystem(ParticleRenderer* renderer, IDevice* device)
{
    ParticleSystem* system = new ParticleSystem();
    system->renderer = renderer;
    system->renderer->device = device;

    system->manager = Effekseer::Manager::Create(8000);

    system->manager->SetSpriteRenderer(renderer->spriteRenderer);
    system->manager->SetRibbonRenderer(renderer->ribbonRenderer);
    system->manager->SetRingRenderer(renderer->ringRenderer);
    system->manager->SetTrackRenderer(renderer->trackRenderer);
    system->manager->SetModelRenderer(renderer->modelRenderer);

    system->manager->SetTextureLoader(renderer->textureLoader);
    system->manager->SetModelLoader(renderer->modelLoader);
    system->manager->SetMaterialLoader(renderer->materialLoader);

    //system->manager->SetCurveLoader(Effekseer::MakeRefPtr<Effekseer::CurveLoader>());

    // We are using a LH coordinate system
    system->manager->SetCoordinateSystem(::Effekseer::CoordinateSystem::LH);

    return system;
}

int Islander::Renderer::LoadParticleEffect(ParticleSystem* system, void* data, int size)
{
    // This may not load all associated resources unless they have been registered.
    auto effect = Effekseer::Effect::Create(system->manager, data, size);
    system->effects.push_back(effect);
    return system->effects.size() - 1;
}

int Islander::Renderer::PlayParticleEffect(Islander::Renderer::ParticleSystem* system, int effectId, float x, float y, float z)
{
    return system->manager->Play(system->effects[effectId], x, y, z);
}

void Islander::Renderer::StopParticleEffect(Islander::Renderer::ParticleSystem* system, int id)
{
    system->manager->StopEffect(id);
}

void Islander::Renderer::StopAllParticleEffects(Islander::Renderer::ParticleSystem* system)
{
    system->manager->StopAllEffects();
}

bool Islander::Renderer::IsParticleEffectAlive(Islander::Renderer::ParticleSystem* system, int id)
{
    return system->manager->Exists(id);
}

void Islander::Renderer::MoveParticleEffect(ParticleSystem* system, int id, float x, float y, float z)
{
    system->manager->AddLocation(id, ::Effekseer::Vector3D(x, y, z));
}

void Islander::Renderer::SetLocationParticleEffect(ParticleSystem* system, int id, float x, float y, float z)
{
    system->manager->SetLocation(id, x, y, z);
}

void Islander::Renderer::SetRotationParticleEffect(ParticleSystem* system, int id, float x, float y, float z)
{
    system->manager->SetRotation(id, x, y, z);
}

void Islander::Renderer::SetTargetLocationParticleEffect(ParticleSystem* system, int id, float x, float y, float z)
{
    system->manager->SetTargetLocation(id, x, y, z);
}

::Effekseer::EffectRef Islander::Renderer::GetParticleEffect(ParticleSystem* system, int id)
{
    return system->effects[id];
}

void Islander::Renderer::UpdateParticleSystem(ParticleSystem* system, float deltaTime)
{
    system->renderer->elapsedTime += deltaTime;
    system->manager->Update();
}

void Islander::Renderer::DrawParticleSystem(ParticleSystem* system)
{
    system->manager->Draw();
}

void Islander::Renderer::DestroyParticleSystem(ParticleSystem* system)
{
    delete system;
}

