#include "RenderGrid.h"
#include "Frustum.h"
#include "Matrix.h"
#include <algorithm>
#include <assert.h>
#include <Component.h>

using namespace Islander::Renderer;
using namespace Islander::Numerics;


// DESIGN:
// SETUP
// 1. Add items to consider.
// 2. Generate grid bounds
// 3. Determine which cells items are in (via bit flags)
// USAGE
// 1. Loop through cells which have items and perform frustum culling on cells.
// 2. Generate bit flags which for the cells which are in the frustum
// 3. For each item compare the bit-flags to determine if can be rendered
//

void Islander::Renderer::RenderGridInitialize(RenderGrid& grid)
{
    grid.dirty = false;
    memset(&grid.box, 0, sizeof(grid.box));
}

void Islander::Renderer::RenderGridAddEntity(RenderGrid& grid, const RenderGridEntity& entity)
{
    grid.dirty = true;
    grid.entities.push_back(entity);
}

void Islander::Renderer::RenderGridUpdateEntity(RenderGrid& grid, const RenderGridEntity& entity)
{
    grid.dirty = true;
    for (auto& ent : grid.entities)
    {
        if (ent.bitflags == entity.bitflags)
        {
            ent = entity;
            break;
        }
    }
}

void Islander::Renderer::RenderGridClearGrid(RenderGrid& grid)
{
    grid.dirty = true;
    grid.entities.clear();
}

void Islander::Renderer::RenderGridResizeGrid(RenderGrid& grid)
{
    if (!grid.dirty)
    {
        return;
    }

    grid.dirty = false;

    for (auto& entity: grid.entities)
    {
        std::fill_n(entity.bitflags, RENDER_GRID_BIT_FLAG_BYTES, 0);
    }

    if (grid.entities.size() > 0)
    {
        // Find out the bounding volume the grid should cover.
        AABB& box = grid.box;
        auto& entity = grid.entities[0];
        AABB meshbox = entity.box;
        CreateTransformedAABB(entity.pos[0],
            entity.pos[1],
            entity.pos[2],
            entity.rotation[0],
            entity.rotation[1],
            entity.rotation[2],
            entity.scale[0],
            entity.scale[1],
            entity.scale[2],
            meshbox);
        box.max[0] = meshbox.max[0];
        box.max[1] = meshbox.max[1];
        box.max[2] = meshbox.max[2];
        box.min[0] = meshbox.min[0];
        box.min[1] = meshbox.min[1];
        box.min[2] = meshbox.min[2];

        for (int i = 1; i < grid.entities.size(); i++)
        {
            auto& entity = grid.entities[i];
            auto meshbox = entity.box;
            CreateTransformedAABB(entity.pos[0],
                entity.pos[1],
                entity.pos[2],
                entity.rotation[0],
                entity.rotation[1],
                entity.rotation[2],
                entity.scale[0],
                entity.scale[1],
                entity.scale[2],
                meshbox);
            box.max[0] = std::max(box.max[0], meshbox.max[0]);
            box.max[1] = std::max(box.max[1], meshbox.max[1]);
            box.max[2] = std::max(box.max[2], meshbox.max[2]);
            box.min[0] = std::min(box.min[0], meshbox.min[0]);
            box.min[1] = std::min(box.min[1], meshbox.min[1]);
            box.min[2] = std::min(box.min[2], meshbox.min[2]);
        }

        // Divide the grid into N x N x N cells.
        float cellSizeX = (box.max[0] - box.min[0]) / RENDER_GRID_COUNT;
        float cellSizeY = (box.max[1] - box.min[1]) / RENDER_GRID_COUNT;
        float cellSizeZ = (box.max[2] - box.min[2]) / RENDER_GRID_COUNT;

        for (int i = 0; i < grid.cells.size(); i++)
        {
            int x = i % RENDER_GRID_COUNT;
            int y = (i / RENDER_GRID_COUNT) % RENDER_GRID_COUNT;
            int z = i / (RENDER_GRID_COUNT * RENDER_GRID_COUNT);
            auto& cell = grid.cells[i];
            cell.box.max[0] = box.min[0] + x * cellSizeX + cellSizeX;
            cell.box.min[0] = box.min[0] + x * cellSizeX;
            cell.box.max[1] = box.max[1] + y * cellSizeY + cellSizeY;
            cell.box.min[1] = box.min[1] + y * cellSizeY;
            cell.box.max[2] = box.min[2] + z * cellSizeZ + cellSizeZ;
            cell.box.min[2] = box.min[2] + z * cellSizeZ;
            cell.x = x;
            cell.y = y;
            cell.z = z;
            AABBToSphere(cell.box, cell.centre, &cell.radius);
        }

        // Assign objects to cells.
        for (int i = 0; i < grid.entities.size(); i++)
        {
            auto& entity = grid.entities[i];
            auto meshbox = entity.box;
            CreateTransformedAABB(entity.pos[0],
                entity.pos[1],
                entity.pos[2],
                entity.rotation[0],
                entity.rotation[1],
                entity.rotation[2],
                entity.scale[0],
                entity.scale[1],
                entity.scale[2],
                meshbox);
            int minCellX = std::min(RENDER_GRID_COUNT - 1, (int)((meshbox.min[0] - box.min[0]) / cellSizeX));
            int maxCellX = std::min(RENDER_GRID_COUNT - 1, (int)((meshbox.max[0] - box.min[0]) / cellSizeX));

            int minCellY = std::min(RENDER_GRID_COUNT - 1, (int)((meshbox.min[1] - box.min[1]) / cellSizeY));
            int maxCellY = std::min(RENDER_GRID_COUNT - 1, (int)((meshbox.max[1] - box.min[1]) / cellSizeY));

            int minCellZ = std::min(RENDER_GRID_COUNT - 1, (int)((meshbox.min[2] - box.min[2]) / cellSizeZ));
            int maxCellZ = std::min(RENDER_GRID_COUNT - 1, (int)((meshbox.max[2] - box.min[2]) / cellSizeZ));

            assert(minCellX >= 0 && minCellX < RENDER_GRID_COUNT);
            assert(maxCellX >= 0 && maxCellX < RENDER_GRID_COUNT);
            assert(minCellY >= 0 && minCellY < RENDER_GRID_COUNT);
            assert(maxCellY >= 0 && maxCellY < RENDER_GRID_COUNT);
            assert(minCellZ >= 0 && minCellZ < RENDER_GRID_COUNT);
            assert(maxCellZ >= 0 && maxCellZ < RENDER_GRID_COUNT);

            for (int z = minCellZ; z < maxCellZ + 1; z++)
            {
                for (int y = minCellY; y < maxCellY + 1; y++)
                {
                    for (int x = minCellX; x < maxCellX + 1; x++)
                    {
                        int index = z * RENDER_GRID_COUNT * RENDER_GRID_COUNT + y * RENDER_GRID_COUNT + x;
                        if (CollisionBoxTest(grid.cells[index].box, meshbox))
                        {
                            entity.bitflags[z * RENDER_GRID_COUNT + y] |= 1 << x;
                        }
                    }
                }
            }
        }
    }
}

void Islander::Renderer::RenderGridCullGrid(RenderGrid& grid, void* camera)
{
    Camera3D* cam = (Camera3D*)camera;

    Frustum frustum;
    if (cam->orthographic)
    {
        CreateFrustumOrthographic(&frustum, cam->width, cam->height, cam->nearZ, cam->farZ);
    }
    else
    {
        CreateFrustum(&frustum, cam->fovY, cam->aspect, cam->nearZ, cam->farZ);
    }

    Matrix4x4 view(Matrix4x4Type::MATRIX_4X4_ZERO);
    Matrix4x4::CreateViewMatrix(cam->pos.GetX(), cam->pos.GetY(), cam->pos.GetZ(), cam->look.GetX(), cam->look.GetY(), cam->look.GetZ(), &view);

    std::fill_n(grid.bitflags, RENDER_GRID_BIT_FLAG_BYTES, 0);

    for (int i = 0; i < grid.cells.size(); i++)
    {
        auto& cell = grid.cells[i];
        float centre[3] = { cell.centre[0], cell.centre[1], cell.centre[2] };

        Matrix4x4::TransformVector(view, &centre[0], &centre[1], &centre[2]);

        if (FrustumTest(&frustum, centre, cell.radius))
        {
            grid.bitflags[cell.z * RENDER_GRID_COUNT + cell.y] |= 1 << cell.x;
        }
    }
}

bool Islander::Renderer::RenderGridCullEntity(RenderGrid& grid, const RenderGridEntity& entity)
{
    for (int i = 0; i < RENDER_GRID_BIT_FLAG_BYTES; i++)
    {
        if (entity.bitflags[i] & grid.bitflags[i])
        {
            return true;
        }
    }

    return false;
}
