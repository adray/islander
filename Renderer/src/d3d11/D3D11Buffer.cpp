#include "d3d11\D3D11Buffer.h"
#include <Logger.h>
#include <d3d11.h>
#include <d3d10.h>

using namespace Islander::Renderer;

void CreateBuffer(ID3D11Buffer** buffer, ID3D11Device* device, UINT bufferType, UINT miscFlags, Usage usage, int sizeInBytes, int byteStride, void* data)
{
	D3D11_BUFFER_DESC bufferdesc;

	bufferdesc.BindFlags = bufferType;
	bufferdesc.ByteWidth = sizeInBytes;
	bufferdesc.MiscFlags = miscFlags;
	bufferdesc.StructureByteStride = byteStride;

	if (usage == USAGE_DYNAMIC)
	{
		bufferdesc.Usage = D3D11_USAGE_DYNAMIC;
		bufferdesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	}
	else if (usage == USAGE_STATIC)
	{
		bufferdesc.Usage = D3D11_USAGE_IMMUTABLE;
		bufferdesc.CPUAccessFlags = 0;
	}
	else if (usage == USAGE_DEFAULT)
	{
		bufferdesc.Usage = D3D11_USAGE_DEFAULT;
		bufferdesc.CPUAccessFlags = 0;
	}

	D3D11_SUBRESOURCE_DATA resource;
	resource.SysMemPitch = 0;
	resource.SysMemSlicePitch = 0;
	resource.pSysMem = data;

	device->CreateBuffer(&bufferdesc, &resource, buffer);
}

D3d11StructuredBuffer::D3d11StructuredBuffer(ID3D11Device* device, int stride)
	:
	_id(-1),
	_device(device),
	_allocated(false),
	_buffer(nullptr),
	_context(nullptr),
	_usage(USAGE_STATIC),
	_bytes(0),
	_shaderResourceView(nullptr),
	_stride(stride)
{
	_device->GetImmediateContext(&_context);
}

void D3d11StructuredBuffer::Allocate(int sizeInBytes, void* data, Usage usage)
{
	if (!_allocated)
	{
		_bytes = sizeInBytes;
		_usage = usage;

		CreateBuffer(&_buffer, _device, D3D11_BIND_UNORDERED_ACCESS | D3D11_BIND_SHADER_RESOURCE, D3D11_RESOURCE_MISC_BUFFER_STRUCTURED, _usage, sizeInBytes, _stride, data);

		D3D11_SHADER_RESOURCE_VIEW_DESC srv = {};
		srv.Format = DXGI_FORMAT_UNKNOWN;
		srv.ViewDimension = D3D11_SRV_DIMENSION_BUFFER;
		srv.Buffer.ElementOffset = 0;
		srv.Buffer.ElementWidth = sizeInBytes / _stride;

		_device->CreateShaderResourceView(_buffer, &srv, &_shaderResourceView);

		_allocated = true;
	}
}

void D3d11StructuredBuffer::Fill(int offset, void* data)
{
	if (_allocated && _usage == USAGE_DYNAMIC)
	{
		D3D11_MAPPED_SUBRESOURCE resource;
		_context->Map(_buffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &resource);
		memcpy(resource.pData, data, _bytes);
		_context->Unmap(_buffer, 0);
	}
}

void D3d11StructuredBuffer::Release()
{
	if (_buffer)
	{
		_buffer->Release();
		_buffer = nullptr;

		_shaderResourceView->Release();
		_shaderResourceView = nullptr;
	}
}

D3d11StructuredBuffer::~D3d11StructuredBuffer()
{
	Release();
}

void D3d11StructuredBuffer::Bind(int index, BindType type)
{
	if ((static_cast<int>(type) & static_cast<int>(BindType::Pixel)) == static_cast<int>(BindType::Pixel))
	{
		_context->PSSetShaderResources(index, 1, &_shaderResourceView);
	}

	if ((static_cast<int>(type) & static_cast<int>(BindType::Vertex)) == static_cast<int>(BindType::Vertex))
	{
		_context->VSSetShaderResources(index, 1, &_shaderResourceView);
	}

	if ((static_cast<int>(type) & static_cast<int>(BindType::Geometry)) == static_cast<int>(BindType::Geometry))
	{
		_context->GSSetShaderResources(index, 1, &_shaderResourceView);
	}

	if ((static_cast<int>(type) & static_cast<int>(BindType::Compute)) == static_cast<int>(BindType::Compute))
	{
		_context->CSSetShaderResources(index, 1, &_shaderResourceView);
	}
}

D3d11ConstantBuffer::D3d11ConstantBuffer(ID3D11Device* device)
	:
	_id(-1),
	_device(device),
	_allocated(false),
	_buffer(nullptr),
	_context(nullptr),
	_usage(USAGE_STATIC),
	_bytes(0)
{
	_device->GetImmediateContext(&_context);
}

void D3d11ConstantBuffer::Allocate(int sizeInBytes, void* data, Usage usage)
{
	if (!_allocated)
	{
		_bytes = sizeInBytes;
		_usage = usage;

		CreateBuffer(&_buffer, _device, D3D11_BIND_CONSTANT_BUFFER, 0, _usage, sizeInBytes, 0, data);

		_allocated = true;
	}
}

void D3d11ConstantBuffer::Fill(int offset, void* data)
{
	if (_allocated && _usage == USAGE_DYNAMIC)
	{
		D3D11_MAPPED_SUBRESOURCE resource;
		_context->Map(_buffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &resource);
		memcpy(resource.pData, data, _bytes);
		_context->Unmap(_buffer, 0);
	}
}

void D3d11ConstantBuffer::Release()
{
	if (_buffer)
	{
		_buffer->Release();
		_buffer = nullptr;
	}
}

D3d11ConstantBuffer::~D3d11ConstantBuffer()
{
	Release();
}

void D3d11ConstantBuffer::Bind(int index, BindType type)
{
	if ((static_cast<int>(type) & static_cast<int>(BindType::Pixel)) == static_cast<int>(BindType::Pixel))
	{
		_context->PSSetConstantBuffers(index, 1, &_buffer);
	}

	if ((static_cast<int>(type) & static_cast<int>(BindType::Vertex)) == static_cast<int>(BindType::Vertex))
	{
		_context->VSSetConstantBuffers(index, 1, &_buffer);
	}

	if ((static_cast<int>(type) & static_cast<int>(BindType::Geometry)) == static_cast<int>(BindType::Geometry))
	{
		_context->GSSetConstantBuffers(index, 1, &_buffer);
	}

	if ((static_cast<int>(type) & static_cast<int>(BindType::Compute)) == static_cast<int>(BindType::Compute))
	{
		_context->CSSetConstantBuffers(index, 1, &_buffer);
	}
}

D3d11IndexBuffer::D3d11IndexBuffer(ID3D11Device* device, GraphicsBuffering buffering)
    :
    _id(-1),
    _index(0),
    _device(device),
    _allocated(false),
    _context(nullptr),
    _usage(USAGE_STATIC),
    _buffering(buffering)
{
    _device->GetImmediateContext(&_context);

    _buffer[0] = nullptr;
    _buffer[1] = nullptr;
}

void D3d11IndexBuffer::Allocate(int sizeInBytes, void* data, Usage usage)
{
    if (!_allocated)
    {
        _bytes = sizeInBytes;
        _usage = usage;

        CreateBuffer(&_buffer[0], _device, D3D11_BIND_INDEX_BUFFER, 0, _usage, sizeInBytes, 0, data);
        if (_buffering == BUFFERING_DOUBLE)
        {
            CreateBuffer(&_buffer[1], _device, D3D11_BIND_INDEX_BUFFER, 0, _usage, sizeInBytes, 0, data);
        }

        _allocated = true;
    }
}

void D3d11IndexBuffer::Fill(int offset, void* data)
{
    if (_allocated && _usage == USAGE_DYNAMIC)
    {
        D3D11_MAPPED_SUBRESOURCE resource;
        _context->Map(_buffer[_index], 0, D3D11_MAP_WRITE_DISCARD, 0, &resource);
        memcpy(resource.pData, data, _bytes);
        _context->Unmap(_buffer[_index], 0);
    }
}

void D3d11IndexBuffer::Release()
{
    if (_buffer[0])
    {
        _buffer[0]->Release();
        _buffer[0] = nullptr;
    }

    if (_buffer[1])
    {
        _buffer[1]->Release();
        _buffer[1] = nullptr;
    }
}

D3d11IndexBuffer::~D3d11IndexBuffer()
{
    Release();
}

void D3d11IndexBuffer::Bind(int index)
{
    UINT offset = 0;

    _context->IASetIndexBuffer(_buffer[_index], DXGI_FORMAT_R32_UINT, offset);
}

D3d11VertexBuffer::D3d11VertexBuffer(ID3D11Device* device, int stride, GraphicsBuffering buffering)
	:
	_id(-1),
	_index(0),
	_device(device),
	_allocated(false),
	_context(nullptr),
	_usage(USAGE_STATIC),
	_stride(stride),
	_buffering(buffering)
{
	_device->GetImmediateContext(&_context);

	_buffer[0] = nullptr;
	_buffer[1] = nullptr;
}

void D3d11VertexBuffer::Swap()
{
	if (_buffering == BUFFERING_DOUBLE)
	{
		_index = (_index + 1) % 2;
	}
}

void D3d11VertexBuffer::Allocate(int sizeInBytes, void* data, Usage usage)
{
	if (!_allocated)
	{
		_bytes = sizeInBytes;
		_usage = usage;

		CreateBuffer(&_buffer[0], _device, D3D11_BIND_VERTEX_BUFFER, 0, _usage, sizeInBytes, 0, data);
		if (_buffering == BUFFERING_DOUBLE)
		{
			CreateBuffer(&_buffer[1], _device, D3D11_BIND_VERTEX_BUFFER, 0, _usage, sizeInBytes, 0, data);
		}

		_allocated = true;
	}
}

void D3d11VertexBuffer::Fill(int offset, void* data)
{
	if (_allocated && _usage == USAGE_DYNAMIC)
	{
		D3D11_MAPPED_SUBRESOURCE resource;
		_context->Map(_buffer[_index], 0, D3D11_MAP_WRITE_DISCARD, 0, &resource);
		memcpy(resource.pData, data, _bytes);
		_context->Unmap(_buffer[_index], 0);
	}
}

void D3d11VertexBuffer::Bind(int index)
{
	UINT offset = 0;
	UINT stride = _stride;

	_context->IASetVertexBuffers(index, 1, &_buffer[_index], &stride, &offset);
}

void D3d11VertexBuffer::Release()
{
	if (_buffer[0])
	{
		_buffer[0]->Release();
		_buffer[0] = nullptr;
	}

	if (_buffer[1])
	{
		_buffer[1]->Release();
		_buffer[1] = nullptr;
	}
}

D3d11VertexBuffer::~D3d11VertexBuffer()
{
	Release();
}
