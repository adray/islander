#include "d3d11\D3D11ParticleRenderer.h"
#include "ParticleSystem\ParticleRenderer.h"

#include "IDevice.h"
#include "IResourceLoader.h"
#include "FileSystem.h"
#include "Component.h"
#include "d3d11\D3D11Texture.h"

#include <d3d11.h>
#include <d3d10.h>
#include <d3dcommon.h>
#include <d3d11.h>
#include "EffekseerRendererDX11.h"
#include "Effekseer.h"

#include <locale>
#include <codecvt>

namespace Islander
{
    namespace Renderer
    {
        class ParticleTextureLoader : public ::Effekseer::TextureLoader
        {
            Islander::Renderer::ParticleRenderer* _renderer;
            HandleCollection<D3d11Texture>* _textures;
        public:
            ParticleTextureLoader(
                Islander::Renderer::ParticleRenderer* renderer,
                HandleCollection<D3d11Texture>* textures);

            Effekseer::TextureRef Load(const EFK_CHAR* path, ::Effekseer::TextureType textureType) override;

            void Unload(Effekseer::TextureRef data) override;
        };

        class ParticleModelLoader : public ::Effekseer::ModelLoader
        {
            Islander::Renderer::ParticleRenderer* _renderer;
        public:
            ParticleModelLoader(Islander::Renderer::ParticleRenderer* renderer);

            Effekseer::ModelRef Load(const EFK_CHAR* path) override;

            void Unload(Effekseer::ModelRef data) override;
        };

        class ParticleMaterialLoader : public ::Effekseer::MaterialLoader
        {

        };
    }
}

Islander::Renderer::ParticleTextureLoader::ParticleTextureLoader(
    Islander::Renderer::ParticleRenderer* renderer,
    HandleCollection<D3d11Texture>* textures)
    :
    _renderer(renderer),
    _textures(textures)
{
}

Effekseer::TextureRef Islander::Renderer::ParticleTextureLoader::Load(const EFK_CHAR* path, ::Effekseer::TextureType textureType)
{
    // Workaround since Islander handles paths in UTF8 and on windows converts them to whatever windows uses, whilst Effekseer gives us UTF16.

    std::wstring_convert<std::codecvt_utf8_utf16<wchar_t> > converter;
    std::string path_utf8 = converter.to_bytes((wchar_t*)path);
    std::string filename;

    Islander::FileSystem::GetFileName(path_utf8, filename);

    auto loader = _renderer->device->GetResourceLoader2();
    auto loadedTexture = loader->FindMaterialTexture(filename);
    if (loadedTexture != nullptr)
    {
        auto textureRef = ::Effekseer::MakeRefPtr<::Effekseer::Texture>();

        auto srv = _textures->Fetch(loadedTexture->texture.index).srv;
        auto texture = ::EffekseerRendererDX11::CreateTexture(
            ((::EffekseerRendererDX11::Renderer*)_renderer->renderer)->GetGraphicsDevice(),
            srv,
            nullptr,
            nullptr);

        textureRef->SetBackend(texture);

        return textureRef;
    }

    return nullptr;
}

void Islander::Renderer::ParticleTextureLoader::Unload(Effekseer::TextureRef data)
{

}

Islander::Renderer::ParticleModelLoader::ParticleModelLoader(Islander::Renderer::ParticleRenderer* renderer) : _renderer(renderer)
{
}

Effekseer::ModelRef Islander::Renderer::ParticleModelLoader::Load(const EFK_CHAR* path)
{
    std::wstring_convert<std::codecvt_utf8_utf16<wchar_t> > converter;
    std::string path_utf8 = converter.to_bytes((wchar_t*)path);
    std::string filename;

    Islander::FileSystem::GetFileName(path_utf8, filename);

    auto loader = _renderer->device->GetResourceLoader2();
    int size;
    char* data = loader->GetRawData(filename, &size);
    if (data != nullptr)
    {
        auto modelRef = ::Effekseer::MakeRefPtr<::Effekseer::Model>(data, (int32_t)size);

        return modelRef;
    }

    return nullptr;
}

void Islander::Renderer::ParticleModelLoader::Unload(Effekseer::ModelRef data)
{
    
}

Islander::Renderer::ParticleRenderer* Islander::Renderer::CreateD3d11ParticleRenderer(
    ID3D11DeviceContext* d3D11DeviceContext,
    ID3D11Device* d3D11Device,
    HandleCollection<D3d11Texture>* textures)
{
    auto renderer = ::EffekseerRendererDX11::Renderer::Create(d3D11Device, d3D11DeviceContext, 8000);

    Islander::Renderer::ParticleRenderer* p = new Islander::Renderer::ParticleRenderer();
    p->renderer = renderer.Get();
    p->spriteRenderer = renderer->CreateSpriteRenderer();
    p->ribbonRenderer = renderer->CreateRibbonRenderer();
    p->ringRenderer = renderer->CreateRingRenderer();
    p->trackRenderer = renderer->CreateTrackRenderer();
    p->modelRenderer = renderer->CreateModelRenderer();
    p->textureLoader = Effekseer::MakeRefPtr<ParticleTextureLoader>(p, textures);
    p->modelLoader = Effekseer::MakeRefPtr<ParticleModelLoader>(p);
    p->materialLoader = Effekseer::MakeRefPtr<ParticleMaterialLoader>();
    p->elapsedTime = 0;

    return p;
}

void Islander::Renderer::D3d11BeginDrawParticles(Islander::Renderer::ParticleRenderer* renderer, Camera3D* camera)
{
    auto r = (::EffekseerRendererDX11::Renderer*)renderer->renderer;

    // Specify a projection matrix
    r->SetProjectionMatrix(
        ::Effekseer::Matrix44().PerspectiveFovLH(camera->fovY, camera->aspect, camera->nearZ, camera->farZ));

    auto pos = ::Effekseer::Vector3D(camera->pos.GetX(), camera->pos.GetY(), camera->pos.GetZ());
    auto at = ::Effekseer::Vector3D(camera->look.GetX(), camera->look.GetY(), camera->look.GetZ());

    // Specify a camera matrix
    r->SetCameraMatrix(
        ::Effekseer::Matrix44().LookAtLH(pos, at, ::Effekseer::Vector3D(0.0f, 1.0f, 0.0f)));

    r->SetTime(renderer->elapsedTime);

    // Begin to rendering effects
    r->BeginRendering();
}

void Islander::Renderer::D3d11EndDrawParticles(Islander::Renderer::ParticleRenderer* renderer)
{
    auto r = (::EffekseerRendererDX11::Renderer*)renderer->renderer;

    // Finish to rendering effects
    r->EndRendering();
}
