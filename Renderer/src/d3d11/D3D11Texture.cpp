#include "d3d11\D3D11Texture.h"
#include <Logger.h>

#include <d3d11.h>
#include <memory>

using namespace Islander::Renderer;

void Islander::Renderer::CreateTextureUAV(D3d11Texture& texture, ID3D11Device* device)
{
    ID3D11Resource* resource = nullptr;
    texture.srv->GetResource(&resource);

    D3D11_SHADER_RESOURCE_VIEW_DESC srv_Desc;
    texture.srv->GetDesc(&srv_Desc);

    if (FAILED(device->CreateUnorderedAccessView(resource, nullptr, &texture.uav)))
    {
        Islander::Logger::GetLogger()->Log("Unable to create Texture UAV", LOGGING_RENDERING);
    }
}
