#include "d3d11\D3D11Shader.h"
#include "Renderer.h"
#include <D3D11.h>
#include <d3dcompiler.h>
#include <sstream>

using namespace Islander::Renderer;

D3d11VertexShader::D3d11VertexShader(ID3D11Device* device, ID3D10Blob* blob, SEMANTIC * semantics, int count)
	: _device(device)
{
	D3DReflect(blob->GetBufferPointer(), blob->GetBufferSize(), IID_ID3D11ShaderReflection, (void**) &_reflection);

	if (FAILED(device->CreateVertexShader(blob->GetBufferPointer(), blob->GetBufferSize(), NULL, &_shader)))
	{
		std::wstringstream error;
		error << "Failed to create vertex shader.";
		_errors.push_back(error.str());
		return;
	}

	_layoutDescription = new D3D11_INPUT_ELEMENT_DESC[count];
	std::unordered_map<std::string, int> semantic_map;

	std::wstringstream str;

	for (int i = 0; i < count; i++)
	{
		if (semantic_map.find(semantics[i].desc) != semantic_map.end())
		{
			semantic_map[semantics[i].desc]++;
		}
		else
		{
			semantic_map.insert(std::pair<char*, int>(semantics[i].desc, 0));
		}

		DXGI_FORMAT formats[] = { DXGI_FORMAT_R32_FLOAT, DXGI_FORMAT_R32G32_FLOAT, DXGI_FORMAT_R32G32B32_FLOAT, DXGI_FORMAT_R32G32B32A32_FLOAT,
			DXGI_FORMAT_R32_SINT, DXGI_FORMAT_R32G32_SINT, DXGI_FORMAT_R32G32B32_SINT, DXGI_FORMAT_R32G32B32A32_SINT };

		ZeroMemory(&_layoutDescription[i], sizeof(D3D11_INPUT_ELEMENT_DESC));
		_layoutDescription[i].Format = formats[semantics[i].format];
		_layoutDescription[i].SemanticName = semantics[i].desc;
		_layoutDescription[i].InputSlot = semantics[i].stream;
		_layoutDescription[i].SemanticIndex = semantic_map[semantics[i].desc];
		_layoutDescription[i].AlignedByteOffset = i > 0 ? D3D11_APPEND_ALIGNED_ELEMENT : 0;
		if (semantics[i].stream == 0)
			_layoutDescription[i].InputSlotClass = D3D11_INPUT_CLASSIFICATION::D3D11_INPUT_PER_VERTEX_DATA;
		else
		{
			_layoutDescription[i].InputSlotClass = D3D11_INPUT_CLASSIFICATION::D3D11_INPUT_PER_INSTANCE_DATA;
			_layoutDescription[i].InstanceDataStepRate = 1;
		}

		str << i << ": " << semantics[i].format << ", " << semantics[i].desc << ", " << semantics[i].stream << std::endl;
	}
	
	if (FAILED(device->CreateInputLayout(_layoutDescription, count, blob->GetBufferPointer(), blob->GetBufferSize(), &_layout)))
	{
		int error = GetLastError();
		str << "Invalid Layout: Error code: " << error << std::endl;
		_errors.push_back(str.str());
		return;
	}
}

void D3d11VertexShader::Link(const std::vector<Attribute>& attributes)
{
	_attributes = attributes;

	D3D11_SHADER_DESC desc;
	_reflection->GetDesc(&desc);
	UINT inputcount = desc.InputParameters;
	
	std::unordered_map<std::string, int> attributeMapping;
	std::unordered_map<std::string, int> attributeIndices;

	int stream = 0;
	int offset = 0;
	for (int i = 0; i < inputcount; i++)
	{
		D3D11_SIGNATURE_PARAMETER_DESC sig;
		_reflection->GetInputParameterDesc(i, &sig);
		std::stringstream ss; ss << sig.SemanticName << sig.SemanticIndex;

		if (_layoutDescription[i].InputSlot > stream)
		{
			stream = _layoutDescription[i].InputSlot;
			offset = 0;
		}

		attributeMapping.insert(std::make_pair(ss.str(), offset));
		attributeIndices.insert(std::make_pair(ss.str(), i));

		if (_layoutDescription[i].Format == DXGI_FORMAT_R32_FLOAT)
			offset += 1;
		else if (_layoutDescription[i].Format == DXGI_FORMAT_R32G32_FLOAT)
			offset += 2;
		else if (_layoutDescription[i].Format == DXGI_FORMAT_R32G32B32_FLOAT)
			offset += 3;
		else if (_layoutDescription[i].Format == DXGI_FORMAT_R32G32B32A32_FLOAT)
			offset += 4;
		else if (_layoutDescription[i].Format == DXGI_FORMAT_R32_SINT)
			offset += 1;
		else if (_layoutDescription[i].Format == DXGI_FORMAT_R32G32_SINT)
			offset += 2;
		else if (_layoutDescription[i].Format == DXGI_FORMAT_R32G32B32_SINT)
			offset += 3;
		else if (_layoutDescription[i].Format == DXGI_FORMAT_R32G32B32A32_SINT)
			offset += 4;
	}

	int constantIndex = 0;
	for (int i = 0; i < desc.ConstantBuffers; i++)
	{
		auto constant = _reflection->GetConstantBufferByIndex(i);
		D3D11_SHADER_BUFFER_DESC buffer;
		constant->GetDesc(&buffer);
		int offset = 0;
		for (int j = 0; j < buffer.Variables; j++)
		{
			auto variable = constant->GetVariableByIndex(j);
			D3D11_SHADER_VARIABLE_DESC desc;
			variable->GetDesc(&desc);
			attributeMapping.insert(std::make_pair(desc.Name, offset));
			attributeIndices.insert(std::make_pair(desc.Name, constantIndex++));
			offset += desc.Size / 4;
		}
	}

	for (auto& attribute : _attributes)
	{
		std::string param = attribute.GetParameter();
		if (attributeMapping.find(param) != attributeMapping.end())
		{
			attribute.SetOffset(attributeMapping[param]);
			attribute.SetIndex(attributeIndices[param]);
		}
	}
}

void D3d11VertexShader::GetErrors(std::vector<std::wstring>& errors)
{
	for (int i = 0; i < _errors.size(); i++)
	{
		errors.push_back(_errors.at(i));
	}
}

void D3d11VertexShader::Bind()
{
	ID3D11DeviceContext* context;
	_device->GetImmediateContext(&context);

	context->VSSetShader(_shader, NULL, NULL);
	context->IASetInputLayout(_layout);
}

void D3d11VertexShader::Release()
{
	if (_shader)
	{
		_shader->Release();
		_shader = nullptr;
	}
}

D3d11PixelShader::D3d11PixelShader(ID3D11Device* device, ID3D10Blob* blob)
	: _device(device)
{
	_device->CreatePixelShader(blob->GetBufferPointer(), blob->GetBufferSize(), nullptr, &_shader);
}

void D3d11PixelShader::Bind()
{
	ID3D11DeviceContext* context;
	_device->GetImmediateContext(&context);

	context->PSSetShader(_shader, nullptr, 0);
}

void D3d11PixelShader::Release()
{
	if (_shader)
	{
		_shader->Release();
		_shader = nullptr;
	}
}

D3d11GeometryShader::D3d11GeometryShader(ID3D11Device* device, ID3D10Blob* blob) : _device(device)
{
	_device->CreateGeometryShader(blob->GetBufferPointer(), blob->GetBufferSize(), nullptr, &_shader);
}

void D3d11GeometryShader::Release()
{
	if (_shader)
	{
		_shader->Release();
		_shader = nullptr;
	}
}

void D3d11GeometryShader::Bind()
{
	ID3D11DeviceContext* context;
	_device->GetImmediateContext(&context);

	context->GSSetShader(_shader, nullptr, 0);
}

D3d11ComputeShader::D3d11ComputeShader(ID3D11Device* device, ID3D10Blob* blob) : _device(device)
{
	_device->CreateComputeShader(blob->GetBufferPointer(), blob->GetBufferSize(), nullptr, &_shader);
}

void D3d11ComputeShader::Release()
{
	if (_shader)
	{
		_shader->Release();
		_shader = nullptr;
	}
}

void D3d11ComputeShader::Bind()
{
	ID3D11DeviceContext* context;
	_device->GetImmediateContext(&context);

	context->CSSetShader(_shader, nullptr, 0);
}
