#include "d3d11\D3D11Renderer.h"
#include "Material.h"
#include "FontEngine.h"
#include "TextureLoader.h"
#include "Logger.h"
#include "FileSystem.h"
#include "d3d11\D3D11ParticleRenderer.h"
#include "backends\imgui_impl_win32.h"
#include "backends\imgui_impl_dx11.h"
#include <d3d11.h>
#include <d3d10.h>
#include <d3d11_1.h>
#include <d3dcommon.h>
#include <d3dcompiler.h>
#include <Window.h>
#include <Component.h>
#include <fstream>
#include <sstream>
#include <unordered_map>
#include <locale>
#include <codecvt>

extern "C"
{
#include <entity.h>
}

using namespace Islander;
using namespace Islander::Renderer;

D3d11Renderer::D3d11Renderer()
	: current_rendertarget(-1), _syncInterval(1)
{
}

void D3d11Renderer::Init(WINDOW *hWindow, FontDescription& font)
{
	this->window = hWindow;

	IDXGIFactory * factory;

	CreateDXGIFactory(__uuidof(IDXGIFactory), (void**)&factory);

	IDXGIAdapter * adapter;

	factory->EnumAdapters(0, &adapter);

	IDXGIOutput * outputs;

	adapter->EnumOutputs(0, &outputs);

	UINT modes; DXGI_MODE_DESC * desc;

	outputs->GetDisplayModeList(DXGI_FORMAT_R8G8B8A8_UNORM, DXGI_ENUM_MODES_INTERLACED, &modes, NULL);

	desc = new DXGI_MODE_DESC[modes];

	outputs->GetDisplayModeList(DXGI_FORMAT_R8G8B8A8_UNORM, DXGI_ENUM_MODES_INTERLACED, &modes, desc);

	UINT denom, numon;

    display_resolutions.count = 0;
	for (UINT i = 0; i < modes; i++)
	{
        if (display_resolutions.count < RESOLUTION_LIST_MAX_COUNT)
        {
            bool add = true;
            if (display_resolutions.count > 0)
            {
                auto prev = &display_resolutions.resolution[display_resolutions.count - 1];
                add = !(prev->width == desc[i].Width && prev->height == desc[i].Height);
            }

            if (add)
            {
                auto resolution = &display_resolutions.resolution[display_resolutions.count];
                resolution->id = display_resolutions.count;
                resolution->width = desc[i].Width;
                resolution->height = desc[i].Height;
                display_resolutions.count++;
            }
        }

		if (desc[i].Height == hWindow->Height() && desc[i].Width == hWindow->Width())
		{
			denom = desc[i].RefreshRate.Denominator;
			numon = desc[i].RefreshRate.Numerator;
		}
	}

#if DEBUG
	_syncInterval = 0; // uncap the frame rate
#endif
	delete[] desc;
	desc = nullptr;

	DXGI_ADAPTER_DESC adapter_desc;

	adapter->GetDesc(&adapter_desc);

	SIZE_T vmem_meg = adapter_desc.DedicatedVideoMemory / 1024 / 1024;

    auto&stream = Islander::Logger::GetStream(LOGGING_RENDERING) << "Video memory: " << vmem_meg << "MB";
    stream.endl();

	size_t description_len;

	wcstombs_s(&description_len, hardware_desc, 128, adapter_desc.Description, 128);

    stream << hardware_desc;
    stream.endl();

	outputs->Release();
	adapter->Release();
	factory->Release();

	DXGI_SWAP_CHAIN_DESC swapchain_desc;

	ZeroMemory(&swapchain_desc, sizeof(DXGI_SWAP_CHAIN_DESC));
	swapchain_desc.BufferCount = 2;
	swapchain_desc.BufferDesc.Width = hWindow->Width();
	swapchain_desc.BufferDesc.Height = hWindow->Height();
	swapchain_desc.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;

	swapchain_desc.BufferDesc.RefreshRate.Denominator = denom;
	swapchain_desc.BufferDesc.RefreshRate.Numerator = numon;

	swapchain_desc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;

	swapchain_desc.OutputWindow = static_cast<HWND>(hWindow->Handle());

	assert(IsWindow(swapchain_desc.OutputWindow));

	swapchain_desc.SampleDesc.Count = 1;
	swapchain_desc.SampleDesc.Quality = 0;

	swapchain_desc.Windowed = TRUE;

	swapchain_desc.BufferDesc.ScanlineOrdering = DXGI_MODE_SCANLINE_ORDER_UNSPECIFIED;
	swapchain_desc.BufferDesc.Scaling = DXGI_MODE_SCALING_UNSPECIFIED;

	swapchain_desc.SwapEffect = DXGI_SWAP_EFFECT_DISCARD;

	swapchain_desc.Flags = 0;

	D3D_FEATURE_LEVEL featurelevel = D3D_FEATURE_LEVEL_11_1;

	UINT flags = 0;
#ifdef _DEBUG
	flags |= D3D11_CREATE_DEVICE_DEBUG;
#endif

    if (FAILED(D3D11CreateDeviceAndSwapChain(NULL, D3D_DRIVER_TYPE_HARDWARE, 0, flags,
        &featurelevel, 1, D3D11_SDK_VERSION, &swapchain_desc, &swapchain, &device, NULL, &context)))
    {
        // try fallback
        featurelevel = D3D_FEATURE_LEVEL_11_0;
        if (FAILED(D3D11CreateDeviceAndSwapChain(NULL, D3D_DRIVER_TYPE_HARDWARE, 0, flags,
            &featurelevel, 1, D3D11_SDK_VERSION, &swapchain_desc, &swapchain, &device, NULL, &context)))
            return;
    }

	swapchain->GetBuffer(0, __uuidof(ID3D11Texture2D), (void**)&backbuffer);

	CreateRenderTarget(hWindow->Width(), hWindow->Height(), backbuffer, &rendertargetview);

	backbuffer->Release();

	CreateRenderTarget(RenderTargetType::Diffuse, hWindow->Width(), hWindow->Height(), &tex_lightmap, &view_lightmap, &texture_lightmap);

	CreateDepthStencilTarget(hWindow->Width(), hWindow->Height(), &depthstenciltexture, &depthstate, &depthstate_no_write, &depthview);

	context->OMSetDepthStencilState(depthstate, 1);

	context->OMSetRenderTargets(1, &rendertargetview, depthview);

    D3d11RasterState rasterState2D;
    rasterState2D._state._backface_culling = false;
    rasterState2D._state._multi_sampling = false;
    rasterState2D._state._wireframe = false;

	CreateRasterState(rasterState2D);

    currentRasterStateParams = rasterState2D._state;
    currentrasterstate = rasterState2D._rasterstate;
    context->RSSetState(currentrasterstate);

	D3D11_VIEWPORT viewport;

	viewport.Height = hWindow->Height();
	viewport.Width = hWindow->Width();
	viewport.TopLeftX = 0;
	viewport.TopLeftY = 0;
	viewport.MinDepth = 0;
	viewport.MaxDepth = 1;

	context->RSSetViewports(1, &viewport);

	// rgb = s.rgb + (1-s.a)*d.rgb
	// a = s.a + (1 - s.a) * d.a
	D3D11_RENDER_TARGET_BLEND_DESC renderblend;
	renderblend.BlendEnable = true;
	renderblend.BlendOp = D3D11_BLEND_OP_ADD;
	renderblend.BlendOpAlpha = D3D11_BLEND_OP_ADD;
	renderblend.DestBlend = D3D11_BLEND_INV_SRC_ALPHA;
	renderblend.DestBlendAlpha = D3D11_BLEND_INV_SRC_ALPHA;
	renderblend.SrcBlend = D3D11_BLEND_ONE;
	renderblend.SrcBlendAlpha = D3D11_BLEND_ONE;
	renderblend.RenderTargetWriteMask = 0xf;

	// rgb = s.rgb + d.rgb
    // a = s.a + (1 - s.a) * d.a
	D3D11_RENDER_TARGET_BLEND_DESC renderblend2;
	renderblend2.BlendEnable = true;
	renderblend2.BlendOp = D3D11_BLEND_OP_ADD;
	renderblend2.BlendOpAlpha = D3D11_BLEND_OP_ADD;
	renderblend2.DestBlend = D3D11_BLEND_ONE;
	renderblend2.DestBlendAlpha = D3D11_BLEND_INV_SRC_ALPHA;
	renderblend2.SrcBlend = D3D11_BLEND_ONE;
	renderblend2.SrcBlendAlpha = D3D11_BLEND_ONE;
	renderblend2.RenderTargetWriteMask = 0xf;

	D3D11_BLEND_DESC blend_desc;
	ZeroMemory(&blend_desc, sizeof(blend_desc));
	blend_desc.RenderTarget[0] = renderblend;

	D3D11_BLEND_DESC blend_desc2;
	ZeroMemory(&blend_desc2, sizeof(blend_desc2));
	blend_desc2.RenderTarget[0] = renderblend2;

	device->CreateBlendState(&blend_desc, &blendstate);
	device->CreateBlendState(&blend_desc2, &blendstate2);

	FLOAT factor[4] = { 1, 1, 1, 1 };

	context->OMSetBlendState(blendstate, factor, 0xffffffff);

	colour[0] = 1;
	colour[1] = 1;
	colour[2] = 1;
	colour[3] = 1;

	CreateSampler(false); // Clamp
	CreateSampler(true); // Wrap

    particleRenderer = Islander::Renderer::CreateD3d11ParticleRenderer(context, device, &textures);
}

void D3d11Renderer::Release()
{
	device->Release();
	context->Release();
	swapchain->Release();
	depthstenciltexture->Release();
	rendertargetview->Release();
	depthview->Release();
	depthstate->Release();

    /*for (auto& state : rasterStateList)
    {
        state._rasterstate->Release();
    }*/

	// TODO: release correctly

	/*for (int i = 0; i < textures.size(); i++)
		textures.at(i)->Release();
	for (int i = 0; i < constantbuffers.size(); i++)
		if (constantbuffers.at(i) != NULL)
			constantbuffers.at(i)->Release();
	for (int i = 0; i < vertexbuffers.size(); i++)
		if (vertexbuffers.at(i) != NULL)
			vertexbuffers.at(i)->Release();
	for (int i = 0; i < this->vertexshaders.size(); i++)
	{
		vertexshaders.at(i)->Release();
		vertexshaders.at(i)->Release();
	}*/
}

void D3d11Renderer::BeginLightPass()
{
	FLOAT factor[4] = { 1, 1, 1, 1 };
	context->ClearRenderTargetView(view_lightmap, colour);
	context->ClearDepthStencilView(depthview, D3D10_CLEAR_DEPTH | D3D10_CLEAR_STENCIL, 1, 0);
	context->OMSetRenderTargets(1, &view_lightmap, NULL);
	context->OMSetBlendState(blendstate2, factor, 0xffffffff);
}

IBuffer* D3d11Renderer::CreateVertexBuffer(Usage flags, int sizeInBytes, void* data, int stride, GraphicsBuffering buffering)
{
	int key = (flags & 0xFF) | (sizeInBytes << 4) | (stride << 20);

	D3d11VertexBuffer* buffer;
	if (vertexbuffers.TryAllocate(key, buffer))
	{
		buffer->Fill(0, data);
	}
	else
	{
		buffer = new D3d11VertexBuffer(device, stride, buffering);
		buffer->Allocate(sizeInBytes, data, flags);
		int handle = vertexbuffers.AllocateNew(key, buffer);
		buffer->SetID(handle);
	}

	return buffer;
}

IBuffer* D3d11Renderer::CreateConstantBuffer(Usage flags, int sizeInBytes, void* data)
{
	int key = (flags & 0xFF) | (sizeInBytes << 4);

	D3d11ConstantBuffer* buffer;
	if (constantbuffers.TryAllocate(key, buffer))
	{
		buffer->Fill(0, data);
	}
	else
	{
		buffer = new D3d11ConstantBuffer(device);
		buffer->Allocate(sizeInBytes, data, flags);
		int handle = constantbuffers.AllocateNew(key, buffer);
		buffer->SetID(handle);
	}

	return buffer;
}

IBuffer* D3d11Renderer::CreateIndexBuffer(Usage flags, int sizeInBytes, void* data)
{
    int key = (flags & 0xFF) | (sizeInBytes << 4);

    D3d11IndexBuffer* buffer;
    if (indexbuffers.TryAllocate(key, buffer))
    {
        buffer->Fill(0, data);
    }
    else
    {
        buffer = new D3d11IndexBuffer(device, BUFFERING_SINGLE);
        buffer->Allocate(sizeInBytes, data, flags);
        int handle = indexbuffers.AllocateNew(key, buffer);
        buffer->SetID(handle);
    }

    return buffer;
}

IBuffer* D3d11Renderer::CreateStructuredBuffer(Usage flags, int sizeInBytes, int stride, void* data)
{
	int key = (flags & 0xFF) | (sizeInBytes << 4);

	D3d11StructuredBuffer* buffer;
	if (structuredbuffers.TryAllocate(key, buffer))
	{
		buffer->Fill(0, data);
	}
	else
	{
		buffer = new D3d11StructuredBuffer(device, stride);
		buffer->Allocate(sizeInBytes, data, flags);
		int handle = structuredbuffers.AllocateNew(key, buffer);
		buffer->SetID(handle);
	}

	return buffer;
}

void D3d11Renderer::FreeVertexBuffer(int handle)
{
	auto vertex = vertexbuffers.Fetch(handle);
	if (vertex)
	{
		int key = (vertex->GetUsage() & 0xFF) | (vertex->Size() << 4) | (vertex->GetStride() << 20);
		vertexbuffers.Remove(key, vertex->ID());
		vertex->Release();
	}
	delete vertex;
}

void D3d11Renderer::FreeConstantBuffer(int handle)
{
	auto constant = constantbuffers.Fetch(handle);
	if (constant)
	{
		int key = (constant->GetUsage() & 0xFF) | (constant->Size() << 4);
		constantbuffers.Remove(key, constant->ID());
		constant->Release();
	}
	delete constant;
}

void D3d11Renderer::FreeIndexBuffer(int handle)
{
    auto index = indexbuffers.Fetch(handle);
    if (index)
    {
        int key = (index->GetUsage() & 0xFF) | (index->Size() << 4);
        indexbuffers.Remove(key, index->ID());
        index->Release();
    }
    delete index;
}

void D3d11Renderer::FreeStructuredBuffer(int handle)
{
	auto structured = structuredbuffers.Fetch(handle);
	if (structured)
	{
		int key = (structured->GetUsage() & 0xFF) | (structured->Size() << 4);
		structuredbuffers.Remove(key, structured->ID());
		structured->Release();
	}
	delete structured;
}

void D3d11Renderer::RecycleBuffer(IBuffer* buffer)
{
	auto vertex = dynamic_cast<D3d11VertexBuffer*>(buffer);
	if (vertex)
	{
		int key = (vertex->GetUsage() & 0xFF) | (vertex->Size() << 4) | (vertex->GetStride() << 20);
		vertexbuffers.Release(key, vertex->ID());
	}

	auto constant = dynamic_cast<D3d11ConstantBuffer*>(buffer);
	if (constant)
	{
		int key = (constant->GetUsage() & 0xFF) | (constant->Size() << 4);
		constantbuffers.Release(key, constant->ID());
	}

	/*auto structured = dynamic_cast<D3d11StructuredBuffer*>(buffer);
	if (structured)
	{
		int key = (structured->GetUsage() & 0xFF) | (structured->Size() << 4);
		structuredbuffers.Release(key, structured->ID());
	}*/
}

void D3d11Renderer::BeginPass(const PASS_CONFIG& config, int pass, int render_flags)
{
	DX<ID3DUserDefinedAnnotation> pPerf;
	if (SUCCEEDED(context->QueryInterface(__uuidof(ID3DUserDefinedAnnotation), reinterpret_cast<void**>(&pPerf))))
	{
		std::wstring_convert<std::codecvt_utf8_utf16<wchar_t> > converter;
		pPerf.Value()->SetMarker(converter.from_bytes(config._passName).c_str());
	}

	int width = window->Width();
	int height = window->Height();
	ID3D11RenderTargetView* currentrendertarget[PASS_CONFIG::RENDER_TARGET_MAX_COUNT] = { rendertargetview };
	int renderTargetCount = 1;
	auto currentdepth = depthview;
	auto currentdepthstate = depthstate;

	auto flags = render_flags;

	// LEGACY
	if (pass == 0 && current_rendertarget >= 0)
	{
		auto item = rendertargets.Fetch(current_rendertarget);
		currentrendertarget[0] = item.render_target;
		width = item.width;
		height = item.height;
		currentdepth = item.depthview;
	}
	else
	{
		if (config._renderTarget[0] >= 0)
		{
			auto item = rendertargets.Fetch(config._renderTarget[0]);
			currentrendertarget[0] = item.render_target;
			width = item.width;
			height = item.height;
			currentdepth = item.depthview;
		}

		for (int i = 1; i < config._renderTargetCount; i++)
		{
			if (config._renderTarget[i] >= 0)
			{
				auto item = rendertargets.Fetch(config._renderTarget[i]);
				currentrendertarget[i] = item.render_target;
				assert(width == item.width);
				assert(height == item.height);
				renderTargetCount++;
			}
		}

		flags = config._flags;
	}

	ID3D11ShaderResourceView * tab[1];
	tab[0] = NULL;
	context->PSSetShaderResources(0, 1, tab);
	context->VSSetShaderResources(0, 1, tab);

	ID3D11RenderTargetView * targets[1];
	targets[0] = NULL;
	context->OMSetRenderTargets(1, targets, NULL);

	D3D11_VIEWPORT viewport;

	viewport.Height = height;
	viewport.Width = width;
	viewport.TopLeftX = 0;
	viewport.TopLeftY = 0;
	viewport.MinDepth = 0;
	viewport.MaxDepth = 1;

	context->RSSetViewports(1, &viewport);

    if ((flags & ISLANDER_RENDER_FLAGS_RETAIN_DEPTH) != ISLANDER_RENDER_FLAGS_RETAIN_DEPTH)
    {
        context->ClearDepthStencilView(currentdepth, D3D11_CLEAR_DEPTH | D3D11_CLEAR_STENCIL, 1, 0);
    }

	if ((flags & ISLANDER_RENDER_FLAGS_NO_DEPTH_WRITE) == ISLANDER_RENDER_FLAGS_NO_DEPTH_WRITE)
	{
		currentdepthstate = depthstate_no_write;
	}

	for (int i = 0; i < renderTargetCount; i++)
	{
		if ((flags & ISLANDER_RENDER_FLAGS_CLEAR) == ISLANDER_RENDER_FLAGS_CLEAR)
		{
			context->ClearRenderTargetView(currentrendertarget[i], colour);
		}
		else if ((flags & ISLANDER_RENDER_FLAGS_CLEAR_TRANSPARENT) == ISLANDER_RENDER_FLAGS_CLEAR_TRANSPARENT)
		{
			float colour[4] = { 0, 0, 0, 0 };
			context->ClearRenderTargetView(currentrendertarget[i], colour);
		}
	}

	context->OMSetDepthStencilState(currentdepthstate, 1);
	context->OMSetRenderTargets(renderTargetCount, currentrendertarget, currentdepth);

	if ((flags & ISLANDER_RENDER_FLAGS_BLEND_ADD) == ISLANDER_RENDER_FLAGS_BLEND_ADD)
	{
		FLOAT factor[4] = { 1, 1, 1, 1 };
		context->OMSetBlendState(blendstate2, factor, 0xffffffff);
	}
	else
	{
		FLOAT factor[4] = { 1, 1, 1, 1 };
		context->OMSetBlendState(blendstate, factor, 0xffffffff);
	}
}

void D3d11Renderer::SelectRasterState(const RasterState& rasterState)
{
    if (!rasterState.Equals(currentRasterStateParams))
    {
        ID3D11RasterizerState* selectedRasterState = nullptr;

        for (auto& state : rasterStateList)
        {
            if (rasterState.Equals(state._state))
            {
                selectedRasterState = state._rasterstate;
                break;
            }
        }

        if (selectedRasterState != nullptr)
        {
            currentrasterstate = selectedRasterState;
            currentRasterStateParams = rasterState;
            context->RSSetState(selectedRasterState);
        }
        else
        {
            D3d11RasterState state;
            state._state = rasterState;

            CreateRasterState(state);

            currentrasterstate = state._rasterstate;
            currentRasterStateParams = rasterState;

            context->RSSetState(selectedRasterState);
        }
    }
}

void D3d11Renderer::SubmitDraw(const RenderOperation& op)
{
    SelectRasterState(op.GetRasterState());

	// Bind shaders
	BindType bindType = BindShader(op.GetPixelShader(), op.GetVertexShader(), op.GetGeometryShader());

	// Bind textures
	for (int i = 0; i < 4; i++)
	{
		if (op.GetTextures(i) != -1)
		{
			BindTexture(i, op.GetTextures(i));
		}
        else
        {
            ID3D11ShaderResourceView* resourceView = nullptr;
            context->PSSetShaderResources(i, 1, &resourceView);
            context->VSSetShaderResources(i, 1, &resourceView);
        }
	}

	// Bind sampler
	if (((int)bindType & (int)BindType::Pixel) == (int)BindType::Pixel)
	{
		auto& pixel = pixelshaders.Fetch(op.GetPixelShader());
		int numSamplers = pixel->NumSamplers();
		if (numSamplers == 0)
		{
			// Bind default
			auto sampler = samplers.Fetch(op.GetSampler());
			context->PSSetSamplers(0, 1, &sampler);
			//context->VSSetSamplers(0, 1, &sampler);
		}
		else
		{
			ID3D11SamplerState* s[GraphicsShader::MAX_SAMPLERS];
			for (int i = 0; i < numSamplers; i++)
			{
				s[i] = samplers.Fetch(pixel->Samplers()[i]);
			}
			context->PSSetSamplers(0, numSamplers, s);
		}
	}

	if (((int)bindType & (int)BindType::Vertex) == (int)BindType::Vertex)
	{
		auto& vertex = vertexshaders.Fetch(op.GetPixelShader());
		int numSamplers = vertex->NumSamplers();
		if (numSamplers == 0)
		{
			// Bind default
			auto sampler = samplers.Fetch(op.GetSampler());
			context->VSSetSamplers(0, 1, &sampler);
		}
		else
		{
			ID3D11SamplerState* s[GraphicsShader::MAX_SAMPLERS];
			for (int i = 0; i < numSamplers; i++)
			{
				s[i] = samplers.Fetch(vertex->Samplers()[i]);
			}
			context->VSSetSamplers(0, numSamplers, s);
		}
	}

	// Bind constant buffer
	if (op.GetConstantBuffer() != -1)
	{
		constantbuffers.Fetch(op.GetConstantBuffer())->Bind(0, bindType);
	}

	if (op.GetConstantBuffer2() != -1)
	{
		constantbuffers.Fetch(op.GetConstantBuffer2())->Bind(1, bindType);
	}

	// Bind vertices
	auto vertex = vertexbuffers.Fetch(op.GetVertexBuffer());
	vertex->Bind(0);

	// Bind index buffer 
    if (op.GetIndexBuffer() != -1)
    {
        indexbuffers.Fetch(op.GetIndexBuffer())->Bind(0);
    }
    else
    {
	    context->IASetIndexBuffer(NULL, DXGI_FORMAT_UNKNOWN, NULL);
    }

	// Render
	D3D10_PRIMITIVE_TOPOLOGY topology = D3D10_PRIMITIVE_TOPOLOGY_TRIANGLELIST;
	if (op.GetTopology() == TOPOLOGY_LINELIST)
	{
		topology = D3D10_PRIMITIVE_TOPOLOGY_LINELIST;
	}

	context->IASetPrimitiveTopology(topology);
    if (op.GetIndexBuffer() != -1)
    {
        context->DrawIndexed(op.GetIndexCount()/*indexbuffers.Fetch(op.GetIndexBuffer())->Size() / 4*/, op.GetIndexOffset(), 0);
    }
    else
    {
	    int maxVertexCount = vertex->Size() / vertex->GetStride();
	    int vertexCount = op.GetVertexCount();
        context->Draw(vertexCount == 0 ? maxVertexCount : vertexCount, 0);
    }
	vertex->Swap();
}

void D3d11Renderer::SubmitDrawInstanced(const InstancedRenderOperation& op, int count)
{
	// Bind shaders
	BindType bindType = BindShader(op.GetPixelShader(), op.GetVertexShader(), -1);

	// Bind textures
	for (int i = 0; i < 4; i++)
	{
		if (op.GetTextures(i) != -1)
		{
			BindTexture(i, op.GetTextures(i));
		}
	}

	// Bind sampler
	auto sampler = samplers.Fetch(op.GetSampler());
	context->PSSetSamplers(0, 1, &sampler);
	context->VSSetSamplers(0, 1, &sampler);

	// Bind constant buffer
	for (int i = 0; i < 4; i++)
	{
		int id = op.GetConstantBuffer(i);
		if (id != -1)
		{
			constantbuffers.Fetch(id)->Bind(i, bindType);
		}
	}

	// Bind index buffer 
	context->IASetIndexBuffer(NULL, DXGI_FORMAT_UNKNOWN, NULL);

	// Bind vertices
	int vertexCount = 0;
	for (int i = 0; i < 4; i++)
	{
		int id = op.GetVertexBuffer(i);
		if (id != -1)
		{
			auto vertex = vertexbuffers.Fetch(id);
			vertex->Bind(i);

			if (i == 0)
			{
				vertexCount = vertex->Size() / vertex->GetStride();
			}
		}
	}

	// Render
	D3D10_PRIMITIVE_TOPOLOGY topology = D3D10_PRIMITIVE_TOPOLOGY_TRIANGLELIST;
	if (op.GetTopology() == TOPOLOGY_LINELIST)
	{
		topology = D3D10_PRIMITIVE_TOPOLOGY_LINELIST;
	}

	context->IASetPrimitiveTopology(topology);
	context->DrawInstanced(vertexCount, count, 0, 0);
	
	// Unbind old textures
	for (int i = 0; i < 4; i++)
	{
		if (op.GetTextures(i) != -1)
		{
			ID3D11ShaderResourceView *const pSRV[1] = { NULL };
			context->PSSetShaderResources(i, 1, pSRV);
			context->VSSetShaderResources(i, 1, pSRV);
		}
	}

	for (int i = 0; i < 4; i++)
	{
		int id = op.GetVertexBuffer(i);
		if (id != -1)
		{
			auto vertex = vertexbuffers.Fetch(id);
			vertex->Swap();
		}
	}
}

void D3d11Renderer::DispatchCompute(const ComputeOperation& op)
{
	computeshaders.Fetch(op.GetComputeShader())->Bind();

	int constantBuffer = op.GetConstantBuffer();
	if (constantBuffer != -1)
	{
		constantbuffers.Fetch(constantBuffer)->Bind(0, BindType::Compute);
	}

	int computeBuffer = op.GetComputeBuffer();
	if (computeBuffer != -1)
	{
		structuredbuffers.Fetch(computeBuffer)->Bind(0, BindType::Compute);
	}

	const int* tex = op.Textures();
	for (int i = 0; i < 4; i++)
	{
		if (tex[i] != -1)
		{
			//D3d11Texture& d3dtexture = textures.Fetch(tex[i]);
			//CreateTextureUAV(d3dtexture, device);

			auto uav = textures.Fetch(tex[i]).uav;
			context->CSSetUnorderedAccessViews(i, 1, &uav, nullptr);
		}
	}

	//D3D11_QUERY_DESC queryDesc;
	//std::memset(&queryDesc, 0, sizeof(queryDesc));
	//queryDesc.Query = D3D11_QUERY_EVENT;
	//if (FAILED(device->CreateQuery(&queryDesc, computeQuery)))
	//{
	//	Logger::GetLogger()->Log("Failed to create query EVENT (DispatchCompute)", LOGGING_RENDERING);
	//	return;
	//}

	//context->End(computeQuery);

	const int* threadGroups = op.ThreadGroups();

	context->Dispatch(threadGroups[0], threadGroups[1], threadGroups[2]);

	ID3D11UnorderedAccessView* uavs[4] = { nullptr, nullptr, nullptr, nullptr };
	context->CSSetUnorderedAccessViews(0, 4, uavs, nullptr);

	context->CSSetShader(nullptr, nullptr, 0);
}

static bool PushBinding(property_entry* entry, const Attribute& attribute, int offset, float* stream)
{
	switch (attribute.GetFormat())
	{
	case ISLANDER_SEMANTIC_FLOAT:
		stream[offset] = entry->data[0];
		return true;

	case ISLANDER_SEMANTIC_FLOAT2:
		stream[offset] = entry->data[0];
		stream[offset + 1] = entry->data[1];
		return true;

	case ISLANDER_SEMANTIC_FLOAT3:
		stream[offset] = entry->data[0];
		stream[offset + 1] = entry->data[1];
		stream[offset + 2] = entry->data[2];
		return true;

	case ISLANDER_SEMANTIC_FLOAT4:
		stream[offset] = entry->data[0];
		stream[offset + 1] = entry->data[1];
		stream[offset + 2] = entry->data[2];
		stream[offset + 3] = entry->data[3];
		return true;
	}

	return false;
}

static void Bind(row_entity* entities, const Attribute& attribute, int offset, float* stream)
{
	for (int i = 0; i < entities->count; ++i)
	{
		if (entities->entries[i].key == attribute.GetPropertyType())
		{
			PushBinding(&entities->entries[i], attribute, offset, stream);
		}
	}
}

void D3d11Renderer::BindQuad(float* stream, const QuadBindingContext& context)
{
	auto vertbuffer = vertexshaders.Fetch(context.mat->vertexShader);

	auto& attributes = vertbuffer->GetAttributes();

	for (auto& attribute : attributes)
	{
		int offset = attribute.GetOffset();
		if (offset == -1)
			continue;

		switch (attribute.GetType())
		{
		case ATTRIBUTE_CLASS_CUSTOM:
			Bind(context.properties, attribute, offset, stream);
			break;
		case ATTRIBUTE_CLASS_TRANSFORM:
		{
			//stream[offset] = x - camera->GetX();
			//stream[offset + 1] = y - camera->GetY();
			//stream[offset + 2] = sx / window->Width();
			//stream[offset + 3] = sy / window->Height();
		}
		break;
		case ATTRIBUTE_CLASS_TEXTURE0:
		case ATTRIBUTE_CLASS_TEXTURE1:
		case ATTRIBUTE_CLASS_TEXTURE2:
		case ATTRIBUTE_CLASS_TEXTURE3:
		{
			int index = attribute.GetType() - ATTRIBUTE_CLASS_TEXTURE0;
			stream[offset] = context.mat->textures[index].px;
			stream[offset + 1] = context.mat->textures[index].py;
			stream[offset + 2] = context.mat->textures[index].sx;
			stream[offset + 3] = context.mat->textures[index].sy;
		}
		break;
		}
	}
}

void D3d11Renderer::BindLine(float* stream, const LineBindingContext& context)
{
	auto vertbuffer = vertexshaders.Fetch(context.mat->vertexShader);

	auto& attributes = vertbuffer->GetAttributes();

	for (auto& attribute : attributes)
	{
		int offset = attribute.GetOffset();
		if (offset == -1)
			continue;

		if (attribute.GetType() == ATTRIBUTE_CLASS_CUSTOM)
		{
			Bind(context.properties, attribute, offset, stream);
		}
		else if (attribute.GetType() == ATTRIBUTE_CLASS_TRANSFORM)
		{
			stream[offset] = context.x - context.camera->GetX();
			stream[offset + 1] = context.y - context.camera->GetY();
			stream[offset + 2] = window->Width();
			stream[offset + 3] = window->Height();
		}
	}
}

void D3d11Renderer::BindText(float* stream, const TextBindingContext& context, const std::unordered_map<std::string, Font*>& engine)
{
	auto vertbuffer = vertexshaders.Fetch(context.vertex);

	auto& attributes = vertbuffer->GetAttributes();

	for (auto& attribute : attributes)
	{
		int offset = attribute.GetOffset();
		if (offset == -1)
			continue;

		switch (attribute.GetType())
		{
		case ATTRIBUTE_CLASS_CUSTOM:
			Bind(context.properties, attribute, offset, stream);
			break;
		case ATTRIBUTE_CLASS_TRANSFORM:
		{
			auto data = (TextData*)context.sprite->data;
			stream[offset] = ((data->centered ? ((float)-data->_rect.width) : -context.sx) + data->_rect.x + context.x + window->Width()) / window->Width() - 1;
			stream[offset + 1] = 1 - ((data->_rect.y - context.y + window->Height() - context.sy) / (window->Height()));
			stream[offset + 2] = context.z;
			stream[offset + 3] = 1;
			break;
		}
		case ATTRIBUTE_CLASS_FONT_COLOUR:
			stream[offset] = context.sprite->_colour.r;
			stream[offset + 1] = context.sprite->_colour.g;
			stream[offset + 2] = context.sprite->_colour.b;
			stream[offset + 3] = context.sprite->_colour.a;
			break;
		}
	}
}

BindType D3d11Renderer::BindShader(int pixelshader, int vertexshader, int geometryshader)
{
	int bindType = 0;

	auto pix = pixelshaders.Fetch(pixelshader);
	auto vert = vertexshaders.Fetch(vertexshader);

	if (geometryshader != -1)
	{
		auto geom = geometryshaders.Fetch(geometryshader);
		geom->Bind();
		bindType |= static_cast<int>(BindType::Geometry);
	}

    if (pix != cache.pixelShader)
    {
        pix->Bind();
        cache.pixelShader = pix;
		bindType |= static_cast<int>(BindType::Pixel);
    }
	else if (pix)
	{
		bindType |= static_cast<int>(BindType::Pixel);
	}

    if (vert != cache.vertexShader)
    {
        vert->Bind();
        cache.vertexShader = vert;
		bindType |= static_cast<int>(BindType::Vertex);
    }
	else if (vert)
	{
		bindType |= static_cast<int>(BindType::Vertex);
	}

	return static_cast<BindType>(bindType);
}

void D3d11Renderer::BindTexture(int slot, int texture)
{
	auto tex = textures.Fetch(texture);
	context->PSSetShaderResources(slot, 1, &tex.srv);
	context->VSSetShaderResources(slot, 1, &tex.srv);
}

void D3d11Renderer::ReportShaderError(ID3D10Blob * error)
{
	char* errorstring = (char*)error->GetBufferPointer();

	auto hwnd = static_cast<HWND>(this->window->Handle());
	MessageBoxA(hwnd, errorstring, "Error in code", 0);
}

void D3d11Renderer::ResizeBackBuffer()
{
	context->OMSetRenderTargets(0, 0, 0);

	// Release all outstanding references to the swap chain's buffers.
	rendertargetview->Release();

	HRESULT hr;
	// Preserve the existing buffer count and format.
	// Automatically choose the width and height to match the client rect for HWNDs.
	hr = swapchain->ResizeBuffers(0, 0, 0, DXGI_FORMAT_UNKNOWN, 0);

	// Perform error handling here!

	// Get buffer and create a render-target-view.
	hr = swapchain->GetBuffer(0, __uuidof(ID3D11Texture2D),
		(void**)&backbuffer);
	CreateRenderTarget(window->Width(), window->Height(), backbuffer, &rendertargetview);

	backbuffer->Release();

	// Set up the viewport.
	D3D11_VIEWPORT vp;
	vp.Width = window->Width();
	vp.Height = window->Height();
	vp.MinDepth = 0.0f;
	vp.MaxDepth = 1.0f;
	vp.TopLeftX = 0;
	vp.TopLeftY = 0;
	context->RSSetViewports(1, &vp);

	context->OMSetDepthStencilState(0, 0);

	depthview->Release();
	depthstenciltexture->Release();
	depthstate->Release();
	depthstate_no_write->Release();
	CreateDepthStencilTarget(window->Width(), window->Height(), &depthstenciltexture, &depthstate, &depthstate_no_write, &depthview);

	context->OMSetDepthStencilState(depthstate, 1);

	tex_lightmap->Release();
	view_lightmap->Release();
	CreateRenderTarget(RenderTargetType::Diffuse, window->Width(), window->Height(), &tex_lightmap, &view_lightmap, &texture_lightmap);
}

static void ParseDefinitions(const char* filename, std::vector<Attribute>& attributes)
{
	std::string def;
	GetFileNameWithoutExtension(filename, def);
	std::ifstream stream(def + ".def");
	if (!stream.fail())
	{
		std::string defstr((std::istreambuf_iterator<char>(stream)), std::istreambuf_iterator<char>());
		stream.close();
		ShaderDefinitionParser parser;
		parser.Parse(defstr, attributes);
	}
}

class D3D11RendererInclude : public ID3D10Include
{
    std::string _includeDirectory;
public:
    D3D11RendererInclude(const std::string& includeDirectory)
        : _includeDirectory(includeDirectory)
    { }

private:
    HRESULT Open(D3D_INCLUDE_TYPE IncludeType, LPCSTR pFileName, LPCVOID pParentData, LPCVOID* ppData, UINT* pBytes)
    {
        std::ifstream stream(_includeDirectory + pFileName);
        if (stream.fail())
        {
            return E_FAIL;
        }

        std::string s((std::istreambuf_iterator<char>(stream)), std::istreambuf_iterator<char>());
        stream.close();

        char* data = new char[s.size()];
        std::memcpy(data, s.c_str(), s.size());

        *ppData = data;
        *pBytes = s.size();

        return S_OK;
    }

    HRESULT Close(LPCVOID pData)
    {
        delete[] (char*)pData;
        return S_OK;
    }
};

static void GetDirectoryFromFilePath(const Islander::FileSystem::PathUTF8& file, std::string& dir)
{
    std::string name;
    Islander::FileSystem::GetFileName(file, name);
    dir = file.substr(0, file.length() - name.length());
}

int D3d11Renderer::LoadVertexShader(const char* filename, const char* method, SEMANTIC * semantics, int count, IslanderShaderDefineList* _defines)
{
	std::ifstream stream(filename);
	if (stream.fail())
	{
		MessageBox(NULL, L"Shader could not be loaded", L"Vertex shader error", 0);
		return -1;
	}

	std::string s((std::istreambuf_iterator<char>(stream)), std::istreambuf_iterator<char>());
	stream.close();

	std::vector<Attribute> attributes;
	ParseDefinitions(filename, attributes);

	ID3D10Blob * blob;
	ID3D10Blob * errors;

	D3D_SHADER_MACRO defines[ISLANDER_DEFINE_MAX_COUNT + 1] = {}; // last entry should be null

	if (_defines)
	{
		for (int i = 0; i < _defines->numDefines; i++)
		{
			defines[i].Name = _defines->defines[i].name;
			defines[i].Definition = _defines->defines[i].value;
		}
	}

    std::string path;
    GetDirectoryFromFilePath(filename, path);
    D3D11RendererInclude include(path);
	HRESULT res = D3DCompile(s.c_str(), s.size(), filename, nullptr, &include, method, "vs_5_0", 0, 0, &blob, &errors);

	if (FAILED(res))
	{
		ReportShaderError(errors);
		errors->Release();
		return -1;
	}
	
	D3d11VertexShader * shader = new D3d11VertexShader(device, blob, semantics, count);

	if (shader->HasErrors())
	{
		std::vector<std::wstring> errors;
		shader->GetErrors(errors);

		for (auto error : errors)
		{
			MessageBox(nullptr, error.c_str(), L"Vertex shader error", 0);
		}

		delete shader;
		return -1;
	}
	
	// Link the reflected shader against the definition to precompute the mapping.

	shader->Link(attributes);

	return vertexshaders.Add(shader);
}

int D3d11Renderer::LoadPixelShader(const char* filename, const char* method, IslanderShaderDefineList* _defines)
{
	std::ifstream stream(filename);
	if (stream.fail())
	{
		MessageBox(NULL, L"Shader could not be loaded", L"Vertex shader error", 0);
		return -1;
	}

	std::string s((std::istreambuf_iterator<char>(stream)), std::istreambuf_iterator<char>());
	stream.close();

	ID3D10Blob * blob;
	ID3D10Blob * errors;

	D3D_SHADER_MACRO defines[ISLANDER_DEFINE_MAX_COUNT + 1] = {}; // last entry should be null

	if (_defines)
	{
		for (int i = 0; i < _defines->numDefines; i++)
		{
			defines[i].Name = _defines->defines[i].name;
			defines[i].Definition = _defines->defines[i].value;
		}
	}

    std::string path;
    GetDirectoryFromFilePath(filename, path);
    D3D11RendererInclude include(path);
	HRESULT res = D3DCompile(s.c_str(), s.size(), filename, defines, &include, method, "ps_5_0", 0, 0, &blob, &errors);

	if (FAILED(res))
	{
		ReportShaderError(errors);
		errors->Release();

		return -1;
	}
	
	return pixelshaders.Add(new D3d11PixelShader(device, blob));
}

int D3d11Renderer::LoadComputeShader(const char* filename, const char* method, IslanderShaderDefineList* _defines)
{
	std::ifstream stream(filename);
	if (stream.fail())
	{
		MessageBox(NULL, L"Shader could not be loaded", L"Compute shader error", 0);
		return -1;
	}

	std::string s((std::istreambuf_iterator<char>(stream)), std::istreambuf_iterator<char>());
	stream.close();

	ID3D10Blob* blob;
	ID3D10Blob* errors;

	D3D_SHADER_MACRO defines[ISLANDER_DEFINE_MAX_COUNT + 1] = {}; // last entry should be null

	if (_defines)
	{
		for (int i = 0; i < _defines->numDefines; i++)
		{
			defines[i].Name = _defines->defines[i].name;
			defines[i].Definition = _defines->defines[i].value;
		}
	}

	std::string path;
	GetDirectoryFromFilePath(filename, path);
	D3D11RendererInclude include(path);
	HRESULT res = D3DCompile(s.c_str(), s.size(), filename, defines, &include, method, "cs_5_0", 0, 0, &blob, &errors);

	if (FAILED(res))
	{
		ReportShaderError(errors);
		errors->Release();

		return -1;
	}

	return computeshaders.Add(new D3d11ComputeShader(device, blob));
}

int D3d11Renderer::LoadGeometryShader(const char* filename, const char* method, IslanderShaderDefineList* _defines)
{
	std::ifstream stream(filename);
	if (stream.fail())
	{
		MessageBox(NULL, L"Shader could not be loaded", L"Geometry shader error", 0);
		return -1;
	}

	std::string s((std::istreambuf_iterator<char>(stream)), std::istreambuf_iterator<char>());
	stream.close();

	ID3D10Blob* blob;
	ID3D10Blob* errors;

	D3D_SHADER_MACRO defines[ISLANDER_DEFINE_MAX_COUNT + 1] = {}; // last entry should be null

	if (_defines)
	{
		for (int i = 0; i < _defines->numDefines; i++)
		{
			defines[i].Name = _defines->defines[i].name;
			defines[i].Definition = _defines->defines[i].value;
		}
	}

	std::string path;
	GetDirectoryFromFilePath(filename, path);
	D3D11RendererInclude include(path);
	HRESULT res = D3DCompile(s.c_str(), s.size(), filename, defines, &include, method, "gs_5_0", 0, 0, &blob, &errors);

	if (FAILED(res))
	{
		ReportShaderError(errors);
		errors->Release();

		return -1;
	}

	return geometryshaders.Add(new D3d11GeometryShader(device, blob));
}

void D3d11Renderer::SetPixelShaderSamplers(int pixelShader, int numSamplers, IslanderTextureSampler* samplers)
{
	if (numSamplers <= GraphicsShader::MAX_SAMPLERS)
	{
		int samplerIds[GraphicsShader::MAX_SAMPLERS];
		for (int i = 0; i < numSamplers; i++)
		{
			samplerIds[i] = CreateSampler(samplers[i].sampler_type == ISLANDER_SAMPLER_TYPE_WRAP);
		}

		pixelshaders.Fetch(pixelShader)->SetSamplers(numSamplers, samplerIds);
	}
}

void D3d11Renderer::SetVertexShaderSamplers(int vertexShader, int numSamplers, IslanderTextureSampler* samplers)
{
	if (numSamplers <= GraphicsShader::MAX_SAMPLERS)
	{
		int samplerIds[GraphicsShader::MAX_SAMPLERS];
		for (int i = 0; i < numSamplers; i++)
		{
			samplerIds[i] = CreateSampler(samplers[i].sampler_type == ISLANDER_SAMPLER_TYPE_WRAP);
		}

		vertexshaders.Fetch(vertexShader)->SetSamplers(numSamplers, samplerIds);
	}
}

ID3D11ShaderResourceView * D3d11Renderer::LoadTextureInternal(void* data, int size, int width, int height, bool greyscale, bool genMipMaps)
{
	ID3D11Texture2D* texture2D;
	D3D11_TEXTURE2D_DESC desc = {};
	desc.Width = width;
	desc.Height = height;
	desc.MipLevels = 0;
	desc.ArraySize = 1;
	desc.Format = greyscale ? DXGI_FORMAT_R8_UNORM : DXGI_FORMAT_R8G8B8A8_UNORM;
	desc.SampleDesc.Count = 1;
	desc.Usage = D3D11_USAGE_DEFAULT;
	desc.BindFlags = D3D11_BIND_SHADER_RESOURCE | D3D11_BIND_RENDER_TARGET | D3D11_BIND_UNORDERED_ACCESS;
	desc.MiscFlags = genMipMaps ? D3D11_RESOURCE_MISC_GENERATE_MIPS : 0;

	device->CreateTexture2D(&desc, nullptr, &texture2D);

    if (data && size > 0)
    {
        D3D11_BOX box;
        box.back = 1;
        box.front = 0;
        box.left = 0;
        box.top = 0;
        box.right = width;
        box.bottom = height;

        context->UpdateSubresource(texture2D, 0, &box, data, sizeof(int8_t) * 4 * width, 0);
    }

	D3D11_SHADER_RESOURCE_VIEW_DESC shaderDesc = {};
	shaderDesc.Texture2D.MipLevels = -1;
	shaderDesc.Texture2D.MostDetailedMip = 0;
	shaderDesc.Format = desc.Format;
	shaderDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
	ID3D11ShaderResourceView * texture;

	auto res = device->CreateShaderResourceView(texture2D, &shaderDesc, &texture);

	if (genMipMaps)
	{
		context->GenerateMips(texture);
	}

	return texture;
}

int D3d11Renderer::CreateEmptyTexture(int texture, int width, int height, int type)
{
    return LoadTexture(texture, nullptr, width, height, 0, type == ISLANDER_TEXTURE_TYPE_GREYSCALE);
}

void D3d11Renderer::CopyToTexture(int texture, void* data, int width, int height, int offsetX, int offsetY)
{
    auto resource = textures.Fetch(texture).srv;
    ID3D11Resource* texture2D;
    resource->GetResource(&texture2D);

    D3D11_BOX box;
    box.back = 1;
    box.front = 0;
    box.left = offsetX;
    box.top = offsetY;
    box.right = width + offsetX;
    box.bottom = height + offsetY;

    // TODO: this only works grey scale (R) channel only
    // width needs to be multiplied for more channels

    context->UpdateSubresource(texture2D, 0, &box, data, width, 0);
    context->GenerateMips(resource);
}

void D3d11Renderer::ClearTexture(int texture, int width, int height, int offsetX, int offsetY)
{
    // TODO: avoid doing this allocation
    // TODO: this only works grey scale (R) channel only
    unsigned char* data = new unsigned char[width*height];
    std::fill_n(data, width*height, 0);

    CopyToTexture(texture, data, width, height, offsetX, offsetY);

    delete[] data;
}

void D3d11Renderer::CreateComputeTexture(int texture)
{
	D3d11Texture& d3dtexture = textures.Fetch(texture);
	CreateTextureUAV(d3dtexture, device);
}

char* D3d11Renderer::GetBytesFromTexture(int texture, int* width, int* height, int* size)
{
	auto tex = textures.Fetch(texture).srv;
	if (tex == nullptr)
	{
		return 0;
	}
	
	ID3D11Texture2D* original;
	tex->GetResource((ID3D11Resource**)&original);

	D3D11_TEXTURE2D_DESC original_desc;
	original->GetDesc(&original_desc);

	D3D11_TEXTURE2D_DESC desc = {};
	*width = desc.Width = original_desc.Width;
	*height = desc.Height = original_desc.Height;
	desc.Usage = D3D11_USAGE_STAGING;
	desc.CPUAccessFlags = D3D11_CPU_ACCESS_READ;
	desc.Format = original_desc.Format;
	desc.MipLevels = original_desc.MipLevels;
	desc.ArraySize = original_desc.ArraySize;
	desc.SampleDesc.Count = original_desc.SampleDesc.Count;

	ID3D11Texture2D* t;

	if (FAILED(device->CreateTexture2D(&desc, nullptr, &t)))
	{
		return 0;
	}

	context->CopyResource(t, original);

	D3D11_MAPPED_SUBRESOURCE map;

	if (FAILED(context->Map(t, 0, D3D11_MAP_READ, 0, &map)))
	{
		t->Release();
		return 0;
	}

	*size = *width * *height*4;
	char* data = new char[*size];

	for (int row = 0; row < *height; row++)
	{
		memcpy(data + row * *width*4, (char*)map.pData + row * map.RowPitch, *width*4);
	}

	context->Unmap(t, 0);

	t->Release();

	return data;
}

void D3d11Renderer::GetStats(IslanderRendererStats& stats)
{
	stats.numConstantBuffers = constantbuffers.Count();
	stats.numIndexBuffers = indexbuffers.Count();
	stats.numVertexBuffers = vertexbuffers.Count();
}

int D3d11Renderer::LoadTexture(int tex, void* data, int width, int height, int size, bool greyscale)
{
    auto texture = LoadTextureInternal(data, size, width, height, greyscale, true);

    if (texture == nullptr)
    {
        std::stringstream buf(std::stringstream::in | std::stringstream::out);
        buf << "Texture: failed to load." << std::endl;

        auto hwnd = static_cast<HWND>(this->window->Handle());
        MessageBoxA(hwnd, buf.str().c_str(), "Error in code", 0);

        return -1;
    }

	D3d11Texture d3dTexture;
	d3dTexture.srv = texture;
	d3dTexture.uav = nullptr;

    textures.Replace(tex, d3dTexture);
    return tex;
}

int D3d11Renderer::LoadTexture(int tex, void* data, int width, int height, int size)
{
    return LoadTexture(tex, data, width, height, size, false);
}

int D3d11Renderer::LoadTexture(void* data, int width, int height, int size)
{
	auto texture = LoadTextureInternal(data, size, width, height, false, true);

	if (texture == nullptr)
	{
		std::stringstream buf(std::stringstream::in | std::stringstream::out);
		buf << "Texture: failed to load." << std::endl;

		auto hwnd = static_cast<HWND>(this->window->Handle());
		MessageBoxA(hwnd, buf.str().c_str(), "Error in code", 0);

		return -1;
	}

	return ProcessTexture(texture);
}

int D3d11Renderer::LoadTexture(const char* filename)
{
	TextureLoadInfo info;
	unsigned char * data = ::LoadTexture(filename, TEXTURE_LOADER_PNG, info);

	if (data == nullptr)
	{
		std::stringstream buf(std::stringstream::in | std::stringstream::out);
		buf << "Texture: " << filename << " failed to load." << std::endl;

		auto hwnd = static_cast<HWND>(this->window->Handle());
		MessageBoxA(hwnd, buf.str().c_str(), "Error in code", 0);

		return -1;
	}

	auto texture = LoadTextureInternal(data, info._size, info._width, info._height, false, true);
	if (texture == nullptr)
	{
		std::stringstream buf(std::stringstream::in | std::stringstream::out);
		buf << "Texture: " << filename << " failed to load." << std::endl;

		auto hwnd = static_cast<HWND>(this->window->Handle());
		MessageBoxA(hwnd, buf.str().c_str(), "Error in code", 0);

		return -1;
	}

	return ProcessTexture(texture);
}

int GetMipLevels(D3d11Texture& texture)
{
	auto source = texture.srv;
	ID3D11Texture2D* resource;
	source->GetResource((ID3D11Resource**)&resource);
	D3D11_TEXTURE2D_DESC desc;
	resource->GetDesc(&desc);
	return desc.MipLevels;
}

int D3d11Renderer::CreateTextureArray(int numTextures, int* textures_, int width, int height)
{
	ID3D11Texture2D* texture2D;
	D3D11_TEXTURE2D_DESC desc = {};
	desc.Width = width;
	desc.Height = height;
	desc.MipLevels = GetMipLevels(textures.Fetch(textures_[0]));
	desc.ArraySize = numTextures;
	desc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	desc.Usage = D3D11_USAGE_DEFAULT;
	desc.BindFlags = D3D11_BIND_SHADER_RESOURCE | D3D11_BIND_RENDER_TARGET;// | D3D11_BIND_UNORDERED_ACCESS;
	desc.MiscFlags = D3D11_RESOURCE_MISC_GENERATE_MIPS;
	desc.SampleDesc.Count = 1;
	desc.SampleDesc.Quality = 0;

	int textureID = -1;

	if (SUCCEEDED(device->CreateTexture2D(&desc, nullptr, &texture2D)))
	{
		for (int i = 0; i < numTextures; i++)
		{
			auto source = textures.Fetch(textures_[i]).srv;
			ID3D11Texture2D* resource;
			source->GetResource((ID3D11Resource**)&resource);

			D3D11_BOX box = {};
			box.left = 0;
			box.right = width;
			box.bottom = height;
			box.top = 0;
			box.front = 0;
			box.back = 1;

			context->CopySubresourceRegion(texture2D,
				D3D10CalcSubresource(0, i, desc.MipLevels), // sub resource dst
				0, // x
				0, // y
				0, // z
				resource,
				0, // sub resource src
				&box);
		}

		D3D11_SHADER_RESOURCE_VIEW_DESC view = {};
		view.Format = desc.Format;
		view.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2DARRAY;
		view.Texture2DArray.MipLevels = desc.MipLevels;
		view.Texture2DArray.MostDetailedMip = 0;
		view.Texture2DArray.ArraySize = numTextures;

		ID3D11ShaderResourceView* srv;
		if (SUCCEEDED(device->CreateShaderResourceView(texture2D, &view, &srv)))
		{
			context->GenerateMips(srv);

			D3d11Texture tex;
			tex.srv = srv;

			textureID = textures.Add(tex);
		}
	}

	return textureID;
}

int D3d11Renderer::CreateCubeMapTexture(int* textureFaces, int width, int height)
{
	ID3D11Texture2D* texture2D;
	D3D11_TEXTURE2D_DESC desc = {};
	desc.Width = width;
	desc.Height = height;
	desc.MipLevels = GetMipLevels(textures.Fetch(textureFaces[0]));
	desc.ArraySize = 6;
	desc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	desc.Usage = D3D11_USAGE_DEFAULT;
	desc.BindFlags = D3D11_BIND_SHADER_RESOURCE | D3D11_BIND_RENDER_TARGET;// | D3D11_BIND_UNORDERED_ACCESS;
	desc.MiscFlags = D3D11_RESOURCE_MISC_TEXTURECUBE | D3D11_RESOURCE_MISC_GENERATE_MIPS;
	desc.SampleDesc.Count = 1;
	desc.SampleDesc.Quality = 0;
	
	int textureID = -1;

	if (SUCCEEDED(device->CreateTexture2D(&desc, nullptr, &texture2D)))
	{
		for (int i = 0; i < 6; i++)
		{
			auto source = textures.Fetch(textureFaces[i]).srv;
			ID3D11Texture2D* resource;
			source->GetResource((ID3D11Resource**)&resource);

			D3D11_BOX box = {};
			box.left = 0;
			box.right = width;
			box.bottom = height;
			box.top = 0;
			box.front = 0;
			box.back = 1;

			context->CopySubresourceRegion(texture2D,
				D3D10CalcSubresource(0, i, desc.MipLevels), // sub resource dst
				0, // x
				0, // y
				0, // z
				resource,
				0, // sub resource src
				&box);
		}

		D3D11_SHADER_RESOURCE_VIEW_DESC view = {};
		view.Format = desc.Format;
		view.ViewDimension = D3D11_SRV_DIMENSION_TEXTURECUBE;
		view.TextureCube.MipLevels = desc.MipLevels;
		view.TextureCube.MostDetailedMip = 0;

		ID3D11ShaderResourceView* srv;
		if (SUCCEEDED(device->CreateShaderResourceView(texture2D, &view, &srv)))
		{
			context->GenerateMips(srv);

			D3d11Texture tex;
			tex.srv = srv;

			textureID = textures.Add(tex);
		}
	}

	return textureID;
}

int D3d11Renderer::CreateTextureAtlas(int sourceTexture, MaterialTexture* set, int count)
{
	auto source = textures.Fetch(sourceTexture).srv;
	ID3D11Texture2D* resource;
	source->GetResource((ID3D11Resource**)&resource);
	D3D11_TEXTURE2D_DESC sourceDesc;
	resource->GetDesc(&sourceDesc);

	float maxWidth = 0;
	float maxHeight = 0;
	for (int i = 0; i < count; i++)
	{
		maxWidth = max(maxWidth, set[i]._scale.GetX());
		maxHeight = max(maxHeight, set[i]._scale.GetY());
	}
	maxWidth *= sourceDesc.Width;
	maxHeight *= sourceDesc.Height;

	int max = (int)max(maxWidth, maxHeight);

	if (max <= 32)
		max = 32;
	else if (max <= 64)
		max = 64;
	else if (max <= 128)
		max = 128;
	else if (max <= 256)
		max = 256;
	else if (max <= 512)
		max = 512;
	else
		max = 1024;

	D3D11_TEXTURE2D_DESC desc;
	ZeroMemory(&desc, sizeof(desc));
	desc.Width = max;
	desc.Height = max;
	desc.MipLevels = 1;
	desc.ArraySize = count;
	desc.Format = sourceDesc.Format; // DXGI_FORMAT_R32G32B32A32_FLOAT;
	desc.SampleDesc.Count = 1;
	desc.Usage = D3D11_USAGE_DEFAULT;
	desc.BindFlags = D3D11_BIND_SHADER_RESOURCE;

	int8_t* empty = new int8_t[desc.Width * desc.Height * sizeof(int8_t) * 4];
	ZeroMemory(empty, desc.Width * desc.Height * sizeof(int8_t) * 4);

	D3D11_SUBRESOURCE_DATA* subRes = new D3D11_SUBRESOURCE_DATA[count];
	for (int i = 0; i < count; i++)
	{
		subRes[i].pSysMem = empty;
		subRes[i].SysMemPitch = sizeof(int8_t) * 4;
		subRes[i].SysMemSlicePitch = desc.Width * desc.Height * 4;
	}

	ID3D11Texture2D * texture;
	if (FAILED(device->CreateTexture2D(&desc, subRes, &texture)))
	{
		int error = GetLastError();
		return -1;
	}

	delete[] subRes;
	delete[] empty;
	
	for (int i = 0; i < count; i++)
	{
		auto& mat = set[i];
		mat._source = TEXTURE_SRC_ATLAS;
		mat._atlasIndex = i;

		D3D11_BOX box;
		box.back = 1;
		box.front = 0;
		box.left = mat._position.GetX()*sourceDesc.Width;
		box.top = mat._position.GetY()*sourceDesc.Height;
		box.bottom = box.top + ceilf(mat._scale.GetY()*sourceDesc.Height);

		// TODO: generate texture such it wraps
		Vec2i offset;

		while (offset.GetY() < max - 1)
		{
			offset.SetX(0);
			box.right = box.left + ceilf(mat._scale.GetX()*sourceDesc.Width);

			while (offset.GetX() < max - 1)
			{
				context->CopySubresourceRegion(texture, i, offset.GetX(), offset.GetY(), 0, resource, 0, &box);

				offset.SetX(offset.GetX() + std::min<unsigned int>(max - 1, (unsigned int)(mat._scale.GetX()*sourceDesc.Width)));
				if (offset.GetX() + sourceDesc.Width > max - 1)
				{
					box.right = max - 1;
				}
				break;
			}

			offset.SetY(offset.GetY() + mat._scale.GetY()*sourceDesc.Height);
			if (offset.GetY() + sourceDesc.Height > max - 1)
			{
				box.bottom = max - 1;
			}
			break;
		}
	}

	D3D11_SHADER_RESOURCE_VIEW_DESC srDesc;
	srDesc.Format = desc.Format;
	srDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2DARRAY;
	srDesc.Texture2DArray.MostDetailedMip = 0;
	srDesc.Texture2DArray.MipLevels = 1;
	srDesc.Texture2DArray.ArraySize = count;
	srDesc.Texture2DArray.FirstArraySlice = 0;

	ID3D11ShaderResourceView * resourceview;
	device->CreateShaderResourceView(texture, &srDesc, &resourceview);

	int atlas = ProcessTexture(resourceview);

	for (int i = 0; i < count; i++)
	{
		auto& mat = set[i];
		mat._texture = atlas;
		mat._scale.SetX(mat._scale.GetX() * (float)sourceDesc.Width / max);
		mat._scale.SetY(mat._scale.GetY() * (float)sourceDesc.Height / max);
	}

	return atlas;
}

int D3d11Renderer::CreateSampler(bool wrap)
{
	D3D11_SAMPLER_DESC samplerdescripton;

	ZeroMemory(&samplerdescripton, sizeof(D3D11_SAMPLER_DESC));
	samplerdescripton.AddressU = wrap ? D3D11_TEXTURE_ADDRESS_WRAP : D3D11_TEXTURE_ADDRESS_CLAMP; //D3D11_TEXTURE_ADDRESS_BORDER;
	samplerdescripton.AddressV = wrap ? D3D11_TEXTURE_ADDRESS_WRAP : D3D11_TEXTURE_ADDRESS_CLAMP; //D3D11_TEXTURE_ADDRESS_BORDER;
	samplerdescripton.AddressW = wrap ? D3D11_TEXTURE_ADDRESS_WRAP : D3D11_TEXTURE_ADDRESS_CLAMP; //D3D11_TEXTURE_ADDRESS_BORDER;
	samplerdescripton.ComparisonFunc = D3D11_COMPARISON_ALWAYS;
	samplerdescripton.Filter = /*D3D11_FILTER_MIN_MAG_MIP_POINT;*/ D3D11_FILTER_MIN_MAG_MIP_LINEAR;
	samplerdescripton.MaxLOD = D3D11_FLOAT32_MAX; //1
	for (int i = 0; i < 4; i++)
		samplerdescripton.BorderColor[i] = 0;

	ID3D11SamplerState * sampler;

	device->CreateSamplerState(&samplerdescripton, &sampler);

	return samplers.Add(sampler);
}

int D3d11Renderer::ReserveTexture()
{
	return textures.Reserve();
}

int D3d11Renderer::ProcessTexture(ID3D11ShaderResourceView* texture)
{
	D3d11Texture d3dTexture; d3dTexture.srv = texture; d3dTexture.uav = nullptr;

	return textures.Add(d3dTexture);
}

int D3d11Renderer::CreateRasterState(D3d11RasterState& rasterState)
{
	D3D11_RASTERIZER_DESC raster_desc;

	ZeroMemory(&raster_desc, sizeof(D3D11_RASTERIZER_DESC));

	D3D11_CULL_MODE cull_mode = D3D11_CULL_NONE;

	if (rasterState._state._backface_culling)
	{
		cull_mode = D3D11_CULL_BACK;
	}
	else if (rasterState._state._frontface_culling)
	{
		cull_mode = D3D11_CULL_FRONT;
	}

	raster_desc.MultisampleEnable = rasterState._state._multi_sampling;
	raster_desc.CullMode = cull_mode;
    raster_desc.FillMode = rasterState._state._wireframe ? D3D11_FILL_WIREFRAME : D3D11_FILL_SOLID;
	raster_desc.DepthClipEnable = TRUE;

	device->CreateRasterizerState(&raster_desc, rasterState._rasterstate);

    rasterStateList.push_back(rasterState);

	return 1;
}

int D3d11Renderer::CreateDepthStencilTarget(
	uint32_t width,
	uint32_t height,
	ID3D11Texture2D ** rendertarget,
	ID3D11DepthStencilState** state,
	ID3D11DepthStencilState ** state_no_write,
	ID3D11DepthStencilView** depthview)
{
	D3D11_TEXTURE2D_DESC depthstencil;

	ZeroMemory(&depthstencil, sizeof(D3D11_TEXTURE2D_DESC));

	depthstencil.ArraySize = 1;
	depthstencil.BindFlags = D3D11_BIND_DEPTH_STENCIL;
	depthstencil.CPUAccessFlags = 0;
	depthstencil.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
	depthstencil.Height = height;
	depthstencil.Width = width;
	depthstencil.MipLevels = 1;
	depthstencil.MiscFlags = 0;
	depthstencil.SampleDesc.Count = 1;
	depthstencil.SampleDesc.Quality = 0;
	depthstencil.Usage = D3D11_USAGE_DEFAULT;

	device->CreateTexture2D(&depthstencil, NULL, rendertarget);

	D3D11_DEPTH_STENCIL_DESC depthstencildesc;

	ZeroMemory(&depthstencildesc, sizeof(D3D11_DEPTH_STENCIL_DESC));

	depthstencildesc.DepthEnable = TRUE;
	depthstencildesc.StencilEnable = TRUE;

	depthstencildesc.StencilReadMask = 0xFF;
	depthstencildesc.StencilWriteMask = 0xFF;

    depthstencildesc.DepthFunc = D3D11_COMPARISON_LESS_EQUAL;

	depthstencildesc.BackFace.StencilDepthFailOp = D3D11_STENCIL_OP_KEEP;
	depthstencildesc.BackFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
	depthstencildesc.BackFace.StencilFunc = D3D11_COMPARISON_ALWAYS;
	depthstencildesc.BackFace.StencilPassOp = D3D11_STENCIL_OP_DECR;

	depthstencildesc.FrontFace.StencilDepthFailOp = D3D11_STENCIL_OP_KEEP;
	depthstencildesc.FrontFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
	depthstencildesc.FrontFace.StencilFunc = D3D11_COMPARISON_ALWAYS;
	depthstencildesc.FrontFace.StencilPassOp = D3D11_STENCIL_OP_INCR;

	depthstencildesc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ALL;

	device->CreateDepthStencilState(&depthstencildesc, state);

	D3D11_DEPTH_STENCIL_DESC depthstencildesc2;

	ZeroMemory(&depthstencildesc2, sizeof(D3D11_DEPTH_STENCIL_DESC));

	depthstencildesc2.DepthEnable = TRUE;
	depthstencildesc2.StencilEnable = TRUE;

	depthstencildesc2.StencilReadMask = 0xFF;
	depthstencildesc2.StencilWriteMask = 0xFF;

	depthstencildesc2.DepthFunc = D3D11_COMPARISON_LESS_EQUAL;

	depthstencildesc2.BackFace.StencilDepthFailOp = D3D11_STENCIL_OP_KEEP;
	depthstencildesc2.BackFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
	depthstencildesc2.BackFace.StencilFunc = D3D11_COMPARISON_ALWAYS;
	depthstencildesc2.BackFace.StencilPassOp = D3D11_STENCIL_OP_DECR;

	depthstencildesc2.FrontFace.StencilDepthFailOp = D3D11_STENCIL_OP_KEEP;
	depthstencildesc2.FrontFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
	depthstencildesc2.FrontFace.StencilFunc = D3D11_COMPARISON_ALWAYS;
	depthstencildesc2.FrontFace.StencilPassOp = D3D11_STENCIL_OP_INCR;

	depthstencildesc2.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ZERO;

	device->CreateDepthStencilState(&depthstencildesc2, state_no_write);

	D3D11_DEPTH_STENCIL_VIEW_DESC depthviewdesc;

	ZeroMemory(&depthviewdesc, sizeof(D3D11_DEPTH_STENCIL_VIEW_DESC));

	depthviewdesc.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2D;
	depthviewdesc.Format = DXGI_FORMAT::DXGI_FORMAT_D24_UNORM_S8_UINT;

	device->CreateDepthStencilView(*rendertarget, &depthviewdesc, depthview);



	return 1;
}

int D3d11Renderer::CreateRenderTarget(
	RenderTargetType type,
	uint32_t width,
	uint32_t height,
	ID3D11Texture2D ** texture,
	ID3D11RenderTargetView ** view,
	int * tex)
{
	D3D11_TEXTURE2D_DESC desc;
	ZeroMemory(&desc, sizeof(desc));
	desc.Width = width;
	desc.Height = height;
	desc.MipLevels = 1;
	desc.ArraySize = 1;	
	desc.SampleDesc.Count = 1;
	desc.Usage = D3D11_USAGE_DEFAULT;
	desc.BindFlags = D3D11_BIND_RENDER_TARGET | D3D11_BIND_SHADER_RESOURCE;

	switch (type)
	{
	case RenderTargetType::Depth:
		desc.Format = DXGI_FORMAT_R32_FLOAT;
		break;
	case RenderTargetType::ObjectID:
		desc.Format = DXGI_FORMAT_R16_UINT;
		break;
	case RenderTargetType::Diffuse:
	default:
		desc.Format = DXGI_FORMAT_B8G8R8A8_UNORM; // DXGI_FORMAT_R32G32B32A32_FLOAT;
		break;
	}

	device->CreateTexture2D(&desc, nullptr, texture);

	device->CreateRenderTargetView(*texture, nullptr, view);

	D3D11_SHADER_RESOURCE_VIEW_DESC srDesc;
	srDesc.Format = desc.Format;
	srDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
	srDesc.Texture2D.MostDetailedMip = 0;
	srDesc.Texture2D.MipLevels = 1;

	ID3D11ShaderResourceView * resourceview;
	device->CreateShaderResourceView(*texture, &srDesc, &resourceview);

	*tex = ProcessTexture(resourceview);

	return 1;
}
int D3d11Renderer::CreateRenderTarget(uint32_t width, uint32_t height, ID3D11Texture2D * backbuffer, ID3D11RenderTargetView ** view)
{
	device->CreateRenderTargetView(backbuffer, nullptr, view);
	return 1;
}

void D3d11Renderer::ReleaseRenderTarget(int handle)
{
	auto target = rendertargets.Fetch(handle);
	target.render_target->Release();
	if (target.depthview != depthview)
	{
		target.depthview->Release();
	}
	rendertargets.Remove(handle);
}

IslanderRenderTargetArray D3d11Renderer::CreateRenderTargetArray(RenderTargetType type, uint32_t width, uint32_t height, uint32_t dimensions)
{
	IslanderRenderTargetArray targetArray;
	targetArray.count = dimensions;
	targetArray._resource = new int[dimensions];

	D3D11_TEXTURE2D_DESC desc;
	ZeroMemory(&desc, sizeof(desc));
	desc.Width = width;
	desc.Height = height;
	desc.MipLevels = 1;
	desc.ArraySize = dimensions;
	desc.SampleDesc.Count = 1;
	desc.Usage = D3D11_USAGE_DEFAULT;
	desc.BindFlags = D3D11_BIND_RENDER_TARGET | D3D11_BIND_SHADER_RESOURCE;

	switch (type)
	{
	case RenderTargetType::Depth:
		desc.Format = DXGI_FORMAT_R32_FLOAT;
		break;
	case RenderTargetType::ObjectID:
		desc.Format = DXGI_FORMAT_R16_UINT;
		break;
	case RenderTargetType::Diffuse:
	default:
		desc.Format = DXGI_FORMAT_B8G8R8A8_UNORM; // DXGI_FORMAT_R32G32B32A32_FLOAT;
		break;
	}

	ID3D11Texture2D* texture;
	device->CreateTexture2D(&desc, nullptr, &texture);
	
	for (int i = 0; i < dimensions; i++)
	{
		D3d11RenderTargetItem item;
		D3D11_RENDER_TARGET_VIEW_DESC viewDesc;
		viewDesc.Format = desc.Format;
		viewDesc.ViewDimension = D3D11_RTV_DIMENSION_TEXTURE2DARRAY;
		viewDesc.Texture2DArray.MipSlice = 0;
		viewDesc.Texture2DArray.ArraySize = 1;
		viewDesc.Texture2DArray.FirstArraySlice = i;

		device->CreateRenderTargetView(texture, &viewDesc, &item.render_target);
		item.width = width;
		item.height = height;
		item.format = type;

		ID3D11Texture2D* depthstenciltexture;
		CreateDepthStencilTarget(item.width, item.height, &depthstenciltexture, &item.depthstate, &item.depthstate_no_write, &item.depthview);

		targetArray._resource[i] = rendertargets.Add(item);
	}

	D3D11_SHADER_RESOURCE_VIEW_DESC srDesc;
	srDesc.Format = desc.Format;
	srDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2DARRAY;
	srDesc.Texture2DArray.MostDetailedMip = 0;
	srDesc.Texture2DArray.MipLevels = 1;
	srDesc.Texture2DArray.ArraySize = dimensions;
	srDesc.Texture2DArray.FirstArraySlice = 0;

	ID3D11ShaderResourceView* resourceview;
	device->CreateShaderResourceView(texture, &srDesc, &resourceview);

	targetArray._texture = ProcessTexture(resourceview);

	return targetArray;
}

IslanderRenderTarget D3d11Renderer::CreateRenderTarget(RenderTargetType type, uint32_t width, uint32_t height)
{
	D3d11RenderTargetItem item;
	ID3D11Texture2D * texture;
	IslanderRenderTarget target;
	if (CreateRenderTarget(type, width, height, &texture, &item.render_target, &target._texture))
	{
		item.width = width;
		item.height = height;
		item.format = type;

		if (item.width == window->Width() && item.height == window->Height())
		{
			item.depthview = depthview;
		}
		else
		{
			ID3D11Texture2D* depthstenciltexture;
			CreateDepthStencilTarget(item.width, item.height, &depthstenciltexture, &item.depthstate, &item.depthstate_no_write, &item.depthview);
		}

		target._resource = rendertargets.Add(item);
	}
	return target;
}

void D3d11Renderer::ResizeRenderTarget(IslanderRenderTarget target, uint32_t width, uint32_t height)
{
    auto renderTarget = rendertargets.Fetch(target._resource);

    renderTarget.render_target->Release();

    int prevTextureIndex = target._texture;
    auto prevTexture = textures.Fetch(prevTextureIndex).srv;
    ID3D11Resource* resource;
    prevTexture->GetResource(&resource);
    resource->Release();
    prevTexture->Release();

    ID3D11Texture2D * texture;
    if (CreateRenderTarget(renderTarget.format, width, height, &texture, &renderTarget.render_target, &target._texture))
    {
		if (renderTarget.depthview != depthview)
		{
			renderTarget.depthview->Release();
		}

		if (width == window->Width() && height == window->Height())
		{
			renderTarget.depthview = depthview;
		}
		else
		{
			ID3D11Texture2D* depthstenciltexture;
			CreateDepthStencilTarget(width, height, &depthstenciltexture, &renderTarget.depthstate, &renderTarget.depthstate_no_write, &renderTarget.depthview);
		}

        rendertargets.Replace(target._resource, renderTarget);

        auto resource = textures.Fetch(target._texture);
        textures.Replace(prevTextureIndex, resource);
        textures.Remove(target._texture);
    }
}

void D3d11Renderer::Present()
{
	swapchain->Present(_syncInterval, 0);
}

void D3d11Renderer::SetFullscreen(bool mode)
{
	swapchain->SetFullscreenState(mode, NULL);
}

bool D3d11Renderer::GetTextureList(TextureListData& list)
{
    list.texture_count = textures.GetCount();
    for (int i = 0; i < list.texture_count; i++)
    {
        TextureData* data = &list.textures[i];
        data->id = i;

        auto resourceView = textures.Fetch(i).srv;
        if (resourceView != nullptr)
        {
            D3D11_SHADER_RESOURCE_VIEW_DESC viewDesc;
            resourceView->GetDesc(&viewDesc);

            if (viewDesc.ViewDimension == D3D_SRV_DIMENSION_TEXTURE2D)
            {
                ID3D11Resource *res;
                resourceView->GetResource(&res);

                D3D11_TEXTURE2D_DESC desc;
                ((ID3D11Texture2D*)res)->GetDesc(&desc);

                data->width = desc.Width;
                data->height = desc.Height;
            }
            else
            {
                // Not supported
                data->width = 0;
                data->height = 0;
            }
        }
        else
        {
            data->width = 0;
            data->height = 0;
        }
    }

    return true;
}

void D3d11Renderer::GetDisplayResolutions(DisplayResolutions& list)
{
    list = display_resolutions;
}

void D3d11Renderer::InitializeImgui()
{
	ImGui_ImplWin32_Init(window->Handle());
	ImGui_ImplDX11_Init(device, context);
}

void D3d11Renderer::NewFrameImgui()
{
	ImGui_ImplDX11_NewFrame();
	ImGui_ImplWin32_NewFrame();
}

void D3d11Renderer::RenderImgui()
{
	//ImVec4 clear_color = ImVec4(0.45f, 0.55f, 0.60f, 1.00f);

	//const float clear_color_with_alpha[4] = { clear_color.x * clear_color.w, clear_color.y * clear_color.w, clear_color.z * clear_color.w, clear_color.w };
	context->OMSetRenderTargets(1, &rendertargetview, NULL);
	//context->ClearRenderTargetView(rendertargetview, clear_color_with_alpha);
	ImGui_ImplDX11_RenderDrawData(ImGui::GetDrawData());
}

void D3d11Renderer::ImageImgui(int texture, float width, float height)
{
	auto resource = textures.Fetch(texture);
	ImGui::Image(resource.srv, ImVec2(width, height));
}

void D3d11Renderer::ImageImgui(int texture, float x, float y, float width, float height)
{
	auto resource = textures.Fetch(texture);
	ImGui::Image(resource.srv, ImVec2(width * 1024, height * 1024), ImVec2(x, y), ImVec2(x + width, y + height));
}

bool D3d11Renderer::ImageButtonImgui(int id, int texture, float x, float y, float sx, float sy)
{
	auto resource = textures.Fetch(texture);
	ImGui::PushID(id);
	bool clicked = ImGui::ImageButton(resource.srv, ImVec2(sx*1024, sy*1024), ImVec2(x, y), ImVec2(x + sx, y + sy));
	ImGui::PopID();
	return clicked;
}

void D3d11Renderer::SetSyncInterval(int syncInterval)
{
	_syncInterval = syncInterval;
}

ParticleRenderer* D3d11Renderer::GetParticleRenderer()
{
    return particleRenderer;
}

void D3d11Renderer::BeginDrawParticles(Camera3D* camera)
{
    D3d11BeginDrawParticles(particleRenderer, camera);
}

void D3d11Renderer::EndDrawParticles()
{
    D3d11EndDrawParticles(particleRenderer);
}

void D3d11Renderer::BeginGPUProfilingFrame()
{
    D3D11_QUERY_DESC queryDesc;
    std::memset(&queryDesc, 0, sizeof(queryDesc));
    queryDesc.Query = D3D11_QUERY_TIMESTAMP_DISJOINT;
    if (FAILED(device->CreateQuery(&queryDesc, disjoint)))
    {
        Logger::GetLogger()->Log("Failed to create query TIMESTAMP_DISJOINT", LOGGING_RENDERING);
        return;
    }

    context->Begin(disjoint);
}

int D3d11Renderer::EndGPUProfilingFrame()
{
    context->End(disjoint);
    return disjointItems.Add(disjoint);
}

bool D3d11Renderer::GetGPUProfileFrameData(int handle, IslanderGPUProfileFrameData& data)
{
    ID3D11Query* item = disjointItems.Fetch(handle);

    D3D11_QUERY_DATA_TIMESTAMP_DISJOINT disjointData;
    if (FAILED(context->GetData(item, &disjointData, sizeof(disjointData), 0)))
    {
        return false;
    }

    disjointItems.Remove(handle);

    data.frequency = disjointData.Frequency;
    data.disjoint = disjointData.Disjoint;

    return true;
}

void D3d11Renderer::BeginGPUProfiling()
{
    D3d11Profile p;
    D3D11_QUERY_DESC queryDesc;

    std::memset(&queryDesc, 0, sizeof(queryDesc));
    queryDesc.Query = D3D11_QUERY_TIMESTAMP;
    if (FAILED(device->CreateQuery(&queryDesc, p.start)))
    {
        Logger::GetLogger()->Log("Failed to create query TIMESTAMP", LOGGING_RENDERING);
        return;
    }

    std::memset(&queryDesc, 0, sizeof(queryDesc));
    queryDesc.Query = D3D11_QUERY_TIMESTAMP;
    if (FAILED(device->CreateQuery(&queryDesc, p.end)))
    {
        Logger::GetLogger()->Log("Failed to create query TIMESTAMP", LOGGING_RENDERING);
        return;
    }

    context->End(p.start);
    profile.push(p);
}

int D3d11Renderer::EndGPUProfiling()
{
    D3d11Profile& p = profile.top();

    context->End(p.end);

    int handle = profileItems.Add(p);
    profile.pop();
    return handle;
}

bool D3d11Renderer::GetGPUProfileData(int handle, IslanderGPUProfileData& data)
{
    D3d11Profile profile = profileItems.Fetch(handle);

    UINT64 start;
    UINT64 end;
    if (FAILED(context->GetData(profile.start, &start, sizeof(start), 0)))
    {
        return false;
    }

    if (FAILED(context->GetData(profile.end, &end, sizeof(end), 0)))
    {
        return false;
    }

    profileItems.Remove(handle);

    data.endTicks = end;
    data.startTicks = start;
    return true;
}

void D3d11Renderer::InitializePerformanceCounters()
{
	D3D11_COUNTER_INFO info;
	device->CheckCounterInfo(&info);

	auto& stream = Islander::Logger::GetStream(LOGGING_RENDERING);
	stream << "LastDeviceDependentCounter " << info.LastDeviceDependentCounter; stream.endl();
	stream << "NumSimultaneousCounters " << info.NumSimultaneousCounters; stream.endl();
	stream << "NumDetectableParallelUnits " << static_cast<int>(info.NumDetectableParallelUnits); stream.endl();

	for (int i = D3D11_COUNTER_DEVICE_DEPENDENT_0 + 1; i < info.LastDeviceDependentCounter; i++)
	{
		D3D11_COUNTER_DESC counterDesc = {};
		counterDesc.Counter = (D3D11_COUNTER)(D3D11_COUNTER_DEVICE_DEPENDENT_0 + 1);
		D3D11_COUNTER_TYPE type;
		UINT activeCounter;
		UINT nameSize = {};
		UINT unitsSize = {};
		UINT descriptionSize = {};
		device->CheckCounter(&counterDesc, &type, &activeCounter, 0, &nameSize, 0, &unitsSize, 0, &descriptionSize);

		char name[256];
		char units[256];
		char description[256];
		device->CheckCounter(&counterDesc, &type, &activeCounter, name, &nameSize, units, &unitsSize, description, &descriptionSize);
		stream << "nameSize " << name << " units " << units << " description " << description; stream.endl();
	}

	//D3D11_COUNTER_DESC counterDesc = {};
	//counterDesc.Counter = (D3D11_COUNTER) (D3D11_COUNTER_DEVICE_DEPENDENT_0 + 1);

	//ID3D11Counter* counter;

	//HRESULT result = device->CreateCounter(&counterDesc, &counter);
	//if (SUCCEEDED(result))
	//{
	//	counters.push_back(counter);
	//}
	//else
	//{
	//	stream << "Unable to create performance counters"; stream.endl();
	//}

	//for (auto& counter : counters)
	//{
	//	context->Begin(counter);
	//}
}
