#include "Renderer.h"
#include "Material.h"
#include "Component.h"
#include "Logger.h"
#include <string>
#include <vector>
#include <sstream>

extern "C"
{
#include <entity.h>
}

using namespace Islander;
using namespace Islander::Renderer;

RenderOperation::RenderOperation(int vertexBuffer, int constantBuffer, component_material* material, Topology topology, SamplerType sampler)
	:
	_vertexbuffer(vertexBuffer),
	_constantBuffer(constantBuffer),
    _constantBuffer2(-1),
    _indexBuffer(-1),
	_topology(topology),
	_sampler(sampler),
	_vertexshader(material->vertexShader),
    _pixelshader(material->pixelShader),
    _geometryShader(material->geometryShader),
	_vertexCount(0),
    _indexOffset(0),
    _indexCount(0)
{
	for (int i = 0; i < 4; i++)
	{
		_texture[i] = material->textures[i].index;
	}
}

RenderOperation::RenderOperation(int vertexBuffer, int constantBuffer, component_material* material, Topology topology, SamplerType sampler, int vertexCount)
	:
	_vertexbuffer(vertexBuffer),
	_constantBuffer(constantBuffer),
    _constantBuffer2(-1),
    _indexBuffer(-1),
	_topology(topology),
	_sampler(sampler),
	_vertexshader(material->vertexShader),
	_pixelshader(material->pixelShader),
    _geometryShader(material->geometryShader),
	_vertexCount(vertexCount),
    _indexOffset(0),
    _indexCount(0)
{
	for (int i = 0; i < 4; i++)
	{
		_texture[i] = material->textures[i].index;
	}
}

RenderOperation::RenderOperation(int vertexBuffer, int constantBuffer, int texture, int pixelshader, int vertexshader, Topology topology, SamplerType sampler)
	:
	_vertexbuffer(vertexBuffer),
	_constantBuffer(constantBuffer),
    _constantBuffer2(-1),
    _indexBuffer(-1),
	_pixelshader(pixelshader),
	_vertexshader(vertexshader),
    _geometryShader(-1),
	_topology(topology),
	_sampler(sampler),
	_vertexCount(0),
    _indexOffset(0),
    _indexCount(0)
{
	_texture[0] = texture;
	_texture[1] = -1;
	_texture[2] = -1;
	_texture[3] = -1;
}

RenderOperation::RenderOperation(int vertexBuffer, int constantBuffer, int indexBuffer, component_material* material, Topology topology, SamplerType sampler, RasterState rasterState, int indexOffset, int indexCount)
    :
    _vertexbuffer(vertexBuffer),
    _constantBuffer(constantBuffer),
    _constantBuffer2(-1),
    _indexBuffer(indexBuffer),
    _topology(topology),
    _sampler(sampler),
    _vertexshader(material->vertexShader),
    _pixelshader(material->pixelShader),
    _geometryShader(material->geometryShader),
    _vertexCount(0),
    _raster_state(rasterState),
    _indexOffset(indexOffset),
    _indexCount(indexCount)
{
    for (int i = 0; i < 4; i++)
    {
        _texture[i] = material->textures[i].index;
    }
}

RenderOperation::RenderOperation(int vertexBuffer, int constantBuffer, int constantBuffer2, int indexBuffer, component_material* material, Topology topology, SamplerType sampler, RasterState rasterState, int indexOffset, int indexCount)
    :
    _vertexbuffer(vertexBuffer),
    _constantBuffer(constantBuffer),
    _constantBuffer2(constantBuffer2),
    _indexBuffer(indexBuffer),
    _topology(topology),
    _sampler(sampler),
    _vertexshader(material->vertexShader),
    _pixelshader(material->pixelShader),
    _geometryShader(material->geometryShader),
    _vertexCount(0),
    _raster_state(rasterState),
    _indexOffset(indexOffset),
    _indexCount(indexCount)
{
    for (int i = 0; i < 4; i++)
    {
        _texture[i] = material->textures[i].index;
    }
}

InstancedRenderOperation::InstancedRenderOperation(int textures[4], int vertexBuffer[4], int constantBuffer[4], int pixelshader, int vertexshader, Topology topology, SamplerType sampler)
	:
	_pixelshader(pixelshader),
	_vertexshader(vertexshader),
	_topology(topology),
	_sampler(sampler)
{
	memcpy(_texture, textures, sizeof(_texture));
	memcpy(_vertexbuffer, vertexBuffer, sizeof(_vertexbuffer));
	memcpy(_constantBuffer, constantBuffer, sizeof(_constantBuffer));
}

ComputeOperation::ComputeOperation(int computeshader, int threadGroups[3], int constantBuffer, int textures[4], int computeBuffer)
    :
    _computeshader(computeshader),
    _constantBuffer(constantBuffer),
    _computeBuffer(computeBuffer)
{
    std::memcpy(_threadGroups, threadGroups, sizeof(_threadGroups));
    std::memcpy(_textures, textures, sizeof(_textures));
}

AttributeClass Islander::Renderer::ShaderDefinitionParser::MatchAttributeClass(const std::string& property)
{
	if (property == "Transform")
	{
		return ATTRIBUTE_CLASS_TRANSFORM;
	}
	else if (property == "Texture0")
	{
		return ATTRIBUTE_CLASS_TEXTURE0;
	}
	else if (property == "Texture1")
	{
		return ATTRIBUTE_CLASS_TEXTURE1;
	}
	else if (property == "Texture2")
	{
		return ATTRIBUTE_CLASS_TEXTURE2;
	}
	else if (property == "Texture3")
	{
		return ATTRIBUTE_CLASS_TEXTURE3;
	}
	else if (property == "Component")
	{
		return ATTRIBUTE_CLASS_CUSTOM;
	}
	else if (property == "TextColour")
	{
		return ATTRIBUTE_CLASS_FONT_COLOUR;
	}

	// Assume custom
	return ATTRIBUTE_CLASS_CUSTOM;
}

int Islander::Renderer::ShaderDefinitionParser::MatchProperty(const std::string& property)
{
	int index = IslanderGetComponentProperty(property.c_str());
	if (!index)
	{
		index = IslanderRegisterComponentProperty(property.c_str());
	}

	return index;
}

#define DEF_TOKEN_OPEN_BRACKET 0
#define DEF_TOKEN_CLOSE_BRACKET 1
#define DEF_TOKEN_EQUALS 2
#define DEF_TOKEN_COLON 3
#define DEF_TOKEN_TEXT 4
#define DEF_TOKEN_NONE 255

#define DEF_SYMBOL_TYPE 0
#define DEF_SYMBOL_BINDING 1
#define DEF_SYMBOL_NONE 255

void Islander::Renderer::ShaderDefinitionParser::Parse(const std::string& input, std::vector<Attribute>& attributes)
{
    std::vector<int> token_type;
    std::vector<int> token_index;
    std::vector<int> token_length;

    int length = input.length();
    int index = 0;
    while (index < length)
    {
        auto next = input.at(index);

        if (isspace(next))
        {
            index++;
        }
        else if (next == '[')
        {
            token_type.push_back(DEF_TOKEN_OPEN_BRACKET);
            token_index.push_back(index);
            token_length.push_back(1);
            index++;
        }
        else if (next == ']')
        {
            token_type.push_back(DEF_TOKEN_CLOSE_BRACKET);
            token_index.push_back(index);
            token_length.push_back(1);
            index++;
        }
        else if (next == '=')
        {
            token_type.push_back(DEF_TOKEN_EQUALS);
            token_index.push_back(index);
            token_length.push_back(1);
            index++;
        }
        else if (next == ':')
        {
            token_type.push_back(DEF_TOKEN_COLON);
            token_index.push_back(index);
            token_length.push_back(1);
            index++;
        }
        else
        {
            int start = index;
            token_type.push_back(DEF_TOKEN_TEXT);
            token_index.push_back(index);

            index++;

            bool cont = true;
            while (index < length && cont)
            {
                auto next2 = input.at(index);
                switch (next2)
                {
                case '[':
                case ']':
                case '=':
                case ':':
                    cont = false;
                    break;
                default:
                    if (isspace(next2))
                    {
                        cont = false;
                    }
                    else
                    {
                        index++;
                    }
                    break;
                }
            }

            token_length.push_back(index - start);
        }
    }

    AttributeClass attributeClass;
    int32_t format;
    std::string extension;
    std::string parameter;

    int symbol = DEF_SYMBOL_NONE;
    int token = DEF_TOKEN_NONE;
    for (int i = 0; i < token_index.size(); i++)
    {
        int next = token_type.at(i);
        switch (token)
        {
        case DEF_TOKEN_NONE:
            if (next == DEF_TOKEN_OPEN_BRACKET)
            {
                token = next;
            }
            else
            {
                Islander::Logger::GetLogger()->Log("Encountered error parsing shader definitions", LOGGING_RESOURCES);
                return;
            }
            break;

        case DEF_TOKEN_OPEN_BRACKET:
            if (next == DEF_TOKEN_TEXT)
            {
                token = next;
                int pos = token_index.at(i);
                int len = token_length.at(i);
                parameter = input.substr(pos, len);
            }
            else
            {
                Islander::Logger::GetLogger()->Log("Encountered error parsing shader definitions '['", LOGGING_RESOURCES);
                return;
            }
            break;
        case DEF_TOKEN_TEXT:
            if (next == DEF_TOKEN_CLOSE_BRACKET)
            {
                token = next;
            }
            else if (next == DEF_TOKEN_EQUALS)
            {
                token = next;
            }
            else if (next == DEF_TOKEN_OPEN_BRACKET)
            {
                token = next;
                attributes.push_back(Attribute(format, attributeClass, MatchProperty(extension), parameter));
                symbol = DEF_SYMBOL_NONE;
            }
            else if (next == DEF_TOKEN_TEXT)
            {
                token = next;
                int pos = token_index.at(i);
                int len = token_length.at(i);
                auto str = input.substr(pos, len);
                if (str == "Type")
                {
                    symbol = DEF_SYMBOL_TYPE;
                }
                else if (str == "Binding")
                {
                    symbol = DEF_SYMBOL_BINDING;
                }
            }
            else
            {
                token = next;
            }
            break;
        case DEF_TOKEN_CLOSE_BRACKET:
            if (next == DEF_TOKEN_TEXT)
            {
                token = next;
                int pos = token_index.at(i);
                int len = token_length.at(i);
                auto str = input.substr(pos, len);
                if (str == "Type")
                {
                    symbol = DEF_SYMBOL_TYPE;
                }
                else if (str == "Binding")
                {
                    symbol = DEF_SYMBOL_BINDING;
                }
            }
            else
            {
                Islander::Logger::GetLogger()->Log("Encountered error parsing shader definitions ']'", LOGGING_RESOURCES);
                return;
            }
            break;
        case DEF_TOKEN_EQUALS:
            if (next == DEF_TOKEN_TEXT)
            {
                token = next;
                int pos = token_index.at(i);
                int len = token_length.at(i);
                auto str = input.substr(pos, len);
                switch (symbol)
                {
                case DEF_SYMBOL_TYPE:
                    if (str == "Float")
                        format = ISLANDER_SEMANTIC_FLOAT;
                    else if (str == "Float2")
                        format = ISLANDER_SEMANTIC_FLOAT2;
                    else if (str == "Float3")
                        format = ISLANDER_SEMANTIC_FLOAT3;
                    else if (str == "Float4")
                        format = ISLANDER_SEMANTIC_FLOAT4;
                    break;
                case DEF_SYMBOL_BINDING:
                    attributeClass = MatchAttributeClass(str);
                    break;
                }
            }
            else
            {
                Islander::Logger::GetLogger()->Log("Encountered error parsing shader definitions '='", LOGGING_RESOURCES);
                return;
            }
            break;
        case DEF_TOKEN_COLON:
            if (next == DEF_TOKEN_TEXT)
            {
                token = next;
                int pos = token_index.at(i);
                int len = token_length.at(i);
                extension = input.substr(pos, len);
            }
            else if (next == DEF_TOKEN_OPEN_BRACKET)
            {
                token = next;
                attributes.push_back(Attribute(format, attributeClass, MatchProperty(extension), parameter));
                symbol = DEF_SYMBOL_NONE;
            }
            else
            {
                Islander::Logger::GetLogger()->Log("Encountered error parsing shader definitions ':'", LOGGING_RESOURCES);
                return;
            }
            break;
        }
    }

    attributes.push_back(Attribute(format, attributeClass, MatchProperty(extension), parameter));
}

void Islander::Renderer::GetFileNameWithoutExtension(const std::string& in, std::string& out)
{
	int index = in.find_last_of(".");
	out = in.substr(0, index);
}

void Islander::Renderer::GetFileNameExtension(const std::string& in, std::string& out)
{
	int index = in.find_last_of(".");
	out = in.substr(index);
}
