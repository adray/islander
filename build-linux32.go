package main

import (
	"fmt"
	"os"
	"os/exec"
)

func main() {

	islanderSource, _ := os.Getwd()
	fmt.Println("Building at " + islanderSource)
	
	// Build libpng
	//os.Chdir(islanderSource)
	
	
	// Generate makefiles
	fmt.Println("Generating unix makefiles")
	var out []byte
	var err error
	out, err = exec.Command("cmake", "-G\"Unix Makefiles\"", ".").Output()
	if err != nil {
		fmt.Println(err.Error())
		fmt.Println(string(out))
		os.Exit(2)
	}

	fmt.Println("Cleaning")
	_, err = exec.Command("make", "clean").Output()
	if err != nil {
	       fmt.Println(err.Error())
	       os.Exit(2)
	}

	fmt.Println("Building")
	_, err = exec.Command("make").Output()
	if err != nil {
	       fmt.Println(err.Error())
	       os.Exit(2)
	}
}
