﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Editor
{

    public class Plugin : Attribute
    {

        public string CommandName { get; set; }

        public bool CommandOnly { get; set; }

    }

    public class PluginMapAction : Attribute
    {

        

    }
        
    public interface IPluginContext
    {

    }

    public class PluginContext<T> : IPluginContext
    {
        public T Value { get; set; }
    }

    public interface IPlugin
    {
        void Action(IPluginContext context);
    }

    public class PluginInstance
    {

        public Plugin Plugin { get; set; }

        public IPlugin Instance { get; set; }
    }
    
    public static class PluginManager
    {
        private static Dictionary<string, PluginInstance> commands
            = new Dictionary<string, PluginInstance>();

        public static IPlugin GetCommand(string command)
        {
            PluginInstance instance = null;
            commands.TryGetValue(command, out instance);
            return instance == null ? null : instance.Instance;
        }

        public static List<PluginInstance> GetPlugins()
        {
            List<PluginInstance> plugins = new List<PluginInstance>();

            var files = System.IO.Directory.GetFiles(".");

            foreach (var file in files)
            {

                try
                {

                    var assembly = System.Reflection.Assembly.LoadFrom(file);

                    Type[] types = assembly.GetExportedTypes();

                    foreach (var type in types)
                    {
                        foreach (var attribute in type.GetCustomAttributes(true))
                        {
                            if (attribute.GetType() == typeof(Plugin))
                            {
                                PluginInstance instance = new PluginInstance();
                                instance.Plugin = attribute as Plugin;
                                instance.Instance = type.GetConstructor(new Type[0]).Invoke(new object[0]) as IPlugin;
                                
                                string commandName = (attribute as Plugin).CommandName;
                                if (!commands.ContainsKey(commandName))
                                {
                                    commands.Add(commandName, instance);
                                }

                                if (!(attribute as Plugin).CommandOnly)
                                {
                                    plugins.Add(instance);
                                }
                            }
                        }
                    }

                }
                catch (Exception)
                {
                }

            }

            return plugins;

        }
    }
}
