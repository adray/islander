﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Editor
{
 
    public partial class Form3 : Form
    {
        
        private enum Editmode
        {
            Draw=0,
            Trigger=1,
            Action=2,
            Fill=3
        }
        
        public Form3()
        {
            InitializeComponent();

            LoadSettings();

            SetLevel(3);

        }

        public class Tile
        {
            public int X {get;set;}
            public int Y { get; set; }

            public Bitmap BackImage { get; set; }
            public Bitmap FrontImage { get; set; }

            public Form1.TileType Type { get; set; }
            public Form1.TileType Type2 { get; set; }

        }

        public class Layer
        {

            public Bitmap Image { get; set; }
            public List<Form1.TileType> Types { get; set; }
            public string imagepath, typespath;
        }

        public class Trigger
        {
            public int X { get; set; }
            public int Y { get; set; }


            public ActionScript actionscript { get; set; }
            public ActionSpawnZone actionzone { get; set; }
            public ActionSpawnTower actiontower { get; set; }
            public ActionSpawnMerchant actionmerchant { get; set; }

        }

        public interface Action
        {
            void Save(System.IO.BinaryWriter writer);
        }

        public class ActionScript : Action
        {
            public string Script { get; set; }

            public void Save(System.IO.BinaryWriter writer)
            {
                writer.Write(Script);
            }
        }

        public class ActionSpawnZone : Action
        {
            public int InitialControl { get; set; }

            public void Save(System.IO.BinaryWriter writer)
            {
                writer.Write(InitialControl);
            }

        }

        public class ActionSpawnTower : Action
        {
            public int Tier { get; set; }
            public int Team { get; set; }

            public void Save(System.IO.BinaryWriter writer)
            {
                writer.Write(Tier);
                writer.Write(Team);
            }
        }

        public class ActionSpawnMerchant : Action
        {
            public void Save(System.IO.BinaryWriter writer)
            {
            }
        }

        private List<Form1.TileType> Types;

        private bool lighting = false;
 
        private int mapwidth, mapheight;

        private Tile[] tiles;
        private BufferedGraphicsContext context;
        private BufferedGraphics bg;
        private int width=8, height=8;
        private int offsetx=0, offsety=0;
        public int Level { get; private set; }

        private List<Trigger> triggers;
        private Editmode mode;

        private Layer[] layers;

        private Bitmap CachedMap { get; set; }
        private bool Redraw { get; set; }
        private string mappath;

        private bool showCellsCoords;

        public void SetLevel(int level)
        {
            this.Level = level;

            width = (int) Math.Pow(8, Level);
            height = (int)Math.Pow(8, Level);

        }

        public void LoadSettings()
        {
            triggers = new List<Trigger>();
            mode = Editmode.Draw;
            SetModeDisplay();

            mapheight = 512;
            mapwidth = 512;

            layers = new Layer[2];
            layers[0] = new Layer(); layers[1] = new Layer();
            //load layer images

            Types = new List<Form1.TileType>();

            for (int i = 0; i < 20; i++)
            {
                PictureBox box = new PictureBox();
                box.Location = new Point((i % 3)* 68, (i/3) * 68);
                box.BackColor = Color.FromArgb(255,i*10,i*10,i*10);
                box.Width = 64; box.Height = 64;
                box.Show();
                tabControl1.TabPages[0].Controls.Add(box);

                box.Click += new EventHandler(box_Click);
            }

            for (int i = 0; i < 10; i++)
            {
                PictureBox box = new PictureBox();
                box.Location = new Point((i % 3) * 68, (i / 3) * 68);
                box.BackColor = Color.FromArgb(255, 255, i * 10, (i%5) * 10);
                box.Width = 64; box.Height = 64;
                box.Show();
                tabControl1.TabPages[1].Controls.Add(box);

                box.Click += new EventHandler(box_Click2);
            }

            //load map - adds handler to swap cells with layer(right-layer1, left-layer3)
            Bitmap placeholder=new Bitmap(128,128);

            tiles = new Tile[mapwidth*mapheight];
            for (int i = 0; i < mapwidth; i++)
                for (int j = 0; j < mapheight; j++)
                {
                    tiles[i * mapwidth + j] = new Tile() { X=i, Y=j, BackImage=placeholder };
                }

            panel1.Paint += new PaintEventHandler(panel1_Paint);
            context = new BufferedGraphicsContext();
            bg = context.Allocate(panel1.CreateGraphics(), new Rectangle(0, 0, panel1.Width, panel1.Height));


            //some way to scroll map?
            panel1.MouseClick += new MouseEventHandler(panel1_MouseClick);

        }

        void panel1_MouseClick(object sender, MouseEventArgs e)
        {

            if (!(e.Location.X > 0 && e.Location.X < 512 && e.Location.Y > 0 && e.Location.Y < 512))
                return;

            switch (Level)
            {
                case 1:
                    int x = ((e.Location.X / ((64*8)/width))+offsetx);
                    int y = ((e.Location.Y / ((64*8)/height))+offsety);
                    int index = x * mapheight + y;

                    if (index >= tiles.Length)
                        return;

                    Tile tile = tiles[index];

                    if (mode == Editmode.Draw)
                    {

                        if (e.Button == System.Windows.Forms.MouseButtons.Left && pictureBox1.BackgroundImage != null)
                        {
                            tile.BackImage = (Bitmap)pictureBox1.BackgroundImage;
                            tile.Type = (Form1.TileType)pictureBox1.Tag;
                        }
                        if (e.Button == System.Windows.Forms.MouseButtons.Right && pictureBox2.BackgroundImage != null)
                        {
                            if (tile.Type2 != (Form1.TileType)pictureBox2.Tag)
                            {
                                tile.FrontImage = (Bitmap)pictureBox2.BackgroundImage;
                                tile.Type2 = (Form1.TileType)pictureBox2.Tag;
                            }
                            else
                            {
                                tile.FrontImage = null;
                                tile.Type2 = null;
                            }
                        }

                    }
                    else if (mode == Editmode.Trigger)
                    {
                        bool found = false;
                        Trigger trigger=null;
                        foreach (var trig in triggers)
                            if (trig.X == x && trig.Y == y)
                            {
                                found = true;
                                trigger=trig;
                            }
                        if (found)
                            triggers.Remove(trigger);
                        else
                            triggers.Add(new Trigger()
                            {
                                X = x,
                                Y = y
                            });
                    }
                    else if (mode == Editmode.Action)
                    {
                        foreach (var trig in triggers)
                            if (trig.X == x && trig.Y == y)
                            {
                                Form5 f = new Form5(trig);
                                if (f.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                                {
                                    trig.actionzone = f.SpawnZone;
                                    trig.actionscript = f.SpawnAI;
                                    trig.actionmerchant = f.SpawnMerchant;
                                    trig.actiontower = f.SpawnTower;
                                }
                            }
                    }
                    else if (mode == Editmode.Fill)
                    {
                        if (e.Button == System.Windows.Forms.MouseButtons.Left && pictureBox1.BackgroundImage != null)
                        {
                            var img = tile.BackImage;
                            HashSet<Tile> closedlist=new HashSet<Tile>();
                            Queue<Tile> openlist=new Queue<Tile>();
                            openlist.Enqueue(tile);
                            closedlist.Add(tile);
                            while (openlist.Count > 0)
                            {
                                Tile this_tile = openlist.Dequeue();

                                this_tile.BackImage = (Bitmap)pictureBox1.BackgroundImage;
                                this_tile.Type = (Form1.TileType)pictureBox1.Tag;

                                for (int i = -1; i <= 1; i++)
                                {
                                    for (int j = -1; j <= 1; j++)
                                    {
                                        if (Math.Abs(i + j) == 1)
                                        {
                                            if (this_tile.X + i < offsetx+width && this_tile.Y+ j < offsety+height && this_tile.X + i < mapwidth
                                                && j + this_tile.Y < mapheight && this_tile.X + i >= offsetx && this_tile.Y+j>=offsety)
                                            {
                                                Tile neighbour = tiles[(this_tile.X + i) * mapheight +this_tile.Y +j];
                                                if (!closedlist.Contains(neighbour))
                                                {
                                                    closedlist.Add(neighbour);
                                                    
                                                    if (neighbour.BackImage == img)
                                                        openlist.Enqueue(neighbour);
                                                }
                                                
                                            }
                                        }
                                    }
                                }

                            }

                        }
                    }

                    break;
                case 2:

                    offsetx += (e.Location.X / ((64*8)/8))*8;
                    offsety += (e.Location.Y / ((64*8)/8))*8;

                    if (e.Button == System.Windows.Forms.MouseButtons.Left)
                    {
                        SetLevel(1);
                    }
                    break;
                case 3:
                    offsetx = (e.Location.X / ((64*8)/8))*64;
                    offsety = (e.Location.Y / ((64*8)/8))*64;

                    if (e.Button == System.Windows.Forms.MouseButtons.Left)
                    {
                        SetLevel(2);
                    }
                    break;
            }

            Redraw = true;


            panel1.Invalidate();

        }

        void panel1_Paint(object sender, PaintEventArgs e)
        {
            Graphics g;

            if (Redraw || CachedMap == null)
            {
                if (CachedMap == null)
                    CachedMap = new Bitmap(width, height);

                System.Drawing.Imaging.ImageAttributes att = new System.Drawing.Imaging.ImageAttributes();
                att.SetColorKey(Color.White, Color.White);

                g = Graphics.FromImage(CachedMap);
                g.Clear(Color.White);
                int w = 64 * 8 / width;
                int h = 64 * 8 / height;

                for (int i = 0; i < width; i++)
                    for (int j = 0; j < height; j++)
                    {
                        if (i + offsetx < mapwidth && j + offsety < mapheight)
                        {

                            int index = (i + offsetx) * mapheight + j + offsety;
                            if (index < tiles.Length)
                            {
                                Tile tile = tiles[index];

                                if (Level == 3)
                                    g.FillRectangle(new SolidBrush(tile.BackImage.GetPixel(5, 5)), i * w, j * h, w, h);
                                else
                                {
                                    g.DrawImage(tile.BackImage, i * w, j * h, w, h);
                                    if (tile.FrontImage!= null)
                                        g.DrawImage(tile.FrontImage, new Rectangle(i * w, j * h, w, h), 0, 0, tile.FrontImage.Width, tile.FrontImage.Height,
                                            GraphicsUnit.Pixel, att); 
                                        //g.DrawImage(tile.FrontImage, i * w, j * h, w, h);

                                    if (Level == 1 && showCellsCoords)
                                        g.DrawString(tile.X + "," + tile.Y, new Font("Arial", 15), new SolidBrush(Color.Black), i * w, j * h + h / 2);

                                }
                            }
                            
                        }
                    }

                if (Level == 1)
                {
                    foreach (var trig in triggers)
                        g.FillEllipse(Brushes.Blue, (trig.X-offsetx) * w + 20, (trig.Y-offsety) * h + 20, 20, 20);
                }

            }

            g = bg.Graphics;

            g.DrawImage(CachedMap, 0, 0);

            bg.Render();

            Redraw = false;

        }

        void box_Click2(object sender, EventArgs e)
        {

            pictureBox2.Tag = ((PictureBox)sender).Tag;
            pictureBox2.BackColor = Color.Transparent;
            pictureBox2.BackgroundImage = ((PictureBox)sender).BackgroundImage;
            pictureBox2.BackgroundImageLayout = ImageLayout.Stretch;
            pictureBox2.Invalidate();

        }

        private void box_Click(object sender, EventArgs e)
        {

            pictureBox1.Tag = ((PictureBox)sender).Tag;
            pictureBox1.BackColor = Color.Transparent;
            pictureBox1.BackgroundImage = ((PictureBox)sender).BackgroundImage;
            pictureBox1.BackgroundImageLayout = ImageLayout.Stretch;
            pictureBox1.Invalidate();

        }

        private void button1_Click(object sender, EventArgs e)
        {
            SetLevel(1);
            Redraw = true;
            panel1.Invalidate();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            SetLevel(2);
            offsetx = (offsetx/64)*64; offsety = (offsety/64)*64;
            Redraw = true;
            panel1.Invalidate();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            SetLevel(3);
            offsetx = 0; offsety = 0;
            Redraw = true;
            panel1.Invalidate();
        }

        private void LoadLayerImage(int index)
        {
            OpenFileDialog open = new OpenFileDialog();
            open.InitialDirectory = System.IO.Directory.GetCurrentDirectory();
            open.Filter = "Image Files|*.png";
            open.Multiselect = false;
            if (open.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                var img = new Bitmap(open.FileName);
                var path = open.FileName;

                foreach (var x in layers)
                {
                    x.Image = img;
                    x.imagepath = path;
                }
            }
        }

        private void LoadTileDefinitions(int index)
        {
            OpenFileDialog open = new OpenFileDialog();
            open.Filter = "Tile Definition (*.tile)|*.tile|Material Files (*.mat)|*mat";
            open.InitialDirectory = System.IO.Directory.GetCurrentDirectory();
            open.Multiselect = false;

            DialogResult res = open.ShowDialog();

            if (res == System.Windows.Forms.DialogResult.OK)
            {
                layers[index].typespath = open.FileName;
                try
                {
                    if (System.IO.Path.GetExtension(open.FileName) == ".tile")
                    {
                        LoadTileDefinitions(open.OpenFile());
                    }                    
                    else if (System.IO.Path.GetExtension(open.FileName) == ".mat")
                    {
                        LoadMaterialFile(open.OpenFile());
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Load layer image first.");
                }
            }
        }

        public static List<Form1.TileType> LoadMaterialFile(string file)
        {
            return Form3.LoadMaterialFile2(System.IO.File.Open(file, System.IO.FileMode.Open));
        }

        private void LoadMaterialFile(System.IO.Stream open)
        {
            CreateTileTypes(Form3.LoadMaterialFile2(open));
        }

        private static List<Form1.TileType> LoadMaterialFile2(System.IO.Stream file)
        {
            System.IO.StreamReader sr = new System.IO.StreamReader(file);

            List<Form1.TileType> types = new List<Form1.TileType>();

            int itemcount = Int32.Parse(sr.ReadLine());
            int width = Int32.Parse(sr.ReadLine());
            int height = Int32.Parse(sr.ReadLine());

            for (int i = 0; i < itemcount; i++)
            {
                string name = sr.ReadLine();
                int x = Int32.Parse(sr.ReadLine());
                int y = Int32.Parse(sr.ReadLine());
                int w = Int32.Parse(sr.ReadLine());
                int h = Int32.Parse(sr.ReadLine());
                
                Form1.TileType t = new Form1.TileType();
                t.width = w;
                t.height = h;
                t.x = x;
                t.y = y;
                t.id = i;
                t.name = name;

                types.Add(t);
            }

            sr.Close();

            return types;
        }

        private void LoadTileDefinitions(System.IO.Stream open)
        {
            //Layer layer = layers[index];
            
            var tiles = Form1.LoadTiles(open);


            CreateTileTypes(tiles);

            //foreach (Tile tile in tiles)
            //    tile.Type = layer.Types[0];
        }

        private void CreateTileTypes(List<Form1.TileType> tiles)
        {
            foreach (var x in layers)
                x.Types = new List<Form1.TileType>();

            Types.Clear();


            foreach (var x in tiles)
            {
                Layer l = layers[x.layer];
                //x.id = l.Types.Count;
                l.Types.Add(x);

                Types.Add(x);
            }



            for (int j = 0; j < layers.Length; j++)
            {

                tabControl1.TabPages[j].Controls.Clear();
                var layer = layers[j];

                for (int i = 0; i < layer.Types.Count; i++)
                {
                    Form1.TileType tile = layer.Types[i];

                    if (tile.layer == j)
                    {

                        Bitmap map = (Bitmap)layer.Image.Clone(new Rectangle(tile.x, tile.y, tile.width, tile.height),
                            System.Drawing.Imaging.PixelFormat.Format32bppArgb);

                        PictureBox box = new PictureBox()
                        {
                            BackColor = Color.Transparent,
                            BackgroundImage = map,
                            Location = new Point((i % 3) * 68, (i / 3) * 68),
                            Width = 64,
                            Height = 64,
                            BorderStyle = BorderStyle.FixedSingle,
                            BackgroundImageLayout = ImageLayout.Stretch,
                            Tag = tile

                        };
                        box.Click += j == 0 ? new EventHandler(box_Click) : new EventHandler(box_Click2);

                        tabControl1.TabPages[j].Controls.Add(box);

                    }
                }

            }
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {

            SaveFileDialog save = new SaveFileDialog();
            save.InitialDirectory = System.IO.Directory.GetCurrentDirectory();
            save.Filter = "Map Files|*.map";

            if (save.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {

                mappath = System.IO.Path.Combine(save.InitialDirectory, save.FileName);
                System.IO.BinaryWriter br = new System.IO.BinaryWriter(save.OpenFile());

                br.Write(mapwidth);
                br.Write(mapheight);
                br.Write(Convert.ToInt32(lighting));    //whether the area is dark or not
                br.Write(1);    //temp version info
                br.Write(0);
                br.Write(0);

                for (int i = 0; i < mapwidth; i++)
                    for (int j = 0; j < mapheight; j++)
                    {
                        var v = tiles[i * mapheight + j].Type2 == null ? 0 : 1 + tiles[i * mapheight + j].Type2.id;
                        br.Write(tiles[i * mapheight + j].Type.id + 1000 * v);
                    }

                foreach (Trigger trig in triggers)
                {
                    br.Write(trig.X);
                    br.Write(trig.Y);

                    if (trig.actionzone != null)
                    {
                        br.Write((byte)1);
                        trig.actionzone.Save(br);
                    }
                    else
                        br.Write((byte)0);
                    if (trig.actionscript != null)
                    {
                        br.Write((byte)1);
                        trig.actionscript.Save(br);
                    }
                    else
                        br.Write((byte)0);
                    if (trig.actionmerchant != null)
                    {
                        br.Write((byte)1);
                        trig.actionmerchant.Save(br);
                    }
                    else
                        br.Write((byte)0);
                    if (trig.actiontower != null)
                    {
                        br.Write((byte)1);
                        trig.actiontower.Save(br);
                    }
                    else
                        br.Write((byte)0);
                }


                br.Close();

                this.Text = "Map - " + mappath;
            }
             

        }

        private void mapToolStripMenuItem_Click(object sender, EventArgs e)
        {

            OpenFileDialog load = new OpenFileDialog();
            load.InitialDirectory = System.IO.Directory.GetCurrentDirectory();
            load.Filter = "Map Files|*.map";
            load.Multiselect = false;

            if (load.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {

                mappath = load.FileName;
                LoadMap(load.OpenFile());

                Redraw = true;
                panel1.Invalidate();

                this.Text = "Map - " + mappath;
            }

        }

        private void LoadMap(System.IO.Stream load)
        {
            System.IO.BinaryReader br = new System.IO.BinaryReader(load);

            mapwidth=br.ReadInt32();
            mapheight=br.ReadInt32();
            lighting = Convert.ToBoolean(br.ReadInt32());
            var version = br.ReadInt32();
            br.ReadInt32();
            br.ReadInt32();
            tiles = new Tile[mapwidth * mapheight];
            triggers.Clear();
            int k = 0;
            for (int i = 0; i < mapheight; i++)
                for (int j = 0; j < mapwidth; j++)
                {
                    Tile tile = new Tile();
                    int id = br.ReadInt32();

                    if (version == 0)
                    {
                        //original version with only a single layer
                        //get the tile, determine layer
                        //fill appropriate layer with correct tile
                        //set back to grass if empty

                        var type = Types[id];

                        if (type.layer == 0)
                        {
                            for (int x = 0; x < layers[0].Types.Count; x++)
                                if (layers[0].Types[x].name == type.name)
                                {
                                    tile.BackImage = (Bitmap)tabControl1.TabPages[0].Controls[x].BackgroundImage;
                                    tile.Type = layers[0].Types[x];
                                    break;
                                }
                        }
                        else if (type.layer == 1)
                        {

                            for (int x = 0; x < layers[1].Types.Count; x++)
                                if (layers[1].Types[x].name == type.name)
                                {
                                    tile.FrontImage = (Bitmap)tabControl1.TabPages[1].Controls[x].BackgroundImage;
                                    tile.Type2 = layers[1].Types[x];
                                    break;
                                }

                            tile.BackImage = (Bitmap)tabControl1.TabPages[0].Controls[0].BackgroundImage;
                            tile.Type = Types[0];

                        }


                    }
                    else if (version == 1)
                    {

                        //new version with layers
                        //the formula for separating the id
                        //a/n + b = v/n, where a,b are the new value for the layers 0,1
                        //                  and v is the value read in
                        //                  and n is 1000 (max ids)
                        //check if layer 1 is defined if so set it
                        //set layer 0

                        int type2;

                        type2 = Math.DivRem(id, 1000, out id);
                        if (type2 > 0)
                        {
                            type2 -= 1;

                            var tn = Types[type2].name;

                            for (int x = 0; x < layers[1].Types.Count; x++)
                                if (layers[1].Types[x].name == tn)
                                {
                                    tile.FrontImage = (Bitmap)tabControl1.TabPages[1].Controls[x].BackgroundImage;
                                    tile.Type2 = layers[1].Types[x];
                                    break;
                                }

                        }

                        // if (version == 0)
                        {

                            var type = Types[id];

                            for (int x = 0; x < layers[0].Types.Count; x++)
                                if (layers[0].Types[x].name == type.name)
                                {
                                    tile.BackImage = (Bitmap)tabControl1.TabPages[0].Controls[x].BackgroundImage;
                                    tile.Type = layers[0].Types[x];
                                    break;
                                }
                            if (tile.BackImage == null)
                            {
                                tile.BackImage = (Bitmap)tabControl1.TabPages[0].Controls[0].BackgroundImage;
                                tile.Type = layers[0].Types[0];
                            }

                        }
                    }

                    tile.Y = k%mapheight; tile.X = k/mapheight;
                    tiles[i * mapwidth + j]=tile;
                    k++;
                }

            while (br.BaseStream.Position < br.BaseStream.Length)
            {
                Trigger trig = new Trigger()
                {
                    X=br.ReadInt32(),
                    Y=br.ReadInt32()
                };

                if (br.ReadByte() == 1)
                {
                    ActionSpawnZone zone = new ActionSpawnZone()
                    {
                        InitialControl=br.ReadInt32()
                    };
                    trig.actionzone = zone;
                }
                if (br.ReadByte() == 1)
                {
                    ActionScript s = new ActionScript()
                    {
                        Script=br.ReadString()
                    };
                    trig.actionscript = s;
                }
                if (br.ReadByte() == 1)
                {
                    ActionSpawnMerchant s = new ActionSpawnMerchant()
                    {
                        
                    };
                    trig.actionmerchant = s;
                }
                if (br.ReadByte() == 1)
                {
                    ActionSpawnTower s = new ActionSpawnTower()
                    {
                        Tier=br.ReadInt32(),
                        Team=br.ReadInt32(),
                    };
                    trig.actiontower = s;
                }

                triggers.Add(trig);

            }


            br.Close();
        }

        private void loadProjectToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog load = new OpenFileDialog();
            load.InitialDirectory = System.IO.Directory.GetCurrentDirectory();
            load.Filter = "Project Files|*.project";
            load.Multiselect = false;

            if (load.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {

                System.IO.BinaryReader br = new System.IO.BinaryReader(load.OpenFile());

                String mappath = br.ReadString();
                
                do
                {
                    //Layer layer = layers[i];

                    String imgpath = br.ReadString();
                    String typepath=  br.ReadString();

                    try
                    {

                        var img = new Bitmap(imgpath);

                        foreach (var layer in layers)
                        {
                            layer.Image = img;
                            layer.imagepath = imgpath;
                            layer.typespath = typepath;
                        }

                        try
                        {
                            LoadTileDefinitions(System.IO.File.OpenRead(typepath));
                            LoadMap(System.IO.File.OpenRead(mappath));
                            this.mappath = mappath;
                            //layer.typespath = typepath;
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(typepath + " not found.");
                        }
                    }
                    catch (Exception edx)
                    {
                        MessageBox.Show(imgpath + " not found.");
                    }

                    
                } while (br.BaseStream.Position < br.BaseStream.Length);
                
                br.Close();

                Redraw = true;
                offsetx = 0; offsety = 0;
                SetLevel(3);
                panel1.Invalidate();
                this.Text = "Map - " + mappath;
            }
        }

        private void saveProjectToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog save = new SaveFileDialog();
            save.InitialDirectory = System.IO.Directory.GetCurrentDirectory();
            save.Filter = "Project Files|*.project";

            if (save.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {

                System.IO.BinaryWriter br = new System.IO.BinaryWriter(save.OpenFile());

                br.Write(mappath);
                for (int i = 0; i < layers.Length; i++)
                {
                    if (layers[i].imagepath != null && layers[i].typespath != null)
                    {
                        br.Write(layers[i].imagepath);
                        br.Write(layers[i].typespath);
                    }
                }

                br.Close();
            }

        }

        private void imageToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            LoadLayerImage(1);
        }

        private void definitionToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            LoadTileDefinitions(1);
        }

        private void newToolStripMenuItem_Click(object sender, EventArgs e)
        {

            if (layers[0].Types == null)
            {
                MessageBox.Show("Must load tiles first.");
                return;
            }

            Form4 form = new Form4();

            //form.Owner = this;
            DialogResult res = form.ShowDialog();
            lighting = false;
            if (res == System.Windows.Forms.DialogResult.OK)
            {
                mapwidth = form.Width; mapheight = form.Height;
                tiles = new Tile[form.Width * form.Height];
                for (int i = 0; i < tiles.Length; i++)
                    tiles[i] = new Tile()
                        {
                            Type=layers[0].Types[0],
                            BackImage=(Bitmap)tabControl1.TabPages[0].Controls[0].BackgroundImage,
                            X = i / mapheight,
                            Y = i % mapheight,
                        };
                triggers.Clear();
                offsetx = 0; offsety = 0;
                SetLevel(3);
                Redraw = true;
                panel1.Invalidate();
                this.Text = "Map - Unsaved";
            }
            
        }

        private void button4_Click(object sender, EventArgs e)
        {

            mode++;

            if ((int)mode >= Enum.GetValues(typeof(Editmode)).Length)
                mode = 0;

            SetModeDisplay();

        }

        private void SetModeDisplay()
        {
            this.button4.Text = mode.ToString() + " Mode";
        }

        private void settingsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form6 form = new Form6(lighting);

            if (form.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                lighting = form.Lighting;

            }
        }

        private void definationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LoadTileDefinitions(0);
        }

        private void imageToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            LoadLayerImage(0);
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            showCellsCoords = ((CheckBox)sender).Checked;
            Redraw = true;
            panel1.Invalidate();
        }

        private void generatePathingToolStripMenuItem_Click(object sender, EventArgs e)
        {



        }


        class MinimapMaker
        {
            public MinimapMaker(string output, Form3 form)
            {

                Bitmap img = new Bitmap(512, 512);

                Graphics g = Graphics.FromImage(img);
                g.Clear(Color.White);

                System.Drawing.Imaging.ImageAttributes att = new System.Drawing.Imaging.ImageAttributes();
                att.SetColorKey(Color.White, Color.White);

                for (int i = 0; i < form.mapwidth; i++)
                {
                    for (int j = 0; j < form.mapheight; j++)
                    {
                        Tile t = form.tiles[j*form.mapwidth+i];
                        int w = img.Width/form.mapwidth; int h = img.Height / form.mapheight;
                        int x = t.X * w; int y = t.Y * h;
                        g.DrawImage(t.BackImage, new Rectangle(x, y, w, h), 0, 0, t.BackImage.Width, t.BackImage.Height, GraphicsUnit.Pixel, att);
                        if (t.FrontImage != null)
                        {
                            g.DrawImage(t.FrontImage, new Rectangle(x, y, w, h), 0, 0, t.FrontImage.Width, t.FrontImage.Height, GraphicsUnit.Pixel, att);
                        }
                    }
                }

                img.Save(output, System.Drawing.Imaging.ImageFormat.Png);

            }
        }

        class Pather
        {

            void GeneratePath(Tile[] tiles, int width, int height, string file)
            {

                System.IO.BinaryWriter writer = new System.IO.BinaryWriter(new System.IO.StreamWriter(file).BaseStream);
                writer.Write(width);
                writer.Write(height);

                for (int i = 0; i < width*height; i++)
                    for (int j = 0; j < width * height; j++)
                    {

                        //find path from every tile to the next

                    }

                writer.Close();

            }

        }

        private void exportMinimapToolStripMenuItem_Click(object sender, EventArgs e)
        {

            SaveFileDialog dialog = new SaveFileDialog();
            dialog.Filter = "Images|*png";

            var result = dialog.ShowDialog();

            if (result == System.Windows.Forms.DialogResult.OK)
            {

                MinimapMaker maker = new MinimapMaker(dialog.FileName, this);

            }

        }

    }
}
