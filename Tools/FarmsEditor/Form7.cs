﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Editor
{
    public partial class Form7 : Form
    {

        public SortedDictionary<string, Animation> animations;
        public SortedDictionary<string, Bitmap> images;
        public Bitmap spritesheet;

        public const int width = 128;
        public const int height = 128;

        public int animation_index = 0;

        public Timer timer;

        private bool looping = false;

        public class Animation
        {

            public Frame[] frames;

            public Animation()
            {
                frames = new Frame[8];
            }

        }

        public class Frame
        {

            public string image_name;
            public int length;

        }

        public class Material
        {
            public string name;
            public Rectangle rectangle;
        }
        
        public Form7()
        {
            InitializeComponent();
            animations = new SortedDictionary<string, Animation>();
            timer = new Timer();
            timer.Tick += timer_Tick;

            animationSetFlowlayout.SuspendLayout();
            for (int i = 0; i < 7; i++)
            {
                animationSetFlowlayout.Controls.Add(new ComboBox()
                {
                    Width = animation1.Width,
                    Height = animation1.Height,
                    DropDownStyle = animation1.DropDownStyle,
                });
                animationSetFlowlayout.Controls.Add(new TextBox()
                {
                    Width = animationTime1.Width,
                    Height = animationTime1.Height,
                });
            }
            animationSetFlowlayout.ResumeLayout();

            playbackSpeed.SelectedIndex = 0;
        }

        void timer_Tick(object sender, EventArgs e)
        {                        

            var fields = new List<TextBox>(8);
            var boxes = new List<ComboBox>(8);
            foreach (var control in animationSetFlowlayout.Controls)
            {
                if (control is TextBox)
                    fields.Add(control as TextBox);
                if (control is ComboBox)
                    boxes.Add(control as ComboBox);
            }

            if (boxes[animation_index].SelectedItem == null)
                return;

            var img = images[(string)boxes[animation_index].SelectedItem];
            
            // var g =panel1.CreateGraphics();
            RefreshPanel(panel1, img);

            int res;
            bool ok = Int32.TryParse(fields[animation_index].Text, out res);
            
            animation_index++;

            if (ok)
            {
                int modifier = 1;
                if (playbackSpeed.SelectedIndex == 1)
                    modifier = 2;
                else if (playbackSpeed.SelectedIndex == 2)
                    modifier = 4;

                timer.Interval = res * modifier;
            }

            if (!(ok && animation_index < boxes.Count && boxes[animation_index].SelectedItem != null))
            {
                if (looping)
                {
                    animation_index = 0;
                }
                else
                {
                    timer.Stop();
                }
            }

        }

        private void loadSpritesheetToolStripMenuItem_Click(object sender, EventArgs e)
        {

            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Filter = "Images|*.mat";
            DialogResult result = dialog.ShowDialog();

            List<Material> materials = new List<Material>();

            if (result == System.Windows.Forms.DialogResult.Yes || result == System.Windows.Forms.DialogResult.OK)
            {

                try
                {
                    string file = System.IO.Path.ChangeExtension(dialog.FileName, "png");
                    
                    spritesheet = (Bitmap)Bitmap.FromFile(file);
                    
                    var stream = new System.IO.StreamReader(dialog.OpenFile());

                    int count = Int32.Parse(stream.ReadLine());

                    stream.ReadLine(); stream.ReadLine();

                    for (int i = 0; i < count; i++)
                    {
                        Material material = new Material();
                        material.name = stream.ReadLine();
                        Rectangle mat = new Rectangle();
                        mat.X = Int32.Parse(stream.ReadLine());
                        mat.Y = Int32.Parse(stream.ReadLine());
                        mat.Width = Int32.Parse(stream.ReadLine());
                        mat.Height = Int32.Parse(stream.ReadLine());
                        material.rectangle = mat;
                        materials.Add(material);
                    }

                    stream.Close();

                }
                catch (Exception ex)
                {
                    MessageBox.Show(this, "Invalid spritesheet.");
                    return;
                }
            }
            else
                return;

            var watch = new System.Diagnostics.Stopwatch();
            watch.Start();

            Bitmap[] spritesheetpool = new Bitmap[10];
            spritesheetpool[0] = spritesheet;
            for (int i = 1; i < spritesheetpool.Length; i++)
                spritesheetpool[i] = new Bitmap(spritesheet); // (Bitmap)spritesheet.Clone();

            if (images == null)
            {
                images = new SortedDictionary<string, Bitmap>();
            }
            ImageCloner[] img_cloners = new ImageCloner[spritesheet.Width / width * spritesheet.Height / height];
            System.Threading.Thread[] threads = new System.Threading.Thread[spritesheet.Width / width * spritesheet.Height / height];

            int spritesheetwidth = spritesheet.Width/width;
            int spritesheetheight = spritesheet.Height / height;

            int img_cloner_count = 0;
            foreach (var mat in materials)
            {
                int i = mat.rectangle.X; int j = mat.rectangle.Y;
                var ic = new ImageCloner(i, j, mat.rectangle.Width, mat.rectangle.Height, mat.name, spritesheetpool[img_cloner_count%spritesheetpool.Length]);
                img_cloners[img_cloner_count] = ic;
                var thread = new System.Threading.Thread(new System.Threading.ThreadStart(ic.Run));
                thread.Start();
                threads[img_cloner_count++] = thread;
            }

            foreach (var thread in threads)
                if (thread != null)
                    thread.Join();

            foreach (var img in img_cloners)
                if (img != null)
                    images.Add(img.value.Key, img.value.Value);

            ComboBox[] boxes = new ComboBox[] { comboBox1, comboBox2, comboBox3 };

            foreach (var box in boxes)
                box.Items.Clear();

            foreach (var control in animationSetFlowlayout.Controls)
                if (control is ComboBox)
                    (control as ComboBox).Items.Clear();

            foreach (var img in images)
            {
                foreach (var box in boxes)
                    box.Items.Add(img.Key);

                foreach (var control in animationSetFlowlayout.Controls)
                    if (control is ComboBox)
                        (control as ComboBox).Items.Add(img.Key);
            }

            MessageBox.Show("loaded in " + watch.ElapsedMilliseconds + "ms");

        }

        private class ImageCloner
        {
            public KeyValuePair<string, Bitmap> value;
            private int i;
            private int j;
            private Bitmap spritesheet;
            private int width;
            private int height;
            private string name;

            public ImageCloner(int i, int j, int width, int height, string name, Bitmap spritesheet)
            {
                this.i = i; this.j = j; this.spritesheet = spritesheet;
                this.width = width; this.height = height; this.name = name;
            }

            public void Run()
            {
                lock (spritesheet)
                {

                    value = new KeyValuePair<string, Bitmap>(name,
                        spritesheet.Clone(new Rectangle(i, j, width, height), System.Drawing.Imaging.PixelFormat.Format32bppArgb));

                }
            }

        }

        private void loadToolStripMenuItem_Click(object sender, EventArgs e)
        {

            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Filter = "Animation|*.anim";
            var res = dialog.ShowDialog();

            if (res == System.Windows.Forms.DialogResult.OK || res == System.Windows.Forms.DialogResult.Yes)
            {

                animations.Clear();
                comboBox4.Items.Clear();

                var reader = new System.IO.BinaryReader(new System.IO.StreamReader(dialog.FileName).BaseStream);

                reader.ReadString();
                reader.ReadInt64();

                int anim_count = reader.ReadInt32();
                for (int k = 0; k < anim_count; k++)
                {
                    Animation anim = new Animation();

                    string key = reader.ReadString();
                    int framecount = reader.ReadInt32();

                    for (int i = 0; i < framecount; i++)
                    {
                        string name = reader.ReadString();
                        int length = reader.ReadInt32();
                        anim.frames[i] = new Frame() { length = length, image_name = name };
                    }

                    animations.Add(key, anim);
                    comboBox4.Items.Add(key);
                }

                reader.Close();

            }

        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
            SaveFileDialog dialog = new SaveFileDialog();
            dialog.Filter = "Animation|*.anim";
            var res = dialog.ShowDialog();

            if (res == System.Windows.Forms.DialogResult.OK || res == System.Windows.Forms.DialogResult.Yes)
            {

                var writer = new System.IO.BinaryWriter(new System.IO.StreamWriter(dialog.FileName).BaseStream);

                writer.Write(Environment.UserName);
                writer.Write(DateTime.Now.ToBinary());

                writer.Write(animations.Count);
                foreach (var anim in animations)
                {
                    writer.Write(anim.Key);
                    int framecount = 0;
                    for (framecount = 0; framecount < anim.Value.frames.Length; framecount++)
                    {
                        if (anim.Value.frames[framecount] == null)
                            break;
                    }

                    writer.Write(framecount);
                    for (int i = 0; i < framecount; i++)
                    {
                        writer.Write(anim.Value.frames[i].image_name);
                        writer.Write(anim.Value.frames[i].length);
                    }
                }

                writer.Close();

            }

        }

        private void comboBox4_SelectedIndexChanged(object sender, EventArgs e)
        {


            var anim = animations[(string)comboBox4.SelectedItem];

            var fields = new List<TextBox>(8);
            var boxes = new List<ComboBox>(8);
            foreach (var control in animationSetFlowlayout.Controls)
            {
                if (control is TextBox)
                    fields.Add(control as TextBox);
                if (control is ComboBox)
                    boxes.Add(control as ComboBox);
            }

            for (int i = 0; i < boxes.Count; i++)
            {
                if (anim.frames[i] != null)
                {
                    boxes[i].SelectedItem = anim.frames[i].image_name;
                    fields[i].Text = anim.frames[i].length.ToString();
                }
                else
                {
                    boxes[i].SelectedItem = null;
                    fields[i].Clear();
                }
            }

            textBox5.Text = (string)comboBox4.SelectedItem;

        }

        private void RefreshPanel(Panel panel, Image img)
        {

            try
            {
                var g = panel.CreateGraphics();

                g.Clear(Color.White);

                g.DrawImage(img, new Point(panel1.Width / 2 - img.Width / 2, panel1.Height / 2 - img.Height / 2));

            }
            catch (ObjectDisposedException)
            { }

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

            var img = images[(string)comboBox1.SelectedItem];

            RefreshPanel(panel2, img);
            
        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {

            var img = images[(string)comboBox2.SelectedItem];

            RefreshPanel(panel3, img);

        }

        private void comboBox3_SelectedIndexChanged(object sender, EventArgs e)
        {

            var img = images[(string)comboBox3.SelectedItem];

            RefreshPanel(panel4, img);

        }

        private void button1_Click(object sender, EventArgs e)
        {
            animation_index = 0;
            looping = false;
            timer.Start();
        }

        private void button2_Click(object sender, EventArgs e)
        {

            var fields = new List<TextBox>(8);
            var boxes = new List<ComboBox>(8);
            foreach (var control in animationSetFlowlayout.Controls)
            {
                if (control is TextBox)
                    fields.Add(control as TextBox);
                if (control is ComboBox)
                    boxes.Add(control as ComboBox);
            }

            Animation anim = new Animation();

            for (int i = 0; i < boxes.Count; i++)
            {
                int res;
                bool ok = Int32.TryParse(fields[i].Text, out res);

                if (boxes[i].SelectedItem == null || !ok)
                    break;

                anim.frames[i] = new Frame();
                anim.frames[i].image_name = (string)boxes[i].SelectedItem;
                anim.frames[i].length = res;

            }

            if (anim.frames[0] == null)
            {
                MessageBox.Show(this, "Animation empty");
                return;
            }

            if (animations.ContainsKey(textBox5.Text))
            {
                animations[textBox5.Text] = anim;
                MessageBox.Show(this, "Animation updated");
                return;
            }

            animations.Add(textBox5.Text, anim);
            comboBox4.Items.Add(textBox5.Text);

            MessageBox.Show(this, "Animation added");

        }

        private void button3_Click(object sender, EventArgs e)
        {

            ResetFields();

        }

        private void ResetFields()
        {
            var fields = new List<TextBox>(8);
            var boxes = new List<ComboBox>(8);
            foreach (var control in animationSetFlowlayout.Controls)
            {
                if (control is TextBox)
                    fields.Add(control as TextBox);
                if (control is ComboBox)
                    boxes.Add(control as ComboBox);
            }

            for (int i = 0; i < boxes.Count; i++)
            {
                boxes[i].SelectedItem = null;
                fields[i].Clear();
            }

            textBox5.Clear();
        }

        private void button4_Click(object sender, EventArgs e)
        {

            animations.Remove(textBox5.Text);
            comboBox4.Items.Remove(textBox5.Text);

            ResetFields();

        }

        private void button5_Click(object sender, EventArgs e)
        {
            animation_index = 0;
            looping = true;
            timer.Start();
        }
    }
}
