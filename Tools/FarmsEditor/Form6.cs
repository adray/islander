﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Editor
{
    public partial class Form6 : Form
    {

        public struct Elem
        {
            public bool on;

            public override string ToString()
            {
                return on.ToString();
    
            }
        }

        public bool Lighting { private set; get; }

        public Form6(bool value)
        {
            InitializeComponent();

            comboBox1.Items.Add(new Elem()
            {
                on=value
            });

            comboBox1.Items.Add(new Elem()
            {
                on = !value
            });

            comboBox1.SelectedIndex = 0;

        }

        private void button2_Click(object sender, EventArgs e)
        {
            DialogResult = System.Windows.Forms.DialogResult.OK;
            Lighting = ((Elem)comboBox1.SelectedItem).on;
            Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DialogResult = System.Windows.Forms.DialogResult.Abort;
            Close();
        }
    }
}
