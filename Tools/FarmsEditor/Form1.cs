﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Editor
{
    public partial class Form1 : Form
    {
        private TileType selected;
        private int id = 0;

        public class TileType
        {
            public string name;
            public int id;
            public int x, y;
            public int width, height;
            public int defence;
            public bool walkable;
            public bool wall;
            public bool water;
            public int layer;

            public TileType()
            {
                this.width = 128;
                this.height = 128;
            }

            public override String ToString()
            {
                return name;
            }

        }

        public Form1()
        {
            InitializeComponent();

            Clear();
        }

        private void Clear()
        {
            this.comboBox2.SelectedItem = this.comboBox2.Items[0];
            this.comboBox3.SelectedItem = this.comboBox3.Items[1];
            this.comboBox4.SelectedItem = this.comboBox4.Items[1];

            this.textBox1.Text = "";
            this.textBox2.Text = id.ToString();
            this.textBox3.Text = "";
            this.textBox4.Text = "";
            this.textBox5.Text = "";

            this.comboBox5.SelectedItem = this.comboBox5.Items[0];
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //clear

            Clear();

        }

        private void button2_Click(object sender, EventArgs e)
        {

            //submit
            try
            {

                selected = new TileType();
                selected.walkable = Boolean.Parse(this.comboBox2.SelectedItem.ToString());
                selected.wall = Boolean.Parse(this.comboBox3.SelectedItem.ToString());
                selected.water = Boolean.Parse(this.comboBox4.SelectedItem.ToString());
                selected.layer = this.comboBox5.SelectedIndex;

                selected.name = this.textBox1.Text;
                selected.id = Int32.Parse(this.textBox2.Text);
                selected.x = Int32.Parse(this.textBox3.Text);
                selected.y = Int32.Parse(this.textBox4.Text);
                selected.defence = Int32.Parse(this.textBox5.Text);

                for (int i = 0; i < this.comboBox1.Items.Count; i++)
                    if (((TileType)this.comboBox1.Items[i]).id == selected.id)
                    {
                        this.comboBox1.Items[i] = selected;
                        return;
                    }

                this.comboBox1.Items.Add(selected);
                id++;

            }
            catch (Exception ex)
            {
                MessageBox.Show("Incorrect data.");
            }

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

            //swap item

            selected = (TileType) this.comboBox1.SelectedItem;

            this.comboBox2.SelectedItem = this.comboBox2.Items[selected.walkable ? 0 : 1];
            this.comboBox3.SelectedItem = this.comboBox3.Items[selected.wall ? 0 : 1];
            this.comboBox4.SelectedItem = this.comboBox4.Items[selected.water ? 0 : 1];

            this.textBox1.Text = selected.name.ToString();
            this.textBox2.Text = selected.id.ToString();
            this.textBox3.Text = selected.x.ToString();
            this.textBox4.Text = selected.y.ToString();
            this.textBox5.Text = selected.defence.ToString();

            this.comboBox5.SelectedIndex = selected.layer;
        }

        private void saveAsToolStripMenuItem_Click(object sender, EventArgs e)
        {

            SaveFileDialog save = new SaveFileDialog();
            save.InitialDirectory = System.IO.Directory.GetCurrentDirectory();
            save.Filter = "Tile Definition (*.tile)|*.tile";

            DialogResult res = save.ShowDialog( );


            if (res == System.Windows.Forms.DialogResult.OK)
            {

                try
                {

                    System.IO.BinaryWriter writer = new System.IO.BinaryWriter(
                        save.OpenFile());

                    writer.Write(DateTime.Now.ToBinary());
                    writer.Write(Environment.UserName);
                    writer.Write(comboBox1.Items.Count);

                    foreach (TileType t in comboBox1.Items)
                    {

                        writer.Write(t.name);
                        writer.Write(t.id);
                        writer.Write(t.x);
                        writer.Write(t.y);
                        writer.Write(t.defence);
                        writer.Write(t.walkable);
                        writer.Write(t.wall);
                        writer.Write(t.water);
                        writer.Write(t.layer);

                    }

                    writer.Close();

                }
                catch (Exception ex)
                {

                    MessageBox.Show("IO error.");

                }


            }


        }

        public static List<TileType> LoadTiles(System.IO.Stream file)
        {

            List<TileType> tiles = new List<TileType>();

            try
                {

                    System.IO.BinaryReader reader = new System.IO.BinaryReader(
                        file);


                    long date = reader.ReadInt64();
                    string author = reader.ReadString();
                    int count = reader.ReadInt32();

                    for (int i = 0; i < count; i++)
                    {
                        TileType type = new TileType();
                        type.name = reader.ReadString();
                        type.id = reader.ReadInt32();
                        type.width = 128;
                        type.height = 128;
                        type.x = reader.ReadInt32()*128;
                        type.y = reader.ReadInt32()*128;
                        type.defence = reader.ReadInt32();
                        type.walkable = reader.ReadBoolean();
                        type.wall = reader.ReadBoolean();
                        type.water = reader.ReadBoolean();
                        type.layer = reader.ReadInt32();
                        tiles.Add(type);
                    }
                    reader.Close();

                }
                catch (Exception ex)
                {

                    MessageBox.Show("IO error.");

                }

                return tiles;


        }


        private void loadToolStripMenuItem_Click(object sender, EventArgs e)
        {

            id = 0;
            selected = null;
            comboBox1.Items.Clear();


            OpenFileDialog open = new OpenFileDialog();
            open.Filter = "Tile Definition (*.tile)|*.tile";
            open.InitialDirectory = System.IO.Directory.GetCurrentDirectory();

            DialogResult res = open.ShowDialog();

            if (res == System.Windows.Forms.DialogResult.OK)
            {
                List<TileType> tiles = LoadTiles(open.OpenFile());

                foreach (TileType type in tiles)
                {
                    comboBox1.Items.Add(type);
                    id = Math.Max(id, type.id);
                }
                id++;
            }

            Clear();

        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {

            MessageBox.Show("QuantumSource Editor v1.0.0", "About");

        }
    }
}
