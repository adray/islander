﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Editor
{
    public partial class Start : Form
    {
        static List<PluginInstance> plugins;

        public Start()
        {
            InitializeComponent();

            Point loc = button4.Location;
            Size size = button4.Size;

            plugins = PluginManager.GetPlugins();

            foreach (var plugin in plugins)
            {

                loc.Y += 50;

                Button button = new Button();
                button.Text = plugin.Plugin.CommandName;
                button.Click += (o, e) =>
                {
                    plugin.Instance.Action(new PluginContext<Start>()
                        {
                            Value = this,
                        });
                };
                button.Location = loc;
                button.Size = size;
                Controls.Add(button);

            }
        }
        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);

            // hack to close all windows
            foreach (var plugin in plugins)
            {
                Type type = plugin.Instance.GetType();
                type.GetMethod("Close").Invoke(plugin.Instance, new object[] { });
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //tile
            new Form1().Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            //item
            new Form2().Show();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            //map
            new Form3().Show();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            //animation
            new Form7().Show();
        }
    }
}
