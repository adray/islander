﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Editor
{
    public partial class Form4 : Form
    {
        public int Width { get; private set; }
        public int Height { get; private set; }

        public Form4()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                Width = Int32.Parse(textBox1.Text);
                Height = Int32.Parse(textBox2.Text);
                if (Width <= 0 || Height <= 0 || Width >= 512 || Height >= 512)
                    throw new Exception();

                this.DialogResult = System.Windows.Forms.DialogResult.OK;
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Invalid selection.");
            }

        }

    }
}
