﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Editor
{
    public partial class Form5 : Form
    {

        public Form3.ActionSpawnMerchant SpawnMerchant { get; private set; }
        public Form3.ActionScript SpawnAI { get; private set; }
        public Form3.ActionSpawnTower SpawnTower { get; private set; }
        public Form3.ActionSpawnZone SpawnZone { get; private set; }

        public Form5(Editor.Form3.Trigger trigger)
        {
            InitializeComponent();

            SpawnMerchant = trigger.actionmerchant;
            SpawnAI = trigger.actionscript;
            SpawnTower = trigger.actiontower;
            SpawnZone = trigger.actionzone;

            if (SpawnTower != null)
            {
                checkBox1.Checked = true;
                groupBox1.Enabled = true;
                textBox2.Text = SpawnTower.Team.ToString();
                textBox3.Text = SpawnTower.Tier.ToString();
            }
            else
            {
                checkBox1.Checked = false;
                groupBox1.Enabled = false;
            }

            if (SpawnAI != null)
            {
                checkBox2.Checked = true;
                groupBox2.Enabled = true;
                textBox4.Text = SpawnAI.Script;
            }
            else
            {
                checkBox2.Checked = false;
                groupBox2.Enabled = false;
            }

            if (SpawnZone != null)
            {
                checkBox3.Checked = true;
                groupBox3.Enabled = true;

            }
            else
            {
                checkBox3.Checked = false;
                groupBox3.Enabled = false;
            }

            if (SpawnZone != null)
            {
                checkBox4.Checked = true;
                groupBox4.Enabled = true;
                textBox1.Text = SpawnZone.InitialControl.ToString();
            }
            else
            {
                checkBox4.Checked = false;
                groupBox4.Enabled = false;
            }

            DialogResult = System.Windows.Forms.DialogResult.Cancel;

        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {

            groupBox1.Enabled = checkBox1.Checked;

        }

        private void button2_Click(object sender, EventArgs e)
        {
            DialogResult = System.Windows.Forms.DialogResult.OK;
            Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            checkBox1.Checked = false;
            checkBox2.Checked = false;
        }

        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);

            if (checkBox1.Checked)
            {
                try
                {
                    SpawnTower = new Form3.ActionSpawnTower()
                    {
                        Team=Int32.Parse(textBox2.Text),
                        Tier=Int32.Parse(textBox3.Text),
                    };
                }
                catch (Exception ex)
                {
                    DialogResult = System.Windows.Forms.DialogResult.Ignore;
                }
            }
            else
                SpawnTower = null;

            if (checkBox2.Checked)
            {
                try
                {
                    SpawnAI = new Form3.ActionScript()
                    {
                        Script = textBox4.Text
                    };
                }
                catch (Exception ex)
                {
                    DialogResult = System.Windows.Forms.DialogResult.Ignore;
                }
            }
            else
                SpawnAI = null;

            if (checkBox3.Checked)
            {
                try
                {
                    SpawnMerchant = new Form3.ActionSpawnMerchant()
                    {
                        
                    };
                }
                catch (Exception ex)
                {
                    DialogResult = System.Windows.Forms.DialogResult.Ignore;
                }
            }
            else
                SpawnMerchant = null;

            if (checkBox4.Checked)
            {
                try
                {
                    SpawnZone = new Form3.ActionSpawnZone()
                    {
                        InitialControl=Int32.Parse(textBox1.Text),
                    };
                }
                catch (Exception ex)
                {
                    DialogResult = System.Windows.Forms.DialogResult.Ignore;
                }
            }
            else
                SpawnZone = null;

        }

        private void checkBox2_CheckedChanged(object sender, EventArgs e)
        {
            groupBox2.Enabled = checkBox2.Checked;
        }

        private void checkBox3_CheckedChanged(object sender, EventArgs e)
        {
            groupBox3.Enabled = checkBox3.Checked;
        }

        private void checkBox4_CheckedChanged(object sender, EventArgs e)
        {
            groupBox4.Enabled = checkBox4.Checked;
        }


    }
}
