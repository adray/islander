﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Editor
{
    public partial class Form2 : Form
    {
        private ItemType selected;
        private Component currentcomponent;
        private int id = 0;

        class ItemType
        {
            public string name;
            public int id;
            public int x, y;
            public List<Component> Components=new List<Component>();
            public string description;
            public int value;

            public override String ToString()
            {
                return name;
            }

        }

        public Form2()
        {
            InitializeComponent();

           // Array array= Enum.GetValues(typeof(Item));
           // foreach (var v in array)
           //     comboBox2.Items.Add(v);

            comboBox3_Setup();
            comboBox4_Setup();

            Clear();
        }

        private void Clear()
        {


            this.textBox1.Text = "";
            this.textBox2.Text = id.ToString();
            this.textBox3.Text = "";
            this.textBox4.Text = "";
            this.textBox5.Text = "";
            this.textBox6.Text = "";
            this.textBox7.Text = "";

            comboBox2.Items.Clear();
            groupBox2.Enabled = false;
            checkBox_CheckedChanged(radioButton1, new EventArgs());
            selected = new ItemType();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //clear

            Clear();

        }

        private void button2_Click(object sender, EventArgs e)
        {

            //submit
            try
            {


                selected.name = this.textBox1.Text;
                selected.id = Int32.Parse(this.textBox2.Text);
                selected.x = Int32.Parse(this.textBox3.Text);
                selected.y = Int32.Parse(this.textBox4.Text);
                selected.description = this.textBox6.Text;
                selected.value = Int32.Parse(this.textBox7.Text);

                for (int i = 0; i < this.comboBox1.Items.Count; i++)
                    if (((ItemType)this.comboBox1.Items[i]).id == selected.id)
                    {
                        this.comboBox1.Items[i] = selected;
                        return;
                    }

                this.comboBox1.Items.Add(selected);
                id++;

            }
            catch (Exception ex)
            {
                MessageBox.Show("Incorrect data.");
            }

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

            //swap item

            selected = (ItemType) this.comboBox1.SelectedItem;

            comboBox2.Items.Clear();
            foreach (var x in selected.Components)
                comboBox2.Items.Add(x);

            this.textBox1.Text = selected.name.ToString();
            this.textBox2.Text = selected.id.ToString();
            this.textBox3.Text = selected.x.ToString();
            this.textBox4.Text = selected.y.ToString();
            this.textBox6.Text = selected.description;
            this.textBox7.Text = selected.value.ToString();

        }

        private void saveAsToolStripMenuItem_Click(object sender, EventArgs e)
        {

            SaveFileDialog save = new SaveFileDialog();
            save.InitialDirectory = System.IO.Directory.GetCurrentDirectory();
            save.Filter = "Item Definition (*.item)|*.item";

            DialogResult res = save.ShowDialog( );


            if (res == System.Windows.Forms.DialogResult.OK)
            {

                try
                {

                    System.IO.BinaryWriter writer = new System.IO.BinaryWriter(
                        save.OpenFile());

                    writer.Write(DateTime.Now.ToBinary());
                    writer.Write(Environment.UserName);
                    writer.Write(comboBox1.Items.Count);

                    foreach (ItemType t in comboBox1.Items)
                    {

                        writer.Write(t.name);
                        writer.Write(t.id);
                        writer.Write(t.x);
                        writer.Write(t.y);
                        writer.Write(t.description);
                        writer.Write(t.value);
                        writer.Write(t.Components.Count);

                        foreach (var comp in t.Components)
                        {
                            writer.Write((int)comp.ComponentType);
                            writer.Write((int)comp.Type);
                            writer.Write(comp.Data);
                            writer.Write(comp.Passive);
                        }
                    }

                    writer.Close();

                }
                catch (Exception ex)
                {

                    MessageBox.Show("IO error.");

                }


            }


        }

        private void loadToolStripMenuItem_Click(object sender, EventArgs e)
        {

            id = 0;
            selected = null;
            comboBox1.Items.Clear();


            OpenFileDialog open = new OpenFileDialog();
            open.Filter = "Item Definition (*.item)|*.item";
            open.InitialDirectory = System.IO.Directory.GetCurrentDirectory();

            DialogResult res = open.ShowDialog();

            if (res == System.Windows.Forms.DialogResult.OK)
            {

                try
                {

                    System.IO.BinaryReader reader = new System.IO.BinaryReader(
                        open.OpenFile());


                    long date = reader.ReadInt64();
                    string author = reader.ReadString();
                    int count = reader.ReadInt32();

                    for (int i = 0; i < count; i++)
                    {
                        ItemType type = new ItemType();
                        type.name = reader.ReadString();
                        type.id = reader.ReadInt32();
                        type.x = reader.ReadInt32();
                        type.y = reader.ReadInt32();
                        type.description = reader.ReadString();
                        type.value = reader.ReadInt32();
                        comboBox1.Items.Add(type);
                        id = Math.Max(id, type.id);

                        int c = reader.ReadInt32();
                        for (int j = 0; j < c; j++)
                        {
                            var comp = new Component();
                            comp.ComponentType =(ComponentType)reader.ReadInt32();
                            comp.Type = (StatType)reader.ReadInt32();
                            comp.Data = reader.ReadSingle();
                            comp.Passive = reader.ReadBoolean();
                            type.Components.Add(comp);
                            comboBox2.Items.Add(comp);
                        }
                    }
                    id++;
                    reader.Close();

                }
                catch (Exception ex)
                {

                    MessageBox.Show("IO error.");

                }


            }

            Clear();

        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {

            MessageBox.Show("Moba Editor v1.0.0", "About");

        }

        #region Components

        enum ComponentType
        {
            Regen=0,
            Bonus,
            Cooldown,       //time before effect can be used again after it has been reset
            Invisability,
            EffectDuration, //length of time effect lasts
            Consumable,
            Planting,
            Egg,
            Ward,
        }

        enum StatType
        {
            Health=0,
            Energy,
            Strength,
            Defence,
            MagicPower,
            MagicResist,
            Movement,
            Default,
            DestroysOnEmpty,
        }

        class Component
        {
            public ComponentType ComponentType { get; set; }
            public StatType Type { get; set; }
            public float Data { get; set; }
            public bool Passive { get; set; }

            public override string ToString()
            {
                return ComponentType.ToString() + " " + Type.ToString();
            }
        }

        //load component
        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            currentcomponent = (Component) comboBox2.SelectedItem;
            ResetRec(groupBox2);
            groupBox2.Enabled = true;

            if (currentcomponent.ComponentType == ComponentType.Regen)
            {
                radioButton1.Checked = true;
                groupBox3.Enabled = true;
                groupBox4.Enabled = false;
                foreach (var x in comboBox3.Items)
                    if (((StatType)x) == currentcomponent.Type)
                        comboBox3.SelectedItem = x;
                textBox5.Text = currentcomponent.Data.ToString();
                checkBox1.Checked = currentcomponent.Passive;
            }
            else if (currentcomponent.ComponentType == ComponentType.Bonus)
            {
                radioButton2.Checked = true;
                foreach (var x in comboBox4.Items)
                    if (((StatType)x) == currentcomponent.Type)
                        comboBox4.SelectedItem = x;
                textBox8.Text = currentcomponent.Data.ToString();
                checkBox1.Checked = currentcomponent.Passive;
            }
            else if (currentcomponent.ComponentType == ComponentType.Cooldown)
            {
                radioButton3.Checked = true;
                textBox9.Text = currentcomponent.Data.ToString();
                checkBox1.Checked = currentcomponent.Passive;
            }
            else if (currentcomponent.ComponentType == ComponentType.Invisability)
            {
                radioButton4.Checked = true;
                //textBox10.Text = currentcomponent.Data.ToString();
                checkBox1.Checked = currentcomponent.Passive;
            }
            else if (currentcomponent.ComponentType == ComponentType.EffectDuration)
            {
                radioButton5.Checked = true;
                textBox11.Text = currentcomponent.Data.ToString();
                checkBox1.Checked = currentcomponent.Passive;
            }
            else if (currentcomponent.ComponentType == ComponentType.Consumable)
            {
                radioButton6.Checked = true;
                textBox12.Text = currentcomponent.Data.ToString();
                checkBox1.Checked = currentcomponent.Passive;
                checkBox2.Checked = currentcomponent.Type == StatType.DestroysOnEmpty;
            }
            else if (currentcomponent.ComponentType == ComponentType.Planting)
            {
                radioButton7.Checked = true;
                checkBox1.Checked = currentcomponent.Passive;
            }
            else if (currentcomponent.ComponentType == ComponentType.Egg)
            {
                radioButton8.Checked = true;
                checkBox1.Checked = currentcomponent.Passive;
            }
            else if (currentcomponent.ComponentType == ComponentType.Ward)
            {
                radioButton9.Checked = true;
                checkBox1.Checked = currentcomponent.Passive;
            }

        }

        private void ResetRec(Control control)
        {
            foreach (Control c in control.Controls)
            {

                if (c is TextBox)
                {
                    ((TextBox)c).Text = "";
                }

                if (c is CheckBox)
                    ((CheckBox)c).Checked = false;

                if (c is ComboBox && ((ComboBox)c).Items.Count>0)
                    ((ComboBox)c).SelectedIndex = 0;

                ResetRec(c);
            }
        }

        //new component
        private void button4_Click(object sender, EventArgs e)
        {
            ResetRec(groupBox2);
            groupBox2.Enabled = true;
            checkBox1.Checked = true;
            radioButton1.Checked = true;
            currentcomponent = new Component();
        }

        //save component
        private void button3_Click(object sender, EventArgs e)
        {
            try
            {
                if (radioButton1.Checked)
                {
                    currentcomponent.ComponentType = ComponentType.Regen;
                    currentcomponent.Type = (StatType)comboBox3.SelectedItem;
                    currentcomponent.Data = float.Parse(textBox5.Text);
                }
                else if (radioButton2.Checked)
                {
                    currentcomponent.ComponentType = ComponentType.Bonus;
                    currentcomponent.Type = (StatType)comboBox4.SelectedItem;
                    currentcomponent.Data = float.Parse(textBox8.Text);
                }
                else if (radioButton3.Checked)
                {
                    currentcomponent.ComponentType = ComponentType.Cooldown;
                    currentcomponent.Type = StatType.Default;
                    currentcomponent.Data = float.Parse(textBox9.Text);
                }
                else if (radioButton4.Checked)
                {
                    currentcomponent.ComponentType = ComponentType.Invisability;
                    currentcomponent.Type = StatType.Default;
                    //currentcomponent.Data = float.Parse(textBox10.Text);
                }
                else if (radioButton5.Checked)
                {
                    currentcomponent.ComponentType = ComponentType.EffectDuration;
                    currentcomponent.Type = StatType.Default;
                    currentcomponent.Data = float.Parse(textBox11.Text);
                }
                else if (radioButton6.Checked)
                {
                    currentcomponent.ComponentType = ComponentType.Consumable;
                    currentcomponent.Type = checkBox2.Checked ? StatType.DestroysOnEmpty : StatType.Default;
                    currentcomponent.Data = float.Parse(textBox12.Text);
                }
                else if (radioButton7.Checked)
                {
                    currentcomponent.ComponentType = ComponentType.Planting;
                    currentcomponent.Type = StatType.Default;
                }
                else if (radioButton8.Checked)
                {
                    currentcomponent.ComponentType = ComponentType.Egg;
                    currentcomponent.Type = StatType.Default;
                }
                else if (radioButton9.Checked)
                {
                    currentcomponent.ComponentType = ComponentType.Ward;
                    currentcomponent.Type = StatType.Default;
                }

                currentcomponent.Passive = checkBox1.Checked;

            }
            catch (Exception ex)
            {
                MessageBox.Show("Incorrect Data");
                return;
            }

            if (!selected.Components.Contains(currentcomponent))
            {
                selected.Components.Add(currentcomponent);
                comboBox2.Items.Add(currentcomponent);
            }

            Object[] obj=new object[comboBox2.Items.Count];
            int i=0;
            for (; i < obj.Length; i++)
                obj[i] = comboBox2.Items[i];

            comboBox2.Items.Clear();
            comboBox2.Items.AddRange(obj);
            
        }

        private void checkBox_CheckedChanged(object sender, EventArgs e)
        {

            var box = (RadioButton)sender;

            if (!box.Checked)
                return;

            GroupBox[] groups = new GroupBox[] { groupBox3, groupBox4, groupBox5, groupBox6, groupBox7, groupBox8, null, null, null };
            RadioButton[] buttons = new RadioButton[] { radioButton1, radioButton2, radioButton3, radioButton4, radioButton5, radioButton6,
                radioButton7, radioButton8, radioButton9 };

            for (int i = 0; i < groups.Length; i++)
            {

                if (groups[i] != null)
                    groups[i].Enabled = groups[i].Parent == box.Parent;
                buttons[i].Checked = buttons[i] == box;

            }

        }

        //delete component
        private void button5_Click(object sender, EventArgs e)
        {
            selected.Components.Remove(currentcomponent);
            comboBox2.Items.Remove(currentcomponent);
        }
        
        private void comboBox3_Setup()
        {
            comboBox3.Items.Add(StatType.Health);
            comboBox3.Items.Add(StatType.Energy);
        }

        private void comboBox4_Setup()
        {
            comboBox4.Items.Add(StatType.Strength);
            comboBox4.Items.Add(StatType.Defence);
            comboBox4.Items.Add(StatType.MagicPower);
            comboBox4.Items.Add(StatType.MagicResist);
        }

        #endregion


        
    }
}
