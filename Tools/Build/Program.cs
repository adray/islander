﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading;

namespace Build
{
    class Program
    {
        class Settings
        {
            public string OutPath;
            public string Devkit;
            public string GamePath;
            public string Version;
        }

        private static string Combine(string arg0, string arg1)
        {
            string root = Directory.GetDirectoryRoot(arg1);
            if (root != string.Empty)
            {
                return arg1;
            }

            Stack<string> path0 = new Stack<string>();
            string[] args = arg0.Split('\\');

            foreach (var arg in args)
                path0.Push(arg);

            string p = "";
            string[] args2 = arg1.Split('\\');
            foreach (var v in args2)
                if (v == "..")
                    path0.Pop();
                else
                    p += v + "\\";

            string q = "";
            while (path0.Count > 0)
                q = path0.Pop() + "\\" + q;

            return q + p;

        }

        private class Logger
        {
            private string path;

            public Logger(string path)
            {
                this.path = path;
            }

            public void Log(string message)
            {
                try
                {
                    using (var log = File.AppendText(path))
                    {
                        log.WriteLine(message);
                        log.Flush();
                    }
                }
                catch (IOException)
                {
                }
            }
        }

        static void Main(string[] args)
        {
            Settings settings = new Settings();
            Logger logger = new Logger("log.txt");

            foreach (var argument in args)
            {
                string[] split = argument.Split(new char[] { ':' }, 2, StringSplitOptions.RemoveEmptyEntries);

                if (split.Length == 0)
                    continue;

                switch (split[0])
                {
                    case "/v":
                        {
                            if (split.Length >= 2)
                                settings.Version = split[1];
                        }
                        break;
                    case "/o":
                        {
                            if (split.Length >= 2)
                                settings.OutPath = Combine(System.Environment.CurrentDirectory, split[1]);
                        }
                        break;
                    case "/i":
                        {
                            if (split.Length >= 2)
                                settings.GamePath = Combine(System.Environment.CurrentDirectory, split[1]);
                        }
                        break;
                    case "/dev":
                        {
                            if (split.Length >= 2)
                                settings.Devkit = Combine(System.Environment.CurrentDirectory, split[1]);
                        }
                        break;
                }
            }

            string command = string.Format("Dev: {0}\nGame: {1}\nOutPath: {2}\nVersion: {3}",
                 settings.Devkit, settings.GamePath, settings.OutPath, settings.Version);

            logger.Log("Command: " + command);
            Console.WriteLine(command);

            try
            {
                Directory.Delete(settings.OutPath + settings.Version, true);
            }
            catch (Exception)
            {
            }

            Thread.Sleep(100);
            DirectoryInfo info = Directory.CreateDirectory(settings.OutPath + settings.Version);

            DirectoryInfo bin       = !string.IsNullOrEmpty(settings.GamePath) ? info.CreateSubdirectory("bin") : null;
            DirectoryInfo resource  = !string.IsNullOrEmpty(settings.GamePath) ? info.CreateSubdirectory("Resources") : null;
            DirectoryInfo dev       = !string.IsNullOrEmpty(settings.Devkit) ? info.CreateSubdirectory("dev") : null;

            if (bin != null)
            {
                List<string> gameDll = new List<string>();
                gameDll.AddRange(Directory.GetFiles(settings.GamePath + @"\Release\", "*.dll"));
                gameDll.AddRange(Directory.GetFiles(settings.GamePath + @"\Release\", "*.exe"));

                CopyFilesTo(logger, bin, gameDll);
            }

            if (dev != null)
            {
                List<string> devFiles = new List<string>();
                devFiles.AddRange(Directory.GetFiles(settings.Devkit));

                CopyFilesTo(logger, dev, devFiles);
            }

            if (resource != null)
            {
                CopyRecursive(logger, settings.GamePath + @"\Resources\", resource);
            }
        }

        private static void CopyRecursive(Logger logger, string inDir, DirectoryInfo outDir)
        {
            List<string> resources = new List<string>();
            DirectoryInfo gameResources = new DirectoryInfo(inDir);
            resources.AddRange(Directory.GetFiles(gameResources.FullName));
            CopyFilesTo(logger, outDir, resources);
            DirectoryInfo[] furtherResources = gameResources.GetDirectories();
            foreach (var dir in furtherResources)
            {
                CopyRecursive(logger, dir.FullName, outDir.CreateSubdirectory(dir.Name));
            }
        }

        private static void CopyFilesTo(Logger logger, DirectoryInfo output, List<string> files)
        {
            foreach (var file in files)
            {
                string dest = output + @"\" + Path.GetFileName(file);
                File.Copy(file, dest);
                string copy = string.Format("Copying {0} to {1}", file, dest);
                logger.Log(copy);
                Console.WriteLine(copy);
            }
        }
    }
}
