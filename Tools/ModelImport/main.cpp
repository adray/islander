// Replicate ISLANDER_MODEL_FORMAT_SUBMESH_BONE_COUNT_Vx as limit
#define AI_SBBC_DEFAULT_MAX_BONES 40

#include "main.h"
#include "assimp/Importer.hpp"
#include "assimp/scene.h"
#include "assimp/postprocess.h"
#include "assimp/matrix4x4.h"
#include "ModelFormat.h"

#include <iostream>
#include <fstream>
#include <vector>
#include <unordered_map>
#include <algorithm>
#include <unordered_set>
#include <assert.h>

typedef uint32_t ui32;

struct subMesh
{
    int indexCount;
    int startIndex;
    int jointCount;
    int bones[ISLANDER_MODEL_FORMAT_SUBMESH_BONE_COUNT_V2];
};

struct vertexSet
{
    uint32_t id;
    int bones[ISLANDER_MODEL_FORMAT_SUBMESH_BONE_COUNT_V2];
    int boneCount;
    std::vector<int32_t> indicies;
};

struct weights
{
    std::vector<float> _weights;
    std::vector<int32_t> joints;
};

struct boneInfo
{
    int index;
    aiNode* node;
    aiBone* bone;
};

struct skeleton
{
    std::unordered_map<aiNode*, boneInfo*> boneMapping;
    boneInfo* infoArray;
};

struct context
{
    const aiScene* scene;
    std::ofstream* writer;
    std::vector<skeleton> skeletons;
    aiMatrix4x4 firstTransform;
    bool hasFirstTransform;
    aiString nodeName;
};

template<typename T>
void writeInt32(context* context, T attribute)
{
    int32_t value = (int32_t) attribute;
    context->writer->write((char*)&value, sizeof(int32_t));
}

template<typename T>
void writeFloat(context* context, T attribute)
{
    float value = (float)attribute;
    context->writer->write((char*)&value, sizeof(float));
}

void processSubMeshes(aiMesh* mesh, std::vector<subMesh>& subMeshes, int32_t* indexData, weights* vertexWeights)
{
    // Process bones

    for (ui32 i = 0; i < mesh->mNumBones; i++)
    {
        auto bone = mesh->mBones[i];
        for (ui32 j = 0; j < bone->mNumWeights; j++)
        {
            auto w = &bone->mWeights[j];
            auto vertex = &vertexWeights[w->mVertexId];
            vertex->joints.push_back(i);
            vertex->_weights.push_back(w->mWeight);
        }
    }

    if (mesh->mNumBones <= ISLANDER_MODEL_FORMAT_SUBMESH_BONE_COUNT_V2)
    {
        // Everything fits into a single submesh

        subMesh submesh;
        submesh.indexCount = (int)mesh->mNumFaces * 3;
        submesh.jointCount = (int)mesh->mNumBones;
        submesh.startIndex = 0;
        for (int k = 0; k < submesh.jointCount; k++)
        {
            submesh.bones[k] = k;
        }

        for (ui32 k = 0; k < mesh->mNumFaces; k++)
        {
            indexData[k*3] = mesh->mFaces[k].mIndices[0];
            indexData[k*3+1] = mesh->mFaces[k].mIndices[1];
            indexData[k*3+2] = mesh->mFaces[k].mIndices[2];
        }

        subMeshes.push_back(submesh); // TODO: incurring cost of copying
    }
    else
    {
        // Assimp failed us there are too many bones
    }
}

aiNode* findBone(const char* name, aiNode* node)
{
    if (!strcmp(name, node->mName.data))
    {
        return node;
    }

    for (ui32 i = 0; i < node->mNumChildren; i++)
    {
        aiNode* found = findBone(name, node->mChildren[i]);
        if (found)
        {
            return found;
        }
    }

    return nullptr;
}

void transformNode(aiMatrix4x4* result, aiNode* node)
{
    if (node->mParent)
    {
        transformNode(result, node->mParent);
        *result = *result * node->mTransformation;
    }
    else
    {
        *result = node->mTransformation;
    }
}

void processSkeleton(context* context, aiMesh* mesh, skeleton* skeleton)
{
    skeleton->infoArray = new boneInfo[mesh->mNumBones];

    for (ui32 i = 0; i < mesh->mNumBones; i++)
    {
        auto bone = mesh->mBones[i];

        auto node = findBone(bone->mName.data, context->scene->mRootNode);

        skeleton->infoArray[i].index = i;
        skeleton->infoArray[i].node = node;
        skeleton->infoArray[i].bone = bone;

        skeleton->boneMapping.insert(std::pair<aiNode*, boneInfo*>(node, &skeleton->infoArray[i]));
    }
}

void processBones(context * context, aiMesh * mesh, skeleton * skeleton)
{
    for (ui32 i = 0; i < mesh->mNumBones; i++)
    {
        auto& parent = skeleton->boneMapping.find(skeleton->infoArray[i].node->mParent);

        writeInt32(context, i); // index

        if (parent != skeleton->boneMapping.end())
        {
            writeInt32(context, parent->second->index); // parent
        }
        else
        {
            writeInt32(context, -1); // root
        }

        aiMatrix4x4 mat;
        mat = skeleton->infoArray[i].bone->mOffsetMatrix;

        writeFloat(context, mat.a1);
        writeFloat(context, mat.a2);
        writeFloat(context, mat.a3);
        writeFloat(context, mat.a4);

        writeFloat(context, mat.b1);
        writeFloat(context, mat.b2);
        writeFloat(context, mat.b3);
        writeFloat(context, mat.b4);

        writeFloat(context, mat.c1);
        writeFloat(context, mat.c2);
        writeFloat(context, mat.c3);
        writeFloat(context, mat.c4);

        writeFloat(context, mat.d1);
        writeFloat(context, mat.d2);
        writeFloat(context, mat.d3);
        writeFloat(context, mat.d4);
    }
}

void processAnimations(context* context)
{
    if (context->skeletons.size() == 0)
    {
        return;
    }

    skeleton& skeleton = context->skeletons[0];

    for (ui32 i = 0; i < context->scene->mNumAnimations; i++)
    {
        auto anim = context->scene->mAnimations[i];

        Islander::ModelFormatAnimationHeader animationHeader;
        animationHeader.boneCount = anim->mNumChannels;// mesh->mNumBones;
        animationHeader.version = 0;
        animationHeader.nameLength = (int)anim->mName.length;

        context->writer->write((char*)&animationHeader, sizeof(animationHeader));
        context->writer->write(anim->mName.data, anim->mName.length);

        for (ui32 j = 0; j < anim->mNumChannels; j++)
        {
            auto channel = anim->mChannels[j];
            auto bone = findBone(channel->mNodeName.data, context->scene->mRootNode);

            auto boneInfo = skeleton.boneMapping.find(bone);

            if (boneInfo != skeleton.boneMapping.end())
            {
                Islander::ModelFormatAnimationFrameBoneHeader boneHeader;
                boneHeader.bone = boneInfo->second->index;
                boneHeader.frameCount = channel->mNumPositionKeys; // assuming they all same length

                context->writer->write((char*)&boneHeader, sizeof(boneHeader));

                for (ui32 k = 0; k < channel->mNumPositionKeys; k++)
                {
                    Islander::ModelFormatAnimationFrameHeader frameHeader;
                    frameHeader.version = 0;
                    frameHeader.time = (float)channel->mPositionKeys[k].mTime / (float)anim->mTicksPerSecond;
                    frameHeader.x = channel->mPositionKeys[k].mValue.x;
                    frameHeader.y = channel->mPositionKeys[k].mValue.y;
                    frameHeader.z = channel->mPositionKeys[k].mValue.z;
                    frameHeader.rw = channel->mRotationKeys[k].mValue.w;
                    frameHeader.rx = channel->mRotationKeys[k].mValue.x;
                    frameHeader.ry = channel->mRotationKeys[k].mValue.y;
                    frameHeader.rz = channel->mRotationKeys[k].mValue.z;
                    frameHeader.sx = channel->mScalingKeys[k].mValue.x;
                    frameHeader.sy = channel->mScalingKeys[k].mValue.y;
                    frameHeader.sz = channel->mScalingKeys[k].mValue.z;

                    context->writer->write((char*)&frameHeader, sizeof(frameHeader));
                }
            }
            else
            {
                Islander::ModelFormatAnimationFrameBoneHeader boneHeader;
                boneHeader.bone = -1;
                boneHeader.frameCount = channel->mNumPositionKeys; // assuming they all same length

                context->writer->write((char*)&boneHeader, sizeof(boneHeader));

                for (ui32 k = 0; k < channel->mNumPositionKeys; k++)
                {
                    Islander::ModelFormatAnimationFrameHeader frameHeader;
                    frameHeader.version = 0;
                    frameHeader.time = (float)channel->mPositionKeys[k].mTime / (float)anim->mTicksPerSecond;
                    frameHeader.x = channel->mPositionKeys[k].mValue.x;
                    frameHeader.y = channel->mPositionKeys[k].mValue.y;
                    frameHeader.z = channel->mPositionKeys[k].mValue.z;
                    frameHeader.rw = channel->mRotationKeys[k].mValue.w;
                    frameHeader.rx = channel->mRotationKeys[k].mValue.x;
                    frameHeader.ry = channel->mRotationKeys[k].mValue.y;
                    frameHeader.rz = channel->mRotationKeys[k].mValue.z;
                    frameHeader.sx = channel->mScalingKeys[k].mValue.x;
                    frameHeader.sy = channel->mScalingKeys[k].mValue.y;
                    frameHeader.sz = channel->mScalingKeys[k].mValue.z;

                    context->writer->write((char*)&frameHeader, sizeof(frameHeader));
                }
            }
        }
    }
}

void processMesh(context* context, aiMesh* mesh, const aiMatrix4x4& transform)
{
    ui32 numColours = mesh->GetNumColorChannels();
    //mesh->HasVertexColors()

    auto indexData = new int32_t[((int64_t)mesh->mNumFaces) * 3];
    memset(indexData, 0, ((int64_t)mesh->mNumFaces) * 3 * sizeof(int32_t));

    weights* vertexWeights = new weights[((int64_t)mesh->mNumFaces) * 3];
    std::vector<subMesh> subMeshes;
    processSubMeshes(mesh, subMeshes, indexData, vertexWeights);

    skeleton skeleton;
    processSkeleton(context, mesh, &skeleton);

    Islander::ModelFormatMeshHeader meshHeader;
    meshHeader.indexCount = mesh->mNumFaces * 3;
    meshHeader.vertexCount = mesh->mNumVertices;
    meshHeader.version = 2;
    meshHeader.boneCount = mesh->mNumBones;
    meshHeader.vertexFlags = (int)Islander::ModelFormatVertex::Pos | (int)Islander::ModelFormatVertex::Normal | (int)Islander::ModelFormatVertex::Colour;
    meshHeader.submeshCount = (int)subMeshes.size();
    meshHeader.animationCount = (int)context->scene->mNumAnimations;
    
    // Transform - transforms the mesh from mesh space to model space
    // Inverse Transform - transforms from model space to mesh space
    aiMatrix4x4 inverseTransform(transform);
    inverseTransform.Inverse();

    if (!context->hasFirstTransform)
    {
        context->firstTransform = inverseTransform;
        context->hasFirstTransform = true;
    }

    // Baked Transform - Transform mesh to the space of the first mesh.
    aiMatrix4x4 bakedTransform = context->firstTransform * transform;

    meshHeader.transform[0] = inverseTransform.a1;
    meshHeader.transform[1] = inverseTransform.a2;
    meshHeader.transform[2] = inverseTransform.a3;
    meshHeader.transform[3] = inverseTransform.a4;

    meshHeader.transform[4] = inverseTransform.b1;
    meshHeader.transform[5] = inverseTransform.b2;
    meshHeader.transform[6] = inverseTransform.b3;
    meshHeader.transform[7] = inverseTransform.b4;

    meshHeader.transform[8] = inverseTransform.c1;
    meshHeader.transform[9] = inverseTransform.c2;
    meshHeader.transform[10] = inverseTransform.c3;
    meshHeader.transform[11] = inverseTransform.c4;

    meshHeader.transform[12] = inverseTransform.d1;
    meshHeader.transform[13] = inverseTransform.d2;
    meshHeader.transform[14] = inverseTransform.d3;
    meshHeader.transform[15] = inverseTransform.d4;

    if (mesh->HasBones())
    {
        meshHeader.vertexFlags |= (int)Islander::ModelFormatVertex::BoneWeight;
    }

    if (mesh->GetNumUVChannels() > 0 && mesh->HasTextureCoords(0))
    {
        meshHeader.vertexFlags |= (int)Islander::ModelFormatVertex::UV;
    }

    writeInt32(context, Islander::ModelFormatElement::Mesh);
    context->writer->write((char*)&meshHeader, sizeof(meshHeader));

    Islander::ModelFormatMeshHeaderEx headerEx;
    memset((char*)&headerEx, 0, sizeof(Islander::ModelFormatMeshHeaderEx));
    headerEx.materialIndex = mesh->mMaterialIndex;
    context->writer->write((char*)&headerEx, sizeof(headerEx));

    writeFloat(context, bakedTransform.a1);
    writeFloat(context, bakedTransform.a2);
    writeFloat(context, bakedTransform.a3);
    writeFloat(context, bakedTransform.a4);
    writeFloat(context, bakedTransform.b1);
    writeFloat(context, bakedTransform.b2);
    writeFloat(context, bakedTransform.b3);
    writeFloat(context, bakedTransform.b4);
    writeFloat(context, bakedTransform.c1);
    writeFloat(context, bakedTransform.c2);
    writeFloat(context, bakedTransform.c3);
    writeFloat(context, bakedTransform.c4);
    writeFloat(context, bakedTransform.d1);
    writeFloat(context, bakedTransform.d2);
    writeFloat(context, bakedTransform.d3);
    writeFloat(context, bakedTransform.d4);

    for (int i = 0; i < subMeshes.size(); i++)
    {
        auto& subMesh = subMeshes[i];

        Islander::ModelFormatSubMeshHeaderV2 subMeshHeader;
        subMeshHeader.indexCount = subMesh.indexCount;
        subMeshHeader.indexOffset = subMesh.startIndex;
        subMeshHeader.boneCount = subMesh.jointCount;
        subMeshHeader.version = 1;

        for (int j = 0; j < subMesh.jointCount; j++)
        {
            subMeshHeader.bones[j] = subMesh.bones[j];
        }

        context->writer->write((char*)&subMeshHeader, sizeof(subMeshHeader));
    }
    
    writeInt32(context, Islander::ModelFormatMeshAttribute::Vertices);

    for (ui32 i = 0; i < mesh->mNumVertices; i++)
    {
        aiVector3D v;
        v.x = mesh->mVertices[i].x;
        v.y = mesh->mVertices[i].y;
        v.z = mesh->mVertices[i].z;

        if (!mesh->HasBones())
        {
            v = bakedTransform * v;
        }

        writeFloat(context, v.x);
        writeFloat(context, v.y);
        writeFloat(context, v.z);

        if (numColours == 0)
        {
            writeFloat(context, 0);
            writeFloat(context, 0);
            writeFloat(context, 0);
        }
        else
        {
            auto channel = mesh->mColors[0];

            writeFloat(context, channel->r);
            writeFloat(context, channel->g);
            writeFloat(context, channel->b);
        }

        if (mesh->HasNormals())
        {
            aiVector3D n;
            n.x = mesh->mNormals[i].x;
            n.y = mesh->mNormals[i].y;
            n.z = mesh->mNormals[i].z;

            if (!mesh->HasBones())
            {
                n = bakedTransform * n;
            }

            writeFloat(context, n.x);
            writeFloat(context, n.y);
            writeFloat(context, n.z);
        }
        else
        {
            writeFloat(context, 0);
            writeFloat(context, 0);
            writeFloat(context, 0);
        }

        if (mesh->GetNumUVChannels() > 0 && mesh->HasTextureCoords(0))
        {
            writeFloat(context, mesh->mTextureCoords[0][i].x);
            writeFloat(context, mesh->mTextureCoords[0][i].y);
        }

        if (mesh->HasBones())
        {
            const int boneCount = 4;
            auto& vertex = vertexWeights[i];

            if (vertex._weights.size() > boneCount)
            {
                std::cout << "Large number of vertex weights " << vertex._weights.size() << " detected (" << context->nodeName.C_Str() << ")" << std::endl;
            }
            else if (vertex._weights.size() == 0)
            {
                std::cout << "No vertex weights detected (" << context->nodeName.C_Str() << ")" << std::endl;
            }

            // Normalize the bones (n-dimensional vector)
            float len = 0;
            for (int j = 0; j < vertex._weights.size(); j++)
            {
                len += vertex._weights[j];
            }

            // Write bone weights
            for (int j = 0; j < boneCount; j++)
            {
                writeFloat(context, j < vertex._weights.size() ? vertex._weights[j] / len : 0);
            }

            // Write bone indices
            for (int j = 0; j < boneCount; j++)
            {
                // TODO: This needs to be the index to the bone in the submesh array, not the bone index
                writeFloat(context, j < vertex.joints.size() ? vertex.joints[j] : 0);
            }
        }
    }

    writeInt32(context, Islander::ModelFormatMeshAttribute::Indicies);
    
    for (ui32 i = 0; i < mesh->mNumFaces; i++)
    {
        writeInt32(context, mesh->mFaces[i].mIndices[0]);
        writeInt32(context, mesh->mFaces[i].mIndices[1]);
        writeInt32(context, mesh->mFaces[i].mIndices[2]);
    }

    writeInt32(context, Islander::ModelFormatMeshAttribute::Bones);

    processBones(context, mesh, &skeleton);

    context->skeletons.push_back(skeleton);

    delete[] indexData;
    delete[] vertexWeights;
}

void processNodes(context* context, const aiNode* node, aiMatrix4x4& parent)
{
    context->nodeName = node->mName;

    for (ui32 i = 0; i < node->mNumMeshes; i++)
    {
        processMesh(context, context->scene->mMeshes[node->mMeshes[i]], parent);
    }

    for (ui32 i = 0; i < node->mNumChildren; i++)
    {
        processNodes(context, node->mChildren[i], parent * node->mChildren[i]->mTransformation);
    }
}

void writeMaterials(const aiScene* scene, context* cxt)
{
    writeInt32(cxt, Islander::ModelFormatElement::Material);

    Islander::ModelFormatMaterialHeader materialHeader;
    std::memset((char*)&materialHeader, 0, sizeof(Islander::ModelFormatMaterialHeader));
    materialHeader.version = 0;
    materialHeader.materialCount = scene->mNumMaterials;

    cxt->writer->write((char*)&materialHeader, sizeof(materialHeader));

    for (ui32 i = 0; i < scene->mNumMaterials; i++)
    {
        Islander::ModelFormatMaterial materialData;
        std::memset((char*)&materialData, 0, sizeof(Islander::ModelFormatMaterial));

        auto material = scene->mMaterials[i];
        materialData.diffuseCount = (int32_t)material->GetTextureCount(aiTextureType_DIFFUSE);
        materialData.normalCount = (int32_t)material->GetTextureCount(aiTextureType_NORMALS);
        materialData.version = 0;

        cxt->writer->write((char*)&materialData, sizeof(materialData));

        for (int j = 0; j < materialData.diffuseCount; j++)
        {
            aiString path;
            material->GetTexture(aiTextureType_DIFFUSE, j, &path);
            if (path.length < ISLANDER_MODEL_FORMAT_IMAGE_NAME_LEN)
            {
                Islander::ModelFormatTexture texture;
                texture.version = 0;
                strcpy(texture.name, path.C_Str());
                cxt->writer->write((char*)&texture, sizeof(texture));
            }
        }

        for (int j = 0; j < materialData.normalCount; j++)
        {
            aiString path;
            material->GetTexture(aiTextureType_NORMALS, j, &path);
            if (path.length < ISLANDER_MODEL_FORMAT_IMAGE_NAME_LEN)
            {
                Islander::ModelFormatTexture texture;
                texture.version = 0;
                strcpy(texture.name, path.C_Str());
                cxt->writer->write((char*)&texture, sizeof(texture));
            }
        }
    }
}

//void writeTextures(const aiScene* scene, context* cxt)
//{
//    if (scene->HasMaterials())
//    {
//        writeInt32(cxt, Islander::ModelFormatElement::Texture);
//
//        Islander::ModelFormatTextureHeader textureHeader;
//        textureHeader.version = 0;
//
//        std::vector<aiString> diffuseTextures;
//        std::vector<aiString> normalTextures;
//
//        for (int i = 0; i < scene->mNumMaterials; i++)
//        {
//            auto material = scene->mMaterials[i];
//            ui32 count = material->GetTextureCount(aiTextureType_DIFFUSE);
//            for (int j = 0; j < count; j++)
//            {
//                aiString path;
//                material->GetTexture(aiTextureType_DIFFUSE, j, &path);
//                if (path.length < ISLANDER_MODEL_FORMAT_IMAGE_NAME_LEN)
//                {
//                    diffuseTextures.push_back(path);
//                }
//            }
//
//            count = material->GetTextureCount(aiTextureType_NORMALS);
//            for (int j = 0; j < count; j++)
//            {
//                aiString path;
//                material->GetTexture(aiTextureType_NORMALS, j, &path);
//                if (path.length < ISLANDER_MODEL_FORMAT_IMAGE_NAME_LEN)
//                {
//                    normalTextures.push_back(path);
//                }
//            }
//        }
//
//        textureHeader.diffuseCount = diffuseTextures.size();
//        textureHeader.normalCount = normalTextures.size();
//
//        cxt->writer->write((char*)&textureHeader, sizeof(textureHeader));
//
//        for (int i = 0; i < textureHeader.diffuseCount; i++)
//        {
//            Islander::ModelFormatTexture texture;
//            texture.version = 0;
//            strcpy(texture.name, diffuseTextures[i].C_Str());
//            cxt->writer->write((char*)&texture, sizeof(texture));
//        }
//
//        for (int i = 0; i < textureHeader.normalCount; i++)
//        {
//            Islander::ModelFormatTexture texture;
//            texture.version = 0;
//            strcpy(texture.name, normalTextures[i].C_Str());
//            cxt->writer->write((char*)&texture, sizeof(texture));
//        }
//    }
//}

int exportMesh(const char* outFile, const aiScene* scene)
{
    std::ofstream stream(outFile, std::ios::out | std::ios::binary);

    context cxt;
    cxt.writer = &stream;
    cxt.scene = scene;
    cxt.hasFirstTransform = false;

    Islander::ModelFormatHeader header;
    header.animationCount = (int)cxt.scene->mNumAnimations;
    header.version = 1;
    header.meshCount = scene->mNumMeshes;

    stream.write((char*)&header, sizeof(header));

    processNodes(&cxt, scene->mRootNode, scene->mRootNode->mTransformation);

    writeInt32(&cxt, Islander::ModelFormatElement::Animation);

    processAnimations(&cxt);

    //writeTextures(scene, &cxt);
    writeMaterials(scene, &cxt);

    return 0;
}

int main(int count, char** str)
{
    int result = 0;

    if (count > 2)
    {
        char* inputFile = str[1];
        char* outFile = str[2];

        Assimp::Importer importer;
        const aiScene* scene = importer.ReadFile(inputFile,
            aiProcess_MakeLeftHanded |
            aiProcess_JoinIdenticalVertices |
            aiProcess_SplitByBoneCount |
            aiProcess_SplitLargeMeshes |
            aiProcess_Triangulate |
            aiProcess_GenSmoothNormals |
            aiProcess_FlipWindingOrder);

        if (scene)
        {
            exportMesh(outFile, scene);
        }
        else
        {
            std::cout << "Unable to import specified file: " << importer.GetErrorString() << std::endl;
            result = 2;
        }
    }

    return result;
}

