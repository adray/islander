﻿using System;
using System.IO;

namespace FontCreator
{
    class Program
    {
        private class FailException : Exception
        {
            public FailException(string msg)
                : base(msg)
            {
            }
        }

        private class RenderPosition
        {
            public int X;
            public int Y;
            public int MaxY;
            public System.Drawing.Font Font;
            public System.Drawing.Graphics Graphics;
        }

        static void Main(string[] args)
        {
            string fontFamily = "Arial";
            int size = 32;
            System.Drawing.FontStyle style = System.Drawing.FontStyle.Regular;

            string combine = string.Join(" ", args);

            for (int i = 0; i < args.Length; i++)
            {
                var input = args[i].Split('=');
                if (input.Length == 2)
                {
                    switch (input[0].ToLower())
                    {
                        case "font":
                            fontFamily = input[1];
                            break;
                        case "size":
                            size = Int32.Parse(input[1]);
                            break;
                        case "options":
                            var items = input[1].Split('|');
                            for (int j = 0; j < items.Length; j++)
                            {
                                switch (items[j].ToLower())
                                {
                                    case "bold":
                                        style |= System.Drawing.FontStyle.Bold;
                                        break;
                                    case "underline":
                                        style |= System.Drawing.FontStyle.Underline;
                                        break;
                                    case "italic":
                                        style |= System.Drawing.FontStyle.Italic;
                                        break;
                                }
                            }
                            break;
                    }
                }
            }

            try
            {

                Console.WriteLine("Attempting to create font.");

                var raw = new System.Drawing.Font(fontFamily, size, style);
                
                BinaryWriter writer = null;

                try
                {

                    writer = new System.IO.BinaryWriter(System.IO.File.Create(string.Format("font-{0}{1}.bin", fontFamily, size)));

                }
                catch (Exception ex)
                {
                    throw new FailException(ex.Message);
                }

                using (System.Drawing.Bitmap img = new System.Drawing.Bitmap(1024, 1024))
                {
                    RenderPosition pos = new RenderPosition();
                    pos.Font = raw;
                    pos.Graphics = System.Drawing.Graphics.FromImage(img);

                    for (int k = 'A'; k <= 'Z'; k++)
                    {
                        Draw(writer, pos, k);
                    }

                    for (int k = 'a'; k <= 'z'; k++)
                    {
                        Draw(writer, pos, k);
                    }

                    for (int k = '0'; k <= '9'; k++)
                    {
                        Draw(writer, pos, k);
                    }

                    for (int k = '!'; k <= '/'; k++)
                    {
                        Draw(writer, pos, k);
                    }

                    for (int k = ':'; k <= '@'; k++)
                    {
                        Draw(writer, pos, k);
                    }

                    for (int k = '['; k <= '`'; k++)
                    {
                        Draw(writer, pos, k);
                    }

                    for (int k = '{'; k <= '~'; k++)
                    {
                        Draw(writer, pos, k);
                    }

                    writer.Flush();
                    writer.Close();

                    var bits = img.LockBits(new System.Drawing.Rectangle(0, 0, img.Size.Width, img.Size.Height),
                    System.Drawing.Imaging.ImageLockMode.ReadWrite,
                    System.Drawing.Imaging.PixelFormat.Format32bppArgb);

                    unsafe
                    {
                        byte* start = (byte*)bits.Scan0;
                        byte* end = start + bits.Stride * bits.Height;

                        for (byte* i = start; i < end; i += 4)
                        {
                            byte* blue = i;
                            byte* green = i + 1;
                            byte* red = i + 2;
                            byte* alpha = i + 3;

                            *red = (byte)(*red * *alpha / 255.0f);
                            *green = (byte)(*green * *alpha / 255.0f);
                            *blue = (byte)(*blue * *alpha / 255.0f);
                        }
                    }

                    img.UnlockBits(bits);

                    img.Save(string.Format("font-{0}{1}.png", fontFamily, size), System.Drawing.Imaging.ImageFormat.Png);

                    Console.WriteLine("Font created correctly.");
                }
            }
            catch (FailException ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {

            }
        }

        private static int MeasureBits(System.Drawing.Imaging.BitmapData bits)
        {
            unsafe
            {
                byte* scan = null;

                for (int i = 1; i < bits.Width; i++)
                {
                    byte* begin = (byte*)bits.Scan0;
                    scan = begin + (bits.Width - i) * bits.Stride / bits.Width;
                    for (int j = 0; j < bits.Height; j++)
                    {
                        int b = *scan++;
                        int r = *scan++;
                        int g = *scan++;
                        int a = *scan++;

                        if (a == 255 && (r > 0 || g > 0 || b > 0))
                        {
                            return bits.Width - i + 1;
                        }

                        scan += bits.Stride - bits.Stride / bits.Width;
                    }
                }
            }

            return 0;
        }

        private static System.Drawing.Size MeasurePrecise(RenderPosition pos, int k)
        {
            System.Drawing.Size size = new System.Drawing.Size();
            size.Height = pos.Font.Height;

            using (System.Drawing.Bitmap bitmap = new System.Drawing.Bitmap(256, 256))
            {
                var gr = System.Drawing.Graphics.FromImage(bitmap);

                gr.TextRenderingHint = System.Drawing.Text.TextRenderingHint.AntiAlias;
                gr.DrawString(((char)k).ToString(), pos.Font, System.Drawing.Brushes.White, 0, 0, System.Drawing.StringFormat.GenericTypographic);

                //bitmap.Save("test.png", System.Drawing.Imaging.ImageFormat.Png);

                var bits = bitmap.LockBits(new System.Drawing.Rectangle(0, 0, bitmap.Size.Width, bitmap.Size.Height),
                    System.Drawing.Imaging.ImageLockMode.ReadOnly,
                    System.Drawing.Imaging.PixelFormat.Format32bppArgb);

                size.Width = MeasureBits(bits);

                bitmap.UnlockBits(bits);

                gr.Clear(System.Drawing.Color.Transparent);

                gr.ScaleTransform(-1, 1);

                gr.TextRenderingHint = System.Drawing.Text.TextRenderingHint.AntiAlias;
                gr.DrawString(((char)k).ToString(), pos.Font, System.Drawing.Brushes.White, -size.Width, 0, System.Drawing.StringFormat.GenericTypographic);

                //bitmap.Save("test.png", System.Drawing.Imaging.ImageFormat.Png);

                bits = bitmap.LockBits(new System.Drawing.Rectangle(0, 0, bitmap.Size.Width, bitmap.Size.Height),
                    System.Drawing.Imaging.ImageLockMode.ReadOnly,
                    System.Drawing.Imaging.PixelFormat.Format32bppArgb);

                var offset = MeasureBits(bits);
                if (offset > size.Width)
                {
                    pos.X += offset;
                }
                else if (size.Width > offset)
                {
                    pos.X += size.Width - offset;
                }

                bitmap.UnlockBits(bits);
            }

            return size;
        }

        private static void Draw(BinaryWriter writer, RenderPosition pos, int k)
        {
            var textSize = MeasurePrecise(pos, k);

            if (pos.X + textSize.Width >= 1000)
            {
                pos.X = 0;
                pos.Y += pos.MaxY + 5;
                pos.MaxY = 0;
            }
            
            pos.Graphics.TextRenderingHint = System.Drawing.Text.TextRenderingHint.AntiAlias;
            pos.Graphics.DrawString(((char)k).ToString(), pos.Font, System.Drawing.Brushes.White, pos.X, pos.Y, System.Drawing.StringFormat.GenericTypographic);

            writer.Write(k);
            writer.Write(pos.X);
            writer.Write(pos.Y);
            writer.Write(textSize.Width);
            writer.Write(textSize.Height);

            pos.MaxY = Math.Max(pos.MaxY, textSize.Height);
            pos.X += textSize.Width;
        }
    }
}
