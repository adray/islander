#include "main.h"
#include "Polygon.h"
#include "AnimationController.h"

#include <istream>
#include <iostream>

Islander::Model::PolygonModel* loadModelData(const char* filePath)
{
    std::ifstream stream;
    stream.open(filePath, std::ios::binary);

    auto lib = Islander::Model::CreatePolyMeshLibrary();
    return Islander::Model::LoadEngineMesh(lib, stream, false);
}

void printMatrix(const Islander::Numerics::Matrix4x4& m)
{
    auto data = m.GetData();
    std::cout << data[0] << "," << data[1] << "," << data[2] << "," << data[3] << std::endl;
    std::cout << data[4] << "," << data[5] << "," << data[6] << "," << data[7] << std::endl;
    std::cout << data[8] << "," << data[9] << "," << data[10] << "," << data[11] << std::endl;
    std::cout << data[12] << "," << data[13] << "," << data[14] << "," << data[15] << std::endl;
}

void printModelInfo(Islander::Model::PolygonModel* model, bool full)
{
    std::cout << "NumMeshes: " << model->numMeshes << std::endl;
    std::cout << "NumMaterials: " << model->numMaterials << std::endl;
    std::cout << "NumTextureChannels: " << model->numTextureChannels << std::endl;
    std::cout << "NumAnimations: " << model->animations->animationCount << std::endl;
    std::cout << "Bounds: Min[" << model->min[0] << "," << model->min[1] << "," << model->min[2] << "]" <<
        "Max[" << model->max[0] << ", " << model->max[1] << ", " << model->max[2] << "]" << std::endl;

    std::cout << "Materials:" << std::endl;
    for (int i = 0; i < model->numMaterials; i++)
    {
        std::cout << "Material[" << i << "]" << std::endl;
        std::cout << "NumDiffuseTextures: " << model->materials[i].numDiffuseTextures << std::endl;
        std::cout << "NumNormalTextures: " << model->materials[i].numNormalTextures << std::endl;
    }

    std::cout << "Meshes:" << std::endl;
    for (int i = 0; i < model->numMeshes; i++)
    {
        std::cout << "Mesh[" << i << "]" << std::endl;
        auto& mesh = model->meshes[i];
        std::cout << "NumVerts: " << mesh.numVerts << std::endl;
        std::cout << "NumIndices: " << mesh.numIndices << std::endl;
        std::cout << "Stride: " << mesh.stride << std::endl;

        std::cout << "NumSubMeshes: " << mesh.subMeshCount << std::endl;
        for (int j = 0; j < mesh.subMeshCount; j++)
        {
            std::cout << "SubMesh[" << j << "]" << std::endl;
            std::cout << "StartIndex: " << mesh.subMeshes[j].startIndex << std::endl;
            std::cout << "IndexCount: " << mesh.subMeshes[j].indexCount << std::endl;
            std::cout << "JointCount: " << mesh.subMeshes[j].jointCount << std::endl;
        }

        if (full && mesh.skeleton)
        {
            std::cout << "Bones:" << std::endl;
            for (int j = 0; j < mesh.skeleton->jointCount; j++)
            {
                auto& joint = mesh.skeleton->joints[j];
                std::cout << "Id" << joint.id << std::endl;
                if (joint.parent)
                {
                    std::cout << "ParentId:" << joint.parent->id << std::endl;
                }
                printMatrix(joint.transform);
            }
        }
    }

    std::cout << "Animations:" << std::endl;
    for (int i = 0; i < model->animations->animationCount; i++)
    {
        auto& anim = model->animations->animations[i];
        std::cout << anim.name.data << std::endl;
        if (full)
        {
            for (int j = 0; j < anim.boneCount; j++)
            {
                std::cout << "Bone: " << j << std::endl;
                for (auto& frame : anim.bones[j].frames)
                {
                    std::cout << "Frame " << frame.time << std::endl;
                    std::cout << "Pos:" << frame.x << "," << frame.y << "," << frame.z << std::endl;
                    std::cout << "Rot (Euler):" << frame.rx << "," << frame.ry << "," << frame.rz << std::endl;
                    std::cout << "Rot (Quat):" << frame.q[0] << "," << frame.q[1] << "," << frame.q[2] << "," << frame.q[3] << std::endl;
                }
            }
        }
    }
}

// From device.cpp
void CalculateBoneMatrix(Islander::Numerics::Matrix4x4* boneMatrix, Islander::Model::PolygonAnimation* animation, Islander::Model::PolygonJoint* parent, Islander::Numerics::Matrix4x4* animationFrames, Islander::Numerics::Matrix4x4& transform, Islander::Numerics::Matrix4x4& inverseMeshTransform)
{
    Islander::Numerics::Matrix4x4 t(Islander::Numerics::Matrix4x4Type::MATRIX_4X4_IDENTITY);

    // Find the animation frame associated with bone
    for (int i = 0; i < animation->boneCount; i++)
    {
        if (animation->bones[i].bone != nullptr && parent->id == animation->bones[i].bone->id)
        {
            t = animationFrames[i]; // "global transform"
            break;
        }
    }

    // For the bind pose this should come to the identity matrix (maybe not in all cases?)

    Islander::Numerics::Matrix4x4::Multiply(t, transform, &t); // (parent->parent->transform * parent->transform) * t
    Islander::Numerics::Matrix4x4::Multiply(parent->transform, t, &boneMatrix[parent->id]);
    Islander::Numerics::Matrix4x4::Multiply(boneMatrix[parent->id], inverseMeshTransform, &boneMatrix[parent->id]);

    for (int i = 0; i < parent->childCount; i++)
    {
        CalculateBoneMatrix(boneMatrix, animation, parent->children[i], animationFrames, t, inverseMeshTransform);
    }
}

void printAnimation(Islander::Model::PolygonModel* model, const char* name, float time)
{
    const int animId = Islander::Model::FindAnimationId(model, name);
    if (animId < 0)
    {
        std::cout << "Unable to find animation '" << name << "'" << std::endl;
        return;
    }

    auto animSystem = Islander::Model::CreateAnimationSystem();
    const int id = Islander::Model::CreateAnimationController(animSystem);
    Islander::Model::PlayAnimation(animSystem, id, animId, false);

    static Islander::Numerics::Matrix4x4 transforms[Islander::Model::MAX_BONES];
    Islander::Model::CalculateBoneTransforms(model->animations, transforms, time, 0, animSystem, id);

    std::cout << "Animation Matrix:" << std::endl;
    for (int i = 0; i < model->animations->animations[animId].boneCount; i++)
    {
        printMatrix(transforms[i]);
    }

    std::cout << "Bone Matrix:" << std::endl;
    for (int j = 0; j < model->numMeshes; j++)
    {
        std::cout << "Mesh " << j << std::endl;
        static Islander::Numerics::Matrix4x4 boneMatrix[Islander::Model::MAX_BONES];
        CalculateBoneMatrix(boneMatrix, &model->animations->animations[animId], model->meshes[j].skeleton->root, transforms, transforms[0], model->meshes[j].transform);

        for (int i = 0; i < model->animations->animations[animId].boneCount; i++)
        {
            printMatrix(boneMatrix[i]);
        }
    }
}

void printBoneWeights(Islander::Model::PolygonModel* model)
{
    for (int j = 0; j < model->numMeshes; j++)
    {
        std::cout << "Mesh " << j << std::endl;
        auto& mesh = model->meshes[j];
        const int off = 9 + (model->numTextureChannels > 0 ? 2 : 0);
        for (int i = 0; i < mesh.numVerts; i++)
        {
            float* vertexData = &mesh.vertexData[i * mesh.stride / sizeof(float)];
            std::cout << "Weights " << vertexData[off] << "," << vertexData[off+1] << "," << vertexData[off+2] << "," << vertexData[off+3] <<
                " Indicies: " << vertexData[off+4] << "," << vertexData[off+5] << "," << vertexData[off+6] << "," << vertexData[off+7] << std::endl;
        }
    }
}

int main(int numArgs, char** args)
{
    if (numArgs >= 2)
    {
        const char* filePath = args[1];
        auto model = loadModelData(filePath);
        bool full = false;
        if (model)
        {
            for (int i = 2; i < numArgs; i++)
            {
                if (std::strcmp(args[i], "-a") == 0)
                {
                    if (numArgs >= i + 2)
                    {
                        const char* name = args[i + 1];
                        const float time = float(std::atof(args[i + 2]));
                        printAnimation(model, name, time);
                        return 0;
                    }
                }
                else if (std::strcmp(args[i], "-w") == 0)
                {
                    printBoneWeights(model);
                    return 0;
                }
                else if (std::strcmp(args[i], "-full") == 0)
                {
                    full = true;
                }
            }

            printModelInfo(model, full);
        }
    }

    return 0;
}
