﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Collections.ObjectModel;
using System.Drawing;
using System.Windows.Forms;
using System.Windows.Forms.Integration;
using System.Windows.Input;
using Islander.Managed;
using System.Windows.Threading;
using System.Threading;
using System.IO;
using Islander.Scripting;

namespace FreeformMapExtensions
{
    public static class Commands
    {
        public const string ClickCommand    = "CLICK_FREEFORM_EDITOR";
        public const string PlaceCommand    = "PLACE_FREEFORM_EDITOR";
        public const string DeleteCommand   = "DELETE_FREEFORM_EDITOR";
        public const string LoadCommand     = "LOAD_FREEFORM_EDITOR";
        public const string SaveCommand     = "SAVE_FREEFORM_EDITOR";
        public const string PlayCommand     = "PLAY_FREEFORM_EDITOR";
        public const string SettingsCommand = "SETTINGS_FREEFORM_EDITOR";
    }

    public class WorldContext : Editor.PluginContext<WorldContext>
    {
        public IWorld World { get; internal set; }
        public string Filename { get; internal set; }
        public List<IEntity> Entities { get; internal set; }
        public IScripting Scripting { get; internal set; }

        public WorldContext()
        {
            Filename = string.Empty;
            Entities = new List<IEntity>();
        }
    }

    public class ScriptingEx : IScripting
    {
        public ScriptingEx()
        {
            var assemblies = AppDomain.CurrentDomain.GetAssemblies();
            foreach (var assembly in assemblies)
            {
                var types = assembly.GetTypes();
                foreach (var type in types)
                {
                    if (type.IsSubclassOf(typeof(IScript)))
                    {
                        this.Types.Add(type);
                    }
                }
            }
        }
    }

    internal struct Material
    {
        public IVertexShader vertex;
        public IPixelShader pixel;
    }
    
    public class Link : ViewModelBase<Link>
    {
        public enum LinkCondition
        {
            Trigger,
            Manual,
            None,
        }

        private LinkCondition condition;

        public IEntity Owner { get; set; }
        public IEntity Child { get; set; }
        public IEntity Linker { get; set; }
        
        public LinkCondition Condition
        {
            get
            {
                return this.condition;
            }
            set
            {
                this.condition = value;
                OnPropertyChanged("Condition");
            }
        }

        public string Name
        {
            get
            {
                return this.Child.GetComponent<Editable>().Name;
            }
        }

        internal void LinkEntities(IWorld world, Material material)
        {
            if (Linker == null)
            {
                Linker = world.CreateEntity();
                ILineSprite line = Linker.AddComponent<ILineSprite>();
                Vec3<float> ownerPos = Owner.GetComponent<ITransform>().Position;
                Vec3<float> childPos = Child.GetComponent<ITransform>().Position;
                line.Start = new Vec2f(ownerPos.X, ownerPos.Y);
                line.End = new Vec2f(childPos.X, childPos.Y);
                line.Material.PixelShader = material.pixel;
                line.Material.VertexShader = material.vertex;
            }
        }

        public Link(IEntity owner, IEntity child)
        {
            Owner = owner;
            Child = child;
        }

        private Link() { }
    }

    public class Linker : IManagedComponent
    {
        private ObservableCollection<Link> links;
        
        public ObservableCollection<Link> Links
        {
            get
            {
                return this.links;
            }
            set
            {
                if (value != null)
                {
                    this.links = value;
                    OnPropertyChanged("Links");
                }
            }
        }

        public Linker() : base("Linker")
        {
            Links = new ObservableCollection<Link>();
            Links.CollectionChanged += (o, e) =>
            {
                OnPropertyChanged("Links");
            };
        }
    }

    public class Editable : IManagedComponent
    {
        private float redTint;
        private string name;
        private bool isPlayer;
        private bool visable;
        private float opacity;

        public float Opacity
        {
            get
            {
                return IsVisable ? opacity : 0.5f;
            }
            set
            {
                this.opacity = value;
                OnPropertyChanged("Opacity");
            }
        }

        public float RedTint
        {
            get
            {
                return this.redTint;
            }
            set
            {
                this.redTint = value;
                OnPropertyChanged("RedTint");
            }
        }

        public string Name
        {
            get
            {
                return this.name;
            }
            set
            {
                this.name = value;
                OnPropertyChanged("Name");
            }
        }

        public bool IsPlayer
        {
            get
            {
                return this.isPlayer;
            }
            set
            {
                this.isPlayer = value;
                OnPropertyChanged("IsPlayer");
            }
        }

        public bool IsVisable
        {
            get
            {
                return this.visable;
            }
            set
            {
                this.visable = value;
                OnPropertyChanged("IsVisable");
                OnPropertyChanged("Opacity");
            }
        }
        
        public Editable()
            : base("Editable")
        {
            RedTint = 0.0f;
            Name = string.Empty;
            IsPlayer = false;
            IsVisable = true;                    
            Opacity = 1.0f;
        }
    }

    public class Command<T> : ICommand
    {
        private Action<T> action;

        public Command(Action<T> action)
        {
            this.action = action;
        }

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public event EventHandler CanExecuteChanged;

        public void Execute(object parameter)
        {
            this.action((T)parameter);
        }
    }

    public class ViewModelBase<T> : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged(string property)
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(property));
            }
        }
    }

    public class ITextureComparer : IEqualityComparer<ITexture2D>
    {
        public bool Equals(ITexture2D x, ITexture2D y)
        {
 	        return x.Equals(y);
        }

        public int GetHashCode(ITexture2D obj)
        {
            return 0;
        }
    }

    public class TriggerBox : IManagedComponent
    {
        private IEntity top;
        private IEntity left;
        private IEntity right;
        private IEntity bottom;
        private Vec2f point;
        private Vec3f colour;
        private float opacity;
        private bool oneTime;

        public TriggerBox() : base("TriggerBox")
        {
            this.point = new Vec2f();
            this.colour = new Vec3f();
            this.colour.PropertyChanged += (sender, e) =>
                {
                    this.OnPropertyChanged("Colour");
                };
        }

        public void InitilizeLines(IWorld world)
        {
            this.top = world.CreateEntity(); this.top.AddComponent<ILineSprite>();
            this.bottom = world.CreateEntity(); this.bottom.AddComponent<ILineSprite>();
            this.left = world.CreateEntity(); this.left.AddComponent<ILineSprite>();
            this.right = world.CreateEntity(); this.right.AddComponent<ILineSprite>();       
        }

        public void Destroy(IWorld world)
        {
            world.RemoveEntity(this.top);
            world.RemoveEntity(this.bottom);
            world.RemoveEntity(this.left);
            world.RemoveEntity(this.right);
        }

        public bool OneTime
        {
            get
            {
                return this.oneTime;
            }
            set
            {
                this.oneTime = value;
                OnPropertyChanged("OneTime");
            }
        }

        public float Opacity
        {
            get
            {
                return this.opacity;
            }
            set
            {
                this.opacity = value;
                OnPropertyChanged("Opacity");
            }
        }

        public Vec3f Colour
        {
            get
            {
                return this.colour;
            }
            set
            {
                this.colour = value;
                OnPropertyChanged("Colour");
            }
        }

        public Vec2f Point
        {
            get
            {
                return this.point;
            }
            set
            {
                this.point = value;
            }
        }

        public IEntity TopEntity
        {
            get
            {
                return this.top;
            }
            set
            {
                this.top = value;
            }
        }

        public IEntity BottomEntity
        {
            get
            {
                return this.bottom;
            }
            set
            {
                this.bottom = value;
            }
        }

        public IEntity LeftEntity
        {
            get
            {
                return this.left;
            }
            set
            {
                this.left = value;
            }
        }

        public IEntity RightEntity
        {
            get
            {
                return this.right;
            }
            set
            {
                this.right = value;
            }
        }

        public ILineSprite Top
        {
            get
            {
                return this.top.GetComponent<ILineSprite>();
            }
        }

        public ILineSprite Bottom
        {
            get
            {
                return this.bottom.GetComponent<ILineSprite>();
            }
        }

        public ILineSprite Left
        {
            get
            {
                return this.left.GetComponent<ILineSprite>();
            }
        }

        public ILineSprite Right
        {
            get
            {
                return this.right.GetComponent<ILineSprite>();
            }
        }
    }

    public class ScriptCollection : IManagedComponent
    {
        private ObservableCollection<Islander.Scripting.IScript> scripts;

        public ScriptCollection()
            : base("ScriptCollection")
        {
            scripts = new ObservableCollection<Islander.Scripting.IScript>();
        }

        public ObservableCollection<Islander.Scripting.IScript> Scripts
        {
            get
            {
                return this.scripts;
            }
            set
            {
                this.scripts = value;
            }
        }
    }
    
    public class Settings : IManagedComponent
    {
        public Settings()
            : base("Settings")
        {

        }
    }

    public class MainViewModel : ViewModelBase<MainViewModel>, IDisposable
    {

        private ObservableCollection<System.Windows.Controls.Image> images = new ObservableCollection<System.Windows.Controls.Image>();

        private Dictionary<System.Windows.Controls.Image, ITexture2D> textures = new Dictionary<System.Windows.Controls.Image, ITexture2D>();

        private Dictionary<ITexture2D, Image> imageDictionary = new Dictionary<ITexture2D, Image>(
            new ITextureComparer());
        
        private IRenderer renderer;

        private IWorld world;

        private System.Windows.Controls.Image currentSelection;

        private IEntity currentEntity;

        private List<IEntity> entities = new List<IEntity>();

        private List<IEntity> candidate = new List<IEntity>();
        
        private Material lineMaterial;

        private Material uniformquad;

        private string levelFilename;

        private ITextSprite tsp;

        private bool ctrl;

        private bool isdirty;

        private Action<object, OnKeyEventArgs> onKeyChanged;

        private Action<object, OnKeyEventArgs> onKeyTriggered;

        private IScripting scripting = new ScriptingEx();
        
        public IRenderer Renderer
        {
            get
            {
                return this.renderer;
            }
        }

        public Command<Object> Settings
        {
            get
            {
                return new Command<object>((o) =>
                    {
                        var command = Editor.PluginManager.GetCommand(Commands.SettingsCommand);
                        if (command != null)
                        {
                            command.Action(new WorldContext()
                            {
                                Scripting = this.scripting,
                                Entities = this.entities,
                                World = this.world,
                            });
                        }
                    });
            }
        }

        public MainViewModel()
        {
            this.ctrl = false;
        }

        private string DirtyString
        {
            get
            {
                return "*";
            }
        }

        private bool IsDirty
        {
            get
            {
                return this.isdirty;
            }
            set
            {
                this.isdirty = value;
                if (this.tsp != null)
                {
                    tsp.Text = this.LevelFilename;
                }
            }
        }

        private string LevelFilename
        {
            get
            {
                string filename = Path.GetFileNameWithoutExtension(this.levelFilename);
                if (string.IsNullOrEmpty(filename))
                {
                    filename = "Unsaved Map";
                }
                if (isdirty)
                {                    
                    return filename + DirtyString;
                }
                return filename;
            }
            set
            {
                this.isdirty = false;
                this.levelFilename = value;
            }
        }

        internal void Init(IRenderer renderer)
        {
            this.renderer = renderer;

            renderer.LoadTexture("font");

            #region Shaders
            List<ISemantic> t = new List<ISemantic>();
            t.Add(new ISemantic()
            {
                Name = "POSITION",
                Format = SemanticFormat.SemanticFloat4,
            });

            renderer.LoadVertexShader("Font.fx", "FontVertex", t);
            renderer.LoadPixelShader("Font.fx", "FontPixel");

            List<ISemantic> s = new List<ISemantic>();
            s.Add(new ISemantic()
            {
                Name = "POSITION",
                Format = SemanticFormat.SemanticFloat2,
            });
            s.Add(new ISemantic()
            {
                Name = "TEXCOORD",
                Format = SemanticFormat.SemanticFloat2,
            });
            s.Add(new ISemantic()
            {
                Name = "TEXCOORD",
                Format = SemanticFormat.SemanticFloat4,
                Stream = 1,
            });
            s.Add(new ISemantic()
            {
                Name = "TEXCOORD",
                Format = SemanticFormat.SemanticFloat4,
                Stream = 1,
            });
            s.Add(new ISemantic()
            {
                Name = "TEXCOORD",
                Format = SemanticFormat.SemanticFloat4,
                Stream = 1,
            });
            s.Add(new ISemantic()
            {
                Name = "TEXCOORD",
                Format = SemanticFormat.SemanticFloat4,
                Stream = 1,
            });
            s.Add(new ISemantic()
            {
                Name = "TEXCOORD",
                Format = SemanticFormat.SemanticFloat3,
                Stream = 1,
            });
            s.Add(new ISemantic()
            {
                Name = "TEXCOORD",
                Format = SemanticFormat.SemanticFloat,
                Stream = 1,
            });
            s.Add(new ISemantic()
            {
                Name = "TEXCOORD",
                Format = SemanticFormat.SemanticFloat4,
                Stream = 1,
            });
            IVertexShader vshader = renderer.LoadVertexShader("Quad.fx", "QuadVertex", s);
            IPixelShader pshader = renderer.LoadPixelShader("Quad.fx", "QuadPixel");

            List<ISemantic> s2 = new List<ISemantic>();
            s2.Add(new ISemantic()
            {
                Name = "POSITION",
                Format = SemanticFormat.SemanticFloat,
            });
            s2.Add(new ISemantic()
            {
                Name = "TEXCOORD",
                Format = SemanticFormat.SemanticFloat2,
                Stream = 1,
            });
            s2.Add(new ISemantic()
            {
                Name = "TEXCOORD",
                Format = SemanticFormat.SemanticFloat2,
                Stream = 1,
            });
            s2.Add(new ISemantic()
            {
                Name = "TEXCOORD",
                Format = SemanticFormat.SemanticFloat4,
                Stream = 1,
            });
            IVertexShader lvshader = renderer.LoadVertexShader("Line.fx", "LineVertex", s2);
            IPixelShader lpshader = renderer.LoadPixelShader("Line.fx", "LinePixel");

            lineMaterial = new Material()
            {
                pixel = lpshader,
                vertex = lvshader,
            };
            #region uniformquad
            List<ISemantic> s3 = new List<ISemantic>();
            s3.Add(new ISemantic()
            {
                Name = "POSITION",
                Format = SemanticFormat.SemanticFloat2,
            });
            s3.Add(new ISemantic()
            {
                Name = "TEXCOORD",
                Format = SemanticFormat.SemanticFloat2,
            });
            s3.Add(new ISemantic()
            {
                Name = "TEXCOORD",
                Format = SemanticFormat.SemanticFloat4,
                Stream = 1,
            });
            s3.Add(new ISemantic()
            {
                Name = "TEXCOORD",
                Format = SemanticFormat.SemanticFloat4,
                Stream = 1,
            });
            s3.Add(new ISemantic()
            {
                Name = "TEXCOORD",
                Format = SemanticFormat.SemanticFloat4,
                Stream = 1,
            });
            s3.Add(new ISemantic()
            {
                Name = "TEXCOORD",
                Format = SemanticFormat.SemanticFloat4,
                Stream = 1,
            });
            s3.Add(new ISemantic()
            {
                Name = "TEXCOORD",
                Format = SemanticFormat.SemanticFloat4,
                Stream = 1,
            });
            s3.Add(new ISemantic()
            {
                Name = "TEXCOORD",
                Format = SemanticFormat.SemanticFloat3,
                Stream = 1,
            });
            s3.Add(new ISemantic()
            {
                Name = "TEXCOORD",
                Format = SemanticFormat.SemanticFloat,
                Stream = 1,
            });
            IVertexShader uniformvshader = renderer.LoadVertexShader("UniformQuad.fx", "UniformQuadVertex", s3);
            IPixelShader uniformpshader = renderer.LoadPixelShader("UniformQuad.fx", "UniformQuadPixel");
            this.uniformquad = new Material()
            {
                pixel = uniformpshader, vertex = uniformvshader,
            };
            #endregion
            #endregion

            world = new IWorld();

            IEntity c = world.CreateEntity();
            ICamera camera = c.AddComponent<ICamera>();
            ITransform transform = c.AddComponent<ITransform>();

            IEntity text = world.CreateEntity();
            tsp = text.AddComponent<ITextSprite>();
            IsDirty = false;

            LoadScriptDlls();

            Dispatcher dispatcher = Dispatcher.CurrentDispatcher;

            float mouseX = 0;
            float mouseY = 0;

            renderer.OnPaint += delegate(object sender, EventArgs e)
            {
                renderer.Render(world, camera);
            };
            renderer.OnClick += delegate(object sender, OnClickEventArgs e)
            {
                mouseX = ((e.Position.X / renderer.Width) - 0.5f + camera.Position.X / 2) * (renderer.Width * 2);
                mouseY = -((e.Position.Y / renderer.Height) - 0.5f - camera.Position.Y / 2) * (renderer.Height * 2);
                if (e.Button == Islander.Managed.MouseButton.Left)
                {
                    if (e.State == MouseState.Down)
                    {
                        if (currentEntity == null)
                        {

                            using (ITransform t1 = new ITransform())
                            {
                                t1.Position = new Vec3<float>(mouseX, mouseY, 0);
                                t1.Direction = new Vec3<float>(0, 1, 0);
                                t1.Scale = new Vec3<float>(1, 1, 0);
                                List<IEntity> foundlist = this.entities.FindAll(new Predicate<IEntity>(delegate(IEntity ent)
                                {
                                    if (ent.HasComponent<ITransform>())
                                    {
                                        var t2 = ent.GetComponent<ITransform>();
                                        if (ent != currentEntity
                                            && Detect(t1, t2))
                                        {
                                            return true;
                                        }
                                    }
                                    return false;
                                }));
                                foundlist.Sort(new Comparison<IEntity>((IEntity a, IEntity b) =>
                                {
                                    float az = 0;
                                    if (a != null && a.HasComponent<ITransform>())
                                    {
                                        az = a.GetComponent<ITransform>().Position.Z;
                                    }
                                    float bz = 0;
                                    if (b != null && b.HasComponent<ITransform>())
                                    {
                                        bz = b.GetComponent<ITransform>().Position.Z;
                                    }

                                    return az > bz ? -1 : 1;
                                }));
                                if (foundlist.Count > 0)
                                {
                                    var found = foundlist.First();
                                    ThreadPool.QueueUserWorkItem(new WaitCallback((obj) =>
                                    {
                                        Editor.IPlugin plugin = Editor.PluginManager.GetCommand(Commands.ClickCommand);
                                        if (plugin != null)
                                        {
                                            dispatcher.Invoke(new Action(() =>
                                            {
                                                plugin.Action(
                                                    new Editor.PluginContext<Tuple<IEntity, IScripting>>()
                                                    {
                                                        Value = new Tuple<IEntity,IScripting>(found, this.scripting),
                                                    });
                                            }));
                                        }
                                    }));
                                }
                                else
                                {
                                renderer.Dispatch(new Action(() =>
                                    {
                                        currentEntity = world.CreateEntity();
                                        Linker linker = currentEntity.AddComponent<Linker>();
                                        linker.PropertyChanged += new PropertyChangedEventHandler(LinkerPropertyChanged);
                                        TriggerBox trigger = currentEntity.AddComponent<TriggerBox>();
                                        trigger.InitilizeLines(world);
                                        trigger.Top.Material.PixelShader = lineMaterial.pixel;
                                        trigger.Top.Material.VertexShader = lineMaterial.vertex;
                                        trigger.Bottom.Material.PixelShader = lineMaterial.pixel;
                                        trigger.Bottom.Material.VertexShader = lineMaterial.vertex;
                                        trigger.Right.Material.PixelShader = lineMaterial.pixel;
                                        trigger.Right.Material.VertexShader = lineMaterial.vertex;
                                        trigger.Left.Material.PixelShader = lineMaterial.pixel;
                                        trigger.Left.Material.VertexShader = lineMaterial.vertex;
                                        trigger.Top.Start = new Vec2f(mouseX, mouseY);
                                        trigger.Top.End = new Vec2f(mouseX, mouseY);
                                        trigger.Bottom.Start = new Vec2f(mouseX, mouseY);
                                        trigger.Bottom.End = new Vec2f(mouseX, mouseY);
                                        trigger.Left.Start = new Vec2f(mouseX, mouseY);
                                        trigger.Left.End = new Vec2f(mouseX, mouseY);
                                        trigger.Right.Start = new Vec2f(mouseX, mouseY);
                                        trigger.Right.End = new Vec2f(mouseX, mouseY);
                                        trigger.Point.X = mouseX;
                                        trigger.Point.Y = mouseY;
                                    }));
                                }
                            }
                        }
                        else
                        {
                            //IEntity intersection = null;
                            //if (!Intersecting(out intersection))
                            {
                                IEntity ent = AddItemSprite(new Vec3f(mouseX, mouseY, 0));
                                if (ent != null)
                                {
                                    ent.GetComponent<Editable>().RedTint = 0.0f;
                                    ent.GetComponent<Editable>().IsVisable = true;
                                    entities.Add(ent);
                                    Editor.IPlugin plugin = Editor.PluginManager.GetCommand(Commands.PlaceCommand);
                                    if (plugin != null)
                                    {
                                        dispatcher.Invoke(new Action(() =>
                                        {
                                            plugin.Action(
                                                new Editor.PluginContext<IEntity>()
                                                {
                                                    Value = ent,
                                                });
                                        }));
                                    }
                                    IsDirty = true;
                                }
                            }
                        }
                    }
                    else if (e.State == MouseState.Up)
                    {
                        TriggerBox triggerbox = null;
                        if (currentEntity != null)
                        {
                            var comps = currentEntity.GetComponents();                            
                            foreach (var comp in comps)
                            {
                                if (triggerbox == null)
                                {
                                    triggerbox = comp as TriggerBox;
                                }
                            }
                        }

                        if (triggerbox != null)
                        {
                            renderer.Dispatch(new Action(() =>
                                {
                                    this.entities.Add(currentEntity);
                                    ISprite sprite = currentEntity.AddComponent<ISprite>();
                                    sprite.Material.PixelShader = uniformpshader;
                                    sprite.Material.VertexShader = uniformvshader;
                                    ITransform tt = currentEntity.AddComponent<ITransform>();
                                    tt.Direction = new Vec3<float>(0, 1, 0);
                                    tt.Position = new Vec3<float>((triggerbox.Top.End.X + triggerbox.Top.Start.X) / 2,
                                        (triggerbox.Right.End.Y + triggerbox.Right.Start.Y) / 2, 0);
                                    tt.Scale = new Vec3<float>((Math.Abs(triggerbox.Top.End.X - triggerbox.Top.Start.X)/2),
                                        Math.Abs((triggerbox.Right.End.Y - triggerbox.Right.Start.Y)/2), 1);
                                    triggerbox.Colour.Z = 1.0f;
                                    currentEntity = null;
                                }));
                        }
                    }
                }
                else if (e.Button == Islander.Managed.MouseButton.Right)
                {
                    if (this.currentEntity != null)
                    {
                        // delete current entity
                        Action remove = new Action(() =>
                        {
                            world.RemoveEntity(this.currentEntity);
                            this.currentEntity = null;
                            this.CurrentItem = null;
                        });
                        renderer.Dispatch(remove);
                    }
                }
                renderer.Render(world, camera);
            };
            renderer.OnMouseMove += delegate(object sender, OnMouseMoveEventArgs e)
            {
                mouseX = ((e.Position.X / renderer.Width) - 0.5f) * (renderer.Width * 2) + camera.Position.X * renderer.Width;
                mouseY = -((e.Position.Y / renderer.Height) - 0.5f) * (renderer.Height * 2) + camera.Position.Y * renderer.Height;
                if (!ctrl)
                {
                    if (currentEntity == null)
                    {
                        currentEntity = AddItemSprite(new Vec3f(mouseX, mouseY, 0));

                        using (ITransform t1 = new ITransform())
                        {
                            t1.Position = new Vec3<float>(mouseX, mouseY, 0);
                            t1.Direction = new Vec3<float>(0, 1, 0);
                            t1.Scale = new Vec3<float>(1, 1, 0);
                            this.entities.ForEach(new Action<IEntity>(delegate(IEntity ent)
                            {
                                float opacity = 0;
                                var t2 = ent.HasComponent<ITransform>() ? ent.GetComponent<ITransform>() : null;
                                if (t2 != null
                                    && ent != currentEntity
                                    && Detect(t1, t2))
                                {
                                    opacity = 0.5f;
                                }
                                if (ent.HasComponent<TriggerBox>())
                                {
                                    TriggerBox box = ent.GetComponent<TriggerBox>();
                                    box.Opacity = opacity;
                                }
                            }));
                        }
                    }
                    else
                    {
                        var comps = currentEntity.GetComponents();
                        TriggerBox triggerbox = null;
                        ITransform tr = null;
                        foreach (var comp in comps)
                        {
                            if (triggerbox == null)
                            {
                                triggerbox = comp as TriggerBox;
                            }
                            if (tr == null)
                            {
                                tr = comp as ITransform;
                            }
                        }

                        if (triggerbox != null)
                        {
                            triggerbox.Top.Start = triggerbox.Point;
                            triggerbox.Top.End = new Vec2f(mouseX, triggerbox.Top.Start.Y);
                            triggerbox.Bottom.Start = new Vec2f(triggerbox.Point.X, mouseY);
                            triggerbox.Bottom.End = new Vec2f(mouseX, mouseY);
                            triggerbox.Right.Start = new Vec2f(mouseX, triggerbox.Point.Y);
                            triggerbox.Right.End = new Vec2f(mouseX, mouseY);
                            triggerbox.Left.Start = new Vec2f(triggerbox.Point.X, triggerbox.Point.Y);
                            triggerbox.Left.End = new Vec2f(triggerbox.Point.X, mouseY);
                        }
                        if (tr != null)
                        {
                            tr.Position.X = mouseX;
                            tr.Position.Y = mouseY;
                        }
                    }
                }
                else if (candidate.Count > 0)
                {
                    ITransform tr = candidate.Last().GetComponent<ITransform>();
                    bool right = true;
                    bool left = true;
                    bool up = true;
                    bool down = true;

                    if (candidate.Count >= 2)
                    {
                        var tr2 = candidate[candidate.Count - 2].GetComponent<ITransform>();
                        if (tr2.Position.X > tr.Position.X)
                            right = false;
                        if (tr2.Position.X < tr.Position.X)
                            left = false;
                        if (tr2.Position.Y < tr.Position.Y)
                            up = false;
                        if (tr2.Position.Y > tr.Position.Y)
                            down = false;
                    }

                    Action remove = new Action(() =>
                    {
                        IEntity last = candidate.Last();
                        candidate.Remove(last);
                        world.RemoveEntity(last);
                    });

                    if (mouseX + tr.Scale.X*2 < tr.Position.X)
                    {
                        if (left)
                        {
                            candidate.Add(AddItemSprite(new Vec3f(tr.Position.X - tr.Scale.X * 2, tr.Position.Y, 0)));
                        }
                        else
                        {
                            renderer.Dispatch(remove);
                        }
                    }
                    if (mouseX - tr.Scale.X * 2 > tr.Position.X)
                    {
                        if (right)
                        {
                            candidate.Add(AddItemSprite(new Vec3f(tr.Position.X + tr.Scale.X * 2, tr.Position.Y, 0)));
                        }
                        else
                        {
                            renderer.Dispatch(remove);
                        }
                    }
                    if (mouseY + tr.Scale.Y * 2 < tr.Position.Y)
                    {
                        if (up)
                        {
                            candidate.Add(AddItemSprite(new Vec3f(tr.Position.X, tr.Position.Y - tr.Scale.Y * 2, 0)));
                        }
                        else
                        {
                            renderer.Dispatch(remove);
                        }
                    }
                    if (mouseY - tr.Scale.Y * 2 > tr.Position.Y)
                    {
                        if (down)
                        {
                            candidate.Add(AddItemSprite(new Vec3f(tr.Position.X, tr.Position.Y + tr.Scale.Y * 2, 0)));
                        }
                        else
                        {
                            renderer.Dispatch(remove);
                        }
                    }
                }
                renderer.Render(world, camera);
            };
            //System.Diagnostics.Stopwatch[] keytimer = new System.Diagnostics.Stopwatch[200];
            this.onKeyTriggered = delegate(object sender, OnKeyEventArgs e)
            {
                //var _timer = keytimer[e.Key];
                //_timer.Stop();
                if (e.Key == (int)System.Windows.Forms.Keys.Delete)
                {
                    if (e.State)
                    {
                        using (ITransform t1 = new ITransform())
                        {
                            t1.Position = new Vec3<float>(mouseX, mouseY, 0);
                            t1.Direction = new Vec3<float>(0, 1, 0);
                            t1.Scale = new Vec3<float>(1, 1, 0);
                            List<IEntity> found = this.entities.FindAll(new Predicate<IEntity>(delegate(IEntity ent)
                            {
                                if (ent.HasComponent<ITransform>())
                                {
                                    var t2 = ent.GetComponent<ITransform>();
                                    if (ent != currentEntity
                                        && Detect(t1, t2))
                                    {
                                        return true;
                                    }
                                }
                                return false;
                            }));
                            if (found != null && found.Count > 0)
                            {
                                found.Sort(new Comparison<IEntity>((IEntity a, IEntity b) =>
                                    {
                                        float az = 0;
                                        if (a != null && a.HasComponent<ITransform>())
                                        {
                                            az = a.GetComponent<ITransform>().Position.Z;
                                        }
                                        float bz = 0;
                                        if (b != null && b.HasComponent<ITransform>())
                                        {
                                            bz = b.GetComponent<ITransform>().Position.Z;
                                        }

                                        return az > bz ? -1 : 1;
                                    }));

                                renderer.Dispatch(new Action(() =>
                                {
                                    DeleteEntity(found.First());
                                }));
                            }
                        }                       
                    }
                }
                else if (e.Key == (int)System.Windows.Forms.Keys.W)
                {
                    if (e.State)
                    {
                        camera.Position.Y += 0.5f * 50 / renderer.Height;
                    }
                }
                else if (e.Key == (int)System.Windows.Forms.Keys.S)
                {
                    if (e.State)
                    {
                        camera.Position.Y -= 0.5f * 50 / renderer.Height;
                    }
                }
                else if (e.Key == (int)System.Windows.Forms.Keys.A)
                {
                    if (e.State)
                    {
                        camera.Position.X -= 0.5f * 50 / renderer.Width;
                    }
                }
                else if (e.Key == (int)System.Windows.Forms.Keys.D)
                {
                    if (e.State)
                    {
                        camera.Position.X += 0.5f * 50 / renderer.Width;
                    }
                }
                //_timer.Restart();
                ThreadPool.QueueUserWorkItem(new WaitCallback((obj) =>
                    {
                        renderer.Render(world, camera);
                    }));
            };
            renderer.OnKeyTriggered += new EventHandler<OnKeyEventArgs>(this.onKeyTriggered);
            onKeyChanged = delegate(object sender, OnKeyEventArgs e)
            {
                //if (e.Key < 200)
                //{
                //    if (keytimer[e.Key] == null)
                //    { keytimer[e.Key] = new System.Diagnostics.Stopwatch(); }
                //    else if (!e.State)
                //    { keytimer[e.Key].Reset(); }
                //}
                if (e.Key == (int)System.Windows.Forms.Keys.ControlKey)
                {
                    if (currentEntity != null)
                        currentEntity.GetComponent<Editable>().RedTint = e.State ? 1.0f : 0.0f;
                    this.ctrl = e.State;
                    if (this.ctrl)
                    {
                        IEntity intersection;
                        if (Intersecting(out intersection))
                        {
                            ITransform trans = currentEntity.GetComponent<ITransform>();
                            ITransform inter =  intersection.GetComponent<ITransform>();
                            trans.Position.X = inter.Position.X;
                            trans.Position.Y = inter.Position.Y;
                            trans.Position.Z = inter.Position.Z;
                        }                        

                        candidate.Clear();
                        candidate.Add(currentEntity);
                    }
                    else
                    {
                        this.entities.AddRange(candidate.Where(new Func<IEntity,bool>((entity) =>
                            {
                                return entity != currentEntity;
                            })));
                        candidate.Clear();
                        this.IsDirty = true;
                    }
                }
                renderer.Render(world, camera);
            };
            renderer.OnKeyChanged += new EventHandler<OnKeyEventArgs>(onKeyChanged);
            renderer.Render(world, camera);

            var timer = new DispatcherTimer();
            timer.Tick += (o, e) =>
            {
                if (currentEntity != null)
                {
                    timer.Stop();
                    // keep this thread free, since renderer.Render is blocking
                    // which could cause deadlocks if a callback from that thread is
                    // waiting on this
                    ThreadPool.QueueUserWorkItem(new WaitCallback((obj) =>
                    {
                        renderer.Dispatch(new Action(() =>
                            {
                                if (currentEntity != null)
                                {
                                    //check if current entity is intersecting,
                                    //if so, set the red tint to 0.5f
                                    bool found = false;
                                    ITransform t2 = currentEntity.HasComponent<ITransform>() ?
                                        currentEntity.GetComponent<ITransform>() : null;
                                    if (t2 != null)
                                    {
                                        var copy = new List<IEntity>(entities);
                                        foreach (var ent in copy)
                                        {
                                            var t1 = ent.GetComponent<ITransform>();
                                            if (ent != currentEntity
                                                && Detect(t1, t2))
                                            {
                                                found = true;
                                                break;
                                            }
                                        }

                                        if (currentEntity.HasComponent<Editable>())
                                        {
                                            currentEntity.GetComponent<Editable>().RedTint = found ? 0.5f : 0.0f;
                                        }
                                    }
                                }
                            }));   
                        renderer.Render(world, camera);
                        timer.Start();
                    }));
                }
            };
            timer.Interval = new TimeSpan(0, 0, 0, 0, 50);
            timer.Start();

        }

        private void LoadScriptDlls()
        {
            // setup script library

            object ret = Microsoft.Win32.Registry.GetValue(Microsoft.Win32.Registry.CurrentUser
                            + @"\Software\MemoryManipulator", "Path", null);
                        
            if (ret != null)
            {
                string path = System.IO.Path.GetDirectoryName(ret as string);

                string temp = Environment.CurrentDirectory + "\\tempDlls\\";
                System.IO.Directory.CreateDirectory(temp);

                try
                {
                    string[] dlls = null;
                    dlls = System.IO.Directory.GetFiles(temp, "*.dll");
                    foreach (var file in dlls)
                    {
                        System.IO.File.Delete(file);
                    }
                }
                catch (Exception)
                {
                }

                var assemblies = AppDomain.CurrentDomain.GetAssemblies();

                string[] files=null;
                try
                {
                    files = System.IO.Directory.GetFiles(path, "*.dll");
                }
                catch (Exception)
                {
                    files = null;
                }

                if (files != null)
                {
                    foreach (var file in files)
                    {
                        string dest = temp + System.IO.Path.GetFileName(file);
                        try
                        {
                            bool loaded = false; ;
                            foreach (var assembly in assemblies)
                            {
                                loaded |= assembly.GetName().Name == System.IO.Path.GetFileNameWithoutExtension(file);
                            }
                            if (!loaded)
                            {
                                try
                                {
                                    System.IO.File.Copy(file, dest);
                                }
                                catch (Exception)
                                {
                                }
                                scripting.LoadScriptDll(dest);
                            }
                        }
                        catch (Exception)
                        {
                        }
                        finally
                        {
                            try
                            {
                                System.IO.File.Delete(dest);
                            }
                            catch (Exception)
                            {
                            }
                        }
                    }

                    try
                    {
                        System.IO.Directory.Delete(temp);
                    }
                    catch (Exception)
                    {
                    }
                }

                var types = scripting.Types;
                types.RemoveAll(new Predicate<Type>((Type type) =>
                    {
                        foreach (var attribute in type.GetCustomAttributes(true))
                        {
                            if (attribute.GetType() == typeof(Islander.Scripting.HideFromEditorAttribute))
                                return true;
                        }
                        return false;
                    }));

            }

        }

        private void DeleteEntity(IEntity entity)
        {
            if (entity.HasComponent<Linker>())
            {
                foreach (var link in entity.GetComponent<Linker>().Links)
                {
                    if (link.Child != entity)
                    {
                        link.Child.GetComponent<Linker>().Links.Remove(link);
                    }
                    if (link.Owner != entity)
                    {
                        link.Owner.GetComponent<Linker>().Links.Remove(link);
                    }
                    if (link.Linker != null)
                    {
                        world.RemoveEntity(link.Linker);
                    }
                }
            }
            if (entity.HasComponent<TriggerBox>())
            {
                var trigger = entity.GetComponent<TriggerBox>();
                trigger.Destroy(world);
            }
            renderer.Release(entity.GetComponent<ISprite>().Material);
            world.RemoveEntity(entity);
            entities.Remove(entity);
            IsDirty = true;
        }

        public void OnKeyChanged(OnKeyEventArgs e)
        {
            if (this.onKeyChanged != null)
            {
                this.onKeyChanged(this, e);
            }
        }

        public void OnKeyTriggered(OnKeyEventArgs e)
        {
            if (this.onKeyTriggered != null)
            {
                this.onKeyTriggered(this, e);
            }
        }

        private bool Intersecting(out IEntity entity)
        {
            entity = null;
            if (currentEntity != null)
            {
                //check if current entity is intersecting,
                //if so, set the red tint to 0.5f
                ITransform t1 = currentEntity.GetComponent<ITransform>();
                var copy = new List<IEntity>(entities);

                List<IEntity> foundlist = copy.FindAll(new Predicate<IEntity>(delegate(IEntity ent)
                {
                    if (ent.HasComponent<ITransform>())
                    {
                        var t2 = ent.GetComponent<ITransform>();
                        if (ent != currentEntity
                            && Detect(t1, t2))
                        {
                            return true;
                        }
                    }
                    return false;
                }));
                foundlist.Sort(new Comparison<IEntity>((IEntity a, IEntity b) =>
                {
                    float az = 0;
                    if (a != null && a.HasComponent<ITransform>())
                    {
                        az = a.GetComponent<ITransform>().Position.Z;
                    }
                    float bz = 0;
                    if (b != null && b.HasComponent<ITransform>())
                    {
                        bz = b.GetComponent<ITransform>().Position.Z;
                    }

                    return az > bz ? -1 : 1;
                }));

                if (foundlist.Count > 0)
                    entity = foundlist.First();
            }
            return entity!=null;
        }

        static bool Detect(ITransform t, ITransform s)
        {
            return t.Position.X + t.Scale.X > s.Position.X - s.Scale.X
                && t.Position.X - t.Scale.X < s.Position.X + s.Scale.X
                && t.Position.Y + t.Scale.Y > s.Position.Y - s.Scale.Y
                && t.Position.Y - t.Scale.Y < s.Position.Y + s.Scale.Y;
        }


        public System.Windows.Controls.Image CurrentItem
        {
            get
            {
                return this.currentSelection;
            }
            set
            {
                this.currentSelection = value;
                if (currentEntity != null)
                {
                    ISprite sprite = currentEntity.GetComponent<ISprite>();
                    sprite.Material = GetResource();
                    var image = imageDictionary[sprite.Material.Texture];
                    currentEntity.GetComponent<ITransform>().Scale
                        = new Vec3<float>(image.Width * sprite.Material.TextureScale.X / 2,
                            image.Height * sprite.Material.TextureScale.Y / 2, 0);
                }
                OnPropertyChanged("CurrentItem");
            }
        }

        public string GetResourceName()
        {
            if (CurrentItem == null)
                return null;

            //PictureBox box = CurrentItem.Child as PictureBox;
            var box = CurrentItem as System.Windows.Controls.Image;
            if (box == null)
                return null;

            String ret = null;
            
            var operation = box.Dispatcher.BeginInvoke(new Action(() =>
                {

                    Editor.Form1.TileType tile = box.Tag as Editor.Form1.TileType;
                    if (tile != null)
                        ret = tile.name;

                }));

            operation.Wait();

            return ret;
        }

        private IMaterial GetResource()
        {            
            return renderer.LoadResource(GetResourceName());
        }

        private IMaterial GetResourceByName(string name)
        {
            return renderer.LoadResource(name);
        }

        private IEntity AddItemSprite(Vec3f position)
        {
            string name = GetResourceName();
            if (name == null)
                return null;

            IMaterial resource = GetResourceByName(name);

            if (resource == null)
                return null;
            
            IEntity sp = null;

            renderer.Dispatch(new Action(() =>
                {
                    try
                    {
                        sp = world.CreateEntity();

                        Linker linker = sp.AddComponent<Linker>();
                        ISprite sprite = sp.AddComponent<ISprite>();
                        ITransform sptrans = sp.AddComponent<ITransform>();
                        Editable editable = sp.AddComponent<Editable>();
                        editable.RedTint = 1;
                        editable.IsVisable = true;
                        editable.Name = name;
                        linker.PropertyChanged += (o, e) =>
                            {
                                LinkerPropertyChanged(o, e);
                            };
                        sptrans.Position = position;

                        sprite.Material = resource;
                        IMaterial mat = sprite.Material;

                        var image = imageDictionary[mat.Texture];

                        sptrans.Scale = new Vec3f(mat.TextureScale.X * image.Width / 2,
                            mat.TextureScale.Y * image.Height / 2, 0);
                        sptrans.Direction.Y = 1;
                    }
                    catch (NullReferenceException)
                    {
                        // Fail to add sprite if resource contains null data
                        if (sp != null)
                        {
                            world.RemoveEntity(sp);
                        }
                    }
                }));

            return sp;
        }

        private void LinkerPropertyChanged(object o, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "Links")
            {
                foreach (var link in (o as Linker).Links)
                {
                    link.LinkEntities(world, this.lineMaterial);
                }
            }
        }

        public Command<System.Windows.Controls.Image> SelectItem
        {
            get
            {
                return new Command<System.Windows.Controls.Image>(delegate(System.Windows.Controls.Image e)
                {
                    this.CurrentItem = e;
                });
            }
        }

        public Command<Vec3f> AddItem
        {
            get
            {
                return new Command<Vec3f>(delegate(Vec3f position)
                    {
                        AddItemSprite(position);
                    });
            }
        }
        
        public class FinderSettings
        {
            public string File { get; set; }
            public string RawFile { get; set; }
        }

        public Command<FinderSettings> FinderSave
        {
            get
            {
                return new Command<FinderSettings>(delegate(FinderSettings settings)
                {
                    settings.File = null;

                    Microsoft.Win32.SaveFileDialog file
                            = new Microsoft.Win32.SaveFileDialog();
                    bool? ret = file.ShowDialog();

                    if (ret != null &&
                        ret.Value)
                    {
                        settings.RawFile = file.FileName;
                        settings.File = System.IO.Path.GetDirectoryName(file.FileName) + @"\" + System.IO.Path.GetFileNameWithoutExtension(file.FileName);
                    }

                });
            }
        }

        public Command<FinderSettings> Finder
        {
            get
            {
                return new Command<FinderSettings>(delegate(FinderSettings settings)
                    {
                        settings.File = null;

                        Microsoft.Win32.OpenFileDialog file
                                = new Microsoft.Win32.OpenFileDialog();
                        bool? ret = file.ShowDialog();

                        if (ret != null &&
                            ret.Value)
                        {
                            settings.RawFile = file.FileName;
                            settings.File = System.IO.Path.GetDirectoryName(file.FileName) + @"\" + System.IO.Path.GetFileNameWithoutExtension(file.FileName);
                        }

                    });
            }
        }

        public Command<object> Deploy
        {
            get
            {
                return new Command<object>((o) =>
                    {
                        new Deploy().ShowDialog();
                    });
            }
        }

        public Command<object> PlayMap
        {
            get
            {
                return new Command<object>((o) =>
                    {
                        var command = Editor.PluginManager.GetCommand(Commands.PlayCommand);
                        if (command != null)
                        {
                            command.Action(new WorldContext()
                                {
                                    Filename = levelFilename,
                                    Scripting = this.scripting,
                                });
                        }
                    });
            }
        }

        public Command<Command<FinderSettings>> LoadMap
        {
            get
            {
                return new Command<Command<FinderSettings>>(new Action<Command<FinderSettings>>(delegate(Command<FinderSettings> finder)
                {
                    FinderSettings settings = new FinderSettings();
                    finder.Execute(settings);
                    string filename = settings.RawFile;

                    if (filename == null)
                        return;

                    DialogResult result = MessageBox.Show("Clear the map before loading?", "Info", MessageBoxButtons.YesNo);

                    if (result == DialogResult.Yes)
                    {
                        renderer.Dispatch(new Action(() =>
                            {
                                while (entities.Count > 0)
                                {
                                    DeleteEntity(entities[0]);
                                }
                            }));
                        entities.Clear();
                    }

                    var command = Editor.PluginManager.GetCommand(Commands.LoadCommand);
                    if (command != null)
                    {
                        renderer.Dispatch(new Action(() =>
                            {
                                command.Action(new WorldContext()
                                    {
                                        Filename = filename,
                                        World = world,
                                        Entities = entities,
                                        Scripting = this.scripting,
                                    });
                                LevelFilename = filename;
                                entities.RemoveAll(new Predicate<IEntity>((e) => { return e == null; }));
                                // re-attach material
                                foreach (var entity in entities)
                                {
                                    ISprite sp= null;
                                    if (entity.HasComponent<ISprite>())
                                        sp = entity.GetComponent<ISprite>();
                                    if (entity.HasComponent<Editable>())
                                    {
                                        sp.Material = GetResourceByName(entity.GetComponent<Editable>().Name);
                                        foreach (var link in entity.GetComponent<Linker>().Links)
                                        {
                                            if (link.Linker != null)
                                            {
                                                var mat = link.Linker.GetComponent<ILineSprite>().Material;
                                                mat.PixelShader = this.lineMaterial.pixel;
                                                mat.VertexShader = this.lineMaterial.vertex;
                                            }
                                        }
                                        System.Diagnostics.Debug.Assert(sp.Material != null);
                                    }
                                    if (entity.HasComponent<TriggerBox>())
                                    {
                                        var triggerbox = entity.GetComponent<TriggerBox>();
                                        ILineSprite[] lines = new ILineSprite[4]
                                        {
                                            triggerbox.Left, triggerbox.Right, triggerbox.Top, triggerbox.Bottom,
                                        };
                                        foreach (var line in lines)
                                        {
                                            line.Material.PixelShader = lineMaterial.pixel;
                                            line.Material.VertexShader = lineMaterial.vertex;
                                        }
                                        sp.Material.PixelShader = uniformquad.pixel;
                                        sp.Material.VertexShader = uniformquad.vertex;
                                    }
                                }
                                IsDirty = false;
                            }));        
                    }

                }));
            }
        }

        public Command<Command<FinderSettings>> SaveMap
        {
            get
            {
                return new Command<Command<FinderSettings>>(new Action<Command<FinderSettings>>(delegate(Command<FinderSettings> finder)
                {
                    FinderSettings settings = new FinderSettings();
                    finder.Execute(settings);
                    string filename = settings.RawFile;

                    if (filename == null)
                        return;

                    var command = Editor.PluginManager.GetCommand(Commands.SaveCommand);
                    if (command != null)
                    {
                        renderer.Suspend();
                        command.Action(new WorldContext()
                            {
                                Filename = filename,
                                World = world,
                                Entities = entities,
                                Scripting = this.scripting,
                            });
                        LevelFilename = filename;
                        renderer.Resume();
                        this.tsp.Text = Path.GetFileNameWithoutExtension(settings.File);
                    }
                }));
            }
        }

        public Command<Command<FinderSettings>> LoadSpriteSheet
        {
            get
            {
                return new Command<Command<FinderSettings>>(new Action<Command<FinderSettings>>(delegate(Command<FinderSettings> finder)
                    {
                        FinderSettings settings = new FinderSettings();
                        finder.Execute(settings);
                        string filename = settings.File;

                        if (filename == null)
                            return;

                        string tilemap=filename + ".mat";

                        string image=filename + ".png";

                        ITexture2D texture = renderer.LoadTexture(filename);

                        List<Editor.Form1.TileType> tiles = Editor.Form3.LoadMaterialFile(tilemap);

                        var bitmap = Bitmap.FromFile(image) as Bitmap;

                        imageDictionary.Add(texture, bitmap);

                        List<System.Windows.Controls.Image> components = CreateTileTypes(bitmap, tiles);

                        components.ForEach(delegate(System.Windows.Controls.Image e)
                        {
                            Images.Add(e);
                            this.textures.Add(e, texture);
                        });

                    }));
            }
        }

        private List<System.Windows.Controls.Image> CreateTileTypes(Bitmap image, List<Editor.Form1.TileType> tiles)
        {
            List<System.Windows.Controls.Image> components = new List<System.Windows.Controls.Image>();

            for (int i = 0; i < tiles.Count; i++)
            {
                Editor.Form1.TileType tile = tiles[i];

                Bitmap map = (Bitmap)image.Clone(new Rectangle(tile.x, tile.y, tile.width, tile.height),
                    System.Drawing.Imaging.PixelFormat.Format32bppArgb);

                var source = System.Windows.Interop.Imaging.CreateBitmapSourceFromHBitmap(
                    map.GetHbitmap(),
                    IntPtr.Zero,                    
                   System.Windows.Int32Rect.Empty, 
                   System.Windows.Media.Imaging.BitmapSizeOptions
                   .FromWidthAndHeight(map.Width, map.Height));

                System.Windows.Controls.Image control = new System.Windows.Controls.Image();
                control.Source = source;
                control.Tag = tile;

                //PictureBox box = new PictureBox()
                //{
                //    BackColor = Color.Transparent,
                //    BackgroundImage = map,
                //    Location = new Point((i % 3) * 68, (i / 3) * 68),
                //    Width = 64,
                //    Height = 64,
                //    BorderStyle = BorderStyle.FixedSingle,
                //    BackgroundImageLayout = ImageLayout.Stretch,
                //    Tag = tile

                //};

                //var host= new WindowsFormsHost();
                //host.Child = box;
                //components.Add(host);

                components.Add(control);

                //control.MouseDown += delegate(object sender, EventArgs e)
                //{
                //    this.SelectItem.Execute(host);
                //};
            }

            return components;
        }

        public ObservableCollection<System.Windows.Controls.Image> Images
        {
            get
            {
                return this.images;
            }
            set
            {
                this.images = value;
                OnPropertyChanged("Images");
            }
        }

        #region IDispose
        private bool disposed;

        private void Dispose(bool disposing)
        {
            if (this.disposed)
                return;

            if (disposing)
            {
                if (this.renderer != null)
                {
                    this.renderer.Dispose();
                }
            }

            this.disposed = true;
        }

        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion
    }
}
