

Texture2D display : register (t0);

SamplerState textureSampler : register (s0);

float2 rotate(float2 s, float2 v)
{

	//return float2(s.x*v.x-s.y*v.y,s.x*v.y+s.y*v.x);	//simplify with dots
	
	return float2(dot(s.xy, v.xy*float2(1,-1)), dot(s.xy, v.yx));
}

float4 UniformQuadVertex(in float2 position : POSITION, in float2 incoord : TEXCOORD0,
	in float4 location : TEXCOORD1, in float4 texation : TEXCOORD2, in float4 transkey : TEXCOORD3,
	in float4 rotation : TEXCOORD4, in float4 effect : TEXCOORD5, in float3 tint : TEXCOORD6,
	in float opacity : TEXCOORD7,
	out float4 tex : TEXCOORD0, out float clipdist : SV_ClipDistance) : SV_POSITION
{
	float2 pos = location.xy;
	float2 scale = location.zw;
	float2 texscale = texation.zw;
	float2 texpos = texation.xy;
	float clip = effect.y;

	tex.xy = incoord*texscale + texpos;

	float2 outpos=position;
	outpos*=scale;	//scale
	outpos=rotate(outpos, rotation.xy);	//rotate
	outpos=outpos+pos; //translate

	clipdist = dot(float3(0, 1, 1), float3(outpos.x, outpos.y-clip, 0));
	
	tex.rgb = tint.rgb;
	tex.a = (opacity/2)+0.5f;
		
	return float4(outpos, transkey.w, 1);
}

void UniformQuadPixel(in float4 pos : SV_POSITION, in float4 incolor : TEXCOORD0, out float4 outcolor : SV_Target)
{

	outcolor = incolor;
	
}



