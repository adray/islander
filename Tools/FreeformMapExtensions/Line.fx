

float4 LineVertex(in float index : POSITION, in float2 start : TEXCOORD0,
	in float2 end : TEXCOORD1, in float4 camera : TEXCOORD2,
	out float debug : TEXCOORD0) : SV_POSITION
{

	float2 pos=float2(0,0);
	if (index == 0)
	{
		pos = start;
	}
	else if (index == 1)
	{
		pos = end;
	}
	
	debug = camera.xy;
	
	return float4(pos.x/camera.z+camera.x, pos.y/camera.w+camera.y, 0, 1);
}


float4 LinePixel(in float4 pos : SV_POSITION, in float2 debug : TEXCOORD0) : SV_TARGET
{

	return float4(debug.x,0,0,1);

}