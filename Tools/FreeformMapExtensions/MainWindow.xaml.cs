﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using Editor;
using Islander.Managed;
using System.Timers;

namespace FreeformMapExtensions
{
    [Plugin(CommandName="Freeform Map")]
    public class FreeformMap : IPlugin
    {
        private MainWindow window;
        
        public void Action(IPluginContext context)
        {
            if (window == null)
            {
                window = new MainWindow();
                window.Closed += new EventHandler(window_Closed);
                window.Show();
            }
            else
            {
                window.Activate();
            }
        }

        public void Close()
        {
            if (window != null)
            {
                window.Close();
            }
        }

        void window_Closed(object sender, EventArgs e)
        {
            window = null;
        }
    }

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private bool[] keysDown = new bool[200];
        private bool isCtrl;
        private Timer timer;

        public MainWindow()
        {
            InitializeComponent();
            
            this.Loaded += (sender, e) =>
                {
                    this.DataContext = new MainViewModel();
                    this.timer = new Timer(50);
                    this.timer.Elapsed += new ElapsedEventHandler(timer_Elapsed);
                    this.timer.Start();
                };

            this.Closed += (sender, e) =>
                {
                    MainViewModel context = this.DataContext as MainViewModel;
                    if (context != null)
                    {
                        context.Dispose();
                    }
                };
            
            this.Deactivated += (sender, e) =>
                {
                    for (int i = 0; i < keysDown.Length; i++)
                    {
                        keysDown[i] = false;
                    }
                };
        }

        void timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            Dispatcher.BeginInvoke(new Action(() =>
                {
                    MainViewModel context = this.DataContext as MainViewModel;
                    if (context != null)
                    {
                        for (int i = 0; i < this.keysDown.Length; i++)
                        {
                            if (this.keysDown[i])
                            {
                                context.OnKeyTriggered(new OnKeyEventArgs(i, this.keysDown[i]));
                            }
                            this.keysDown[i] = false;
                        }
                    }
            }));
        }

        protected override void OnKeyDown(KeyEventArgs e)
        {
            base.OnKeyDown(e);

            ProcessKeys(e);
        }

        protected override void OnKeyUp(KeyEventArgs e)
        {
            base.OnKeyUp(e);

            ProcessKeys(e);
        }

        private void ProcessKeys(KeyEventArgs e)
        {
            MainViewModel context = this.DataContext as MainViewModel;
            if (context != null)
            {
                int key = KeyInterop.VirtualKeyFromKey(e.Key);
                if (e.KeyboardDevice.Modifiers.HasFlag(ModifierKeys.Control))
                {
                    isCtrl = true;
                }
                if (isCtrl)
                {
                    key = (int)System.Windows.Forms.Keys.ControlKey;
                }
                if (!e.IsRepeat || !e.IsDown)
                {
                    context.OnKeyChanged(new OnKeyEventArgs(key, e.IsDown));
                }
                if (key >= 0 && key < this.keysDown.Length)
                {
                    this.keysDown[key] = e.IsDown;
                }
                //if (e.IsDown)
                //{
                    //context.OnKeyTriggered(new OnKeyEventArgs(key, e.IsDown));
                //}
                if (isCtrl)
                {
                    isCtrl = e.IsDown;
                }
            }
        }
        
        public void RenderInit(object sender, EventArgs<IRenderer> e)
        {
            MainViewModel context = this.DataContext as MainViewModel;
            if (context != null)
            {
                context.Init(e.Value);
            }
        }
    }
}
