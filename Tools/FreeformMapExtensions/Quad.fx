

Texture2D display : register (t0);

SamplerState textureSampler : register (s0);

//cbuffer Constants : register (c0)
//{

//	float2 pos;
//	float2 scale;
//	float2 texpos;
//	float2 texscale;
//	float4 transkey;
//	float2 rotation;
//	float2 threshold;
//	float glow;
//	float clip;
//	float2 padding;

//};


float2 rotate(float2 s, float2 v)
{

	//return float2(s.x*v.x-s.y*v.y,s.x*v.y+s.y*v.x);	//simplify with dots
	
	return float2(dot(s.xy, v.xy*float2(1,-1)), dot(s.xy, v.yx));
}

float4 QuadVertex(in float2 position : POSITION, in float2 incoord : TEXCOORD0,
	in float4 location : TEXCOORD1, in float4 texation : TEXCOORD2, in float4 transkey : TEXCOORD3,
	in float4 rotation : TEXCOORD4, in float3 effect : TEXCOORD5, in float opacity : TEXCOORD6, in float4 light : TEXCOORD7,
	out float4 tex : TEXCOORD0, out float4 out_effect : TEXCOORD1, out float clipdist : SV_ClipDistance) : SV_POSITION
{
	float2 pos = location.xy;
	float2 scale = location.zw;
	float2 texscale = texation.zw;
	float2 texpos = texation.xy;
	float clip = effect.y;

	tex.xy = incoord*texscale + texpos;

	float2 outpos=position;
	outpos*=scale;	//scale
	outpos=rotate(outpos, rotation.xy);	//rotate
	outpos=outpos+pos; //translate

	clipdist = dot(float3(0, 1, 1), float3(outpos.x, outpos.y-clip, 0));
	
	tex.z = effect.x;
	tex.w = 0;
	//tex.w = tint;
	
	out_effect = float4(opacity,0,0,0);
	
	return float4(outpos, transkey.w, 1);
}

void QuadPixel(in float4 pos : SV_POSITION, in float4 tex : TEXCOORD0, in float4 effect : TEXCOORD1, out float4 outcolor : SV_Target)
{

	outcolor =  display.Sample(textureSampler, tex.xy);
	
	float glow = tex.z;
	//outcolor = float4(glow,glow,glow,1);
		
	outcolor.rgb -= float3(0.3f * glow, 0.6f * glow, 0.3f * glow);
	outcolor.r += tex.w;
	
	outcolor.a *= effect.x;
	
}



