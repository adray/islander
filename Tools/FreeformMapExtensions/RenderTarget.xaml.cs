﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Interop;
using Islander.Managed;
using System.Runtime.InteropServices;

namespace FreeformMapExtensions
{
    public class EventArgs<T> : EventArgs
    {
        public T Value { get; set; }

        public EventArgs(T arg)
        {
            Value = arg;
        }
    }

    public class RenderHost : HwndHost
    {
        private IRenderer _renderer;

        public event EventHandler<EventArgs<IRenderer>> RendererInitilized;

        public RenderHost()
        {
        }

        protected override System.Runtime.InteropServices.HandleRef BuildWindowCore(System.Runtime.InteropServices.HandleRef hwndParent)
        {
            _renderer = new IRenderer(hwndParent.Handle);
            this.Focusable = true;
                        
            if (RendererInitilized != null)
            {
                RendererInitilized(this, new EventArgs<IRenderer>(_renderer));
            }

            return new System.Runtime.InteropServices.HandleRef(this, (IntPtr)_renderer.Handle);
        }
        protected override bool TranslateAcceleratorCore(ref MSG msg, ModifierKeys modifiers)
        {
            System.IntPtr handle = (System.IntPtr)_renderer.Handle;

            

            return false;
        }

        protected override IntPtr WndProc(IntPtr hwnd, int msg, IntPtr wParam, IntPtr lParam, ref bool handled)
        {
            return base.WndProc(hwnd, msg, wParam, lParam, ref handled);
        }

        protected override void DestroyWindowCore(System.Runtime.InteropServices.HandleRef hwnd)
        {
            
        }
    }
    
    /// <summary>
    /// Interaction logic for RenderTarget.xaml
    /// </summary>
    public partial class RenderTarget : UserControl
    {
        private RenderHost host;
        public event EventHandler<EventArgs<IRenderer>> RendererInitilized;

        public RenderTarget()
        {
            InitializeComponent();

            Loaded += new RoutedEventHandler(RenderTarget_Loaded);
        }

        void RenderTarget_Loaded(object sender, RoutedEventArgs e)
        {
            host = new RenderHost();
            host.RendererInitilized += new EventHandler<EventArgs<IRenderer>>(host_RendererInitilized);
            this.ControlHostElement.Child = host;
        }

        void host_RendererInitilized(object sender, EventArgs<IRenderer> e)
        {
            if (RendererInitilized != null)
            {
                RendererInitilized(this, e);
            }
        }
    }
}
