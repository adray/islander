﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Diagnostics;

namespace FreeformMapExtensions
{
    /// <summary>
    /// Interaction logic for Deploy.xaml
    /// </summary>
    public partial class Deploy : Window
    {
        public Deploy()
        {
            InitializeComponent();

            Loaded += new RoutedEventHandler(Deploy_Loaded);
        }

        void Deploy_Loaded(object sender, RoutedEventArgs e)
        {
            this.DataContext = new DeployModel();
        }

        private class DeployModel : ViewModelBase<DeployModel>
        {
            private string version;
            private string output;
            private string dev;
            private string game;

            public DeployModel()
            {
                Dev = new System.IO.DirectoryInfo(Environment.CurrentDirectory).FullName;
            }

            public string Version
            {
                get
                {
                    return this.version;
                }
                set
                {
                    this.version = value;
                    OnPropertyChanged("Version");
                }
            }

            public string GameDir
            {
                get
                {
                    return this.game;
                }
                set
                {
                    this.game = value;
                    OnPropertyChanged("GameDir");
                }
            }

            public string OutputDir
            {
                get
                {
                    return this.output;
                }
                set
                {
                    this.output = value;
                    OnPropertyChanged("OutputDir");
                }
            }

            public string Dev
            {
                get
                {
                    return this.dev;
                }
                set
                {
                    this.dev = value;
                    OnPropertyChanged("Dev");
                }
            }

            public Command<object> BrowseOutput
            {
                get
                {
                    return new Command<object>((o) =>
                        {
                            var dialog = new System.Windows.Forms.FolderBrowserDialog();
                            System.Windows.Forms.DialogResult result = dialog.ShowDialog();

                            if (result == System.Windows.Forms.DialogResult.OK)
                            {
                                OutputDir = dialog.SelectedPath;
                            }
                        });
                }
            }

            public Command<object> BrowseGame
            {
                get
                {
                    return new Command<object>((o) =>
                    {
                        var dialog = new System.Windows.Forms.FolderBrowserDialog();
                        System.Windows.Forms.DialogResult result = dialog.ShowDialog();

                        if (result == System.Windows.Forms.DialogResult.OK)
                        {
                            GameDir = dialog.SelectedPath;
                        }
                    });
                }
            }

            public Command<object> BrowseDev
            {
                get
                {
                    return new Command<object>((o) =>
                    {
                        var dialog = new System.Windows.Forms.FolderBrowserDialog();
                        System.Windows.Forms.DialogResult result = dialog.ShowDialog();

                        if (result == System.Windows.Forms.DialogResult.OK)
                        {
                            Dev = dialog.SelectedPath;
                        }
                    });
                }
            }

            public Command<object> Deploy
            {
                get
                {
                    return new Command<object>((o) =>
                    {
                        bool result =
                            !string.IsNullOrEmpty(Version) &&
                            !string.IsNullOrEmpty(OutputDir) &&
                            (!string.IsNullOrEmpty(Dev) || !string.IsNullOrEmpty(GameDir));
                        if (result)
                        {
                            Process process = new Process();
                            process.StartInfo.FileName = Environment.CurrentDirectory + @"\Build.exe";
                            if (!string.IsNullOrEmpty(Version))
                            {
                                process.StartInfo.Arguments += string.Format("/v:{0} ", Version);
                            }
                            if (!string.IsNullOrEmpty(OutputDir))
                            {
                                process.StartInfo.Arguments += string.Format("/o:\"{0}\"\\ ", OutputDir);
                            }
                            if (!string.IsNullOrEmpty(Dev))
                            {
                                process.StartInfo.Arguments += string.Format("/dev:\"{0}\" ", Dev);
                            }
                            if (!string.IsNullOrEmpty(GameDir))
                            {
                                process.StartInfo.Arguments += string.Format("/i:\"{0}\" ", GameDir);
                            }

                            try
                            {
                                process.Start();
                                MessageBox.Show("Deployed!");
                            }
                            catch (Exception)
                            {
                                MessageBox.Show("Failed to deploy.");

                            }
                        }
                    });
                }
             }
        }
    }
}
