﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace SpriteSheetGenerator
{
    class Program
    {
        private bool verbose;
        private bool rebuild;

        enum ColorTransform
        {
            Default,
            Invert
        }

        class Transform
        {
            public string Suffix { get; set; }
            public ColorTransform R { get; set; }
            public ColorTransform G { get; set; }
            public ColorTransform B { get; set; } 
        }

        class Settings
        {
            public String InputFolder { get; set; }
            public String OutFolder { get; set; }
            public int Width { get; set; }
            public int Height { get; set; }
            public String Name { get; set; }
            public Transform Transform { get; set; }
            public Color? TransparencyKey { get; set; }
        }

        class Material
        {
            public int X { get; set; }
            public int Y { get; set; }
            public int Width { get; set; }
            public int Height { get; set; }
            public string Name { get; set; }
        }

        class Sprite
        {
            public System.Drawing.Bitmap Bitmap { get; set; }
            public string Name { get; set; }
        }

        public void Build(string[] args)
        {
            foreach (string p in args)
            {
                switch (p)
                {
                    case "/rebuild":
                        rebuild = true;
                        break;
                    case "/verbose":
                        verbose = true;
                        break;
                    default:
                        Console.WriteLine("Unknown parameter " + p);
                        return;
                }
            }

            System.Xml.XmlDocument doc = new System.Xml.XmlDocument();
            try
            {
                string filename = "sprite-config";
                const string win = "sprite-config-win";
                const string linux = "sprite-config-linux";
                if (System.IO.File.Exists(win))
                {
                    filename = win;
                }
                else if (System.IO.File.Exists(linux))
                {
                    filename = linux;
                }

                doc.Load(filename);                
            }
            catch (Exception ex)
            {
                doc = null;
                Console.WriteLine("Failed to load configuration file");
                Console.WriteLine(ex.Message);
                Console.WriteLine(ex.StackTrace);
            }

            if (doc != null)
            {
                foreach (System.Xml.XmlNode node in doc.SelectNodes("/Config/Spritesheet"))
                {
                    var width = node.Attributes["Width"];
                    var height = node.Attributes["Height"];
                    var input = node.Attributes["Input"];
                    var output = node.Attributes["Output"];
                    var name = node.Attributes["Name"];
                    var transform = node.SelectSingleNode("Transform");
                    var transparent = node.SelectSingleNode("TransparencyKey");

                    var setting = new Settings();
                    if (name != null) setting.Name = name.Value;
                    if (width != null) setting.Width = Int32.Parse(width.Value);
                    if (height != null) setting.Height = Int32.Parse(height.Value);
                    if (input != null) setting.InputFolder = input.Value;
                    if (output != null) setting.OutFolder = output.Value;

                    if (transform != null)
                    {
                        setting.Transform = new Transform();

                        var suffix = transform.Attributes["Suffix"];
                        var r = transform.Attributes["R"];
                        var g = transform.Attributes["G"];
                        var b = transform.Attributes["B"];

                        if (suffix != null) setting.Transform.Suffix = suffix.Value;
                        if (r != null) setting.Transform.R = (ColorTransform)Enum.Parse(typeof(ColorTransform), r.Value);
                        if (g != null) setting.Transform.G = (ColorTransform)Enum.Parse(typeof(ColorTransform), g.Value);
                        if (b != null) setting.Transform.B = (ColorTransform)Enum.Parse(typeof(ColorTransform), b.Value);
                    }

                    if (transparent != null)
                    {
                        var r = transparent.Attributes["R"];
                        var g = transparent.Attributes["G"];
                        var b = transparent.Attributes["B"];

                        int colR = 0;
                        int colG = 0;
                        int colB = 0;

                        if (r != null) colR = int.Parse(r.Value);
                        if (g != null) colG = int.Parse(g.Value);
                        if (b != null) colB = int.Parse(b.Value);

                        setting.TransparencyKey = Color.FromArgb(colR, colG, colB);
                    }

                    Build(setting);
                }
            }
        }

        private void Build(Settings settings)
        {
            System.Console.WriteLine("Computing files");

            //Find the last modification date of the output file, if it exists
            DateTime time = DateTime.MinValue;

            string outfile = System.IO.Path.Combine(settings.OutFolder, settings.Name + ".png");

            if (System.IO.File.Exists(outfile))
            {
                time = System.IO.File.GetLastWriteTimeUtc(outfile);
            }

            System.Drawing.Bitmap outimage = null;

            try
            {
                //create main bitmap
                outimage = new System.Drawing.Bitmap(settings.Width, settings.Height);
            }
            catch (Exception)
            {
                System.Console.WriteLine("Could not create outgoing image, check dimensions.");
                return;
            }

            System.Drawing.Graphics g = System.Drawing.Graphics.FromImage(outimage);

            //g.Clear(System.Drawing.Color.White);
            //g.Clear(System.Drawing.Color.Transparent);
            g.Clear(System.Drawing.Color.FromArgb(0, 0, 0, 0));

            List<Material> materials = new List<Material>();

            //load images
            
            string[] files = new string[0];
            try
            {
                files = System.IO.Directory.GetFiles(settings.InputFolder);
                System.Console.WriteLine("Looking in:" + settings.InputFolder + " and found " + files.Length + " files.");
            }
            catch (Exception)
            {
                System.Console.WriteLine("Failed to open:" + settings.InputFolder);
            }

            bool processingRequired = false;

            SortedList<int, Sprite> bitmaps = new SortedList<int, Sprite>();
            
            foreach (var file in files)
            {

                if (System.IO.Path.GetExtension(file) == ".png")
                {
                    try
                    {

                        var bitmap = (System.Drawing.Bitmap)System.Drawing.Bitmap.FromFile(file);

                        System.Console.WriteLine("Loaded:" + file);

                        // the key sorts them
                        bitmaps.Add(-(((bitmap.Height & 0xffff) << 16) | (bitmaps.Count & 0xffff)), new Sprite() { /*Bitmap = bitmap,*/ Name = file });

                        processingRequired |= System.IO.File.GetLastWriteTimeUtc(file) >= time;

                        bitmap.Dispose();
                    }
                    catch (Exception ex)
                    {
                        System.Console.WriteLine("Failed to load:" + file + Environment.NewLine + ex.Message);
                    }
                }
            }

            if (!processingRequired && !rebuild)
            {
                Console.WriteLine("No modifications to input files.");
                return;
            }

            int x = 0; int y = 0; int maxy = 0; int maxx = -1; int miny = 0; int minx = 0;

            foreach (var entry in bitmaps)
            {
                var bitmap = (System.Drawing.Bitmap)System.Drawing.Bitmap.FromFile(entry.Value.Name);          

                if (verbose)
                {
                    Console.WriteLine("Preparing " + entry.Value.Name);
                    Console.WriteLine("Performing pre-multiplication of alpha");
                }

                // Perform pre-multiplication of alpha

                var imgData = bitmap.LockBits(new System.Drawing.Rectangle(0, 0, bitmap.Width, bitmap.Height),
                    System.Drawing.Imaging.ImageLockMode.ReadWrite,
                    System.Drawing.Imaging.PixelFormat.Format32bppArgb);

                unsafe
                {
                    byte* start = (byte*)imgData.Scan0;
                    byte* end = start + imgData.Stride * imgData.Height;

                    for (byte* i = start; i < end; i += 4)
                    {
                        byte* blue = i;
                        byte* green = i + 1;
                        byte* red = i + 2;
                        byte* alpha = i + 3;

                        if (settings.Transform != null)
                        {
                            if (settings.Transform.R == ColorTransform.Invert) *red = (byte)(255 - *red);
                            if (settings.Transform.G == ColorTransform.Invert) *green = (byte)(255 - *green);
                            if (settings.Transform.B == ColorTransform.Invert) *blue = (byte)(255 - *blue);
                        }

                        if (settings.TransparencyKey.HasValue)
                        {
                            Color color = settings.TransparencyKey.Value;
                            if (color.R == *red &&
                                color.G == *green &&
                                color.B == *blue)
                            {
                                *alpha = 0;
                            }
                        }

                        *red = (byte)(*red * *alpha / 255.0f);
                        *green = (byte)(*green * *alpha / 255.0f);
                        *blue = (byte)(*blue * *alpha / 255.0f);
                        //*alpha = 255;
                    }
                }

                bitmap.UnlockBits(imgData);

                bool padx = bitmap.Width + 4 <= outimage.Width;
                bool pady = bitmap.Height + 4 <= outimage.Height;
                int paddedWidth = padx ? bitmap.Width + 4 : bitmap.Width;
                int paddedHeight = pady ? bitmap.Height + 4 : bitmap.Height;

                if (y + paddedHeight > maxy || x + paddedWidth > maxx)
                {
                    x = maxx+1; y = miny; minx = x;
                }

                if (x + paddedWidth > outimage.Width)
                {
                    x = 0; y += maxy + 1; maxy = 0; maxx = 0; miny = y; minx = 0;
                }

                g.DrawImage(bitmap, padx ? x+2 : x, pady ? y+2 : y, bitmap.Width, bitmap.Height);

                string name = System.IO.Path.GetFileNameWithoutExtension(entry.Value.Name);
                if (settings.Transform != null && !string.IsNullOrEmpty(settings.Transform.Suffix))
                {
                    name += settings.Transform.Suffix;
                }

                materials.Add(new Material() { Name = name, X = padx ? x+2 : x, Y = pady ? y+2 : y, Width = bitmap.Width, Height = bitmap.Height });

                if (padx || pady)
                {
                    if (verbose)
                    {
                        Console.WriteLine("Drawing border");
                    }

                    // Draw border. This protects in the case the sampler decides to round up and sample the pixel after our last pixel (minx, max, miny, maxy)

                    imgData = outimage.LockBits(new System.Drawing.Rectangle(0, 0, bitmap.Width, bitmap.Height),
                        System.Drawing.Imaging.ImageLockMode.ReadWrite,
                        System.Drawing.Imaging.PixelFormat.Format32bppArgb);

                    unsafe
                    {
                        if (pady)
                        {
                            {
                                byte* start = (byte*)imgData.Scan0 + 4 * (x + 2) + imgData.Stride * (y + 2);
                                byte* end = start + 4 * bitmap.Width;

                                for (byte* p = start; p <= end; p++)
                                {
                                    // Copy up
                                    *(p - imgData.Stride) = *p;
                                    *(p - imgData.Stride * 2) = *p;
                                }
                            }

                            {
                                byte* start = (byte*)imgData.Scan0 + 4 * (x + 2) + imgData.Stride * (y + 1 + bitmap.Height);
                                byte* end = start + 4 * bitmap.Width;

                                for (byte* p = start; p <= end; p++)
                                {
                                    // Copy down
                                    *(p + imgData.Stride) = *p;
                                    *(p + imgData.Stride * 2) = *p;
                                }
                            }
                        }

                        if (padx)
                        {
                            {
                                byte* start = (byte*)imgData.Scan0 + 4 * (x + 1 + bitmap.Width) + imgData.Stride * (y + 2);
                                byte* end = start + imgData.Stride * bitmap.Height;

                                for (byte* p = start; p <= end; p += imgData.Stride)
                                {
                                    for (byte* q = p; q < p + 4; q++)
                                    {
                                        // Copy Right
                                        *(q + 4) = *q;
                                        *(q + 8) = *q;
                                    }
                                }
                            }

                            {
                                byte* start = (byte*)imgData.Scan0 + 4 * (x + 2) + imgData.Stride * (y + 2);
                                byte* end = start + imgData.Stride * bitmap.Height;

                                for (byte* p = start; p <= end; p += imgData.Stride)
                                {
                                    for (byte* q = p; q < p + 4; q++)
                                    {
                                        // Copy Left
                                        *(q - 4) = *q;
                                        *(q - 8) = *q;
                                    }
                                }
                            }
                        }
                    }

                    outimage.UnlockBits(imgData);
                }

                maxx = Math.Max(maxx, minx + paddedWidth); maxy = Math.Max(maxy, paddedHeight); y += paddedHeight;
                bitmap.Dispose();
            }

            System.Console.WriteLine("Saving");
            
            try
            {

                outimage.Save(outfile, System.Drawing.Imaging.ImageFormat.Png);
                System.Console.WriteLine("Saved to:" + outfile);

            }
            catch (Exception)
            {

                System.Console.WriteLine("Failed to save:" + outfile);
                return;

            }

            string matfile = System.IO.Path.Combine(settings.OutFolder, settings.Name + ".mat");

            try
            {

                System.IO.StreamWriter wr = new System.IO.StreamWriter(matfile);
                wr.WriteLine(materials.Count);
                wr.WriteLine(outimage.Width);
                wr.WriteLine(outimage.Height);
                foreach (var mat in materials)
                {
                    wr.WriteLine(mat.Name);
                    wr.WriteLine(mat.X);
                    wr.WriteLine(mat.Y);
                    wr.WriteLine(mat.Width);
                    wr.WriteLine(mat.Height);
                }

                wr.Close();
                outimage.Dispose();

                System.Console.WriteLine("Saved to:" + matfile);

            }
            catch (Exception)
            {

                System.Console.WriteLine("Failed to save:" + matfile);
                return;

            }
        }

        static void Main(string[] args)
        {
#if DEBUG
            System.Diagnostics.Debugger.Launch();
#endif
            new Program().Build(args);

        }
    }
}
