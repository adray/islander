﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Editor
{
    [Editor.Plugin(CommandName = "Hero Editor")]
    public partial class HeroForm : Form, IPlugin
    {
        public enum HeroAttackType
        {
            Melee,
            Ranged,
        }

        public class Hero
        {
            
            private static int counter;

            public int id { get; set; }
            public string name { get; set; }
            public string material_portrait { get; set; }
            public string material_character { get; set; }
            public HeroAttackType attacktype { get; set; }
            public float base_strength { get; set; }
            public float base_defence { get; set; }
            public float base_movement { get; set; }
            public float base_turning { get; set; }
            public float base_resist { get; set; }
            public float base_magic { get; set; }
            public float base_health { get; set; }
            public float base_energy { get; set; }
            public float gain_strength { get; set; }
            public float gain_defence { get; set; }
            public float gain_movement { get; set; }
            public float gain_turning { get; set; }
            public float gain_resist { get; set; }
            public float gain_magic { get; set; }
            public float gain_health { get; set; }
            public float gain_energy { get; set; }

            public Hero()
            {
                id = counter;
                counter++;

                attacktype = HeroAttackType.Melee;
            }

        }

        private BindingList<Hero> heroes=new BindingList<Hero>();

        

        public HeroForm()
        {
            InitializeComponent();
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            List<HeroAttackType> types = new List<HeroAttackType>();
            types.Add(HeroAttackType.Melee); types.Add(HeroAttackType.Ranged);

            dataGridView1.DataSource = heroes;

            dataGridView1.Columns["id"].ReadOnly = true;
            dataGridView1.Columns["attacktype"].ReadOnly = true;

            dataGridView1.Click += new EventHandler(dataGridView1_Click);

        }

        void dataGridView1_Click(object sender, EventArgs e)
        {

            if (dataGridView1.SelectedCells.Count > 0)
            {
                if (dataGridView1.Columns[dataGridView1.SelectedCells[0].ColumnIndex].Name == "attacktype")
                {
                    Hero hero = (Hero)dataGridView1.Rows[dataGridView1.SelectedCells[0].RowIndex].DataBoundItem;
                    if (hero != null)
                    {
                        hero.attacktype = hero.attacktype == HeroAttackType.Ranged ? HeroAttackType.Melee : HeroAttackType.Ranged;
                    }
                }
            }

        }


        private void addItemToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
            

        }

        private void deleteItemToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {

            SaveFileDialog save = new SaveFileDialog();

            save.Filter = "Hero Files|*.hero";

            DialogResult res = save.ShowDialog();

            if (res == System.Windows.Forms.DialogResult.OK)
            {

                System.IO.StreamWriter sw = new System.IO.StreamWriter(save.FileName);

                sw.WriteLine(heroes.Count);

                foreach (Hero hero in heroes)
                {

                    sw.WriteLine(hero.id);
                    sw.WriteLine(hero.name);
                    sw.WriteLine(hero.material_portrait);
                    sw.WriteLine(hero.material_character);
                    sw.WriteLine(hero.attacktype);
                    sw.WriteLine(hero.base_defence);
                    sw.WriteLine(hero.base_energy);
                    sw.WriteLine(hero.base_health);
                    sw.WriteLine(hero.base_magic);
                    sw.WriteLine(hero.base_movement);
                    sw.WriteLine(hero.base_resist);
                    sw.WriteLine(hero.base_strength);
                    sw.WriteLine(hero.base_turning);
                    sw.WriteLine(hero.gain_defence);
                    sw.WriteLine(hero.gain_energy);
                    sw.WriteLine(hero.gain_health);
                    sw.WriteLine(hero.gain_magic);
                    sw.WriteLine(hero.gain_movement);
                    sw.WriteLine(hero.gain_resist);
                    sw.WriteLine(hero.gain_strength);
                    sw.WriteLine(hero.gain_turning);

                }

                sw.Close();

            }

        }

        private void loadToolStripMenuItem_Click(object sender, EventArgs e)
        {

            OpenFileDialog save = new OpenFileDialog();

            save.Filter = "Hero Files|*.hero";

            DialogResult res = save.ShowDialog();

            if (res == System.Windows.Forms.DialogResult.OK)
            {

                System.IO.StreamReader sw = new System.IO.StreamReader(save.FileName);

                int count = Int32.Parse(sw.ReadLine());

                dataGridView1.DataSource = null;


                for (int i = 0; i < count; i++)
                {
                    Hero hero = heroes.AddNew();
                    hero.id = Int32.Parse(sw.ReadLine());
                    hero.name = sw.ReadLine();
                    hero.material_portrait = sw.ReadLine();
                    hero.material_character = sw.ReadLine();
                    hero.attacktype = (HeroAttackType)Enum.Parse(typeof(HeroAttackType), sw.ReadLine());
                    hero.base_defence = Single.Parse(sw.ReadLine());
                    hero.base_energy = Single.Parse(sw.ReadLine());
                    hero.base_health = Single.Parse(sw.ReadLine());
                    hero.base_magic = Single.Parse(sw.ReadLine());
                    hero.base_movement = Single.Parse(sw.ReadLine());
                    hero.base_resist = Single.Parse(sw.ReadLine());
                    hero.base_strength = Single.Parse(sw.ReadLine());
                    hero.base_turning = Single.Parse(sw.ReadLine());
                    hero.gain_defence = Single.Parse(sw.ReadLine());
                    hero.gain_energy = Single.Parse(sw.ReadLine());
                    hero.gain_health = Single.Parse(sw.ReadLine());
                    hero.gain_magic = Single.Parse(sw.ReadLine());
                    hero.gain_movement = Single.Parse(sw.ReadLine());
                    hero.gain_resist = Single.Parse(sw.ReadLine());
                    hero.gain_strength = Single.Parse(sw.ReadLine());
                    hero.gain_turning = Single.Parse(sw.ReadLine());
                }

                dataGridView1.DataSource = heroes;

                sw.Close();
            }
        }

        public void Action(IPluginContext context)
        {
            this.Show();
        }
    }
}
