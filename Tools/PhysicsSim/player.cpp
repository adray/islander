#include "player.h"
#include "Vector.h"

#define _USE_MATH_DEFINES
#include <cmath>
#include <math.h>

void ComputeDesiredDirection(Player& player, PlayerInput& input, float& desiredX, float& desiredZ)
{
    float dirX = input.inputH;
    float dirY = 0;
    float dirZ = input.inputV;

    player.magnitude = std::sqrtf(dirX * dirX + dirZ * dirZ);

    Islander::Numerics::Normalize(dirX, dirY, dirZ, dirX, dirY, dirZ);

    float forward[3] = {
        0,//sim.camera.lookat[0] - sim.camera.pos[0],
        0,
        1//sim.camera.lookat[2] - sim.camera.pos[2]
    };

    float right[3] = {
        1,//sim.camera.lookat[2] - sim.camera.pos[2],
        0,
        0//sim.camera.lookat[0] - sim.camera.pos[0]
    };


    desiredX = forward[0] * dirX + right[0] * dirZ;
    desiredZ = forward[2] * dirX + right[2] * dirZ;
}

void OnPlayerInput(Player& player, PlayerInput& input)
{
    float desiredX;
    float desiredZ;

    ComputeDesiredDirection(player, input, desiredX, desiredZ);

    /*bool hasDash = !player.dashing && input.dash_KeyDown && !sim.player.falling &&
        (!sim.player.controller.dash.dashOnCooldown ||
            (sim.player.controller.dash.dashOnCooldown && sim.player.controller.dash.dashCooldown >= (float)sim.config.PlayerDashCooldown && sim.player.manaPoints >= (int)sim.config.PlayerDashMana))
        && !sim.player.controller.dash.dashInputReset;*/
    //bool hasEvade = !sim.player.controller.evading && input.parry_KeyDown && !sim.player.falling &&
    //    ((sim.player.controller.evadeOnCooldown && sim.player.controller.evadeCooldown >= 
    //        (float)sim.config.EvadeCooldown) || !sim.player.controller.evadeOnCooldown);

    bool updateInputs = false;

    bool isCurrentlyStill = !player.isMoving;
    bool isStopping = !((fabs(desiredX) + fabs(desiredZ)) > 0);// && !hasDash;

    if (isStopping || isCurrentlyStill /*|| hasDash || hasEvade*/)
    {
        // Start / Stop the character if the inputs were previously blank or become blank.

//        bool startEvading = hasEvade && !sim.player.controller.evading && isCurrentlyStill;
//        if (startEvading)
//        {
//            sim.player.controller.evadeCooldown = 0.0f;
//            sim.player.controller.evadeOnCooldown = true;
//
//            if (IsPerfectParry(sim))
//            {
//#ifdef DEBUG_LOGGING
//                DebugLog(sim, "Perfect Evade" /*: "Evade!"*/);
//#endif
//
//                if (sim.player.controller.dashTrailEffect >= 0)
//                {
//                    IslanderStopParticleEffect(sim.particle, sim.player.controller.dashTrailEffect);
//                }
//
//                sim.player.controller.dashTrailEffect = PlayAttackEffect(sim, sim.debug.darkDashEffect, sim.player.entity.transform);
//
//                sim.player.controller.evading = true;
//            }
//        }

//        bool startDashing = hasDash && !sim.player.controller.dash.dashing;
//        if (startDashing)
//        {
//            MakeNullable(sim.player.controller.bufferedInput);
//            sim.player.manaPoints -= (int)sim.config.PlayerDashMana;
//
//            if (sim.player.controller.effects.dashTrailEffect >= 0)
//            {
//                IslanderStopParticleEffect(sim.particle, sim.player.controller.effects.dashTrailEffect);
//            }
//
//            sim.player.controller.effects.dashTrailEffect = PlayAttackEffect(sim, sim.debug.darkDashEffect, sim.player.entity.transform);
//            sim.player.controller.dash.dashing = true;
//            sim.player.controller.dash.dashElapsed = 0.0f;
//            sim.player.controller.dash.dashCooldown = 0.0f;
//            sim.player.controller.dash.dashOnCooldown = false;
//
//            ApplyDashBomb(sim);
//
//#ifdef PLAYER_CONTROLLER_SPEED_LOGGING
//            DebugLog(sim, "Begin Dashing");
//#endif
//        }

        bool startAccelerating = !player.accelerating && isCurrentlyStill && !isStopping && !player.evading;
        if (startAccelerating)
        {
            updateInputs = true;
            player.lerp = false; // snap to the direction we are moving.
            player.accelerating = true;
            player.accelerationElapsed = 0;
#ifdef PLAYER_CONTROLLER_SPEED_LOGGING
            DebugLog(sim, "Begin Accelerating");
#endif
        }

        bool startDecelerating = !player.decelerating && isStopping && !isCurrentlyStill && !player.evading;
        if (startDecelerating)
        {
            player.decelerating = true;
            player.decelerationElapsed = 0;
#ifdef PLAYER_CONTROLLER_SPEED_LOGGING
            DebugLog(sim, "Begin Decelerating");
#endif
        }
    }
    else
    {
        // Continued input.

        player.lerp = true; // rotate towards where we are facing, rather than snapping.
        updateInputs = true;
    }

    if (updateInputs)
    {
        // Store the desired direction - which should be updated if we are moving or just started moving.
        player.desiredX = desiredX;
        player.desiredZ = desiredZ;
    }

    player.isMoving = !isStopping && !player.evading;
}


bool LookAt(float& dirX, float& dirZ, float rotationSpeed, float desiredDirX, float desiredDirZ)
{
    bool done = false;
    float angle = (float)atan2(dirZ, dirX);
    float desiredAngle = (float)atan2(desiredDirZ, desiredDirX);

    if (abs(angle + M_PI) < 0.001f)
    {
        angle = (float)M_PI;
    }

    if (abs(desiredAngle + M_PI) < 0.001f)
    {
        desiredAngle = (float)M_PI;
    }

    float proj = desiredAngle;
    if (abs(angle - desiredAngle) > rotationSpeed)
    {
        float dist1 = abs(desiredAngle - angle);
        float dist2 = abs(-(float)M_PI - std::min(angle, desiredAngle)) + abs((float)M_PI - std::max(angle, desiredAngle));

        if (dist1 < dist2)
        {
            proj = angle > desiredAngle ? angle - rotationSpeed : angle + rotationSpeed;
        }
        else
        {
            proj = angle < desiredAngle ? angle - rotationSpeed : angle + rotationSpeed;
        }
    }
    else
    {
        done = true;
    }

    dirX = (float)cosf(proj);
    dirZ = (float)sinf(proj);

    float projLength = (float)sqrtf(dirX * dirX + dirZ * dirZ);
    dirX = dirX / projLength;
    dirZ = dirZ / projLength;

    return done; // returns at desired direction
}

void PlayerUpdate(Player& player, PlayerConfig& cfg, float delta, float* velocity)
{
    float& dirX = player.dirX, & dirY = player.dirY, & dirZ = player.dirZ;

    Islander::Numerics::Normalize(dirX, dirY, dirZ, dirX, dirY, dirZ);
    Islander::Numerics::Normalize(player.desiredX, player.desiredY, player.desiredZ, player.desiredX, player.desiredY, player.desiredZ);

    if (player.lerp)
    {
        if (player.isMoving || player.decelerating /*|| player.attackSnap*/)
        {
            // Rotate towards desired direction.
            // When we have stopped moving we no longer want to move to the last direction.
            // e.g.
            // Frame N-2: they have 'W' and 'A' keys down 
            // Frame N-1: they have 'W' down
            // Frame N: No keys down
            //
            // We don't want to continue rotating towards 'W'.
            //

            const float rotationSpeed = (float)cfg.PlayerRotationSpeed;

            LookAt(dirX, dirZ, rotationSpeed * delta, player.desiredX, player.desiredZ);
        }
    }
    //else if (sim.player.controller.dash.dashing)
    //{
    //    // When dashing from a stationary position move in the direction currectly facing
    //    dirX = cosf(-sim.player.entity.transform.dy + M_PI * 3 / 2);
    //    dirZ = sinf(-sim.player.entity.transform.dy + M_PI * 3 / 2);
    //}
    else if (player.isMoving)
    {
        // Snap to desired direction.
        dirX = player.desiredX;
        dirZ = player.desiredZ;
    }

    //auto& transform = sim.player.entity.transform;
    //transform.dy = atan2(-sim.player.controller.dirZ, sim.player.controller.dirX) + M_PI * 3 / 2; // TODO: M_PI * 3 / 2 is 270 degrees which is the rotation applied in the scene file
    float deltaRotationY = atan2(-player.dirZ, player.dirX) + M_PI * 3 / 2 - player.dy;// TODO: M_PI * 3 / 2 is 270 degrees which is the rotation applied in the scene file

    //if (sim.player.controller.effects.attackTrailEffect != -1)
    //{
    //    float pos[3];
    //    CalculatePose(sim, pos);

    //    IslanderSetLocationParticleEffect(sim.particle, sim.player.controller.effects.attackTrailEffect, pos[0], pos[1], pos[2]);
    //}

    float dx = 0;
    float dz = 0;

    if (player.isMoving || player.decelerating)
    {
        dx = dirX;
        dz = dirZ;
    }

    float s = player.magnitude > (float)cfg.PlayerRunThreshold ? (float)cfg.PlayerRunSpeed : (float)cfg.PlayerSpeed;

    float speed = s;
//    if (sim.player.controller.dash.dashing)
//    {
//        speed = (float)sim.config.PlayerDashSpeed;
//
//        sim.player.controller.bomb.dashBombCooldown += delta;
//        ApplyDashBomb(sim);
//
//        sim.player.controller.dash.dashElapsed += delta;
//        if (sim.player.controller.dash.dashElapsed >= (float)sim.config.PlayerDashTime)
//        {
//            sim.player.manaDashAccumulator += (int)sim.config.PlayerDashMana * delta * 4.0f;
//            int dashMana = (int)sim.player.manaDashAccumulator;
//            sim.player.manaPoints = std::max(0, sim.player.manaPoints - dashMana);
//            sim.player.manaDashAccumulator -= dashMana;
//        }
//
//        IslanderSetLocationParticleEffect(sim.particle, sim.player.controller.effects.dashTrailEffect,
//            sim.player.entity.transform.px,
//            sim.player.entity.transform.py,
//            sim.player.entity.transform.pz);
//
//        if (sim.player.manaPoints == 0 || !input.dash_KeyDown)
//        {
//            sim.player.controller.dash.dashInputReset = sim.player.manaPoints == 0; // if out of mana need to release the input and press it again
//            sim.player.controller.dash.dashing = false;
//            sim.player.controller.dash.dashOnCooldown = true;
//#ifdef PLAYER_CONTROLLER_SPEED_LOGGING
//            DebugLog(sim, "End Dashing");
//#endif
//
//            sim.player.controller.effects.dashTrailEffect = -1;
//        }
//    }
    //else if (sim.player.controller.evading)
    //{
    //    dx = -cosf(-sim.player.entity.transform.dy + M_PI * 3 / 2);
    //    dz = -sinf(-sim.player.entity.transform.dy + M_PI * 3 / 2);

    //    IslanderSetLocationParticleEffect(sim.particle, sim.player.controller.effects.dashTrailEffect,
    //        sim.player.entity.transform.px,
    //        sim.player.entity.transform.py,
    //        sim.player.entity.transform.pz);

    //    speed = (float)sim.config.PlayerDashSpeed;
    //    sim.player.controller.evadeElapsed += delta;
    //    if (sim.player.controller.evadeElapsed >= (float)sim.config.EvadeLength)
    //    {
    //        sim.player.controller.evadeElapsed = 0.0f;
    //        sim.player.controller.evading = false;
    //        sim.player.controller.effects.dashTrailEffect = -1;
    //    }
    //}
    if (player.accelerating)
    {
        player.accelerationElapsed += delta;
        if (player.accelerationElapsed >= (float)cfg.PlayerAccelerationTime)
        {
            player.accelerating = false;
#ifdef PLAYER_CONTROLLER_SPEED_LOGGING
            DebugLog(sim, "End Accelerating");
#endif
        }
        else
        {
            speed = s * (player.accelerationElapsed / (float)cfg.PlayerAccelerationTime);
        }
    }
    else if (player.decelerating)
    {
        player.decelerationElapsed += delta;
        if (player.decelerationElapsed >= (float)cfg.PlayerDecelerationTime)
        {
            player.decelerating = false;
#ifdef PLAYER_CONTROLLER_SPEED_LOGGING
            DebugLog(sim, "End Decelerating");
#endif
        }
        else
        {
            speed = s * (((float)cfg.PlayerDecelerationTime - player.decelerationElapsed) / (float)cfg.PlayerDecelerationTime);
        }
    }

    velocity[0] = dx * speed;// *delta;
    velocity[2] = dz * speed;// *delta;
}
