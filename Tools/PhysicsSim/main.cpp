#include "main.h"
#include "API.h"
#include "Vector.h"
#include "Keys.h"
#include "player.h"

//#define VOLT

#ifdef VOLT
#include "JoltPhysics.h"
#include "Matrix.h"
#define MAX_BODY_COUNT 128

struct PhysicsState2
{
	IslanderPhysicsBody colliders[MAX_BODY_COUNT];
	void* pState;
	int bodyCount;
};

#else
#define NATIVE_PHYSICS
#include "psim.h"
#endif

#define _USE_MATH_DEFINES
#include <math.h>


#ifdef VOLT

void PhysicsTest3(PhysicsState2& state)
{
	const int spheres = 20;
	state.bodyCount = spheres + 3;
	for (int i = 0; i < spheres; i++)
	{
		IslanderPhysicsBody& pBody = state.colliders[i];
		auto& s = pBody.sphere;

		pBody.shape = ISLANDER_COLLISION_SHAPE_SPHERE;
		s.centreX = 0;
		s.centreY = 0;
		s.centreZ = 0;
		s.radius = 1;

		//state.colliders[i].rb.velocity[0] = 0.0f;
		//state.colliders[i].rb.velocity[1] = 0.0f;
		//state.colliders[i].rb.velocity[2] = 0.0f;

		//state.colliders[i].rb.angularVelocity[0] = 0.0f;
		//state.colliders[i].rb.angularVelocity[1] = 0.0f;
		//state.colliders[i].rb.angularVelocity[2] = 0.0f;

		pBody.pos[0] = 10.0f;
		pBody.pos[1] = 20 + i * 10.0f;
		pBody.pos[2] = 10.0f;

		pBody.rot[0] = 0.0f;
		pBody.rot[1] = 0.0f;
		pBody.rot[2] = 0.0f;

		//state.colliders[i].scale[0] = 1.0f;
		//state.colliders[i].scale[1] = 1.0f;
		//state.colliders[i].scale[2] = 1.0f;

		//state.colliders[i].rb.impulse[0] = 0.0f;
		//state.colliders[i].rb.impulse[1] = 0.0f;
		//state.colliders[i].rb.impulse[2] = 0.0f;

		//state.colliders[i].rb.mass = 0.1f;
		//state.colliders[i].rb.is_static = false;
		//state.colliders[i].rb.is_character = false;

		pBody.is_character = false;
		pBody.body_type = ISLANDER_PHYSICS_BODY_TYPE_DYNAMIC;
		pBody.mass = 0.1f;

		Islander::Physics::AddJoltPhysicsBody(state.pState, pBody);
	}

	{
		IslanderPhysicsBody& ground = state.colliders[spheres];
		ground.box.maxx = 0.5f;
		ground.box.minx = -0.5f;
		ground.box.maxy = 0.5f;
		ground.box.miny = -0.5f;
		ground.box.minz = -0.5f;
		ground.box.maxz = 0.5f;
		ground.body_type = ISLANDER_PHYSICS_BODY_TYPE_STATIC;
		ground.is_character = false;
		ground.pos[0] = 0.0f; ground.pos[1] = 1.0f; ground.pos[2] = 0.0f;
		ground.rot[0] = ground.rot[1] = ground.rot[2] = 0.0f;
		ground.scale[0] = 200.0f; ground.scale[1] = 2.0f; ground.scale[2] = 100.0f;
		state.colliders[spheres].shape = ISLANDER_COLLISION_SHAPE_BOX;
		// Ground inverse would be all zeros (if it has assumed infinite mass)

		Islander::Physics::AddJoltPhysicsBody(state.pState, ground);
	}

	{
		IslanderPhysicsBody& platform = state.colliders[spheres + 1];
		platform.box.maxx = 0.5f;
		platform.box.minx = -0.5f;
		platform.box.maxy = 0.5f;
		platform.box.miny = -0.5f;
		platform.box.minz = -0.5f;
		platform.box.maxz = 0.5f;
		platform.body_type = ISLANDER_PHYSICS_BODY_TYPE_KINEMATIC;
		platform.is_character = false;
		platform.pos[0] = 25.0f; platform.pos[1] = 0.0f; platform.pos[2] = 60.0f;
		platform.rot[0] = platform.rot[1] = platform.rot[2] = 0.0f;
		platform.scale[0] = 20.0f; platform.scale[1] = 2.0f; platform.scale[2] = 20.0f;
		state.colliders[spheres].shape = ISLANDER_COLLISION_SHAPE_BOX;

		Islander::Physics::AddJoltPhysicsBody(state.pState, platform);
	}

	{
		IslanderPhysicsBody& player = state.colliders[spheres + 2];
		player.box.maxx = 0.5f;
		player.box.minx = -0.5f;
		player.box.maxy = 0.5f;
		player.box.miny = -0.5;
		player.box.minz = -0.5f;
		player.box.maxz = 0.5f;

		player.pos[0] = 7.0f;
		player.pos[1] = 6.0f;
		player.pos[2] = 7.0f;

		player.rot[0] = 0.0f;
		player.rot[1] = 0.0f;
		player.rot[2] = 0.0f;

		player.scale[0] = 2.0f;
		player.scale[1] = 2.0f;
		player.scale[2] = 2.0f;

		player.mass = 50.f;
		player.is_character = true;

		state.colliders[spheres + 1].shape = ISLANDER_COLLISION_SHAPE_BOX;
		Islander::Physics::AddJoltPhysicsBody(state.pState, player);
	}
}

#else
void PhysicsTest3(PhysicsState& state)
{
	const int spheres = 20;
	state.bodyCount = spheres+2;
	for (int i = 0; i < spheres; i++)
	{
		state.colliders[i].type = ISLANDER_COLLISION_SHAPE_SPHERE;
		state.colliders[i].s.centreX = 0;
		state.colliders[i].s.centreY = 0;
		state.colliders[i].s.centreZ = 0;
		state.colliders[i].s.radius = 1;

		state.colliders[i].rb.velocity[0] = 0.0f;
		state.colliders[i].rb.velocity[1] = 0.0f;
		state.colliders[i].rb.velocity[2] = 0.0f;

		state.colliders[i].rb.angularVelocity[0] = 0.0f;
		state.colliders[i].rb.angularVelocity[1] = 0.0f;
		state.colliders[i].rb.angularVelocity[2] = 0.0f;

		state.colliders[i].position[0] = 10.0f;
		state.colliders[i].position[1] = 20 + i * 10.0f;
		state.colliders[i].position[2] = 10.0f;

		state.colliders[i].rotation[0] = 0.0f;
		state.colliders[i].rotation[1] = 0.0f;
		state.colliders[i].rotation[2] = 0.0f;

		state.colliders[i].scale[0] = 1.0f;
		state.colliders[i].scale[1] = 1.0f;
		state.colliders[i].scale[2] = 1.0f;

		state.colliders[i].rb.impulse[0] = 0.0f;
		state.colliders[i].rb.impulse[1] = 0.0f;
		state.colliders[i].rb.impulse[2] = 0.0f;

		state.colliders[i].rb.mass = 0.1f;
		state.colliders[i].rb.is_static = false;
		state.colliders[i].rb.is_character = false;
	}

	{
		Collider& ground = state.colliders[spheres];
		ground.b.maxx = 0.5f;
		ground.b.minx = -0.5f;
		ground.b.maxy = 0.5f;
		ground.b.miny = -0.5f;
		ground.b.minz = -0.5f;
		ground.b.maxz = 0.5f;
		ground.rb.is_static = true;
		ground.rb.is_character = false;
		ground.position[0] = 0.0f; ground.position[1] = 1.0f; ground.position[2] = 0.0f;
		ground.rotation[0] = ground.rotation[1] = ground.rotation[2] = 0.0f;
		ground.scale[0] = 200.0f; ground.scale[1] = 2.0f; ground.scale[2] = 100.0f;
		state.colliders[spheres].type = ISLANDER_COLLISION_SHAPE_BOX;
		// Ground inverse would be all zeros (if it has assumed infinite mass)
		//CreateInverseInertiaMatrixBox(std::numeric_limits<float>::max(), ground, &state.groundInverseInertia);
	}

	{
		Collider& player = state.colliders[spheres + 1];
		player.b.maxx = 0.5f;
		player.b.minx = -0.5f;
		player.b.maxy = 0.5f;
		player.b.miny = -0.5;
		player.b.minz = -0.5f;
		player.b.maxz = 0.5f;

		player.rb.velocity[0] = 0.0f;
		player.rb.velocity[1] = 0.0f;
		player.rb.velocity[2] = 0.0f;

		player.rb.impulse[0] = 0.f;
		player.rb.impulse[1] = 0.f;
		player.rb.impulse[2] = 0.f;

		player.rb.angularVelocity[0] = 0.0f;
		player.rb.angularVelocity[1] = 0.0f;
		player.rb.angularVelocity[2] = 0.0f;

		player.position[0] = 7.0f;
		player.position[1] = 6.0f;
		player.position[2] = 7.0f;

		player.rotation[0] = 0.0f;
		player.rotation[1] = 0.0f;
		player.rotation[2] = 0.0f;

		player.scale[0] = 2.0f;
		player.scale[1] = 2.0f;
		player.scale[2] = 2.0f;

		player.rb.mass = 50.f;
		player.rb.is_static = false;
		player.rb.is_character = true;

		state.colliders[spheres + 1].type = ISLANDER_COLLISION_SHAPE_BOX;
	}
}
#endif

struct GeometryData
{
	int strideSize;
	int vertexCount;
	float* vertexData;
	int indexCount;
	int* indexData;
};

void CreateBoxMesh(GeometryData* gData)
{
	float maxx = 1.f*0.5f;
	float minx = -1.f * 0.5f;
	float maxy = 1.f * 0.5f;
	float miny = -1.f * 0.5f;
	float maxz = 1.f * 0.5f;
	float minz = -1.f * 0.5f;

	gData->indexCount = 3 * 12;
	gData->vertexCount = 8;
	gData->strideSize = 9 * sizeof(float);
	gData->vertexData = new float[8 * 9];
	gData->indexData = new int32_t[gData->indexCount];

	float data[] =
	{
		maxx, maxy, maxz, 0, 0, 0, 1, 1, 1,
		maxx, miny, maxz, 0, 0, 0, 1, -1, 1,
		maxx, maxy, minz, 0, 0, 0, 1, 1, -1,
		minx, maxy, minz, 0, 0, 0, -1, 1, -1,
		maxx, miny, minz, 0, 0, 0, 1, -1, -1,
		minx, miny, maxz, 0, 0, 0, -1, -1, 1,
		minx, maxy, maxz, 0, 0, 0, -1, 1, 1,
		minx, miny, minz, 0, 0, 0, -1, -1, -1,
	};

	memcpy(gData->vertexData, data, sizeof(data));

	int32_t indexData[] =
	{
		7, 3, 2,
		7, 2, 4,
		5, 6, 0,
		5, 0, 1,
		3, 6, 0,
		3, 0, 2,
		7, 5, 1,
		7, 1, 4,
		4, 2, 0,
		4, 2, 1,
		7, 3, 5,
		6, 3, 5
	};

	memcpy(gData->indexData, indexData, sizeof(indexData));
}

void CreateSphereMesh(GeometryData* gData)
{
	const int slices = 12;
	const int stacks = 12;

	int sphere_vertex_count = (stacks - 1) * slices + 2;
	int sphere_index_count = slices * 6 + (stacks - 2) * slices * 6;

	gData->strideSize = 9 * sizeof(float);
	gData->vertexCount = sphere_vertex_count;
	gData->vertexData = new float[gData->vertexCount * 9];
	gData->indexCount = sphere_index_count;
	gData->indexData = new int[gData->indexCount];

	memset(gData->vertexData, 0, (unsigned long long)gData->vertexCount * 9 * sizeof(float));

	// Sphere equations
	// x = p * sin(theta) * cos(phi)
	// y = p * sin(theta) * sin(phi)
	// z = p * cos(phi)
	// where p is radius

	int vertexOffset = 0;

	float radius = 1.0f;

	gData->vertexData[0] = 0.0f;// sphere.centreX;
	gData->vertexData[1] = radius;// sphere.centreY + sphere.radius; // top vertex
	gData->vertexData[2] = 0.0f;// sphere.centreZ;
	vertexOffset += 9;

	for (int j = 0; j < stacks - 1; j++)
	{
		float theta = M_PI * (j + 1) / stacks;

		for (int i = 0; i < slices; i++)
		{
			float phi = M_PI * 2 * i / slices;
			float dirX = sinf(theta) * cosf(phi);
			float dirY = cosf(theta);
			float dirZ = sinf(theta) * sinf(phi);

			gData->vertexData[vertexOffset] = dirX * radius;
			gData->vertexData[vertexOffset + 1] = dirY * radius;
			gData->vertexData[vertexOffset + 2] = dirZ * radius;
			vertexOffset += 9;
		}
	}

	gData->vertexData[vertexOffset] = 0.0f;
	gData->vertexData[vertexOffset + 1] = radius; // bottom
	gData->vertexData[vertexOffset + 2] = 0.0f;
	int indexOffset = 0;

	// top/bottom tris
	for (int i = 0; i < slices; i++)
	{
		int i0 = i + 1;
		int i1 = (i + 1) % slices + 1;
		gData->indexData[indexOffset] = 0;
		gData->indexData[indexOffset + 1] = i1;
		gData->indexData[indexOffset + 2] = i0;

		int i2 = i + slices * (stacks - 2) + 1;
		int i3 = (i + 1) % slices + slices * (stacks - 2) + 1;
		gData->indexData[indexOffset + 3] = sphere_vertex_count - 1;
		gData->indexData[indexOffset + 4] = i2;
		gData->indexData[indexOffset + 5] = i3;
		indexOffset += 6;
	}

	// Add tris per stacks/slices
	for (int j = 0; j < stacks - 2; j++)
	{
		int j0 = j * slices + 1;
		int j1 = (j + 1) * slices + 1;

		for (int i = 0; i < slices; i++)
		{
			int i0 = j0 + i;
			int i1 = j0 + (i + 1) % slices;
			int i2 = j1 + (i + 1) % slices;
			int i3 = j1 + i;

			gData->indexData[indexOffset] = i0;
			gData->indexData[indexOffset + 1] = i1;
			gData->indexData[indexOffset + 2] = i2;
			gData->indexData[indexOffset + 3] = i1;
			gData->indexData[indexOffset + 4] = i2;
			gData->indexData[indexOffset + 5] = i3;

			indexOffset += 6;
		}
	}
}

struct Shaders
{
	int pixelShader;
	int vertexShader;
	ISLANDER_POLYGON_DATA sphereData;
	ISLANDER_POLYGON_DATA boxData;
};

#define LIGHT_CAMERA_WIDTH 1024
#define LIGHT_CAMERA_HEIGHT 1024
#define LIGHT_CAMERA_NEAR_PLANE 0.0f
#define LIGHT_CAMERA_FAR_PLANE 100.0f

void UpdateLighting(float* camPos, float* lightDir, ISLANDER_CAMERA3D camera, float* viewProjData, ISLANDER_WINDOW window)
{
	float dirX;
	float dirY;
	float dirZ;

	Islander::Numerics::Normalize(lightDir[0], lightDir[1], lightDir[2], dirX, dirY, dirZ);

	float dist = 100;
	float x = camPos[0] - dirX * dist;
	float y = camPos[1] - dirY * dist;
	float z = camPos[2] - dirZ * dist;

	//IslanderSetCamera3DTranslation(camera, x, y, z);
	//IslanderSetCamera3DLookAt(camera, camPos[0], camPos[1], camPos[2]);

	Islander::Numerics::Matrix4x4 view(Islander::Numerics::Matrix4x4Type::MATRIX_4X4_ZERO);
	Islander::Numerics::Matrix4x4::CreateViewMatrix(x, y, z, camPos[0], camPos[1], camPos[2], &view);

	Islander::Numerics::Matrix4x4 proj(Islander::Numerics::Matrix4x4Type::MATRIX_4X4_ZERO);
	Islander::Numerics::Matrix4x4::CreateOrthographicMatrix(LIGHT_CAMERA_WIDTH, LIGHT_CAMERA_HEIGHT, LIGHT_CAMERA_NEAR_PLANE, LIGHT_CAMERA_FAR_PLANE, &proj);
	//Islander::Renderer::Matrix4x4::Multiply(proj, view, &sim.lightCamera.viewProj);

	//memcpy(sim.lightCamera.viewProjData, sim.lightCamera.viewProj.GetData(), sizeof(float) * 16);
	memcpy(viewProjData, view.GetData(), sizeof(float) * 16);
	memcpy(&viewProjData[16], proj.GetData(), sizeof(float) * 16);
	viewProjData[32] = dirX;
	viewProjData[33] = dirY;
	viewProjData[34] = dirZ;
	viewProjData[35] = LIGHT_CAMERA_NEAR_PLANE;
	viewProjData[36] = LIGHT_CAMERA_FAR_PLANE;
	viewProjData[37] = IslanderWindowWidth(window);
	viewProjData[38] = IslanderWindowHeight(window);

	viewProjData[39] = 0;
	viewProjData[40] = camPos[0];
	viewProjData[41] = camPos[1];
	viewProjData[42] = camPos[2];
}

#ifdef VOLT
void RenderScene(ISLANDER_DEVICE device, ISLANDER_CAMERA3D camera, Shaders& shaders, PhysicsState2& state)
{
	IslanderRenderable renderables[MAX_BODY_COUNT];

	for (int i = 0; i < state.bodyCount; i++)
	{
		if (state.colliders[i].shape == ISLANDER_COLLISION_SHAPE_SPHERE)
		{
			auto& c = state.colliders[i];
			Islander::Physics::RetrieveJoltPhysicsBody(state.pState, c);

			auto& s = c.sphere;
			IslanderRenderable& r = renderables[i];
			std::memset(&r, 0, sizeof(r));

			r.transform.pos[0] = c.pos[0];
			r.transform.pos[1] = c.pos[1];
			r.transform.pos[2] = c.pos[2];
			r.transform.scale[0] = s.radius;
			r.transform.scale[1] = s.radius;
			r.transform.scale[2] = s.radius;

			Islander::Numerics::CopyVec3(state.colliders[i].rot, r.transform.rot);

			r.mesh.pixelShader = shaders.pixelShader;
			r.mesh.vertexShader = shaders.vertexShader;
			r.mesh.polydata = shaders.sphereData;
			r.mesh.parentBone = -1;
			r.mesh.parentEntity = -1;

			float colour[4] = { 1.0f, 1.0f, 1.0f, 0.0f };
			r.mesh.constantBufferData = colour;
			r.mesh.constantBufferDataSize = sizeof(colour);
		}
		else if (state.colliders[i].shape == ISLANDER_COLLISION_SHAPE_BOX)
		{
			auto& c = state.colliders[i];
			Islander::Physics::RetrieveJoltPhysicsBody(state.pState, c);

			IslanderRenderable& r = renderables[i];
			std::memset(&r, 0, sizeof(r));
			auto& box = state.colliders[i].box;

			r.transform.pos[0] = c.pos[0];
			r.transform.pos[1] = c.pos[1];
			r.transform.pos[2] = c.pos[2];
			r.transform.scale[0] = c.scale[0];
			r.transform.scale[1] = c.scale[1];
			r.transform.scale[2] = c.scale[2];

			Islander::Numerics::CopyVec3(state.colliders[i].rot, r.transform.rot);

			r.mesh.pixelShader = shaders.pixelShader;
			r.mesh.vertexShader = shaders.vertexShader;
			r.mesh.polydata = shaders.boxData;
			r.mesh.parentBone = -1;
			r.mesh.parentEntity = -1;
			//ground.mesh.wireframe = 1;

			if (state.colliders[i].is_character)
			{
				float colour[4] = { 0.0f, 1.0f, 0.0f, 0.0f };
				r.mesh.constantBufferData = colour;
				r.mesh.constantBufferDataSize = sizeof(colour);
			}
			else if (state.colliders[i].body_type == ISLANDER_PHYSICS_BODY_TYPE_KINEMATIC)
			{
				float colour[4] = { 0.0f, 0.0f, 1.0f, 0.0f };
				r.mesh.constantBufferData = colour;
				r.mesh.constantBufferDataSize = sizeof(colour);
			}
			else
			{
				float colour[4] = { 1.0f, 0.0f, 0.0f, 0.0f };
				r.mesh.constantBufferData = colour;
				r.mesh.constantBufferDataSize = sizeof(colour);
			}
		}
	}

	IslanderRenderablePass pass;
	pass.renderableBegin = 0;
	pass.renderableCount = state.bodyCount;
    pass.camera = camera;

	IslanderRenderScene3D(device, &pass, 1, renderables);
}
#else
void RenderScene(ISLANDER_DEVICE device, ISLANDER_CAMERA3D camera, Shaders& shaders, PhysicsState& state)
{
	IslanderRenderable renderables[MAX_BODY_COUNT];

	for (int i = 0; i < state.bodyCount; i++)
	{
		if (state.colliders[i].type == ISLANDER_COLLISION_SHAPE_SPHERE)
		{
			auto& c = state.colliders[i];
			auto& s = c.s;
			IslanderRenderable& r = renderables[i];
			std::memset(&r, 0, sizeof(r));

			r.transform.pos[0] = c.position[0];
			r.transform.pos[1] = c.position[1];
			r.transform.pos[2] = c.position[2];
			r.transform.scale[0] = s.radius;
			r.transform.scale[1] = s.radius;
			r.transform.scale[2] = s.radius;

			Islander::Numerics::CopyVec3(state.colliders[i].rotation, r.transform.rot);

			r.mesh.pixelShader = shaders.pixelShader;
			r.mesh.vertexShader = shaders.vertexShader;
            r.mesh.geometryShader = -1;
			r.mesh.polydata = shaders.sphereData;
			r.mesh.parentBone = -1;
			r.mesh.parentEntity = -1;

			static float colour[4] = { 1.0f, 1.0f, 1.0f, 0.0f };
			r.mesh.constantBufferData = colour;
			r.mesh.constantBufferDataSize = sizeof(colour);
		}
		else if (state.colliders[i].type == ISLANDER_COLLISION_SHAPE_BOX)
		{
			auto& ground = renderables[i];
			std::memset(&ground, 0, sizeof(ground));
			auto& c = state.colliders[i];
			auto& box = state.colliders[i].b;

			ground.transform.pos[0] = c.position[0];
			ground.transform.pos[1] = c.position[1];
			ground.transform.pos[2] = c.position[2];
			ground.transform.scale[0] = c.scale[0];
			ground.transform.scale[1] = c.scale[1];
			ground.transform.scale[2] = c.scale[2];

			Islander::Numerics::CopyVec3(state.colliders[i].rotation, ground.transform.rot);

			ground.mesh.pixelShader = shaders.pixelShader;
			ground.mesh.vertexShader = shaders.vertexShader;
            ground.mesh.geometryShader = -1;
			ground.mesh.polydata = shaders.boxData;
			ground.mesh.parentBone = -1;
			ground.mesh.parentEntity = -1;
			//ground.mesh.wireframe = 1;

			if (state.colliders[i].rb.is_static)
			{
				static float colour[4] = { 1.0f, 0.0f, 0.0f, 0.0f };
				ground.mesh.constantBufferData = colour;
				ground.mesh.constantBufferDataSize = sizeof(colour);
			}
			else
			{
				static float colour[4] = { 0.0f, 1.0f, 0.0f, 0.0f };
				ground.mesh.constantBufferData = colour;
				ground.mesh.constantBufferDataSize = sizeof(colour);
			}
		}
	}

	IslanderRenderablePass pass;
	pass.renderableBegin = 0;
	pass.renderableCount = state.bodyCount;
    pass.camera = camera;

	IslanderRenderScene3D(device, &pass, 1, renderables);
}
#endif

void LoadShaders(Shaders& shader, ISLANDER_DEVICE device)
{
	constexpr int maxSemanticCount = 16;
	IslanderShaderSemantic shaderSemantic[maxSemanticCount];
	shaderSemantic[0]._desc = "POSITION";
	shaderSemantic[0]._format = ISLANDER_SEMANTIC_FLOAT3;
	shaderSemantic[0]._stream = 0;
	shaderSemantic[1]._desc = "TEXCOORD";
	shaderSemantic[1]._format = ISLANDER_SEMANTIC_FLOAT3;
	shaderSemantic[1]._stream = 0;
	shaderSemantic[2]._desc = "TEXCOORD";
	shaderSemantic[2]._format = ISLANDER_SEMANTIC_FLOAT3;
	shaderSemantic[2]._stream = 0;

	shader.pixelShader = IslanderLoadPixelShader(device, "..\\Mesh.fx", "MeshPixel");
	shader.vertexShader = IslanderLoadVertexShader(device, "..\\Mesh.fx", "MeshVertex", shaderSemantic, 3);
}

void HandleInput(ISLANDER_WINDOW window, PlayerInput& input)
{
	if (IslanderIsKeyDown(window, Islander::KEY_RIGHT))
	{
		input.inputH = -1.0f;
	}
	else if (IslanderIsKeyDown(window, Islander::KEY_LEFT))
	{
		input.inputH = 1.0f;
	}
	else
	{
		input.inputH = 0.0f;
	}

	if (IslanderIsKeyDown(window, Islander::KEY_UP))
	{
		input.inputV = 1.0f;
	}
	else if (IslanderIsKeyDown(window, Islander::KEY_DOWN))
	{
		input.inputV = -1.0f;
	}
	else
	{
		input.inputV = 0.0f;
	}
}

int main()
{
	auto window = IslanderCreateWindow();
	IslanderSetWindowSize(window, 1366, 768);

	auto device = IslanderCreateDevice();
	IslanderSetPreferredRenderer(device, (int)ISLANDER_RENDERER_TYPE_D3D11);
	IslanderFontDescription defaultFont;
	defaultFont.filedef = "../../../Resources/Font/Aleo/Aleo-Regular.ttf";
	defaultFont.name = "";
	IslanderInitializeDevice(device, window, defaultFont);

	constexpr int unlocked_fps = 0;
	constexpr int sixty_fps = 1;
	constexpr int thirty_fps = 2;
#if defined(DEBUG) && defined(THIRTY_FPS)
	IslanderSetSyncInterval(device, thirty_fps);
#else
	IslanderSetSyncInterval(device, sixty_fps);
#endif

	ISLANDER_PASS_LIST passes = IslanderCreatePassList();
	IslanderPassConfig passCfg;
	passCfg._flags = 0x1;
	passCfg._renderTarget = -1;
	IslanderAppendPassList(passes, passCfg);
	IslanderSetPassList(device, passes);

	Shaders shaders;
	LoadShaders(shaders, device);

	ISLANDER_FILE_LIBRARY fileLib = IslanderCreateFileLibrary();

	GeometryData sphereData;
	CreateSphereMesh(&sphereData);
	shaders.sphereData = IslanderAddPolyMeshData(fileLib,
		sphereData.vertexData,
		sphereData.indexData,
		sphereData.vertexCount, 
		sphereData.indexCount, 
		sphereData.strideSize,
		0x4 /* Copy the mesh data */ | 0x2 /* Generate AABB */);

	GeometryData boxData;
	CreateBoxMesh(&boxData);
	shaders.boxData = IslanderAddPolyMeshData(fileLib,
		boxData.vertexData,
		boxData.indexData,
		boxData.vertexCount,
		boxData.indexCount,
		boxData.strideSize,
		0x4 /* Copy the mesh data */ | 0x2 /* Generate AABB */);

	float camPos[3] = {-95.0f, 50.0f, 10.0f};
	float lightDir[3] = { 0.3f, -0.8f, 0.1f };

	ISLANDER_CAMERA3D camera =	IslanderCreateCamera3D();
	IslanderSetCamera3DLookAt(camera, 10.0f, 5.0f, 10.0f);
	IslanderSetCamera3DTranslation(camera, camPos[0], camPos[1], camPos[2]);
	IslanderSetCamera3DProjection(camera, 0.1f, 400.0f, M_PI_4, 1366.0f / 768.0f);

	Player p; std::memset(&p, 0, sizeof(p));
	PlayerInput pInput; std::memset(&pInput, 0, sizeof(pInput));
	PlayerConfig pConfig; std::memset(&pConfig, 0, sizeof(pConfig));

	pConfig.PlayerAccelerationTime = .06f;
	pConfig.PlayerDecelerationTime = .035f;
	pConfig.PlayerRotationSpeed = 5.6f;
	pConfig.PlayerRunSpeed = 22.0f;
	pConfig.PlayerRunThreshold = 0.75f;
	pConfig.PlayerSpeed = 11.0f;

#ifdef VOLT
	PhysicsState2* state = new PhysicsState2();
	state->pState = Islander::Physics::CreateJoltPhysics();
	PhysicsTest3(*state);
#else
	PhysicsState* state = new PhysicsState();
	PhysicsTest3(*state);
#endif

	float viewProjData[44];

	while (IslanderPumpWindow(window))
	{
		HandleInput(window, pInput);
		
#ifdef VOLT
		static float platformElapsed = 0.0f;
		static float platformDir = 1.0f;

		platformElapsed += 1.0f / 60.0f;
		if (platformElapsed > 2.0f)
		{
			platformDir *= -1.0f;
			platformElapsed = 0.0f;
		}

		auto& plat = state->colliders[state->bodyCount - 2];
		plat.velocity[1] = 20.0f * platformDir;
		Islander::Physics::ApplyJoltPhysicsVelocity(state->pState, plat);

		auto& pCol = state->colliders[state->bodyCount - 1];
		OnPlayerInput(p, pInput);
		PlayerUpdate(p, pConfig, 1 / 60.0f, pCol.velocity);
		Islander::Physics::ApplyJoltPhysicsVelocity(state->pState, pCol);

		Islander::Physics::RunJoltPhysics(state->pState, 1.0f / 60.0f);
#else
		auto& pCol = state->colliders[state->bodyCount - 1];
		OnPlayerInput(p, pInput);
		PlayerUpdate(p, pConfig, 1/60.0f, pCol.rb.velocity);

		RunPhysics(*state, 1/60.0f);
#endif

		UpdateLighting(camPos, lightDir, camera, viewProjData, window);
		IslanderSetPassConstantData(passes, 0, viewProjData, sizeof(float) * 44);
		RenderScene(device, camera, shaders, *state);

		IslanderPresent(device);
	}

    return 0;
}
