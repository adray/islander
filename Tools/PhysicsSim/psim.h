#pragma once
#include "Matrix.h"
#include "CollisionTypes.h"

struct ContactConstraint
{
	int index;
	int index2;
	float rA[3];
	float rB[3];
	float pA[3];
	float pB[3];
	float normal[3];
	float J[12];	// the jacobian vector of [-n, cross(-rA,n), n, cross(rB,n)]
	float V[12];	// the velocity vector of [V1, W1, V2, W2]
	float inverseMass1;
	float inverseMass2;
	float totalLambda;
	float penetration;
	Islander::Numerics::Matrix3x3 inverseInteria1;
	Islander::Numerics::Matrix3x3 inverseInteria2;
};

struct PositionConstraint
{
	int index;
};

struct RigidBody
{
	float impulse[3];
	float velocity[3];
	float angularVelocity[3];
	float mass;
	bool is_static;
	bool is_character;
	Islander::Numerics::Matrix3x3 inverseInteria;
};

struct Collider
{
	RigidBody rb;
	int type;
	union
	{
		IslanderCollisionSphere s;
		IslanderCollisionBox b;
		IslanderCollisionCapsule c;
	};	union
	{
		IslanderCollisionSphere s_t;
		IslanderCollisionBox b_t;
		IslanderCollisionCapsule c_t;
	};
	float position[3];
	float rotation[3];
	float scale[3];
	Islander::Numerics::Matrix4x4 transform;
};

#define MAX_BODY_COUNT 32
struct PhysicsState
{
	Collider colliders[MAX_BODY_COUNT];
	int bodyCount;

	ContactConstraint constraints[MAX_BODY_COUNT * 5];
	int constraintCount = 0;

	PositionConstraint posConstraints[5];
	int posConstraintCount = 0;
};

void CreateInverseInertiaMatrixSphere(float mass, float radius, Islander::Numerics::Matrix3x3* matrix);
void CreateInverseInertiaMatrixBox(float mass, IslanderCollisionBox& box, Islander::Numerics::Matrix3x3* matrix);
void RunPhysics(PhysicsState& state, float delta);
