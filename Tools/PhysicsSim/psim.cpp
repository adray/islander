#include "psim.h"

#include "Vector.h"
#include "EPA.h"
#include "glm\mat4x4.hpp"

using namespace Islander::Collision;

float ComputeContactConstraintBias(ContactConstraint& c, float dt)
{
	// Baumguarte stabilization
	// b = - (beta / dt) * d
	//
	// (for contact constraint)
	// d = (Pb - Pa) dot n 
	// 

	const float beta = 0.2f;
	float d = c.penetration;
	float b = -beta / dt * d;
	return b;
}

float ComputeEffectiveMass(ContactConstraint& c)
{
	// Meff = 1 / (J * M * transpose(J))
	// Meff = 1.0f / ( 1/mass1) + (cross(-rA,n) * I1 * cross(-rA,n)) + (1/mass2) + (cross(rB,n) * I2 * cross(rB,n))

	float meff1[3];
	Islander::Numerics::Matrix3x3::Multiply(&c.J[3], c.inverseInteria1, meff1);
	float meff2[3];
	Islander::Numerics::Matrix3x3::Multiply(&c.J[9], c.inverseInteria2, meff2);

	float k = c.inverseMass1 + Islander::Numerics::DotVec3(meff1, &c.J[3]) + c.inverseMass2 + Islander::Numerics::DotVec3(meff2, &c.J[9]);
	return 1.0f / k;
}

float ComputeContactConstraintLagrangianMultiplier(ContactConstraint& c, float dt)
{
	float bias = ComputeContactConstraintBias(c, dt); // bias

	// lambda = - (J * V + bias) * Meff

	float JV = 0.0f;
	for (int i = 0; i < 12; i++)
	{
		JV += c.J[i] * c.V[i];
	}

	float Meff = ComputeEffectiveMass(c);
	float lambda = -(JV + bias) * Meff;

	float oldTotalLambda = c.totalLambda;
	c.totalLambda = std::max(0.0f, c.totalLambda + lambda);
	lambda = c.totalLambda - oldTotalLambda;

	return lambda;
}

void CreateInverseInertiaMatrixSphere(float mass, float radius, Islander::Numerics::Matrix3x3* matrix)
{
	float inertia = (2.0f / 5.0f) * mass * radius * radius;

	float inverseInertia = 1.0f / inertia;

	Islander::Numerics::Matrix3x3::CreateScalingMatrix(inverseInertia, inverseInertia, inverseInertia, matrix);
}

void CreateInverseInertiaMatrixBox(float mass, float* scale, float* rotation, Islander::Numerics::Matrix3x3* matrix)
{
	// Inverse Inertia Tensor = inverse(R * T * transpose(R))

	Islander::Numerics::Matrix4x4 scaleMat(Islander::Numerics::Matrix4x4Type::MATRIX_4X4_IDENTITY);

	float w = scale[0];
	float h = scale[1];
	float d = scale[2];

	float inertiaX = (1.0f / 12.0f) * mass * (h * h + d * d);
	float inertiaY = (1.0f / 12.0f) * mass * (w * w + d * d);
	float inertiaZ = (1.0f / 12.0f) * mass * (w * w + h * h);

	Islander::Numerics::Matrix4x4::CreateScaleMatrix(inertiaX, inertiaY, inertiaZ, &scaleMat);

	Islander::Numerics::Matrix4x4 rotMat(Islander::Numerics::Matrix4x4Type::MATRIX_4X4_IDENTITY);
	Islander::Numerics::Matrix4x4::CreateTransformationMatrix(0,0,0, rotation[0], rotation[1], rotation[2], 1,1,1, &rotMat);

	Islander::Numerics::Matrix4x4 rotMatTranspose(Islander::Numerics::Matrix4x4Type::MATRIX_4X4_IDENTITY);
	Islander::Numerics::Matrix4x4::Transpose(rotMat, &rotMatTranspose);

	Islander::Numerics::Matrix4x4 mat4x4(Islander::Numerics::Matrix4x4Type::MATRIX_4X4_IDENTITY);
	Islander::Numerics::Matrix4x4::Multiply(rotMat, scaleMat, &mat4x4);
	Islander::Numerics::Matrix4x4::Multiply(scaleMat, rotMatTranspose, &mat4x4);

	const float* const data = mat4x4.GetData();

	glm::mat4x4 glmMat(data[0], data[1], data[2], data[3], data[4], data[5], data[6], data[7], data[8], data[9], data[10], data[11], data[12], data[13], data[14], data[15]);
	glm::mat4x4 inverse = glm::inverse(glmMat);
	
	float inverseData[9] = {
		inverse[0][0], inverse[0][1], inverse[0][2],
		inverse[1][0], inverse[1][1], inverse[1][2],
		inverse[2][0], inverse[2][1], inverse[2][2],
	};

	*matrix = Islander::Numerics::Matrix3x3(inverseData);
}

void GetShapeCentre(Collider& collider, float* pos)
{
	if (collider.type == ISLANDER_COLLISION_SHAPE_SPHERE)
	{
		pos[0] = collider.s_t.centreX;
		pos[1] = collider.s_t.centreY;
		pos[2] = collider.s_t.centreZ;
	}
	else if (collider.type == ISLANDER_COLLISION_SHAPE_BOX)
	{
		pos[0] = (collider.b_t.maxx - collider.b_t.minx) / 2 + collider.b_t.minx;
		pos[1] = (collider.b_t.maxy - collider.b_t.miny) / 2 + collider.b_t.miny;
		pos[2] = (collider.b_t.maxz - collider.b_t.minz) / 2 + collider.b_t.minz;
	}
	else if (collider.type == ISLANDER_COLLISION_SHAPE_CAPSULE)
	{
		pos[0] = collider.c_t.centreX;
		pos[1] = collider.c_t.centreY;
		pos[2] = collider.c_t.centreZ;
	}
}

void CreateContactConstraint(PhysicsState& state, int j, int k, float* normal, float* pt1, float* pt2, float depth)
{
	Collider* s = state.colliders;

	float pos1[3];
	float pos2[3];
	GetShapeCentre(s[j], pos1);
	GetShapeCentre(s[k], pos2);

	auto& con = state.constraints[state.constraintCount++];
	con.index = j;
	con.index2 = k;
	con.inverseMass1 = s[j].rb.is_static ? 0.0f : 1.0f / s[j].rb.mass;
	con.inverseMass2 = s[k].rb.is_static ? 0.0f : 1.0f / s[k].rb.mass;
	con.inverseInteria1 = s[j].rb.inverseInteria;
	con.inverseInteria2 = s[k].rb.inverseInteria;
	con.totalLambda = 0.0f;
	con.penetration = depth;

	Islander::Numerics::NegateVec3(normal, normal); // TODO: may need to invert the normal inside EPA
	Islander::Numerics::CopyVec3(normal, con.normal);

	Islander::Numerics::CopyVec3(pt1, con.pA);
	Islander::Numerics::CopyVec3(pt2, con.pB);

	con.rA[0] = pt1[0] - pos1[0];
	con.rA[1] = pt1[1] - pos1[1];
	con.rA[2] = pt1[2] - pos1[2];

	con.rB[0] = pt2[0] - pos2[0];
	con.rB[1] = pt2[1] - pos2[1];
	con.rB[2] = pt2[2] - pos2[2];

	// Calculate the jacobian
	con.J[0] = -normal[0];
	con.J[1] = -normal[1];
	con.J[2] = -normal[2];

	float negrA[3];
	Islander::Numerics::NegateVec3(con.rA, negrA);
	Islander::Numerics::CrossVec3(negrA, normal, &con.J[3]);

	con.J[6] = normal[0];
	con.J[7] = normal[1];
	con.J[8] = normal[2];

	Islander::Numerics::CrossVec3(con.rB, normal, &con.J[9]);
}

float TestCollisionSwept(PhysicsState& state, int index1, int index2, float* normal, float* velocity)
{
	float t = 1.0f;

	if (state.colliders[index1].type == ISLANDER_COLLISION_SHAPE_CAPSULE)
	{
		if (state.colliders[index2].type == ISLANDER_COLLISION_SHAPE_BOX)
		{
			t = GJK_Swept(state.colliders[index1].b_t, state.colliders[index2].s_t, normal, velocity);
		}
	}

	return t;
}

bool TestCollision(PhysicsState& state, int index1, int index2, float* normal, float* depth, float* pt1, float* pt2)
{
	bool collision = false;

	if (state.colliders[index1].type == ISLANDER_COLLISION_SHAPE_SPHERE)
	{
		if (state.colliders[index2].type == ISLANDER_COLLISION_SHAPE_SPHERE)
		{
			collision = GJK_EPA(state.colliders[index1].s_t, state.colliders[index2].s_t, normal, depth, pt1, pt2);
		}
		else if (state.colliders[index2].type == ISLANDER_COLLISION_SHAPE_BOX)
		{
			collision = GJK_EPA(state.colliders[index1].s_t, state.colliders[index2].b_t, normal, depth, pt1, pt2);
		}
	}
	else if (state.colliders[index1].type == ISLANDER_COLLISION_SHAPE_BOX)
	{
		if (state.colliders[index2].type == ISLANDER_COLLISION_SHAPE_SPHERE)
		{
			collision = GJK_EPA(state.colliders[index1].b_t, state.colliders[index2].s_t, normal, depth, pt1, pt2);
		}
		else if (state.colliders[index2].type == ISLANDER_COLLISION_SHAPE_BOX)
		{
			collision = GJK_EPA(state.colliders[index1].b_t, state.colliders[index2].b_t, normal, depth, pt1, pt2);
		}
	}
	else if (state.colliders[index1].type == ISLANDER_COLLISION_SHAPE_CAPSULE)
	{
		if (state.colliders[index2].type == ISLANDER_COLLISION_SHAPE_BOX)
		{
			collision = GJK_EPA(state.colliders[index1].b_t, state.colliders[index2].s_t, normal, depth, pt1, pt2);
		}
	}

	return collision;
}

void TestCollision(PhysicsState& state, int j)
{
	for (int k = 0; k < state.bodyCount; k++)
	{
		if (k == j)
		{
			continue;
		}

		//if (state.colliders[k].rb.is_character)
		//{
		//	continue;
		//}

		float normal[3]; float depth; float pt1[3]; float pt2[3];
		bool collision = TestCollision(state, j, k, normal, &depth, pt1, pt2);

		if (collision)
		{
			CreateContactConstraint(state, j, k, normal, pt1, pt2, depth);
		}
	}
}

void RunPhysics(PhysicsState& state, float delta)
{
	float gravity[3] = { 0.0f, -10.0f, 0.0f };

	// apply gravity and impulses
	for (int j = 0; j < state.bodyCount; j++)
	{
		auto& c = state.colliders[j];
		if (!c.rb.is_static)
		{
			c.rb.velocity[0] += gravity[0] * delta + c.rb.impulse[0] / c.rb.mass;
			c.rb.velocity[1] += gravity[1] * delta + c.rb.impulse[1] / c.rb.mass;
			c.rb.velocity[2] += gravity[2] * delta + c.rb.impulse[2] / c.rb.mass;
		}
	}

	// Calculate transforms
	for (int j = 0; j < state.bodyCount; j++)
	{
		auto& c = state.colliders[j];

		c.transform = Islander::Numerics::Matrix4x4(Islander::Numerics::Matrix4x4Type::MATRIX_4X4_IDENTITY);
		Islander::Numerics::Matrix4x4::CreateTransformationMatrix(
			c.position[0], c.position[1], c.position[2],
			c.rotation[0], c.rotation[1], c.rotation[2],
			c.scale[0], c.scale[1], c.scale[2], &c.transform);

		if (c.type == ISLANDER_COLLISION_SHAPE_SPHERE)
		{
			c.s_t = c.s;
			c.s_t.centreX += c.position[0];
			c.s_t.centreY += c.position[1];
			c.s_t.centreZ += c.position[2];
		}
		else if (c.type == ISLANDER_COLLISION_SHAPE_BOX)
		{
			c.b_t = c.b;
			Islander::Numerics::Matrix4x4::TransformVector(c.transform, &c.b_t.maxx, &c.b_t.maxy, &c.b_t.maxz);
			Islander::Numerics::Matrix4x4::TransformVector(c.transform, &c.b_t.minx, &c.b_t.miny, &c.b_t.minz);
		}
	}

	// Calculate inverse interia tensor
	for (int j = 0; j < state.bodyCount; j++)
	{
		auto& c = state.colliders[j];

		if (!c.rb.is_static)
		{
			if (c.type == ISLANDER_COLLISION_SHAPE_SPHERE)
			{
				CreateInverseInertiaMatrixSphere(c.rb.mass, c.s.radius, &c.rb.inverseInteria);
			}
			else if (c.type == ISLANDER_COLLISION_SHAPE_BOX)
			{
				//CreateInverseInertiaMatrixBox(c.rb.mass, c.b, &c.rb.inverseInteria);
				CreateInverseInertiaMatrixBox(c.rb.mass, c.scale, c.rotation, &c.rb.inverseInteria);
			}
		}
	}

	// Create contact constraints

	for (int j = 0; j < state.bodyCount; j++)
	{
		if (!state.colliders[j].rb.is_static)
		{
			TestCollision(state, j);
		}
	}

	// Solve velocity constraints

	const int iterations = 10;
	for (int it = 0; it < iterations; it++)
	{
		for (int j = 0; j < state.constraintCount; j++)
		{
			auto& c = state.constraints[j];

			// Calculate the V vector
			std::memset(c.V, 0, sizeof(c.V));
			c.V[0] = state.colliders[c.index].rb.velocity[0];
			c.V[1] = state.colliders[c.index].rb.velocity[1];
			c.V[2] = state.colliders[c.index].rb.velocity[2];
			c.V[3] = state.colliders[c.index].rb.angularVelocity[0];
			c.V[4] = state.colliders[c.index].rb.angularVelocity[1];
			c.V[5] = state.colliders[c.index].rb.angularVelocity[2];

			if (c.index2 != -1)
			{
				c.V[6] = state.colliders[c.index2].rb.velocity[0];
				c.V[7] = state.colliders[c.index2].rb.velocity[1];
				c.V[8] = state.colliders[c.index2].rb.velocity[2];
				c.V[9] = state.colliders[c.index2].rb.angularVelocity[0];
				c.V[10] = state.colliders[c.index2].rb.angularVelocity[1];
				c.V[11] = state.colliders[c.index2].rb.angularVelocity[2];
			}

			float l = ComputeContactConstraintLagrangianMultiplier(c, delta);

			// dV = inverse(M) * transpose(J) * L

			float vx = c.J[0] * l * c.inverseMass1;
			float vy = c.J[1] * l * c.inverseMass1;
			float vz = c.J[2] * l * c.inverseMass1;

			state.colliders[c.index].rb.velocity[0] += vx;
			state.colliders[c.index].rb.velocity[1] += vy;
			state.colliders[c.index].rb.velocity[2] += vz;

			float w[3];
			Islander::Numerics::Matrix3x3::Multiply(&c.J[3], c.inverseInteria1, w);
			w[0] *= l;
			w[1] *= l;
			w[2] *= l;

			state.colliders[c.index].rb.angularVelocity[0] += w[0];
			state.colliders[c.index].rb.angularVelocity[1] += w[1];
			state.colliders[c.index].rb.angularVelocity[2] += w[2];

			if (c.index2 != -1)
			{
				state.colliders[c.index2].rb.velocity[0] += c.J[6] * l * c.inverseMass2;
				state.colliders[c.index2].rb.velocity[1] += c.J[7] * l * c.inverseMass2;
				state.colliders[c.index2].rb.velocity[2] += c.J[8] * l * c.inverseMass2;

				float w2[3];
				Islander::Numerics::Matrix3x3::Multiply(&c.J[9], c.inverseInteria2, w2);
				w2[0] *= l;
				w2[1] *= l;
				w2[2] *= l;

				state.colliders[c.index2].rb.angularVelocity[0] += w2[0];
				state.colliders[c.index2].rb.angularVelocity[1] += w2[1];
				state.colliders[c.index2].rb.angularVelocity[2] += w2[2];
			}
		}
	}
	state.constraintCount = 0;

	// Apply velocity

	for (int j = 0; j < state.bodyCount; j++)
	{
		if (!state.colliders[j].rb.is_static)
		{
			float v[3] = {
				state.colliders[j].rb.velocity[0] * delta,
				state.colliders[j].rb.velocity[1] * delta,
				state.colliders[j].rb.velocity[2] * delta
			};

			float w[3] = {
				state.colliders[j].rb.angularVelocity[0] * delta,
				state.colliders[j].rb.angularVelocity[1] * delta,
				state.colliders[j].rb.angularVelocity[2] * delta
			};

			state.colliders[j].position[0] += v[0];
			state.colliders[j].position[1] += v[1];
			state.colliders[j].position[2] += v[2];

			state.colliders[j].rotation[0] += w[0];
			state.colliders[j].rotation[1] += w[1];
			state.colliders[j].rotation[2] += w[2];
		}
	}


	// Solve position constraints
	for (int it = 0; it < iterations; it++)
	{
		for (int j = 0; j < state.posConstraintCount; j++)
		{
			auto& c = state.posConstraints[j];

			auto& body = state.colliders[c.index];
			float minT = 1.0f;

			for (int i = 0; i < state.bodyCount; i++)
			{
				float normal[3];
				float t = TestCollisionSwept(state, c.index, i, normal, body.rb.velocity);
				if (t < 1.0f)
				{
					minT = std::min(t, minT);
				}
			}

			if (minT < 1.0f)
			{

			}
		}
	}	
}

