#pragma once

struct PlayerInput
{
    bool dash_KeyDown;
    float inputH;
    float inputV;
};

struct PlayerConfig
{
    float PlayerRotationSpeed;
    float PlayerRunThreshold;
    float PlayerRunSpeed;
    float PlayerSpeed;
    float PlayerAccelerationTime;
    float PlayerDecelerationTime;
};

struct Player
{
    bool isMoving;
    bool evading;
    bool lerp;
    bool accelerating;
    bool decelerating;
    float accelerationElapsed;
    float decelerationElapsed;
    float desiredX;
    float desiredY;
    float desiredZ;
    float dirX;
    float dirY;
    float dirZ;
    float magnitude;
    float dy;
};

void OnPlayerInput(Player& player, PlayerInput& input);
void PlayerUpdate(Player& player, PlayerConfig& cfg, float delta, float* velocity);
