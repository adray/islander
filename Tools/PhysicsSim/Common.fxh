#include "RenderConfig.fxh"

float4 ToonLighting(float3 normal, float3 lightDir, float3 viewPos, float3 worldPos)
{
    normal = normalize(normal);
    lightDir = normalize(lightDir);
    float dotN = dot(normal, -lightDir);
    float light = smoothstep(0, 0.01f, dotN);
    
    // TODO: expose variables
    float4 ambient = float4(0.4,0.4,0.4,1);
    float glossiness = 32;
    float rimAmount = 0.716f;
    float rimThreshold = 0.1f;
    float4 lightColor = float4(0.7f, 0.7f, 0.7f, 1.0f);
    float4 rimColor = float4(1,1,1,1);

    float3 viewDir = normalize(viewPos - worldPos);

    float3 reflectDir = normalize(reflect(-lightDir, normal));
    float specular = dot(reflectDir, viewDir);

    float4 color = light * lightColor + ambient;
    if (specular > 0)
    {
       color += pow(specular, glossiness * glossiness);
    }
    
    float4 rimDot = 1 - dot(viewDir, normal);
    float rimIntensity = smoothstep(rimAmount - 0.01f, rimAmount + 0.01f, rimDot * pow(dotN, rimThreshold));
    color += rimIntensity * rimColor;

    return color;
}

float SampleDepth(Texture2D displayShadowMap, SamplerState textureSampler, float2 depthTexCoord)
{
    // Percentage-closest filtering
    float totalDepth = 0;
    [unroll]
    for (int i = -2; i <= 2; i++) {
        [unroll]
        for (int j = -2; j <= 2; j++) {
            float2 offset = float2(i / SHADOW_MAP_WIDTH, j / SHADOW_MAP_HEIGHT);
            totalDepth += displayShadowMap.Sample(textureSampler, depthTexCoord + offset).r;
        }
    }
    totalDepth /= 25;
    return totalDepth;
}

float GetLuminance(float4 rgb)
{
    return sqrt(rgb.g * (0.587f/0.299f) + rgb.r);
}

float4 LightWithShadow(float4 light, float4 depth, Texture2D displayShadowMap, SamplerState textureSampler, float4 inColor)
{
    float4 outcolor;
    float2 depthTexCoord;
    depthTexCoord.x = depth.x / depth.w / 2 + 0.5f;
    depthTexCoord.y = -depth.y / depth.w / 2 + 0.5f;

    if((saturate(depthTexCoord.x) == depthTexCoord.x) && (saturate(depthTexCoord.y) == depthTexCoord.y))
    {
        float depthSample = SampleDepth(displayShadowMap, textureSampler, depthTexCoord);

        if (depth.z/depth.w - 0.001f < depthSample)
        {
            outcolor.rgb = inColor.rgb * light;
        }
        else
        {
            outcolor.rgb = .1f * inColor.rgb * light;
        }
    }
    else
    {
        outcolor.rgb = inColor.rgb * light;
    }

    outcolor.a = inColor.a;

    return outcolor;
}

