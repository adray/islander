#include "Common.fxh"

Texture2D display : register (t0);
Texture2D displayShadowMap : register (t1);

SamplerState textureSampler : register (s0);

cbuffer Constants : register (b0)
{
    float4x4 proj;
    float4x4 view;
    float4x4 world;
    float4x4 lightView;
    float4x4 lightProj;
    float3 lightDir;
    float nearPlane;
    float farPlane;
    float2 screenSize;
    float pad1;
    float3 viewPos;
    float1 pad2;
};

cbuffer Constants_Model : register (b1)
{
    float4 modelColour;
};

// DIFFUSE

float4 MeshVertex(in float3 position : POSITION, in float3 colour : TEXCOORD0, in float3 normal : TEXCOORD1,
	out float4 outColor : TEXCOORD0, out float3 outnormal : NORMAL, out float4 depth : TEXCOORD1, out float3 worldPos : TEXCOORD2) : SV_POSITION
{
    depth = mul(mul(mul(float4(position, 1), world), lightView), lightProj); 

    worldPos = mul(float4(position, 1), world).xyz;
    float4 outPos = mul(mul(mul(float4(position, 1), world), view), proj);
    
    outnormal = mul(float4(normal.xyz,0), world).xyz;
    
    outColor = float4(colour.r, colour.g, colour.b, 1) + modelColour;
    
    return outPos;
}

void MeshPixel(in float4 pos : SV_POSITION, in float4 inColor : TEXCOORD0, in float3 normal : NORMAL, in float4 depth : TEXCOORD1, in float3 worldPos : TEXCOORD2, out float4 outcolor : SV_Target)
{
    float4 light = ToonLighting(normal, lightDir, viewPos, worldPos);

    outcolor = LightWithShadow(light, depth, displayShadowMap, textureSampler, inColor);
}

float4 TexturedMeshVertex(in float3 position : POSITION, in float3 colour : TEXCOORD0, in float3 normal : TEXCOORD1,
    in float2 uv : TEXCOORD2, out float4 outColor : TEXCOORD0, out float2 outUV : TEXCOORD1, out float3 outnormal : NORMAL, out float4 depth : TEXCOORD2, out float3 worldPos : TEXCOORD3) : SV_POSITION
{   
    depth = mul(mul(mul(float4(position, 1), world), lightView), lightProj);    
    
    float4 p = mul(float4(position, 1), world);
    worldPos = p.xyz;
    float4 outPos = mul(mul(mul(float4(position, 1), world), view), proj); 
    
    outnormal = mul(float4(normal.xyz,0), world).xyz;
    
    outColor = float4(colour.r, colour.g, colour.b, 1) + modelColour;
    outUV = uv;
    
    return outPos;
}

void TexturedMeshPixel(in float4 pos : SV_POSITION, in float4 inColor : TEXCOORD0, in float2 inUV : TEXCOORD1,
    in float3 normal : NORMAL, in float4 depth : TEXCOORD2, in float3 worldPos : TEXCOORD3,
    out float4 outcolor : SV_Target)
{
    float4 light = ToonLighting(normal, lightDir, viewPos, worldPos);

    outcolor = LightWithShadow(light, depth, displayShadowMap, textureSampler, display.Sample(textureSampler, float2(inUV.x, 1-inUV.y))) + inColor;
}

// SHADOWS

float4 TexturedShadowMeshVertex(in float3 position : POSITION, in float3 colour : TEXCOORD0, in float3 normal : TEXCOORD1,
    in float2 uv : TEXCOORD2, out float depth : TEXCOORD0) : SV_POSITION
{
    float4 outPos = mul(mul(mul(float4(position, 1), world), view), proj);
    
    //outnormal = mul(float4(normal.xyz,0), world).xyz;
    
    depth = outPos.z/outPos.w;
    
    return outPos;
}

float4 ShadowMeshVertex(in float3 position : POSITION, in float3 colour : TEXCOORD0, in float3 normal : TEXCOORD1,
	out float depth : TEXCOORD0) : SV_POSITION
{
    float4 outPos = mul(mul(mul(float4(position, 1), world), view), proj);
    
    //outnormal = mul(float4(normal.xyz,0), world).xyz;
    
    depth = outPos.z/outPos.w;
    
    return outPos;
}

void ShadowMeshPixel(in float4 pos : SV_POSITION, in float depth : TEXCOORD0, out float4 outcolor : SV_Target)
{
	//outcolor.rgba = float4(depth, depth, depth, 1);
    outcolor.rgb = depth;
    outcolor.a = 1;
    //outcolor.rgb = pos.x; //pos.z/pos.w;
    //outcolor.a = 1;
}

// OUTLINE

float4 OulineTexturedMeshVertex(in float3 position : POSITION, in float3 colour : TEXCOORD0, in float3 normal : TEXCOORD1,
    in float2 uv : TEXCOORD2, out float4 outColor : TEXCOORD0, out float2 outUV : TEXCOORD1, out float3 outnormal : NORMAL, out float4 depth : TEXCOORD2) : SV_POSITION
{   
    depth = mul(mul(mul(float4(position, 1), world), lightView), lightProj);    
    
    float4 outPos = mul(mul(mul(float4(position, 1), world), view), proj); 
    
    outnormal = mul(float4(normal.xyz,0), world).xyz;

    float3 clipnormal = mul(mul(outnormal, view), proj);
    float2 offset = (normalize(clipnormal.xy) / screenSize) * 1.5f * outPos.w * 2;
    outPos.xy += offset;
    
    outColor = float4(colour.r, colour.g, colour.b, 1);
    outUV = uv;
    
    return outPos;
}

void OutlineTexturedMeshPixel(in float4 pos : SV_POSITION, in float4 inColor : TEXCOORD0, in float2 inUV : TEXCOORD1,
    in float3 normal : NORMAL, in float4 depth : TEXCOORD2,
    out float4 outcolor : SV_Target)
{
   outcolor = float4(0,0,0,1);
}
