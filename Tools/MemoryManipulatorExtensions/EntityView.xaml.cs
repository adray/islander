﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Collections.ObjectModel;

using Islander.Managed;
using FreeformMapExtensions;
using Islander.Scripting;
using System.Windows.Markup;
using System.Windows.Forms.Integration;

namespace MemoryManipulatorExtensions
{
    public class EntityWrapper : FreeformMapExtensions.ViewModelBase<EntityWrapper>
    {
        private IEntity entity;
        private IScripting scripting;
        private IComponentBase selected;

        public EntityWrapper(IEntity entity, IScripting scripting)
        {
            this.entity = entity;
            this.scripting = scripting;
            if (this.entity.HasComponent<FreeformMapExtensions.Linker>())
            {
                this.entity.GetComponent<FreeformMapExtensions.Linker>().Links.CollectionChanged += (o, e) =>
                {
                    OnPropertyChanged("Links");
                };
            }
        }

        public IComponentBase Selected
        {
            get
            {
                return this.selected;
            }
            set
            {
                this.selected = value;
                OnPropertyChanged("Selected");
            }
        }

        public IEntity Entity
        {
            get
            {
                return this.entity;
            }
        }

        public IScripting Scripting
        {
            get
            {
                return this.scripting;
            }
        }

        public ObservableCollection<IComponentBase> Components
        {
            get
            {
                return new ObservableCollection<IComponentBase>(
                    this.entity.GetComponents());
            }
        }

        public string Name
        {
            get
            {
                if (this.entity.HasComponent<Editable>())
                {
                    return this.entity.GetComponent<FreeformMapExtensions.Editable>().Name ?? string.Empty;
                }
                return string.Empty;
            }
        }

        public ObservableCollection<EntityWrapper> Links
        {
            get
            {
                ObservableCollection<EntityWrapper> wrappers = new ObservableCollection<EntityWrapper>();
                if (this.entity.HasComponent<Linker>())
                {
                    var links = this.entity.GetComponent<FreeformMapExtensions.Linker>().Links;
                    foreach (var link in links)
                        wrappers.Add(new EntityWrapper(link.Child != this.Entity ? link.Child : link.Owner, this.scripting));
                }
                return wrappers;
            }
        }

        public ObservableCollection<FreeformMapExtensions.Link> LinksRaw
        {
            get
            {
                if (this.entity.HasComponent<Linker>())
                {
                    var links = this.entity.GetComponent<FreeformMapExtensions.Linker>().Links;
                    return links;
                }
                return null;
            }
        }

        public ICommand Link
        {
            get
            {
                return new FreeformMapExtensions.Command<object>(new Action<object>((o) =>
                    {

                        FreeformEditorClick.Entity = entity;
                        FreeformEditorClick.State = EditState.Link;

                    }));
            }
        }

        public ICommand MoveBack
        {
            get
            {
                return new FreeformMapExtensions.Command<object>(new Action<object>((o) =>
                    {
                        this.Entity.GetComponent<ITransform>().Position.Z -= 1;
                    }));
            }
        }

        public ICommand MoveForward
        {
            get
            {
                return new FreeformMapExtensions.Command<object>(new Action<object>((o) =>
                {
                    this.Entity.GetComponent<ITransform>().Position.Z += 1;
                }));
            }
        }

        public ICommand RemoveComponent
        {
            get
            {
                return new FreeformMapExtensions.Command<object>((o) =>
                    {
                        if (this.Selected != null)
                        {
                            this.Entity.RemoveComponent(this.Selected);
                            this.Selected = null;
                            OnPropertyChanged("Components");
                        }
                    });
            }
        }

        public void AddComponent(IManagedComponent component)
        {
            this.entity.AddComponent(component);
            OnPropertyChanged("Components");
        }

    }

    public class ParameterSelector : DataTemplateSelector
    {
        public override DataTemplate SelectTemplate(object item, DependencyObject container)
        {
            FrameworkElement element = container as FrameworkElement;

            if (item.GetType() == typeof(ITransform))
            {
                return element.FindResource("TransformParams") as DataTemplate;
            }
            else if (item.GetType() == typeof(Editable))
            {
                return element.FindResource("EditableParams") as DataTemplate;
            }
            else if (item.GetType() == typeof(TriggerBox))
            {
                return element.FindResource("TriggerBoxParams") as DataTemplate;
            }
            else if (item.GetType() == typeof(ISprite))
            {
                return element.FindResource("SpriteParams") as DataTemplate;
            }
            else if (item.GetType().IsSubclassOf(typeof(IScript)))
            {
                return element.FindResource("GenericParams") as DataTemplate;
            }

            return null;
        }
    }

    /// <summary>
    /// Interaction logic for EntityView.xaml
    /// </summary>
    public partial class EntityView : Window
    {
        public EntityView(IEntity entity, IScripting scripting)
        {
            InitializeComponent();

            this.Loaded += (o, e) =>
                {
                    ElementHost.EnableModelessKeyboardInterop(this);
                    this.DataContext = new EntityWrapper(entity, scripting);
                    this.LinkGrid.AutoGeneratedColumns += (o1, e1) =>
                        {
                            this.LinkGrid.Columns[0].Visibility = System.Windows.Visibility.Collapsed;
                            this.LinkGrid.Columns[1].Visibility = System.Windows.Visibility.Collapsed;
                            this.LinkGrid.Columns[2].Visibility = System.Windows.Visibility.Collapsed;
                        };
                };
            
            this.Closing += (o, e) =>
                {
                    this.LinkGrid.CancelEdit(DataGridEditingUnit.Row);
                };
        }

        public void ScriptChanged(object sender, EventArgs e)
        {
            EntityWrapper wrapper = this.DataContext as EntityWrapper;
            ComboBox combobox = sender as ComboBox;
            //IScript script = wrapper.Scripting.Activate((combobox.SelectedItem as Type).FullName);
            try
            {
                IScript script = Activator.CreateInstance(combobox.SelectedItem as Type) as IScript;

                script.OnLoad();
                wrapper.AddComponent(script);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.StackTrace);
            }
        }
    }
}
