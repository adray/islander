﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;

using Islander.Managed;
using Islander.Scripting;
using Islander.Utilities;

namespace MemoryManipulatorExtensions
{
    public enum EditState
    {
        Entry,
        Link,
    }

    [Editor.Plugin(CommandName=FreeformMapExtensions.Commands.ClickCommand,
        CommandOnly=true)]
    public class FreeformEditorClick : Editor.IPlugin
    {
        public static EditState State = EditState.Entry;
        public static IEntity Entity;

        public void Action(Editor.IPluginContext context)
        {
            var entity = (context as Editor.PluginContext<Tuple<IEntity, IScripting>>).Value;

            if (State == EditState.Entry)
            {
                new EntityView(entity.Item1, entity.Item2).Show();
            }
            else if (State == EditState.Link)
            {
                var link = new FreeformMapExtensions.Link(Entity, entity.Item1);
                Entity.GetComponent<FreeformMapExtensions.Linker>().Links.Add(link);
                entity.Item1.GetComponent<FreeformMapExtensions.Linker>().Links.Add(link);
                State = EditState.Entry;
            }
        }
    }
    
    [Editor.Plugin(CommandName=FreeformMapExtensions.Commands.SaveCommand,
        CommandOnly=true)]
    public class FreeformEditorSave : Editor.IPlugin
    {
        public void Action(Editor.IPluginContext context)
        {
            var world = context as FreeformMapExtensions.WorldContext;
            
            Serializer serialize = new Serializer();
            serialize.Serialize(world.Filename, world.Entities);
        }
    }

    [Editor.Plugin(CommandName = FreeformMapExtensions.Commands.LoadCommand,
        CommandOnly = true)]
    public class FreeformEditorLoad : Editor.IPlugin
    {
        public void Action(Editor.IPluginContext context)
        {
            var world = context as FreeformMapExtensions.WorldContext;

            Serializer serialize = new Serializer();
            serialize.Error += new EventHandler<Serializer.SerializationError>(serialize_Error);
            world.Entities.AddRange(serialize.Deserialize(world.Filename, world.World));
        }

        void serialize_Error(object sender, Serializer.SerializationError e)
        {
            MessageBox.Show("Could not load level." + Environment.NewLine + e.Error);
        }
    }

    [Editor.Plugin(CommandName = FreeformMapExtensions.Commands.PlayCommand,
        CommandOnly = true)]
    public class FreefromEditorPlay : Editor.IPlugin
    {
        public void Action(Editor.IPluginContext context)
        {
            object ret = Microsoft.Win32.Registry.GetValue(Microsoft.Win32.Registry.CurrentUser
                            + @"\Software\MemoryManipulator", "Path", null);

            var world = context as FreeformMapExtensions.WorldContext;

            if (world.Filename == null)
            {
                MessageBox.Show("No file to play.", "Info");
            }
            else if (ret != null)
            {
                string path = ret as string;
                System.Diagnostics.Process proc = new System.Diagnostics.Process();
                proc.StartInfo = new System.Diagnostics.ProcessStartInfo(path);
                proc.StartInfo.WorkingDirectory = System.IO.Path.GetDirectoryName(path);
                proc.StartInfo.Arguments = "/level \"" + world.Filename + "\"";
                proc.Start();
            }
            else
            {
                MessageBox.Show("Dev environment not set up. Please run" +
                " memory manipulator with the /dev option.", "Info");
            }
        }
    }

    [Editor.Plugin(CommandName = FreeformMapExtensions.Commands.SettingsCommand,
        CommandOnly = true)]
    public class FreeformEditorSettings : Editor.IPlugin
    {
        public void Action(Editor.IPluginContext context)
        {
            var val = context as FreeformMapExtensions.WorldContext;
            IEntity settings = null;

            if (settings == null)
            {
                foreach (var entity in val.Entities)
                {
                    if (entity.HasComponent<FreeformMapExtensions.Settings>())
                    {
                        settings = entity;
                        break;
                    }
                }
            }

            if (settings == null)
            {
                settings = val.World.CreateEntity();
                settings.AddComponent<FreeformMapExtensions.Settings>();
                val.Entities.Add(settings);
            }

            new EntityView(settings, val.Scripting).Show();
        }
    }
}
