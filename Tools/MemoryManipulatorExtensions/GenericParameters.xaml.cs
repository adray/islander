﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Markup;

namespace MemoryManipulatorExtensions
{
    public class EnumConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
           string val = (value as ComboBoxItem).Content as string;

            try
            {
                return Enum.Parse(targetType, val);
            }
            catch (Exception)
            {
                return null;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return null;
        }
    }

    /// <summary>
    /// Interaction logic for GenericParameters.xaml
    /// </summary>
    public partial class GenericParameters : UserControl
    {
        public GenericParameters()
        {
            InitializeComponent();

            Loaded += new RoutedEventHandler(GenericParameters_Loaded);
        }

        void Combobox_Changed(object sender, EventArgs e)
        {
            ComboBox combo = sender as ComboBox;
            if (combo != null)
            {
                var type = this.DataContext.GetType();
                System.Reflection.PropertyInfo info = type.GetProperty(combo.Name);
                EnumConverter converter = new EnumConverter();
                var setter = info.GetSetMethod();
                setter.Invoke(this.DataContext,new object[]{converter.Convert(combo.SelectedItem, info.PropertyType, null, 
                    System.Globalization.CultureInfo.InvariantCulture)});
            }
        }

        void GenericParameters_Loaded(object sender, RoutedEventArgs e)
        {
            var props = this.DataContext.GetType().GetProperties();

            string generated = string.Empty;
            
            generated += "<TextBlock Text=\"" + this.DataContext.GetType().Name + "\"/>";

            foreach (var prop in props)
            {
                if (prop.CanRead && prop.CanWrite)
                {
                    if (typeof(string) == prop.GetGetMethod().ReturnType)
                    {
                        generated += "<TextBlock Text=\"" + prop.Name + "\"/>";
                        generated += "<TextBox Width=\"300\" Text=\"{Binding " + prop.Name + "}\"/>\n";
                    }
                    else if (typeof(bool) == prop.GetGetMethod().ReturnType)
                    {
                        generated += "<CheckBox IsChecked=\"{Binding " + prop.Name + "}\" Content=\"" + prop.Name + "\"/>";
                    }
                    else if (typeof(Int32) == prop.GetGetMethod().ReturnType)
                    {
                        generated += "<TextBlock Text=\"" + prop.Name + "\"/>";
                        generated += "<TextBox Width=\"300\" Text=\"{Binding " + prop.Name + "}\"/>\n";                    
                    }
                    else if (prop.GetGetMethod().ReturnType.IsEnum)
                    {
                        Array values = Enum.GetValues(prop.GetGetMethod().ReturnType);
                        generated += "<ComboBox Name=\"" + prop.Name + "\" " //SelectedValue=\"{Binding " + prop.Name
                            //+ ", Converter={DynamicResource EnumConverter}}\""
                            + "Width=\"300\" >";
                        foreach (var item in values)
                        {
                            generated += "<ComboBoxItem >" + item + "</ComboBoxItem>";
                        }
                        generated += "</ComboBox>";
                    }
                }
            }

            string assName = System.Reflection.Assembly.GetExecutingAssembly().GetName().Name;

            string xaml =
                    "<StackPanel " + // "xmlns:src='clr-namespace:MemoryManipulatorExtensions; assembly=" + assName
                   // + "' " +
                    "Orientation=\"Vertical\">\n" +
                   // "<StackPanel.Resources>" +
                  //  "<l:EnumConverter x:Key=\"EnumConverter\" />" +
                  //  "</StackPanel.Resources>" +
                        generated +
                    "</StackPanel>";

            var context = new ParserContext();

            var viewType = this.GetType();
            context.XamlTypeMapper = new XamlTypeMapper(new string[0]);
            //context.XamlTypeMapper.AddMappingProcessingInstruction("vm", viewModelType.Namespace, viewModelType.Assembly.FullName);
            context.XamlTypeMapper.AddMappingProcessingInstruction("v", viewType.Namespace, viewType.Assembly.FullName);

            context.XmlnsDictionary.Add("", "http://schemas.microsoft.com/winfx/2006/xaml/presentation");
            context.XmlnsDictionary.Add("x", "http://schemas.microsoft.com/winfx/2006/xaml");
            context.XmlnsDictionary.Add("l", "clr-namespace:MemoryManipulatorExtensions");

            //context.XmlnsDictionary.Add("vm", "vm");
            context.XmlnsDictionary.Add("v", "v");

            UIElement content = XamlReader.Parse(xaml, context) as UIElement;

            ProvideHandlers(content);

            this.MainGrid.Children.Add(content);            
        }

        private void ProvideHandlers(UIElement element)
        {
            element.AddHandler(ComboBox.SelectionChangedEvent, new RoutedEventHandler(Combobox_Changed));

            ComboBox combo = element as ComboBox;
            if (combo != null)
            {
                var type = this.DataContext.GetType();
                System.Reflection.PropertyInfo info = type.GetProperty(combo.Name);
                var ret = info.GetGetMethod().Invoke(this.DataContext, new object[] { });
                for (int i = 0; i < combo.Items.Count; i++)
                {
                    ComboBoxItem item = combo.Items[i] as ComboBoxItem;
                    if (item.Content.ToString() == ret.ToString())
                        combo.SelectedIndex = i;
                }
            }

            for (int i = 0; i < VisualTreeHelper.GetChildrenCount(element); i++)
            {
                UIElement obj = VisualTreeHelper.GetChild(element, i) as UIElement;
                if (obj != null)
                {
                    ProvideHandlers(obj);
                }
            }
        }
    }
}
