﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Raft
{
    class Program
    {
        /*
         * <Package>
         *  <File Source="resources/myfile.txt" Target="resources/myfile.txt" />
         *  <File Source="release/my.exe" Target="bin/my.exe" />
         *  <Pack Target="resources/pak1.pak" Type="ZIP">
         *    <File Source="resources/myfile.txt" Target="myfile.txt" />
         *  </Pack>
         * </Package>
         * 
         * 
         */
        static void Main(string[] args)
        {
            string package = null;
            for (int i = 0; i < args.Length; i++)
            {
                if (args[i].StartsWith("/package:"))
                {
                    package = args[i].Substring("/package:".Length);
                }
            }

            Console.WriteLine("Raft - The Islander Packager");

            if (package == null)
            {
                Console.WriteLine("No package file");
                Environment.Exit(2);
            }

            System.Xml.XmlDocument doc = new System.Xml.XmlDocument();

            try
            {
                doc.Load(package);

                var packageNode = doc.SelectSingleNode("Package");
                if (packageNode != null)
                {
                    var clean = packageNode.SelectNodes("Clean");
                    if (clean != null)
                    {
                        foreach (System.Xml.XmlNode cleanNode in clean)
                        {
                            var target = cleanNode.Attributes["Target"];
                            if (target != null)
                            {
                                // Delete all files and folders
                                string path = System.IO.Path.Combine(Environment.CurrentDirectory, target.Value);
                                Console.WriteLine("Deleting files at " + path);

                                if (System.IO.Directory.Exists(path))
                                {
                                    try
                                    {
                                        System.IO.Directory.Delete(path, true);
                                    }
                                    catch (System.IO.IOException)
                                    {
                                        Console.WriteLine("Failed to delete " + path);
                                        Environment.Exit(2);
                                    }
                                }
                                else
                                {
                                    Console.WriteLine("No cleaning required.");
                                }
                            }
                        }
                    }

                    var packs = packageNode.SelectNodes("Pack");
                    if (packs != null)
                    {
                        foreach (System.Xml.XmlNode pack in packs)
                        {
                            var target = pack.Attributes["Target"];
                            var type = pack.Attributes["Type"];

                            if (target != null && type != null)
                            {
                                if (type.Value == "ZIP")
                                {
                                    System.IO.Compression.ZipArchive archive = null;

                                    try
                                    {
                                        string path = System.IO.Path.GetDirectoryName(target.Value);
                                        System.IO.Directory.CreateDirectory(path);
                                        archive = System.IO.Compression.ZipFile.Open(target.Value, System.IO.Compression.ZipArchiveMode.Create);
                                    }
                                    catch (Exception)
                                    {
                                        Console.WriteLine("Error creating archive " + target.Value);
                                        Environment.Exit(2);
                                    }

                                    foreach (System.Xml.XmlNode file in pack.SelectNodes("File"))
                                    {
                                        var filesource = file.Attributes["Source"];
                                        var filetarget = file.Attributes["Target"];

                                        if (filesource != null && filetarget != null)
                                        {
                                            var entry = archive.CreateEntry(filetarget.Value, System.IO.Compression.CompressionLevel.Optimal);

                                            byte[] bytes = null;
                                            try
                                            {
                                                bytes = System.IO.File.ReadAllBytes(filesource.Value);
                                            }
                                            catch (Exception)
                                            {
                                                Console.WriteLine("Error loading file to be packed " + filesource.Value);
                                                Environment.Exit(2);
                                            }

                                            using (var writer = new System.IO.BinaryWriter(entry.Open()))
                                            {
                                                writer.Write(bytes);
                                            }

                                            Console.WriteLine("Packed " + filesource.Value + " to " + target.Value);
                                        }
                                        else
                                        {
                                            Console.WriteLine("Error in Pack/File tag " + file.OuterXml);
                                            Environment.Exit(2);
                                        }
                                    }

                                    archive.Dispose();
                                }
                                else
                                {
                                    Console.WriteLine("Unsupported Packing operation: '" + type.Value ?? string.Empty + "'");
                                    Environment.Exit(2);
                                }
                            }
                            else
                            {
                                Console.WriteLine("Error in Pack tag " + pack.OuterXml);
                                Environment.Exit(2);
                            }
                        }
                    }

                    var files = packageNode.SelectNodes("File");
                    if (files != null)
                    {
                        foreach (System.Xml.XmlNode file in files)
                        {
                            var source = file.Attributes["Source"];
                            var target = file.Attributes["Target"];

                            if (source != null && target != null)
                            {
                                string s = System.IO.Path.Combine(Environment.CurrentDirectory, source.Value);
                                string t = System.IO.Path.Combine(Environment.CurrentDirectory, target.Value);

                                try
                                {
                                    string path = t.Substring(0, t.Length - System.IO.Path.GetFileName(t).Length);
                                    System.IO.Directory.CreateDirectory(path);
                                    System.IO.File.Copy(s, t);
                                    Console.WriteLine("Copied " + s + " to " + t);
                                }
                                catch (Exception)
                                {
                                    Console.WriteLine("Failed to copy " + s + " to " + t);
                                    Environment.Exit(2);
                                }
                            }
                            else
                            {
                                Console.WriteLine("File defined without a source/target pair");
                                Environment.Exit(2);
                            }
                        }
                    }
                    else
                    {
                        Console.WriteLine("No files to package?");
                        Environment.Exit(2);
                    }
                }
                else
                {
                    Console.WriteLine("Could not locate 'Package'");
                    Environment.Exit(2);
                }
            }
            catch (System.Xml.XmlException ex)
            {
                Console.WriteLine("Package parse error: " + ex.Message);
                Environment.Exit(2);
            }
            catch (System.IO.IOException ex)
            {
                Console.WriteLine("Package parse error: " + ex.Message);
                Environment.Exit(2);
            }
            catch (System.UnauthorizedAccessException ex)
            {
                Console.WriteLine("Package parse error: " + ex.Message);
                Environment.Exit(2);
            }
        }
    }
}
