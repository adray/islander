project(islander_crimson)

#set(CMAKE_AUTOMOC TRUE)

include_directories(includes)
include_directories("internal\\includes")

# Source

set(islander_crimson_HEADERS
    includes/Crimson.h
    internal/includes/CrimsonCore.h)
	
set(islander_crimson_SOURCES
	internal/src/Crimson.cpp
    internal/src/CrimsonCore.cpp)


source_group("crimson\\Source Files" FILES ${islander_crimson_SOURCES})
source_group("crimson\\Header Files" FILES ${islander_crimson_HEADERS})

add_library(islCrimson SHARED ${islander_crimson_SOURCES} ${islander_crimson_HEADERS})

