#include "CrimsonCore.h"

bool IsOverlapping(int px, int py, int width, int height, int qx, int qy)
{
    return qx >= px && qx <= px + width &&
        qy >= py && qy <= py + height;
}

bool FindButton(Crimson* crimson, int id, bool* pressed, bool* focus)
{
    *pressed = false;
    *focus = false;

    if (!crimson)
    {
        return false;
    }

    auto& it = crimson->buttons.find(id);
    if (it != crimson->buttons.end())
    {
        // Existing button from prev frame

        it->second.cleanup = false;

        *pressed = it->second.isPressedDown && crimson->mouseInput.leftMouseUp;
        *focus = it->second.hasFocus;
        return true;
    }

    return false;
}

void GetButton(Crimson* crimson, int id, float px, float py, float w, float h, bool* pressed, bool* focus)
{
    *pressed = false;
    *focus = false;

    if (!crimson)
    {
        return;
    }

    auto& it = crimson->buttons.find(id);
    if (it != crimson->buttons.end())
    {
        // Existing button from prev frame
        
        it->second.cleanup = false;

        bool overlap = IsOverlapping(
            px * crimson->window.width,
            py * crimson->window.height,
            w * crimson->window.width,
            h * crimson->window.height,
            crimson->mouseInput.mouseX,
            crimson->mouseInput.mouseY);
        *focus = overlap;
        it->second.hasFocus = overlap;

        if (it->second.isPressedDown && crimson->mouseInput.leftMouseUp)
        {
            it->second.isPressedDown = false;
            it->second.isPressed = true;
            *pressed = true;
        }
        else if (crimson->mouseInput.leftMouseDown && overlap)
        {
            it->second.isPressedDown = true;
            *pressed = false;
        }
        else if (!it->second.isPressedDown)
        {
            it->second.isPressed = false;
            *pressed = false;
        }
    }
    else
    {
        // New button
        Button button = {};
        crimson->buttons.insert(std::make_pair(id, button));
    }
}
