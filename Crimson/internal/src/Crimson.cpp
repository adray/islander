#include "CrimsonCore.h"
#include <algorithm>
#include <cstring>

CRIMSON_HANDLE CrimsonInitialize()
{
    auto crimson = new Crimson();
    std::memset(&crimson->selectionBox, 0, sizeof(SelectionBox));
    std::memset(&crimson->mouseInput, 0, sizeof(CrimsonMouseInput));
    std::memset(&crimson->window, 0, sizeof(CrimsonWindow));

    return crimson;
}

void CrimsonDestroy(CRIMSON_HANDLE handle)
{
    delete (Crimson*)handle;
}

int CrimsonGetCommandList(CRIMSON_HANDLE handle, CrimsonCommand* commands, int num, int* index)
{
    auto crimson = (Crimson*)handle;

    if (*index >= 0 && *index < crimson->commands.size())
    {
        int cmd = 0;
        for (; *index < std::min(*index + num, (int)crimson->commands.size()); (*index)++)
        {
            commands[cmd++] = crimson->commands[*index];
        }

        return cmd;
    }

    return 0;
}

void CrimsonGetMouseInput(CRIMSON_HANDLE handle, CrimsonMouseInput** input)
{
    *input = &((Crimson*)handle)->mouseInput;
}

void CrimsonGetWindow(CRIMSON_HANDLE handle, CrimsonWindow** window)
{
    *window = &((Crimson*)handle)->window;
}

void SetLine(CrimsonCommand *line, float startX, float startY, float endX, float endY, bool dotted, float thickness, const float* colour, int absolute)
{
    line->type = CRIMSON_COMMAND_LINE;
    std::memset(&line->line, 0, sizeof(CrimsonCommandLine));

    line->line.startX = startX;
    line->line.startY = startY;
    line->line.endX = endX;
    line->line.endY = endY;
    line->line.dotted = dotted;
    line->line.thickness = thickness;
    line->line.colour[0] = colour[0];
    line->line.colour[1] = colour[1];
    line->line.colour[2] = colour[2];
    line->line.colour[3] = colour[3];
    line->line.absolute = absolute;
}

void SubmitIcon(Crimson* crimson, const Control& control)
{
    CrimsonCommand icon;
    icon.type = CRIMSON_COMMAND_ICON;
    icon.icon.imageId = control.icon.imageId;
    icon.icon.widthPercent = control.icon.widthPercent;
    icon.icon.heightPercent = control.icon.heightPercent;
    icon.icon.posXPercent = control.icon.posXPercent;
    icon.icon.posYPercent = control.icon.posYPercent;
    icon.icon.px = control.icon.px;
    icon.icon.py = control.icon.py;
    icon.icon.sx = control.icon.sx;
    icon.icon.sy = control.icon.sy;
    icon.icon.arrayIndex = control.icon.arrayId;
    icon.userData = control.userData;

    crimson->commands.push_back(icon);
}

void SubmitFilledRect(Crimson* crimson, const Control& control)
{
    CrimsonCommand rect;
    rect.type = CRIMSON_COMMAND_FILL_RECT;
    rect.userData = control.userData;
    rect.filledRect.heightPercent = control.filledRect.heightPercent;
    rect.filledRect.widthPercent = control.filledRect.widthPercent;
    rect.filledRect.posXPercent = control.filledRect.posXPercent;
    rect.filledRect.posYPercent = control.filledRect.posYPercent;
    rect.filledRect.colour[0] = control.filledRect.colour[0];
    rect.filledRect.colour[1] = control.filledRect.colour[1];
    rect.filledRect.colour[2] = control.filledRect.colour[2];
    rect.filledRect.colour[3] = control.filledRect.colour[3];

    crimson->commands.push_back(rect);

    float startX = control.filledRect.posXPercent;
    float startY = control.filledRect.posYPercent;
    float endX = control.filledRect.posXPercent + control.filledRect.widthPercent;
    float endY = control.filledRect.posYPercent + control.filledRect.heightPercent;

    CrimsonCommand lines[4];
    SetLine(&lines[0], startX, startY, endX, startY, false, control.filledRect.thickness, control.filledRect.borderColour, CRIMSON_PERCENT); // top
    SetLine(&lines[1], startX, endY, endX, endY, false, control.filledRect.thickness, control.filledRect.borderColour, CRIMSON_PERCENT);     // bottom
    SetLine(&lines[2], startX, startY, startX, endY, false, control.filledRect.thickness, control.filledRect.borderColour, CRIMSON_PERCENT); // left
    SetLine(&lines[3], endX, startY, endX, endY, false, control.filledRect.thickness, control.filledRect.borderColour, CRIMSON_PERCENT);     // right

    for (int i = 0; i < 4; i++)
    {
        lines[i].userData = crimson->userData;
    }

    crimson->commands.push_back(lines[0]);
    crimson->commands.push_back(lines[1]);
    crimson->commands.push_back(lines[2]);
    crimson->commands.push_back(lines[3]);
}

void SubmitText(Crimson* crimson, const Control& control)
{
    CrimsonCommand text;
    text.type = CRIMSON_COMMAND_TEXT;
    text.userData = control.userData;

    text.text.flags = control.text.flags;
    text.text.fontType = control.text.fontType;
    text.text.heightPercent = control.text.heightPercent;
    text.text.widthPercent = control.text.widthPercent;
    text.text.posXPercent = control.text.posXPercent;
    text.text.posYPercent = control.text.posYPercent;
    text.text.text = new char[control.strText.length() + 1];
    strcpy(text.text.text, control.strText.c_str());
    std::memcpy(text.text.colour, control.text.colour, sizeof(control.text.colour));

    crimson->commands.push_back(text);
}

void SubmitTri(Crimson* crimson, const Control& control)
{
    CrimsonCommand tri;
    tri.type = CRIMSON_COMMAND_TRI;
    tri.userData = control.userData;
    
    if (control.spinner.direction == SpinnerDirection::Left)
    {
        tri.tri.posXPercent[0] = control.spinner.posXPercent;
        tri.tri.posYPercent[0] = control.spinner.posYPercent + control.spinner.heightPercent / 2.0f;
        tri.tri.posXPercent[1] = control.spinner.posXPercent + control.spinner.widthPercent;
        tri.tri.posYPercent[1] = control.spinner.posYPercent;
        tri.tri.posXPercent[2] = control.spinner.posXPercent + control.spinner.widthPercent;
        tri.tri.posYPercent[2] = control.spinner.posYPercent + control.spinner.heightPercent;
    }
    else if (control.spinner.direction == SpinnerDirection::Right)
    {
        tri.tri.posXPercent[0] = control.spinner.posXPercent + control.spinner.widthPercent;
        tri.tri.posYPercent[0] = control.spinner.posYPercent + control.spinner.heightPercent / 2.0f;
        tri.tri.posXPercent[1] = control.spinner.posXPercent;
        tri.tri.posYPercent[1] = control.spinner.posYPercent;
        tri.tri.posXPercent[2] = control.spinner.posXPercent;
        tri.tri.posYPercent[2] = control.spinner.posYPercent + control.spinner.heightPercent;
    }
    
    std::memcpy(tri.tri.colour, control.spinner.colour, sizeof(control.spinner.colour));

    crimson->commands.push_back(tri);
}

void CrimsonFrame(CRIMSON_HANDLE handle)
{
    auto crimson = (Crimson*)handle;
    for (auto& com : crimson->commands)
    {
        if (com.type == CRIMSON_COMMAND_TEXT)
        {
            delete[] com.text.text;
        }
    }

    crimson->commands.clear();

    std::vector<int> cleanup;
    for (auto& kvp : crimson->buttons)
    {
        if (kvp.second.cleanup)
        {
            cleanup.push_back(kvp.first);
            kvp.second.cleanup = true;
        }
    }

    for (int i : cleanup)
    {
        crimson->buttons.erase(i);
    }

    if (crimson->selectionBox.dragging)
    {
        CrimsonCommand lines[4];
        SetLine(&lines[0], crimson->selectionBox.startx, crimson->selectionBox.starty, crimson->selectionBox.endx, crimson->selectionBox.starty, true, crimson->selectionBox.thickness, crimson->selectionBox.colour, CRIMSON_ABSOLUTE); // top
        SetLine(&lines[1], crimson->selectionBox.startx, crimson->selectionBox.endy, crimson->selectionBox.endx, crimson->selectionBox.endy, true, crimson->selectionBox.thickness, crimson->selectionBox.colour, CRIMSON_ABSOLUTE);     // bottom
        SetLine(&lines[2], crimson->selectionBox.startx, crimson->selectionBox.starty, crimson->selectionBox.startx, crimson->selectionBox.endy, true, crimson->selectionBox.thickness, crimson->selectionBox.colour, CRIMSON_ABSOLUTE); // left
        SetLine(&lines[3], crimson->selectionBox.endx, crimson->selectionBox.starty, crimson->selectionBox.endx, crimson->selectionBox.endy, true, crimson->selectionBox.thickness, crimson->selectionBox.colour, CRIMSON_ABSOLUTE);     // right

        for (int i = 0; i < 4; i++)
        {
            lines[i].userData = nullptr;
        }

        crimson->commands.push_back(lines[0]);
        crimson->commands.push_back(lines[1]);
        crimson->commands.push_back(lines[2]);
        crimson->commands.push_back(lines[3]);
    }

    for (auto& control : crimson->controls)
    {
        switch (control.second.type)
        {
        case ControlType::Icon:
            SubmitIcon(crimson, control.second);
            break;
        case ControlType::FilledRect:
            SubmitFilledRect(crimson, control.second);
            break;
        case ControlType::Text:
            SubmitText(crimson, control.second);
            break;
        case ControlType::Spinner:
            SubmitTri(crimson, control.second);
            break;
        }
    }

    crimson->controls.clear();
    crimson->currentId = 0;
    crimson->parentId = -1;
    crimson->sameLinePosX = 0;
    crimson->sameLinePosY = 0;
    crimson->samePosX = 0;
    crimson->samePosY = 0;
    crimson->nextPosX = 0;
    crimson->nextPosY = 0;
    crimson->sameLine = false;
    crimson->samePos = false;
    crimson->userData = nullptr;
}

void CrimsonSameLine(CRIMSON_HANDLE handle)
{
    auto crimson = (Crimson*)handle;
    crimson->sameLine = true;
    crimson->samePos = false;
}

void CrimsonSamePos(CRIMSON_HANDLE handle)
{
    auto crimson = (Crimson*)handle;
    crimson->sameLine = false;
    crimson->samePos = true;
}

CRIMSON_RESULT CrimsonSetPos(CRIMSON_HANDLE handle, float xPercent, float yPercent)
{
    auto crimson = (Crimson*)handle;

    crimson->sameLinePosX = xPercent;
    crimson->sameLinePosY = yPercent;
    crimson->samePosX = xPercent;
    crimson->samePosY = yPercent;
    crimson->nextPosX = xPercent;
    crimson->nextPosY = yPercent;

    return CRIMSON_SUCCESS;
}

CRIMSON_RESULT CrimsonTextEx(CRIMSON_HANDLE handle, const char* text, float widthPercent, float heightPercent, int fontType, int flags, float* colour)
{
    auto crimson = (Crimson*)handle;

    Control control;
    control.userData = crimson->userData;
    control.parentId = crimson->parentId;
    control.text.heightPercent = heightPercent;
    control.text.widthPercent = widthPercent;
    if (colour)
    {
        std::memcpy(control.text.colour, colour, sizeof(float) * 4);
    }
    else
    {
        control.text.colour[0] = 0.0f;
        control.text.colour[1] = 0.0f;
        control.text.colour[2] = 0.0f;
        control.text.colour[3] = 1.0f;
    }
    control.text.flags = flags;
    control.text.fontType = fontType;
    if (crimson->sameLine)
    {
        control.text.posXPercent = crimson->sameLinePosX;
        control.text.posYPercent = crimson->sameLinePosY;
    }
    else if (crimson->samePos)
    {
        control.text.posXPercent = crimson->samePosX;
        control.text.posYPercent = crimson->samePosY;
    }
    else
    {
        control.text.posXPercent = crimson->nextPosX;
        control.text.posYPercent = crimson->nextPosY;
    }
    control.strText = text;
    control.type = ControlType::Text;

    crimson->controls.insert(std::pair<int, Control>(crimson->currentId++, control));

    crimson->sameLinePosX = control.text.posXPercent + control.text.widthPercent;
    crimson->sameLinePosY = control.text.posYPercent;
    crimson->nextPosX = 0;
    crimson->nextPosY = control.text.posYPercent + control.text.heightPercent;
    crimson->samePosX = control.text.posXPercent;
    crimson->samePosY = control.text.posYPercent;
    crimson->sameLine = false;
    crimson->samePos = false;

    return CRIMSON_SUCCESS;
}

CRIMSON_RESULT CrimsonText(CRIMSON_HANDLE handle, const char* text, float widthPercent, float heightPercent, int fontType, int flags)
{
    return CrimsonTextEx(handle, text, widthPercent, heightPercent, fontType, flags, nullptr);
}

CRIMSON_RESULT CrimsonIcon(CRIMSON_HANDLE handle, int imageId, float widthPercent, float heightPercent, float px, float py, float sx, float sy)
{
    return CrimsonIconEx(handle, imageId, -1, widthPercent, heightPercent, px, py, sx, sy);
}

CRIMSON_RESULT CrimsonIconEx(CRIMSON_HANDLE handle, int imageId, int arrayIndex, float widthPercent, float heightPercent, float px, float py, float sx, float sy)
{
    auto crimson = (Crimson*)handle;

    Control control;
    control.userData = crimson->userData;
    control.parentId = crimson->parentId;
    control.icon.heightPercent = heightPercent;
    control.icon.widthPercent = widthPercent;
    if (crimson->sameLine)
    {
        control.icon.posXPercent = crimson->sameLinePosX;
        control.icon.posYPercent = crimson->sameLinePosY;
    }
    else if (crimson->samePos)
    {
        control.icon.posXPercent = crimson->samePosX;
        control.icon.posYPercent = crimson->samePosY;
    }
    else
    {
        control.icon.posXPercent = crimson->nextPosX;
        control.icon.posYPercent = crimson->nextPosY;
    }
    control.icon.imageId = imageId;
    control.type = ControlType::Icon;
    control.icon.px = px;
    control.icon.py = py;
    control.icon.sx = sx;
    control.icon.sy = sy;
    control.icon.arrayId = arrayIndex;

    crimson->controls.insert(std::pair<int, Control>(crimson->currentId++, control));

    crimson->sameLinePosX = control.icon.posXPercent + control.icon.widthPercent;
    crimson->sameLinePosY = control.icon.posYPercent;
    crimson->nextPosX = 0;
    crimson->nextPosY = control.icon.posYPercent + control.icon.heightPercent;
    crimson->samePosX = control.icon.posXPercent;
    crimson->samePosY = control.icon.posYPercent;
    crimson->sameLine = false;
    crimson->samePos = false;

    return CRIMSON_SUCCESS;
}

CRIMSON_RESULT CrimsonFilledRect(CRIMSON_HANDLE handle, float widthPercent, float heightPercent, float* colour, float thickness, float* border)
{
    auto crimson = (Crimson*)handle;

    Control control;
    control.userData = crimson->userData;
    control.parentId = crimson->parentId;
    control.filledRect.heightPercent = heightPercent;
    control.filledRect.widthPercent = widthPercent;
    if (crimson->sameLine)
    {
        control.filledRect.posXPercent = crimson->sameLinePosX;
        control.filledRect.posYPercent = crimson->sameLinePosY;
    }
    else if (crimson->samePos)
    {
        control.filledRect.posXPercent = crimson->samePosX;
        control.filledRect.posYPercent = crimson->samePosY;
    }
    else
    {
        control.filledRect.posXPercent = crimson->nextPosX;
        control.filledRect.posYPercent = crimson->nextPosY;
    }
    control.type = ControlType::FilledRect;
    control.filledRect.thickness = thickness;
    std::memcpy(control.filledRect.colour, colour, sizeof(float) * 4);
    std::memcpy(control.filledRect.borderColour, border, sizeof(float) * 4);

    crimson->controls.insert(std::pair<int, Control>(crimson->currentId++, control));

    crimson->sameLinePosX = control.filledRect.posXPercent + control.filledRect.widthPercent;
    crimson->sameLinePosY = control.filledRect.posYPercent;
    crimson->nextPosX = 0;
    crimson->nextPosY = control.filledRect.posYPercent + control.filledRect.heightPercent;
    crimson->samePosX = control.filledRect.posXPercent;
    crimson->samePosY = control.filledRect.posYPercent;
    crimson->sameLine = false;
    crimson->samePos = false;

    return CRIMSON_SUCCESS;
}

CRIMSON_RESULT CrimsonButton(CRIMSON_HANDLE handle, int id, const char* text, float widthPercent, float heightPercent, int flags)
{
    auto crimson = (Crimson*)handle;
    
    if ((flags & CRIMSON_BUTTON_FLAGS_CENTERED) == CRIMSON_BUTTON_FLAGS_CENTERED)
    {
        crimson->nextPosX -= widthPercent / 2.0f;
    }
    
    float posX = crimson->nextPosX;
    float posY = crimson->nextPosY;

    bool pressed; bool focus;
    GetButton(crimson, id, posX, posY, widthPercent, heightPercent, &pressed, &focus);

    if ((flags & CRIMSON_BUTTON_FLAGS_SELECTED) == CRIMSON_BUTTON_FLAGS_SELECTED)
    {
        focus = true;
    }

    float colour[4] = {
        0.2f, 0.2f, 0.2f, 1.0f
    };
    if (pressed)
    {
        float pressedColour[4] = {
            .8f, .8f, .8f, 1.0f
        };
        std::memcpy(colour, pressedColour, sizeof(colour));
    }
    else if (focus)
    {
        float focusColour[4] = {
            0.5f, 0.5f, 0.5f, 1.0f
        };
        std::memcpy(colour, focusColour, sizeof(colour));
    }

    float border[4] = {
        0.0f, 0.0f, 0.0f, 1.0f
    };
    if (CrimsonFilledRect(handle, widthPercent, heightPercent, colour, 1.0f, border) == CRIMSON_FAILURE)
    {
        return CRIMSON_FAILURE;
    }
    //CrimsonSamePos(handle);
    CrimsonSetPos(crimson, posX + widthPercent / 2.0f, posY /*+ heightPercent / 2.0f*/);
    if (CrimsonTextEx(handle, text, widthPercent*2, heightPercent*2, 0, CRIMSON_TEXT_FLAGS_CENTERED, border) == CRIMSON_FAILURE)
    {
        return CRIMSON_FAILURE;
    }

    return pressed ? CRIMSON_SUCCESS : CRIMSON_FAILURE;
}

CRIMSON_RESULT CrimsonSelectionBox(CRIMSON_HANDLE handle, float* colour, float thickness, float* startX, float* startY, float* endX, float* endY)
{
    auto crimson = (Crimson*)handle;

    *startX = 0.0f;
    *startY = 0.0f;
    *endX = 0.0f;
    *endY = 0.0f;

    crimson->selectionBox.thickness = thickness;
    std::memcpy(crimson->selectionBox.colour, colour, sizeof(float) * 4);

    if (crimson->selectionBox.dragging)
    {
        crimson->selectionBox.endx = (float)crimson->mouseInput.mouseX;
        crimson->selectionBox.endy = (float)crimson->mouseInput.mouseY;

        // End dragging if mouse up
        if (!crimson->mouseInput.leftMouseDown)
        {
            crimson->selectionBox.dragging = false;

            *startX = crimson->selectionBox.startx;
            *startY = crimson->selectionBox.starty;
            *endX = crimson->selectionBox.endx;
            *endY = crimson->selectionBox.endy;
        }
    }
    else
    {
        // Start dragging
        crimson->selectionBox.startx = (float)crimson->mouseInput.mouseX;
        crimson->selectionBox.starty = (float)crimson->mouseInput.mouseY;
        crimson->selectionBox.endx = (float)crimson->mouseInput.mouseX;
        crimson->selectionBox.endy = (float)crimson->mouseInput.mouseY;
        crimson->selectionBox.dragging = true;
    }

    return crimson->selectionBox.dragging ? CRIMSON_FAILURE : CRIMSON_SUCCESS;
}

CRIMSON_RESULT CrimsonBeginSelectable(CRIMSON_HANDLE handle, int id, int flags, int* selected)
{
    auto crimson = (Crimson*)handle;

    float posX = crimson->nextPosX;
    float posY = crimson->nextPosY;
    if (crimson->samePos)
    {
        posX = crimson->samePosX;
        posY = crimson->samePosY;
    }
    else if (crimson->sameLine)
    {
        posX = crimson->sameLinePosX;
        posY = crimson->sameLinePosY;
    }

    crimson->selectableX = posX;
    crimson->selectableY = posY;

    bool pressed; bool focus;
    if (FindButton(crimson, id, &pressed, &focus))
    {
        *selected = focus ? 1 : 0;

        return pressed ? CRIMSON_SUCCESS : CRIMSON_FAILURE;
    }
    else
    {
        *selected = 0;
        return CRIMSON_FAILURE;
    }
}

CRIMSON_RESULT CrimsonEndSelectable(CRIMSON_HANDLE handle, int id)
{
    auto crimson = (Crimson*)handle;

    // The end point of the selectable will be +width (same line X) and +height (next pos Y)
    float posX = crimson->sameLinePosX;
    float posY = crimson->nextPosY;

    bool pressed; bool focus;
    GetButton(crimson, id, crimson->selectableX, crimson->selectableY, posX - crimson->selectableX, posY - crimson->selectableY, &pressed, &focus);

    return CRIMSON_SUCCESS;
}

void SpinnerButton(Crimson* crimson, float widthPercent, float heightPercent, float* colour, SpinnerDirection direction)
{
    Control control;
    control.userData = crimson->userData;
    control.parentId = crimson->parentId;
    control.spinner.heightPercent = heightPercent;
    control.spinner.widthPercent = widthPercent;
    if (colour)
    {
        std::memcpy(control.spinner.colour, colour, sizeof(float) * 4);
    }
    else
    {
        control.spinner.colour[0] = 0.0f;
        control.spinner.colour[1] = 0.0f;
        control.spinner.colour[2] = 0.0f;
        control.spinner.colour[3] = 1.0f;
    }
    //control.spinner.flags = flags;
    if (crimson->sameLine)
    {
        control.spinner.posXPercent = crimson->sameLinePosX;
        control.spinner.posYPercent = crimson->sameLinePosY;
    }
    else if (crimson->samePos)
    {
        control.spinner.posXPercent = crimson->samePosX;
        control.spinner.posYPercent = crimson->samePosY;
    }
    else
    {
        control.spinner.posXPercent = crimson->nextPosX;
        control.spinner.posYPercent = crimson->nextPosY;
    }
    control.spinner.direction = direction;
    control.type = ControlType::Spinner;

    crimson->controls.insert(std::pair<int, Control>(crimson->currentId++, control));

    crimson->sameLinePosX = control.spinner.posXPercent + control.spinner.widthPercent;
    crimson->sameLinePosY = control.spinner.posYPercent;
    crimson->nextPosX = 0;
    crimson->nextPosY = control.spinner.posYPercent + control.spinner.heightPercent;
    crimson->samePosX = control.spinner.posXPercent;
    crimson->samePosY = control.spinner.posYPercent;
    crimson->sameLine = false;
    crimson->samePos = false;
}

CRIMSON_RESULT CrimsonSpinner(CRIMSON_HANDLE handle, int id, float widthPercent, float heightPercent, int min, int max, int* value, int flags)
{
    auto crimson = (Crimson*)handle;

    const float posX = crimson->samePosX;
    const float posY = crimson->samePosY;
    CrimsonSetPos(handle, posX + widthPercent * 0.5f, posY);

    char buffer[256];
    snprintf(buffer, sizeof(buffer), "%i", *value);
    CrimsonText(handle, buffer, widthPercent * 0.6f, heightPercent, 0, CRIMSON_BUTTON_FLAGS_CENTERED);

    float colour[4] = { 0.0f, 0.0f, 0.6f, 1.0f };
    float focusColour[4] = { 0.3f, 0.3f, 0.7f, 1.0f };

    bool pressed; bool focus;
    GetButton(crimson, id, posX, posY, widthPercent * 0.2f, heightPercent, &pressed, &focus);
    if (pressed)
    {
        (*value)--;
    }

    CrimsonSetPos(handle, posX, posY);
    SpinnerButton(crimson, widthPercent * 0.2f, heightPercent, focus ? focusColour : colour, SpinnerDirection::Left);
    
    GetButton(crimson, id + 1, posX + widthPercent * 0.8f, posY, widthPercent * 0.2f, heightPercent, &pressed, &focus);
    if (pressed)
    {
        (*value)++;
    }

    CrimsonSetPos(handle, posX + widthPercent * 0.8f, posY);
    SpinnerButton(crimson, widthPercent * 0.2f, heightPercent, focus ? focusColour : colour, SpinnerDirection::Right);

    (*value) = std::min(std::max(*value, min), max);

    return CRIMSON_SUCCESS;
}

CRIMSON_RESULT CrimsonPushUserData(CRIMSON_HANDLE handle, void* userData)
{
    auto crimson = (Crimson*)handle;
    crimson->userData = userData;

    return CRIMSON_SUCCESS;
}

CRIMSON_RESULT CrimsonPopUserData(CRIMSON_HANDLE handle)
{
    auto crimson = (Crimson*)handle;
    crimson->userData = nullptr;

    return CRIMSON_SUCCESS;
}
