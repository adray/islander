#pragma once
#include "Crimson.h"
#include <vector>
#include <unordered_map>

struct SelectionBox
{
    float startx;
    float endx;
    float starty;
    float endy;
    float thickness;
    float colour[4];
    bool dragging;
};

enum class ControlType
{
    Icon = 0,
    FilledRect = 1,
    Text = 2,
    Spinner = 3
};

struct Icon
{
    int imageId;
    int arrayId;
    float widthPercent;
    float heightPercent;
    float posXPercent;
    float posYPercent;
    float px;
    float py;
    float sx;
    float sy;
};

struct FilledRect
{
    float widthPercent;
    float heightPercent;
    float posXPercent;
    float posYPercent;
    float thickness;
    float colour[4];
    float borderColour[4];
};

struct Text
{
    float widthPercent;
    float heightPercent;
    float posXPercent;
    float posYPercent;
    float colour[4];
    int flags;
    int fontType;
};

struct Animation
{
    int animationId;
    float elapsed;
};

struct Button
{
    bool cleanup;
    bool hasFocus;
    bool isPressed;
    bool isPressedDown;
    Animation animation;
};

enum class SpinnerDirection
{
    Left = 0,
    Right = 1
};

struct Spinner
{
    float widthPercent;
    float heightPercent;
    float posXPercent;
    float posYPercent;
    SpinnerDirection direction;
    float colour[4];
};

struct Control
{
    int parentId;
    ControlType type;
    union
    {
        Icon icon;
        FilledRect filledRect;
        Text text;
        Spinner spinner;
    };
    std::string strText;
    void* userData;
};

struct Crimson
{
    CrimsonMouseInput mouseInput;
    CrimsonWindow window;
    std::vector<CrimsonCommand> commands;
    std::unordered_map<int, Control> controls;
    SelectionBox selectionBox; // there can only be one selection box at a time
    std::unordered_map<int, Button> buttons;
    int parentId = -1;
    int currentId = 0;
    float nextPosX;
    float nextPosY;
    float sameLinePosX;
    float sameLinePosY;
    float samePosX;
    float samePosY;
    bool sameLine;
    bool samePos;
    float selectableX;
    float selectableY;
    void* userData;
};

bool FindButton(Crimson* crimson, int id, bool* pressed, bool* focus);
void GetButton(Crimson* crimson, int id, float px, float py, float w, float h, bool* pressed, bool* focus);
