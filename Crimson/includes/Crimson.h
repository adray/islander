#pragma once

#ifdef Islander_Crimson_EXPORTS
#ifdef _WIN32
#define CRIMSON_EXPORT __declspec( dllexport )
#else
#define CRIMSON_EXPORT __attribute__ ((visibility("default")))
#endif
#else
#ifdef _WIN32
#define CRIMSON_EXPORT __declspec( dllimport ) 
#else
#define CRIMSON_EXPORT __attribute__ ((visibility("default")))
#endif
#endif

extern "C"
{
    typedef int CRIMSON_RESULT;
    typedef void* CRIMSON_HANDLE;
    typedef int CRIMSON_COMMAND_TYPE;

#define CRIMSON_FAILURE (CRIMSON_RESULT)0
#define CRIMSON_SUCCESS (CRIMSON_RESULT)1
#define CRIMSON_COMMAND_NONE (CRIMSON_COMMAND_TYPE)0
#define CRIMSON_COMMAND_LINE (CRIMSON_COMMAND_TYPE)1
#define CRIMSON_COMMAND_ICON (CRIMSON_COMMAND_TYPE)2
#define CRIMSON_COMMAND_FILL_RECT (CRIMSON_COMMAND_TYPE)3
#define CRIMSON_COMMAND_TEXT (CRIMSON_COMMAND_TYPE)4
#define CRIMSON_COMMAND_TRI (CRIMSON_COMMAND_TYPE)5
#define CRIMSON_PERCENT 0
#define CRIMSON_ABSOLUTE 1
#define CRIMSON_TEXT_FLAGS_NONE 0
#define CRIMSON_TEXT_FLAGS_CENTERED 1
#define CRIMSON_TEXT_FLAGS_WRAP 2
#define CRIMSON_BUTTON_FLAGS_NONE 0
#define CRIMSON_BUTTON_FLAGS_CENTERED 1
#define CRIMSON_BUTTON_FLAGS_SELECTED 2

    // Command instructing to draw a line
    struct CrimsonCommandLine
    {
        int absolute;
        float startX;
        float endX;
        float startY;
        float endY;
        int dotted;
        float thickness;
        float colour[4];
    };

    // Command instructing to draw an icon
    struct CrimsonCommandIcon
    {
        int imageId;
        int arrayIndex;
        float posXPercent;
        float posYPercent;
        float widthPercent;
        float heightPercent;
        float px;
        float py;
        float sx;
        float sy;
    };

    // Command instructing to draw a filled rectangle
    struct CrimsonCommandFilledRect
    {
        float widthPercent;
        float heightPercent;
        float posXPercent;
        float posYPercent;
        float colour[4];
    };

    // Command intructing to draw text
    struct CrimsonCommandText
    {
        float widthPercent;
        float heightPercent;
        float posXPercent;
        float posYPercent;
        char* text;
        float colour[4];
        int flags;
        int fontType;
    };

    // Command instructing to draw a tri
    struct CrimsonCommandTri
    {
        float posXPercent[3];
        float posYPercent[3];
        float colour[4];
    };

    struct CrimsonCommand
    {
        CRIMSON_COMMAND_TYPE type;
        void* userData;
        union
        {
            CrimsonCommandLine line;
            CrimsonCommandIcon icon;
            CrimsonCommandFilledRect filledRect;
            CrimsonCommandText text;
            CrimsonCommandTri tri;
        };
    };

    struct CrimsonMouseInput
    {
        int leftMouseDown;
        int rightMouseDown;
        int leftMouseUp;
        int rightMouseUp;
        int mouseX;
        int mouseY;
    };

    struct CrimsonWindow
    {
        int width;
        int height;
    };

    // Create a new instance of Crimson.
    CRIMSON_HANDLE CRIMSON_EXPORT CrimsonInitialize();

    // Destroy an instance of Crimson.
    void CRIMSON_EXPORT CrimsonDestroy(CRIMSON_HANDLE handle);

    // Gets the input data. Set by the user to drive interaction with crimson.
    void CRIMSON_EXPORT CrimsonGetMouseInput(CRIMSON_HANDLE handle, CrimsonMouseInput** input);

    // Gets the window data. Set by the user to drive interaction with crimson.
    void CRIMSON_EXPORT CrimsonGetWindow(CRIMSON_HANDLE handle, CrimsonWindow** window);

    // Call at the start of the frame.
    void CRIMSON_EXPORT CrimsonFrame(CRIMSON_HANDLE handle);

    // Gets the command list for rendering. Where 'commands' is an array provided by user of size 'num'.
    // This will return upto the number of maximum number of commands in the array.
    // Index is the starting point for returning commands from.
    // This returns the number of commands added. 
    // The index is modified with the internal list of where to start from should it be called again to fetch the next set.
    int CRIMSON_EXPORT CrimsonGetCommandList(CRIMSON_HANDLE handle, CrimsonCommand* commands, int num, int* index);

    // Makes the next control flow onto the same line.
    void CRIMSON_EXPORT CrimsonSameLine(CRIMSON_HANDLE handle);

    // Makes the next control overlay over the last control.
    void CRIMSON_EXPORT CrimsonSamePos(CRIMSON_HANDLE handle);

    // Moves the drawing cursor to specified x,y in screen percent.
    CRIMSON_RESULT CRIMSON_EXPORT CrimsonSetPos(CRIMSON_HANDLE handle, float xPercent, float yPercent);

    // Handle a selection box.
    CRIMSON_RESULT CRIMSON_EXPORT CrimsonSelectionBox(CRIMSON_HANDLE handle, float* colour, float thickness, float* startX, float* startY, float* endX, float* endY);

    // Draws an icon.
    CRIMSON_RESULT CRIMSON_EXPORT CrimsonIcon(CRIMSON_HANDLE handle, int imageId, float widthPercent, float heightPercent, float px, float py, float sx, float sy);

    // Draws an icon.
    CRIMSON_RESULT CRIMSON_EXPORT CrimsonIconEx(CRIMSON_HANDLE handle, int imageId, int arrayIndex, float widthPercent, float heightPercent, float px, float py, float sx, float sy);

    // Draws a filled rectangle.
    CRIMSON_RESULT CRIMSON_EXPORT CrimsonFilledRect(CRIMSON_HANDLE handle, float widthPercent, float heightPercent, float* colour, float thickness, float* border);

    // Draws text.
    // Flags:
    // CRIMSON_TEXT_FLAGS_NONE: No flags
    // CRIMSON_TEXT_FLAGS_CENTERED: Draws the text centered at the draw position.
    // CRIMSON_TEXT_FLAGS_WRAP: Draws the text with wrapping enabled.
    CRIMSON_RESULT CRIMSON_EXPORT CrimsonText(CRIMSON_HANDLE handle, const char* text, float widthPercent, float heightPercent, int fontType, int flags);

    // Draws text.
    // Flags:
    // CRIMSON_TEXT_FLAGS_NONE: No flags
    // CRIMSON_TEXT_FLAGS_CENTERED: Draws the text centered at the draw position.
    // CRIMSON_TEXT_FLAGS_WRAP: Draws the text with wrapping enabled.
    CRIMSON_RESULT CRIMSON_EXPORT CrimsonTextEx(CRIMSON_HANDLE handle, const char* text, float widthPercent, float heightPercent, int fontType, int flags, float* colour);

    // Draws a button.
    // Returns success if the button has been pressed.
    // The id supplied will be used to maintain state between calls to button across frames.
    // Flags:
    // CRIMSON_BUTTON_FLAGS_NONE: No flags
    // CRIMSON_BUTTON_FLAGS_CENTERED: Draws the button centered at the draw position.
    // CRIMSON_BUTTON_FLAGS_SELECTED: Draws the button as selected.
    CRIMSON_RESULT CRIMSON_EXPORT CrimsonButton(CRIMSON_HANDLE handle, int id, const char* text, float widthPercent, float heightPercent, int flags);

    // Begins a selectable block.
    CRIMSON_RESULT CRIMSON_EXPORT CrimsonBeginSelectable(CRIMSON_HANDLE handle, int id, int flags, int* selected);

    // Ends a selectable block.
    CRIMSON_RESULT CRIMSON_EXPORT CrimsonEndSelectable(CRIMSON_HANDLE handle, int id);

    // Draws a spinner control.
    //
    CRIMSON_RESULT CRIMSON_EXPORT CrimsonSpinner(CRIMSON_HANDLE handle, int id, float widthPercent, float heightPercent, int min, int max, int* value, int flags);

    CRIMSON_RESULT CRIMSON_EXPORT CrimsonPushUserData(CRIMSON_HANDLE handle, void* userData);
    
    CRIMSON_RESULT CRIMSON_EXPORT CrimsonPopUserData(CRIMSON_HANDLE handle);
}
