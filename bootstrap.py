import subprocess
import sys, getopt
import os

def bootstrap():
	print 'Synching source'
	if os.path.isdir('islander'):
		os.chdir('islander')
		subprocess.call(['%GIT%\git.exe', 'pull'], shell=True)
	else:
		subprocess.call(['%GIT%\git.exe', 'clone', 'https://adray@bitbucket.org/adray/islander.git'], shell=True)

def main(argv):
	targetDir = ''
	try:
		opts, args = getopt.getopt(argv, 'd:', ['-dir='])
	except getopt.GetoptError:
		print 'Usage: bootstrap -d directory'
		sys.exit(2)
	for opt, arg in opts:
		if opt == 'd':
			targetDir = arg
	if targetDir != '':
		os.chdir(targetDir)
	bootstrap()
	
if __name__ == "__main__":
   main(sys.argv[1:])