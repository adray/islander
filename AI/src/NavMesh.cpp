#include "NavMesh.h"
#include "Matrix.h"
#include "Recast.h"
#include "Polygon.h"
#include "DetourNavMesh.h"
#include "DetourNavMeshBuilder.h"
#include "DetourNavMeshQuery.h"
#include "Logger.h"
#include "RendererTypes.h"
#include "NavigationTypes.h"
#include "Vector.h"
#include <algorithm>
#include <cstring>
#include <queue>

using namespace Islander::NavMesh;
using namespace Islander::Numerics;

namespace Islander
{
    namespace NavMesh
    {
        class NavMeshContext : public rcContext
        {
        public:
            
            NavMeshContext()
            {
            }

        protected:
            virtual void doLog(const rcLogCategory category, const char* msg, const int len)
            {
                Islander::Logger::GetLogger()->Log(msg, LOGGING_RENDERING);
            }
        };
    }
}

Islander::NavMesh::Navigation* Islander::NavMesh::NavMeshCreateNavigation()
{
    auto nav = new Navigation();
    nav->data.indexCount = 0;
    nav->data.vertexCount = 0;
    nav->data.navCount = 0;
    nav->data.vertexData = nullptr;
    nav->data.indexData = nullptr;
    nav->data.navData = nullptr;
    nav->navmesh = nullptr;
    nav->query = nullptr;
    nav->position[0] = 0.0f;
    nav->position[1] = 0.0f;
    nav->position[2] = 0.0f;
    return nav;
}

void Islander::NavMesh::NavMeshAddRegion(Islander::NavMesh::Navigation* nav, Islander::NavMesh::NavMeshRegion& region)
{
    Matrix4x4 matrix(Matrix4x4Type::MATRIX_4X4_IDENTITY);
    Matrix4x4::CreateTransformationMatrix(
        region.px,
        region.py,
        region.pz,
        region.rx,
        region.ry,
        region.rz,
        region.sx,
        region.sy,
        region.sz,
        &matrix);

    region.numVerts = 0;
    region.numIndices = 0;
    for (int i = 0; i < region.polymesh->numMeshes; i++)
    {
        auto& mesh = region.polymesh->meshes[i];
        region.numVerts += mesh.numVerts;
        region.numIndices += mesh.numIndices;
    }

    region.vertexData = new float[region.numVerts * 3];
    region.indexData = new int32_t[region.numIndices];

    int vertOffset = 0;
    int indexOffset = 0;

    for (int i = 0; i < region.polymesh->numMeshes; i++)
    {
        auto& mesh = region.polymesh->meshes[i];

        memcpy(region.vertexData + vertOffset, mesh.posData, mesh.numVerts * 3 * sizeof(float));

        for (int j = 0; j < mesh.numIndices; j++)
        {
            region.indexData[indexOffset + j] = mesh.indexData[j] + (vertOffset / 3);
        }

        vertOffset += mesh.numVerts * 3;
        indexOffset += mesh.numIndices;
    }
    
    // Transform triangles
    for (int i = 0; i < region.numVerts * 3; i += 3)
    {
        Matrix4x4::TransformVector(matrix, &region.vertexData[i], &region.vertexData[i + 1], &region.vertexData[i + 2]);
    }

    nav->nodes.push_back(region);
}

void Islander::NavMesh::NavMeshClearRegions(Islander::NavMesh::Navigation* nav)
{
    for (auto& node : nav->nodes)
    {
        delete[] node.indexData;
        delete[] node.vertexData;
    }

    nav->nodes.clear();
    nav->nodes.swap(nav->nodes);
}

void Islander::NavMesh::NavMeshGenerate(Islander::NavMesh::Navigation* nav, Islander::NavMesh::NavMeshConfig& cfg)
{
    float bmin[3] = { 0, 0, 0 };
    float bmax[3] = { 0, 0, 0 };

    int triangle_count = 0;

    for (auto& region : nav->nodes)
    {
        for (int i = 0; i < region.numVerts * 3; i += 3)
        {
            float x = region.vertexData[i];
            float y = region.vertexData[i + 1];
            float z = region.vertexData[i + 2];

            bmin[0] = std::min(x, bmin[0]);
            bmin[1] = std::min(y, bmin[1]);
            bmin[2] = std::min(z, bmin[2]);

            bmax[0] = std::max(x, bmax[0]);
            bmax[1] = std::max(y, bmax[1]);
            bmax[2] = std::max(z, bmax[2]);
        }

        triangle_count = std::max(region.numIndices / 3, triangle_count);
    }

    auto& log = Islander::Logger::GetStream(LOGGING_RENDERING);
    
    NavMeshContext cxt;

    // Set voxel grid size.
    
    float cs = cfg.cs; // Voxel/grid/cell size.
    float ch = cfg.ch; // Voxel/grid/cell size.
    int w;
    int h;
    rcCalcGridSize(&bmin[0], &bmax[0], cs, &w, &h);

    // Create voxel height field.

    auto solid = rcAllocHeightfield();
    if (!rcCreateHeightfield(&cxt, *solid, w, h, &bmin[0], &bmax[0], cs, ch))
    {
        log << "(NavMesh) Error creating height field.";
        log.endl();
        return;
    }

    // Add and rasterize the triangles.

    float walkableSlopeAngle = cfg.walkableSlopeAngle;
    int walkableClimb = cfg.walkableClimb;
    unsigned char* triareas = new unsigned char[triangle_count];

    for (auto& region : nav->nodes)
    {
        memset(triareas, 0, triangle_count*sizeof(unsigned char));

        rcMarkWalkableTriangles(&cxt, walkableSlopeAngle, region.vertexData, region.numVerts, region.indexData, region.numIndices / 3, triareas);
        if (!rcRasterizeTriangles(&cxt, region.vertexData, region.numVerts, region.indexData, triareas, region.numIndices / 3, *solid, walkableClimb))
        {
            log << "(NavMesh) Error rasterising triangles.";
            log.endl();
            return;
        }
    }

    delete[] triareas;
    triareas = 0;

    //if (m_filterLowHangingObstacles)
    //    rcFilterLowHangingWalkableObstacles(m_ctx, m_cfg.walkableClimb, *m_solid);
    //if (m_filterLedgeSpans)
    //    rcFilterLedgeSpans(m_ctx, m_cfg.walkableHeight, m_cfg.walkableClimb, *m_solid);
    
    int walkableHeight = cfg.walkableHeight;
    rcFilterWalkableLowHeightSpans(&cxt, walkableHeight, *solid);

    // Optimize the data.

    auto chf = rcAllocCompactHeightfield();
    if (!rcBuildCompactHeightfield(&cxt, walkableHeight, walkableClimb, *solid, *chf))
    {
        log << "(NavMesh) Error compacting heightfield.";
        log.endl();
        return;
    }

    rcFreeHeightField(solid);
    solid = 0;

    int walkableRadius = cfg.walkableRadius;
    if (!rcErodeWalkableArea(&cxt, walkableRadius, *chf))
    {
        log << "(NavMesh) Error Erroding walkable area.";
        log.endl();
        return;
    }

    // Watershed partitioning

    if (!rcBuildDistanceField(&cxt, *chf))
    {
        log << "(NavMesh) Error building distance field.";
        log.endl();
        return;
    }

    // Partition the walkable surface into simple regions without holes.
    int minRegionArea = cfg.minRegionArea;
    int mergeRegionArea = cfg.mergeRegionArea;
    if (!rcBuildRegions(&cxt, *chf, 0, minRegionArea, mergeRegionArea))
    {
        log << "(NavMesh) Error bulding regions.";
        log.endl();
        return;
    }

    int maxEdgeLen = cfg.maxEdgeLen;
    float maxSimplificationError = cfg.maxSimplificationError;

    auto cset = rcAllocContourSet();
    if (!rcBuildContours(&cxt, *chf, maxSimplificationError, maxEdgeLen, *cset))
    {
        log << "(NavMesh) Error building contours.";
        log.endl();
        return;
    }

    int maxVertsPerPoly = cfg.maxVertsPerPoly;

    auto pmesh = rcAllocPolyMesh();
    if (!rcBuildPolyMesh(&cxt, *cset, maxVertsPerPoly, *pmesh))
    {
        log << "(NavMesh) Error building poly mesh.";
        log.endl();
        return;
    }

    auto dmesh = rcAllocPolyMeshDetail();
    
    float detailSampleDist = cfg.detailSampleDist;
    float detailSampleMaxError = cfg.detailSampleMaxError;

    if (!rcBuildPolyMeshDetail(&cxt, *pmesh, *chf, detailSampleDist, detailSampleMaxError, *dmesh))
    {
        log << "(NavMesh) Error building poly mesh detail.";
        log.endl();
        return;
    }

    rcFreeCompactHeightfield(chf);
    chf = 0;
    rcFreeContourSet(cset);
    cset = 0;

    std::vector<float> vertices;
    std::vector<int32_t> indicies;
 
    for (int i = 0; i < dmesh->nmeshes; ++i)
    {
        const unsigned int* meshDef = &dmesh->meshes[i * 4];
        const unsigned int baseVerts = meshDef[0];
        const unsigned int baseTri = meshDef[2];
        const int ntris = (int)meshDef[3];

        const float* verts = &dmesh->verts[baseVerts * 3];
        const unsigned char* tris = &dmesh->tris[baseTri * 4];

        // Iterate the sub-mesh's triangles.
        for (int j = 0; j < ntris; ++j)
        {
            const float* v1 = &verts[tris[j * 4 + 0] * 3];
            const float* v2 = &verts[tris[j * 4 + 1] * 3];
            const float* v3 = &verts[tris[j * 4 + 2] * 3];

            indicies.push_back(vertices.size() / 3);
            indicies.push_back(vertices.size() / 3 + 1);
            indicies.push_back(vertices.size() / 3 + 2);

            vertices.push_back(v1[0]);
            vertices.push_back(v1[1]);
            vertices.push_back(v1[2]);
            vertices.push_back(v2[0]);
            vertices.push_back(v2[1]);
            vertices.push_back(v2[2]);
            vertices.push_back(v3[0]);
            vertices.push_back(v3[1]);
            vertices.push_back(v3[2]);
        }
    }

    nav->data.vertexCount = vertices.size();
    nav->data.indexCount = indicies.size();

    nav->data.vertexData = new float[nav->data.vertexCount];
    memcpy(nav->data.vertexData, vertices.data(), nav->data.vertexCount * sizeof(float));

    nav->data.indexData = new int32_t[nav->data.indexCount];
    memcpy(nav->data.indexData, indicies.data(), nav->data.indexCount * sizeof(int32_t));

    for (int i = 0; i < pmesh->npolys; ++i)
    {
        pmesh->flags[i] = 1;
    }

    dtNavMeshCreateParams createParams;
    memset(&createParams, 0, sizeof(createParams));

    createParams.verts = pmesh->verts;
    createParams.vertCount = pmesh->nverts;
    createParams.polys = pmesh->polys;
    createParams.polyAreas = pmesh->areas;
    createParams.polyFlags = pmesh->flags;
    createParams.polyCount = pmesh->npolys;
    createParams.nvp = pmesh->nvp;
    createParams.detailMeshes = dmesh->meshes;
    createParams.detailVerts = dmesh->verts;
    createParams.detailVertsCount = dmesh->nverts;
    createParams.detailTris = dmesh->tris;
    createParams.detailTriCount = dmesh->ntris;
    createParams.walkableHeight = walkableHeight;
    createParams.walkableRadius = walkableRadius;
    createParams.walkableClimb = walkableClimb;
    rcVcopy(createParams.bmin, pmesh->bmin);
    rcVcopy(createParams.bmax, pmesh->bmax);
    createParams.cs = cs;
    createParams.ch = ch;
    
    if (dtStatusFailed(dtCreateNavMeshData(&createParams, &nav->data.navData, &nav->data.navCount)))
    {
        log << "(NavMesh) Unable to create navmesh data.";
        log.endl();
        return;
    }

    nav->navmesh = dtAllocNavMesh();
    auto status = nav->navmesh->init(nav->data.navData, nav->data.navCount, DT_TILE_FREE_DATA);
    if (dtStatusFailed(status))
    {
        log << "(NavMesh) Unable to initalize navmesh.";
        log.endl();
        return;
    }

    nav->query = dtAllocNavMeshQuery();
    if (dtStatusFailed(nav->query->init(nav->navmesh, 2048)))
    {
        log << "(NavMesh) Unable to initalize navmesh query.";
        log.endl();
        return;
    }
}

void ResetNavMesh(Navigation* nav)
{
    dtFreeNavMeshQuery(nav->query);
    dtFreeNavMesh(nav->navmesh);

    delete[] nav->data.indexData;
    delete[] nav->data.vertexData;

    nav->navmesh = nullptr;
    nav->query = nullptr;
    nav->data.indexData = nullptr;
    nav->data.vertexData = nullptr;
    nav->data.indexCount = 0;
    nav->data.vertexCount = 0;
    nav->data.navCount = 0;

    nav->position[0] = 0.0f;
    nav->position[1] = 0.0f;
    nav->position[2] = 0.0f;
}

void Islander::NavMesh::NavMeshDestroy(Navigation* nav)
{
    // TODO: when to free these
    //rcFreePolyMesh(nav->pmesh);
    //rcFreePolyMeshDetail(nav->dmesh);
    
    ResetNavMesh(nav);

    delete nav;
}

void CalcTriArea(NavTri& tri)
{
    float dist[3];
    rcVsub(dist, tri.verts[0].pos, tri.verts[1].pos);

    float baseLength = rcSqrt(rcVdistSqr(tri.verts[0].pos, tri.verts[1].pos));

    float mid[3] = {
        tri.verts[1].pos[0] + dist[0] / 2.0f,
        tri.verts[1].pos[1] + dist[1] / 2.0f,
        tri.verts[1].pos[2] + dist[2] / 2.0f
    };

    float height = rcSqrt(rcVdistSqr(tri.verts[2].pos, mid));

    tri.area = baseLength * height / 2.0f;
}

void InsertAtLowerBound(std::vector<NavTri>& tris, const NavTri& tri)
{
    auto& bound = std::lower_bound(tris.begin(), tris.end(), tri.area, [](NavTri& tri, float area) { return tri.area < area; });
    tris.insert(bound, tri);
}

bool Islander::NavMesh::FindPointsWithinCircle(Navigation* nav, float* pt, float radius, float* points, int max_count, int* count)
{
    float ext[3] = { 1, 5, 1 };

    float closest[3] = { 0, 0, 0 };
    dtPolyRef poly;
    dtQueryFilter filter;

    SubVec3(pt, nav->position, pt);

    if (dtStatusFailed(nav->query->findNearestPoly(pt, ext, &filter, &poly, closest)) || poly == 0)
    {
        return false;
    }

    dtPolyRef* result = new dtPolyRef[max_count];
    dtPolyRef* parent = new dtPolyRef[max_count];
    int resultCount = 0;

    if (dtStatusFailed(nav->query->findPolysAroundCircle(poly, pt, radius, &filter, result, parent, nullptr, &resultCount, max_count)) || &resultCount == 0)
    {
        delete[] result;
        delete[] parent;
        return false;
    }

    std::vector<NavTri> tris;

    for (int i = 0; i < resultCount; i++)
    {
        const dtMeshTile* tile = nullptr;
        const dtPoly* poly = nullptr;
        if (dtStatusSucceed(nav->navmesh->getTileAndPolyByRef(result[i], &tile, &poly)))
        {
            if (poly->vertCount == 3)
            {
                NavTri tri;
                rcVcopy(tri.verts[0].pos, &tile->verts[poly->verts[0] * 3]);
                rcVcopy(tri.verts[1].pos, &tile->verts[poly->verts[1] * 3]);
                rcVcopy(tri.verts[2].pos, &tile->verts[poly->verts[2] * 3]);
                CalcTriArea(tri);
                InsertAtLowerBound(tris, tri);
            }
            else
            {
                for (int j = 2; j < poly->vertCount; j++)
                {
                    NavTri tri;
                    rcVcopy(tri.verts[0].pos, &tile->verts[poly->verts[j - 2] * 3]);
                    rcVcopy(tri.verts[1].pos, &tile->verts[poly->verts[j - 1] * 3]);
                    rcVcopy(tri.verts[2].pos, &tile->verts[poly->verts[j] * 3]);
                    CalcTriArea(tri);
                    InsertAtLowerBound(tris, tri);
                }
            }
        }
    }

    delete[] result;
    delete[] parent;

    int totalTriCount = tris.size();
    *count = 0;

    if (totalTriCount > 0)
    {
        while (*count < max_count && !tris.empty())
        {
            auto& tri = tris.back();
            tris.erase(tris.begin() + tris.size() - 1);

            float mid[3] = {
                (tri.verts[0].pos[0] + tri.verts[1].pos[0] + tri.verts[2].pos[0]) / 3.0f,
                (tri.verts[0].pos[1] + tri.verts[1].pos[1] + tri.verts[2].pos[1]) / 3.0f,
                (tri.verts[0].pos[2] + tri.verts[1].pos[2] + tri.verts[2].pos[2]) / 3.0f
            };

            if (totalTriCount >= max_count)
            {
                // Return the mid-points of the triangles

                if (*count < max_count)
                {
                    rcVcopy(&points[*count * 3], mid);
                    AddVec3(&points[*count * 3], nav->position, &points[*count * 3]);
                    (*count)++;
                }
            }
            else
            {
                // Tesselate the triangle
                NavTri ntri;
                rcVcopy(ntri.verts[0].pos, mid);
                rcVcopy(ntri.verts[1].pos, tri.verts[0].pos);
                rcVcopy(ntri.verts[2].pos, tri.verts[1].pos);
                CalcTriArea(ntri);
                InsertAtLowerBound(tris, ntri);
                rcVcopy(ntri.verts[1].pos, tri.verts[1].pos);
                rcVcopy(ntri.verts[2].pos, tri.verts[2].pos);
                CalcTriArea(ntri);
                InsertAtLowerBound(tris, ntri);
                rcVcopy(ntri.verts[1].pos, tri.verts[0].pos);
                rcVcopy(ntri.verts[2].pos, tri.verts[2].pos);
                CalcTriArea(ntri);
                InsertAtLowerBound(tris, ntri);
                totalTriCount += 2; // 1 tri becomes 3 tris (+2)
            }
        }
    }

    return true;
}

bool Islander::NavMesh::FindRandomPointOnCircle(Navigation* nav, IslanderNavRandomPointOnCircle* rndPt, float (*rndFunc)(void))
{
    float v[3] = { rndPt->sx - nav->position[0], rndPt->sy - nav->position[1], rndPt->sz - nav->position[2] };
    float ext[3] = { 1, 5, 1 };

    float pt[3] = { 0, 0, 0 };
    dtPolyRef poly;
    dtQueryFilter filter;

    if (dtStatusFailed(nav->query->findNearestPoly(v, ext, &filter, &poly, pt)) || poly == 0)
    {
        return false;
    }

    constexpr int max_count = 4;
    dtPolyRef result[max_count];
    dtPolyRef parent[max_count];
    int count;

    if (dtStatusFailed(nav->query->findPolysAroundCircle(poly, v, rndPt->radius, &filter, result, parent, nullptr, &count, max_count)) || count == 0)
    {
        return false;
    }

    int index = rndFunc() * count;
    dtPolyRef r = result[index];

    rndPt->px = 0.0f;
    rndPt->py = 0.0f;
    rndPt->pz = 0.0f;

    return true;
}

bool Islander::NavMesh::NavGetClosestPoly(Navigation* nav, IslanderNavClosestPoly* closest)
{
    float v[3] = { closest->sx - nav->position[0], closest->sy - nav->position[1], closest->sz - nav->position[2] };
    float ext[3] = { 1, 5, 1 };

    float pt[3] = { 0, 0, 0 };
    dtPolyRef poly;
    dtQueryFilter filter;

    if (dtStatusFailed(nav->query->findNearestPoly(v, ext, &filter, &poly, pt)) || poly == 0)
    {
        return false;
    }

    closest->px = pt[0] + nav->position[0];
    closest->py = pt[1] + nav->position[1];
    closest->pz = pt[2] + nav->position[2];

    return true;
}

int Islander::NavMesh::NavCalculatePath(Navigation* nav, IslanderNavPath* navpath)
{
    navpath->nx = navpath->sx - nav->position[0];
    navpath->ny = navpath->sy - nav->position[1];
    navpath->nz = navpath->sz - nav->position[2];

    if (nav->query)
    {
        float v[3] = { navpath->sx - nav->position[0], navpath->sy - nav->position[1], navpath->sz - nav->position[2] };
        float ext[3] = { 1, 5, 1 };
        
        float pt[3] = { 0, 0, 0 };
        dtPolyRef poly, poly2;

        dtQueryFilter filter;

        if (dtStatusFailed(nav->query->findNearestPoly(v, ext, &filter, &poly, pt)) || poly == 0)
        {
            return ISLANDER_NAVMESH_ERROR_NOT_ON_NAVMESH;
        }

        float v2[3] = { navpath->ex - nav->position[0], navpath->ey - nav->position[1], navpath->ez - nav->position[2] };
        float pt2[3] = { 0, 0, 0 };

        if (dtStatusFailed(nav->query->findNearestPoly(v2, ext, &filter, &poly2, pt2)) || poly2 == 0)
        {
            return ISLANDER_NAVMESH_ERROR_TARGET_NOT_ON_NAVMESH;
        }

        const int maxpathsize = 20;
        dtPolyRef path[maxpathsize];
        int pathcount;

        dtStatus pathResult = nav->query->findPath(poly, poly2, v, v2, &filter, path, &pathcount, maxpathsize);
        if (dtStatusFailed(pathResult) || dtStatusDetail(pathResult, DT_PARTIAL_RESULT))
        {
            return ISLANDER_NAVMESH_ERROR_NO_PATH;
        }
        
        if (pathcount == 1)
        {
            navpath->nx = navpath->ex;
            navpath->ny = navpath->ey;
            navpath->nz = navpath->ez;
        }
        else if (pathcount > 1)
        {
            float c[3];
            bool positionOverPoly;
            if (dtStatusSucceed(nav->query->closestPointOnPoly(path[1], v, c, &positionOverPoly)))
            {
                navpath->nx = c[0] + nav->position[0];
                navpath->ny = c[1] + nav->position[1];
                navpath->nz = c[2] + nav->position[2];
            }
            else
            {
                return ISLANDER_NAVMESH_ERROR_NO_PATH;
            }
        }
    }
    else
    {
        auto& log = Islander::Logger::GetStream(LOGGING_RENDERING);
        log << "(NavMesh) No mesh query created.";
        log.endl();
        return ISLANDER_NAVMESH_ERROR_NO_PATH;
    }

    return ISLANDER_NAVMESH_SUCCESS;
}

void Islander::NavMesh::SetNavMeshPosition(Navigation* nav, float* pos)
{
    Islander::Numerics::CopyVec3(pos, nav->position);
}

void Islander::NavMesh::NavLoad(Navigation* nav, unsigned char* data, int count)
{
    auto& log = Islander::Logger::GetStream(LOGGING_RENDERING);

    ResetNavMesh(nav);
    
    nav->data.navCount = count;

    nav->data.navData = new unsigned char[count];
    memcpy(nav->data.navData, data, count);

    nav->navmesh = dtAllocNavMesh();
    auto status = nav->navmesh->init(nav->data.navData, nav->data.navCount, DT_TILE_FREE_DATA);
    if (dtStatusFailed(status))
    {
        log << "(NavMesh) Unable to initalize navmesh.";
        log.endl();
        return;
    }

    nav->query = dtAllocNavMeshQuery();
    if (dtStatusFailed(nav->query->init(nav->navmesh, 2048)))
    {
        log << "(NavMesh) Unable to initalize navmesh query.";
        log.endl();
        return;
    }
}

int Islander::NavMesh::NavRayCast(Navigation* nav, float* pt, float* end, float* t)
{
    if (nav->query)
    {
        float ext[3] = { 1, 5, 1 };
        dtQueryFilter filter;
        dtPolyRef nearest;
        float nearestPt[3];
        if (dtStatusFailed(nav->query->findNearestPoly(pt, ext, &filter, &nearest, nearestPt)) || nearest == 0)
        {
            return ISLANDER_NAVMESH_ERROR_NOT_ON_NAVMESH;
        }

        dtPolyRef endPolyRef;
        if (dtStatusFailed(nav->query->findNearestPoly(end, ext, &filter, &endPolyRef, nearestPt)) || endPolyRef == 0)
        {
            return ISLANDER_NAVMESH_ERROR_NOT_ON_NAVMESH;
        }

        if (nearest == endPolyRef)
        {
            if (t)
            {
                // If they are in the same poly, the distance is simply the distance between them.
                float dt[3];
                SubVec3(end, pt, dt);
                *t = LengthSqr(dt);
            }
        }
        else
        {
            const int MAX_PATH = 32;
            dtPolyRef path[MAX_PATH];

            dtRaycastHit hit = {};
            hit.maxPath = MAX_PATH;
            hit.path = path;

            if (dtStatusFailed(nav->query->raycast(nearest, pt, end, &filter, 0, &hit)))
            {
                return ISLANDER_NAVMESH_ERROR_NO_PATH;
            }

            if (t)
            {
                *t = hit.t; // FLT_MAX if there is not hit
            }
        }
    }
    else
    {
        auto& log = Islander::Logger::GetStream(LOGGING_RENDERING);
        log << "(NavMesh) No mesh query created.";
        log.endl();
        return ISLANDER_NAVMESH_ERROR_NO_PATH;
    }

    return ISLANDER_NAVMESH_SUCCESS;
}
