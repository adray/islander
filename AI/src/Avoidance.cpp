#include "Avoidance.h"
#include "Vector.h"
#include "NavMesh.h"
#include "DetourCrowd.h"

using namespace Islander;
using namespace Islander::Avoidance;
using namespace Islander::Numerics;

namespace Islander
{
    namespace Avoidance
    {
        struct AvoidanceManager
        {
            dtCrowd* crowd;
            NavMesh::Navigation* navigation;
            dtCrowdAgentDebugInfo agentInfo;
        };
    }
}

AvoidanceManager* Islander::Avoidance::CreateAvoidance()
{
    return new AvoidanceManager();
}

void Islander::Avoidance::InitAvoidance(AvoidanceManager* avoidance, NavMesh::Navigation* navigation)
{
    if (avoidance->crowd != nullptr)
    {
        // Already created
        return;
    }

    avoidance->crowd = dtAllocCrowd();
    avoidance->crowd->init(64, 10.0f, navigation->navmesh);
    avoidance->crowd->setExternalPhysics(true);

    avoidance->navigation = navigation;
}

void Islander::Avoidance::ResetAvoidance(AvoidanceManager* avoidance)
{
    dtFreeCrowd(avoidance->crowd);
    avoidance->crowd = nullptr;
    avoidance->navigation = nullptr;
}

ISLANDER_AVOIDANCE_AGENT Islander::Avoidance::AddAvoidanceAgent(AvoidanceManager* avoidance, const IslanderAvoidanceAgentConfig& config)
{
    if (avoidance && avoidance->crowd)
    {
        dtCrowdAgentParams params;
        std::memset(&params, 0, sizeof(params));
        params.collisionQueryRange = config.collisionQueryRange;
        params.height = config.height;
        params.maxAcceleration = config.maxAcceleration;
        params.maxSpeed = config.maxSpeed;
        params.obstacleAvoidanceType = config.obstacleAvoidanceType;
        params.pathOptimizationRange = config.pathOptimizationRange;
        params.queryFilterType = config.queryFilterType;
        params.radius = config.radius;
        params.separationWeight = config.separationWeight;
        params.updateFlags = config.updateFlags;

        float pos[3] = {};
        SubVec3(config.pos, avoidance->navigation->position, pos);

        return avoidance->crowd->addAgent(pos, &params);
    }
    else
    {
        return -1;
    }
}

void Islander::Avoidance::RemoveAvoidanceAgent(AvoidanceManager* avoidance, ISLANDER_AVOIDANCE_AGENT id)
{
    if (avoidance && avoidance->crowd)
    {
        avoidance->crowd->removeAgent(id);
    }
}

void Islander::Avoidance::UpdateAvoidanceAgentDesiredVelocity(AvoidanceManager* avoidance, ISLANDER_AVOIDANCE_AGENT agent, float* desiredVelocity)
{
    if (avoidance && avoidance->crowd)
    {
        avoidance->crowd->requestMoveVelocity(agent, desiredVelocity);
    }
}

void Islander::Avoidance::ClearAvoidanceAgentTarget(AvoidanceManager* avoidance, ISLANDER_AVOIDANCE_AGENT agent)
{
    if (avoidance && avoidance->crowd)
    {
        avoidance->crowd->resetMoveTarget(agent);
    }
}

void Islander::Avoidance::ApplyAvoidanceAgentPhysics(AvoidanceManager* avoidance, ISLANDER_AVOIDANCE_AGENT agent, float* pos)
{
    if (avoidance && avoidance->crowd)
    {
        float newPos[3] = {};
        SubVec3(pos, avoidance->navigation->position, newPos);

        avoidance->crowd->requestExternalMove(agent, newPos);
    }
}

void Islander::Avoidance::UpdateAvoidanceAgentTarget(AvoidanceManager* avoidance, ISLANDER_AVOIDANCE_AGENT agent, float* desiredTarget)
{
    if (avoidance && avoidance->crowd)
    {
        float newPos[3] = {};
        SubVec3(desiredTarget, avoidance->navigation->position, newPos);

        auto info = avoidance->crowd->getAgent(agent);

        float ext[3] = { 1, 8/*5*/, 1 };

        float pt[3] = { 0, 0, 0 };
        dtPolyRef poly;
        dtQueryFilter filter;

        if (dtStatusFailed(avoidance->navigation->query->findNearestPoly(newPos, ext, &filter, &poly, pt)) || poly == 0)
        {
            return;
        }

        avoidance->crowd->requestMoveTarget(agent, poly, newPos);
    }
}

void Islander::Avoidance::GetAvoidanceAgentData(AvoidanceManager* avoidance, ISLANDER_AVOIDANCE_AGENT agent, IslanderAvoidanceAgentData* agentData)
{
    auto info = avoidance->crowd->getAgent(agent);

    CopyVec3(info->vel, agentData->velocity);
    CopyVec3(info->npos, agentData->pos);
    CopyVec3(info->targetPos, agentData->target);

    AddVec3(agentData->pos, avoidance->navigation->position, agentData->pos);
    AddVec3(agentData->target, avoidance->navigation->position, agentData->target);
}

void Islander::Avoidance::SetAvoidanceAgentActive(AvoidanceManager* avoidance, ISLANDER_AVOIDANCE_AGENT agent, bool active)
{
    //avoidance->crowd.
}

void Islander::Avoidance::UpdateAvoidanceAgentFlags(AvoidanceManager* avoidance, ISLANDER_AVOIDANCE_AGENT agent, int flags)
{
    if (avoidance && avoidance->crowd)
    {
        auto params = avoidance->crowd->getAgent(agent)->params;
        params.updateFlags = flags;
        avoidance->crowd->updateAgentParameters(agent, &params);
    }
}

void Islander::Avoidance::AddAvoidanceType(AvoidanceManager* avoidance, int idx, IslanderAvoidanceType* type)
{
    if (avoidance && avoidance->crowd)
    {
        dtObstacleAvoidanceParams params;
        memset(&params, 0, sizeof(params));
        params.adaptiveDepth = type->adaptiveDepth;
        params.adaptiveDivs = type->adaptiveDivs;
        params.adaptiveRings = type->adaptiveRings;
        params.gridSize = type->gridSize;
        params.horizTime = type->horizTime;
        params.velBias = type->velBias;
        params.weightCurVel = type->weightCurVel;
        params.weightDesVel = type->weightDesVel;
        params.weightSide = type->weightSide;
        params.weightToi = type->weightToi;

        avoidance->crowd->setObstacleAvoidanceParams(idx, &params);
    }
}

void Islander::Avoidance::UpdateAvoidance(AvoidanceManager* avoidance, float dt)
{
    if (avoidance && avoidance->crowd)
    {
        avoidance->crowd->update(dt, &avoidance->agentInfo);
    }
}
