#pragma once
#include <vector>
#include <stdint.h>

class dtNavMesh;
class dtNavMeshQuery;
struct IslanderNavClosestPoly;
struct IslanderNavPath;
struct IslanderNavRandomPointOnCircle;

namespace Islander
{
    namespace Model
    {
        struct PolygonModel;
    }

    namespace NavMesh
    {
        struct NavMeshRegion
        {
            // Transformed vertex data
            float* vertexData;
            int32_t* indexData;
            int numVerts;
            int numIndices;

            // Mesh
            Islander::Model::PolygonModel* polymesh;

            // Object transform.
            float px, py, pz;
            float rx, ry, rz;
            float sx, sy, sz;
        };

        struct NavMeshData 
        {
            float* vertexData;
            int32_t* indexData;
            unsigned char* navData;

            int vertexCount;
            int indexCount;
            int navCount;
        };

        struct NavMeshConfig
        {
            float cs; // cell size
            float ch; // cell height
            float walkableSlopeAngle;
            int walkableClimb;
            int walkableHeight;
            int walkableRadius;
            int minRegionArea;
            int mergeRegionArea;
            int maxEdgeLen;
            float maxSimplificationError;
            int maxVertsPerPoly;
            float detailSampleDist;
            float detailSampleMaxError;
        };

        struct NavTri
        {
            struct Vec3
            {
                float pos[3];
            };

            Vec3 verts[3];
            float area;
        };

        struct Navigation
        {
            std::vector<NavMeshRegion> nodes;
            NavMeshData data;
            dtNavMesh* navmesh;
            dtNavMeshQuery* query;
            float position[3];
        };

        Navigation* NavMeshCreateNavigation();
        void NavMeshDestroy(Navigation* nav);
        void NavMeshAddRegion(Navigation* nav, NavMeshRegion& region);
        void NavMeshGenerate(Navigation* nav, NavMeshConfig& cfg);
        void NavMeshClearRegions(Navigation* nav);
        int NavCalculatePath(Navigation* nav, IslanderNavPath* path);
        void SetNavMeshPosition(Navigation* nav, float* pos);
        void NavLoad(Navigation* nav, unsigned char* data, int count);
        bool NavGetClosestPoly(Navigation* nav, IslanderNavClosestPoly* closest);
        bool FindRandomPointOnCircle(Navigation* nav, IslanderNavRandomPointOnCircle* rndPt, float (*rndFunc)(void));
        bool FindPointsWithinCircle(Navigation* nav, float* pt, float radius, float* points, int max_count, int* count);
        int NavRayCast(Navigation* nav, float* pt, float* end, float* t);
    }
}
