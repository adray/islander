#pragma once
#include "NavigationTypes.h"
#include "CollisionTypes.h"
#include <vector>


namespace Islander
{
    namespace NavMesh
    {
        struct Navigation;
    }

    namespace Avoidance
    {
        struct AvoidanceManager;

        AvoidanceManager* CreateAvoidance();
        void InitAvoidance(AvoidanceManager* avoidance, NavMesh::Navigation* navigation);
        void ResetAvoidance(AvoidanceManager* avoidance);
        ISLANDER_AVOIDANCE_AGENT AddAvoidanceAgent(AvoidanceManager* avoidance, const IslanderAvoidanceAgentConfig& config);
        void RemoveAvoidanceAgent(AvoidanceManager* avoidance, ISLANDER_AVOIDANCE_AGENT id);
        void UpdateAvoidance(AvoidanceManager* avoidance, float dt);
        void ClearAvoidanceAgentTarget(AvoidanceManager* avoidance, ISLANDER_AVOIDANCE_AGENT agent);
        void ApplyAvoidanceAgentPhysics(AvoidanceManager* avoidance, ISLANDER_AVOIDANCE_AGENT agent, float* pos);
        void UpdateAvoidanceAgentTarget(AvoidanceManager* avoidance, ISLANDER_AVOIDANCE_AGENT agent, float* desiredTarget);
        void UpdateAvoidanceAgentDesiredVelocity(AvoidanceManager* avoidance, ISLANDER_AVOIDANCE_AGENT agent, float* desiredVelocity);
        void GetAvoidanceAgentData(AvoidanceManager* avoidance, ISLANDER_AVOIDANCE_AGENT agent, IslanderAvoidanceAgentData* agentData);
        void SetAvoidanceAgentActive(AvoidanceManager* avoidance, ISLANDER_AVOIDANCE_AGENT agent, bool active);
        void UpdateAvoidanceAgentFlags(AvoidanceManager* avoidance, ISLANDER_AVOIDANCE_AGENT agent, int flags);
        void AddAvoidanceType(AvoidanceManager* avoidance, int idx, IslanderAvoidanceType* type);
    }
}
