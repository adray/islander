﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Islander.UI
{
    public interface IDataGridRow
    {
        void AddBinding(string Property, int Width);

        void AddImageBinding(string property, int width);

        string TooltipText { get; set; }

        string TooltipStyle { get; set; }
    }

    public interface IDataGridColumn
    {
        void InitializeRow(IDataGridRow row, DataGridRowTemplate rowTemplate);

        string Heading { get; set; }

        int Width { get; set; }
    }

    [Serializable]
    public class DataGridColumn : IDataGridColumn
    {
        public string Heading { get; set; }
        public string Property { get; set; }
        public int Width { get; set; }

        public DataGridColumn()
        {
            Width = 150;
        }

        public void InitializeRow(IDataGridRow row, DataGridRowTemplate rowTemplate)
        {
            row.AddBinding(Property, Width);
            if (rowTemplate != null)
            {
                if (row is Control)
                {
                    rowTemplate.Bind((Control)row);
                }

                if (!string.IsNullOrEmpty(rowTemplate.Tooltip))
                {
                    row.TooltipText = rowTemplate.Tooltip;
                }

                if (!string.IsNullOrEmpty(rowTemplate.TooltipStyle))
                {
                    row.TooltipStyle = rowTemplate.TooltipStyle;
                }
            }
        }
    }

    [Serializable]
    public class DataGridImageColumn : IDataGridColumn
    {        
        public string Heading { get; set; }
        public string Property { get; set; }
        public int Width { get; set; }

        public DataGridImageColumn()
        {
            Width = 150;
        }

        public void InitializeRow(IDataGridRow row, DataGridRowTemplate rowTemplate)
        {
            row.AddImageBinding(this.Property, this.Width);
        }
    }

    [Serializable]
    public class DataGridRowTemplate : ControlTemplate
    {
        public string Tooltip { get; set; }
        public string TooltipStyle { get; set; }
        public bool Enabled { get; set; }
    }

    public enum DataGridSelectionMode
    {
        None,
        One,
        Multiple,
    }

    [Serializable]
    public class DataGrid : Control
    {
        [Serializable]
        private class Row : Control, IDataGridRow
        {
            private bool enabled;
            private bool selected;
            protected List<Control> controls = new List<Control>();
            protected List<int> widths = new List<int>();

            public Row()
            {
                this.Height = 30;
                //this.SetImage(ImageType.Debug);
                this.enabled = true;
            }            

            public void ToggleSelected()
            {
                if (this.enabled)
                {
                    this.selected = !this.selected;
                    this.DetermineStyle();
                }
            }

            private void DetermineStyle()
            {
                foreach (var control in controls)
                {
                    TextStyle style = control.TextStyle;
                    if (enabled && selected)
                    {
                        style.Status = TextStatus.Selected;
                    }
                    else if (!enabled)
                    {
                        style.Status = TextStatus.Disabled;
                    }
                    else
                    {
                        style.Status = TextStatus.None;
                    }
                    control.SetTextStyle(style);
                }
            }

            public bool Enabled
            {
                get
                {
                    return this.enabled;
                }
                set
                {
                    this.enabled = value;
                    this.DetermineStyle();
                }
            }

            protected override void OnLayout()
            {
                base.OnLayout();

                int index = 0;
                float x = 0;
                foreach (var control in this.controls)
                {
                    var label = control as Label;
                    if (label != null)
                    {
                        label.SetLabelX(this.X, this.Width, x);
                        label.SetLabelY(this.Y, this.Height, label.Height);
                    }
                    else
                    {
                        control.X = this.X + x + control.Width - this.Width;
                        control.Y = this.Y;
                    }

                    control.Z = this.Z;
                    x += widths[index];
                    index++;
                }
            }

            public void AddBinding(string property, int width)
            {
                var label = new Label();
                label.AddBinding(new Binding(label, property, "Caption"));
                this.controls.Add(label);
                this.widths.Add(width);
                this.Add(label);
            }

            private class ImageContainer : FlowLayoutPanel
            {
                private IList data;

                public event EventHandler DataChanged;

                public IList Data
                {
                    get
                    {
                        return this.data;
                    }
                    set
                    {
                        this.data = value;

                        foreach (var child in this.Children)
                        {
                            this.Remove(child);
                        }

                        foreach (var obj in this.data)
                        {
                            var datagridimage = new DataGridImage();
                            datagridimage.Width = 20;
                            datagridimage.Height = 20;
                            datagridimage.Image = (int)Convert.ChangeType(obj, typeof(int));
                            this.Add(datagridimage);
                        }

                        if (this.DataChanged != null)
                        {
                            this.DataChanged(this, EventArgs.Empty);
                        }
                    }
                }
            }

            [Serializable]
            private class DataGridImage : Control
            {
                public int Image
                {
                    get
                    {
                        throw new InvalidOperationException();
                    }
                    set
                    {
                        this.SetCustomImage(value);
                    }
                }
            }

            public void AddImageBinding(string property, int width)
            {
                var flowlayoutpanel = new ImageContainer();
                flowlayoutpanel.Orientation = Orientation.Horizontal;
                flowlayoutpanel.AddBinding(new Binding(flowlayoutpanel, property, "Data"));
                flowlayoutpanel.DataChanged += flowlayoutpanel_DataChanged;
                this.widths.Add(width);
                this.controls.Add(flowlayoutpanel);
                this.Add(flowlayoutpanel);
            }

            void flowlayoutpanel_DataChanged(object sender, EventArgs e)
            {
                this.OnLayout();
            }
        }

        [Serializable]
        private class Header : Row
        {
            public void AppendColumn(string heading, int width)
            {
                var label = new Label();
                label.Caption = heading;
                this.controls.Add(label);
                this.widths.Add(width);
                this.Add(label);
            }
        }

        private Header header = new Header();
        private List<IDataGridColumn> columns = new List<IDataGridColumn>();
        private object dataSource;
        private List<Row> rows = new List<Row>();
        private int lastRowSelected = -1;
        private DataGridSelectionMode selectionMode = DataGridSelectionMode.One;
        private List<int> selectedRows = new List<int>();
        private DataGridRowTemplate rowTemplate;

        public DataGrid()
        {
            this.Add(header);
            //this.SetImage(ImageType.Debug);
        }

        public void AddColumn(IDataGridColumn column)
        {
            this.header.AppendColumn(column.Heading, column.Width);
            this.columns.Add(column);

            int width = 0;
            foreach (var col in this.columns)
            {
                width += col.Width;
            }

            this.Width = width / 2.0f;
        }

        public object DataSource
        {
            get
            {
                return this.dataSource;
            }
            set
            {
                IList list = value as IList;

                bool success = this.dataSource != value;
                if (!success && value != null)
                {
                    success = list.Count != this.rows.Count;
                }

                if (success)
                {
                    foreach (var row in this.rows)
                    {
                        this.Remove(row);
                    }
                    this.rows.Clear();

                    this.dataSource = value;
                    this.SuspendLayout();
                    if (list != null)
                    {
                        foreach (var item in list)
                        {
                            Row row = new Row();
                            this.rows.Add(row);
                            this.Add(row);

                            foreach (var col in columns)
                            {
                                col.InitializeRow(row, this.rowTemplate);
                            }
                            row.Connect(item);
                            row.Click += row_Click;
                        }

                        float y = this.header.Height;
                        foreach (var row in this.rows)
                        {
                            y += row.Height;
                        }
                        this.Height = y;
                    }
                    this.ResumeLayout();
                }
            }
        }

        void row_Click(object sender, EventArgs e)
        {
            if (this.selectionMode == DataGridSelectionMode.None)
            {
                return;
            }

            Row current = null;

            if (lastRowSelected != -1 && this.selectionMode == DataGridSelectionMode.One)
            {
                current = rows[lastRowSelected];
            }

            Row row = sender as Row;
            if (row.Enabled)
            {
                lastRowSelected = rows.IndexOf(row);

                if (this.selectionMode == DataGridSelectionMode.Multiple)
                {
                    if (this.selectedRows.Contains(lastRowSelected))
                    {
                        this.selectedRows.Remove(lastRowSelected);
                    }
                    else
                    {
                        this.selectedRows.Add(lastRowSelected);
                    }
                }

                row.ToggleSelected();

                if (current != null)
                {
                    current.ToggleSelected();
                }

                this.OnPropertyChanged("Selected");
            }
        }

        public DataGridSelectionMode SelectionMode
        {
            get
            {
                return this.selectionMode;
            }
            set
            {
                this.selectionMode = value;
            }
        }

        public object Selected
        {
            get
            {
                if (this.selectionMode == DataGridSelectionMode.One)
                {
                    if (lastRowSelected == -1)
                    {
                        return null;
                    }

                    var list = this.dataSource as IList;

                    if (list != null)
                    {
                        return list[lastRowSelected];
                    }
                }
                else if (this.selectionMode == DataGridSelectionMode.Multiple)
                {
                    var list = this.dataSource as IList;

                    if (list != null)
                    {
                        var data = Activator.CreateInstance(list.GetType()) as IList;
                        foreach (var item in selectedRows)
                        {
                            data.Add(list[item]);
                        }
                        return data;
                    }
                }

                return null;
            }
        }

        protected override void OnLayout()
        {
            base.OnLayout();

            this.header.X = this.X;
            this.header.Y = this.Y - this.header.Height + this.Height;
            this.header.Z = this.Z + 1;
            this.header.Width = this.Width;

            float y = this.header.Y - this.header.Height;
            foreach (var row in this.rows)
            {
                y -= row.Height;
                row.X = this.X;
                row.Y = y;
                row.Z = this.Z + 1;
                row.Width = this.Width;
                y -= row.Height;
            }
        }

        public DataGridRowTemplate Template
        {
            get
            {
                return this.rowTemplate;
            }
            set
            {
                this.rowTemplate = value;
            }
        }
    }
}
