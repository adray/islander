﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Islander.UI
{
    [Serializable]
    class FontStyleConfig
    {
        public string Name { get; set; }
        public byte FontSize { get; set; }
        public string FontColour { get; set; }
    }

    [Serializable]
    class FontColour
    {
        public float R { get; set; }
        public float G { get; set; }
        public float B { get; set; }
        public float A { get; set; }
    }

    [Serializable]
    class ColourStyleConfig
    {
        public FontColour Default { get; set; }
        public FontColour Selected { get; set; }
        public FontColour Disabled { get; set; }

        public ColourStyleConfig()
        {
            this.Default = new FontColour();
            this.Selected = new FontColour();
            this.Disabled = new FontColour();
        }
    }

    [Serializable]
    class StyleSheet
    {
        private Dictionary<string, FontStyleConfig> fonts = new Dictionary<string, FontStyleConfig>();
        private Dictionary<string, ColourStyleConfig> colours = new Dictionary<string, ColourStyleConfig>();

        private StyleSheet()
        { }

        private enum Tokens
        {
            Text,
            OpenBracket,
            CloseBracket,
            Equals,
            Colon,
        }

        private enum State
        {
            ReadTypeName,
            ReadChildElements,
        }

        private enum ElementState
        {
            ColourStyle,
            Text,
        }

        private class ParsingContext
        {
            public int ValuePos;
            public int Position;
            public List<Tokens> Tokens;
            public List<string> Values;
            
            public Tokens ReadNextToken()
            {
                return Tokens[Position++];
            }

            public string ReadString()
            {
                return Values[ValuePos++];
            }
        }

        public bool TryGetFontStyle(string name, out FontStyleConfig config)
        {
            return this.fonts.TryGetValue(name, out config);
        }

        public bool TryGetFontColour(string name, out ColourStyleConfig config)
        {            
            return this.colours.TryGetValue(name, out config);
        }

        public static StyleSheet LoadStyleSheet(string text)
        {
            int pos = 0;

            List<FontStyleConfig> configs = new List<FontStyleConfig>();
            List<Tokens> tokens = new List<Tokens>();
            List<string> values = new List<string>();

            string currentSymbol = string.Empty;

            HashSet<char> tokenMap = new HashSet<char>(new char[] { '=', '{', '}', ':' });

            while (pos < text.Length)
            {
                char nextChar = text[pos++];

                if ((tokenMap.Contains(nextChar) || char.IsWhiteSpace(nextChar)) && !string.IsNullOrWhiteSpace(currentSymbol))
                {
                    tokens.Add(Tokens.Text);
                    values.Add(currentSymbol);
                    currentSymbol = string.Empty;
                }
                
                if (nextChar == '=')
                {
                    tokens.Add(Tokens.Equals);
                }
                else if (nextChar == '{')
                {
                    tokens.Add(Tokens.OpenBracket);
                }
                else if (nextChar == '}')
                {
                    tokens.Add(Tokens.CloseBracket);
                }
                else if (nextChar == ':')
                {
                    tokens.Add(Tokens.Colon);
                }
                else if (!Char.IsWhiteSpace(nextChar))
                {
                    currentSymbol += nextChar;
                }
            }

            StyleSheet sheet = new StyleSheet();
            State state = State.ReadTypeName;
            ElementState element = ElementState.ColourStyle;
            ParsingContext cxt = new ParsingContext();
            cxt.Values = values;
            cxt.Tokens = tokens;
            ColourStyleConfig currentColour = null;
            FontStyleConfig currentFont = null;

            while (cxt.Position < cxt.Tokens.Count)
            {
                switch (state)
                {
                    case State.ReadTypeName:
                        {
                            if (cxt.ReadNextToken() != Tokens.Text)
                            {
                                throw new InvalidOperationException("Unexpected token, when expecting typename");
                            }

                            var typename = cxt.ReadString();

                            if (cxt.ReadNextToken() != Tokens.Colon || cxt.ReadNextToken() != Tokens.Text)
                            {
                                throw new InvalidOperationException("Expecting a derived type");
                            }

                            var baseType = cxt.ReadString();

                            switch (baseType)
                            {
                                case "ColourStyle":
                                case "ColorStyle":
                                    currentColour = new ColourStyleConfig();
                                    sheet.colours.Add(typename, currentColour);
                                    element = ElementState.ColourStyle;
                                    break;
                                case "Text":
                                    currentFont = new FontStyleConfig();
                                    currentFont.Name = typename;
                                    sheet.fonts.Add(typename, currentFont);
                                    element = ElementState.Text;
                                    break;
                                default:
                                    throw new InvalidOperationException("Invalid base type specified");
                            }

                            if (cxt.ReadNextToken() != Tokens.OpenBracket)
                            {
                                throw new InvalidOperationException("Child elements expected.");
                            }

                            state = State.ReadChildElements;
                        }
                        break;
                    case State.ReadChildElements:
                        {
                            var token = cxt.ReadNextToken();
                            if (token == Tokens.CloseBracket)
                            {
                                state = State.ReadTypeName;
                                currentColour = null;
                                currentFont = null;
                            }
                            else if (token == Tokens.Text)
                            {
                                var variableName = cxt.ReadString().Split('.');

                                if (cxt.ReadNextToken() != Tokens.Equals || cxt.ReadNextToken() != Tokens.Text)
                                {
                                    throw new InvalidOperationException("Unexpected token, was expecting member assignment");
                                }

                                var variableValue = cxt.ReadString();

                                string memberError = "Unexpected member variable " + variableName;

                                switch (element)
                                {
                                    case ElementState.ColourStyle:
                                        {
                                            FontColour colour = null;
                                            if (variableName[0] == "Selected")
                                            {
                                                colour = currentColour.Selected;
                                            }
                                            else if (variableName[0] == "Default")
                                            {
                                                colour = currentColour.Default;
                                            }
                                            else if (variableName[0] == "Disabled")
                                            {
                                                colour = currentColour.Disabled;
                                            }
                                            else
                                            {
                                                throw new InvalidOperationException(memberError);
                                            }

                                            if (variableName.Length == 2)
                                            {
                                                switch (variableName[1])
                                                {
                                                    case "r":
                                                        colour.R = float.Parse(variableValue);
                                                        break;
                                                    case "g":
                                                        colour.G = float.Parse(variableValue);
                                                        break;
                                                    case "b":
                                                        colour.B = float.Parse(variableValue);
                                                        break;
                                                    case "a":
                                                        colour.A = float.Parse(variableValue);
                                                        break;
                                                    default:
                                                        throw new InvalidOperationException(memberError);
                                                }
                                            }
                                            else
                                            {
                                                throw new InvalidOperationException(memberError);
                                            }
                                        }
                                        break;
                                    case ElementState.Text:
                                        {
                                            if (variableName[0] == "FontSize")
                                            {
                                                currentFont.FontSize = byte.Parse(variableValue);
                                            }
                                            else if (variableName[0] == "Colour" || variableName[0] == "Color")
                                            {
                                                currentFont.FontColour = variableValue;
                                            }
                                        }
                                        break;
                                }
                            }
                            else
                            {
                                throw new InvalidOperationException("Error parsing child elements");
                            }
                        }
                        break;
                }
            }

            return sheet;
        }
    }
}
