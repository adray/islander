﻿using System;
using System.Collections.Generic;

namespace Islander.UI
{
    public class PropertyChangedEventArgs : EventArgs
    {
        public string Property { get; private set; }

        public PropertyChangedEventArgs(string property)
        {
            this.Property = property;
        }
    }

    public interface INotifyPropertyChanged
    {
        event EventHandler<PropertyChangedEventArgs> PropertyChanged;
    }

    public interface IProperty
    {
        String Name { get; }
        object Pull();
        void Push(object obj);
    }

    public interface IExtendedProperties
    {
        IList<IProperty> GetProperties();
    }

    [Serializable]
    public class Binding
    {
        public object DataSource { get; private set; }
        public string Property { get; private set; }
        public string DataMember { get; private set; }
        public Control Control { get; private set; }
        private bool pushOnHook;

        public Binding(Control control, string dataMember, string property)
        {
            this.DataMember = dataMember;
            this.Property = property;
            this.Control = control;

            this.Control.PropertyChanged += Control_PropertyChanged;
            this.Control.Destroyed += Control_Destroyed;
        }

        void Control_Destroyed(object sender, EventArgs e)
        {
            this.Control.PropertyChanged -= Control_PropertyChanged;
            this.Control.Destroyed -= Control_Destroyed;

            INotifyPropertyChanged inpc = DataSource as INotifyPropertyChanged;
            if (inpc != null)
            {
                inpc.PropertyChanged -= inpc_PropertyChanged;
            }
        }

        void Control_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.Property == this.Property)
            {
                this.Push();
            }
        }

        public void Push()
        {
            if (this.Control != null)
            {
                if (this.DataSource == null)
                {
                    this.pushOnHook = true;
                    return;
                }

                var property = this.Control.GetType().GetProperty(this.Property);
                var data = property.GetValue(this.Control, new object[0]);

                var dataSourceProp = this.DataSource.GetType().GetProperty(this.DataMember);
                if (dataSourceProp != null)
                {
                    dataSourceProp.SetValue(this.DataSource, data, new object[0]);
                }
                else
                {
                    var extended = this.DataSource as IExtendedProperties;
                    if (extended != null)
                    {
                        foreach (var extendedProperty in extended.GetProperties())
                        {
                            if (extendedProperty.Name == this.DataMember)
                            {
                                extendedProperty.Push(data);
                                break;
                            }
                        }
                    }
                }
            }
        }

        public void Unhook()
        {
            if (this.DataSource == null)
                return;

            this.DataSource = null;
            INotifyPropertyChanged inpc = DataSource as INotifyPropertyChanged;
            if (inpc != null)
            {
                inpc.PropertyChanged -= inpc_PropertyChanged;
            }
        }

        public void Hook(object dataSource)
        {
            if (this.DataSource != null)
                return;

            this.DataSource = dataSource;
            INotifyPropertyChanged inpc = DataSource as INotifyPropertyChanged;
            if (inpc != null)
            {
                inpc.PropertyChanged += inpc_PropertyChanged;
            }

            if (this.pushOnHook)
            {
                this.Push();
            }
            else
            {
                this.Pull();
            }
        }

        void inpc_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
             if (e.Property == this.DataMember)
             {
                 this.Pull();
             }
        }

        public void Pull()
        {
            if (this.Control != null)
            {
                var property = this.Control.GetType().GetProperty(this.Property);
                if (property.CanWrite)
                {
                    if (this.DataMember == null)
                    {
                        property.SetValue(this.Control, this.DataSource.ToString(), new object[] { });
                    }
                    else
                    {
                        object value = null;

                        var dataSourceProp = this.DataSource.GetType().GetProperty(this.DataMember);
                        if (dataSourceProp != null)
                        {
                            value = dataSourceProp.GetValue(this.DataSource, new object[] { });
                        }
                        else
                        {
                            var extended = this.DataSource as IExtendedProperties;
                            if (extended != null)
                            {
                                foreach (var extendedProperty in extended.GetProperties())
                                {
                                    if (extendedProperty.Name == this.DataMember)
                                    {
                                        value = extendedProperty.Pull();
                                        break;
                                    }
                                }
                            }
                        }

                        if (property.PropertyType is IConvertible)
                        {
                            value = System.Convert.ChangeType(value, property.PropertyType);
                        }

                        if (property.PropertyType == typeof(string))
                        {
                            value = value.ToString();
                        }

                        property.SetValue(this.Control, value, new object[] { });
                    }
                }
            }
        }
    }
}