﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Islander.UI
{
    public enum Orientation
    {
        Vertical,
        Horizontal,
    }

    public enum Align
    {
        Left,
        Right,
        Center,
        Bottom,
        Top,
    }

    [Serializable]
    public class FlowLayoutPanel : Control
    {
        private Orientation orientation;
        private Align align;

        public FlowLayoutPanel()
        {
            this.orientation = Orientation.Vertical;
            this.align = Align.Center;
            //this.Height = 0;
#if DEBUG
            //this.SetImage(ImageType.Debug);
            //this.Z = 1;
#endif
        }

        private void DetermineSize()
        {
            this.SuspendLayout();
            if (this.orientation == Orientation.Vertical)
            {
                float height = 0;
                float width = this.Width;

                foreach (var control in this.Children)
                {
                    if (control.Visible)
                    {
                        height += control.Height + control.Margin.Up / 2 + control.Margin.Down / 2;
                        width = Math.Max(width, control.Width);
                    }
                }

                this.Height = height;
                this.Width = width;
            }
            else if (this.orientation == UI.Orientation.Horizontal)
            {
                float width = 0;
                float height = this.Height;

                foreach (var control in this.Children)
                {
                    if (control.Visible)
                    {
                        width += control.Width + control.Margin.Left / 2 + control.Margin.Right / 2;
                        height = Math.Max(height, control.Height);
                    }
                }

                this.Width = width;
                this.Height = height;
            }
            this.ResumeLayout();
        }

        protected override void OnAdd(Control control)
        {
            base.OnAdd(control);
            this.DetermineSize();                
        }

        protected override void OnRemove(Control control)
        {
            base.OnRemove(control);
            this.DetermineSize();
        }

        protected override void OnChildResize(Control child)
        {
            base.OnChildResize(child);
            this.DetermineSize();
        }

        protected override void OnChildVisibleChanged(Control child)
        {
            base.OnChildVisibleChanged(child);
            this.DetermineSize();
        }

        protected override void OnLayout()
        {
 	        base.OnLayout();

            if (this.orientation == Orientation.Vertical)
            {
                float y = this.Y + this.Height;

                foreach (var control in this.Children)
                {
                    if (control.Visible)
                    {
                        control.SuspendLayout();
                        y -= control.Margin.Up;

                        y -= control.Height;

                        // Move this control to correct position
                        if (this.align == Align.Center || this.align == Align.Bottom || this.align == Align.Top)
                        {
                            control.X = this.X;
                            control.Y = y;
                        }
                        else if (this.align == Align.Left)
                        {
                            control.X = this.X - this.Width + control.Width;
                            control.Y = y;
                        }
                        else if (this.align == Align.Right)
                        {
                            control.X = this.X + this.Width - control.Width;
                            control.Y = y;
                        }

                        control.Z = this.Z + 1;

                        y -= control.Height;

                        y -= control.Margin.Down;
                        control.ResumeLayout();
                    }
                }
            }
            else if (this.orientation == UI.Orientation.Horizontal)
            {
                float x = this.X - this.Width;

                foreach (var control in this.Children)
                {
                    if (control.Visible)
                    {
                        control.SuspendLayout();
                        x += control.Margin.Left;

                        x += control.Width;

                        // Move this control to correct position
                        if (this.align == Align.Center || this.align == Align.Right || this.align == Align.Left)
                        {
                            control.X = x;
                            control.Y = this.Y;
                        }
                        else if (this.align == Align.Top)
                        {
                            control.X = x;
                            control.Y = this.Y + this.Height - control.Height;

                        }
                        else if (this.align == Align.Bottom)
                        {
                            control.X = x;
                            control.Y = this.Y - this.Height + control.Height;
                        }

                        control.Z = this.Z + 1;

                        x += control.Width;

                        x += control.Margin.Right;
                        control.ResumeLayout();
                    }
                }
            }
            
            this.SetClipRectangle(this.X, this.Y, this.Width, this.Height);
        }

        public Orientation Orientation
        {
            get
            {
                return this.orientation;
            }
            set
            {
                if (this.orientation != value)
                {
                    this.orientation = value;
                    this.DetermineSize();
                    this.OnLayout();
                }
            }
        }

        public Align Align
        {
            get
            {
                return this.align;
            }
            set
            {
                if (this.align != value)
                {
                    this.align = value;
                    this.OnLayout();
                }
            }
        }
    }
}
