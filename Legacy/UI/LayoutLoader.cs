﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Islander.UI
{
    internal class LoadedLayout
    {
        public List<Control> Controls { get; set; }
        public LayoutContext Context { get; set; }
        public bool SaveDefaults { get; set; }

        public LoadedLayout()
        {
            this.Controls = new List<Control>();
            this.Context = new LayoutContext();
        }
    }

    internal class LayoutContext
    {
        public List<TooltipStyle> TooltipStyles { get; set; }

        public LayoutContext()
        {
            this.TooltipStyles = new List<TooltipStyle>();
        }
    }

    public enum MeasurementType
    {
        Pixel,
        Percentage,
    }

    [System.ComponentModel.TypeConverter(typeof(MeasurementTypeConverter))]
    [Serializable]
    public struct Measurement
    {
        public float Value { get; set; }
        public MeasurementType Type { get; set; }

        public static implicit operator float(Measurement m)
        {
            return m.Value;
        }
    }

    public class MeasurementTypeConverter : System.ComponentModel.TypeConverter
    {
        public override bool CanConvertFrom(System.ComponentModel.ITypeDescriptorContext context, Type sourceType)
        {
            if (sourceType == typeof(string))
            {
                return true;
            }

            return base.CanConvertFrom(context, sourceType);
        }

        public override object ConvertFrom(System.ComponentModel.ITypeDescriptorContext context, System.Globalization.CultureInfo culture, object value)
        {
            if (value == null)
            {
                return new Measurement();
            }
            else if (value.GetType() == typeof(string))
            {
                Measurement m = new Measurement();
                var str = (string)value;
                if (str.EndsWith("%"))
                {
                    m.Type = MeasurementType.Percentage;
                    str = str.Substring(0, str.Length - 1);
                }
                else
                {
                    m.Type = MeasurementType.Pixel;
                }

                m.Value = (float) Convert.ChangeType(str, typeof(float));

                if (m.Type == MeasurementType.Percentage)
                {
                    m.Value /= 100.0f;
                }

                return m;
            }

            return base.ConvertFrom(context, culture, value);
        }
    }

    public interface ILayoutNode
    {
        string GetAttribute(String name);
        IEnumerable<ILayoutNode> SelectNodes(String name);
        void Fetch<T>(object obj, string property);
    }

    public interface ILayoutExtender
    {
        Control LoadControl(ILayoutNode node);
    }
    
    internal static class LayoutLoader
    {
        private class LayoutLoaderNode : ILayoutNode
        {
            private System.Xml.XmlNode node;
            private HashSet<string> overridenProperties = new HashSet<string>();

            public LayoutLoaderNode(System.Xml.XmlNode node)
            {
                this.node = node;
            }

            public string GetAttribute(string name)
            {
                var attribute = node.Attributes[name];
                return attribute == null ? string.Empty : attribute.Value;
            }

            public IEnumerable<ILayoutNode> SelectNodes(string name)
            {
                foreach (System.Xml.XmlNode child in this.node.SelectNodes(name))
                {
                    yield return new LayoutLoaderNode(child);
                }
            }

            public void Fetch<T>(object obj, string property)
            {
                overridenProperties.Add(property);
                Read<T>(this.node, obj, property);
            }

            public bool IsOverriden(string property)
            {
                return this.overridenProperties.Contains(property);
            }
        }

        public static LoadedLayout LoadLayout(string xml, StyleSheet styles, Dictionary<string, Type> extender)
        {
            CacheControlBindings();

            System.Xml.XmlDocument doc = new System.Xml.XmlDocument();
            doc.LoadXml(xml);

            LoadedLayout layout = new LoadedLayout();

            var node = doc.SelectSingleNode("Layout");

            var saveDefaults = node.Attributes["SaveDefaults"];
            if (saveDefaults != null)
            {
                layout.SaveDefaults = Convert.ToBoolean(saveDefaults.Value);
            }

            layout.Controls.AddRange(LoadControls(node, null, layout.Context, styles, extender));
            return layout;
        }

        private static void CacheControlBindings()
        {
            if (loadedBindingDefinitions)
            {
                return;
            }
            
            bindingDefinitions.Add(typeof(FlowLayoutPanel), new List<ControlBinding>()
                {
                    new ControlBinding() { PropertyName = "Orientation", PropertyType = typeof(Orientation)},
                    new ControlBinding() { PropertyName = "Align", PropertyType = typeof(Align)}
                });
            bindingDefinitions.Add(typeof(SelectionPanel), new List<ControlBinding>()
                {
                    new ControlBinding() { PropertyName = "Display", PropertyType = typeof(string)},
                    new ControlBinding() { PropertyName = "DataSource", PropertyType = typeof(object)},
                    new ControlBinding() { PropertyName = "Property", PropertyType = typeof(string)},
                    new ControlBinding() { PropertyName = "MaxCount", PropertyType = typeof(int)},
                    new ControlBinding() { PropertyName = "SelectedItems", PropertyType = typeof(object)}
                });
            bindingDefinitions.Add(typeof(Button), new List<ControlBinding>()
                {
                    new ControlBinding() { PropertyName = "Caption", PropertyType = typeof(string)},
                    new ControlBinding() { PropertyName = "Command", PropertyType = typeof(string)},
                });
            bindingDefinitions.Add(typeof(Checkbox), new List<ControlBinding>()
                {
                    new ControlBinding() { PropertyName = "Caption", PropertyType = typeof(string)},
                    new ControlBinding() { PropertyName = "Checked", PropertyType = typeof(bool)},
                });
            bindingDefinitions.Add(typeof(CheckboxList), new List<ControlBinding>()
                {
                    new ControlBinding() { PropertyName = "DataSource", PropertyType = typeof(string)},
                    new ControlBinding() { PropertyName = "Columns", PropertyType = typeof(int)},
                });
            bindingDefinitions.Add(typeof(Window), new List<ControlBinding>()
                {
                    new ControlBinding() { PropertyName = "Caption", PropertyType = typeof(string)},
                    new ControlBinding() { PropertyName = "HideExit", PropertyType = typeof(YesNo)},
                    new ControlBinding() { PropertyName = "HideMinimize", PropertyType = typeof(YesNo)},
                });
            bindingDefinitions.Add(typeof(Slider), new List<ControlBinding>()
                {
                    new ControlBinding() { PropertyName = "Min", PropertyType = typeof(float)},
                    new ControlBinding() { PropertyName = "Max", PropertyType = typeof(float)},
                    new ControlBinding() { PropertyName = "Caption", PropertyType = typeof(string)},
                    new ControlBinding() { PropertyName = "SliderValue", PropertyType = typeof(int)},
                });
            bindingDefinitions.Add(typeof(TableLayoutPanelCell), new List<ControlBinding>()
                {
                    new ControlBinding() { PropertyName = "Column", PropertyType = typeof(int)},
                    new ControlBinding() { PropertyName = "Row", PropertyType = typeof(int)},
                });
            bindingDefinitions.Add(typeof(DataGrid), new List<ControlBinding>()
                {
                    new ControlBinding() { PropertyName = "DataSource", PropertyType = typeof(object)},
                    new ControlBinding() { PropertyName = "Selected", PropertyType = typeof(object)},
                    new ControlBinding() { PropertyName = "SelectionMode", PropertyType = typeof(DataGridSelectionMode)},
                });
            bindingDefinitions.Add(typeof(LabelControl), new List<ControlBinding>()
                {
                    new ControlBinding() { PropertyName = "Caption", PropertyType = typeof(String)},
                    new ControlBinding() { PropertyName = "Wrap", PropertyType = typeof(bool)},
                    new ControlBinding() { PropertyName = "Centered", PropertyType = typeof(bool)},
                });
            bindingDefinitions.Add(typeof(Combobox), new List<ControlBinding>()
                {
                    new ControlBinding() { PropertyName = "Caption", PropertyType = typeof(String)},
                    new ControlBinding() { PropertyName = "DataSource", PropertyType = typeof(object)},
                    new ControlBinding() { PropertyName = "SelectedItem", PropertyType = typeof(object)},
                });
            bindingDefinitions.Add(typeof(Spinner), new List<ControlBinding>()
                {
                    new ControlBinding() { PropertyName = "Property", PropertyType = typeof(string)},
                    new ControlBinding() { PropertyName = "DataSource", PropertyType = typeof(object)},
                    new ControlBinding() { PropertyName = "SelectedItem", PropertyType = typeof(object)},
                    new ControlBinding() { PropertyName = "ErrorText", PropertyType = typeof(string)},
                });

            typeMap.Add("FlowLayoutPanel", typeof(FlowLayoutPanel));
            typeMap.Add("SelectionPanel", typeof(SelectionPanel));
            typeMap.Add("Button", typeof(Button));
            typeMap.Add("Checkbox", typeof(Checkbox));

            typeMap.Add("CheckboxList", typeof(CheckboxList));
            typeMap.Add("Window", typeof(Window));
            typeMap.Add("Slider", typeof(Slider));
            typeMap.Add("TableLayoutPanel", typeof(TableLayoutPanel));

            typeMap.Add("Cell", typeof(TableLayoutPanelCell));
            typeMap.Add("DataGrid", typeof(DataGrid));
            typeMap.Add("Label", typeof(LabelControl));
            typeMap.Add("Combobox", typeof(Combobox));

            typeMap.Add("BoundedContainer", typeof(BoundedContainer));
            typeMap.Add("Frame", typeof(Frame));
            typeMap.Add("Spinner", typeof(Spinner));

            loadedBindingDefinitions = true;
            // unsupported: Contextmenu, SortBy, CheckboxListTemplate, Rows, Columns,Column,ImageColumn,DataGridRowTemplate,
            // ComboboxItemTemplate, TooltipStyle
        }

        private class BindableAttribute : Attribute
        {

        }

        private class ControlBinding
        {
            public Type PropertyType { get; set; }
            public string PropertyName { get; set; }
        }

        private static Dictionary<string, Type> typeMap = new Dictionary<string, Type>();
        private static Dictionary<Type, List<ControlBinding>> bindingDefinitions = new Dictionary<Type, List<ControlBinding>>();
        private static bool loadedBindingDefinitions;

        private static List<Control> LoadControls(System.Xml.XmlNode layout, Control parent, LayoutContext context, StyleSheet styles, Dictionary<string, Type> extender)
        {
            List<Control> controls = new List<Control>();

            foreach (System.Xml.XmlNode node in layout.ChildNodes)
            {
                Control ctrl = null;

                Type controlType = null;
                if (typeMap.TryGetValue(node.Name, out controlType))
                {
                    ctrl = (Control)Activator.CreateInstance(controlType);

                    List<ControlBinding> bindings = null;
                    if (bindingDefinitions.TryGetValue(controlType, out bindings))
                    {
                        foreach (var binding in bindings)
                        {
                            Read(node, ctrl, binding.PropertyName, binding.PropertyType);
                        }
                    }
                }
                else
                {
                    // unsupported: Contextmenu, SortBy, CheckboxListTemplate, Rows, Columns,Column,ImageColumn,DataGridRowTemplate,
                    // ComboboxItemTemplate, TooltipStyle

                    if (node.Name == "ContextMenu")
                    {
                        var host = parent as IContextMenuHost;
                        if (host != null)
                        {
                            host.Menu = new ContextMenu();
                            Read<object>(node, host.Menu, "DataSource");
                        }
                    }
                    else if (node.Name == "SortBy")
                    {
                        var panel = parent as SelectionPanel;
                        if (panel != null)
                        {
                            var sort = new SelectionPanelSort();
                            Read<string>(node, sort, "Caption");
                            Read<SelectionPanelSortType>(node, sort, "Sort");
                            Read<string>(node, sort, "Property");
                            panel.AddSort(sort);
                        }
                    }
                    else if (node.Name == "CheckboxListTemplate")
                    {
                        CheckboxList list = parent as CheckboxList;
                        if (list != null)
                        {
                            CheckboxListTemplate template = new CheckboxListTemplate();
                            Read<string>(node, template, "Caption");
                            Read<bool>(node, template, "Checked");
                            Read<int>(node, template, "Width");
                            list.Template = template;
                        }
                    }
                    else if (node.Name == "Rows")
                    {
                        var table = parent as TableLayoutPanel;
                        if (table != null)
                        {
                            foreach (System.Xml.XmlNode rowNode in node.SelectNodes("Row"))
                            {
                                var row = new TableLayoutPanelRow();
                                Read<Measurement>(rowNode, row, "Height");
                                table.Rows.Add(row);
                            }
                        }
                    }
                    else if (node.Name == "Columns")
                    {
                        var table = parent as TableLayoutPanel;
                        if (table != null)
                        {
                            foreach (System.Xml.XmlNode colNode in node.SelectNodes("Column"))
                            {
                                var col = new TableLayoutPanelColumn();
                                Read<Measurement>(colNode, col, "Width");
                                table.Columns.Add(col);
                            }
                        }
                    }
                    else if (node.Name == "Column")
                    {
                        DataGrid grid = parent as DataGrid;
                        if (grid != null)
                        {
                            DataGridColumn column = new DataGridColumn();
                            Read<string>(node, column, "Property");
                            Read<string>(node, column, "Heading");
                            Read<int>(node, column, "Width");
                            grid.AddColumn(column);
                        }
                    }
                    else if (node.Name == "ImageColumn")
                    {
                        DataGrid grid = parent as DataGrid;
                        if (grid != null)
                        {
                            DataGridImageColumn column = new DataGridImageColumn();
                            Read<string>(node, column, "Property");
                            Read<string>(node, column, "Heading");
                            Read<int>(node, column, "Width");
                            grid.AddColumn(column);
                        }
                    }
                    else if (node.Name == "DataGridRowTemplate")
                    {
                        DataGrid grid = parent as DataGrid;
                        if (grid != null)
                        {
                            DataGridRowTemplate row = new DataGridRowTemplate();
                            Read<bool>(node, row, "Enabled");
                            Read<string>(node, row, "TooltipText");
                            Read<string>(node, row, "TooltipStyle");
                            grid.Template = row;
                        }
                    }
                    else if (node.Name == "ComboboxItemTemplate")
                    {
                        Combobox combobox = parent as Combobox;
                        if (combobox != null)
                        {
                            ComboboxItemTemplate template = new ComboboxItemTemplate();
                            Read<string>(node, template, "Caption");
                            Read<string>(node, template, "TooltipText");
                            Read<string>(node, template, "TooltipStyle");
                            combobox.Template = template;
                        }
                    }
                    else if (node.Name == "TooltipStyle")
                    {
                        TooltipStyle style = new TooltipStyle();
                        Read<int>(node, style, "Delay");
                        Read<int>(node, style, "Width");
                        Read<int>(node, style, "Height");
                        Read<string>(node, style, "Name");
                        Read<TooltipAlign>(node, style, "Align");
                        context.TooltipStyles.Add(style);
                    }
                    else
                    {
                        Type type = null;
                        if (extender.TryGetValue(node.Name, out type))
                        {
                            if (type.GetInterfaces().Contains(typeof(ILayoutExtender)))
                            {
                                try
                                {
                                    ILayoutExtender extended = (ILayoutExtender)Activator.CreateInstance(type);
                                    var loader = new LayoutLoaderNode(node);
                                    ctrl = extended.LoadControl(loader);
                                    if (ctrl != null)
                                    {
                                        // Search up the inheritance heirarchy for bindings

                                        HashSet<string> overrides = new HashSet<string>();

                                        Type ancestor = ctrl.GetType();

                                        while (ancestor != null)
                                        {
                                            List<ControlBinding> bindings = null;
                                            if (bindingDefinitions.TryGetValue(ancestor, out bindings))
                                            {
                                                foreach (var binding in bindings)
                                                {
                                                    if (!loader.IsOverriden(binding.PropertyName) &&
                                                        !overrides.Contains(binding.PropertyName))
                                                    {
                                                        overrides.Add(binding.PropertyName);
                                                        Read(node, ctrl, binding.PropertyName, binding.PropertyType);
                                                    }
                                                }
                                            }

                                            ancestor = ancestor.BaseType;
                                        }
                                    }
                                }
                                catch (Exception ex)
                                {
                                    Managed.Logger.Log("Failed to create extension loader " + node.Name, Managed.LoggingLevels.Managed);
                                    throw ex;
                                }
                            }
                            else
                            {
                                Managed.Logger.Log(node.Name + " is not a valid layout extender.", Managed.LoggingLevels.Managed);
                            }
                        }
                    }
                }

                if (ctrl != null)
                {
                    if (node.Attributes["Anchor"] != null)
                    {
                        ctrl.Anchor = (Anchor) Enum.Parse(typeof(Anchor), node.Attributes["Anchor"].Value);
                    }

                    bool exlusiveFocus = false;
                    if (node.Attributes["Focus"] != null)
                    {
                        exlusiveFocus = node.Attributes["Focus"].Value.ToLower() == "yes";
                    }

                    ctrl.SuspendLayout();
                    Read<float>(node, ctrl, "X");
                    Read<float>(node, ctrl, "Y");
                    Read<float>(node, ctrl, "Z");
                    Read<float>(node, ctrl, "Width");
                    Read<float>(node, ctrl, "Height");
                    Read<bool>(node, ctrl, "Visible");
                    Read<string>(node, ctrl, "TooltipText");
                    Read<string>(node, ctrl, "TooltipStyle");

                    var style = node.Attributes["Style"];
                    if (style != null)
                    {
                        var textStyle = ctrl.TextStyle;
                        
                        FontStyleConfig config = null;
                        if (styles.TryGetFontStyle(style.Value, out config))
                        {
                            textStyle.FontSize = config.FontSize;
                            textStyle.FontColour = config.FontColour;
                        }

                        ctrl.SetTextStyle(textStyle);
                    }

                    var mar = node.SelectSingleNode("Margin");
                    if (mar != null)
                    {
                        Padding margin = new Padding();
                        Read<float>(mar, margin, "Left");
                        Read<float>(mar, margin, "Right");
                        Read<float>(mar, margin, "Up");
                        Read<float>(mar, margin, "Down");

                        ctrl.Margin = margin;
                    }

                    var pad = node.SelectSingleNode("Padding");
                    if (pad != null)
                    {
                        Padding padding = new Padding();
                        Read<float>(pad, padding, "Left");
                        Read<float>(pad, padding, "Right");
                        Read<float>(pad, padding, "Up");
                        Read<float>(pad, padding, "Down");

                        ctrl.Padding = padding;
                    }

                    var triggers = node.SelectNodes("Trigger");
                    foreach (System.Xml.XmlNode trigger in triggers)
                    {
                        TriggerType type = TriggerType.OnLoad;

                        var storyboard = trigger.SelectSingleNode("Storyboard");
                        if (storyboard != null)
                        {
                            var story = new Storyboard();

                            var animations = storyboard.SelectNodes("Animator");
                            if (animations != null)
                            {
                                foreach (System.Xml.XmlNode animation in animations)
                                {
                                    string property = null;
                                    float duration = 0;
                                    string start = null;
                                    string end = null;

                                    foreach (System.Xml.XmlAttribute attribute in animation.Attributes)
                                    {
                                        if (attribute.Name == "Property")
                                        {
                                            property = attribute.Value;
                                        }
                                        else if (attribute.Name == "Duration")
                                        {
                                            duration = (float)System.Convert.ChangeType(attribute.Value, typeof(float));
                                        }
                                        else if (attribute.Name == "Start")
                                        {
                                            start = attribute.Value;
                                        }
                                        else if (attribute.Name == "End")
                                        {
                                            end = attribute.Value;
                                        }
                                    }

                                    var animator = new Animator(ctrl, property, duration);
                                    animator.Begin = start;
                                    animator.End = end;
                                    story.Animations.Add(animator);
                                }
                            }

                            ctrl.AddAnimation(type, story);
                        }
                    }

                    controls.Add(ctrl);
                    foreach (var child in LoadControls(node, ctrl, context, styles, extender))
                    {
                        ctrl.Add(child);
                    }
                    ctrl.ResumeLayout();

                    if (exlusiveFocus)
                    {
                        UIContainer.Default.Focus(ctrl);
                    }
                }
            }

            return controls;
        }

        private static void Read<T>(System.Xml.XmlNode node, object obj, string property)
        {
            Read(node, obj, property, typeof(T));
        }

        private static void Read(System.Xml.XmlNode node, object obj, string property, Type type)
        {
            var prop = node.Attributes[property];
            if (prop != null)
            {
                var value = prop.Value.Trim();
                if (value.StartsWith("{") && value.EndsWith("}"))
                {
                    Control control = obj as Control;
                    ControlTemplate template = obj as ControlTemplate;
                    if (control != null)
                    {
                        string[] action = value.Substring(1, value.Length - 2).Split(' ');
                        if (action.Length >= 2 && action[0].ToLower() == "binding")
                        {
                            control.AddBinding(new Binding(control, action[1], property));
                        }
                    }
                    else if (template != null)
                    {
                        string[] action = value.Substring(1, value.Length - 2).Split(' ');
                        if (action.Length >= 2 && action[0].ToLower() == "binding")
                        {
                            template.AddBinding(action[1], property);
                        }
                    }
                    else
                    {
                        throw new InvalidOperationException("Binding to " + property + " could not be made.");
                    }
                }
                else if (type.IsEnum)
                {
                    var def = obj.GetType().GetProperty(property);
                    def.SetValue(obj, Enum.Parse(type, prop.Value), new object[0]);
                }
                else
                {
                    var def = obj.GetType().GetProperty(property);
                    var c = System.ComponentModel.TypeDescriptor.GetConverter(type);
                    if (c != null && c.CanConvertFrom(prop.Value.GetType()))
                    {
                        def.SetValue(obj, c.ConvertFrom(prop.Value), new object[0]);
                    }
                    else
                    {
                        def.SetValue(obj, System.Convert.ChangeType(prop.Value, type), new object[0]);
                    }
                }
            }
        }

        public static string PeekName(string xml)
        {
            System.Xml.XmlDocument doc = new System.Xml.XmlDocument();
            doc.LoadXml(xml);

            return doc.SelectSingleNode("Layout").Attributes["Name"].Value;
        }

        public static Dictionary<string, Type> LoadExtensions(string xml)
        {
            Dictionary<string, Type> types = new Dictionary<string,Type>();

            System.Xml.XmlDocument doc = new System.Xml.XmlDocument();
            doc.LoadXml(xml);

            var extensions = doc.SelectSingleNode("Extensions");
            foreach (System.Xml.XmlNode node in extensions.SelectNodes("Extension"))
            {
                var control = node.Attributes["Control"];
                var handler = node.Attributes["Handler"];
                if (control != null && handler != null)
                {
                    var assemblies = AppDomain.CurrentDomain.GetAssemblies();
                    foreach (var assembly in assemblies)
                    {
                        Type type = assembly.GetType(handler.Value);
                        if (type != null)
                        {
                            types.Add(control.Value, type);
                            break;
                        }
                    }
                }
            }

            return types;
        }
    }
}
