﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Islander.UI
{
    public class DragEventArgs : EventArgs
    {
        public float X { get; private set; }
        public float Y { get; private set; }

        public DragEventArgs(float x, float y)
        {
            this.X = x;
            this.Y = y;
        }
    }

    /// <summary>
    /// Provides a border, title bar and ability to close the window.
    /// </summary>
    [Serializable]
    public class Window : Control, IControlDefaults
    {
        [Serializable]
        private class TitleBar : Control
        {
            private bool minimized;
            private bool hideExit;
            private bool hideMinimize = true;
            private Label label;
            private ButtonBase exitButton;
            private ButtonBase minimizeButton;
            public event EventHandler Exit;
            public event EventHandler<DragEventArgs> OnDragging;
            public event EventHandler Minimize;
            public event EventHandler Maximize;

            public TitleBar()
            {
                this.SuspendLayout();
                // Add text and sprite components
                this.SetImage(ImageType.Titlebar);
                this.label = new Label();
                this.label.CanClickThrough = true;
                this.exitButton = new Button();
                this.Add(this.label);
                this.minimizeButton = new ButtonBase();

                this.Height = 30;

                // Create exit button
                this.exitButton.SetImage(ImageType.Close);
                this.exitButton.Width = 20;
                this.exitButton.Height = 20;
                this.exitButton.Margin = new Padding()
                {
                    Right = 10,
                    Left = 10
                };
                this.Add(exitButton);

                // Create minimize/maximize button
                this.minimizeButton.SetImage(ImageType.Minimize);
                this.minimizeButton.Width = 20;
                this.minimizeButton.Height = 20;
                this.minimizeButton.Margin = new Padding()
                {
                    Right = 10,
                };
                this.minimizeButton.Hide();
                this.Add(minimizeButton);

                this.exitButton.Click += exitButton_Click;
                this.minimizeButton.Click += MinimizeButton_Click;
                this.exitButton.Caption = string.Empty;
                this.ResumeLayout();
            }

            public bool Minimized
            {
                get
                {
                    return this.minimized;
                }
                set
                {
                    if (this.minimized != value)
                    {
                        this.minimized = value;
                        this.OnMinimizedChanged();
                    }
                }
            }

            private void MinimizeButton_Click(object sender, EventArgs e)
            {
                this.minimized = !this.minimized;
                OnMinimizedChanged();
            }

            private void OnMinimizedChanged()
            {
                if (this.minimized)
                {
                    this.minimizeButton.SetImage(ImageType.Maximize);
                    this.OnMinimize();
                }
                else
                {
                    this.minimizeButton.SetImage(ImageType.Minimize);
                    this.OnMaximize();
                }
            }

            private void OnMinimize()
            {
                if (this.Minimize != null)
                {
                    this.Minimize(this, EventArgs.Empty);
                }
            }

            private void OnMaximize()
            {
                if (this.Maximize != null)
                {
                    this.Maximize(this, EventArgs.Empty);
                }
            }

            protected override void OnMouseClick()
            {
                base.OnMouseClick();
                UIContainer.Default.StartDragging(this);
            }

            protected override void OnMouseUp()
            {
                base.OnMouseUp();
                UIContainer.Default.EndDragging(this);
            }

            protected override void OnDrag(float mouseX, float mouseY)
            {
                base.OnDrag(mouseX, mouseY);
                if (this.OnDragging != null)
                {
                    this.OnDragging(this, new DragEventArgs(mouseX, mouseY));
                }
            }

            void exitButton_Click(object sender, EventArgs e)
            {
                if (Exit != null)
                {
                    Exit(this, EventArgs.Empty);
                }
            }

            public string Caption
            {
                set
                {
                    this.label.Caption = value;
                }
                get
                {
                    return this.label.Caption;
                }
            }

            protected override void OnLayout()
            {
                base.OnLayout();

                this.label.Width = this.Width;
                this.label.Height = this.Height;
                this.label.SetLabelX(this.X, this.Width, this.Padding.Left);
                this.label.SetLabelY(this.Y, this.Height, this.Padding.Up);
                this.label.Z = this.Z;

                this.exitButton.Z = this.Z + 1;
                this.exitButton.X = this.X + this.Width - this.exitButton.Width - this.exitButton.Margin.Right;
                this.exitButton.Y = this.Y;

                this.minimizeButton.Z = this.Z + 1;
                this.minimizeButton.X = this.exitButton.X - this.exitButton.Width - this.exitButton.Margin.Left - this.minimizeButton.Width - this.minimizeButton.Margin.Right;
                this.minimizeButton.Y = this.Y;

                this.SetClipRectangle(this.X, this.Y, this.Width, this.Height);
            }

            public bool HideExit
            {
                get
                {
                    return this.hideExit;
                }
                set
                {
                    if (this.hideExit != value)
                    {
                        this.hideExit = value;
                        if (this.hideExit)
                        {
                            this.exitButton.Hide();
                        }
                        else
                        {
                            this.exitButton.Show();
                        }
                    }
                }
            }

            public bool HideMinimize
            {
                get
                {
                    return this.hideMinimize;
                }
                set
                {
                    if (this.hideMinimize != value)
                    {
                        this.hideMinimize = value;
                        if (this.hideMinimize)
                        {
                            this.minimizeButton.Hide();
                        }
                        else
                        {
                            this.minimizeButton.Show();
                        }
                    }
                }
            }
        }

        private readonly TitleBar titleBar;
        private float previousHeight;
        private bool suspendMinimization;

        public Window()
        {
            this.titleBar = new TitleBar();
            this.Add(titleBar);
            this.titleBar.Exit += titleBar_Exit;
            this.titleBar.OnDragging += titleBar_OnDragging;
            this.titleBar.Maximize += TitleBar_Maximize;
            this.titleBar.Minimize += TitleBar_Minimize;
            this.SetImage(ImageType.Window);
        }

        private void TitleBar_Minimize(object sender, EventArgs e)
        {
            if (!this.suspendMinimization)
            {
                this.previousHeight = this.Height;
                this.Height = this.titleBar.Height;
                this.Y += this.previousHeight - this.Height;
                foreach (var c in Children)
                {
                    if (c != titleBar)
                    {
                        c.Hide();
                    }
                }
            }
        }

        private void TitleBar_Maximize(object sender, EventArgs e)
        {
            if (!this.suspendMinimization)
            {
                this.Y += this.Height - this.previousHeight;
                this.Height = this.previousHeight;
                foreach (var c in Children)
                {
                    if (c != titleBar)
                    {
                        c.Show();
                    }
                }
            }
        }

        void titleBar_OnDragging(object sender, DragEventArgs e)
        {
            this.X = e.X;
            this.Y = e.Y - Height + this.titleBar.Height;
        }

        void titleBar_Exit(object sender, EventArgs e)
        {
            // Close the window
            UIContainer.Default.Close(this);
        }

        public string Caption
        {
            set
            {
                this.titleBar.Caption = value;
            }
            get
            {
                return this.titleBar.Caption;
            }
        }
        
        public YesNo HideExit
        {
            get
            {
                return this.titleBar.HideExit ? YesNo.Yes : YesNo.No;
            }
            set
            {
                this.titleBar.HideExit = value == YesNo.Yes;
            }
        }

        public YesNo HideMinimize
        {
            get
            {
                return this.titleBar.HideMinimize ? YesNo.Yes : YesNo.No;
            }
            set
            {
                this.titleBar.HideMinimize = value == YesNo.Yes;
            }
        }

        protected override void OnChildResize(Control child)
        {
            base.OnChildResize(child);
            this.OnLayout();
        }

        protected override void OnChildVisibleChanged(Control child)
        {
            base.OnChildVisibleChanged(child);
            this.OnLayout();
        }

        protected override void OnLayout()
        {
            base.OnLayout();

            this.titleBar.X = this.X;
            this.titleBar.Y = this.Y + this.Height - this.titleBar.Height;
            this.titleBar.Width = this.Width;

            foreach (var child in this.Children)
            {
                if (child != this.titleBar)
                {
                    child.Z = this.Z + 1;
                    if (child.Anchor == UI.Anchor.Top || child.Anchor == UI.Anchor.None)
                    {
                        child.Y = this.Y + this.Height - child.Height - this.titleBar.Height * 2;
                        child.X = this.X;
                    }
                    else if (child.Anchor == UI.Anchor.Bottom)
                    {
                        child.Y = this.Y - this.Height + child.Height;
                        child.X = this.X;
                    }
                    else if (child.Anchor == UI.Anchor.Left)
                    {
                        child.Y = this.Y - this.titleBar.Height * 2;
                        child.X = this.X - this.Width + child.Width;
                    }
                    else if (child.Anchor == UI.Anchor.Right)
                    {
                        child.Y = this.Y - this.titleBar.Height * 2;
                        child.X = this.X + this.Width - child.Width;
                    }
                }
                else
                {
                    titleBar.Z = this.Z + 5;
                }
            }

            this.SetClipRectangle(this.X, this.Y, this.Width, this.Height);
        }

        public void LoadDefaults(List<KeyValuePair<string, object>> defaults)
        {
            this.suspendMinimization = true;
            foreach (var @default in defaults)
            {
                if (@default.Key == "PreviousHeight")
                {
                    this.previousHeight = (float)@default.Value;
                }
                else if (@default.Key == "Minimized")
                {
                    this.titleBar.Minimized = (bool)@default.Value;
                    if (this.titleBar.Minimized)
                    {
                        foreach (var c in Children)
                        {
                            if (c != titleBar)
                            {
                                c.Hide();
                            }
                        }
                    }
                }
            }
            this.suspendMinimization = false;
        }

        public List<KeyValuePair<string, object>> SaveDefaults()
        {
            return new List<KeyValuePair<string, object>>()
            {
                new KeyValuePair<string, object>("PreviousHeight", this.previousHeight),
                new KeyValuePair<string, object>("Minimized", this.titleBar.Minimized)
            };
        }
    }
}
