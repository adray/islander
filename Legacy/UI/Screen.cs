﻿using Islander.Managed;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;

namespace Islander.UI
{
    public class Resolution
    {
        public int Width { get; set; }
        public int Height { get; set; }

        public override string ToString()
        {
            return string.Format("{0}x{1}", this.Width, this.Height);
        }
    }

    public static class Screen
    {
        public static List<Resolution> Resolutions
        {
            get
            {
                HashSet<string> uniqueRes = new HashSet<string>();
                List<Resolution> list = new List<Resolution>();

                NativeMethods.DEVMODE mode = new NativeMethods.DEVMODE();
                int i = 0;
                while (NativeMethods.EnumDisplaySettings(null, i, ref mode))
                {
                    var res = new Resolution()
                    {
                        Width = mode.dmPelsWidth,
                        Height = mode.dmPelsHeight,
                    };
                    if (!uniqueRes.Contains(res.ToString()))
                    {
                        list.Add(res);
                        uniqueRes.Add(res.ToString());
                    }
                    i++;
                }

                return list;
            }
        }
    }
}
