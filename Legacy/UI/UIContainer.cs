﻿using Islander.Managed;
using Islander.Managed.Scripting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Islander.UI
{
    public interface IContainer
    {
        event EventHandler Resized;

        void Invoke(string command, object obj=null);
        void Activate(string layout, object dataSource = null, string tag = null);
        void ActivateInto(string layout, object dataSource = null, string tag = null);
        void ActivateInto(Control control, string tag = null);
        IEntity Allocate();
        void BindTexture(IEntity handle, long material);
        void BindFont(IEntity handle, TextStyle style);
        int Width { get; }
        int Height { get; }
        void Resize(int width, int height);
        void Close(Control control);
        void StartDragging(Control control);
        void EndDragging(Control control);
        void Focus(Control control);
        void LoseFocus();
        void Destroy(IEntity entity);
        void Pop();
        void Close(string tag);
        void ShowTooltip(Control control, string text);
        void HideTooltip(Control control);
        void AddListener(IUIListener listener, string tag);
        void RemoveListener(IUIListener listener, string tag);
        void Use(IStoryboard storyboard);
        void Tick(float delta);
        void MeasureText(UIMeasureText text);
        void AddTooltipStyle(TooltipStyle style, string tag=null);
        void SaveDefaults(IDefaultsWriter writer);
        void LoadDefaults(IDefaultsReader reader);

        IResourceIterator ResourceIterator { get; set; }
        bool IsFocused { get; }
    }

    public interface IAnimationClock
    {
        void Tick(float delta);
    }

    public interface IUIListener
    {
        void OnClose();
    }

    public interface IFocus
    {
        void FocusClickMiss();
    }

    public enum YesNo
    {
        No,
        Yes,
    }

    public interface IControlDefaults
    {
        void LoadDefaults(List<KeyValuePair<string, object>> defaults);
        List<KeyValuePair<string, object>> SaveDefaults();
    }

    public interface IDefaultsWriter
    {
        void WriteBeginLayout(string layout);
        void WriteEndLayout();
        void WriteBeginDefaultControl();
        void WriteEndDefaultControl();
        void WriteDefault(string key, object value);
    }

    public interface IDefaultsReader
    {
        int GetLayoutCount();
        string GetNextLayout();
        int GetControlCount();
        void GetNextControl();
        int GetDefaultCount();
        string GetNextDefault();
        object GetDefaultValue();
    }

    [Serializable]
    public abstract class Command
    {
        public string Name { get; private set; }

        protected internal abstract void Invoke(object obj);

        internal static List<Command> CreateFromXml(string xml)
        {
            var assemblies = AppDomain.CurrentDomain.GetAssemblies();
            Dictionary<string, System.Reflection.Assembly> indexed = new Dictionary<string,System.Reflection.Assembly>();
            foreach (var assembly in assemblies)
            {
                indexed.Add(assembly.GetName().Name, assembly);
            }

            List<Command> commands = new List<Command>();
            System.Xml.XmlDocument doc = new System.Xml.XmlDocument();
            doc.LoadXml(xml);
            var node = doc.SelectSingleNode("Commands");
            foreach (System.Xml.XmlNode command in node.SelectNodes("Command"))
            {
                string name = command.Attributes["Name"].Value;
                string type = command.Attributes["Type"].Value;
                string assembly = command.Attributes["Assembly"].Value;

                Type loadedType = indexed[assembly].GetType(type);
                if (loadedType != null)
                {
                    Command inst = Activator.CreateInstance(loadedType) as Command;
                    if (inst != null)
                    {
                        inst.Name = name;
                        commands.Add(inst);
                    }
                }
                else
                {
                    Logger.Log(string.Format("{0} not found", type), LoggingLevels.Managed);
                }
            }

            return commands;
        }
    }

    public interface IResourceIterator
    {
        string GetNextCommandXml();
        string GetNextLayoutXml();
        string GetNextStyleSheet();
        string GetNextExtensionFile();
        void Reset();
    }

    internal class DefaultResourceIterator : IResourceIterator
    {
        string[] commands;
        int commandIndex = -1;
        
        string[] layouts;
        int layoutIndex = -1;

        string[] styles;
        int styleIndex = -1;

        string[] extenders;
        int extenderIndex = -1;

        public DefaultResourceIterator()
        {
            this.commands = GetFiles("Commands", "*.xml");
            this.layouts = GetFiles("Layouts", "*.xml");
            this.styles = GetFiles("Layouts", "*.ss");
            this.extenders = GetFiles("Layouts", "*.ext");
        }

        private static string[] GetFiles(string folder, string extension)
        {
            try
            {
                string path = System.IO.Path.Combine(System.IO.Directory.GetCurrentDirectory(), folder);
                return System.IO.Directory.GetFiles(path, extension);
            }
            catch
            {
                return new string[0];
            }
        }

        private static string GetNextContent(string[] files, ref int index)
        {
            index++;
            if (index < files.Length)
            {
                return System.IO.File.ReadAllText(files[index]);
            }
            return null;
        }

        public string GetNextCommandXml()
        {
            return GetNextContent(commands, ref commandIndex);
        }

        public string GetNextLayoutXml()
        {
            return GetNextContent(layouts, ref layoutIndex);
        }

        public string GetNextStyleSheet()
        {
            return GetNextContent(styles, ref styleIndex);
        }

        public string GetNextExtensionFile()
        {
            return GetNextContent(extenders, ref extenderIndex);
        }

        public void Reset()
        {
            commandIndex = -1;
            layoutIndex = -1;
            styleIndex = -1;
            extenderIndex = -1;
        }
    }

    public enum FocusChange
    {
        FocusGot,
        FocusLost,
        FocusChild,
    }

    public class UIMeasureText
    {
        public float Width { get; set; }
        public float Height { get; set; }
        public TextStyle Style { get; set; }
        public bool Wrap { get; set; }
        public string Text { get; set; }
    }

    public interface IUIService
    {
        void ApplyMaterial(IEntity entity, long imageType);
        void ApplyFont(IEntity entity, TextStyle style);
        void Focus(IEntity entity, FocusChange ev);
        bool Enabled { get; set; }
        void MeasureText(UIMeasureText text);
    }

    /// <summary>
    /// This class processes messages from the native host.
    /// For example: mouse click and mouse move, these are
    /// then handled by the appropriate control.
    /// </summary>
    [Serializable]
    public class UIContainer : Islander.Managed.Scripting.IScript, IContainer, IDeserializationCallback
    {
        private Control exclusiveFocus;
        private Stack<Control> previousFocus = new Stack<Control>();
        private static UIContainer @default;
        [NonSerialized]
        private Dictionary<string, Command> commands = new Dictionary<string, Command>();
        [NonSerialized]
        private Dictionary<string, string> layouts = new Dictionary<string, string>();
        [NonSerialized]
        private StyleSheet styles = null;
        [NonSerialized]
        private Dictionary<string, Type> extensions = new Dictionary<string, Type>();
        private Container container;
        private Control dragging;
        private Stack<Container> states = new Stack<Container>();
        [NonSerialized]
        private HashSet<Control> hasRaisedRaiseEvent = new HashSet<Control>();
        private Dictionary<string, LoadedLayout> tagged = new Dictionary<string, LoadedLayout>();
        private Dictionary<string, List<IUIListener>> listeners = new Dictionary<string, List<IUIListener>>();
        private IResourceIterator iterator;
        private Tooltip tooltip;
        private AnimationSet animations = new AnimationSet();
        private Dictionary<string, LayoutDefaults> defaults = new Dictionary<string, LayoutDefaults>();

        public event EventHandler Resized;

        private class LayoutDefaults
        {
            private class ControlDefaults
            {
                public List<KeyValuePair<string, object>> Properties { get; set; }

                public ControlDefaults()
                {
                    this.Properties = new List<KeyValuePair<string, object>>();
                }
            }

            private List<ControlDefaults> defaults = new List<ControlDefaults>();
            
            public void Restore(List<Control> layout)
            {
                if (this.defaults.Count == layout.Count)
                {
                    for (int i = 0; i < layout.Count; i++)
                    {
                        var controlDefault = defaults[i];
                        var control = layout[i];
                        foreach (var propertyDefault in controlDefault.Properties)
                        {
                            var prop = control.GetType().GetProperty(propertyDefault.Key);
                            if (prop != null && prop.CanWrite)
                            {
                                try
                                {
                                    prop.GetSetMethod().Invoke(control, new object[] { propertyDefault.Value });
                                }
                                catch (System.Reflection.TargetInvocationException)
                                {
                                    // nothing
                                }
                            }
                        }

                        IControlDefaults controlDefaults = control as IControlDefaults;
                        if (controlDefaults != null)
                        {
                            controlDefaults.LoadDefaults(controlDefault.Properties);
                        }
                    }
                }
            }

            public void Restore(IDefaultsWriter writer)
            {
                foreach (var @default in this.defaults)
                {
                    writer.WriteBeginDefaultControl();

                    foreach (var property in @default.Properties)
                    {
                        writer.WriteDefault(property.Key, property.Value);
                    }

                    writer.WriteEndDefaultControl();
                }
            }

            public void Store(IDefaultsReader reader)
            {
                int controls = reader.GetControlCount();
                for (int j = 0; j < controls; j++)
                {
                    ControlDefaults defaults = new ControlDefaults();

                    reader.GetNextControl();
                    int defaultCount = reader.GetDefaultCount();
                    for (int k = 0; k < defaultCount; k++)
                    {
                        string key = reader.GetNextDefault();
                        object value = reader.GetDefaultValue();

                        defaults.Properties.Add(new KeyValuePair<string, object>(key, value));
                    }

                    this.defaults.Add(defaults);
                }
            }

            public void Store(List<Control> layout)
            {
                this.defaults.Clear();

                foreach (var control in layout)
                {
                    ControlDefaults controlDefault = new ControlDefaults();

                    foreach (var property in control.GetType().GetProperties())
                    {
                        var type = property.PropertyType;
                        if ((type.IsPrimitive || type.IsEnum) && property.CanRead)
                        {
                            var value = property.GetGetMethod().Invoke(control, new object[0]);
                            controlDefault.Properties.Add(new KeyValuePair<string, object>(property.Name, value));
                        }
                    }

                    IControlDefaults controlDefaults = control as IControlDefaults;
                    if (controlDefaults != null)
                    {
                        controlDefault.Properties.AddRange(controlDefaults.SaveDefaults());
                    }

                    defaults.Add(controlDefault);
                }
            }
        }

        [Serializable]
        private class AnimationSet : IAnimationClock
        {
            private LazyQueue<IAnimator> running = new LazyQueue<IAnimator>();

            public void Tick(float delta)
            {
                foreach (var item in running)
                {
                    item.Update(delta);
                    if (item.IsComplete)
                    {
                        running.Remove(item);
                    }
                }
            }

            public void Add(IStoryboard storyboard)
            {
                this.running.AddRange(storyboard.Animations);
                foreach (var animation in storyboard.Animations)
                {
                    animation.Start();
                }
            }
        }

        [Serializable]
        private class Container : Control
        {
            protected override void OnChildResize(Control child)
            {
                base.OnChildResize(child);

                this.OnLayout();
            }

            protected override void OnLayout()
            {
                base.OnLayout();

                foreach (var child in this.Children)
                {
                    if (child.Anchor == UI.Anchor.Left || child.Anchor == UI.Anchor.BottomLeft || child.Anchor == UI.Anchor.TopLeft)
                    {
                        child.X = -this.Width + child.Width + child.Padding.Left;
                    }
                    if (child.Anchor == UI.Anchor.Right || child.Anchor == UI.Anchor.BottomRight || child.Anchor == UI.Anchor.TopRight)
                    {
                        child.X = this.Width - child.Width - child.Padding.Right;
                    }
                    if (child.Anchor == UI.Anchor.Top || child.Anchor == UI.Anchor.TopRight || child.Anchor == UI.Anchor.TopLeft)
                    {
                        child.Y = this.Height - child.Height - child.Padding.Up;
                    }
                    if (child.Anchor == UI.Anchor.Bottom || child.Anchor == UI.Anchor.BottomLeft || child.Anchor == UI.Anchor.BottomRight)
                    {
                        child.Y = -this.Height + child.Height + child.Padding.Down;
                    }
                }
            }

            protected override bool ClickThrough
            {
                get
                {
                    return true;
                }
            }
        }

        public bool IsFocused
        {
            get
            {
                return this.exclusiveFocus != null;
            }
        }

        public IAnimationClock Clock
        {
            get
            {
                return this.animations;
            }
        }

        public static IContainer Default
        {
            get
            {
                return @default;
            }
        }

        public IResourceIterator ResourceIterator
        {
            get
            {
                return this.iterator;
            }
            set
            {
                if (this.ResourceIterator != value)
                {
                    // Find all the layouts and command xml files and index them.
                    this.iterator = value;

                    this.commands.Clear();
                    this.layouts.Clear();

                    LoadResources();
                }
            }
        }

        private void LoadResources()
        {
            this.iterator.Reset();

            string resource = null;
            while ((resource = this.iterator.GetNextStyleSheet()) != null)
            {
                this.styles = StyleSheet.LoadStyleSheet(resource);
            }

            while ((resource = this.iterator.GetNextExtensionFile()) != null)
            {
                foreach (var entry in LayoutLoader.LoadExtensions(resource))
                {
                    this.extensions.Add(entry.Key, entry.Value);
                }
            }

            while ((resource = this.iterator.GetNextLayoutXml()) != null)
            {
                this.layouts.Add(LayoutLoader.PeekName(resource), resource);
            }

            while ((resource = this.iterator.GetNextCommandXml()) != null)
            {
                foreach (var cmd in Command.CreateFromXml(resource))
                {
                    commands.Add(cmd.Name, cmd);
                }
            }
        }

        public void Tick(float delta)
        {
            this.Clock.Tick(delta);
            this.tooltip.Tick(delta);
        }

        public UIContainer()
        {
            ResourceIterator = new DefaultResourceIterator();
            @default = this;
        }

        public override void OnActivate()
        {
        }

        public override void OnLoad()
        {
            this.container = new Container();
            this.container.Width = Width;
            this.container.Height = Height;
            this.tooltip = new Tooltip(this.System);

            this.System.Site.ServiceAvailable += Site_ServiceAvailable;

            var windowing = this.System.Site.GetService<IWindowingService>();
            if (windowing != null)
            {
                SetupWindowing(windowing);
            }

            var input = this.System.Site.GetService<IInputService>();
            if (input != null)
            {
                SetupInput(input);
            }
        }

        private void SetupWindowing(IWindowingService windowing)
        {
            this.Width = windowing.Width;
            this.Height = windowing.Height;
            windowing.Resized += windowing_Resized;
        }

        private void SetupInput(IInputService input)
        {
            input.LeftMouseDown += input_LeftMouseDown;
            input.LeftMouseUp += input_LeftMouseUp;
            input.MouseMove += input_MouseMove;
            input.KeyPress += input_KeyPress;
        }

        void Site_ServiceAvailable(object sender, ServiceAvailableEventArgs e)
        {
            if (e.Type == typeof(IWindowingService))
            {
                SetupWindowing((IWindowingService)e.Service);
            }
            else if (e.Type == typeof(IInputService))
            {
                SetupInput((IInputService)e.Service);
            }
        }

        void input_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (this.exclusiveFocus != null)
            {
                this.exclusiveFocus.PerformKeyPress(e);
            }

            /*Control control = Intersect(e.X, e.Y);
            if (control != null && control != this.exclusiveFocus)
            {
                control.PerformKeyPress(e.Key);
            }*/
        }

        void input_MouseMove(object sender, MouseEventArgs e)
        {
            if (this.exclusiveFocus != null)
            {
                this.exclusiveFocus.PerformMouseMove(e.X, e.Y);
            }

            Control control = Intersect(e.X, e.Y);
            if (control != null && control != this.exclusiveFocus)
            {
                control.PerformMouseMove(e.X, e.Y);
            }

            if (dragging != null)
            {
                dragging.Drag(e.X, e.Y);
            }

            e.Handled = true;
        }

        void input_LeftMouseUp(object sender, MouseEventArgs e)
        {
            if (this.dragging != null)
            {
                this.dragging.PerformMouseUp(e.X, e.Y);
            }
            this.dragging = null;

            Control control = Intersect(e.X, e.Y);
            if (control != null)
            {
                if (control.PerformMouseUp(e.X, e.Y))
                {
                    e.Handled = true;
                }
            }
            else if (this.exclusiveFocus != null)
            {
                e.Handled = true;
            }
        }

        void input_LeftMouseDown(object sender, MouseEventArgs e)
        {
            Control control = Intersect(e.X, e.Y);
            if (control != null)
            {
                if (control.PerformClick(e.X, e.Y))
                {
                    e.Handled = true;
                }
            }
            else if (this.exclusiveFocus != null)
            {
                IFocus focus = this.exclusiveFocus as IFocus;
                if (focus != null)
                {
                    focus.FocusClickMiss();
                }

                e.Handled = true;
            }
        }

        void windowing_Resized(object sender, EventArgs e)
        {
            var windowing = this.System.Site.GetService<IWindowingService>();
            if (windowing != null)
            {
                this.Resize(windowing.Width, windowing.Height);
            }
        }

        public override void OnTriggered()
        {
        }
        
        private Control Intersect(float x, float y)
        {
            if (exclusiveFocus != null)
            {
                if (exclusiveFocus.Intersects(x, y))
                {
                    return exclusiveFocus;
                }
                return null;
            }
            return container;
        }
        
        public void Activate(string layout, object dataSource = null, string tag = null)
        {
            this.container.Hide();
            states.Push(container);
            container = new Container();
            container.Width = Width;
            container.Height = Height;
            ActivateInto(layout, dataSource, tag);
        }

        public void ActivateInto(string layout, object dataSource = null, string tag = null)
        {
            // Loads the layout into the toplevelcontrols.
            if (layouts.ContainsKey(layout))
            {
                List<Control> controlList = new List<Control>();
                string xml = layouts[layout];
                LoadedLayout loaded = LayoutLoader.LoadLayout(xml, styles, extensions);
                foreach (var control in loaded.Controls)
                {
                    control.Connect(dataSource);
                    control.OnLoad();
                    this.container.Add(control);
                    control.Tag = tag;
                    controlList.Add(control);
                }

                if (!string.IsNullOrEmpty(tag))
                {
                    this.tagged.Add(tag, loaded);

                    LayoutDefaults defaults = null;
                    if (this.defaults.TryGetValue(tag, out defaults))
                    {
                        defaults.Restore(controlList);
                    }
                }

                if (this.exclusiveFocus != null)
                {
                    this.FocusChild(this.exclusiveFocus);
                }

                this.tooltip.AddStyles(tag, loaded.Context.TooltipStyles);
            }
        }

        public void AddTooltipStyle(TooltipStyle style, string tag=null)
        {
            if (style != null)
            {
                this.tooltip.AddStyle(tag, style);
            }
        }

        public void Pop()
        {
            dragging = null;

            foreach (var child in this.container.Children)
            {
                this.container.Remove(child);
            }

            this.container = states.Pop();
            this.container.Show();
            this.container.Width = this.Width;
            this.container.Height = this.Height;
        }

        public void Invoke(string command, object obj=null)
        {
            // Invokes the command specified.
            if (commands.ContainsKey(command))
            {
                commands[command].Invoke(obj);
            }
        }

        public IEntity Allocate()
        {
            return this.System.World.CreateEntity();
        }

        public void BindTexture(IEntity handle, long material)
        {
            IUIService service = this.System.Site.GetService<IUIService>();
            if (service != null)
            {
                service.ApplyMaterial(handle, material);
            }
        }

        public void BindFont(IEntity handle, TextStyle style)
        {
            var sprite = handle.GetComponent<ITextSprite>();
            ColourStyleConfig config = null;
            if (style.FontColour != null && this.styles.TryGetFontColour(style.FontColour, out config))
            {
                FontColour colour = null;

                switch (style.Status)
                {
                    case TextStatus.None:
                        colour = config.Default;
                        break;
                    case TextStatus.Disabled:
                        colour = config.Disabled;
                        break;
                    case TextStatus.Selected:
                        colour = config.Selected;
                        break;
                }

                sprite.SetColour(colour.R, colour.G, colour.B, colour.A);
            }

            IUIService service = this.System.Site.GetService<IUIService>();
            if (service != null)
            {
                service.ApplyFont(handle, style);
            }
        }

        #region Window Dimensions
        private int width;
        private int height;

        [Binding("Window", "Width")]
        public int Width
        {
            get
            {
                return width;
            }
            set
            {
                this.width = value;
                this.container.Width = value;
            }
        }

        [Binding("Window", "Height")]
        public int Height
        {
            get
            {
                return height;
            }
            set
            {
                this.height = value;
                this.container.Height = value;
            }
        }
        #endregion

        public void Close(Control control)
        {
            if (control == exclusiveFocus)
            {
                this.LoseFocus();
            }

            this.container.Remove(control);
            if (!string.IsNullOrEmpty(control.Tag))
            {
                List<Control> taggedControls = this.tagged[control.Tag].Controls;
                taggedControls.Remove(control);

                if (taggedControls.Count == 0)
                {
                    this.tagged.Remove(control.Tag);
                    this.OnClose(control.Tag);
                }

                this.tooltip.ClearStyles(control.Tag);
            }
        }

        public void StartDragging(Control control)
        {
            if (dragging == null)
            {
                dragging = control;
            }
        }

        public void EndDragging(Control control)
        {
            if (dragging == control)
            {
                dragging = null;
            }
        }

        public void Focus(Control control)
        {
            if (this.exclusiveFocus != null)
            {
                this.previousFocus.Push(this.exclusiveFocus);
                this.LoseFocus(this.exclusiveFocus);
            }

            dragging = null;

            this.hasRaisedRaiseEvent.Clear();
            this.exclusiveFocus = control;

            var service = this.System.Site.GetService<IUIService>();
            service.Focus(control.Handle, FocusChange.FocusGot);

            this.FocusChild(control);
        }

        private void FocusChild(Control control)
        {
            foreach (var child in control.Children)
            {
                if (!this.hasRaisedRaiseEvent.Contains(child))
                {
                    var service = this.System.Site.GetService<IUIService>();
                    service.Focus(child.Handle, FocusChange.FocusChild);

                    this.hasRaisedRaiseEvent.Add(child);
                }
                this.FocusChild(child);
            }
        }

        public void LoseFocus()
        {
            dragging = null;

            if (previousFocus.Count > 0)
            {
                // Regain focus on the last control.
                this.exclusiveFocus = this.previousFocus.Pop();
                this.FocusChild(this.exclusiveFocus);
            }
            else
            {
                this.EndFocus();
            }
        }

        private void LoseFocus(Control control)
        {
            var service = this.System.Site.GetService<IUIService>();
            service.Focus(control.Handle, FocusChange.FocusLost);
            foreach (var child in control.Children)
            {
                LoseFocus(child);
            }
        }

        private void EndFocus()
        {
            var service = this.System.Site.GetService<IUIService>();
            service.Focus(this.exclusiveFocus != null ? this.exclusiveFocus.Handle : null, FocusChange.FocusLost);

            this.exclusiveFocus = null;
        }

        public void Destroy(IEntity entity)
        {
            this.System.World.RemoveEntity(entity);
        }

        public void ActivateInto(Control control, string tag = null)
        {
            this.container.Add(control);
            if (!string.IsNullOrEmpty(tag))
            {
                this.tagged.Add(tag, new LoadedLayout()
                {
                    Controls = new List<Control>()
                {
                    control,
                }
                });
            }
        }
        
        public void Resize(int width, int height)
        {
            this.container.SuspendLayout();
            this.Width = width;
            this.Height = height;
            this.container.ResumeLayout();
            
            if (this.Resized != null)
            {
                this.Resized(this, EventArgs.Empty);
            }
        }

        public void Close(string tag)
        {
            LoadedLayout layout = null;
            if (tagged.TryGetValue(tag, out layout))
            {
                tagged.Remove(tag);

                if (layout.SaveDefaults)
                {
                    LayoutDefaults defaults = null;
                    if (!this.defaults.TryGetValue(tag, out defaults))
                    {
                        defaults = new LayoutDefaults();
                        this.defaults.Add(tag, defaults);
                    }
                    defaults.Store(layout.Controls);
                }

                foreach (var control in layout.Controls)
                {
                    if (this.exclusiveFocus == control)
                    {
                        this.EndFocus();
                    }

                    this.container.Remove(control);
                }
                this.OnClose(tag);
            }

            this.tooltip.ClearStyles(tag);
        }

        public void ShowTooltip(Control control, string text)
        {
            if (this.tooltip != null)
            {
                this.tooltip.Show(text, control);
            }
        }

        public void HideTooltip(Control control)
        {
            if (this.tooltip != null)
            {
                this.tooltip.Hide(control);
            }
        }

        private void OnClose(string tag)
        {
            if (tag != null)
            {
                List<IUIListener> listeners = null;
                if (this.listeners.TryGetValue(tag, out listeners))
                {
                    foreach (var listener in listeners)
                    {
                        listener.OnClose();
                    }

                    this.listeners.Remove(tag);
                }
            }
        }
        
        public void AddListener(IUIListener listener, string tag)
        {
            if (tag == null)
            {
                throw new InvalidOperationException("Tag cannot be null");
            }

            List<IUIListener> listeners = null;
            if (!this.listeners.TryGetValue(tag, out listeners))
            {
                listeners = new List<IUIListener>();
                this.listeners.Add(tag, listeners);
            }
            listeners.Add(listener);
        }

        public void RemoveListener(IUIListener listener, string tag)
        {
            if (tag == null)
            {
                throw new InvalidOperationException("Tag cannot be null");
            }

            List<IUIListener> listeners = null;
            if (this.listeners.TryGetValue(tag, out listeners))
            {
                listeners.Remove(listener);
            }
        }

        public void Use(IStoryboard storyboard)
        {
            this.animations.Add(storyboard);
        }

        public void MeasureText(UIMeasureText text)
        {
            var ui = this.System.Site.GetService<IUIService>();
            if (ui != null)
            {
                ui.MeasureText(text);
            }
        }

        public void OnDeserialization(object sender)
        {
            this.commands = new Dictionary<string, Command>();
            this.layouts = new Dictionary<string, string>();
            this.extensions = new Dictionary<string, Type>();
            this.hasRaisedRaiseEvent = new HashSet<Control>();

            this.LoadResources();

            // Close all existing layout and switch to this one
            foreach (var layout in @default.tagged.Values.ToList())
            {
                foreach (var c in layout.Controls.ToList())
                {
                    @default.Close(c);
                }
            }

            @default = this;
        }

        public void SaveDefaults(IDefaultsWriter writer)
        {
            foreach (var @default in this.defaults)
            {
                writer.WriteBeginLayout(@default.Key);
                @default.Value.Restore(writer);
                writer.WriteEndLayout();
            }
        }

        public void LoadDefaults(IDefaultsReader reader)
        {
            int count = reader.GetLayoutCount();
            for (int i = 0; i < count; i++)
            {
                string layout = reader.GetNextLayout();

                LayoutDefaults defaults = new LayoutDefaults();
                defaults.Store(reader);
                this.defaults.Add(layout, defaults);
            }
        }
    }
}
