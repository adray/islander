﻿using Islander.Managed;
using Islander.Managed.Scripting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Islander.UI
{
    public enum ImageType
    {
        Button=0,
        Titlebar=1,
        Checkbox_Checked=2,
        Checkbox_Unchecked=3,
        Close=4,
        Window=5,
        Debug=6,
        Slider=7,
        Slider_Item=8,
        Combobox_Unexpanded=9,
        Comboxbox_Expanded=10,
        Comboxbox_Item=11,
        Tooltip=12,
        SelectionButton_Selected=13,
        SelectionButton_Unselected=14,
        Spinner_Control=15,
        Spinner_Button_Enabled=16,
        Spinner_Button_Disabled=17,
        Minimize=18,
        Maximize=19
    }

    public enum TextStatus
    {
        None = 0,
        Selected = 1,
        Disabled = 2,
    }

    [Serializable]
    public struct TextStyle
    {
        public byte FontSize { get; set; }
        public string FontColour { get; set; }
        public TextStatus Status { get; set; }
        public string FontName { get; set; }
    }

    public class LinkEventArgs : EventArgs
    {
        public string Text { get; private set; }

        public LinkEventArgs(string text)
        {
            this.Text = text;
        }
    }
    
    [Serializable]
    public class Control : INotifyPropertyChanged
    {
        [Serializable]
        private class Clipping : IManagedComponent
        {
            private float left, right, top, bottom;

            public Clipping() : base("Clipping")
            {
                Left = -1;
                Right = 1;
                Top = -1;
                Bottom = 1;
            }

            public float Left
            {
                get
                {
                    return left;
                }
                set
                {
                    this.left = value;
                    OnPropertyChanged("Left");
                }
            }

            public float Right
            {
                get
                {
                    return right;
                }
                set
                {
                    this.right = value;
                    OnPropertyChanged("Right");
                }
            }

            public float Top
            {
                get
                {
                    return top;
                }
                set
                {
                    this.top = value;
                    OnPropertyChanged("Top");
                }
            }

            public float Bottom
            {
                get
                {
                    return bottom;
                }
                set
                {
                    this.bottom = value;
                    OnPropertyChanged("Bottom");
                }
            }
        }

        private IEntity handle;
        private List<Control> children = new List<Control>();
        public event EventHandler Click;
        public event EventHandler VisibleChanged;
        public event EventHandler Destroyed;
        public event EventHandler MouseEnter;
        public event EventHandler MouseExit;
        public event EventHandler<LinkEventArgs> LinkClick;
        public event EventHandler<KeyPressEventArgs> KeyPress;
        private ITransform transform;
        private List<Binding> dataBindings = new List<Binding>();
        private bool destroyed;
        private Clipping clipping;
        private Clipping localClipping;
        private Control parent;
        private TextStyle textStyle = new TextStyle();
        private bool doNotInheritStyle;
        private string tag;
        private string tooltipText;
        private string tooltipStyle;
        private Dictionary<TriggerType, IStoryboard> animations = new Dictionary<TriggerType, IStoryboard>();
        private bool visible;
        private bool flipHorizontal;
        private bool flipVertical;
        private float width;
        private float height;
        private IList<IEntity> patches;
        private bool isSliced;
        private long? image;
        private bool wrap;

        public event EventHandler<PropertyChangedEventArgs> PropertyChanged;

        public Control()
        {
            this.Padding = new Padding();
            this.Margin = new Padding();
            this.localClipping = new Clipping();
            CreateHandle();
            this.visible = true;
        }

        public string Tag
        {
            get
            {
                return this.tag;
            }
            set
            {
                if (this.tag == null)
                {
                    this.tag = value;
                }
                else
                {
                    throw new InvalidOperationException("Tag can only be set once.");
                }
            }
        }

        public string TooltipText
        {
            get
            {
                return this.tooltipText;
            }
            set
            {
                if (this.tooltipText != value)
                {
                    this.tooltipText = value;
                }
            }
        }

        public string TooltipStyle
        {
            get
            {
                return this.tooltipStyle;
            }
            set
            {
                if (this.tooltipStyle != value)
                {
                    this.tooltipStyle = value;
                }
            }
        }

        public IEntity Handle
        {
            get
            {
                return this.handle;
            }
        }

        public IList<Control> Children
        {
            get
            {
                return new List<Control>(this.children).AsReadOnly();
            }
        }

        protected void OnPropertyChanged(string property)
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(property));
            }
        }

        protected virtual void OnDestroyed()
        {
        }

        protected void SetClipRectangle(float x, float y, float width, float height)
        {
            this.EnsureClipping();

            this.localClipping.Top = (y - height) / UIContainer.Default.Height;
            this.localClipping.Bottom = (y + height) / UIContainer.Default.Height;
            this.localClipping.Right = (x + width) / UIContainer.Default.Width;
            this.localClipping.Left = (x - width) / UIContainer.Default.Width;

            this.UpdateClipRectangle();
        }

        private void UpdateClipRectangle()
        {
            if (!this.destroyed)
            {
                this.EnsureClipping();

                if (this.parent != null && this.parent.clipping != null)
                {
                    this.clipping.Top = Math.Max(this.parent.clipping.Top, this.localClipping.Top);
                    this.clipping.Bottom = Math.Min(this.parent.clipping.Bottom, this.localClipping.Bottom);
                    this.clipping.Right = Math.Min(this.parent.clipping.Right, this.localClipping.Right);
                    this.clipping.Left = Math.Max(this.parent.clipping.Left, this.localClipping.Left);
                }
                else
                {
                    this.clipping.Top = this.localClipping.Top;
                    this.clipping.Bottom = this.localClipping.Bottom;
                    this.clipping.Right = this.localClipping.Right;
                    this.clipping.Left = this.localClipping.Left;
                }

                foreach (var child in this.children)
                {
                    child.UpdateClipRectangle();
                }
            }
        }

        private void EnsureClipping()
        {
            if (this.clipping == null)
            {
                this.clipping = this.handle.GetComponent<Clipping>();
            }
        }

        public bool Wrap
        {
            get
            {
                if (destroyed)
                {
                    return false;
                }

                return wrap;
            }
            set
            {
                if (!destroyed)
                {
                    this.wrap = value;
                    if (handle.HasComponent<ITextSprite>())
                    {
                        this.handle.GetComponent<ITextSprite>().Wrap = value;
                    }
                    this.OnWrapChanged();
                }
            }
        }

        protected virtual void OnWrapChanged()
        {
        }

        public bool Visible
        {
            get
            {
                if (this.handle == null)
                {
                    return false;
                }

                if (parent == null)
                {
                    return this.visible;
                }

                return this.visible && parent.Visible;
            }
            set
            {
                if (this.handle != null)
                {
                    if (value)
                    {
                        this.Show();
                    }
                    else
                    {
                        this.Hide();
                    }
                }
            }
        }

        private void HideCore()
        {
            if (handle != null)
            {
                this.handle.Active = false;
                foreach (var child in this.children)
                {
                    child.HideCore();
                }

                if (this.patches != null)
                {
                    foreach (var patch in this.patches)
                    {
                        patch.Active = false;
                    }
                }

                this.OnVisibleChanged();
                this.OnHide();
            }
        }

        public void Hide()
        {
            this.visible = false;
            this.HideCore();
        }

        protected virtual void OnHide()
        {
        }

        private void OnVisibleChanged()
        {
            if (this.VisibleChanged != null)
            {
                this.VisibleChanged(this, EventArgs.Empty);
            }

            if (this.parent != null)
            {
                this.parent.OnChildVisibleChanged(this);
            }
        }

        protected virtual void OnChildVisibleChanged(Control child)
        {
        }

        public void Show()
        {
            this.visible = true;
            if (handle != null && this.Visible)
            {
                this.handle.Active = !this.isSliced;
                foreach (var child in this.children)
                {
                    if (child.visible)
                    {
                        child.Show();
                    }
                }

                if (this.patches != null)
                {
                    foreach (var patch in this.patches)
                    {
                        patch.Active = true;
                    }
                }

                this.CheckTriggers(TriggerType.OnLoad);
                this.OnVisibleChanged();
                this.OnShow();
            }
        }

        internal void AddAnimation(TriggerType type, IStoryboard storyboard)
        {
            this.animations.Add(type, storyboard);
        }

        internal void OnLoad()
        {
            if (this.Visible)
            {
                this.CheckTriggers(TriggerType.OnLoad);
                foreach (var child in this.Children)
                {
                    child.OnLoad();
                }
            }
        }

        private void CheckTriggers(TriggerType type)
        {
            IStoryboard storyboard = null;
            if (this.animations.TryGetValue(type, out storyboard))
            {
                UIContainer.Default.Use(storyboard);
            }
        }

        protected virtual void OnShow()
        {
        }

        private void CreateHandle()
        {
            // Initialize handle
            this.handle = UIContainer.Default.Allocate();
            this.transform = this.handle.AddComponent<ITransform>();

            this.OnHandleCreated();
        }

        protected virtual void OnHandleCreated()
        {
        }

        private void DestroyHandle()
        {
            if (this.handle != null)
            {
                if (this.Destroyed != null)
                {
                    this.Destroyed(this, EventArgs.Empty);
                }

                UIContainer.Default.Destroy(this.handle);

                if (this.patches != null)
                {
                    foreach (var patch in this.patches)
                    {
                        UIContainer.Default.Destroy(patch);
                    }
                }
            }
            this.handle = null;
            this.transform = null;
        }

        public void AddBinding(Binding binding)
        {
            this.dataBindings.Add(binding);
        }

        public void SetText(string text)
        {
            if (!destroyed)
            {
                if (!handle.HasComponent<ITextSprite>())
                {
                    UIContainer.Default.Resized += Default_Resized;
                }

                var comp = handle.GetComponent<ITextSprite>();
                comp.Text = text;
                UIContainer.Default.BindFont(handle, textStyle);

                this.EnsureClipping();
            }
        }

        protected virtual void ApplyTextStyle(TextStyle style)
        {            
        }

        public void SetTextStyle(TextStyle style)
        {
            this.doNotInheritStyle = true;
            SetTextStyleCore(style);
        }

        private void SetTextStyleCore(TextStyle style)
        {
            this.textStyle = style;
            if (!destroyed)
            {
                if (handle.HasComponent<ITextSprite>())
                {
                    UIContainer.Default.BindFont(handle, textStyle);
                }

                foreach (var child in this.children)
                {
                    if (!child.doNotInheritStyle)
                    {
                        child.SetTextStyleCore(this.textStyle);
                    }
                }

                this.EnsureClipping();
                this.ApplyTextStyle(style);
            }
        }

        public TextStyle TextStyle
        {
            get
            {
                return this.textStyle;
            }
        }

        private void Default_Resized(object sender, EventArgs e)
        {
            //UIContainer.Default.Resized -= Default_Resized;
            if (!destroyed)
            {
                if (handle.HasComponent<ITextSprite>())
                {
                    handle.GetComponent<ITextSprite>().MakeDirty();
                }
            }
        }

        private void PerformSlicing()
        {
            bool active = this.handle.Active;
            if (this.patches != null)
            {
                foreach (var patch in this.patches)
                {
                    active |= patch.Active;
                    UIContainer.Default.Destroy(patch);
                }
            }

            var sp = handle.GetComponent<ISprite>();
            if (sp.SubMaterialType == SubMaterialType._9Sliced)
            {
                this.patches = _9Sliced.Slice(this, UIContainer.Default);
                foreach (var slice in this.patches)
                {
                    slice.GetComponent<Clipping>();
                }

                if (this.layoutSuspended == 0)
                {
                    _9Sliced.Align(this.handle, this.patches);
                }

                this.isSliced = true;
                // In a 9 slice the children are visible and the parent is not.
                this.handle.Active = false;
            }
            else
            {
                this.patches = null;
                this.isSliced = false;
                // If were previous 9 sliced, we are visible if the children
                // were active. If were not previously 9 sliced then
                // we are active if we were before.
                this.handle.Active = active;
            }
        }

        public void SetImage(ImageType material)
        {
            if (!destroyed)
            {
                this.image = (long)material;
                handle.GetComponent<ISprite>();
                UIContainer.Default.BindTexture(handle, (long)material);
                this.PerformSlicing();
            }
        }

        public void SetCustomImage(long material)
        {
            if (!destroyed)
            {
                if (material >=0 && material < 1000)
                {
                    throw new InvalidOperationException("0-999 inclusive are reserved for internal usage.");
                }

                this.image = material;
                handle.GetComponent<ISprite>();
                UIContainer.Default.BindTexture(handle, material);
                this.PerformSlicing();
            }
        }

        public void Refresh()
        {
            if (!this.destroyed)
            {
                if (this.image.HasValue)
                {
                    handle.GetComponent<ISprite>();
                    UIContainer.Default.BindTexture(handle, this.image.Value);
                    this.PerformSlicing();
                }

                foreach (var child in this.children)
                {
                    child.Refresh();
                }
            }
        }

        protected virtual void OnChildResize(Control child)
        {
        }

        public float X
        {
            get
            {
                return transform.Position.X;
            }
            set
            {
                if (this.transform.Position.X != value)
                {
                    this.transform.Position.X = value;
                    this.InternalLayout();
                }
            }
        }

        public float Y
        {
            get
            {
                return transform.Position.Y;
            }
            set
            {
                if (this.transform.Position.Y != value)
                {
                    this.transform.Position.Y = value;
                    this.InternalLayout();
                }
            }
        }

        public float Z
        {
            get
            {
                return transform.Position.Z;
            }
            set
            {
                if (this.transform.Position.Y != value)
                {
                    this.transform.Position.Z = value;
                    this.InternalLayout();
                }
            }
        }

        public bool VerticalFlipped
        {
            get
            {
                return this.flipVertical;
            }
            set
            {
                if (this.flipVertical != value)
                {
                    this.flipVertical = value;
                    this.transform.Scale.Y = this.height * (this.flipVertical ? -1 : 1);
                }
            }
        }

        public bool HorizontalFlipped
        {
            get
            {
                return this.flipHorizontal;
            }
            set
            {
                if (this.flipHorizontal != value)
                {
                    this.flipHorizontal = value;
                    this.transform.Scale.X = this.width * (this.flipHorizontal ? -1 : 1);
                }
            }
        }
        
        public float Width
        {
            get
            {
                return this.width;
            }
            set
            {
                if (Width != value)
                {
                    this.width = value;
                    transform.Scale.X = value * (this.flipHorizontal ? -1 : 1);
                    if (this.parent != null)
                    {
                        this.parent.OnChildResize(this);
                    }
                    this.InternalLayout();
                }
            }
        }

        public float Height
        {
            get
            {
                return this.height;
            }
            set
            {
                if (Height != value)
                {
                    this.height = value;
                    transform.Scale.Y = value * (this.flipVertical ? -1 : 1);
                    if (this.parent != null && !this.parent.destroyed)
                    {
                        this.parent.OnChildResize(this);
                    }
                    this.InternalLayout();
                }
            }
        }

        public Anchor Anchor { get; set; }

        public Padding Margin { get; set; }

        public Padding Padding { get; set; }

        public bool Intersects(float x, float y)
        {
            return this.X - Width < x && this.X + Width > x &&
                this.Y - Height < y && this.Y + Height > y;
        }

        private IEnumerable<Control> SortChildrenByZ()
        {
            return this.children.OrderByDescending<Control, float>(c => c.Z);
        }

        public bool PerformKeyPress(KeyPressEventArgs e)
        {
            if (this.KeyPress != null)
            {
                this.KeyPress(this, e);
            }

            if (!e.Handled)
            {
                this.OnKeyPress(e);
            }
            return e.Handled;
        }

        protected virtual void OnKeyPress(KeyPressEventArgs e)
        {
        } 

        public bool PerformClick(float x, float y)
        {
            foreach (var child in this.SortChildrenByZ())
            {
                if (child.Intersects(x, y) && child.Visible)
                {
                    if (child.PerformClick(x, y))
                    {
                        return true;
                    }
                    else
                    {
                        // Propagate link click from child if no click occurred
                        HandleLinkClick(child, x, y);
                        break;
                    }
                }
            }

            HandleLinkClick(this, x, y);

            if (this.Click != null)
            {
                this.Click(this, EventArgs.Empty);
            }
            this.OnMouseClick();
            return !this.ClickThrough;
        }

        private void HandleLinkClick(Control control, float x, float y)
        {
            if (control.Handle.HasComponent<ITextSprite>())
            {
                var textSprite = control.Handle.GetComponent<ITextSprite>();
                foreach (var link in textSprite.GetLinkData())
                {
                    float textX = link.Rectangle.X*2 + control.X;
                    float textY = link.Rectangle.Y*2 + control.Y;
                    if (x >= textX - control.Width &&
                        x <= textX - control.Width + link.Rectangle.Width*2 &&
                        y <= textY + control.Height &&
                        y >= textY + control.Height - link.Rectangle.Height*2)
                    {
                        string linkText = textSprite.Text.Substring(link.StartIndex, link.Length);

                        if (this.LinkClick != null)
                        {
                            this.LinkClick(this, new LinkEventArgs(linkText));
                        }

                        this.OnLinkClick(linkText);
                        break;
                    }
                }
            }
        }

        public bool PerformMouseUp(float x, float y)
        {
            foreach (var child in this.SortChildrenByZ())
            {
                if (child.Intersects(x, y) && child.Visible)
                {
                    return child.PerformMouseUp(x, y);
                }
            }

            this.OnMouseUp();
            return !this.ClickThrough;
        }

        protected virtual bool ClickThrough
        {
            get
            {
                return false;
            }
        }

        protected virtual void OnMouseClick()
        {
        }

        protected virtual void OnMouseUp()
        {
        }

        protected virtual void OnLinkClick(string text)
        {

        }

        private bool hasMouseFocus;

        private void RemoveMouseFocus()
        {
            if (this.MouseExit != null)
            {
                this.MouseExit(this, EventArgs.Empty);
            }
            this.OnMouseExit();
            this.hasMouseFocus = false;
            if (!string.IsNullOrEmpty(this.TooltipText))
            {
                UIContainer.Default.HideTooltip(this);
            }

            foreach (var child in this.Children)
            {
                child.RemoveMouseFocus();
            }
        }

        public void PerformMouseMove(float x, float y)
        {
            if (!this.hasMouseFocus)
            {
                if (this.MouseEnter != null)
                {
                    this.MouseEnter(this, EventArgs.Empty);
                }
                this.OnMouseEnter();
            }

            this.hasMouseFocus = true;
            this.OnMouseMove(x, y);

            if (!string.IsNullOrEmpty(this.TooltipText))
            {
                UIContainer.Default.ShowTooltip(this, this.TooltipText);
            }

            bool givenMouseFocus = false;
            foreach (var child in this.SortChildrenByZ())
            {
                if (child.Visible && child.Intersects(x, y) && !givenMouseFocus)
                {
                    child.PerformMouseMove(x, y);
                    givenMouseFocus = true;
                }
                else if (child.hasMouseFocus)
                {
                    child.RemoveMouseFocus();
                }
            }
        }

        protected virtual void OnMouseEnter()
        {
        }

        protected virtual void OnMouseExit()
        {
        }

        protected virtual void OnMouseMove(float x, float y)
        {
        }

        public UIMeasureText MeasureText(string text)
        {
            UIMeasureText t = new UIMeasureText();
            t.Width = this.width;
            t.Height = this.Height;
            t.Style = this.textStyle;
            t.Text = text;
            t.Wrap = this.Wrap;

            UIContainer.Default.MeasureText(t);

            return t;
        }

        public void Add(Control control)
        {
            if (control.parent != null)
            {
                throw new InvalidOperationException("Control already has a parent.");
            }
            control.parent = this;
            if (!this.Visible)
            {
                control.HideCore();
            }
            this.children.Add(control);
            if (!control.doNotInheritStyle)
            {
                control.SetTextStyleCore(this.textStyle);
            }
            if (!control.connected && connected)
            {
                control.Connect(bindingContext);
            }
            this.OnAdd(control);
            this.InternalLayout();
            control.UpdateClipRectangle();
        }

        protected virtual void OnAdd(Control control)
        {
        }

        protected virtual void OnRemove(Control control)
        {
        }

        private bool layoutPending;
        private int layoutSuspended = 0;
        private void InternalLayout()
        {
            if (!this.destroyed)
            {
                if (layoutSuspended == 0)
                {
                    layoutPending = false;
                    this.OnLayout();

                    if (this.patches != null)
                    {
                        var type = this.handle.GetComponent<ISprite>().SubMaterialType;
                        if (type == SubMaterialType._9Sliced)
                        {
                            _9Sliced.Align(this.handle, this.patches);
                        }
                    }
                }
                else
                {
                    layoutPending = true;
                }
            }
        }

        public void SuspendLayout()
        {
            layoutSuspended++;
        }

        public void ResumeLayout()
        {
            layoutSuspended--;
            if (layoutPending)
            {
                InternalLayout();
            }
        }

        protected virtual void OnLayout()
        {
        }

        public void Drag(float mouseX, float mouseY)
        {
            this.OnDrag(mouseX, mouseY);
        }

        protected virtual void OnDrag(float mouseX, float mouseY)
        {            
        }

        private object bindingContext;
        private bool connected = false;
        public void Connect(object dataSource)
        {
            if (dataSource == bindingContext)
                return;

            if (this.connected)
            {
                foreach (var binding in this.dataBindings)
                {
                    binding.Unhook();
                }
            }
            
            this.connected = true;
            this.bindingContext = dataSource;
            foreach (var binding in this.dataBindings)
            {
                binding.Hook(dataSource);
            }

            foreach (var child in this.children)
            {
                // If the child is bound to something already, don't overwrite it
                if (!child.connected)
                {
                    child.Connect(dataSource);
                }
            }

            this.OnConnected(dataSource);
        }

        protected virtual void OnConnected(object dataSource)
        {
        }

        private void MarkDestroyed()
        {
            UIContainer.Default.Resized -= Default_Resized;
            this.destroyed = true;
            foreach (var child in this.Children)
            {
                child.MarkDestroyed();
            }

            this.OnDestroyed();
        }

        private void RemoveAll()
        {
            this.MarkDestroyed();

            foreach (var child in this.Children)
            {
                this.children.Remove(child);
                //this.OnRemove(child);
                child.RemoveAll();
            }
            this.DestroyHandle();
        }

        public void Remove(Control control)
        {
            control.parent = null;
            control.RemoveAll();

            this.children.Remove(control);
            this.OnRemove(control);
            this.InternalLayout();
            control.UpdateClipRectangle();
        }

        [Flags]
        public enum ControlPropagationFlags
        {
            Children=1,
            Patches=2,
            All=0xffff,
        }

        public void PropagateComponent<T>(ControlPropagationFlags flags) where T : IManagedComponent, new()
        {
            if (this.destroyed)
            {
                return;
            }

            var component = this.Handle.GetComponent<T>();
            List<string> keys = new List<string>();
            List<object> values = new List<object>();
            foreach (var p in typeof(T).GetProperties(System.Reflection.BindingFlags.Public
                |System.Reflection.BindingFlags.Instance))
            {
                var g = p.GetGetMethod();
                if (g != null)
                {
                    values.Add(g.Invoke(component, new object[0]));
                    keys.Add(p.Name);
                }
            }

            PropagateComponentCore<T>(flags, keys, values);
        }

        private void UpdateComponent<T>(object c, List<string> keys, List<object> values)
        {
            Type t = typeof(T);
            for (int i = 0; i < keys.Count; i++)
            {
                var p = t.GetProperty(keys[i]);
                var s = p.GetSetMethod();
                if (s != null)
                {
                    s.Invoke(c, new object[] { values[i] });
                }
            }
        }

        private void PropagateComponentCore<T>(ControlPropagationFlags flags, List<string> keys, List<object> values) where T : IManagedComponent, new()
        {
            if ((flags & ControlPropagationFlags.Children) == ControlPropagationFlags.Children)
            {
                foreach (var child in this.children)
                {
                    if (child.destroyed)
                        continue;

                    var handle = child.Handle;
                    T c = handle.GetComponent<T>();
                    UpdateComponent<T>(c, keys, values);

                    child.PropagateComponentCore<T>(flags, keys, values);
                }
            }

            if ((flags & ControlPropagationFlags.Patches) == ControlPropagationFlags.Patches)
            {
                if (patches != null)
                {
                    foreach (var patch in this.patches)
                    {
                        T c = patch.GetComponent<T>();
                        UpdateComponent<T>(c, keys, values);
                    }
                }
            }
        }
    }

    [Serializable]
    public class Padding
    {
        public float Left { get; set; }
        public float Right { get; set; }

        public float Up { get; set; }
        public float Down { get; set; }
    }

    /// <summary>
    /// Defines where in the parent control, this control
    /// is anchored to.
    /// </summary>
    public enum Anchor
    {
        None,
        Top,
        Left,
        Right,
        Bottom,
        TopRight,
        TopLeft,
        BottomRight,
        BottomLeft,
    }

    [Serializable]
    public class ControlTemplate
    {
        [Serializable]
        private class Binding
        {
            public string Property { get; set; }
            public string DataMember { get; set; }
        }

        private List<Binding> bindings = new List<Binding>();

        public void AddBinding(string dataMember, string property)
        {
            bindings.Add(new Binding()
            {
                Property = property,
                DataMember = dataMember,
            });
        }

        public void Bind(Control control)
        {
            foreach (var binding in this.bindings)
            {
                control.AddBinding(new UI.Binding(control, binding.DataMember, binding.Property));
            }
        }
    }
}
