﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Islander.UI
{
    public class ComboboxItemTemplate : ControlTemplate
    {
        public string Caption { get; set; }
        public string TooltipText { get; set; }
        public string TooltipStyle { get; set; }
    }

    [Serializable]
    public class Combobox : Control
    {
        [Serializable]
        private class ListControl : Control, IFocus
        {
            public event EventHandler FocusMiss;

            public ListControl()
            {
                this.Z = 10;
                //this.SetImage(ImageType.Debug);
            }

            protected override void OnLayout()
            {
                base.OnLayout();

                float y = this.Height;
                for (int i = 0; i < this.Children.Count; i++)
                {
                    var item = this.Children[i];
                    y -= item.Height;
                    item.Width = this.Width;
                    item.Height = 35;
                    item.X = this.X;
                    item.Y = this.Y + y;
                    item.Z = this.Z + 1;
                    y -= item.Height;
                }

                this.SetClipRectangle(this.X, this.Y, this.Width, this.Height);
            }

            protected override void OnShow()
            {
                base.OnShow();

                float height = 0;
                for (int i = 0; i < this.Children.Count; i++)
                {
                    var item = this.Children[i];
                    height += item.Height;
                }
                this.Height = height;
            }

            public void FocusClickMiss()
            {
                if (this.FocusMiss != null)
                {
                    this.FocusMiss(this, EventArgs.Empty);
                }
            }
        }

        private class ComboboxItem : ButtonBase
        {
            public ComboboxItem()
            {
                this.SetImage(ImageType.Comboxbox_Item);
            }
        }

        private Dictionary<Control, object> dataMapping = new Dictionary<Control, object>();
        private ListControl list = new ListControl();
        private ComboboxItem header = new ComboboxItem();
        private bool expanded;
        private object data;
        private object selectedItem;
        private ComboboxItemTemplate template;
        private bool destroyed;

        public Combobox()
        {
            //this.SetImage(ImageType.Combobox_Unexpanded);
            this.header.Click += header_Click;
            this.header.Height = 35;
            this.Height = 35;
            this.Add(this.header);

            this.list.Hide();
            this.list.FocusMiss += list_FocusMiss;
            UIContainer.Default.ActivateInto(this.list);
        }

        void list_FocusMiss(object sender, EventArgs e)
        {
            this.ToggleExpanded();
        }

        public ComboboxItemTemplate Template
        {
            get
            {
                return this.template;
            }
            set
            {
                if (this.template != value)
                {
                    this.template = value;
                }
            }
        }

        public string Caption
        {
            get
            {
                return this.header.Caption;
            }
            set
            {
                this.header.Caption = value;
            }
        }

        public object DataSource
        {
            get
            {
                return this.data;
            }
            set
            {
                if (this.data != value)
                {
                    this.data = value;
                    this.SuspendLayout();

                    var list = this.data as IList;
                    if (list != null)
                    {
                        foreach (var element in list)
                        {
                            ComboboxItem item = new ComboboxItem();
                            this.list.Add(item);

                            item.Click += item_Click;

                            if (this.template == null)
                            {
                                item.AddBinding(new Binding(item, null, "Caption"));
                            }
                            else
                            {
                                this.template.Bind(item);

                                if (!string.IsNullOrEmpty(this.template.Caption))
                                {
                                    item.Caption = this.template.Caption;
                                }

                                if (!string.IsNullOrEmpty(this.template.TooltipText))
                                {
                                    item.TooltipText = this.template.TooltipText;
                                }

                                if (!string.IsNullOrEmpty(this.template.TooltipStyle))
                                {
                                    item.TooltipStyle = this.template.TooltipStyle;
                                }
                            }

                            item.Connect(element);
                            this.dataMapping.Add(item, element);
                        }
                    }

                    this.ResumeLayout();
                }
            }
        }

        void item_Click(object sender, EventArgs e)
        {
            var control = sender as ComboboxItem;
            this.SelectedItem = this.dataMapping[control];
            this.ToggleExpanded();
        }

        public object SelectedItem
        {
            get
            {
                return this.selectedItem;
            }
            set
            {
                this.selectedItem = value;
                this.OnPropertyChanged("SelectedItem");
            }
        }

        protected override void OnLayout()
        {
            base.OnLayout();

            Control[] controls = new Control[2] { this.header, this.list };

            float y = this.Height;
            for (int i = 0; i < controls.Length; i++)
            {
                var item = controls[i];
                y -= item.Height;
                item.Width = this.Width;
                item.X = this.X;
                item.Y = this.Y + y;
                item.Z = this.Z + 1;
                y -= item.Height;
            }

            this.SetClipRectangle(this.X, this.Y, this.Width, this.Height);
        }

        void header_Click(object sender, EventArgs e)
        {
            base.OnMouseClick();

            this.ToggleExpanded();
        }

        private void ToggleExpanded()
        {
            this.expanded = !this.expanded;
            if (this.expanded)
            {
                this.SetImage(ImageType.Comboxbox_Expanded);
                this.list.Show();
                UIContainer.Default.Focus(this.list);
            }
            else
            {
                this.SetImage(ImageType.Combobox_Unexpanded);
                UIContainer.Default.LoseFocus();
                this.list.Hide();
            }

            if (!this.destroyed)
            {
                this.OnLayout();
            }
        }

        protected override void OnDestroyed()
        {
            base.OnDestroyed();

            this.destroyed = true;
            UIContainer.Default.Close(this.list);
        }
    }
}
