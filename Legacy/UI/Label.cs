﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Islander.UI
{
    [Serializable]
    public class Label : Control
    {
        private string caption;
        private Layout layout;
        private bool hooked;
        private bool centered;

        [Serializable]
        private struct Layout
        {
            public float X { get; set; }
            public float Width { get; set; }
            public float Left { get; set; }
            public float Y { get; set; }
            public float Height { get; set; }
            public float Up { get; set; }
        }

        public string Caption
        {
            get
            {
                return this.caption;
            }
            set
            {
                this.caption = value;
                this.SetText(this.caption);
            }
        }

        public Label()
        {
            this.Caption = string.Empty;
            this.layout = new Layout();

            this.Hook();
        }

        protected override void OnDestroyed()
        {
            base.OnDestroyed();

            this.UnHook();
        }

        void Default_Resized(object sender, EventArgs e)
        {
            this.SetLabelX(layout.X, this.layout.Width, this.layout.Left);
            this.SetLabelY(layout.Y, this.layout.Height, this.layout.Up);
        }
        
        protected override void OnHide()
        {
            base.OnHide();

            this.UnHook();
        }

        private void Hook()
        {
            if (!this.hooked)
            {
                this.hooked = true;
                UIContainer.Default.Resized += Default_Resized;
            }
        }

        protected override void OnShow()
        {
            base.OnShow();

            this.Hook();
        }

        private void UnHook()
        {
            if (this.hooked)
            {
                this.hooked = false;
                UIContainer.Default.Resized -= Default_Resized;
            }
        }
        
        public void SetLabelX(float x, float width, float paddingLeft)
        {
            this.layout.X = x;
            this.layout.Width = width;
            this.layout.Left = paddingLeft;
            this.X = x + paddingLeft;
        }

        public void SetLabelY(float y, float height, float paddingUp)
        {
            this.layout.Y = y;
            this.layout.Height = height;
            this.layout.Up = paddingUp;
            this.Y = y + paddingUp;
        }

        public bool Centered
        {
            get
            {
                return this.centered;
            }
            set
            {
                this.centered = value;
                this.Handle.GetComponent<Islander.Managed.ITextSprite>().Centered = value;
            }
        }

        public bool CanClickThrough
        {
            get;
            set;
        }

        protected override bool ClickThrough
        {
            get
            {
                return this.CanClickThrough;
            }
        }
    }

    [Serializable]
    public class LabelControl : Control
    {
        private Label label;

        public LabelControl()
        {
            this.label = new Label();
            this.Add(label);
        }

        protected override void OnLayout()
        {
            base.OnLayout();

            this.label.SetLabelX(this.X, this.Width, this.Padding.Left);
            this.label.SetLabelY(this.Y, this.Height, this.Padding.Up);
            this.label.Z = this.Z;
            this.label.Width = this.Width;
            this.label.Height = this.Height;
            this.label.Wrap = this.Wrap;
            //this.label.Handle.GetComponent<Islander.Managed.ITextSprite>().MakeDirty();

            this.SetClipRectangle(this.X, this.Y, this.Width, this.Height);
        }

        public string Caption
        {
            get
            {
                return this.label.Caption;
            }
            set
            {
                this.label.Caption = value;
            }
        }

        public bool Centered
        {
            get
            {
                return label.Centered;
            }
            set
            {
                this.label.Centered = value;
                this.OnLayout();
            }
        }
    }
}
