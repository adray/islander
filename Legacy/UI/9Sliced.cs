﻿using Islander.Managed;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Islander.UI
{
    internal static class _9Sliced
    {
        [Flags]
        private enum StretchFlags
        {
            None,
            Vertical,
            Horizontal,
        }

        [Serializable]
        private class SliceComponent : IManagedComponent
        {
            public SliceComponent()
                : base("Slice")
            { }

            public int SlicePosition { get; set; }
            public StretchFlags Flags { get; set; }
            public float PositionX { get; set; }
            public float PositionY { get; set; }
            public float PaddingX { get; set; }
            public float PaddingY { get; set; }
        }

        public static IList<IEntity> Slice(Control control, IContainer world)
        {
            var psp = control.Handle.GetComponent<ISprite>();
            var pmat = psp.CloneMaterial();
            
            StretchFlags[] flags = new StretchFlags[]
            {
                StretchFlags.None,
                StretchFlags.Horizontal,
                StretchFlags.None,
                StretchFlags.Vertical,
                StretchFlags.Vertical | StretchFlags.Horizontal,
                StretchFlags.Vertical,
                StretchFlags.None,
                StretchFlags.Horizontal,
                StretchFlags.None,
            };

            float[] px = new float[]
            {
                -1, 0, 1,
                -1, 0, 1,
                -1, 0, 1,
            };

            float[] py = new float[]
            {
                -1, -1, -1,
                0, 0, 0,
                1, 1, 1,
            };

            float[] fixedWidth = new float[9];
            float[] fixedHeight = new float[9];
            
            const float spriteSheetSize = 1024; // hack, spritesheet is always 1024x1024

            for (int i = 0; i < 9; i++)
            {
                var sub = pmat.GetSubMaterial(i);
                var f = flags[i];
                fixedWidth[i] = (f & StretchFlags.Horizontal) == StretchFlags.None ? sub.GetTexture(0).Sx * spriteSheetSize : 0;
                fixedHeight[i] = (f & StretchFlags.Vertical) == StretchFlags.None ? sub.GetTexture(0).Sy * spriteSheetSize : 0;
            }

            float[] padX = new float[]
            {
                0, fixedWidth[0] + fixedWidth[2], 0,
                0, fixedWidth[3] + fixedWidth[5], 0,
                0, fixedWidth[6] + fixedWidth[8], 0,
            };
            float[] padY = new float[]
            {
                0, 0, 0,
                fixedHeight[0] + fixedHeight[6], fixedHeight[1] + fixedHeight[7], fixedHeight[2] + fixedHeight[8],
                0, 0, 0,
            };

            List<IEntity> sliced = new List<IEntity>();
            for (int i = 0; i < 9; i++)
            {
                IEntity e = world.Allocate();
                e.Active = control.Visible;

                var sub = pmat.GetSubMaterial(i);

                Material m = new Material(pmat.PixelShader, pmat.VertexShader, pmat.Pass,
                    new Texture[] {
                        sub.GetTexture(0),
                        sub.GetTexture(1),
                        sub.GetTexture(2),
                        sub.GetTexture(3)
                    });

                var sp = e.GetComponent<ISprite>();
                sp.SetMaterial(m);

                var t = e.GetComponent<ITransform>();

                var sli = e.GetComponent<SliceComponent>();
                sli.SlicePosition = i;
                sli.Flags = flags[i];
                sli.PositionX = px[i];
                sli.PositionY = py[i];
                sli.PaddingX = padX[i];
                sli.PaddingY = padY[i];

                sliced.Add(e);
            }

            return sliced;
        }

        public static void Align(IEntity parent, IList<IEntity> slices)
        {
            var psp = parent.GetComponent<ISprite>();
            var pmat = psp.CloneMaterial();

            var pt = parent.GetComponent<ITransform>();
            float totalWidth = pt.Scale.X;
            float totalHeight = pt.Scale.Y;
            float pX = pt.Position.X;
            float pY = pt.Position.Y;

            const float spriteSheetSize = 1024; // hack, spritesheet is always 1024x1024

            foreach (var slice in slices)
            {
                var sp = slice.GetComponent<ISprite>();
                var t = slice.GetComponent<ITransform>();
                var sli = slice.GetComponent<SliceComponent>();
                
                var m = pmat.GetSubMaterial(sli.SlicePosition);

                var scale = new Vec3f();
                if ((sli.Flags & StretchFlags.Horizontal) == StretchFlags.Horizontal)
                {
                    scale.X = totalWidth - sli.PaddingX;
                }
                else
                {
                    scale.X = m.GetTexture(0).Sx * spriteSheetSize;
                }

                if ((sli.Flags & StretchFlags.Vertical) == StretchFlags.Vertical)
                {
                    scale.Y = totalHeight - sli.PaddingY;
                }
                else
                {
                    scale.Y = m.GetTexture(0).Sy * spriteSheetSize;
                }

                t.Scale = scale;

                t.Direction = new Vec3<float>(0, 1, 0);

                t.Position = new Vec3<float>(
                    pX + (totalWidth - t.Scale.X)*sli.PositionX,
                    pY + (-totalHeight + t.Scale.Y)*sli.PositionY,
                    pt.Position.Z);
            }
        }
    }
}
