﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Islander.UI
{
    public interface ITableLayoutPanelSize
    {
        Measurement Value { get; }
    }

    [Serializable]
    public class TableLayoutPanelRow : ITableLayoutPanelSize
    {
        private Measurement length;

        public Measurement Value
        {
            get
            {
                return this.length;
            }
            set
            {
                this.length = value;
            }
        }

        public Measurement Height
        {
            get
            {
                return this.length;
            }
            set
            {
                this.length = value;
            }
        }
    }
    
    [Serializable]
    public class TableLayoutPanelColumn : ITableLayoutPanelSize
    {
        private Measurement length;

        public Measurement Value
        {
            get
            {
                return this.length;
            }
            set
            {
                this.length = value;
            }
        }

        public Measurement Width
        {
            get
            {
                return this.length;
            }
            set
            {
                this.length = value;
            }
        }
    }
    
    [Serializable]
    public class TableLayoutPanelCell : Control
    {
        public int Row { get; set; }
        public int Column { get; set; }

        protected override void OnLayout()
        {
            foreach (var child in this.Children)
            {
                child.X = this.X;
                child.Y = this.Y;
                child.Width = this.Width;
                child.Height = this.Height;
                child.Z = this.Z+1;
            }

            this.SetClipRectangle(this.X, this.Y, this.Width, this.Height);
        }
    }

    /// <summary>
    ///
    /// <TableLayoutPanel Width="800" Height="600">
    ///     <Rows>
    ///         <Row Width="10%" />
    ///         <Row Width="90%" />
    ///     </Rows>
    ///     <Columns>
    ///         <Column Height="25%" />
    ///         <Column Height="75%" />
    ///     </Columns>
    ///       
    ///     <Cell Row="0" Column="0">
    ///        <Button Caption="News" />
    ///     </Cell>
    /// </TableLayoutPanel>
    /// 
    /// </summary>
    [Serializable]
    public class TableLayoutPanel : Control
    {
        private List<ITableLayoutPanelSize> rows = new List<ITableLayoutPanelSize>();
        private List<ITableLayoutPanelSize> cols = new List<ITableLayoutPanelSize>();

        public List<ITableLayoutPanelSize> Rows
        {
            get
            {
                return this.rows;
            }
            set
            {
                this.rows = value;
            }
        }

        public List<ITableLayoutPanelSize> Columns
        {
            get
            {
                return this.cols;
            }
            set
            {
                this.cols = value;
            }
        }

        protected override void OnLayout()
        {
            foreach (var child in this.Children)
            {
                child.SuspendLayout();

                var cell = child as TableLayoutPanelCell;
                if (cell != null)
                {
                    if (this.cols[cell.Column].Value.Type == MeasurementType.Percentage)
                    {
                        cell.Width = this.cols[cell.Column].Value * Width;
                    }
                    else
                    {
                        cell.Width = this.cols[cell.Column].Value;
                    }

                    cell.X = this.X - this.Width + cell.Width;
                    cell.Width -= 10;

                    for (int i = 0; i < cell.Column; i++)
                    {
                        if (this.cols[i].Value.Type == MeasurementType.Percentage)
                        {
                            cell.X += this.cols[i].Value * Width * 2;
                        }
                        else
                        {
                            cell.X += this.cols[i].Value*2;
                        }
                    }

                    if (this.rows[cell.Row].Value.Type == MeasurementType.Percentage)
                    {
                        cell.Height = this.rows[cell.Row].Value * Height;
                    }
                    else
                    {
                        cell.Height = this.rows[cell.Row].Value;
                    }

                    cell.Y = this.Y + this.Height - cell.Height;
                    cell.Height -= 10;

                    for (int i = 0; i < cell.Row; i++)
                    {
                        if (this.rows[i].Value.Type == MeasurementType.Percentage)
                        {
                            cell.Y -= this.rows[i].Value * Height * 2;
                        }
                        else
                        {
                            cell.Y -= this.rows[i].Value*2;
                        }
                    }

                    cell.Z = this.Z+1;
                }

                child.ResumeLayout();
            }

            this.SetClipRectangle(this.X, this.Y, this.Width, this.Height);
        }
    }
}
