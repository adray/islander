﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Islander.UI
{
    [Serializable]
    public class ButtonBase : Control
    {
        private Label label;
        private string caption;
        private float labelOffsetX;
        private float labelOffsetY;

        public ButtonBase()
        {
            this.SuspendLayout();
            this.label = new Label();
            this.label.Wrap = true;
            this.label.CanClickThrough = true;
            this.Add(this.label);
            this.ResumeLayout();
        }

        public string Caption
        {
            get
            {
                return this.caption;
            }
            set
            {
                this.caption = value;
                this.label.SetText(this.caption);
            }
        }

        public float LabelOffsetX
        {
            get
            {
                return this.labelOffsetX;
            }
            set
            {
                this.labelOffsetX = value;
            }
        }

        public float LabelOffsetY
        {
            get
            {
                return this.labelOffsetY;
            }
            set
            {
                this.labelOffsetY = value;
            }
        }

        protected override void ApplyTextStyle(TextStyle style)
        {
            this.label.SetTextStyle(style);
        }

        protected override void OnLayout()
        {
            base.OnLayout();

            this.label.SetLabelX(this.X + this.LabelOffsetX, this.Width, this.Padding.Left);
            this.label.SetLabelY(this.Y + this.LabelOffsetY, this.Height, this.Padding.Up);
            this.label.Z = this.Z;
            this.label.Width = this.Width;
            this.label.Height = this.Height;
        }

        protected override void OnWrapChanged()
        {
            this.label.Wrap = this.Wrap;
        }
    }

    [Serializable]
    public class Button : ButtonBase
    {
        public string Command { get; set; }
        private object dataSource;

        public Button()
        {
            this.SetImage(ImageType.Button);
        }

        protected override void OnMouseClick()
        {
            base.OnMouseClick();

            if (!string.IsNullOrEmpty(Command))
            {
                UIContainer.Default.Invoke(Command, this.dataSource);
            }
        }

        protected override void OnConnected(object dataSource)
        {
            base.OnConnected(dataSource);

            this.dataSource = dataSource;
        }

        protected override void OnLayout()
        {
            base.OnLayout();

            this.SetClipRectangle(this.X, this.Y, this.Width, this.Height);
        }
    }
}
