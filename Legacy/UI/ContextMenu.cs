﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Islander.UI
{
    public interface IContextMenuHost
    {
        ContextMenu Menu { get; set; }
    }

    public class ContextMenuEventArgs : EventArgs
    {
        public object Item { get; private set; }

        public ContextMenuEventArgs(object item)
        {
            this.Item = item;
        }
    }

    [Serializable]
    public class ContextMenu : ControlTemplate
    {
        private ContextMenuControl control;

        public event EventHandler<ContextMenuEventArgs> Click
        {
            add
            {
                control.ContextClick += value;
            }
            remove
            {
                control.ContextClick -= value;
            }
        }

        public ContextMenu()
        {
            this.control = new ContextMenuControl();
        }

        public bool Visible
        {
            get
            {
                return this.control.Visible;
            }
            set
            {
                this.control.Visible = value;
            }
        }

        public void Show()
        {
            this.control.Show();
        }

        public void Hide()
        {
            this.control.Hide();
        }            

        public float X
        {
            get
            {
                return this.control.X;
            }
            set
            {
                this.control.X = value;
            }
        }

        public float Y
        {
            get
            {
                return this.control.Y;
            }
            set
            {
                this.control.Y = value;
            }
        }

        public float Z
        {
            get
            {
                return this.control.Z;
            }
            set
            {
                this.control.Z = value;
            }
        }

        public float Height
        {
            get
            {
                return this.control.Height;
            }
        }

        [Serializable]
        private class ContextMenuControl : Control, IFocus
        {
            private bool expanded;
            private bool activated;
            private object dataSource;
            private Dictionary<object, object> map;

            public event EventHandler<ContextMenuEventArgs> ContextClick;

            public ContextMenuControl()
            {
                this.SetImage(ImageType.Button);
                this.Width = 200;
                this.Height = 30;
                this.Visible = false;
                this.map = new Dictionary<object, object>();
            }

            public object DataSource
            {
                get
                {
                    return this.dataSource;
                }
                set
                {
                    if (this.dataSource != value)
                    {
                        this.dataSource = value;

                        if (this.dataSource != null)
                        {
                            IList list = this.dataSource as IList;
                            if (list != null)
                            {
                                this.SuspendLayout();
                                foreach (var item in list)
                                {
                                    ButtonBase button = new ButtonBase()
                                    {
                                        Width = this.Width,
                                        Height = 30,
                                        Caption = item.ToString(),
                                    };
                                    button.SetImage(ImageType.Button);
                                    button.Click += button_Click;
                                    this.Add(button);
                                    this.Connect(item);
                                    this.map.Add(button, item);
                                }
                                this.Height = 30 * list.Count;
                                this.ResumeLayout();
                            }
                        }
                    }
                }
            }

            void button_Click(object sender, EventArgs e)
            {
                if (this.ContextClick != null)
                {
                    this.ContextClick(this, new ContextMenuEventArgs(this.map[sender]));
                }

                this.Hide();
            }

            protected override void OnLayout()
            {
                base.OnLayout();

                if (!this.activated)
                {
                    this.activated = true;
                    UIContainer.Default.ActivateInto(this);
                }

                float y = this.Height + this.Y;
                foreach (var item in this.Children)
                {
                    y -= item.Height;

                    item.SuspendLayout();
                    item.X = this.X;
                    item.Y = y;
                    item.Z = this.Z + 1;
                    item.ResumeLayout();

                    y -= item.Height;
                }

                this.SetClipRectangle(this.X, this.Y, this.Width, this.Height);
            }

            protected override void OnShow()
            {
                base.OnShow();
                if (this.Visible && !this.expanded)
                {
                    this.expanded = true;
                    UIContainer.Default.Focus(this);
                }
            }

            protected override void OnHide()
            {
                base.OnHide();
                if (!this.Visible && this.expanded)
                {
                    this.expanded = false;
                    UIContainer.Default.LoseFocus();
                }
            }

            public void FocusClickMiss()
            {
                if (this.expanded)
                {
                    this.Visible = false;
                }
            }
        }

        public void Connect(object dataSource)
        {
            this.Bind(this.control);
            this.control.Connect(dataSource);
        }
    }
}
