﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Islander.UI
{
    [Serializable]
    public class Frame : Control
    {
        protected override void OnChildResize(Control child)
        {
            base.OnChildResize(child);

            this.OnLayout();
        }

        protected override void OnLayout()
        {
            base.OnLayout();

            foreach (var child in this.Children)
            {
                child.SuspendLayout();
                child.X = this.X;
                child.Y = this.Y;
                child.Z = this.Z + 1;

                if (child.Anchor == UI.Anchor.Left || child.Anchor == UI.Anchor.BottomLeft || child.Anchor == UI.Anchor.TopLeft)
                {
                    child.X = this.X -this.Width + child.Width + child.Padding.Left;
                }
                if (child.Anchor == UI.Anchor.Right || child.Anchor == UI.Anchor.BottomRight || child.Anchor == UI.Anchor.TopRight)
                {
                    child.X = this.X + this.Width - child.Width - child.Padding.Right;
                }
                if (child.Anchor == UI.Anchor.Top || child.Anchor == UI.Anchor.TopLeft || child.Anchor == UI.Anchor.TopRight)
                {
                    child.Y = this.Y + this.Height - child.Height - child.Padding.Up;
                }
                if (child.Anchor == UI.Anchor.Bottom || child.Anchor == UI.Anchor.BottomLeft || child.Anchor == UI.Anchor.BottomRight)
                {
                    child.Y = this.Y -this.Height + child.Height + child.Padding.Down;
                }
                child.ResumeLayout();
            }
        }
    }
}
