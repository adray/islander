﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Islander.UI
{
    [Serializable]
    public class BoundedContainer : Control
    {
        private VerticalScrollbar scrollbar;

        public BoundedContainer()
        {
            this.SuspendLayout();
            this.scrollbar = new VerticalScrollbar()
            {
                Width = 20,
                Height = this.Height
            };
            this.scrollbar.PropertyChanged += scrollbar_PropertyChanged;
            this.Add(this.scrollbar);
            this.ResumeLayout();
        }

        public void ScrollToMin()
        {
            this.scrollbar.Value = this.scrollbar.Min;
        }

        public void ScrollToMax()
        {
            this.scrollbar.Value = this.scrollbar.Max;
        }

        private void scrollbar_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.Property == "Value")
            {
                this.OnLayout();
            }
        }

        protected override void OnChildResize(Control child)
        {
            base.OnChildResize(child);
            this.OnLayout();
        }

        protected override void OnLayout()
        {
            base.OnLayout();

            float sum = 0;
            foreach (var child in this.Children)
            {
                if (child != this.scrollbar)
                {
                    sum += child.Height;
                    child.Y = this.Y + this.Height - sum + this.scrollbar.Value;
                    child.X = this.X;
                    child.Z = this.Z + 1;
                }
            }

            if (this.scrollbar != null)
            {
                this.scrollbar.Max = Math.Max(0, sum*2 - this.Height * 2);
                this.scrollbar.Visible = this.scrollbar.Max > 0;
            }

            this.LayoutScrollbar(this.Width);

            this.SetClipRectangle(this.X, this.Y, this.Width, this.Height);
        }

        private void LayoutScrollbar(float width)
        {
            if (this.scrollbar != null)
            {
                this.scrollbar.SuspendLayout();
                this.scrollbar.X = this.X + width - this.scrollbar.Width;
                this.scrollbar.Y = this.Y;
                this.scrollbar.Z = this.Z + 1;
                this.scrollbar.Height = this.Height;
                this.scrollbar.ResumeLayout();
            }
        }
    }
}
