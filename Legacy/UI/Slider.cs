﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Islander.UI
{
    [Serializable]
    public class Slider : Control
    {
        [Serializable]
        private class SlidingControl : Control
        {
            public event EventHandler<DragEventArgs> Slide;

            public SlidingControl()
            {
                this.SetImage(ImageType.Slider_Item);
            }

            protected override void OnMouseClick()
            {
                base.OnMouseClick();
                UIContainer.Default.StartDragging(this);
            }

            protected override void OnMouseUp()
            {
                base.OnMouseUp();
                UIContainer.Default.EndDragging(this);
            }

            protected override void OnDrag(float mouseX, float mouseY)
            {
                base.OnDrag(mouseX, mouseY);
                if (this.Slide != null)
                {
                    this.Slide(this, new DragEventArgs(mouseX, mouseY));
                }
            }
        }

        private SlidingControl slider;
        private LabelControl label;
        private string caption;
        private int sliderValue;

        public Slider()
        {
            this.slider = new SlidingControl();
            this.label = new LabelControl();

            this.slider.Slide += slider_Slide;
            this.Add(this.slider);
            this.Add(this.label);

            this.SetImage(ImageType.Slider);
        }

        void slider_Slide(object sender, DragEventArgs e)
        {
            RestrictSlider(e.X);
            this.UpdateText();
        }

        private void RestrictSlider(float x)
        {
            this.slider.X = Math.Min(Math.Max(x, this.X - this.Width + slider.Width), this.X + this.Width - slider.Width);
            
            float max = this.X + this.Width - slider.Width;
            float min = this.X - this.Width + slider.Width;
            float range = Max - Min;
            float r = max - min;
            this.SliderValue = (int)(Min + (range * (this.slider.X - min) / r));
        }

        public int SliderValue
        {
            get
            {
                return this.sliderValue;
            }
            set
            {
                this.sliderValue = value;
                this.OnPropertyChanged("SliderValue");

                ResetSlider();
                UpdateText();
            }
        }

        private void ResetSlider()
        {
            float max = this.X + this.Width - slider.Width;
            float min = this.X - this.Width + slider.Width;
            float range = Max - Min;
            float r = max - min;
            this.slider.X = ((this.sliderValue - Min) * r / range) + min;
        }

        private void UpdateText()
        {
            if (string.IsNullOrEmpty(caption))
            {
                this.label.SetText(this.sliderValue.ToString());
            }
            else
            {
                this.label.SetText(caption + ":" + this.sliderValue.ToString());
            }
        }

        protected override void OnLayout()
        {
            base.OnLayout();

            this.slider.Height = this.Height;
            this.slider.Width = 30;
            this.slider.Y = this.Y;
            this.slider.Z = this.Z + 1;
            //this.RestrictSlider(this.slider.X);
            this.ResetSlider();
            this.UpdateText();

            this.label.X = this.X;
            this.label.Y = this.Y;
            this.label.Z = this.slider.Z;

            this.label.Width = this.Width;
            this.label.Height = this.Height;
        }

        public float Max { get; set; }
        public float Min { get; set; }

        public string Caption
        {
            get
            {
                return this.Caption;
            }
            set
            {
                this.caption = value;
                UpdateText();
            }
        }
    }
}
