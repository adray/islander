﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Islander.UI
{
    public enum SpinnerFlip
    {
        FlipNone,
        FlipLeft,
        FlipRight,
    }

    public interface ISpinnerButton
    {
        bool Enabled { get; }
        bool IsRight { get; }
        bool IsLeft { get; }
        Control Control { get; }
    }

    [Serializable]
    public class Spinner : Control
    {
        private ButtonBase controlItem;
        private ButtonBase left;
        private ButtonBase right;
        private object @default;
        private int currentItem = 0;
        private string property;
        private string errorText;
        private IList items;
        private int itemCount;
        private SpinnerFlip flip = SpinnerFlip.FlipNone;

        private class SpinnerButton : ISpinnerButton
        {
            public SpinnerButton(Control control, bool enabled, bool left, bool right)
            {
                this.Control = control;
                this.Enabled = enabled;
                this.IsLeft = left;
                this.IsRight = right;
            }

            public bool Enabled
            {
                get;
                private set;
            }

            public bool IsRight
            {
                get;
                private set;
            }

            public bool IsLeft
            {
                get;
                private set;
            }

            public Control Control
            {
                get;
                private set;
            }
        }

        public Spinner()
        {
            this.SuspendLayout();
            this.controlItem = new ButtonBase();
            this.Add(this.controlItem);

            this.left = new ButtonBase();
            this.Add(this.left);

            this.right = new ButtonBase();
            this.Add(this.right);

            this.right.Click += right_Click;
            this.left.Click += left_Click;

            this.controlItem.SetImage(ImageType.Spinner_Control);
            this.left.SetImage(ImageType.Spinner_Button_Disabled);
            this.right.SetImage(ImageType.Spinner_Button_Disabled);
            this.ResumeLayout();
        }

        public SpinnerFlip Flip
        {
            get
            {
                return this.flip;
            }
            set
            {
                this.flip = value;
                switch (this.flip)
                {
                    case SpinnerFlip.FlipNone:
                        this.left.HorizontalFlipped = false;
                        this.right.HorizontalFlipped = false;
                        break;
                    case SpinnerFlip.FlipLeft:
                        this.left.HorizontalFlipped = true;
                        this.right.HorizontalFlipped = false;
                        break;
                    case SpinnerFlip.FlipRight:
                        this.left.HorizontalFlipped = false;
                        this.right.HorizontalFlipped = true;
                        break;
                }
            }
        }

        void right_Click(object sender, EventArgs e)
        {
            if (this.items != null && currentItem + 1 < this.items.Count)
            {
                currentItem++;
                this.UpdateSelectedItem();
                this.UpdateSpinnerButtons();
                this.OnPropertyChanged("SelectedItem");
            }
        }

        void left_Click(object sender, EventArgs e)
        {
            if (currentItem - 1 >= 0)
            {
                currentItem--;
                this.UpdateSelectedItem();
                this.UpdateSpinnerButtons();
                this.OnPropertyChanged("SelectedItem");
            }
        }

        public object DataSource
        {
            get
            {
                return this.items;
            }
            set
            {
                var list = value as IList;
                if (items != value || (list != null && list.Count != itemCount))
                {
                    this.items = list;
                    this.itemCount = list == null ? 0 : this.items.Count;

                    this.currentItem = 0; // set to 0 for now, if there is a default override it
                    this.SetDefault();
                    this.UpdateSelectedItem();
                    this.UpdateSpinnerButtons();
                    this.OnPropertyChanged("SelectedItem");
                }
            }
        }

        public string Property
        {
            get
            {
                return this.property;
            }
            set
            {
                this.property = value;
                this.UpdateSelectedItem();
            }
        }

        public string ErrorText
        {
            get
            {
                return this.errorText;
            }
            set
            {
                this.errorText = value;
                this.UpdateSelectedItem();
            }
        }

        public object Default
        {
            set
            {
                if (this.@default != value)
                {
                    this.@default = value;
                    SetDefault();
                }
            }
            get
            {
                return this.@default;
            }
        }

        private void SetDefault()
        {
            if (this.items != null)
            {
                for (int i = 0; i < this.items.Count; i++)
                {
                    if (Equals(this.items[i], this.@default))
                    {
                        this.currentItem = i;
                        this.UpdateSelectedItem();
                        this.UpdateSpinnerButtons();
                        this.OnPropertyChanged("SelectedItem");
                        break;
                    }
                }
            }
        }

        public object SelectedItem
        {
            get
            {
                if (this.items == null || this.currentItem >= this.items.Count || this.currentItem < 0)
                {
                    return null;
                }

                return this.items[this.currentItem];
            }
        }

        private void UpdateSelectedItem()
        {
            if (this.items == null || this.items.Count == 0)
            {
                this.controlItem.Caption = this.errorText ?? string.Empty;
            }
            else if (string.IsNullOrEmpty(this.property))
            {
                this.controlItem.Caption = this.items[this.currentItem].ToString();
            }
            else
            {
                var item = this.items[this.currentItem];
                var getter = item.GetType().GetProperty(this.property).GetGetMethod();
                this.controlItem.Caption = getter.Invoke(item, new object[0]).ToString();
            }
        }

        private void UpdateSpinnerButtons()
        {
            if (this.items == null)
            {
                this.right.SetImage(ImageType.Spinner_Button_Disabled);
                this.left.SetImage(ImageType.Spinner_Button_Disabled);
            }
            else
            {
                this.right.SetImage(this.currentItem < this.items.Count-1 ? ImageType.Spinner_Button_Enabled : ImageType.Spinner_Button_Disabled);
                this.left.SetImage(this.currentItem > 0 ? ImageType.Spinner_Button_Enabled : ImageType.Spinner_Button_Disabled);
            }
        }

        public IEnumerable<ISpinnerButton> GetButtonState()
        {
            bool leftEnabled = this.items != null && this.currentItem > 0;
            yield return new SpinnerButton(this.left, leftEnabled, true, false);

            bool rightEnabled = this.items != null && this.currentItem < this.items.Count - 1;
            yield return new SpinnerButton(this.right, rightEnabled, false, true);
        }

        protected override void OnLayout()
        {
            base.OnLayout();

            this.controlItem.SuspendLayout();
            this.left.SuspendLayout();
            this.right.SuspendLayout();

            this.controlItem.X = this.X;
            this.controlItem.Y = this.Y;
            this.controlItem.Z = this.Z + 1;

            this.left.Width = 40;
            this.left.Height = 40;

            this.right.Width = 40;
            this.right.Height = 40;

            this.controlItem.Width = this.Width - this.left.Width - this.right.Width - 160;
            this.controlItem.Height = 30;

            this.left.X = this.controlItem.X - this.controlItem.Width - this.left.Width - 20;
            this.left.Y = this.Y;
            this.left.Z = this.Z + 1;

            this.right.X = this.controlItem.X + this.controlItem.Width + this.right.Width + 20;
            this.right.Y = this.Y;
            this.right.Z = this.Z + 1;

            this.SetClipRectangle(this.X, this.Y, this.Width, this.Height);

            this.right.ResumeLayout();
            this.left.ResumeLayout();
            this.controlItem.ResumeLayout();
        }
    }
}
