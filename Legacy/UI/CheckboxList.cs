﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Islander.UI
{
    [Serializable]
    public class CheckboxListTemplate : ControlTemplate
    {
        public int Width { get; set; }
        public string Caption { get; set; }
        public bool Checked { get; set; }
    }

    [Serializable]
    public class CheckboxList : Control
    {
        [Serializable]
        private class CheckboxRow : FlowLayoutPanel
        {
            private LabelControl label;
            private Checkbox checkbox;

            public CheckboxRow()
            {
                this.Orientation = UI.Orientation.Horizontal;
                this.label = new LabelControl()
                    {
                        Width = 200,
                        Height = 30,
                    };
                this.checkbox = new Checkbox()
                    {
                        Width = 30,
                        Height = 30,
                    };
                this.checkbox.PropertyChanged += checkbox_PropertyChanged;

                this.Add(this.label);
                this.Add(this.checkbox);

                //this.SetImage(ImageType.Debug);
            }

            void checkbox_PropertyChanged(object sender, PropertyChangedEventArgs e)
            {
                if (e.Property == "Checked")
                {
                    this.OnPropertyChanged("Checked");
                }
            }

            public string Caption
            {
                get
                {
                    return this.label.Caption;
                }
                set
                {
                    this.label.Caption = value;
                }
            }

            public bool Checked
            {
                get
                {
                    return this.checkbox.Checked;
                }
                set
                {
                    this.checkbox.Checked = value;
                }
            }
        }

        private object dataSource;
        private CheckboxListTemplate template;
        private int columns;
        
        public CheckboxList()
        {
            this.columns = 1;
            //this.SetImage(ImageType.Debug);
        }

        public int Columns
        {
            get
            {
                return this.columns;
            }
            set
            {
                if (value <= 0)
                {
                    throw new InvalidOperationException("Value must be greater than 0");
                }

                if (this.columns != value)
                {
                    this.columns = value;
                    this.DetermineSize();
                    this.OnLayout();
                }
            }
        }

        private void DetermineSize()
        {
            float maxWidth = 0;
            float height = 0;
            int index = 0;
            foreach (var row in this.Children)
            {
                if (index == 0)
                {
                    height += row.Height + 5;
                }

                index = (++index) % this.columns;

                maxWidth = Math.Max(row.Width, maxWidth);
            }
            this.Height = height;
            this.Width = maxWidth * this.columns;
        }

        public object DataSource
        {
            get
            {
                return this.dataSource;
            }
            set
            {
                if (this.dataSource != value)
                {
                    this.dataSource = value;
                    this.SuspendLayout();

                    foreach (var child in this.Children)
                    {
                        if (child is CheckboxRow)
                        {
                            this.Remove(child);
                        }
                    }

                    IList list = this.dataSource as IList;
                    if (list != null)
                    {
                        foreach (var obj in list)
                        {
                            CheckboxRow row = new CheckboxRow();
                            this.Add(row);

                            if (this.template != null)
                            {
                                this.template.Bind(row);
                            }
                            row.Connect(obj);
                        }
                        this.DetermineSize();
                    }
                    this.ResumeLayout();
                }
            }
        }

        public CheckboxListTemplate Template
        {
            set
            {
                this.template = value;
            }
            get
            {
                return this.template;
            }
        }

        protected override void OnShow()
        {
            base.OnShow();
            this.DetermineSize();
        }

        protected override void OnLayout()
        {
            base.OnLayout();

            int index = 0;
            float y = this.Y + this.Height;
            foreach (var row in this.Children)
            {
                if (index == 0)
                {
                    y -= row.Height + 5;
                }

                float x = this.X - this.Width + row.Width + 2 * row.Width * index;

                row.SuspendLayout();
                row.Y = y;
                row.X = x;
                row.Z = this.Z + 1;
                row.ResumeLayout();

                if (index == 0)
                {
                    y -= row.Height + 5;
                }

                index = (++index) % this.columns;
            }

            this.SetClipRectangle(this.X, this.Y, this.Width, this.Height);
        }
    }
}
