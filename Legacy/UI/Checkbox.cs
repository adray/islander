﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Islander.UI
{
    [Serializable]
    public class Checkbox : ButtonBase
    {
        private bool @checked;
        private ButtonBase checkbox;
        private bool initialized;

        public Checkbox()
        {
            this.SuspendLayout();
            this.checkbox = new ButtonBase();

            this.checkbox.Width = 30;
            this.checkbox.Height = 30;
            this.checkbox.Click += checkbox_Click;
            this.Add(this.checkbox);

            SetImage();
            this.initialized = true;
            this.ResumeLayout();
        }

        void checkbox_Click(object sender, EventArgs e)
        {
            this.Checked = !this.Checked;
        }

        protected override void OnMouseClick()
        {
            base.OnMouseClick();
            this.Checked = !this.Checked;
        }

        private void SetImage()
        {
            if (@checked)
            {
                checkbox.SetImage(ImageType.Checkbox_Checked);
            }
            else
            {
                checkbox.SetImage(ImageType.Checkbox_Unchecked);
            }
        }

        public bool Checked
        {
            get
            {
                return this.@checked;
            }
            set
            {
                if (this.@checked != value)
                {
                    this.@checked = value;
                    // Swap the image.
                    this.SetImage();
                    this.OnPropertyChanged("Checked");
                }
            }
        }

        protected override void OnLayout()
        {
            if (!initialized)
                return;

            this.LabelOffsetX = this.checkbox.Width*2 + 10;
            this.LabelOffsetY = 0;

            this.checkbox.X = this.X - this.Width + this.checkbox.Width;
            this.checkbox.Y = this.Y;
            this.checkbox.Z = this.Z + 1;

            base.OnLayout();
        }
    }
}
