﻿using Islander.Managed.Scripting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Islander.UI
{
    [Serializable]
    public class TooltipStyle
    {
        public string Name { get; set; }
        public int Delay { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
        public TooltipAlign Align { get; set; }
        public TextStyle? TextStyle { get; set; }
        public long? CustomImage { get; set; }

        public TooltipStyle()
        {
            this.Width = 100;
            this.Height = 30;
            this.Align = TooltipAlign.Left;
        }
    }

    public enum TooltipAlign
    {
        Top,
        Left,
    }

    [Serializable]
    public class Tooltip
    {
        [Serializable]
        private class Item
        {
            public float Delay;
            public string Text;
            public Control Control;
            public TooltipStyle Style;
        }

        private IScriptingSystem system;
        private LabelControl label;
        private Dictionary<string, TooltipStyle> styles = new Dictionary<string, TooltipStyle>();
        private Dictionary<string, List<TooltipStyle>> tagged = new Dictionary<string, List<TooltipStyle>>();
        private Control control;
        private Item delayedItem;

        public Tooltip(IScriptingSystem system)
        {
            this.system = system;
            this.label = new LabelControl()
            {
                Visible = false,
                Wrap = true,
            };
            this.label.SetImage(ImageType.Tooltip);
            UIContainer.Default.ActivateInto(this.label);
        }

        public void ClearStyles(string tag = null)
        {
            if (string.IsNullOrEmpty(tag))
            {
                this.styles.Clear();
            }
            else
            {
                List<TooltipStyle> list = null;
                if (tagged.TryGetValue(tag, out list))
                {
                    this.tagged.Remove(tag);
                    foreach (var style in list)
                    {
                        this.styles.Remove(style.Name);
                    }
                }
            }
        }

        public void Show(string text, Control control)
        {
            if (control.TooltipStyle != null && this.styles.ContainsKey(control.TooltipStyle)
                && !string.IsNullOrEmpty(control.TooltipText) && control != this.control)
            {
                var style = this.styles[control.TooltipStyle];

                if (style.Delay == 0)
                {
                    this.DelayCore(text, style, control);
                }
                else
                {
                    this.delayedItem = new Item()
                    {
                        Control = control,
                        Delay = style.Delay,
                        Style = style,
                        Text = text,
                    };
                }

                if (this.control != null)
                {
                    this.control.VisibleChanged -= control_Destroyed;
                    this.control.Destroyed -= control_Destroyed;
                }

                this.control = control;
                this.control.VisibleChanged += control_Destroyed;
                this.control.Destroyed += control_Destroyed;
            }
        }

        void control_Destroyed(object sender, EventArgs e)
        {
            Control control = sender as Control;
            if (control == this.control)
            {
                control.VisibleChanged -= this.control_Destroyed;
                control.Destroyed -= this.control_Destroyed;
                this.Hide(control);
            }
        }

        private void DelayCore(string text, TooltipStyle style, Control control)
        {
            this.label.Caption = text;
            this.label.Width = style.Width;
            this.label.Height = style.Height;

            if (style.TextStyle == null)
            {
                this.label.SetTextStyle(new TextStyle());
            }
            else
            {
                this.label.SetTextStyle(style.TextStyle.Value);
            }

            if (style.CustomImage == null)
            {
                this.label.SetImage(ImageType.Tooltip);
            }
            else
            {
                this.label.SetCustomImage(style.CustomImage.Value);
            }

            if (style.Align == TooltipAlign.Left)
            {
                this.label.X = control.X + control.Width + this.label.Width;
                this.label.Y = control.Y;
            }
            else if (style.Align == TooltipAlign.Top)
            {
                this.label.X = control.X + this.label.Width - control.Width;
                this.label.Y = control.Y + control.Height + this.label.Height;
            }

            this.label.Z = control.Z + 1;
            this.label.Show();
        }

        public void Hide(Control control)
        {
            if (this.control == control)
            {
                this.label.Hide();
                this.control.VisibleChanged -= control_Destroyed;
                this.control.Destroyed -= control_Destroyed;
                this.control = null;
            }
        }

        public void AddStyle(string tag, TooltipStyle style)
        {
            if (!string.IsNullOrEmpty(tag))
            {
                this.tagged.Add(tag, new List<TooltipStyle>() { style });
            }

            this.styles.Add(style.Name, style);
        }

        public void AddStyles(string tag, List<TooltipStyle> list)
        {
            if (!string.IsNullOrEmpty(tag))
            {
                this.tagged.Add(tag, list);
            }

            foreach (var style in list)
            {
                this.styles.Add(style.Name, style);
            }
        }

        public void Tick(float delta)
        {
            if (this.delayedItem != null)
            {
                this.delayedItem.Delay -= delta;

                if (this.delayedItem.Delay <= 0)
                {
                    this.DelayCore(this.delayedItem.Text, this.delayedItem.Style, this.delayedItem.Control);
                    this.delayedItem = null;
                }
            }
        }
    }
}
