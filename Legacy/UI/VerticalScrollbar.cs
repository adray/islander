﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Islander.UI
{
    [Serializable]
    public class VerticalScrollbar : Control
    {
        [Serializable]
        private class SlidingControl : Control
        {
            public event EventHandler<DragEventArgs> Slide;

            public SlidingControl()
            {
                this.SetImage(ImageType.Slider_Item);
            }

            protected override void OnMouseClick()
            {
                base.OnMouseClick();
                UIContainer.Default.StartDragging(this);
            }

            protected override void OnMouseUp()
            {
                base.OnMouseUp();
                UIContainer.Default.EndDragging(this);
            }

            protected override void OnDrag(float mouseX, float mouseY)
            {
                base.OnDrag(mouseX, mouseY);
                if (this.Slide != null)
                {
                    this.Slide(this, new DragEventArgs(mouseX, mouseY));
                }
            }
        }

        private SlidingControl slider;
        private float sliderValue;

        public VerticalScrollbar()
        {
            this.slider = new SlidingControl();

            this.slider.Slide += slider_Slide;
            this.Add(this.slider);

            this.SetImage(ImageType.Slider);
        }

        void slider_Slide(object sender, DragEventArgs e)
        {
            RestrictSlider(e.Y);
            this.UpdateValue();
        }

        private void RestrictSlider(float y)
        {
            this.slider.Y = Math.Min(Math.Max(y, this.Y - this.Height + slider.Height), this.Y + this.Height - slider.Height);
        }

        private void UpdateValue()
        {
            float max = this.Y + this.Height - slider.Height;
            float min = this.Y - this.Height + slider.Height;
            float range = Max - Min;
            float r = max - min;

            float value = range - (range * (this.slider.Y - min) / r);

            if (value != this.sliderValue)
            {
                this.sliderValue = value;
                this.OnPropertyChanged("Value");
            }
        }

        private void CalcSliderY()
        {
            float max = this.Y + this.Height - slider.Height;
            float min = this.Y - this.Height + slider.Height;
            float range = Max - Min;
            float r = max - min;

            if (r > 0)
            {
                this.slider.Y = (this.sliderValue - range) * (-r / range) + min;
            }
            else
            {
                this.slider.Y = min;
            }
        }

        protected override void OnLayout()
        {
            base.OnLayout();

            float ratio = this.Height - (this.Max - this.Min);

            this.slider.Height = Math.Max(30, ratio);
            this.slider.Width = this.Width;
            this.slider.X = this.X;
            this.slider.Z = this.Z + 1;
            this.CalcSliderY();
        }

        public float Value
        {
            get
            {
                return this.sliderValue;
            }
            set
            {
                if (value < Min || value > Max)
                {
                    throw new InvalidOperationException("Slider value not in range");
                }

                this.sliderValue = value;
                this.CalcSliderY();
                this.OnPropertyChanged("Value");
            }
        }

        public float Max { get; set; }
        public float Min { get; set; }
    }
}
