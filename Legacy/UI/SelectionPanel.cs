﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Islander.UI
{
    public enum SelectionPanelSortType
    {
        Descending=0,
        Ascending=1,
    }

    public class SelectionPanelSort : ControlTemplate
    {
        public string Caption { get; set; }
        public string Property { get; set; }
        public SelectionPanelSortType Sort { get; set; }
    }

    [Serializable]
    public class SelectionPanel : Control, IContextMenuHost
    {
        private List<SelectionLayout> items;
        private FlowLayoutPanel sort;
        private object dataSource;
        private HashSet<object> selectedItemsSet;
        private ContextMenu menu;
        private const int buttonWidth = 250;
        private VerticalScrollbar scrollbar;
        private object currentSelectedItem;

        public object SelectedItems { get; private set; }

        [Serializable]
        private class SelectionSortButton : ButtonBase
        {
            public SelectionSortButton()
            {
                this.SetImage(ImageType.Button);
            }

            protected override void OnLayout()
            {
                base.OnLayout();

                this.SetClipRectangle(this.X, this.Y, this.Width, this.Height);
            }

            public string Property { get; set; }
            public SelectionPanelSortType Sort { get; set; }
        }

        [Serializable]
        private class SelectionContextButton : ButtonBase
        {
            public object Data { get; set; }

            public SelectionContextButton()
            {
                this.SetImage(ImageType.Button);
            }

            protected override void OnLayout()
            {
                base.OnLayout();

                this.SetClipRectangle(this.X, this.Y, this.Width, this.Height);
            }
        }

        [Serializable]
        private class SelectionButton : ButtonBase
        {
            private bool selected;

            public object Data { get; private set; }

            public bool Selected
            {
                get
                {
                    return this.selected;
                }
                set
                {
                    this.selected = value;
                    this.SetImage(this.Selected ? ImageType.SelectionButton_Selected : ImageType.SelectionButton_Unselected);
                }
            }

            public SelectionButton()
            {
                this.SetImage(ImageType.SelectionButton_Unselected);
            }

            protected override void OnConnected(object dataSource)
            {
                base.OnConnected(dataSource);

                this.Data = dataSource;
            }

            protected override void OnLayout()
            {
                base.OnLayout();

                this.SetClipRectangle(this.X, this.Y, this.Width, this.Height);
            }
        }

        [Serializable]
        private class SelectionLayout : FlowLayoutPanel
        {
            private SelectionContextButton context;
            private SelectionButton selection;

            public SelectionLayout()
            {
                this.Orientation = UI.Orientation.Horizontal;
            }

            public SelectionContextButton Context
            {
                get
                {
                    return this.context;
                }
                set
                {
                    this.context = value;
                    this.Add(this.context);
                }
            }

            public SelectionButton Selection
            {
                get
                {
                    return this.selection;
                }
                set
                {
                    this.selection = value;
                    this.Add(this.selection);
                }
            }
        }

        public SelectionPanel()
        {
            this.items = new List<SelectionLayout>();
            this.sort = new FlowLayoutPanel()
            {
                Orientation = Orientation.Vertical,
            };
            this.selectedItemsSet = new HashSet<object>();

            this.sort.Add(new LabelControl()
                {
                    Caption = "Sort",
                    Width = 100,
                    Height = 30,
                });

            this.Add(this.sort);

            this.scrollbar = new VerticalScrollbar()
            {
                Width = 20,
                Height = this.Height,
            };
            this.scrollbar.PropertyChanged += scrollbar_PropertyChanged;
            this.Add(this.scrollbar);

            //this.SetImage(ImageType.Debug);
        }

        void scrollbar_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.Property == "Value")
            {
                this.OnLayout();
            }
        }

        public object DataSource
        {
            get
            {
                return this.dataSource;
            }
            set
            {
                if (this.dataSource != value)
                {
                    this.dataSource = value;

                    IList list = this.dataSource as IList;
                    if (list != null)
                    {
                        this.SuspendLayout();
                        foreach (var item in list)
                        {
                            var selection = new SelectionLayout();
                            var button = new SelectionButton()
                                {
                                    Width = 200,
                                    Height = 30,
                                };
                            button.Click += button_Click;
                            if (!string.IsNullOrEmpty(this.Display))
                            {
                                button.AddBinding(new Binding(button, this.Display, "Caption"));
                            }
                            button.Connect(item);
                            selection.Selection = button;

                            var context = new SelectionContextButton()
                                {
                                    Width = 50,
                                    Height = 30,
                                };
                            selection.Context = context;
                            context.Click += context_Click;
                            context.Data = item;

                            this.items.Add(selection);
                            this.Add(selection);
                        }
                    }
                    this.ResumeLayout();
                }
            }
        }

        void context_Click(object sender, EventArgs e)
        {
            var button = sender as SelectionContextButton;
            if (button != null && this.menu != null && this.selectedItemsSet.Count <= this.MaxCount)
            {
                this.currentSelectedItem = button.Data;
                this.menu.X = button.X;
                this.menu.Y = button.Y - button.Height - this.menu.Height;
                this.menu.Show();
            }
        }

        protected override void OnConnected(object dataSource)
        {
            base.OnConnected(dataSource);

            if (this.menu != null)
            {
                this.menu.Connect(dataSource);
            }
        }

        public ContextMenu Menu
        {
            get
            {
                return this.menu;
            }
            set
            {
                if (this.menu != value)
                {
                    if (this.menu != null)
                    {
                        this.menu.Click -= menu_Click;
                    }

                    this.menu = value;

                    if (this.menu != null)
                    {
                        this.menu.Click += menu_Click;
                    }
                    this.OnPropertyChanged("Menu");
                }
            }
        }

        void menu_Click(object sender, ContextMenuEventArgs e)
        {
            if (this.currentSelectedItem != null)
            {
                var setter = this.currentSelectedItem
                    .GetType()
                    .GetProperty(this.Property, System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.Public)
                    .GetSetMethod();
                if (setter != null)
                {
                    setter.Invoke(this.currentSelectedItem, new object[1]{ e.Item });
                }
            }
        }

        public string Property
        {
            get;
            set;
        }

        public int MaxCount
        {
            get;
            set;
        }

        public void AddSort(SelectionPanelSort sort)
        {
            var button = new SelectionSortButton()
                {
                    Width = 120,
                    Height = 30,
                    Margin = new Padding() { Down = 5, }
                };
            sort.Bind(button);

            if (!string.IsNullOrEmpty(sort.Caption))
            {
                button.Caption = sort.Caption;
            }

            if (!string.IsNullOrEmpty(sort.Property))
            {
                button.Property = sort.Property;
            }

            button.Sort = sort.Sort;

            button.Click += sort_Click;
            this.sort.Add(button);
        }

        void sort_Click(object sender, EventArgs e)
        {
            var button = sender as SelectionSortButton;
            if (button != null)
            {
                Dictionary<Control, object> map = new Dictionary<Control, object>();

                foreach (var control in this.items)
                {
                    var obj = control.Selection.Data.GetType()
                        .GetProperty(button.Property, System.Reflection.BindingFlags.Instance|System.Reflection.BindingFlags.Public)
                        .GetGetMethod()
                        .Invoke(control.Selection.Data, new object[0]);
                    map.Add(control, obj);
                }

                if (button.Sort == SelectionPanelSortType.Descending)
                {
                    this.items = this.items.OrderByDescending(s => map[s]).ToList();
                }
                else if (button.Sort == SelectionPanelSortType.Ascending)
                {
                    this.items = this.items.OrderBy(s => map[s]).ToList();
                }
                this.OnLayout();
            }
        }

        public string Display
        {
            get;
            set;
        }

        void button_Click(object sender, EventArgs e)
        {
            var button = sender as SelectionButton;
            if (button != null)
            {
                if (this.selectedItemsSet.Contains(button.Data) && button.Selected)
                {
                    button.Selected = false;
                    this.selectedItemsSet.Remove(button.Data);
                    ((IList)this.SelectedItems).Remove(button.Data);
                    this.OnPropertyChanged("SelectedItems");
                }
                else if (this.selectedItemsSet.Count < this.MaxCount)
                {
                    button.Selected = true;
                    this.selectedItemsSet.Add(button.Data);
                    ((IList)this.SelectedItems).Add(button.Data);
                    this.OnPropertyChanged("SelectedItems");
                }
            }
        }

        protected override void OnLayout()
        {
            base.OnLayout();

            if (this.menu != null)
            {
                this.menu.Z = this.Z + 2;
            }


            if (this.sort.Children.Count >= 2)
            {
                float width = this.Width * 0.7f;
                this.LayoutButtons(width, this.Height);

                this.sort.SuspendLayout();
                this.sort.Visible = true;
                this.sort.Width = this.Width - width;
                this.sort.Height = this.Height;
                this.sort.Y = this.Y;
                this.sort.X = this.X + this.Width - this.sort.Width;
                this.sort.Z = this.Z + 1;
                this.sort.ResumeLayout();

                this.LayoutScrollbar(width);
            }
            else
            {
                this.LayoutButtons(this.Width, this.Height);
                this.sort.Visible = false;

                this.LayoutScrollbar(this.Width);
            }

            this.SetClipRectangle(this.X, this.Y, this.Width, this.Height);
        }

        private void LayoutScrollbar(float width)
        {
            if (this.scrollbar != null)
            {
                this.scrollbar.SuspendLayout();
                this.scrollbar.X = this.X + width/2 - this.scrollbar.Width;
                this.scrollbar.Y = this.Y;
                this.scrollbar.Z = this.Z + 1;
                this.scrollbar.Height = this.Height;
                this.scrollbar.ResumeLayout();
            }
        }

        private void LayoutButtons(float width, float height)
        {
            float x = this.X - this.Width;
            float startY = this.Y + this.Height -30;
            if (this.scrollbar != null)
            {
                startY += this.scrollbar.Value;
            }
            float y = 0;
            int count = (int)width / buttonWidth;
            int index = 0;

            foreach (var item in this.items)
            {
                if (index++ >= count)
                {
                    x = this.X - this.Width;
                    y -= item.Height * 2 + 10;
                    index = 1;
                }

                x += item.Width + 5;

                item.SuspendLayout();
                item.X = x;
                item.Y = startY + y;
                item.Z = this.Z + 1;
                item.ResumeLayout();

                x += item.Width + 5;
            }

            y -= 60; // First half of first control and second half of last control

            if (this.scrollbar != null)
            {
                this.scrollbar.Max = Math.Max(0, -(y + this.Height*2));
                this.scrollbar.Visible = this.scrollbar.Max > 0;
            }
        }
    }
}
