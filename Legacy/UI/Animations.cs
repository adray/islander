﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Islander.UI
{
    /// <summary>
    /// Collection safe to add/remove while iterating.
    /// </summary>
    [Serializable]
    public sealed class LazyQueue<T> : IEnumerable<T>
    {
        private Queue<T> collection = new Queue<T>();
        private HashSet<T> removed = new HashSet<T>();
        private int references;

        public void Add(T item)
        {
            this.collection.Enqueue(item);
        }

        public void AddRange(IEnumerable<T> items)
        {
            foreach (var item in items)
            {
                this.collection.Enqueue(item);
            }
        }

        public void Remove(T item)
        {
            this.removed.Add(item);
        }

        private class LazyIterator : IEnumerator<T>
        {
            private Queue<T> copied;
            private LazyQueue<T> queue;
            private T current;
            private int position;
            private bool committed;

            public LazyIterator(LazyQueue<T> queue)
            {
                this.queue = queue;
                this.copied = new Queue<T>(queue.collection);
                this.queue.references++;
            }

            public T Current
            {
                get { return this.current; }
            }

            public void Dispose()
            {
                if (!this.committed)
                {
                    this.queue.references--;
                }
            }

            object System.Collections.IEnumerator.Current
            {
                get { return this.current; }
            }

            public bool MoveNext()
            {
                bool success = false;
                if (this.position < this.copied.Count)
                {
                    this.current = this.copied.Dequeue();
                    if (!this.queue.removed.Contains(this.current))
                    {
                        success = true;
                    }
                    else
                    {
                        while (this.copied.Count > 0 && !success)
                        {
                            this.current = this.copied.Dequeue();
                            if (!this.queue.removed.Contains(this.current))
                            {
                                success = true;
                            }
                        }
                    }

                    if (success)
                    {
                        this.position++;
                        this.copied.Enqueue(this.current);
                    }
                    else
                    {
                        this.queue.references--;
                        if (this.queue.references == 0)
                        {
                            this.committed = true;
                            this.queue.collection = this.copied;
                            this.queue.removed.Clear();
                        }
                    }
                }

                return success;
            }

            public void Reset()
            {
                this.position = 0;
                this.copied = new Queue<T>(this.queue.collection);

                if (this.committed)
                {
                    this.committed = false;
                    this.queue.references++;
                }
            }
        }

        public IEnumerator<T> GetEnumerator()
        {
            return new LazyIterator(this);
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return new LazyIterator(this);
        }
    }

    public interface IStoryboard
    {
        LazyQueue<IAnimator> Animations { get; }
    }

    internal class Storyboard : IStoryboard
    {
        public LazyQueue<IAnimator> Animations { get; private set; }
        
        public Storyboard()
        {
            this.Animations = new LazyQueue<IAnimator>();
        }        
    }

    public interface IAnimator
    {
        void Start();
        void Update(float delta);
        bool IsComplete { get; }
    }

    internal enum TriggerType
    {
        OnLoad,
    }

    /// <summary>
    /// Provides easing functionallity.
    /// Where t=elapsed time, b=start, c=end-start, d=total time
    /// </summary>
    public static class Easing
    {
        public static float EaseInExpo(float t, float b, float c, float d)
        {
            return (t == 0) ? b : c * (float)Math.Pow(2, 10 * (t / d - 1)) + b;
        }

        public static float EaseInQuad(float t, float b, float c, float d)
        {
            t /= d;
            return c * t * t * t + b;
        }
    }

    internal class Animator : IAnimator
    {
        private Control control;
        private string property;
        private object start;
        private object end;
        private float duration;
        private float elapsed;
        private BoundObject bound;
        private Binding binding;
        private bool destroyed;

        private class BoundObject
        {
            public object Bound { get; set; }
        }

        public Animator(Control control, string property, float duration)
        {
            this.control = control;
            this.property = property;
            this.duration = duration;

            this.bound = new BoundObject();
            this.binding = new Binding(control, "Bound", property);
            this.binding.Hook(this.bound);

            this.control.Destroyed += control_Destroyed;
        }

        void control_Destroyed(object sender, EventArgs e)
        {
            this.destroyed = true;
        }

        private object ChangeType(object val)
        {
            return System.Convert.ChangeType(val, typeof(float));
        }

        public object Begin
        {
            get
            {
                return this.start;
            }
            set
            {
                if (value == null)
                {
                    this.start = null;
                }
                else
                {
                    this.start = ChangeType(value);
                }
            }
        }

        public object End
        {
            get
            {
                return this.end;
            }
            set
            {
                if (value == null)
                {
                    this.end = null;
                }
                else
                {
                    this.end = ChangeType(value);
                }
            }
        }

        public void Start()
        {
            this.binding.Push();

            if (this.end == null)
            {
                this.end = this.bound.Bound;
            }

            if (this.start == null)
            {
                this.start = this.bound.Bound;
            }
        }

        public void Update(float delta)
        {
            if (!this.destroyed)
            {
                this.elapsed += delta;

                if (this.bound.Bound is float)
                {
                    var s = (float)this.start;
                    var e = (float)this.end;
                    this.bound.Bound = Math.Min(e, Easing.EaseInQuad(this.elapsed, s, (e - s), duration));
                }

                this.binding.Pull();
            }
        }
        
        public bool IsComplete
        {
            get { return this.destroyed || this.elapsed >= this.duration; }
        }
    }
}
