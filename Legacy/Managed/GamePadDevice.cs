﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace Islander.Managed
{
    public enum GamePadButton
    {
        A,
        B,
        X,
        Y,
        Start,
        Select,
        LeftButton,
        RightButton,
        DPadLeft,
        DPadRight,
        DPadUp,
        DPadDown,
        LeftThumb,
        RightThumb,
        Count
    };

    [StructLayout(LayoutKind.Sequential)]
    internal unsafe struct GamePadState
    {
        public bool connected;
        public float RX;
        public float RY;
        public float LX;
        public float LY;
        public float LeftTrigger;
        public float RightTrigger;
        public fixed bool buttons[(int)GamePadButton.Count];
    };

    public unsafe class GamePad
    {
        private GamePadState* state;
        private int id;

        internal GamePad(IntPtr ptr, int id)
        {
            this.state = (GamePadState*)ptr;
            this.id = id;
        }

        public bool GetConnected()
        {
            return state->connected;
        }

        public int ID => id;

        public bool GetButtonDown(GamePadButton button)
        {
            return state->buttons[(int)button];
        }

        public float GetRightStickX()
        {
            return state->RX;
        }

        public float GetRightStickY()
        {
            return state->RY;
        }

        public float GetLeftStickX()
        {
            return state->LX;
        }

        public float GetLeftStickY()
        {
            return state->LY;
        }

        public float GetRightTrigger()
        {
            return state->RightTrigger;
        }

        public float GetLeftTrigger()
        {
            return state->LeftTrigger;
        }
    }

    public class GamePadDevice : IDisposable
    {
        private const int MaxGamePads = 4;
        private IntPtr device;
        private readonly GamePad[] gamepads;

        public static bool IsGamePadSupported()
        {
            return NativeMethods.IsGamePadSupported() == 1;
        }

        public static GamePadDevice CreateDevice()
        {
            if (!IsGamePadSupported())
            {
                return null;
            }

            return new GamePadDevice();
        }

        private GamePadDevice()
        {
            this.device = NativeMethods.CreateGamePadDevice();

            this.gamepads = new GamePad[MaxGamePads];
            for (int i = 0; i < MaxGamePads; i++)
            {
                this.gamepads[i] = new GamePad(NativeMethods.GetGamepadState(this.device, i), i);
            }
        }

        public bool IsGamePadConnected()
        {
            return NativeMethods.GamePadDetected(this.device) == 1;
        }

        public void Update()
        {
            NativeMethods.UpdateGamePadDevice(this.device);
        }

        public GamePad[] GetGamePads()
        {
            return this.gamepads;
        }

        /// <summary>
        /// Rumbles the gamepad if it is supported. Right - high frequency motor, Left - low frequency motor. Values between 0-1.
        /// </summary>
        public void SetRumble(GamePad pad, float right, float left)
        {
            NativeMethods.IslanderGamePadRumble(device, pad.ID, right, left);
        }

        public void Dispose()
        {
            if (this.device != IntPtr.Zero)
            {
                NativeMethods.DestroyGamePadDevice(this.device);
                this.device = IntPtr.Zero;
            }
        }
    }
}
