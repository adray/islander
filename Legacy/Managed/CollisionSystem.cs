﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace Islander.Managed
{
    [StructLayout(LayoutKind.Sequential)]
    internal struct InternalCollisionBox
    {
        public float maxx, maxy, maxz;
        public float minx, miny, minz;
    }

    [StructLayout(LayoutKind.Sequential)]
    internal struct InternalCollisionData
    {
        public float px, py, pz;
        public IntPtr static_mesh1;
        public IntPtr static_mesh2;
        public IntPtr static_mesh3;
        public IntPtr static_mesh4;
        public IntPtr dynamic_mesh1;
        public IntPtr dynamic_mesh2;
        public IntPtr dynamic_mesh3;
        public IntPtr dynamic_mesh4;
        public int static_mesh_count;
        public int dynamic_mesh_count;
    };

    public class CollisionData
    {
        public float px, py, pz;
        public bool collision;
        internal IntPtr static_mesh1;
        internal IntPtr static_mesh2;
        internal IntPtr static_mesh3;
        internal IntPtr static_mesh4;
        internal IntPtr dynamic_mesh1;
        internal IntPtr dynamic_mesh2;
        internal IntPtr dynamic_mesh3;
        internal IntPtr dynamic_mesh4;
        internal int static_mesh_count;
        internal int dynamic_mesh_count;
    }

    [StructLayout(LayoutKind.Sequential)]
    internal struct InternalRayCollisionData
    {
        internal IntPtr ignore_mesh;
        internal IntPtr static_mesh;
        internal IntPtr dynamic_mesh;
    }

    public class RayCollisionData
    {
        internal IntPtr static_mesh;
        internal IntPtr dynamic_mesh;
    }

    public class CollisionBox
    {
        public Vec3f Min { get; set; }
        public Vec3f Max { get; set; }
    }

    public class CollisionStaticMesh
    {
        internal readonly IntPtr ptr;

        [StructLayout(LayoutKind.Sequential)]
        private struct InternalStaticMesh
        {
            public InternalCollisionBox box;

            // Object transform.
            public float px, py, pz;
            public float rx, ry, rz;
            public float sx, sy, sz;

            // Object is not solid, acts like a trigger.
            public int trigger;

            // Object is only for rays.
            public int rayonly;

            // Triangle data
            public IntPtr polymesh;
        }

        internal unsafe CollisionStaticMesh(IntPtr system, CollisionBox box, IEntity entity, bool trigger, bool rayonly, IntPtr polydata)
        {
            var t = entity.GetComponent<ITransform>();

            InternalStaticMesh mesh = new InternalStaticMesh()
            {
                box = new  InternalCollisionBox()
                {
                    maxx = box.Max.X,
                    maxy = box.Max.Y,
                    maxz = box.Max.Z,
                    minx = box.Min.X,
                    miny = box.Min.Y,
                    minz = box.Min.Z
                },
                px = t.Position.X,
                py = t.Position.Y,
                pz = t.Position.Z,
                rx = t.Direction.X,
                ry = t.Direction.Y,
                rz = t.Direction.Z,
                sx = t.Scale.X,
                sy = t.Scale.Y,
                sz = t.Scale.Z,
                trigger = trigger ? 1 : 0,
                rayonly = rayonly ? 1 : 0,
                polymesh = polydata
            };

            this.ptr = NativeMethods.AddStaticMesh(system, new IntPtr(&mesh));
        }
    }

    public class CollisionDynamicMesh
    {
        internal readonly IntPtr ptr;
        private readonly CollisionBox box;
        private readonly IEntity entity;
        private readonly bool trigger;

        [StructLayout(LayoutKind.Sequential)]
        private struct InternalDynamicMesh
        {
            // Position of object.
            public float px, py, pz;

            // Scale of object.
            public float sx, sy, sz;

            // Rotation of object.
            public float rx, ry, rz;

            // Size of box.
            public InternalCollisionBox box;

            // Value of max height of stairs can climb.
            public float stepUpY;

            // Velocity of object.
            public float vx, vy, vz;

            // Object is not solid, acts like a trigger.
            public int trigger;

            // Object is only for rays.
            public int rayonly;
        }

        internal unsafe CollisionDynamicMesh(IntPtr system, CollisionBox box, IEntity entity, float stepUpY, Vec3f velocity, bool trigger, bool rayonly)
        {
            this.entity = entity;
            this.box = box;
            this.trigger = trigger;

            var t = entity.GetComponent<ITransform>();

            InternalDynamicMesh mesh = new InternalDynamicMesh()
            {
                box = new InternalCollisionBox()
                {
                    maxx = box.Max.X,
                    maxy = box.Max.Y,
                    maxz = box.Max.Z,
                    minx = box.Min.X,
                    miny = box.Min.Y,
                    minz = box.Min.Z
                },
                px = t.Position.X,
                py = t.Position.Y,
                pz = t.Position.Z,
                sx = t.Scale.X,
                sy = t.Scale.Y,
                sz = t.Scale.Z,
                rx = t.Direction.X,
                ry = t.Direction.Y,
                rz = t.Direction.Z,
                vx = velocity.X,
                vy = velocity.Y,
                vz = velocity.Z,
                trigger = trigger ? 1 : 0,
                rayonly = rayonly ? 1 : 0,
                stepUpY = stepUpY
            };

            this.ptr = NativeMethods.AddDynamicMesh(system, new IntPtr(&mesh));
        }

        public unsafe void SetVelocity(Vec3f velocity)
        {
            var t = entity.GetComponent<ITransform>();

            InternalDynamicMesh mesh = new InternalDynamicMesh()
            {
                box = new InternalCollisionBox()
                {
                    maxx = box.Max.X,
                    maxy = box.Max.Y,
                    maxz = box.Max.Z,
                    minx = box.Min.X,
                    miny = box.Min.Y,
                    minz = box.Min.Z
                },
                px = t.Position.X,
                py = t.Position.Y,
                pz = t.Position.Z,
                sx = t.Scale.X,
                sy = t.Scale.Y,
                sz = t.Scale.Z,
                rx = t.Direction.X,
                ry = t.Direction.Y,
                rz = t.Direction.Z,
                vx = velocity.X,
                vy = velocity.Y,
                vz = velocity.Z,
                trigger = trigger ? 1 : 0
            };

            NativeMethods.UpdateDynamicMesh(this.ptr, new IntPtr(&mesh));
        }

        public unsafe CollisionData GetCollisionData()
        {
            InternalCollisionData data;

            NativeMethods.GetCollisionInfo(this.ptr, new IntPtr(&data));

            return new CollisionData()
            {
                px = data.px,
                py = data.py,
                pz = data.pz,
                collision = data.dynamic_mesh_count > 0 || data.static_mesh_count > 0,
                dynamic_mesh1 = data.dynamic_mesh1,
                dynamic_mesh2 = data.dynamic_mesh2,
                dynamic_mesh3 = data.dynamic_mesh3,
                dynamic_mesh4 = data.dynamic_mesh4,
                static_mesh1 = data.static_mesh1,
                static_mesh2 = data.static_mesh2,
                static_mesh3 = data.static_mesh3,
                static_mesh4 = data.static_mesh4,
                dynamic_mesh_count = data.dynamic_mesh_count,
                static_mesh_count = data.static_mesh_count
            };
        }
    }

    public class CollisionSystem
    {
        private readonly IntPtr ptr;

        public CollisionSystem()
        {
            this.ptr = NativeMethods.CreateCollisionSystem();
        }

        public CollisionStaticMesh AddStaticMesh(CollisionBox box, IEntity entity, bool trigger, bool rayonly = false)
        {
            return new CollisionStaticMesh(this.ptr, box, entity, trigger, rayonly, IntPtr.Zero);
        }

        public CollisionStaticMesh AddStaticMesh(CollisionBox box, IEntity entity, PolygonMesh polymesh, bool trigger, bool rayonly = false)
        {
            return new CollisionStaticMesh(this.ptr, box, entity, trigger, rayonly, polymesh.ptr);
        }

        public CollisionDynamicMesh AddDynamicMesh(CollisionBox box, IEntity entity, float stepUpY, Vec3f velocity, bool trigger, bool rayonly = false)
        {
            return new CollisionDynamicMesh(this.ptr, box, entity, stepUpY, velocity, trigger, rayonly);
        }

        public void RemoveStaticMesh(CollisionStaticMesh mesh)
        {
            NativeMethods.RemoveStaticMesh(this.ptr, mesh.ptr);
        }

        public void RemoveDynamicMesh(CollisionDynamicMesh mesh)
        {
            NativeMethods.RemoveDynamicMesh(this.ptr, mesh.ptr);
        }

        public void RunSimulation(float delta)
        {
            NativeMethods.RunCollision(this.ptr, delta);
        }

        public unsafe bool Collides(CollisionData data, CollisionStaticMesh staticMesh)
        {
            for (int i = 0; i < data.static_mesh_count; i++)
            {
                if (data.static_mesh1 == staticMesh.ptr ||
                    data.static_mesh2 == staticMesh.ptr ||
                    data.static_mesh3 == staticMesh.ptr ||
                    data.static_mesh4 == staticMesh.ptr)
                {
                    return true;
                }
            }

            return false;
        }

        public unsafe bool Collides(CollisionData data, CollisionDynamicMesh dynamicMesh)
        {
            for (int i = 0; i < data.dynamic_mesh_count; i++)
            {
                if (data.dynamic_mesh1 == dynamicMesh.ptr ||
                    data.dynamic_mesh2 == dynamicMesh.ptr ||
                    data.dynamic_mesh3 == dynamicMesh.ptr ||
                    data.dynamic_mesh4 == dynamicMesh.ptr)
                {
                    return true;
                }
            }

            return false;
        }

        public unsafe RayCollisionData RayCast(Camera3D camera, float px, float py, float pz, float dx, float dy, float dz)
        {
            InternalRayCollisionData data;
            data.ignore_mesh = IntPtr.Zero;

            NativeMethods.RayCast(this.ptr, camera.camera, px, py, pz, dx, dy, dz, new IntPtr(&data));
            return new RayCollisionData()
            {
                dynamic_mesh = data.dynamic_mesh,
                static_mesh = data.static_mesh
            };
        }

        public unsafe RayCollisionData RayCast(CollisionDynamicMesh source, float px, float py, float pz, float dx, float dy, float dz)
        {
            InternalRayCollisionData data;
            data.ignore_mesh = source.ptr;

            NativeMethods.RayCast(this.ptr, IntPtr.Zero, px, py, pz, dx, dy, dz, new IntPtr(&data));
            return new RayCollisionData()
            {
                dynamic_mesh = data.dynamic_mesh,
                static_mesh = data.static_mesh
            };
        }

        public unsafe bool Collides(RayCollisionData data, CollisionDynamicMesh dynamicMesh)
        {
            return data.dynamic_mesh == dynamicMesh.ptr;
        }

        public unsafe bool Collides(RayCollisionData data, CollisionStaticMesh staticMesh)
        {
            return data.static_mesh == staticMesh.ptr;
        }

        public unsafe bool CollidesOnlyWith(RayCollisionData data, CollisionDynamicMesh dynamicMesh)
        {
            return data.dynamic_mesh == dynamicMesh.ptr &&
                data.static_mesh == IntPtr.Zero;
        }

        public unsafe bool CollidesOnlyWith(RayCollisionData data, CollisionStaticMesh staticMesh)
        {
            return data.static_mesh == staticMesh.ptr &&
                data.dynamic_mesh == IntPtr.Zero;
        }

        public unsafe static void TranformRay(Camera3D camera, ref float px, ref float py, ref float pz, ref float dx, ref float dy, ref float dz)
        {
            float px2 = px;
            float py2 = py;
            float pz2 = pz;

            float dx2 = dx;
            float dy2 = dy;
            float dz2 = dz;

            NativeMethods.TransformRay(camera.camera, new IntPtr(&px2), new IntPtr(&py2), new IntPtr(&pz2), new IntPtr(&dx2), new IntPtr(&dy2), new IntPtr(&dz2));

            px = px2;
            py = py2;
            pz = pz2;

            dx = dx2;
            dy = dy2;
            dz = dz2;
        }
    }
}
