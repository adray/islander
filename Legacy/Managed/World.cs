﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;
using System.Text;

namespace Islander.Managed
{
    public interface IWorld : IDisposable
    {
        IEntity CreateEntity();
        void RemoveEntity(IEntity entity);
        WorldBindings Binding { get; }
        List<StatBlock> GatherStats();
    }

    public static class WorldBuilder
    {
        public static IWorld CreateWorld()
        {
            return new World();
        }
    }

    internal enum WorldComponents
    {
        Entity=0,
        Sprite=1,
        Transform=2,
        Mesh=3,
        Text=4,
    }
    
    internal class DefaultValueAttribute : Attribute
    {
        public object Value { get; private set; }

        public DefaultValueAttribute(object value)
        {
            this.Value = value;
        }
    }

    public class StatBlock
    {
        public int FreeRows { get; private set; }
        public int UsedRows { get; private set; }
        public int MaxRows { get; private set; }
        public int MemUsage { get; private set; }

        internal StatBlock(int freeRow, int usedRows, int maxRows, int memUsage)
        {
            this.FreeRows = freeRow;
            this.UsedRows = usedRows;
            this.MaxRows = maxRows;
            this.MemUsage = memUsage;
        }
    }

    [Serializable]
    internal sealed class World : IWorld
    {
        [NonSerialized]
        private IntPtr ptr;
        [NonSerialized]
        private WorldBindings binding;
        internal Guid guid = Guid.NewGuid();
        private bool disposed;

        internal unsafe World() : this(NativeMethods.CreateWorld())
        {
        }

        public unsafe List<StatBlock> GatherStats()
        {
            int count=0;
            IntPtr stats = NativeMethods.GatherStats(this.ptr, new IntPtr(&count));
            
            List<StatBlock> blocks = new List<StatBlock>();
            for (int i = 0; i < count; i++)
            {
                InternalStatBlock block;
                NativeMethods.GetStatBlock(stats, i, new IntPtr(&block));
                blocks.Add(new StatBlock(block._free_rows, block._used_rows, block._max_rows, block._mem_usage));
            }

            NativeMethods.FreeStats(stats);

            return blocks;
        }

        internal unsafe void CreateTable(InternalSchema schema)
        {
            int type = NativeMethods.GetComponentTable(this.ptr, schema._name);
            if (type == -1)
            {
                type = NativeMethods.AddComponentTable(this.ptr,
                    new IntPtr(&schema));
            }
        }

        internal unsafe void InitializeColumns<T>(InternalSchema schema, WorldComponents c, int offset)
        {
            int type = NativeMethods.GetComponentTable(this.ptr, schema._name);
            
            var t = typeof(T);

            var row = (T)Activator.CreateInstance(t);

            var members = t.GetFields().OrderBy(i => 
                Marshal.OffsetOf(t, i.Name).ToInt32());
            foreach (var mem in members)
            {
                if (!mem.FieldType.IsPointer && mem.FieldType != typeof(IntPtr)
                    && !mem.IsNotSerialized && mem.Name != "id")
                {
                    NativeMethods.AddComponentColumn(this.ptr, (int)c, mem.Name,
                        Marshal.OffsetOf(t, mem.Name).ToInt32() + offset,
                        Marshal.SizeOf(mem.FieldType));
                }

                var attribs = mem.GetCustomAttributes(typeof(DefaultValueAttribute), false);
                if (attribs.Length > 0)
                {
                    var @default = (DefaultValueAttribute)attribs[0];
                    var val = @default.Value;
                    mem.SetValue(row, val);
                }
            }

            // Set the default for this table
            IntPtr x = Marshal.AllocHGlobal(Marshal.SizeOf(t));
            Marshal.StructureToPtr(row, x, false);
            NativeMethods.SetDefault(this.ptr, type, offset, x, Marshal.SizeOf(t));
            Marshal.FreeHGlobal(x);
        }

        internal unsafe World(IntPtr handle)
        {
            this.ptr = handle;

            InternalSchema entitySchema = new InternalSchema();
            SchemaHelper.CreateSchema(ref entitySchema, "entity", "id", sizeof(Entity.InternalEntity) + sizeof(CustomComponentSink.Properties), 0, sizeof(int));
            CreateTable(entitySchema);
            //InitializeColumns<Entity.InternalEntity>(entitySchema, WorldComponents.Entity, 0);
            InitializeColumns<CustomComponentSink.Properties>(entitySchema, WorldComponents.Entity, sizeof(Entity.InternalEntity));
            
            //InternalSchema customSchema = new InternalSchema();
            //SchemaHelper.CreateSchema(ref customSchema, "property", "id", sizeof(CustomComponentSink.Properties), 0, sizeof(int));
            //CreateTable<CustomComponentSink.Properties>(customSchema, WorldComponents.Custom);

            InternalSchema spriteSchema = new InternalSchema();
            SchemaHelper.CreateSchema(ref spriteSchema, "sprite", "id", sizeof(Entity.InternalEntity) + sizeof(CustomComponentSink.Properties) + sizeof(ISprite.InternalSprite) + sizeof(ITransform.InternalTransform), 0, sizeof(int));
            CreateTable(spriteSchema);
            InitializeColumns<Entity.InternalEntity>(spriteSchema, WorldComponents.Sprite, 0);
            InitializeColumns<CustomComponentSink.Properties>(spriteSchema, WorldComponents.Sprite, sizeof(Entity.InternalEntity));
            InitializeColumns<ITransform.InternalTransform>(spriteSchema, WorldComponents.Sprite, sizeof(Entity.InternalEntity) + sizeof(CustomComponentSink.Properties));
            InitializeColumns<ISprite.InternalSprite>(spriteSchema, WorldComponents.Sprite, sizeof(Entity.InternalEntity) + sizeof(CustomComponentSink.Properties) + sizeof(ITransform.InternalTransform));
            //NativeMethods.AddComponentColumn(this.ptr, (int)WorldComponents.Sprite, "index", sizeof(int), sizeof(int));
            NativeMethods.AddComponentIndex(this.ptr, (int)WorldComponents.Sprite, 1, 1);

            InternalSchema transformSchema = new InternalSchema();
            SchemaHelper.CreateSchema(ref transformSchema, "transform", "id", sizeof(Entity.InternalEntity) + sizeof(CustomComponentSink.Properties) + sizeof(ITransform.InternalTransform), 0, sizeof(int));
            CreateTable(transformSchema);
            InitializeColumns<Entity.InternalEntity>(transformSchema, WorldComponents.Transform, 0);
            InitializeColumns<CustomComponentSink.Properties>(transformSchema, WorldComponents.Transform, sizeof(Entity.InternalEntity));
            InitializeColumns<ITransform.InternalTransform>(transformSchema, WorldComponents.Transform, sizeof(Entity.InternalEntity) + sizeof(CustomComponentSink.Properties));
            NativeMethods.AddComponentIndex(this.ptr, (int)WorldComponents.Transform, 1, 1);

            InternalSchema meshSchema = new InternalSchema();
            SchemaHelper.CreateSchema(ref meshSchema, "mesh", "id", sizeof(Entity.InternalEntity) + sizeof(CustomComponentSink.Properties) + sizeof(IMesh.InternalMesh) + sizeof(ITransform.InternalTransform), 0, sizeof(int));
            CreateTable(meshSchema);
            InitializeColumns<Entity.InternalEntity>(meshSchema, WorldComponents.Mesh, 0);
            InitializeColumns<CustomComponentSink.Properties>(meshSchema, WorldComponents.Mesh, sizeof(Entity.InternalEntity));
            InitializeColumns<ITransform.InternalTransform>(meshSchema, WorldComponents.Mesh, sizeof(Entity.InternalEntity) + sizeof(CustomComponentSink.Properties));
            InitializeColumns<IMesh.InternalMesh>(meshSchema, WorldComponents.Mesh, sizeof(Entity.InternalEntity) + sizeof(CustomComponentSink.Properties) + sizeof(ITransform.InternalTransform));
            NativeMethods.AddComponentIndex(this.ptr, (int)WorldComponents.Mesh, 1, 1);

            InternalSchema textSchema = new InternalSchema();
            SchemaHelper.CreateSchema(ref textSchema, "text", "id", sizeof(Entity.InternalEntity) + sizeof(CustomComponentSink.Properties) + sizeof(ITextSprite.InternalTextSprite) + sizeof(ITransform.InternalTransform), 0, sizeof(int));
            CreateTable(textSchema);
            InitializeColumns<Entity.InternalEntity>(textSchema, WorldComponents.Text, 0);
            InitializeColumns<CustomComponentSink.Properties>(textSchema, WorldComponents.Text, sizeof(Entity.InternalEntity));
            InitializeColumns<ITransform.InternalTransform>(textSchema, WorldComponents.Text, sizeof(Entity.InternalEntity) + sizeof(CustomComponentSink.Properties));
            InitializeColumns<ITextSprite.InternalTextSprite>(textSchema, WorldComponents.Text, sizeof(Entity.InternalEntity) + sizeof(CustomComponentSink.Properties) + sizeof(ITransform.InternalTransform));
            NativeMethods.AddComponentIndex(this.ptr, (int)WorldComponents.Text, 1, 1);

            this.binding = new WorldBindings(NativeMethods.Bind(handle), this.ptr);
        }

        public IEntity CreateEntity()
        { 
            return new Entity(this);
        }

        public void RemoveEntity(IEntity entity)
        {
            if (!this.disposed)
            {
                PerfStats.Record("FreeEntity");
                NativeMethods.FreeEntity(ptr, entity.ID);
                GC.SuppressFinalize(entity);
            }
        }

        public WorldBindings Binding
        {
            get { return this.binding; }
        }

        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("GUID", this.guid);
        }

        private World(SerializationInfo info, StreamingContext cxt)
        {
            this.guid = (Guid)info.GetValue("GUID", typeof(Guid));

            this.ptr = (IntPtr)((SerializationManager)cxt.Context).Unmap(this.guid);
            this.binding = new WorldBindings(NativeMethods.Bind(this.ptr), this.ptr);
        }

        public void Dispose()
        {
            this.disposed = true;
            NativeMethods.DestroyWorld(this.ptr);
        }
    }
}
