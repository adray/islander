﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace Islander.Managed
{
    [StructLayout(LayoutKind.Sequential)]
    internal unsafe struct InternalNavRegion
    {
        //public float* vertexData;
        //public IntPtr indexData;

        //public int vertexCount;
        //public int indexCount;

        public IntPtr polymesh;

        // Object transform.
        public float px, py, pz;
        public float rx, ry, rz;
        public float sx, sy, sz;
    }

    [StructLayout(LayoutKind.Sequential)]
    internal unsafe struct InternalNavMeshData
    {
        public float* vertexData;
        public int* indexData;
        public byte* data;

        public int vertexCount;
        public int indexCount;
        public int dataCount;
    };

    [StructLayout(LayoutKind.Sequential)]
    internal unsafe struct InternalNavMeshConfig
    {
        public float cs; // cell size
        public float ch; // cell height
        public float walkableSlopeAngle;
        public int walkableClimb;
        public int walkableHeight;
        public int walkableRadius;
        public int minRegionArea;
        public int mergeRegionArea;
        public int maxEdgeLen;
        public float maxSimplificationError;
        public int maxVertsPerPoly;
        public float detailSampleDist;
        public float detailSampleMaxError;
    }

    [StructLayout(LayoutKind.Sequential)]
    internal unsafe struct InternalNavPath
    {
        public float sx; public float sy; public float sz;
        public float ex; public float ey; public float ez;
        public float nx; public float ny; public float nz;
    };

    public class NavMeshConfig
    {
        public float cs; // cell size
        public float ch; // cell height
        public float walkableSlopeAngle;
        public int walkableClimb;
        public int walkableHeight;
        public int walkableRadius;
        public int minRegionArea;
        public int mergeRegionArea;
        public int maxEdgeLen;
        public float maxSimplificationError;
        public int maxVertsPerPoly;
        public float detailSampleDist;
        public float detailSampleMaxError;

        internal void ToInternalMeshConfig(ref InternalNavMeshConfig cfg)
        {
            cfg.ch = ch;
            cfg.cs = cs;
            cfg.walkableSlopeAngle = walkableSlopeAngle;
            cfg.walkableClimb = walkableClimb;
            cfg.walkableHeight = walkableHeight;
            cfg.walkableRadius = walkableRadius;
            cfg.minRegionArea = minRegionArea;
            cfg.maxEdgeLen = maxEdgeLen;
            cfg.maxSimplificationError = maxSimplificationError;
            cfg.maxVertsPerPoly = maxVertsPerPoly;
            cfg.detailSampleDist = detailSampleDist;
            cfg.detailSampleMaxError = detailSampleMaxError;
        }
    }

    public class NavMeshData
    {
        public List<float> VertexData { get; set; }
        public List<int> IndexData { get; set; }
        public byte[] NavData { get; set; }
    }

    public class NavigationSystem : IDisposable
    {
        private IntPtr nav;

        public NavigationSystem()
        {
            this.nav = NativeMethods.CreateNavigation();
        }

        public unsafe void Load(byte[] data)
        {
            byte* d = (byte*)Marshal.AllocHGlobal(data.Length);
            Marshal.Copy(data, 0, new IntPtr(d), data.Length);
            NativeMethods.NavLoad(this.nav, new IntPtr(d), data.Length);
        }

        public unsafe void AddRegion(IEntity entity, PolygonMesh polymesh/*, int vertexPositionDataOffset*/)
        {
            var t = entity.GetComponent<ITransform>();

            InternalNavRegion region = new InternalNavRegion()
            {
                px = t.Position.X,
                py = t.Position.Y,
                pz = t.Position.Z,
                sx = t.Scale.X,
                sy = t.Scale.Y,
                sz = t.Scale.Z,
                rx = t.Direction.X,
                ry = t.Direction.Y,
                rz = t.Direction.Z,
                polymesh = polymesh.ptr
            };
            NativeMethods.NavAddRegion(this.nav, new IntPtr(&region));
        }

        public NavMeshData GenerateNavMesh(NavMeshConfig cfg)
        {
            InternalNavMeshConfig internalCfg = new InternalNavMeshConfig();
            cfg.ToInternalMeshConfig(ref internalCfg);

            unsafe
            {
                NativeMethods.NavGenerate(this.nav, new IntPtr(&internalCfg));

                InternalNavMeshData meshData;

                NativeMethods.NavGetData(this.nav, new IntPtr(&meshData));

                NavMeshData userMeshData = new NavMeshData();

                userMeshData.VertexData = new List<float>();
                for (int i = 0; i < meshData.vertexCount; i++)
                {
                    userMeshData.VertexData.Add(*(meshData.vertexData + i));
                }

                userMeshData.IndexData = new List<int>();
                for (int i = 0; i < meshData.indexCount; i++)
                {
                    userMeshData.IndexData.Add(*(meshData.indexData + i));
                }

                userMeshData.NavData = new byte[meshData.dataCount];
                for (int i = 0; i < meshData.dataCount; i++)
                {
                    userMeshData.NavData[i] = (*(meshData.data + i));
                }

                return userMeshData;
            }
        }

        public void ClearRegions()
        {
            NativeMethods.NavClearRegions(this.nav);
        }

        public unsafe bool FindPath(float sx, float sy, float sz, float ex, float ey, float ez, out Vec3f pos)
        {
            InternalNavPath path = new InternalNavPath();
            path.ex = ex;
            path.ey = ey;
            path.ez = ez;
            path.sx = sx;
            path.sy = sy;
            path.sz = sz;

            bool result = NativeMethods.NavCalculatePath(this.nav, new IntPtr(&path)) == 1;

            pos = new Vec3f(path.nx, path.ny, path.nz);
            return result;
        }

        public void Dispose()
        {
            ClearRegions();
        }

        ~NavigationSystem()
        {
            ClearRegions();
        }
    }
}
