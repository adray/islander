﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace Islander.Managed
{
    public class AnimationSystem
    {
        private IntPtr system;

        public AnimationSystem()
        {
            this.system = NativeMethods.CreateAnimationSystem();
        }

        public AnimationController CreateController()
        {
            int id = NativeMethods.CreateAnimationController(this.system);
            return new AnimationController(id, this.system);
        }

        public void DestroyController(AnimationController controller)
        {
            NativeMethods.DestroyAnimationController(this.system, controller.ControllerId);
        }

        public void Update(float deltaTime)
        {
            NativeMethods.AnimationUpdate(this.system, deltaTime);
        }
    }

    public class AnimationController
    {
        private int controllerId;
        internal IntPtr system;

        public int ControllerId => controllerId;

        internal AnimationController(int id, IntPtr system)
        {
            this.controllerId = id;
            this.system = system;
        }

        public void PlayAnimation(int id, bool loop)
        {
            NativeMethods.PlayAnimation(this.system, controllerId, id, loop ? 1 : 0);
        }

        public void StopAnimation()
        {
            NativeMethods.PauseAnimation(system, controllerId);
        }

        public void ResetAnimation()
        {
            NativeMethods.ResetAnimation(system, controllerId);
        }
    }

    [Serializable]
    public class IMesh : IManagedComponent, WellDefinedComponent
    {
        private InternalMesh mesh = new InternalMesh();
        private IntPtr data;
        private IntPtr indexData;

        public IMesh()
            : base("Mesh")
        { }

        [Serializable]
        [StructLayout(LayoutKind.Sequential)]
        internal unsafe struct InternalMeshConfig
        {
            [NonSerialized] // TODO: serialize as flat info?
            public IntPtr Data;
            [NonSerialized] // TODO: serialize as flat info?
            public IntPtr IndexData;
            public int Count;
            public int IndexCount;
            public int Stride;
        }

        [Serializable]
        [StructLayout(LayoutKind.Sequential)]
        internal struct InternalMesh
        {
            public InternalMaterial Material;
            public InternalMeshConfig Config;
            [DefaultValue(-1)]
            [NonSerialized]
            public int MeshID;
            [DefaultValue(-1)]
            [NonSerialized]
            public int MeshIndexID;
            public int Dirty;
            public bool Wireframe;
            public bool DebugBones;
            public IntPtr polymesh;
            [DefaultValue(-1)]
            public int controllerId;
            public IntPtr animationSystem;
            [DefaultValue(-1)]
            public int parentEntity;
            [DefaultValue(-1)]
            public int parentBone;
        }

        private void Pull()
        {
            PerfStats.Record("GetComponent");
            unsafe
            {
                byte* mesh = stackalloc byte[this.Row.Entity.Size];
                NativeMethods.GetComponent(this.Row.World.Binding.world, (int)this.Row.Entity.ComponentType, Row.Entity.ID, new IntPtr(mesh));

                InternalMesh* m = (InternalMesh*)(mesh + sizeof(Entity.InternalEntity) + sizeof(CustomComponentSink.Properties) + sizeof(ITransform.InternalTransform));
                this.mesh = *m;
            }
        }

        public bool WireFrame
        {
            get
            {
                return this.mesh.Wireframe;
            }
            set
            {
                this.Pull();
                this.mesh.Wireframe = value;
                this.UpdateMesh();
            }
        }

        public bool DebugBones
        {
            get
            {
                return this.mesh.DebugBones;
            }
            set
            {
                this.Pull();
                this.mesh.DebugBones = value;
                this.UpdateMesh();
            }
        }

        public int Pass
        {
            get
            {
                return this.mesh.Material.pass;
            }
            set
            {
                this.Pull();
                this.mesh.Material.pass = value;
                this.UpdateMesh();
            }
        }

        public int PixelShader
        {
            get
            {
                return this.mesh.Material.pixelShader;
            }
            set
            {
                this.Pull();
                this.mesh.Material.pixelShader = (byte)value;
                UpdateMesh();
            }
        }

        public int VertexShader
        {
            get
            {
                return this.mesh.Material.vertexShader;
            }
            set
            {
                this.Pull();
                this.mesh.Material.vertexShader = (byte)value;
                UpdateMesh();
            }
        }

        public int Stride
        {
            get
            {
                return this.mesh.Config.Stride;
            }
        }

        internal IntPtr VertexData => this.mesh.Config.Data;

        internal IntPtr IndexData => this.mesh.Config.IndexData;

        public int VertexDataLength => this.mesh.Config.Count;

        public int IndexDataLength => this.mesh.Config.IndexCount;

        public void SetData(float[] data, int stride)
        {
            this.Pull();

            if (this.data != null)
            {
                Marshal.FreeHGlobal(this.data);
            }

            if (this.indexData != null)
            {
                Marshal.FreeHGlobal(this.indexData);
            }

            this.data = Marshal.AllocHGlobal(data.Length * Marshal.SizeOf(typeof(float)));
            mesh.Config.Count = data.Length;
            mesh.Config.Stride = stride;
            mesh.Config.IndexData = IntPtr.Zero;
            mesh.Config.IndexCount = 0;

            Marshal.Copy(data, 0, this.data, data.Length);

            this.mesh.Config.Data = this.data;
            this.mesh.Dirty = 1;
            this.mesh.polymesh = IntPtr.Zero;

            this.UpdateMesh();
        }

        public void SetData(float[] data, int stride, int[] indexData)
        {
            this.Pull();

            if (this.data != null)
            {
                Marshal.FreeHGlobal(this.data);
            }

            if (this.indexData != null)
            {
                Marshal.FreeHGlobal(this.indexData);
            }

            this.data = Marshal.AllocHGlobal(data.Length * Marshal.SizeOf(typeof(float)));
            mesh.Config.Count = data.Length;
            mesh.Config.Stride = stride;

            this.indexData = Marshal.AllocHGlobal(indexData.Length * Marshal.SizeOf(typeof(int)));
            mesh.Config.IndexCount = indexData.Length;

            Marshal.Copy(data, 0, this.data, data.Length);
            Marshal.Copy(indexData, 0, this.indexData, indexData.Length);

            this.mesh.Config.Data = this.data;
            this.mesh.Config.IndexData = this.indexData;
            this.mesh.Dirty = 1;
            this.mesh.polymesh = IntPtr.Zero;

            this.UpdateMesh();
        }

        public void SetData(PolygonMesh polyMesh)
        {
            this.Pull();

            if (this.data != null)
            {
                Marshal.FreeHGlobal(this.data);
            }

            if (this.indexData != null)
            {
                Marshal.FreeHGlobal(this.indexData);
            }

            mesh.Config.IndexData = IntPtr.Zero;
            mesh.Config.IndexCount = 0;
            mesh.Config.Data = IntPtr.Zero;
            mesh.Config.Count = 0;
            mesh.Config.Stride = 0;

            this.mesh.polymesh = polyMesh.ptr;

            this.UpdateMesh();
        }

        ~IMesh()
        {
            if (this.data != null)
            {
                Marshal.FreeHGlobal(this.data);
            }

            if (this.indexData != null)
            {
                Marshal.FreeHGlobal(this.indexData);
            }
        }

        public void SetMaterial(Material material)
        {
            this.Pull();
            this.mesh.Material = material.material;
            UpdateMesh();
        }

        void WellDefinedComponent.Update()
        {
            UpdateMesh();
        }

        unsafe private void UpdateMesh()
        {
            PerfStats.Record("UpdateComponent");
            fixed (InternalMesh* fix = &this.mesh)
            {
                NativeMethods.UpdateComponentPartial(this.Row.World.Binding.world, (int)this.Row.Entity.ComponentType, Row.Entity.ID, new IntPtr(fix),
                    sizeof(Entity.InternalEntity) + sizeof(CustomComponentSink.Properties) + sizeof(ITransform.InternalTransform), sizeof(InternalMesh));
            }
        }

        public unsafe void CreateNew()
        {
            mesh.MeshID = -1;
            mesh.MeshIndexID = -1;
            mesh.parentEntity = -1;
            mesh.parentBone = -1;
            
            PerfStats.Record("UpdateComponent");
            fixed (InternalMesh* fix = &this.mesh)
            {
                NativeMethods.UpdateComponentPartial(this.Row.World.Binding.world, (int)this.Row.Entity.ComponentType, Row.Entity.ID, new IntPtr(fix),
                    sizeof(Entity.InternalEntity) + sizeof(CustomComponentSink.Properties) + sizeof(ITransform.InternalTransform), sizeof(InternalMesh));
            }
        }

        public void SetAnimationController(AnimationController controller)
        {
            mesh.controllerId = controller.ControllerId;
            mesh.animationSystem = controller.system;
            UpdateMesh();
        }

        public void RemoveAnimationController()
        {
            mesh.controllerId = -1;
            mesh.animationSystem = IntPtr.Zero;
            UpdateMesh();
        }

        public void SetParent(IEntity entity, int bone)
        {
            this.mesh.parentBone = bone;
            this.mesh.parentEntity = entity != null ? entity.ID : -1;
            UpdateMesh();
        }
    }
}
