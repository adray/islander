﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Islander.Managed
{
    [AttributeUsage(AttributeTargets.Class|AttributeTargets.Struct)]
    public class SerializationNotRequiredAttribute : Attribute
    {
    }

    public class SerializationManager
    {
        private HashSet<World> worlds;
        private Dictionary<object, object> mapper;

        internal void Map(object k, object v)
        {
            mapper.Add(k, v);
        }

        internal object Unmap(object o)
        {
            object v = null;
            mapper.TryGetValue(o, out v);
            return v;
        }

        internal void Tag(World world)
        {
            this.worlds.Add(world);
        }

        public void Serialize(System.IO.Stream stream, object graph)
        {
            worlds = new HashSet<World>();

            using (var s = new System.IO.MemoryStream())
            {
                var b = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter(
                    new SS(),
                    new System.Runtime.Serialization.StreamingContext(System.Runtime.Serialization.StreamingContextStates.File, this));
                b.Serialize(s, graph);

                var w = new System.IO.BinaryWriter(stream);
                
                w.Write(this.worlds.Count);
                // serialize all the world objects in the graph
                foreach (var world in this.worlds)
                {
                    w.Write(world.guid.ToByteArray());
                    IntPtr data;
                    IntPtr count = IntPtr.Zero;
                    int len = 0;
                    unsafe
                    {
                        count = new IntPtr(&len);
                        data = Islander.Managed.NativeMethods.Serialize(world.
                            Binding.world, count);

                        w.Write(len);
                        byte[] bt = new byte[len];
                        System.Runtime.InteropServices.Marshal.Copy(data, bt, 0, len);
                        w.Write(bt);
                    }
                }

                w.Write(s.ToArray());                
            }
        }

        public object Deserialize(System.IO.Stream stream)
        {
            mapper = new Dictionary<object, object>();

            var br = new System.IO.BinaryReader(stream);
            int worldCount = br.ReadInt32();
            for (int i = 0; i < worldCount; i++)
            {
                var guid = new Guid(br.ReadBytes(16));
                int len = br.ReadInt32();
                var bt = br.ReadBytes(len);
                IntPtr world = Islander.Managed.NativeMethods.CreateWorld();

                // force creation of the tables
                var w = new World(world);

                unsafe {
                    fixed (byte* p = bt)
                    {
                        Islander.Managed.NativeMethods.Deserialize(world, new IntPtr(p), len);
                    }
                }

                this.Map(guid, world);
            }

            var b = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter(
                new SS(),
                new System.Runtime.Serialization.StreamingContext(System.Runtime.Serialization.StreamingContextStates.File, this));
            return b.Deserialize(stream);
        }
    }

    class AutogenSug :
    System.Runtime.Serialization.ISerializationSurrogate
    {
        // This class handles serializing compilier
        // generated classes e.g. anon methods and yield

        public void GetObjectData(object obj, System.Runtime.Serialization.SerializationInfo info, System.Runtime.Serialization.StreamingContext context)
        {
            var m = obj.GetType().GetFields(
                System.Reflection.BindingFlags.NonPublic |
                System.Reflection.BindingFlags.Public |
                System.Reflection.BindingFlags.Instance);
            for (int i = 0; i < m.Length; i++)
            {
                info.AddValue(m[i].Name,
                    m[i].GetValue(obj));
            }
        }

        public object SetObjectData(object obj, System.Runtime.Serialization.SerializationInfo info, System.Runtime.Serialization.StreamingContext context, System.Runtime.Serialization.ISurrogateSelector selector)
        {
            var m = obj.GetType().GetFields(
                System.Reflection.BindingFlags.NonPublic |
                System.Reflection.BindingFlags.Public |
                System.Reflection.BindingFlags.Instance);
            for (int i = 0; i < m.Length; i++)
            {
                m[i].SetValue(obj, info.GetValue(m[i].Name,
                    m[i].FieldType));
            }
            return obj;
        }
    }

    class WorldProxySerializer :
        System.Runtime.Serialization.ISerializationSurrogate
    {
        public void GetObjectData(object obj, SerializationInfo info, StreamingContext context)
        {
            var w = obj as World;
            w.GetObjectData(info, context);

            ((SerializationManager)context.Context).Tag((World)obj);
        }

        public object SetObjectData(object obj, SerializationInfo info, StreamingContext context, ISurrogateSelector selector)
        {
            var c = obj.GetType().GetConstructor(System.Reflection.BindingFlags.Instance |
                System.Reflection.BindingFlags.NonPublic, null, new Type[]
                {
                    typeof(SerializationInfo),
                    typeof(StreamingContext),
                }, null);
            c.Invoke(obj, new object[]{ info, context});

            return obj;
        }
    }

    class StopwatchProxy : 
        System.Runtime.Serialization.ISerializationSurrogate
    {
        public void GetObjectData(object obj, SerializationInfo info, StreamingContext context)
        {
            System.Diagnostics.Stopwatch w =
                obj as System.Diagnostics.Stopwatch;
            info.AddValue("TIME", w.ElapsedMilliseconds);
        }

        public object SetObjectData(object obj, SerializationInfo info, StreamingContext context, ISurrogateSelector selector)
        {
            // TODO: we do nothing with the time stored
            return new System.Diagnostics.Stopwatch();
        }
    }

    class DummyProxy :
        System.Runtime.Serialization.ISerializationSurrogate
    {
        public void GetObjectData(object obj, SerializationInfo info, StreamingContext context)
        {
            Logger.Log("Error: Attempting to serialize " + obj.GetType().Name + " not marked as serializable.", LoggingLevels.Managed);
        }

        public object SetObjectData(object obj, SerializationInfo info, StreamingContext context, ISurrogateSelector selector)
        {
            throw new NotImplementedException();
        }
    }

    class SS : System.Runtime.Serialization.ISurrogateSelector
    {
        private HashSet<Type> types
            = new HashSet<Type>();

        public void ChainSelector(System.Runtime.Serialization.ISurrogateSelector selector)
        {

        }

        public System.Runtime.Serialization.ISurrogateSelector GetNextSelector()
        {
            throw new NotImplementedException();
        }

        public System.Runtime.Serialization.ISerializationSurrogate GetSurrogate(Type type, System.Runtime.Serialization.StreamingContext context, out System.Runtime.Serialization.ISurrogateSelector selector)
        {
            selector = this;

            if (type.GetCustomAttributes(true)
                .Where(a => a is System.Runtime.CompilerServices.CompilerGeneratedAttribute).Count() > 0)
            {
                return new AutogenSug();
            }
            else if (type == typeof(World))
            {
                return new WorldProxySerializer();
            }
            else if (type == typeof(System.Diagnostics.Stopwatch))
            {
                return new StopwatchProxy();
            }
            else
            {
                if (types.Add(type) && 
                    type.GetCustomAttributes(true)
                    .Where(a => a is SerializationNotRequiredAttribute).Count() > 0)
                {
                    Logger.Log("Warning: Attempt to serialize " + type.Name + ", but is marked as not serializable.", 
                        LoggingLevels.Managed);
                }

                if (type.IsSerializable)
                {
                    return null;
                }
                else
                {
                    return new DummyProxy();
                }
            }
        }
    }
}
