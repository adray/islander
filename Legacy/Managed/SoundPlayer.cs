﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;

namespace Islander.Managed
{
    public interface ISoundPlayer : IDisposable
    {
        ITrackState PlaySound(ITrack track, int loopCount);
        IPlaylistStatus PlayList(IPlaylist list);
        ITrack LoadSound(string filename, int category);
        IPlaylist CreatePlaylist();
        int CreateCategory();
        void SetCategoryVolume(int category, float volume);
        void StopPlaylist(IPlaylistStatus status);
        void Update();
        SoundPlayerStats GetStats();
    }

    public interface ITrack
    {
        int Category { get; }
        int Handle { get; }
        string Filename { get; }
    }

    public interface IPlaylist
    {
        void AddTrack(ITrack track);
        void RemoveTrack(ITrack track);
        void SetTimeDelay(int minSeconds, int maxSeconds);
        ITrack SelectTrack();
        bool HasDelay { get; }
        int SelectDelaySeconds();
    }

    public enum TrackStatus
    {
        None,
        Queued,
        Playing,
        Completing,
        Completed,
        Aborted,
    }

    public interface ITrackState
    {
        ITrack Underlying { get; }
        TrackStatus Status { get; }
        float Volume { get; set; }
        IntPtr Handle { get; }
    }

    public interface IPlaylistStatus
    {
        IPlaylist Playlist { get; }
        ITrackState Current { get; }
    }

    internal sealed class TrackState : ITrackState
    {
        private IntPtr handle;
        private ITrack underlying;
        private IntPtr soundPlayer;
        private float volume;

        public TrackState(IntPtr handle, ITrack underlying, IntPtr soundPlayer)
        {
            this.handle = handle;
            this.underlying = underlying;
            this.soundPlayer = soundPlayer;
        }

        public TrackStatus Status
        {
            get
            {
                int state = NativeMethods.GetTokenState(this.soundPlayer, this.handle);
                return (TrackStatus)state;
            }
        }

        public float Volume
        {
            get
            {
                return this.volume;
            }
            set
            {
                this.volume = value;

                if (Status == TrackStatus.Playing ||
                    Status == TrackStatus.Queued)
                {
                    NativeMethods.SetSoundVolume(soundPlayer, handle, Volume);
                }

                if (this.VolumeChanged != null)
                {
                    this.VolumeChanged(this, EventArgs.Empty);
                }
            }
        }

        public event EventHandler VolumeChanged;

        public ITrack Underlying
        {
            get { return this.underlying; }
        }

        public IntPtr Handle
        {
            get { return this.handle; }
        }
    }

    internal sealed class Track : ITrack
    {
        private int handle;
        private int category;
        private string filename;

        public Track(int handle, int category, string filename)
        {
            this.handle = handle;
            this.category = category;
            this.filename = filename;
        }

        public int Category
        {
            get { return this.category; }
        }

        public int Handle
        {
            get { return this.handle; }
        }

        public string Filename
        {
            get { return this.filename; }
        }
    }

    internal sealed class Playlist : IPlaylist
    {
        private static Random rnd = new Random();
        private List<ITrack> tracks = new List<ITrack>();
        private int minSeconds;
        private int maxSeconds;

        public void AddTrack(ITrack track)
        {
            this.tracks.Add(track);
        }

        public void RemoveTrack(ITrack track)
        {
            this.tracks.Remove(track);
        }

        public ITrack SelectTrack()
        {
            return tracks[rnd.Next(tracks.Count)];
        }

        public void SetTimeDelay(int minSeconds, int maxSeconds)
        {
            if (maxSeconds < minSeconds) throw new InvalidOperationException("maxSeconds should be >= minSeconds");
            if (maxSeconds < 0) throw new ArgumentException("maxSeconds should be >= 0");
            if (minSeconds < 0) throw new ArgumentException("minSeconds should be >= 0");

            this.minSeconds = minSeconds;
            this.maxSeconds = maxSeconds;
        }

        public bool HasDelay
        {
            get
            {
                return this.maxSeconds > 0;
            }
        }

        public int SelectDelaySeconds()
        {
            return rnd.Next(this.minSeconds, this.maxSeconds);
        }
    }

    internal enum PlaylistState
    {
        Stopped,
        Stopping,
        Running,
        Restarting,
        Waiting
    }

    internal class PlaylistStatus : IPlaylistStatus
    {
        public IPlaylist Playlist { get; set; }
        public ITrackState Current { get; set; }
        public PlaylistState State { get; set; }
        public long ResumeTime { get; set; }
        
        public PlaylistStatus()
        {
            State = PlaylistState.Stopped;
        }
    }

    public class SoundPlayerStats
    {
        public int PlayListsRunning { get; private set; }
        public int PlayListsWaiting { get; private set; }

        public SoundPlayerStats(int running, int waiting)
        {
            this.PlayListsRunning = running;
            this.PlayListsWaiting = waiting;
        }
    }

    internal sealed class SoundPlayer : ISoundPlayer
    {
        private IntPtr player;
        private Dictionary<IntPtr, PlaylistStatus> playlists = new Dictionary<IntPtr, PlaylistStatus>();
        private List<PlaylistStatus> waiting = new List<PlaylistStatus>();

        private enum EventType
        {
            None,
            BufferStart,
            BufferEnd,
            LoopEnd,
            Error,
            StreamEnd
        }

        [StructLayout(LayoutKind.Sequential)]
        private struct SoundEvent
        {
            public IntPtr _token;
            public EventType _type;
        }

        internal SoundPlayer(IntPtr window)
        {
            this.player = NativeMethods.CreateSoundPlayer(window);
        }

        public SoundPlayerStats GetStats()
        {
            return new SoundPlayerStats(this.playlists.Count, this.waiting.Count);
        }

        public unsafe void Update()
        {
            SoundEvent ev;
            NativeMethods.ProcessSoundQueue(this.player, new IntPtr(&ev));
            while (ev._token != IntPtr.Zero)
            {
                switch (ev._type)
                {
                    case EventType.StreamEnd:
                        PlaylistStatus list = null;
                        if (this.playlists.TryGetValue(ev._token, out list))
                        {
                            this.playlists.Remove(ev._token);
                        }

                        if (list != null)
                        {
                            bool @continue = false;
                            NativeMethods.ReleaseToken(this.player, ev._token);
                            list.Current = null;
                            if (list.State == PlaylistState.Running)
                            {
                                list.State = PlaylistState.Restarting;
                                @continue = true;
                            }

                            if (@continue)
                            {
                                ContinuePlaylist(list);
                            }
                        }
                        else
                        {
                            NativeMethods.ReleaseToken(this.player, ev._token);
                        }
                        break;
                }

                NativeMethods.ProcessSoundQueue(this.player, new IntPtr(&ev));
            }

            ProcessWaitingList();
        }

        private void ProcessWaitingList()
        {
            int index = 0;
            while (index < this.waiting.Count)
            {
                var list = waiting[index];
                Debug.Assert(list.State == PlaylistState.Waiting);
                if (Environment.TickCount >= list.ResumeTime)
                {
                    waiting.RemoveAt(index);
                    ContinuePlaylist(list);
                }
                else
                {
                    index++;
                }
            }
        }

        ~SoundPlayer()
        {
            NativeMethods.CloseSoundPlayer(this.player);
        }

        public ITrackState PlaySound(ITrack track, int loopCount)
        {
            IntPtr ptr = NativeMethods.PlaySound(this.player, track.Handle, loopCount);

            if (ptr == IntPtr.Zero)
            {
                // bad things
                return null;
            }
            
            return new TrackState(ptr, track, this.player);
        }

        public void StopPlaylist(IPlaylistStatus status)
        {             
            var s = (PlaylistStatus)status;

            if (s.State == PlaylistState.Running)
            {
                NativeMethods.StopSound(this.player, status.Current.Handle);
            }
            else if (s.State == PlaylistState.Waiting)
            {
                this.waiting.Remove(s);
            }

            s.State = PlaylistState.Stopping;
        }

        private void ContinuePlaylist(PlaylistStatus playlist)
        {
            ITrackState status = null;
            if (playlist.State == PlaylistState.Restarting || playlist.State == PlaylistState.Stopped || playlist.State == PlaylistState.Waiting)
            {
                if (playlist.Playlist.HasDelay && playlist.State != PlaylistState.Waiting)
                {
                    playlist.State = PlaylistState.Waiting;
                    playlist.ResumeTime = Environment.TickCount + playlist.Playlist.SelectDelaySeconds() * 1000;
                    waiting.Add(playlist);
                }
                else
                {
                    playlist.State = PlaylistState.Running;
                    var track = playlist.Playlist.SelectTrack();
                    status = this.PlaySound(track, 0);
                    playlist.Current = status;
                    //((TrackState)status).VolumeChanged += status_VolumeChanged;
                }
            }

            if (status != null)
            {
                if (status.Status == TrackStatus.Aborted)
                {
                    // abort, some issue with it
                    playlist.Current = null;
                }
                else if (status.Status == TrackStatus.Completed)
                {
                    // that was fast?
                    playlist.Current = null;
                }
                else
                {
                    this.playlists.Add(status.Handle, playlist);
                }
            }
        }

        public IPlaylistStatus PlayList(IPlaylist list)
        {
            PlaylistStatus current = new PlaylistStatus()
                {
                    Playlist = list,
                };

            ContinuePlaylist(current);

            return current;
        }

        public int CreateCategory()
        {
            return NativeMethods.CreateSoundCategory(this.player);
        }

        public void SetCategoryVolume(int category, float volume)
        {
            NativeMethods.SetCategoryVolume(this.player, category, volume);
        }

        public IPlaylist CreatePlaylist()
        {
            return new Playlist();
        }

        public ITrack LoadSound(string filename, int category)
        {
            int handle = NativeMethods.LoadSound(this.player, filename, category);
            return new Track(handle, category, System.IO.Path.GetFileName(filename));
        }

        public void Dispose()
        {
            NativeMethods.CloseSoundPlayer(this.player);
            GC.SuppressFinalize(this);
        }
    }
}
