﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace Islander.Managed
{
    public enum MouseState
    {
        MOUSE_DOWN=0,
        MOUSE_UP=1,
        MOUSE_NONE=2,
    }

    public enum KeyboardKeys
    {
        KEY_A,
        KEY_B,
        KEY_C,
        KEY_D,
        KEY_E,
        KEY_F,
        KEY_G,
        KEY_H,
        KEY_I,
        KEY_J,
        KEY_K,
        KEY_L,
        KEY_M,
        KEY_N,
        KEY_O,
        KEY_P,
        KEY_Q,
        KEY_R,
        KEY_S,
        KEY_T,
        KEY_U,
        KEY_V,
        KEY_W,
        KEY_X,
        KEY_Y,
        KEY_Z,
        KEY_0,
        KEY_1,
        KEY_2,
        KEY_3,
        KEY_4,
        KEY_5,
        KEY_6,
        KEY_7,
        KEY_8,
        KEY_9,
        KEY_LEFT,
        KEY_RIGHT,
        KEY_UP,
        KEY_DOWN,
        KEY_SHIFT,
        KEY_ALT,
        KEY_ESCAPE,
        KEY_ENTER,
        KEY_BACKSPACE,
        KEY_CTRL,
        KEY_SPACE,
        KEY_DELETE,
        KEY_TAB,
        KEY_HOME,
        KEY_PAGE_UP,
        KEY_PAGE_DOWN,
        KEY_F1,
        KEY_F2,
        KEY_F3,
        KEY_F4,
        KEY_F5,
        KEY_F6,
        KEY_F7,
        KEY_F8,
        KEY_F9,
        KEY_F10,
        KEY_F11,
        KEY_F12,
        KEY_COUNT
    }
    
    public sealed class Window
    {
        private IntPtr window;
        private IntPtr cursor;

        public event EventHandler Resized;

        public Window()
        {
            this.window = NativeMethods.CreateWindow();
            this.cursor = NativeMethods.CreateCursor(window);
        }

        public unsafe void SetWindowText(string text)
        {
            var bytes = Encoding.UTF8.GetBytes(text);
            var data = Marshal.AllocHGlobal((bytes.Length + 1) * Marshal.SizeOf(typeof(byte)));
            Marshal.Copy(bytes, 0, data, bytes.Length);
            ((byte*)data)[bytes.Length] = 0;

            NativeMethods.SetWindowText(this.window, data);
            Marshal.FreeHGlobal(data);
        }

        public ISoundPlayer CreateSoundPlayer()
        {
            return new SoundPlayer(this.window);
        }

        public int Width
        {
            get
            {
                return NativeMethods.WindowWidth(this.window);
            }
        }

        public int Height
        {
            get
            {
                return NativeMethods.WindowHeight(this.window);
            }
        }

        public IDevice CreateDevice(FontDescriptor defaultFont)
        {
            return new Device(this.window, defaultFont, RendererType.OpenGL);
        }

        public IDevice CreateDevice(FontDescriptor defaultFont, RendererType preferredRenderer)
        {
            return new Device(this.window, defaultFont, preferredRenderer);
        }

        public IList<KeyboardKeys> GetKeyboardState()
        {
            var state = new List<KeyboardKeys>();

            var keys = (KeyboardKeys[]) Enum.GetValues(typeof(KeyboardKeys));
            foreach (KeyboardKeys key in keys)
            {
                if (IsKeyDown(key))
                {
                    state.Add(key);
                }
            }

            return state;
        }

        public bool IsKeyDown(KeyboardKeys key)
        {
            return NativeMethods.IsKeyDown(this.window, (byte)key) == 1;
        }

        public void SetKeyUp(KeyboardKeys key)
        {
            NativeMethods.SetKeyUp(this.window, (byte)key);
        }

        public MouseState GetLeftMouseState()
        {
            return (MouseState) NativeMethods.GetLeftMouseState(this.window);
        }

        public MouseState GetRightMouseState()
        {
            return (MouseState) NativeMethods.GetRightMouseState(this.window);
        }

        public int GetMouseWheelState()
        {
            return NativeMethods.GetMouseWheelState(this.window);
        }

        public bool ProcessMessageQueue()
        {
            return NativeMethods.ProcessMessageQueue(this.window) == 1;
        }

        public void SetWindowSize(int width, int height)
        {
            NativeMethods.SetWindowSize(this.window, width, height);

            if (this.Resized != null)
            {
                this.Resized(this, EventArgs.Empty);
            }
        }

        public int LoadCursor(string filename)
        {
            return NativeMethods.LoadCursor(this.cursor, filename);
        }

        public void SetCursor(int type)
        {
            NativeMethods.SetCursor(this.cursor, type);
        }

        public void ShowCursor()
        {
            NativeMethods.ShowCursor(this.cursor);
        }

        public void HideCursor()
        {
            NativeMethods.HideCursor(this.cursor);
        }

        public int MouseX
        {
            get
            {
                return NativeMethods.MouseX(this.window);
            }
        }

        public int MouseY
        {
            get
            {
                return NativeMethods.MouseY(this.window);
            }
        }

        public List<Display> FindDisplays()
        {
            List<Display> displays = new List<Display>();

            unsafe
            {
                InternalDisplays _displays = new InternalDisplays();

                NativeMethods.GetDisplays(this.window, new IntPtr(&_displays));

                for (int i = 0; i < _displays._count; i++)
                {
                    InternalDisplay* display = (InternalDisplay*)_displays.data + i;

                    byte[] bytes = new byte[128];
                    System.Runtime.InteropServices.Marshal.Copy(new IntPtr(display->_name), bytes, 0, bytes.Length);

                    displays.Add(new Display(display->_id, Encoding.UTF8.GetString(bytes, 0, display->_size)));
                }
            }

            return displays;
        }

        public void SetIcon(byte[] data, int width, int height)
        {
            var handle = System.Runtime.InteropServices.Marshal.AllocHGlobal(data.Length);
            System.Runtime.InteropServices.Marshal.Copy(data, 0, handle, data.Length);

            NativeMethods.SetWindowIcon(this.window, handle, width, height);

            System.Runtime.InteropServices.Marshal.FreeHGlobal(handle);
        }

        public void SetDisplay(Display display)
        {
            NativeMethods.SetDisplay(this.window, display.ID);
        }

        ~Window()
        {
             NativeMethods.DestroyCursor(this.cursor);
        }
    }
}
