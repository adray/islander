﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace Islander.Managed
{
    internal static class NativeMethods
    {
        [DllImport("Islander.Entity.dll", EntryPoint = "IslanderCreateWorld", CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr CreateWorld();

        [DllImport("Islander.Entity.dll", EntryPoint = "IslanderDestroyWorld", CallingConvention = CallingConvention.Cdecl)]
        public static extern void DestroyWorld(IntPtr world);

        [DllImport("Islander.Entity.dll", EntryPoint = "IslanderSerialize", CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr Serialize(IntPtr world, IntPtr count);

        [DllImport("Islander.Entity.dll", EntryPoint = "IslanderDeserialize", CallingConvention = CallingConvention.Cdecl)]
        public static extern void Deserialize(IntPtr world, IntPtr data, int count);

        [DllImport("Islander.Entity.dll", EntryPoint = "IslanderAddComponentTable", CallingConvention = CallingConvention.Cdecl)]
        public static extern int AddComponentTable(IntPtr ptr, IntPtr schema);

        [DllImport("Islander.Entity.dll", EntryPoint = "IslanderGetComponentTable", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Ansi)]
        public static extern int GetComponentTable(IntPtr ptr, IntPtr name);

        [DllImport("Islander.Entity.dll", EntryPoint = "IslanderCreateEntity", CallingConvention = CallingConvention.Cdecl)]
        public static extern int CreateEntity(IntPtr world);
                
        [DllImport("Islander.Entity.dll", EntryPoint = "IslanderFreeEntity", CallingConvention = CallingConvention.Cdecl)]
        public static extern void FreeEntity(IntPtr world, int entity);

        [DllImport("Islander.Entity.dll", EntryPoint = "IslanderSetDefault", CallingConvention = CallingConvention.Cdecl)]
        public static extern void SetDefault(IntPtr world, int component, int offset, IntPtr data, int width);

        [DllImport("Islander.Entity.dll", EntryPoint = "IslanderAddComponent", CallingConvention = CallingConvention.Cdecl)]
        public static extern void AddComponent(IntPtr world, int type, IntPtr ptr);

        [DllImport("Islander.Entity.dll", EntryPoint = "IslanderRemoveComponent", CallingConvention = CallingConvention.Cdecl)]
        public static extern void RemoveComponent(IntPtr world, int type, int id);

        [DllImport("Islander.Entity.dll", EntryPoint = "IslanderUpdateComponent", CallingConvention = CallingConvention.Cdecl)]
        public static extern void UpdateComponent(IntPtr world, int type, int id, IntPtr ptr);

        [DllImport("Islander.Entity.dll", EntryPoint = "IslanderUpdateComponentPartial", CallingConvention = CallingConvention.Cdecl)]
        public static extern void UpdateComponentPartial(IntPtr world, int type, int id, IntPtr ptr, int offset, int size);

        [DllImport("Islander.Entity.dll", EntryPoint = "IslanderAddComponentIndex", CallingConvention = CallingConvention.Cdecl)]
        public static extern void AddComponentIndex(IntPtr world, int component, int index_type, int key);

        [DllImport("Islander.Entity.dll", EntryPoint = "IslanderGetComponent", CallingConvention = CallingConvention.Cdecl)]
        public static extern void GetComponent(IntPtr world, int type, int id, IntPtr ptr);

        [DllImport("Islander.Entity.dll", EntryPoint = "IslanderGatherStats", CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr GatherStats(IntPtr world, IntPtr count);

        [DllImport("Islander.Entity.dll", EntryPoint = "IslanderGetStatBlock", CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr GetStatBlock(IntPtr stats, int index, IntPtr ptr);

        [DllImport("Islander.Entity.dll", EntryPoint = "IslanderFreeStats", CallingConvention = CallingConvention.Cdecl)]
        public static extern void FreeStats(IntPtr stats);

        [DllImport("Islander.Entity.dll", EntryPoint = "IslanderAddComponentHandler", CallingConvention = CallingConvention.Cdecl)]
        public static extern void AddComponentHandler(IntPtr world, int type, IntPtr ptr);

        [DllImport("Islander.Entity.dll", EntryPoint = "IslanderRegisterComponentProperty", CallingConvention = CallingConvention.Cdecl, CharSet=CharSet.Ansi)]
        public static extern int RegisterComponentProperty(string name);

        [DllImport("Islander.Entity.dll", EntryPoint = "IslanderGetComponentProperty", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Ansi)]
        public static extern int GetComponentProperty(string name);

        [DllImport("Islander.Entity.dll", EntryPoint = "IslanderAddComponentColumn", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Ansi)]
        public static extern int AddComponentColumn(IntPtr world, int component, string name, int offset, int width);

        [DllImport("Islander.Entity.dll", EntryPoint = "IslanderAllocate", CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr Allocate(int numBytes);

        [DllImport("Islander.Entity.dll", EntryPoint = "IslanderFree", CallingConvention = CallingConvention.Cdecl)]
        public static extern void Free(IntPtr ptr);

        [DllImport("Islander.Renderer.dll", EntryPoint = "IslanderCreateWindow", CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr CreateWindow();

        [DllImport("Islander.Renderer.dll", EntryPoint = "IslanderSetWindowText", CallingConvention = CallingConvention.Cdecl)]
        public static extern void SetWindowText(IntPtr window, IntPtr text);

        [DllImport("Islander.Renderer.dll", EntryPoint = "IslanderPumpWindow", CallingConvention = CallingConvention.Cdecl)]
        public static extern int ProcessMessageQueue(IntPtr window);

        [DllImport("Islander.Renderer.dll", EntryPoint = "IslanderIsKeyDown", CallingConvention = CallingConvention.Cdecl)]
        public static extern int IsKeyDown(IntPtr window, byte key);

        [DllImport("Islander.Renderer.dll", EntryPoint = "IslanderSetKeyUp", CallingConvention = CallingConvention.Cdecl)]
        public static extern int SetKeyUp(IntPtr window, byte key);

        [DllImport("Islander.Renderer.dll", EntryPoint = "IslanderGetLeftMouseState", CallingConvention = CallingConvention.Cdecl)]
        public static extern int GetLeftMouseState(IntPtr window);

        [DllImport("Islander.Renderer.dll", EntryPoint = "IslanderGetRightMouseState", CallingConvention = CallingConvention.Cdecl)]
        public static extern int GetRightMouseState(IntPtr window);

        [DllImport("Islander.Renderer.dll", EntryPoint = "IslanderGetMouseWheelState", CallingConvention = CallingConvention.Cdecl)]
	    public static extern int GetMouseWheelState(IntPtr window);

        [DllImport("Islander.Renderer.dll", EntryPoint = "IslanderCreateSoundPlayer", CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr CreateSoundPlayer(IntPtr window);

        [DllImport("Islander.Renderer.dll", EntryPoint = "IslanderPlaySound", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Ansi)]
        public static extern IntPtr PlaySound(IntPtr player, int handle, int loopCount);

        [DllImport("Islander.Renderer.dll", EntryPoint = "IslanderProcessSoundEventQueue", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Ansi)]
        public static extern void ProcessSoundQueue(IntPtr player, IntPtr ptr);

        [DllImport("Islander.Renderer.dll", EntryPoint = "IslanderCloseSoundPlayer", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Ansi)]
        public static extern void CloseSoundPlayer(IntPtr player);

        [DllImport("Islander.Renderer.dll", EntryPoint = "IslanderLoadSound", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Ansi)]
        public static extern int LoadSound(IntPtr player, string filename, int category);

        [DllImport("Islander.Renderer.dll", EntryPoint = "IslanderStopSound", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Ansi)]
        public static extern int StopSound(IntPtr player, IntPtr sound);

        [DllImport("Islander.Renderer.dll", EntryPoint = "IslanderReleaseToken", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Ansi)]
        public static extern int ReleaseToken(IntPtr player, IntPtr sound);

        [DllImport("Islander.Renderer.dll", EntryPoint = "IslanderCreateSoundCategory", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Ansi)]
        public static extern int CreateSoundCategory(IntPtr player);

        [DllImport("Islander.Renderer.dll", EntryPoint = "IslanderIsSoundPlayerInitialized", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Ansi)]
        public static extern int IsSoundPlayerInitialized(IntPtr player);

        [DllImport("Islander.Renderer.dll", EntryPoint = "IslanderSetCategoryVolume", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Ansi)]
        public static extern void SetCategoryVolume(IntPtr player, int category, float vol);
        
        [DllImport("Islander.Renderer.dll", EntryPoint = "IslanderSetSoundVolume", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Ansi)]
        public static extern void SetSoundVolume(IntPtr player, IntPtr token, float vol);

        [DllImport("Islander.Renderer.dll", EntryPoint = "IslanderGetTokenState", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Ansi)]
        public static extern int GetTokenState(IntPtr player, IntPtr token);

        [DllImport("Islander.Renderer.dll", EntryPoint = "IslanderCreateDevice", CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr CreateDevice();

        [DllImport("Islander.Renderer.dll", EntryPoint = "IslanderInitializeDevice", CallingConvention = CallingConvention.Cdecl, CharSet=CharSet.Ansi)]
        public static extern void InitializeDevice(IntPtr device, IntPtr window, InternalFont font);

        [DllImport("Islander.Renderer.dll", EntryPoint = "IslanderGetCurrentRenderer", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Ansi)]
        public static extern int GetCurrentRenderer(IntPtr device);

        [DllImport("Islander.Renderer.dll", EntryPoint = "IslanderSetPreferredRenderer", CallingConvention = CallingConvention.Cdecl)]
        public static extern void SetPreferredRenderer(IntPtr device, int renderer);

        [DllImport("Islander.Renderer.dll", EntryPoint = "IslanderLoadFont", CallingConvention = CallingConvention.Cdecl)]
        public static extern int LoadFont(IntPtr device, InternalFont font);

        [DllImport("Islander.Renderer.dll", EntryPoint = "IslanderCreateBindings", CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr Bind(IntPtr world);

        [DllImport("Islander.Renderer.dll", EntryPoint = "IslanderRenderScene", CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr RenderScene(IntPtr device, IntPtr world, IntPtr camera, IntPtr binding);
        
        [DllImport("Islander.Renderer.dll", EntryPoint = "IslanderRenderScene3D", CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr RenderScene3D(IntPtr device, IntPtr world, IntPtr camera, IntPtr binding);

        [DllImport("Islander.Renderer.dll", EntryPoint = "IslanderCreateCamera", CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr CreateCamera();

        [DllImport("Islander.Renderer.dll", EntryPoint = "IslanderSetCameraTranslation", CallingConvention = CallingConvention.Cdecl)]
        public static extern void SetCameraTranslation(IntPtr camera, float x, float y, float z);

        [DllImport("Islander.Renderer.dll", EntryPoint = "IslanderCreateCamera3D", CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr CreateCamera3D();

        [DllImport("Islander.Renderer.dll", EntryPoint = "IslanderSetCamera3DTranslation", CallingConvention = CallingConvention.Cdecl)]
        public static extern void SetCamera3DTranslation(IntPtr camera, float x, float y, float z);

        [DllImport("Islander.Renderer.dll", EntryPoint = "IslanderSetCamera3DProjection", CallingConvention = CallingConvention.Cdecl)]
        public static extern void SetCamera3DProjection(IntPtr camera, float nearZ, float farZ, float fovY, float aspect);

        [DllImport("Islander.Renderer.dll", EntryPoint = "IslanderSetCamera3DLookAt", CallingConvention = CallingConvention.Cdecl)]
        public static extern void SetCamera3DLookAt(IntPtr camera, float x, float y, float z);

        [DllImport("Islander.Renderer.dll", EntryPoint = "IslanderMeasureText", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Ansi)]
        public static extern void MeasureText(IntPtr device, IntPtr measureText);

        [DllImport("Islander.Renderer.dll", EntryPoint = "IslanderCreateRenderTarget", CallingConvention = CallingConvention.Cdecl)]
        public static extern void CreateRenderTarget(IntPtr device, int width, int height, IntPtr renderTarget);

        [DllImport("Islander.Renderer.dll", EntryPoint = "IslanderGetBytesFromTexture", CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr GetBytesFromTexure(IntPtr device, int texture, IntPtr width, IntPtr height, IntPtr size);

        [DllImport("Islander.Renderer.dll", EntryPoint = "IslanderFreeTextureData", CallingConvention = CallingConvention.Cdecl)]
        public static extern void FreeTextureData(IntPtr data);

        [DllImport("Islander.Renderer.dll", EntryPoint = "IslanderCreateTextureFromBytes", CallingConvention = CallingConvention.Cdecl)]
        public static extern int CreateTexture(IntPtr device, IntPtr data, int width, int height, int size);

        [DllImport("Islander.Renderer.dll", EntryPoint = "IslanderWindowWidth", CallingConvention = CallingConvention.Cdecl)]
        public static extern int WindowWidth(IntPtr window);

        [DllImport("Islander.Renderer.dll", EntryPoint = "IslanderWindowHeight", CallingConvention = CallingConvention.Cdecl)]
        public static extern int WindowHeight(IntPtr window);

        [DllImport("Islander.Renderer.dll", EntryPoint = "IslanderSetWindowSize", CallingConvention = CallingConvention.Cdecl)]
        public static extern void SetWindowSize(IntPtr window, int width, int height);

        [DllImport("Islander.Renderer.dll", EntryPoint = "IslanderMouseX", CallingConvention = CallingConvention.Cdecl)]
        public static extern int MouseX(IntPtr window);

        [DllImport("Islander.Renderer.dll", EntryPoint = "IslanderMouseY", CallingConvention = CallingConvention.Cdecl)]
        public static extern int MouseY(IntPtr window);
        
        [DllImport("Islander.Renderer.dll", EntryPoint = "IslanderSetPassList", CallingConvention = CallingConvention.Cdecl)]
        public static extern void SetPassList(IntPtr device, IntPtr pass);

        [DllImport("Islander.Renderer.dll", EntryPoint = "IslanderCreatePassList", CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr CreatePassList();

        [DllImport("Islander.Renderer.dll", EntryPoint = "IslanderAppendPassList", CallingConvention = CallingConvention.Cdecl)]
        public static extern void AppendPassList(IntPtr list, InternalPassConfig config);

        [DllImport("Islander.Renderer.dll", EntryPoint = "IslanderLoadPixelShader", CallingConvention = CallingConvention.Cdecl, CharSet=CharSet.Ansi)]
        public static extern int LoadPixelShader(IntPtr device, string filename, string method);

        [DllImport("Islander.Renderer.dll", EntryPoint = "IslanderLoadVertexShader", CallingConvention = CallingConvention.Cdecl, CharSet=CharSet.Ansi)]
        public static extern int LoadVertexShader(IntPtr device, string filename, string method, InternalSemantic[] semantics, int count);

        [DllImport("Islander.Renderer.dll", EntryPoint = "IslanderPresent", CallingConvention = CallingConvention.Cdecl)]
        public static extern void Present(IntPtr device);

        [DllImport("Islander.Renderer.dll", EntryPoint = "IslanderCreateCursor", CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr CreateCursor(IntPtr window);

        [DllImport("Islander.Renderer.dll", EntryPoint = "IslanderLoadCursor", CallingConvention = CallingConvention.Cdecl, CharSet=CharSet.Ansi)]
        public static extern int LoadCursor(IntPtr cursor, string filename);

        [DllImport("Islander.Renderer.dll", EntryPoint = "IslanderShowCursor", CallingConvention = CallingConvention.Cdecl)]
        public static extern void ShowCursor(IntPtr cursor);

        [DllImport("Islander.Renderer.dll", EntryPoint = "IslanderHideCursor", CallingConvention = CallingConvention.Cdecl)]
        public static extern void HideCursor(IntPtr cursor);

        [DllImport("Islander.Renderer.dll", EntryPoint = "IslanderDestroyCursor", CallingConvention = CallingConvention.Cdecl)]
        public static extern void DestroyCursor(IntPtr cursor);
        
        [DllImport("Islander.Renderer.dll", EntryPoint = "IslanderSetCursor", CallingConvention = CallingConvention.Cdecl)]
        public static extern void SetCursor(IntPtr cursor, int type);

        [DllImport("Islander.Renderer.dll", EntryPoint = "IslanderLog", CallingConvention = CallingConvention.Cdecl, CharSet=CharSet.Ansi)]
        public static extern void Log(string msg, int levels);

        [DllImport("Islander.Renderer.dll", EntryPoint = "IslanderSetLogLevels", CallingConvention = CallingConvention.Cdecl)]
        public static extern void SetLogLevels(int levels);

        [DllImport("Islander.Renderer.dll", EntryPoint = "IslanderCreateFileLibrary", CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr CreateFileLibrary();

        [DllImport("Islander.Renderer.dll", EntryPoint = "IslanderAddFile", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Ansi)]
        public static extern int AddFile(IntPtr lib, string path);

        [DllImport("Islander.Renderer.dll", EntryPoint = "IslanderAddCompressedFile", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Ansi)]
        public static extern int AddCompressedFile(IntPtr lib, string path);

        [DllImport("Islander.Renderer.dll", EntryPoint = "IslanderAddDirectory", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Ansi)]
        public static extern int AddDirectory(IntPtr lib, string dir);

        [DllImport("Islander.Renderer.dll", EntryPoint = "IslanderGetFile", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Ansi)]
        public static extern IntPtr GetFile(IntPtr lib, string name);

        [DllImport("Islander.Renderer.dll", EntryPoint = "IslanderCreateResource", CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr CreateResource(IntPtr device, IntPtr img, IntPtr def);

        [DllImport("Islander.Renderer.dll", EntryPoint = "IslanderCreateResourceFromTexture", CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr CreateResourceFromTexture(IntPtr device, IntPtr res);

        [DllImport("Islander.Renderer.dll", EntryPoint = "IslanderCreateResourceFromMesh", CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr CreateResourceFromMesh(IntPtr device, IntPtr res);

        [DllImport("Islander.Renderer.dll", EntryPoint = "IslanderLoadResource", CallingConvention = CallingConvention.Cdecl)]
        public static extern void LoadResource(IntPtr device, IntPtr resource);

        [DllImport("Islander.Renderer.dll", EntryPoint = "IslanderCreateReferencedResources", CallingConvention = CallingConvention.Cdecl)]
        public static extern void CreateReferencedResources(IntPtr device, IntPtr resource, IntPtr fileLib);

        [DllImport("Islander.Renderer.dll", EntryPoint = "IslanderLoadReferencedResources", CallingConvention = CallingConvention.Cdecl)]
        public static extern void LoadReferencedResources(IntPtr device, IntPtr resource);

        [DllImport("Islander.Renderer.dll", EntryPoint = "IslanderUnloadResource", CallingConvention = CallingConvention.Cdecl)]
        public static extern void UnloadResource(IntPtr device, IntPtr resource);

        [DllImport("Islander.Renderer.dll", EntryPoint = "IslanderLoadResourceAsync", CallingConvention = CallingConvention.Cdecl)]
        public static extern void LoadResourceAsync(IntPtr device, IntPtr resource);

        [DllImport("Islander.Renderer.dll", EntryPoint = "IslanderGetPolyMeshDataFromResource", CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr GetPolyMeshDataFromResource(IntPtr res);

        [DllImport("Islander.Renderer.dll", EntryPoint = "IslanderFindMaterialTexture", CallingConvention = CallingConvention.Cdecl, CharSet=CharSet.Ansi)]
        public static extern void FindMaterialTexture(IntPtr device, string name, IntPtr texture);

        [DllImport("Islander.Renderer.dll", EntryPoint = "IslanderFindMaterialSubTexture", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Ansi)]
        public static extern IntPtr FindMaterialSubTexture(IntPtr device, string name, IntPtr count);

        [DllImport("Islander.Renderer.dll", EntryPoint = "IslanderGenerateSubTextures", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Ansi)]
        public static extern void GenerateSubTextures(IntPtr device, string name, int type, IntPtr data);

        [DllImport("Islander.Renderer.dll", EntryPoint = "IslanderGetTextureSize", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Ansi)]
        public static extern int GetTextureSize(IntPtr device, string name, IntPtr width, IntPtr height);

        [DllImport("Islander.Renderer.dll", EntryPoint = "IslanderGetTextureList", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Ansi)]
        public static extern int GetTextureList(IntPtr device, IntPtr textureList);

        [DllImport("Islander.Renderer.dll", EntryPoint = "IslanderSetWindowIcon", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Ansi)]
        public static extern int SetWindowIcon(IntPtr window, IntPtr iconBytes, int width, int height);

        [DllImport("Islander.Renderer.dll", EntryPoint = "IslanderGetDisplays", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Ansi)]
        public static extern void GetDisplays(IntPtr window, IntPtr displays);

        [DllImport("Islander.Renderer.dll", EntryPoint = "IslanderSetDisplay", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Ansi)]
        public static extern void SetDisplay(IntPtr window, int display);

        [DllImport("Islander.Renderer.dll", EntryPoint = "IslanderGetResolutions", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Ansi)]
        public static extern void GetResolutions(IntPtr device, IntPtr resolutions);

        [DllImport("Islander.Renderer.dll", EntryPoint = "IslanderResizeBackBuffer", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Ansi)]
        public static extern void ResizeBackBuffer(IntPtr device);

        [DllImport("Islander.Renderer.dll", EntryPoint = "IslanderResizeRenderTarget", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Ansi)]
        public static extern void ResizeRenderTarget(IntPtr device, InternalRenderTarget renderTarget, int width, int height);

        [DllImport("Islander.Renderer.dll", EntryPoint = "IslanderCreateCollisionSystem", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Ansi)]
        public static extern IntPtr CreateCollisionSystem();

        [DllImport("Islander.Renderer.dll", EntryPoint = "IslanderAddStaticMesh", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Ansi)]
        public static extern IntPtr AddStaticMesh(IntPtr system, IntPtr mesh);

        [DllImport("Islander.Renderer.dll", EntryPoint = "IslanderAddDynamicMesh", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Ansi)]
        public static extern IntPtr AddDynamicMesh(IntPtr system, IntPtr mesh);

        [DllImport("Islander.Renderer.dll", EntryPoint = "IslanderUpdateDynamicMesh", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Ansi)]
        public static extern void UpdateDynamicMesh(IntPtr mesh, IntPtr info);

        [DllImport("Islander.Renderer.dll", EntryPoint = "IslanderRemoveStaticMesh", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Ansi)]
        public static extern void RemoveStaticMesh(IntPtr system, IntPtr mesh);

        [DllImport("Islander.Renderer.dll", EntryPoint = "IslanderRemoveDynamicMesh", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Ansi)]
        public static extern void RemoveDynamicMesh(IntPtr system, IntPtr mesh);

        [DllImport("Islander.Renderer.dll", EntryPoint = "IslanderRunCollision", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Ansi)]
        public static extern void RunCollision(IntPtr system, float delta);

        [DllImport("Islander.Renderer.dll", EntryPoint = "IslanderGetCollisionInfo", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Ansi)]
        public static extern void GetCollisionInfo(IntPtr mesh, IntPtr data);

        [DllImport("Islander.Renderer.dll", EntryPoint = "IslanderRayCast", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Ansi)]
        public static extern void RayCast(IntPtr system, IntPtr camera, float px, float py, float pz, float dx, float dy, float dz, IntPtr info);

        [DllImport("Islander.Renderer.dll", EntryPoint = "IslanderTransformRay", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Ansi)]
        public static extern void TransformRay(IntPtr camera, IntPtr px, IntPtr py, IntPtr pz, IntPtr dx, IntPtr dy, IntPtr dz);

        [DllImport("Islander.Renderer.dll", EntryPoint = "IslanderCreateNavigation", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Ansi)]
        public static extern IntPtr CreateNavigation();

        [DllImport("Islander.Renderer.dll", EntryPoint = "IslanderNavAddRegion", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Ansi)]
        public static extern void NavAddRegion(IntPtr nav, IntPtr region);

        [DllImport("Islander.Renderer.dll", EntryPoint = "IslanderNavGenerate", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Ansi)]
        public static extern void NavGenerate(IntPtr nav, IntPtr cfg);

        [DllImport("Islander.Renderer.dll", EntryPoint = "IslanderNavClearRegions", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Ansi)]
        public static extern void NavClearRegions(IntPtr nav);

        [DllImport("Islander.Renderer.dll", EntryPoint = "IslanderNavGetData", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Ansi)]
        public static extern void NavGetData(IntPtr nav, IntPtr data);

        [DllImport("Islander.Renderer.dll", EntryPoint = "IslanderNavLoad", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Ansi)]
        public static extern void NavLoad(IntPtr nav, IntPtr data, int count);

        [DllImport("Islander.Renderer.dll", EntryPoint = "IslanderNavCalculatePath", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Ansi)]
        public static extern int NavCalculatePath(IntPtr nav, IntPtr path);

        [DllImport("Islander.Renderer.dll", EntryPoint = "IslanderCreatePolyLibrary", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Ansi)]
        public static extern IntPtr CreatePolyLibrary();

        [DllImport("Islander.Renderer.dll", EntryPoint = "IslanderAddPolyMeshData", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Ansi)]
        public static extern IntPtr AddPolyMeshData(IntPtr lib, IntPtr vertexData, IntPtr indexData, int numVerts, int numIndicies, int stride, int createFlags);

        [DllImport("Islander.Renderer.dll", EntryPoint = "IslanderLoadEngineMesh", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Ansi)]
        public static extern IntPtr LoadEngineMesh(IntPtr lib, IntPtr data, int count);

        [DllImport("Islander.Renderer.dll", EntryPoint = "IslanderGetPolyMeshData", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Ansi)]
        public static extern void GetPolyMeshData(IntPtr poly, IntPtr data);

        [DllImport("Islander.Renderer.dll", EntryPoint = "IslanderSetDebugBoneMesh", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Ansi)]
        public static extern void SetDebugBoneMesh(IntPtr device, IntPtr poly, int vertexShader, int pixelShader);

        [DllImport("Islander.Renderer.dll", EntryPoint = "IslanderCreateAnimationSystem", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Ansi)]
        public static extern IntPtr CreateAnimationSystem();

        [DllImport("Islander.Renderer.dll", EntryPoint = "IslanderCreateAnimationController", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Ansi)]
        public static extern int CreateAnimationController(IntPtr system);

        [DllImport("Islander.Renderer.dll", EntryPoint = "IslanderDestroyAnimationController", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Ansi)]
        public static extern void DestroyAnimationController(IntPtr system, int controllerId);

        [DllImport("Islander.Renderer.dll", EntryPoint = "IslanderPlayAnimation", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Ansi)]
        public static extern void PlayAnimation(IntPtr system, int controllerId, int id, int loop);

        [DllImport("Islander.Renderer.dll", EntryPoint = "IslanderPauseAnimation", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Ansi)]
        public static extern void PauseAnimation(IntPtr system, int controllerId);

        [DllImport("Islander.Renderer.dll", EntryPoint = "IslanderAnimationUpdate", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Ansi)]
        public static extern void AnimationUpdate(IntPtr system, float deltaTime);

        [DllImport("Islander.Renderer.dll", EntryPoint = "IslanderResetAnimation", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Ansi)]
        public static extern void ResetAnimation(IntPtr system, int controllerId);

        [DllImport("Islander.Renderer.dll", EntryPoint = "IslanderGetAnimationId", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Ansi)]
        public static extern int GetAnimationId(IntPtr poly, IntPtr name);

        [DllImport("Islander.Renderer.dll", EntryPoint = "IslanderGetAnimationId", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Ansi)]
        public static extern float GetAnimationLength(IntPtr poly, int id);

        [DllImport("Islander.Renderer.dll", EntryPoint = "IslanderGetAnimationCount", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Ansi)]
        public static extern int GetAnimationCount(IntPtr poly);

        [DllImport("Islander.Renderer.dll", EntryPoint = "IslanderGetAnimationName", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Ansi)]
        public static extern int GetAnimationName(IntPtr poly, int id, IntPtr str);

        [DllImport("Islander.Renderer.dll", EntryPoint = "IslanderCreateGamePadDevice", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Ansi)]
        public static extern IntPtr CreateGamePadDevice();

        [DllImport("Islander.Renderer.dll", EntryPoint = "IslanderDestroyGamePadDevice", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Ansi)]
        public static extern void DestroyGamePadDevice(IntPtr device);

        [DllImport("Islander.Renderer.dll", EntryPoint = "IslanderIsGamePadSupported", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Ansi)]
        public static extern int IsGamePadSupported();

        [DllImport("Islander.Renderer.dll", EntryPoint = "IslanderGamePadDetected", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Ansi)]
        public static extern int GamePadDetected(IntPtr device);

        [DllImport("Islander.Renderer.dll", EntryPoint = "IslanderUpdateGamePadDevice", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Ansi)]
        public static extern void UpdateGamePadDevice(IntPtr device);

        [DllImport("Islander.Renderer.dll", EntryPoint = "IslanderGetGamepadState", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Ansi)]
        public static extern IntPtr GetGamepadState(IntPtr device, int gamepad);

        [DllImport("Islander.Renderer.dll", EntryPoint = "IslanderGamePadRumble", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Ansi)]
        public static extern void IslanderGamePadRumble(IntPtr device, int gamepad, float right, float left);

        [DllImport("Islander.Renderer.dll", EntryPoint = "IslanderCreateTransformationMatrix", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Ansi)]
        public static extern IntPtr CreateTransformationMatrix(float px, float py, float pz, float rx, float ry, float rz, float sx, float sy, float sz);
        
        [DllImport("Islander.Renderer.dll", EntryPoint = "IslanderCreateRotationMatrix", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Ansi)]
        public static extern IntPtr CreateRotationMatrix(float rx, float ry, float rz);

        [DllImport("Islander.Renderer.dll", EntryPoint = "IslanderTransformVector", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Ansi)]
        public static extern void TransformVector(IntPtr matrix, IntPtr px, IntPtr py, IntPtr pz);

        [DllImport("Islander.Renderer.dll", EntryPoint = "IslanderDestroyMatrix", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Ansi)]
        public static extern void DestroyMatrix(IntPtr matrix);
    }

    internal static class IslanderMarshal
    {
        public static IntPtr GetUTF8(string value)
        {
            var array = System.Text.Encoding.UTF8.GetBytes(value);

            var copy = new byte[array.Length + 1];
            Array.Copy(array, copy, array.Length);

            int size = copy.Length;
            var data = NativeMethods.Allocate(size);
            Marshal.Copy(copy, 0, data, size);

            return data;
        }
    }

    [StructLayout(LayoutKind.Sequential)]
    internal unsafe struct InternalResolution
    {
        public const int SIZE = 4 * 3;
        public int id;
        public int width;
        public int height;
    }

    [StructLayout(LayoutKind.Sequential)]
    internal unsafe struct InternalResolutions
    {
        public int count;
        public fixed byte data[64 * InternalResolution.SIZE];
    }

    [StructLayout(LayoutKind.Sequential)]
    internal unsafe struct InternalDisplay
    {
        public const int SIZE = 128 + 8;
        public int _id;
        public fixed byte _name[128];
        public int _size;
    };

    [StructLayout(LayoutKind.Sequential)]
    internal unsafe struct InternalDisplays
    {
        public int _count;
        public fixed byte data[8 * InternalDisplay.SIZE];
    };

    [StructLayout(LayoutKind.Sequential)]
    internal struct InternalTextureListData
    {
        public const int SIZE = 3 * 4;
        public int id;
        public int width;
        public int height;
    }

    [StructLayout(LayoutKind.Sequential)]
    internal unsafe struct InternalTextureList
    {
        public int count;
        public fixed byte data[128 * InternalTextureListData.SIZE];
    }
    
    [Serializable]
    [StructLayout(LayoutKind.Sequential)]
    internal struct InternalMeasureText
    {
        public float _width;
        public float _height;
        public int _wrap;
        public int _size;
        public IntPtr _name;
        public IntPtr _text;
    }

    [Serializable]
    [StructLayout(LayoutKind.Sequential)]
    internal struct InternalFont
    {
        public IntPtr _name;
        public IntPtr _file;
        public int _img;
    }

    [Serializable]
    [StructLayout(LayoutKind.Sequential)]
    internal struct InternalRenderTarget
    {
        public int _texture;
        public int _resource;
    }

    [Serializable]
    [StructLayout(LayoutKind.Sequential)]
    internal struct InternalPassConfig
    {
        public int _target;
        public int _flags;
    }

    [Serializable]
    [StructLayout(LayoutKind.Sequential)]
    internal struct InternalSemantic
    {
        public int _format;
        public uint _stream;
        public string _desc;
    }

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    internal delegate void RowHandler(IntPtr userData, IntPtr row, int e);

    internal enum RowHandlerEvent
    {
        Added=0,
        Updated=1,
        Deleted=2,
    }

    [Serializable]
    [StructLayout(LayoutKind.Sequential)]
    internal unsafe struct InternalSchema
    {
        public IntPtr _name;
        public int _row_size;
        public IntPtr _primaryKey;
        public int _priOffset;
        public int _priWidth;
    }

    internal static class SchemaHelper
    {
        public static unsafe void CreateSchema(ref InternalSchema schema, string name, string key, int size, int offset, int width)
        {
            schema._row_size = size;
            schema._priOffset = offset;
            schema._priWidth = width;

            schema._name = Marshal.StringToHGlobalAnsi(name);
            schema._primaryKey = Marshal.StringToHGlobalAnsi(key);
        }

        public static unsafe void FreeSchema(ref InternalSchema schema)
        {
            Marshal.FreeHGlobal(schema._name);
            Marshal.FreeHGlobal(schema._primaryKey);
        }
    }

    //internal static class ReferenceCounter
    //{
    //    private static Dictionary<IntPtr, int> refs = new Dictionary<IntPtr, int>();

    //    public static void AddRef(IntPtr p)
    //    {
    //        if (p != IntPtr.Zero)
    //        {
    //            if (refs.ContainsKey(p))
    //            {
    //                refs[p]++;
    //            }
    //            else
    //            {
    //                refs[p] = 1;
    //            }
    //        }
    //    }

    //    public static void RemoveRef(IntPtr p)
    //    {
    //        if (refs.ContainsKey(p))
    //        {
    //            refs[p]--;
    //            if (refs[p] == 0)
    //            {
    //                NativeMethods.Free(p);
    //            }
    //        }
    //    }
    //}

    [Serializable]
    [StructLayout(LayoutKind.Sequential)]
    internal struct InternalTexture
    {
        public int index;
        public int source;
        public int atlasIndex;
        public float px;
        public float py;
        public float sx;
        public float sy;
    };

    [Serializable]
    [StructLayout(LayoutKind.Sequential)]
    internal unsafe struct InternalSubMaterial
    {
        public InternalTexture texture1;
        public InternalTexture texture2;
        public InternalTexture texture3;
        public InternalTexture texture4;
    }

    [Serializable]
    [StructLayout(LayoutKind.Sequential)]
    internal unsafe struct InternalMaterial
    {
        public int pass;
        public byte vertexShader;
        public byte pixelShader;
        public byte submaterialcount;
        public byte submaterialtype;
    	public float clipping;
        public IntPtr submaterials;
        public InternalTexture texture1;
        public InternalTexture texture2;
        public InternalTexture texture3;
        public InternalTexture texture4;
    }

    [StructLayout(LayoutKind.Sequential)]
    internal struct InternalStatBlock
    {
        public int _id;
        public int _free_rows;
        public int _used_rows;
        public int _max_rows;
        public int _mem_usage;
    }
}
