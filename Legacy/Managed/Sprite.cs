﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.InteropServices;
using System.Text;

namespace Islander.Managed
{
    [Serializable]
    public class Texture
    {
        internal InternalTexture tex;

        internal Texture(InternalTexture tex)
        {
            this.tex = tex;
        }

        public Texture(float px, float py, float sx, float sy, int tex)
        {
            this.tex = new InternalTexture()
            {
                px = px,
                py = py,
                sx = sx,
                sy = sy,
                index = tex,
            };
        }

        public float Px { get { return tex.px; } }

        public float Py { get { return tex.py; } }

        public float Sx { get { return tex.sx; } }

        public float Sy { get { return tex.sy; } }

        public int Index { get { return tex.index; } }
    }

    public enum SubMaterialType
    {
        Unknown=0,
        _9Sliced=1,
    }

    [Serializable]
    public class SubMaterial
    {
        internal InternalSubMaterial material;
        private Texture[] texture = new Texture[4];

        internal SubMaterial(InternalSubMaterial material)
        {
            this.material = material;

            this.texture[0] = new Texture(material.texture1);
            this.texture[1] = new Texture(material.texture2);
            this.texture[2] = new Texture(material.texture3);
            this.texture[3] = new Texture(material.texture4);
        }

        public SubMaterial(Texture[] textures)
        {
            this.texture = textures;

            this.material = new InternalSubMaterial();

            for (int i = 0; i < textures.Length; i++)
            {
                switch (i)
                {
                    case 0:
                        this.material.texture1 = this.texture[0].tex;
                        break;
                    case 1:
                        this.material.texture2 = this.texture[1].tex;
                        break;
                    case 2:
                        this.material.texture3 = this.texture[2].tex;
                        break;
                    case 3:
                        this.material.texture4 = this.texture[3].tex;
                        break;
                }
            }
        }

        public Texture GetTexture(int index)
        {
            if (index < 0 || index >= texture.Length)
                return null;

            return texture[index];
        }
    }

    [Serializable]
    public class Material
    {
        internal InternalMaterial material;
        private Texture[] texture = new Texture[4];
        private SubMaterial[] submaterials = new SubMaterial[16];

        internal Material(InternalMaterial material, List<SubMaterial> sub)
        {
            this.material = material;

            this.texture[0] = new Texture(material.texture1);
            this.texture[1] = new Texture(material.texture2);
            this.texture[2] = new Texture(material.texture3);
            this.texture[3] = new Texture(material.texture4);

            for (int i = 0; i < sub.Count; i++)
            {
                if (sub[i] != null)
                {
                    submaterials[i] = new SubMaterial(sub[i].material);
                }
            }
        }

        public Material(int pix, int vert, int pass, Texture[] tex, SubMaterial[] sub, SubMaterialType submaterialtype)
        {
            this.material = new InternalMaterial()
            {
                pass = pass,
                pixelShader = (byte)pix,
                vertexShader = (byte)vert,
                clipping = -1,
            };

            if (sub.Length > 0)
            {
                this.material.submaterialtype = (byte)submaterialtype;

                Array.Copy(sub, submaterials, sub.Length);
            }

            Array.Copy(tex, this.texture, tex.Length);

            for (int i = 0; i < tex.Length; i++)
            {
                switch (i)
                {
                    case 0:
                        material.texture1 = this.texture[0].tex;
                        break;
                    case 1:
                        material.texture2 = this.texture[1].tex;
                        break;
                    case 2:
                        material.texture3 = this.texture[2].tex;
                        break;
                    case 3:
                        material.texture4 = this.texture[3].tex;
                        break;
                }
            }
        }

        public Material(int pix, int vert, int pass, Texture[] tex)
        {
            this.material = new InternalMaterial()
            {
                pass = pass,
                pixelShader = (byte)pix,
                vertexShader = (byte)vert,
                clipping = -1,
            };

            Array.Copy(tex, this.texture, tex.Length);

            for (int i = 0; i < tex.Length; i++)
            {
                switch (i)
                {
                    case 0:
                        material.texture1 = this.texture[0].tex;
                        break;
                    case 1:
                        material.texture2 = this.texture[1].tex;
                        break;
                    case 2:
                        material.texture3 = this.texture[2].tex;
                        break;
                    case 3:
                        material.texture4 = this.texture[3].tex;
                        break;
                }
            }
        }

        public SubMaterialType SubMaterialType
        {
            get
            {
                return (SubMaterialType) this.material.submaterialtype;
            }
        }

        public List<SubMaterial> CopySubMaterials()
        {
            return new List<SubMaterial>(submaterials);
        }

        public SubMaterial GetSubMaterial(int index)
        {
            if (index < 0 || index >= this.submaterials.Length)
            {
                return null;
            }

            return this.submaterials[index];
        }

        public Texture GetTexture(int index)
        {
            if (index < 0 || index >= texture.Length)
                return null;

            return texture[index];
        }

        public int Pass
        {
            get
            {
                return this.material.pass;
            }
        }

        public int VertexShader
        {
            get
            {
                return this.material.vertexShader;
            }
        }

        public int PixelShader
        {
            get
            {
                return this.material.pixelShader;
            }
        }
    }

    [Serializable]
    public class ISprite : IManagedComponent, WellDefinedComponent, IDeserializationCallback
    {
        private InternalSprite sprite;
        private List<SubMaterial> subMaterials;

        public ISprite() : base("Sprite")
        { }

        [Serializable]
        [StructLayout(LayoutKind.Sequential)]
        internal struct InternalSprite
        {
            public int index;
            public int _wrap;
            public InternalMaterial _material;
        }

        public unsafe void CreateNew()
        {
            sprite._material.clipping = -1;
        }

        public int Pass
        {
            get
            {
                return this.sprite._material.pass;
            }
            set
            {
                this.sprite._material.pass = value;
                this.UpdateSprite();
            }
        }

        public int PixelShader
        {
            get
            {
                return this.sprite._material.pixelShader;
            }
            set
            {
                this.sprite._material.pixelShader = (byte)value;
                UpdateSprite();
            }
        }

        public int VertexShader
        {
            get
            {
                return this.sprite._material.vertexShader;
            }
            set
            {
                this.sprite._material.vertexShader = (byte)value;
                UpdateSprite();
            }
        }

        public SubMaterialType SubMaterialType
        {
            get
            {
                return (SubMaterialType) this.sprite._material.submaterialtype;
            }
        }

        public void SetMaterial(Material material)
        {          
            this.sprite._material = material.material;
            this.subMaterials = material.CopySubMaterials();
            UpdateSprite();
        }

        public Material CloneMaterial()
        {
            return new Material(this.sprite._material, this.subMaterials);
        }

        public void OnDeserialization(object sender)
        {
            //unsafe
            //{
            //    this.sprite._material.submaterials = NativeMethods.Allocate(sizeof(InternalSubMaterial) * 16);
            //    for (int i = 0; i < this.sprite._material.submaterialcount; i++)
            //    {
            //        IntPtr mat = this.sprite._material.submaterials + i * sizeof(InternalSubMaterial);
            //        Marshal.StructureToPtr(this.subMaterials[i].material, mat, false);
            //    }
            //}
            
            //ReferenceCounter.AddRef(this.sprite._material.submaterials);
        }

        unsafe private void UpdateSprite()
        {
            PerfStats.Record("UpdateComponent");
            fixed (InternalSprite* sp = &sprite)
            {
                NativeMethods.UpdateComponentPartial(this.Row.World.Binding.world, (int)this.Row.Entity.ComponentType, Row.Entity.ID, new IntPtr(sp),
                    sizeof(Entity.InternalEntity) + sizeof(CustomComponentSink.Properties) + sizeof(ITransform.InternalTransform), sizeof(InternalSprite));
            }
        }

        unsafe void WellDefinedComponent.Update()
        {
            UpdateSprite();
        }
    }
}
