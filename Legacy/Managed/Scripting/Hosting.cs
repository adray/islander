﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace Islander.Managed.Scripting
{
    public static class Hosting2
    {
        public static IScript CreateScript<T>(IScriptSite site)
        {
            var script = (IScript) Activator.CreateInstance(typeof(T));
            script.Initialize(new ScriptingSystem(WorldBuilder.CreateWorld(), site));
            return script;
        }
    }
}
