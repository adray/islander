﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text;

namespace Islander.Managed.Scripting
{
    [Obsolete("Prefer explicit interfaces.")]
    public sealed class HandlesActionAttribute : Attribute
    {
        public string Action { get; private set; }

        public HandlesActionAttribute(string action)
        {
            this.Action = action;
        }
    }

    public sealed class BindingAttribute : Attribute
    {
        public string Scope { get; private set; }
        public string Parameter { get; private set; }

        public BindingAttribute(string scope, string parameter)
        {
            this.Scope = scope;
            this.Parameter = parameter;
        }
    }

    [Serializable]
    internal class HandleCollection<T>
    {
        private List<T> list = new List<T>();
        private Queue<int> free = new Queue<int>();
 
        public int Allocate(T value)
        {
            if (free.Count > 0)
            {
                int index = free.Dequeue();
                list[index] = value;
                return index;
            }

            list.Add(value);
            return list.Count-1;
        }

        public void Free(int handle)
        {
            list[handle] = default(T);
            free.Enqueue(handle);
        }

        public T GetAndFree(int handle)
        {
            T value = list[handle];
            Free(handle);
            return value;
        }
    }

    public interface IPlayerProfile
    {
        string Name { get; }
    }

    public interface ISave
    {
        string Name { get; }
    }

    public interface IGameContext
    {
        void NewGame();
        void SaveGame(ISave save);
        void LoadGame(ISave save);
        IPlayerProfile Profile { get; }
    }

    public class ServiceAvailableEventArgs : EventArgs
    {
        public object Service { get; private set; }
        public Type Type { get; private set; }

        public ServiceAvailableEventArgs(object service, Type type)
        {
            this.Service = service;
            this.Type = type;
        }
    }

    public interface IScriptSite
    {
        T GetService<T>();
        event EventHandler<ServiceAvailableEventArgs> ServiceAvailable;
    }

    [Serializable]
    public class ScriptSite : IScriptSite, IDeserializationCallback
    {
        public event EventHandler<ServiceAvailableEventArgs> ServiceAvailable;

        [NonSerialized]
        private Dictionary<Type, object> services = new Dictionary<Type, object>();

        public void AddService<T>(T service)
        {
            this.services.Add(typeof(T), service);

            if (this.ServiceAvailable != null)
            {
                this.ServiceAvailable(this,
                    new ServiceAvailableEventArgs(service, typeof(T)));
            }
        }

        public T GetService<T>()
        {
            return (T)this.services[typeof(T)];
        }

        public void OnDeserialization(object sender)
        {
            this.services = new Dictionary<Type, object>();
        }
    }

    public sealed class MouseEventArgs : EventArgs
    {
        public bool Handled { get; set; }
        public float X { get; private set; }
        public float Y { get; private set; }

        public MouseEventArgs(float x, float y)
        {
            this.X = x;
            this.Y = y;
        }
    }

    public sealed class KeyPressEventArgs : EventArgs
    {
        public bool Handled { get; set; }
        public KeyboardKeys Key { get; set; }

        public KeyPressEventArgs(KeyboardKeys e)
        {
            this.Key = e;
        }
    }

    public interface IInputService
    {
        event EventHandler<MouseEventArgs> LeftMouseDown;
        event EventHandler<MouseEventArgs> RightMouseDown;
        event EventHandler<MouseEventArgs> LeftMouseUp;
        event EventHandler<MouseEventArgs> RightMouseUp;
        event EventHandler<MouseEventArgs> MouseMove;
        event EventHandler<KeyPressEventArgs> KeyPress;
    }

    public interface IWindowingService
    {
        event EventHandler Resized;
        int Width { get; }
        int Height { get; }
        void ToggleCursor(bool visible);
    }

    public class WindowingService : Islander.Managed.Scripting.IWindowingService
    {
        private Window window;

        public event EventHandler Resized;

        public WindowingService(Window window)
        {
            this.window = window;
            this.window.Resized += Window_Resized;
        }

        private void Window_Resized(object sender, EventArgs e)
        {
            if (this.Resized != null)
            {
                this.Resized(this, EventArgs.Empty);
            }
        }

        public int Height
        {
            get { return this.window.Height; }
        }

        public int Width
        {
            get { return this.window.Width; }
        }

        public void ToggleCursor(bool visible)
        {
            if (visible)
            {
                this.window.ShowCursor();
            }
            else
            {
                this.window.HideCursor();
            }
        }
    }

    public interface IScriptingSystem
    {
        IGameContext Context { get; set; }
        IWorld World { get; }
        IScriptSite Site { get; }
    }

    [Serializable]
    internal sealed class ScriptingSystem : IScriptingSystem, IDisposable
    {
        private IWorld world;
        private IScriptSite site;
        private IGameContext context;

        internal ScriptingSystem(IWorld world, IScriptSite site)
        {
            this.world = world;
            this.site = site;
        }

        public IGameContext Context
        {
            get { return this.context; }
            set { this.context = value; }
        }

        public IWorld World
        {
            get { return this.world; }
        }

        public IScriptSite Site
        {
            get { return this.site; }
        }
        
        public void Dispose()
        {
            if (this.world != null)
            {
                this.world.Dispose();
            }

            GC.SuppressFinalize(this);
        }
        
        ~ScriptingSystem()
        {
            if (this.world != null)
            {
                this.world.Dispose();
            }
        }
    }

    [Serializable]
    public abstract class IScript
    {
        private IScriptingSystem system;
        private IEntity self;
        private Dictionary<string, MethodInfo> actions = new Dictionary<string, MethodInfo>();
        private Dictionary<string, PropertyInfo> bind = new Dictionary<string, PropertyInfo>();

        internal void Initialize(IScriptingSystem system)
        {
            this.system = system;
            this.self = system.World.CreateEntity();

            var methods = this.GetType().GetMethods();
            foreach (var method in methods)
            {
                var attributes = method.GetCustomAttributes(true);
                if (attributes != null)
                {
                    foreach (Attribute attribute in attributes)
                    {
                        if (attribute is HandlesActionAttribute)
                        {
                            this.actions.Add(((HandlesActionAttribute)attribute).Action, method);
                        }
                    }                    
                }
            }

            var properties = this.GetType().GetProperties();
            foreach (var property in properties)
            {
                var attributes = property.GetCustomAttributes(true);
                if (attributes != null)
                {
                    foreach (Attribute attribute in attributes)
                    {
                        if (attribute is BindingAttribute)
                        {
                            var binding = (BindingAttribute)attribute;
                            this.bind.Add(binding.Scope + "." + binding.Parameter, property);
                        }
                    }
                }
            }
        }

        [Obsolete]
        public bool TryInvoke(string action)
        {
            MethodInfo callback = null;
            if (actions.TryGetValue(action, out callback))
            {
                callback.Invoke(this, new object[0]);
            }

            return callback != null;
        }

        internal object ReadProperty(string property)
        {
            PropertyInfo info = null;
            if (bind.TryGetValue(property, out info))
            {
                return info.GetGetMethod().Invoke(this, new object[0]);
            }
            return null;
        }

        internal void WriteProperty(string property, object obj)
        {
            PropertyInfo info = null;
            if (bind.TryGetValue(property, out info))
            {
                info.GetSetMethod().Invoke(this, new object[1] { obj });
            }
        }

        public virtual void OnLoad()
        { }

        public virtual void OnActivate()
        { }

        public virtual void OnTriggered()
        { }

        public IScriptingSystem System
        {
            get
            {
                return this.system;
            }
        }

        public IEntity Self
        {
            get
            {
                return this.self;
            }
        }
    }
}
