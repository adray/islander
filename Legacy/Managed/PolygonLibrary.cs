﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace Islander.Managed
{
    [Flags]
    internal enum PolygonCreateFlags
    {
        None = 0x0,
        PosData = 0x1,
        Box = 0x2,
        Copy = 0x4
    }

    [StructLayout(LayoutKind.Sequential)]
    internal unsafe struct InternalPolyData
    {
        public fixed float min[3];
        public fixed float max[3];
        public int skinned;
        public int textureChannels;
    }

    public class PolygonMesh
    {
        internal IntPtr ptr;
        
        public float[] Min { get; internal set; }
        public float[] Max { get; internal set; }
        public bool Skinned { get; internal set; }
        public int TextureChannels { get; internal set; }

        public unsafe int GetAnimationId(string name)
        {
            var bytes = Encoding.UTF8.GetBytes(name);
            var data = Marshal.AllocHGlobal((bytes.Length + 1) * Marshal.SizeOf(typeof(byte)));
            Marshal.Copy(bytes, 0, data, bytes.Length);
            ((byte*)data)[bytes.Length] = 0;

            int id = NativeMethods.GetAnimationId(this.ptr, data);
            Marshal.FreeHGlobal(data);

            return id;
        }

        public float GetAnimationLength(int id)
        {
            return NativeMethods.GetAnimationLength(this.ptr, id);
        }

        public List<string> GetAnimations()
        {
            int count = NativeMethods.GetAnimationCount(this.ptr);
            List<string> animations = new List<string>(count);

            for (int i = 0; i < count; i++)
            {
                int len = NativeMethods.GetAnimationName(this.ptr, i, IntPtr.Zero);
                if (len == 0)
                {
                    continue;
                }

                var data = Marshal.AllocHGlobal(len * Marshal.SizeOf(typeof(byte)));

                NativeMethods.GetAnimationName(this.ptr, i, data);

                unsafe
                {
                    animations.Add(new string((sbyte*)data, 0, len, Encoding.UTF8));
                }

                Marshal.FreeHGlobal(data);
            }

            return animations;
        }
    }

    public class PolygonLibrary
    {
        private readonly IntPtr ptr;

        public PolygonLibrary()
        {
            this.ptr = NativeMethods.CreatePolyLibrary();
        }

        public unsafe PolygonMesh AddPolygonData(float[] vertexData, int[] indexData, int stride, bool createBoxData, bool createPosData)
        {
            var data = Marshal.AllocHGlobal(vertexData.Length * Marshal.SizeOf(typeof(float)));
            var indexData2 = Marshal.AllocHGlobal(indexData.Length * Marshal.SizeOf(typeof(int)));

            Marshal.Copy(vertexData, 0, data, vertexData.Length);
            Marshal.Copy(indexData, 0, indexData2, indexData.Length);

            PolygonCreateFlags flags = PolygonCreateFlags.None;
            flags |= createBoxData ? PolygonCreateFlags.Box : PolygonCreateFlags.None;
            flags |= createPosData ? PolygonCreateFlags.PosData : PolygonCreateFlags.None;

            IntPtr mesh = NativeMethods.AddPolyMeshData(this.ptr, data, indexData2, vertexData.Length * Marshal.SizeOf(typeof(float)) / stride, indexData.Length, stride, (int)flags);

            InternalPolyData polyData;

            NativeMethods.GetPolyMeshData(mesh, new IntPtr(&polyData));

            return new PolygonMesh()
            {
                ptr = mesh,
                Skinned = polyData.skinned == 1,
                Min = new float[] { polyData.min[0], polyData.min[1], polyData.min[2] },
                Max = new float[] { polyData.max[0], polyData.max[1], polyData.max[2] }
            };
        }

        public unsafe PolygonMesh LoadEngine(byte[] data)
        {
            var data2 = Marshal.AllocHGlobal(data.Length * Marshal.SizeOf(typeof(byte)));
            Marshal.Copy(data, 0, data2, data.Length);

            IntPtr mesh = NativeMethods.LoadEngineMesh(this.ptr, data2, data.Length);

            Marshal.FreeHGlobal(data2);

            if (mesh == IntPtr.Zero)
            {
                return null;
            }

            InternalPolyData polyData;
            
            NativeMethods.GetPolyMeshData(mesh, new IntPtr(&polyData));

            return new PolygonMesh()
            {
                ptr = mesh,
                Skinned = polyData.skinned == 1,
                Min = new float[] { polyData.min[0], polyData.min[1], polyData.min[2] },
                Max = new float[] { polyData.max[0], polyData.max[1], polyData.max[2] },
                TextureChannels = polyData.textureChannels
            };
        }
    }
}
