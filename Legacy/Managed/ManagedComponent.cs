﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;
using System.Text;

namespace Islander.Managed
{
    public class PropertyChangedEventArgs : EventArgs
    {
        public string Property { get; private set; }

        public PropertyChangedEventArgs(String property)
        {
            this.Property = property;
        }
    }

    [Serializable]
    internal class Row
    {
        public Entity Entity;
        public World World;
    }

    [Serializable]
    public abstract class IManagedComponent
    {
        private string name;
        private Row row;
        private bool custom;
        private CustomComponentSink sink;
        private Dictionary<string, Property> properties = new Dictionary<string, Property>();
        private Dictionary<string, Property> updateQueue = new Dictionary<string, Property>();
        public event EventHandler<PropertyChangedEventArgs> PropertyChanged;

        private enum PropertyType
        {
            None,
            Float,
            Vec2f,
            Vec3f,
            Vec2Float,
            Vec3Float
        }

        [Serializable]
        private class Property
        {
            public string Name { get; set; }
            public int Index { get; set; }
            public PropertyType PropertyType { get; set; }
            public Func<float> F1 { get; set; }
            public Func<Vec2f> F2 { get; set; }
            public Func<Vec3f> F3 { get; set; }
            public Func<Vec2<float>> F4 { get; set; }
            public Func<Vec3<float>> F5 { get; set; }
        }
        
        public IManagedComponent(string name)
        {
            this.name = name;
            this.custom = true;
        }

        internal IManagedComponent(string name, bool custom)
        {
            this.name = name;
            this.custom = custom;
        }

        internal virtual void Init(Row row)
        {
            this.row = row;
            this.sink = this.row.Entity.GetComponent<CustomComponentSink>();

            if (this.sink != null)
            {
                foreach (var pair in this.updateQueue)
                {
                    Property prop = pair.Value;
                    this.ProcessItem(prop);
                }

                this.updateQueue.Clear();
            }
        }

        internal Row Row
        {
            get { return this.row; }
        }

        internal protected int EntityID
        {
            get
            {
                return this.row.Entity.ID;
            }
        }

        public bool IsCustom
        {
            get
            {
                return this.custom;
            }
        }

        private void ProcessItem(Property property)
        {
            property.Index = sink.AllocateIndex(property.Name);
            this.properties.Add(property.Name, property);
            this.UpdateProperty(property);
        }

        private void UpdateProperty(Property property)
        {
            float[] f = new float[4];
            int len = 0;
            if (property.PropertyType == PropertyType.Float)
            {
                f[0] = property.F1();
                len = 1;
            }
            else if (property.PropertyType == PropertyType.Vec2f)
            {
                var v2 = property.F2();
                f[0] = v2.X;
                f[1] = v2.Y;
                len = 2;
            }
            else if (property.PropertyType == PropertyType.Vec3f)
            {
                var v3 = property.F3();
                f[0] = v3.X;
                f[1] = v3.Y;
                f[2] = v3.Z;
                len = 3;
            }
            else if (property.PropertyType == PropertyType.Vec2Float)
            {
                var v2 = property.F4();
                f[0] = v2.X;
                f[1] = v2.Y;
                len = 2;
            }
            else if (property.PropertyType == PropertyType.Vec3Float)
            {
                var v3 = property.F5();
                f[0] = v3.X;
                f[1] = v3.Y;
                f[2] = v3.Z;
                len = 3;
            }

            if (len > 0)
            {
                sink.UpdateProperty(property.Index, f, len);
            }
        }

        protected void OnPropertyChanged(string property)
        {
            if (this.IsCustom)
            {
                string name = this.GetType().Name + "." + property;

                Property prop;
                if (this.properties.TryGetValue(name, out prop))
                {
                    this.UpdateProperty(prop);
                }
                else
                {
                    prop = new Property();
                    prop.Name = name;

                    var methodInfo = this.GetType().GetProperty(property).GetGetMethod();

                    var returnType = methodInfo.ReturnType;
                    if (returnType == typeof(float))
                    {
                        prop.F1 = (Func<float>)Delegate.CreateDelegate(typeof(Func<float>), this, methodInfo);
                        prop.PropertyType = PropertyType.Float;
                    }
                    else if (returnType == typeof(Vec2f))
                    {
                        prop.F2 = (Func<Vec2f>)Delegate.CreateDelegate(typeof(Func<Vec2f>), this, methodInfo);
                        prop.PropertyType = PropertyType.Vec2f;
                    }
                    else if (returnType == typeof(Vec3f))
                    {
                        prop.F3 = (Func<Vec3f>)Delegate.CreateDelegate(typeof(Func<Vec3f>), this, methodInfo);
                        prop.PropertyType = PropertyType.Vec3f;
                    }
                    else if (returnType == typeof(Vec2<float>))
                    {
                        prop.F4 = (Func<Vec2<float>>)Delegate.CreateDelegate(typeof(Func<Vec2<float>>), this, methodInfo);
                        prop.PropertyType = PropertyType.Vec2Float;
                    }
                    else if (returnType == typeof(Vec3<float>))
                    {
                        prop.F5 = (Func<Vec3<float>>)Delegate.CreateDelegate(typeof(Func<Vec3<float>>), this, methodInfo);
                        prop.PropertyType = PropertyType.Vec3Float;
                    }

                    if (this.sink == null)
                    {
                        if (this.updateQueue.ContainsKey(name))
                        {
                            this.updateQueue[name] = prop;
                        }
                        else
                        {
                            this.updateQueue.Add(name, prop);
                        }
                    }
                    else
                    {
                        this.ProcessItem(prop);
                    }
                }
            }

            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(property));
            }
        }

        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        { }
    }

    internal interface WellDefinedComponent
    {
        void CreateNew();
        void Update();
    }

    [Serializable]
    internal class CustomComponentSink : IManagedComponent, WellDefinedComponent
    {
        private static Dictionary<string, int> keys = new Dictionary<string, int>();
        private Dictionary<string, int> allocated = new Dictionary<string, int>(); 
        private Properties properties = new Properties();
        private int index;

        [Serializable]
        [StructLayout(LayoutKind.Sequential)]
        internal unsafe struct Entry
        {
	        public int key;
	        public fixed float data[4];
        }

        [Serializable]
        [StructLayout(LayoutKind.Sequential)]
        internal unsafe struct Properties
        {
	        public int count;
            public Entry entry1;
            public Entry entry2;
            public Entry entry3;
            public Entry entry4;
            public Entry entry5;
            public Entry entry6;
            public Entry entry7;
            public Entry entry8;
        }

        public CustomComponentSink() : base("CustomComponentSink")
        { }

        public void CreateNew()
        {
        }

        unsafe void WellDefinedComponent.Update()
        {
            Update();
        }

        private unsafe void Update()
        {
            PerfStats.Record("UpdateComponent");
            fixed (Properties* prop = &properties)
            {
                NativeMethods.UpdateComponentPartial(this.Row.World.Binding.world, (int)this.Row.Entity.ComponentType, Row.Entity.ID, new IntPtr(prop), sizeof(Entity.InternalEntity), sizeof(Properties));
            }
        }

        internal unsafe int AllocateIndex(string name)
        {
            int idx = 0;
            if (this.allocated.TryGetValue(name, out idx))
            {
                return idx;
            }

            if (this.index == 8)
            {
                Logger.Log("ManagedComponent: Unable to allocate index " + name,
                    LoggingLevels.Managed);
                return -1;
            }

            int key = 0;
            if (!keys.TryGetValue(name, out key))
            {
                PerfStats.Record("GetComponentProperty");
                key = NativeMethods.GetComponentProperty(name);
                if (key == 0)
                {
                    PerfStats.Record("RegisterComponentProperty");
                    key = NativeMethods.RegisterComponentProperty(name);
                }
                keys.Add(name, key);
            }
            
            properties.count++;

            fixed (Properties* prop = &properties)
            {
                Entry* entry = &prop->entry1 + index; // Will fail if there is padding between elements
                entry->key = key;
            }

            this.allocated.Add(name, this.index);
            
            this.Update();
            return this.index++;
        }

        internal unsafe void UpdateProperty(int index, float[] values, int len)
        {
            if (index < 0 || index >= this.index)
            {
                Logger.Log("ManagedComponent: Failed to set value at index " + index + " of length " + len,
                    LoggingLevels.Managed);
                return;
            }

            fixed (Properties* prop = &properties)
            {
                Entry* entry = &prop->entry1 + index; // Will fail if there is padding between elements
                Marshal.Copy(values, 0, new IntPtr(entry->data), len);
            }
            this.Update();
        }
    }
}
