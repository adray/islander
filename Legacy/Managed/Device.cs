﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace Islander.Managed
{
    public sealed class Camera
    {
        internal IntPtr camera;
        private float x;
        private float y;
        private float z = 1;

        public Camera()
        {
            this.camera = NativeMethods.CreateCamera();
        }

        public float X
        {
            get
            {
                return this.x;
            }
        }

        public float Y
        {
            get
            {
                return this.y;
            }
        }

        public float Z
        {
            get
            {
                return this.z;
            }
        }

        public void SetPosition(float x, float y, float z)
        {
            this.x = x;
            this.y = y;
            this.z = z;
            NativeMethods.SetCameraTranslation(this.camera, this.x, this.y, this.z);
        }
    }

    public sealed class Camera3D
    {
        internal IntPtr camera;

        private float x;
        private float y;
        private float z;
        private float nearZ;
        private float farZ;
        private float fovY;
        private float aspect;
        private float lookAtX;
        private float lookAtY;
        private float lookAtZ;

        public float Aspect => this.aspect;
        public float Fov => this.fovY;
        public float NearZ => this.nearZ;
        public float FarZ => this.farZ;
        public float PosX => this.x;
        public float PosY => this.y;
        public float PosZ => this.z;
        public float LookAtX => this.lookAtX;
        public float LookAtY => this.lookAtY;
        public float LookAtZ => this.lookAtZ;

        public Camera3D()
        {
            this.camera = NativeMethods.CreateCamera3D();
        }

        public void SetPosition(float x, float y, float z)
        {
            this.x = x;
            this.y = y;
            this.z = z;
            NativeMethods.SetCamera3DTranslation(this.camera, this.x, this.y, this.z);
        }

        public void SetProjection(float nearZ, float farZ, float fovY, float aspect)
        {
            this.nearZ = nearZ;
            this.farZ = farZ;
            this.fovY = fovY;
            this.aspect = aspect;
            NativeMethods.SetCamera3DProjection(this.camera, nearZ, farZ, fovY, aspect);
        }

        public void SetLookAt(float x, float y, float z)
        {
            this.lookAtX = x;
            this.lookAtY = y;
            this.lookAtZ = z;
            NativeMethods.SetCamera3DLookAt(this.camera, x, y, z);
        }
    }

    public sealed class WorldBindings
    {
        internal IntPtr binding;
        internal IntPtr world;

        internal WorldBindings(IntPtr binding, IntPtr world)
        {
            this.binding = binding;
            this.world = world;
        }
    }

    public enum ResourceClass
    {
        Texture,
        Mesh
    }

    public class Resource
    {
        private IntPtr device;
        private IntPtr ptr;
        private ResourceClass @class;

        internal Resource(IntPtr device, IntPtr img, IntPtr def)
        {
            this.device = device;
            this.ptr = NativeMethods.CreateResource(device, img, def);
            this.@class = ResourceClass.Texture;
        }

        internal Resource(IntPtr device, IntPtr resource, ResourceClass @class)
        {
            this.device = device;
            this.@class = @class;

            switch (@class)
            {
                case ResourceClass.Texture:
                    this.ptr = NativeMethods.CreateResourceFromTexture(device, resource);
                    break;
                case ResourceClass.Mesh:
                    this.ptr = NativeMethods.CreateResourceFromMesh(device, resource);
                    break;
            }
        }

        public ResourceClass ResourceClass => @class;

        public void Load()
        {
            NativeMethods.LoadResource(this.device, this.ptr);
        }

        public void Unload()
        {
            NativeMethods.UnloadResource(this.device, this.ptr);
        }

        public void LoadAsync()
        {
            NativeMethods.LoadResourceAsync(this.device, this.ptr);
        }

        public void CreateReferencedResources(FileLibrary lib)
        {
            NativeMethods.CreateReferencedResources(this.device, this.ptr, lib.ptr);
        }

        public void LoadReferencedResources()
        {
            NativeMethods.LoadReferencedResources(this.device, this.ptr);
        }

        public PolygonMesh GetMesh()
        {
            if (@class == ResourceClass.Mesh)
            {
                var mesh = NativeMethods.GetPolyMeshDataFromResource(this.ptr);
                if (mesh != IntPtr.Zero)
                {
                    unsafe
                    {
                        InternalPolyData polyData;

                        NativeMethods.GetPolyMeshData(mesh, new IntPtr(&polyData));

                        return new PolygonMesh()
                        {
                            ptr = mesh,
                            Skinned = polyData.skinned == 1,
                            Min = new float[] { polyData.min[0], polyData.min[1], polyData.min[2] },
                            Max = new float[] { polyData.max[0], polyData.max[1], polyData.max[2] },
                            TextureChannels = polyData.textureChannels
                        };
                    }
                }
            }

            return null;
        }
    }

    public interface IDevice
    {
        void RenderScene(IWorld world, Camera camera);
        void RenderScene3D(IWorld world, Camera3D camera);
        void LoadFont(FontDescriptor font);
        void LoadFont(TrueTypeDescriptor font);
        RenderTarget CreateRenderTarget(int width, int height);
        void LoadShaderSemantics(string file);
        int LoadPixelShader(string filename, string method);
        int LoadVertexShader(string filename, string method, string semantic);
        void Present();
        void SetPassList(PassList list);
        void MeasureText(MeasureText text);
        TextureData GetTextureData(RenderTarget target);
        Texture CreateTexture(TextureData data);
        Resource CreateResource(ResourceFile img, ResourceFile def);
        Resource CreateResourceFromTexture(ResourceFile img);
        Resource CreateResourceFromMesh(ResourceFile mesh);
        Texture FindTexture(string name);
        Texture[] FindSubTexture(string name);
        void Generate9Sliced2(string name, float minX, float maxX, float minY, float maxY);
        Vec2f GetTextureSize(string name);
        RendererType RenderType { get; }
        IList<TextureListData> GetTextureList();
        List<DisplayResolution> GetDisplayResolutions();
        void ResizeBackBuffer();
        void ResizeRenderTarget(RenderTarget target, int width, int height);
        void SetDebugBoneMesh(PolygonMesh mesh, int vertexShader, int pixelShader);
    }

    public class ResourceFile
    {
        internal IntPtr ptr;
    }

    public class FileLibrary
    {
        internal IntPtr ptr;

        public FileLibrary()
        {
            this.ptr = NativeMethods.CreateFileLibrary();
        }

        public bool AddFile(string path)
        {
            bool added = NativeMethods.AddFile(this.ptr, path) != 0;
            return added;
        }

        public bool AddDirectory(string dir)
        {
            bool added = NativeMethods.AddDirectory(this.ptr, dir) != 0;
            return added;
        }

        public bool AddCompressedFile(string path)
        {
            bool added = NativeMethods.AddCompressedFile(this.ptr, path) != 0;
            return added;
        }

        public ResourceFile GetFile(string name)
        {
            IntPtr file = NativeMethods.GetFile(this.ptr, name);
            return new ResourceFile() { ptr = file };
        }
    }

    public sealed class TrueTypeDescriptor
    {
        internal InternalFont font;
        private string name;
        private string file;

        public TrueTypeDescriptor(string name, string file)
        {
            this.name = name;
            this.file = file;

            this.font._name = IslanderMarshal.GetUTF8(name);
            this.font._file = IslanderMarshal.GetUTF8(file);
        }

        public string Name
        {
            get
            {
                return this.name;
            }
        }

        public string File
        {
            get
            {
                return this.file;
            }
        }
    }

    public sealed class FontDescriptor
    {
        internal InternalFont font;
        private string name;
        private string file;

        public FontDescriptor(string name, string file)
        {
            this.name = name;
            this.file = file;

            this.font._name = IslanderMarshal.GetUTF8(name);
            this.font._file = IslanderMarshal.GetUTF8(file);
        }

        public string Name
        {
            get
            {
                return this.name;
            }
        }

        public string File
        {
            get
            {
                return this.file;
            }
        }

        public int ID
        {
            get
            {
                return this.font._img;
            }
        }
    }

    public sealed class RenderTarget
    {
        private InternalRenderTarget renderTarget;
        private static RenderTarget @default = new RenderTarget(new InternalRenderTarget()
        {
            _resource = -1,
            _texture = -1,
        });

        public static RenderTarget Default
        {
            get { return @default; }
        }

        internal RenderTarget(InternalRenderTarget renderTarget)
        {
            this.renderTarget = renderTarget;
        }

        public int Texture
        {
            get
            {
                return this.renderTarget._texture;
            }
        }

        public int Resource
        {
            get
            {
                return this.renderTarget._resource;
            }
        }
    }

    public enum RenderFlags
    {
        RENDER_FLAGS_NONE = 0,
        RENDER_FLAGS_CLEAR = 1,
        RENDER_FLAGS_CLEAR_TRANSPARENT = 2,
        RENDER_FLAGS_BLEND_ADD = 4,
    }

    public sealed class PassList
    {
        internal IntPtr passes;
        private int id;

        public PassList()
        {
            PerfStats.Record("CreatePassList");
            this.passes = NativeMethods.CreatePassList();
        }

        public int AddPass(RenderTarget target, RenderFlags flags)
        {
            InternalPassConfig config = new InternalPassConfig();
            config._target = target.Resource;
            config._flags = (int)flags;

            PerfStats.Record("AppendPassList");
            NativeMethods.AppendPassList(this.passes, config);
            return id++;
        }
    }

    public class MeasureText
    {
        public string Font { get; set; }
        public string Text { get; set; }
        public bool Wrap { get; set; }
        public int Size { get; set; }
        public float Width { get; set; }
        public float Height { get; set; }
    }

    public class TextureData
    {
        public byte[] Data { get; private set; }
        public int Width { get; private set; }
        public int Height { get; private set; }

        public TextureData(byte[] data, int width, int height)
        {
            Data = data;
            Width = width;
            Height = height;
        }
    }

    public class TextureListData
    {
        public int ID { get; private set; }
        public int Width { get; private set; }
        public int Height { get; private set; }

        public TextureListData(int id, int width, int height)
        {
            this.ID = id;
            this.Width = width;
            this.Height = height;
        }
    }

    public class Display
    {
        public int ID { get; }
        public string Name { get; }

        public Display(int id, string name)
        {
            this.ID = id;
            this.Name = name;
        }
    }

    public class DisplayResolution
    {
        public int ID { get; }
        public int Width { get; }
        public int Height { get; }

        public DisplayResolution(int id, int width, int height)
        {
            this.ID = id;
            this.Width = width;
            this.Height = height;
        }
    }

    public enum RendererType
    {
        D3D11 = 0,
        OpenGL = 1,
    }

    internal sealed class Device : IDevice
    {
        private IntPtr device;
        private Dictionary<string, InternalSemantic[]> semantics = new Dictionary<string, InternalSemantic[]>();

        internal Device(IntPtr window, FontDescriptor descriptor, RendererType preferredRenderer)
        {
            this.device = NativeMethods.CreateDevice();
            NativeMethods.SetPreferredRenderer(this.device, (int)preferredRenderer);
            NativeMethods.InitializeDevice(this.device, window, descriptor.font);
            descriptor.font._img = 2;
        }

        public RendererType RenderType
        {
            get
            {
                return (RendererType)NativeMethods.GetCurrentRenderer(this.device);
            }
        }

        public void MeasureText(MeasureText text)
        {
            InternalMeasureText measure = new InternalMeasureText()
                {
                    _wrap = text.Wrap ? 1 : 0,
                    _size = text.Size
                };

            unsafe
            {
                measure._name = Marshal.StringToHGlobalAnsi(text.Font);
                measure._text = Marshal.StringToHGlobalAnsi(text.Text);
                measure._width = text.Width;
                measure._height = text.Height;

                PerfStats.Record("MeasureText");
                NativeMethods.MeasureText(this.device, new IntPtr(&measure));

                Marshal.FreeHGlobal(measure._name);
                Marshal.FreeHGlobal(measure._text);
            }

            text.Width = measure._width;
            text.Height = measure._height;
        }

        public void SetPassList(PassList list)
        {
            PerfStats.Record("MeasureText");
            NativeMethods.SetPassList(this.device, list.passes);
        }

        public void Present()
        {
            PerfStats.Record("Present");
            NativeMethods.Present(this.device);
        }

        public void RenderScene(IWorld world, Camera camera)
        {
            PerfStats.Record("RenderScene");
            NativeMethods.RenderScene(this.device,
                world.Binding.world,
                camera.camera,
                world.Binding.binding);
        }

        public void RenderScene3D(IWorld world, Camera3D camera)
        {
            PerfStats.Record("RenderScene");
            NativeMethods.RenderScene3D(this.device,
                world.Binding.world,
                camera.camera,
                world.Binding.binding);
        }

        public Texture CreateTexture(TextureData data)
        {
            int texture;
            unsafe
            {
                fixed (byte* p = data.Data)
                {
                    texture = NativeMethods.CreateTexture(this.device, new IntPtr(p), data.Width, data.Height, data.Data.Length);
                }
            }

            return new Texture(0, 0, 1, 1, texture);
        }

        public IList<TextureListData> GetTextureList()
        {
            InternalTextureList data;
            List<TextureListData> list = new List<TextureListData>();

            unsafe
            {
                if (NativeMethods.GetTextureList(device, new IntPtr(&data)) == 1)
                {
                    for (int i = 0; i < data.count; i++)
                    {
                        var item = (InternalTextureListData*)data.data + i;

                        list.Add(new TextureListData(item->id, item->width, item->height));
                    }
                }
            }

            return list;
        }

        public TextureData GetTextureData(RenderTarget target)
        {
            return GetTextureData(target.Texture);
        }

        public TextureData GetTextureData(int textureId)
        {
            unsafe
            {
                int w, h, s;
                IntPtr data = NativeMethods.GetBytesFromTexure(this.device, textureId,
                    new IntPtr(&w),
                    new IntPtr(&h),
                    new IntPtr(&s));

                if (data != IntPtr.Zero)
                {
                    byte[] array = new byte[s];
                    Marshal.Copy(data, array, 0, array.Length);

                    NativeMethods.FreeTextureData(data);

                    return new TextureData(array, w, h);
                }
            }

            return new TextureData(null, 0, 0);
        }

        public void LoadFont(FontDescriptor font)
        {
            font.font._img = NativeMethods.LoadFont(this.device, font.font);
        }

        public void LoadFont(TrueTypeDescriptor font)
        {
            NativeMethods.LoadFont(this.device, font.font);
        }

        public Texture FindTexture(string name)
        {
            InternalTexture texture;
            unsafe
            {
                NativeMethods.FindMaterialTexture(this.device, name, new IntPtr(&texture));
            }
            return new Texture(texture);
        }

        public Texture[] FindSubTexture(string name)
        {
            int count;
            unsafe
            {
                InternalTexture* texture = (InternalTexture *) NativeMethods.FindMaterialSubTexture(this.device, name, new IntPtr(&count));

                if (texture != null)
                {
                    Texture[] textures = new Texture[count];
                    for (int i = 0; i < count; i++)
                    {
                        textures[i] = new Texture(texture[i]);
                    }

                    return textures;
                }
            }

            return null;
        }

        public Vec2f GetTextureSize(string name)
        {
            unsafe
            {
                int w, h;
                if (NativeMethods.GetTextureSize(this.device, name, new IntPtr(&w), new IntPtr(&h)) == 1)
                {
                    return new Vec2f(w, h);
                }
            }

            return new Vec2f();
        }

        [StructLayout(LayoutKind.Sequential)]
        private struct _9SlicedData
        {
            public float _minX;
            public float _maxX;
            public float _minY;
            public float _maxY;
        }

        public void Generate9Sliced2(string name, float minX, float maxX, float minY, float maxY)
        {
            _9SlicedData data = new _9SlicedData();
            data._minX = minX;
            data._maxX = maxX;
            data._minY = minY;
            data._maxY = maxY;

            unsafe
            {
                NativeMethods.GenerateSubTextures(this.device, name, 1, new IntPtr(&data));
            }
        }

        public RenderTarget CreateRenderTarget(int width, int height)
        {
            InternalRenderTarget target = new InternalRenderTarget();

            unsafe
            {
                NativeMethods.CreateRenderTarget(this.device, width, height, new IntPtr(&target));
            }

            return new RenderTarget(target);
        }

        public int LoadPixelShader(string filename, string method)
        {
            return NativeMethods.LoadPixelShader(this.device, filename, method);
        }

        public int LoadVertexShader(string filename, string method, string semantic)
        {
            InternalSemantic[] semantics = null;

            if (this.semantics.TryGetValue(semantic, out semantics))
            {
                return NativeMethods.LoadVertexShader(this.device, filename, method, semantics, semantics.Length);
            }

            return -1;
        }

        public void LoadShaderSemantics(string file)
        {
            System.Xml.XmlDocument doc = new System.Xml.XmlDocument();

            doc.Load(file);

            foreach (System.Xml.XmlNode root in doc.SelectNodes("/Semantics/Set"))
            {
                string name = root.Attributes["Name"].Value;

                List<InternalSemantic> items = new List<InternalSemantic>();
                foreach (System.Xml.XmlNode semantic in root.SelectNodes("Semantic"))
                {
                    InternalSemantic item = new InternalSemantic();

                    string description = semantic.Attributes["Description"].Value;
                    string format = semantic.Attributes["Format"].Value;

                    System.Xml.XmlNode streamNode = semantic.Attributes["Stream"];

                    uint stream = 0;
                    if (streamNode != null)
                    {
                        stream = UInt32.Parse(streamNode.Value);
                    }

                    item._desc = description;
                    item._stream = stream;

                    switch (format)
                    {
                        case "FLOAT":
                            item._format = 0;
                            break;
                        case "FLOAT2":
                            item._format = 1;
                            break;
                        case "FLOAT3":
                            item._format = 2;
                            break;
                        case "FLOAT4":
                            item._format = 3;
                            break;
                    }

                    items.Add(item);
                }

                this.semantics.Add(name, items.ToArray());
            }
        }

        public Resource CreateResource(ResourceFile img, ResourceFile def)
        {
            return new Resource(this.device, img.ptr, def.ptr);
        }

        public Resource CreateResourceFromTexture(ResourceFile img)
        {
            return new Resource(this.device, img.ptr, ResourceClass.Texture);
        }

        public Resource CreateResourceFromMesh(ResourceFile mesh)
        {
            return new Resource(this.device, mesh.ptr, ResourceClass.Mesh);
        }

        public List<DisplayResolution> GetDisplayResolutions()
        {
            List<DisplayResolution> displayResolutions = new List<DisplayResolution>();

            unsafe
            {
                InternalResolutions resolutions;
                NativeMethods.GetResolutions(this.device, new IntPtr(&resolutions));

                for (int i = 0; i < resolutions.count; i++)
                {
                    InternalResolution* element = (InternalResolution*)resolutions.data + i;
                    displayResolutions.Add(new DisplayResolution(element->id, element->width, element->height));
                }
            }

            return displayResolutions;
        }

        public void ResizeBackBuffer()
        {
            NativeMethods.ResizeBackBuffer(this.device);
        }

        public void ResizeRenderTarget(RenderTarget target, int width, int height)
        {
            NativeMethods.ResizeRenderTarget(this.device, new InternalRenderTarget()
            {
                _resource = target.Resource,
                _texture = target.Texture
            }, width, height);
        }

        public void SetDebugBoneMesh(PolygonMesh mesh, int vertexShader, int pixelShader)
        {
            NativeMethods.SetDebugBoneMesh(device, mesh.ptr, vertexShader, pixelShader);
        }
    }
}
