﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;
using System.Text;

namespace Islander.Managed
{
    public class LinkRectangle
    {
        public float X { get; private set; }
        public float Y { get; private set; }
        public float Width { get; private set; }
        public float Height { get; private set; }

        public LinkRectangle(float x, float y, float width, float height)
        {
            this.X = x;
            this.Y = y;
            this.Width = width;
            this.Height = height;
        }
    }

    public class LinkElement
    {
        public LinkRectangle Rectangle { get; private set; }
        public int StartIndex { get; private set; }
        public int Length { get; private set; }

        public LinkElement(LinkRectangle rectangle, int startIndex, int length)
        {
            this.Rectangle = rectangle;
            this.StartIndex = startIndex;
            this.Length = length;
        }
    }

    [Serializable]
    public class ITextSprite : IManagedComponent,
        WellDefinedComponent,
        IDeserializationCallback
    {
        private InternalTextSprite sprite = new InternalTextSprite();
        private string text;
        private string font;
        private bool centered;
        private bool initialized;
        private static int ManagedCleaup = 2;
        private int truetype_size;

        public ITextSprite()
            : base("TextSprite")
        { }

        [Serializable]
        [StructLayout(LayoutKind.Sequential)]
        internal struct InternalRectange
        {
            public int x;
            public int y;
            public int width;
            public int height;
        }

        [Serializable]
        [StructLayout(LayoutKind.Sequential)]
        internal struct InternalLinkElement
        {
            public InternalRectange _rect;
            public int _startIndex;
            public int _length;
        }

        [SerializationNotRequired]
        [StructLayout(LayoutKind.Sequential)]
        internal unsafe struct InternalTextData
        {
	        public IntPtr _text;
            public InternalRectange _rect;
            public fixed int tabs[32];
            public IntPtr _font;
            public int centered;
            public int cleanup;
            public InternalLinkElement link1;
            public InternalLinkElement link2;
            public InternalLinkElement link3;
            public InternalLinkElement link4;
            public InternalLinkElement link5;
            public InternalLinkElement link6;
            public InternalLinkElement link7;
            public InternalLinkElement link8;
            public InternalLinkElement link9;
            public InternalLinkElement link10;
            public InternalLinkElement link11;
            public InternalLinkElement link12;
            public InternalLinkElement link13;
            public InternalLinkElement link14;
            public InternalLinkElement link15;
            public InternalLinkElement link16;
            public int linkcount;
            public int truetype_size;
        }

        [Serializable]
        [StructLayout(LayoutKind.Sequential)]
        internal struct InternalColour
        {
            public float r;
            public float g;
            public float b;
            public float a;
        }

        [Serializable]
        [StructLayout(LayoutKind.Sequential)]
        internal struct InternalTextSprite
        {
            [MarshalAs(UnmanagedType.Struct)]
            public InternalColour _colour;
	        public int _vertid;
	        public int dirty;
	        public int vertex;
	        public int pixel;
	        public int img;
	        public int pass;
	        public int wrap;
            [NonSerialized]
    	    public IntPtr data;
        }

        public unsafe void CreateNew()
        {
            sprite.data = NativeMethods.Allocate(Marshal.SizeOf(typeof(InternalTextData)));
            sprite._vertid = -1;

            InternalTextData textData = (InternalTextData)Marshal.PtrToStructure(sprite.data, typeof(InternalTextData));
            textData.cleanup = ManagedCleaup;
            textData._font = IntPtr.Zero;
            textData._text = IntPtr.Zero;
            textData.centered = 0;
            textData.linkcount = 0;
            Marshal.StructureToPtr(textData, sprite.data, false);
            this.initialized = true;
            this.Update();
        }

        public void SetColour(float r, float g, float b, float a)
        {
            this.Pull();
            this.sprite._colour.r = r;
            this.sprite._colour.g = g;
            this.sprite._colour.b = b;
            this.sprite._colour.a = a;
            this.Update();
        }

        public bool Centered
        {
            get
            {
                return this.centered;
            }
            set
            {
                this.centered = value;
                this.Pull();

                InternalTextData data = (InternalTextData)Marshal.PtrToStructure(sprite.data, typeof(InternalTextData));
                
                data.centered = this.centered ? 1 : 0;

                this.initialized = true;

                Marshal.StructureToPtr(data, sprite.data, false);

                this.Update();
            }
        }

        public int Pass
        {
            get
            {
                return this.sprite.pass;
            }
            set
            {
                this.Pull();
                this.sprite.pass = value;
                this.Update();
            }
        }

        public void MakeDirty()
        {
            this.Pull();
            this.sprite.dirty = 1;
            this.Update();
        }

        public bool Wrap
        {
            get
            {
                return this.sprite.wrap == 1;
            }
            set
            {
                var val = value ? 1 : 0;
                if (val != this.sprite.wrap)
                {
                    this.Pull();
                    this.sprite.wrap = val;
                    this.sprite.dirty = 1;
                    this.Update();
                }
            }
        }

        public float Opacity
        {
            get
            {
                return this.sprite._colour.a;
            }
            set
            {
                this.Pull();
                this.sprite._colour.a = value;
                this.Update();
            }
        }

        public int VertexShader
        {
            get
            {
                return this.sprite.vertex;
            }
            set
            {
                this.Pull();
                this.sprite.vertex = value;
                this.Update();
            }
        }

        public int PixelShader
        {
            get
            {
                return this.sprite.pixel;
            }
            set
            {
                this.Pull();
                this.sprite.pixel = value;
                this.Update();
            }
        }        

        public string Font
        {
            get
            {
                return this.font;
            }
        }

        public void SetTrueTypeDescriptor(TrueTypeDescriptor font, int size)
        {
            this.Pull();

            sprite.img = (1 << 16) | (sprite.img & 0xff); // as we are truetype
            this.font = font.Name;
            this.truetype_size = size;            

            this.Push();
        }

        public void SetFontDescriptor(FontDescriptor font)
        {
            this.Pull();

            sprite.img = font.ID;            
            this.font = font.Name;
            this.truetype_size = 0;

            this.Push();
        }
        
        private void Push()
        {
            InternalTextData data = (InternalTextData)Marshal.PtrToStructure(sprite.data, typeof(InternalTextData));

            if (data._font != IntPtr.Zero)
                NativeMethods.Free(data._font);

            var array = System.Text.Encoding.UTF8.GetBytes(this.font);

            var copy = new byte[array.Length + 1];
            Array.Copy(array, copy, array.Length);

            int size = copy.Length;
            data._font = NativeMethods.Allocate(size);
	        Marshal.Copy(copy, 0, data._font, size);

            data.truetype_size = truetype_size;

            this.initialized = true;

            Marshal.StructureToPtr(data, sprite.data, false);

            this.Update();
        }

        private void Pull()
        {
            PerfStats.Record("GetComponent");
            unsafe
            {
                byte* text = stackalloc byte[this.Row.Entity.Size];
                NativeMethods.GetComponent(this.Row.World.Binding.world, (int)this.Row.Entity.ComponentType, Row.Entity.ID, new IntPtr(text));

                InternalTextSprite* sp = (InternalTextSprite*)(text + sizeof(Entity.InternalEntity) + sizeof(CustomComponentSink.Properties) + sizeof(ITransform.InternalTransform));
                sprite = *sp;
            }
        }

        public List<LinkElement> GetLinkData()
        {
            this.Pull();

            List<LinkElement> elements = new List<LinkElement>();
            InternalTextData data = (InternalTextData) Marshal.PtrToStructure(sprite.data, typeof(InternalTextData));
            InternalLinkElement[] array = new InternalLinkElement[]
            {
                data.link1, data.link2, data.link3, data.link4, data.link5, data.link6, data.link7,
                data.link8, data.link9, data.link10, data.link11, data.link12, data.link13, data.link14, data.link15, data.link16
            };

            for (int i = 0; i < data.linkcount; i++)
            {
                var link = array[i];
                var rect = new LinkRectangle(link._rect.x, link._rect.y, link._rect.width, link._rect.height);
                var element = new LinkElement(rect, link._startIndex, link._length);
                elements.Add(element);
            }

            return elements;
        }

        public string Text
        {
            get
            {
                return this.text;
            }
            set
            {
                if (this.text != value)
                {
                    this.text = value;

                    this.Pull();

                    InternalTextData data = (InternalTextData) Marshal.PtrToStructure(sprite.data, typeof(InternalTextData));

                    if (data._text != IntPtr.Zero)
                        NativeMethods.Free(data._text);

                    var array = System.Text.Encoding.UTF8.GetBytes(this.text);
		            var copy = new byte[array.Length+1];
		            Array.Copy(array, copy, array.Length);

                    int size = copy.Length;
                    data._text = NativeMethods.Allocate(size);
		    
                    Marshal.Copy(copy, 0, data._text, size);// this.text.Length+1);
                    
                    sprite.dirty = 1;

                    this.initialized = true;

                    Marshal.StructureToPtr(data, sprite.data, false);

                    this.Update();
                }
            }
        }

        void WellDefinedComponent.Update()
        {
            Update();
        }

        private unsafe void Update()
        {
            PerfStats.Record("UpdateComponent");
            fixed (InternalTextSprite* sp = &this.sprite)
            {
                NativeMethods.UpdateComponentPartial(this.Row.World.Binding.world, (int)this.Row.Entity.ComponentType, Row.Entity.ID, new IntPtr(sp),
                    sizeof(Entity.InternalEntity) + sizeof(CustomComponentSink.Properties) + sizeof(ITransform.InternalTransform), sizeof(InternalTextSprite));
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && this.initialized)
            {
                InternalTextData data = (InternalTextData)Marshal.PtrToStructure(sprite.data, typeof(InternalTextData));

                if (data.cleanup == ManagedCleaup)
                {
                    NativeMethods.Free(data._text);
                    NativeMethods.Free(data._font);
                    NativeMethods.Free(sprite.data);
                }
            }

            base.Dispose(disposing);
        }

        ~ITextSprite()
        {
            this.Dispose(true);
        }

        private void DeserializeFont(ref InternalTextData data)
        {
            int size = sizeof(char) * (this.font.Length + 1);
            data._font = NativeMethods.Allocate(size);
            var array = new char[this.font.Length + 1];
            Array.Copy(this.font.ToArray(), 0, array, 0, this.font.Length);
            Marshal.Copy(array, 0, data._font, this.font.Length + 1);
        }

        private void DeserializeText(ref InternalTextData data)
        {
            if (data._text != IntPtr.Zero)
                NativeMethods.Free(data._text);

            int size = sizeof(char) * (this.text.Length + 1);
            data._text = NativeMethods.Allocate(size);
            var array = new char[this.text.Length + 1];
            Array.Copy(this.text.ToArray(), 0, array, 0, this.text.Length);
            Marshal.Copy(array, 0, data._text, this.text.Length + 1);
        }

        public void OnDeserialization(object sender)
        {
            sprite.data = NativeMethods.Allocate(Marshal.SizeOf(typeof(InternalTextData)));
            sprite._vertid = -1;

            InternalTextData data = (InternalTextData)Marshal.PtrToStructure(sprite.data, typeof(InternalTextData));
            data.cleanup = ManagedCleaup;
            data._font = IntPtr.Zero;
            data._text = IntPtr.Zero;
            data.centered = 0;
            this.initialized = true;

            DeserializeFont(ref data);

            DeserializeText(ref data);
            
            sprite.dirty = 1;

            this.initialized = true;

            Marshal.StructureToPtr(data, sprite.data, false);

            this.Update();
        }
    }
}
