﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Islander.Managed
{
    [Flags]
    public enum LoggingLevels
    {
        None=0,
        All=0xff,
        Rendering = 1 << 0,
	    Resources = 1 << 1,
        Debug = 1 << 2,
        Managed = 1 << 4,
    }

    public static class Logger
    {
        public static void Log(string msg, LoggingLevels levels)
        {
            NativeMethods.Log(msg, (int)levels);
        }

        public static void Log(string msg, int levels)
        {
            NativeMethods.Log(msg, levels);
        }

        public static void SetLogLevels(LoggingLevels levels)
        {
            NativeMethods.SetLogLevels((int)levels);
        }

        public static void SetLogLevels(int levels)
        {
            NativeMethods.SetLogLevels(levels);
        }
    }

    public static class PerfStats
    {
        private static Dictionary<string, int> Data = new Dictionary<string, int>();

#if DEBUG
        public static void Record(string method)
        {
            if (Data.ContainsKey(method))
            {
                Data[method]++;
            }
            else
            {
                Data.Add(method, 1);
            }
        }
#else
        public static void Record(string method)
        {
            // should be optimized away
        }
#endif

        public static void DumpStats()
        {
            string msg = "Perf Stats:";
            foreach (var pair in Data)
            {
                msg += Environment.NewLine + pair.Key + " " + pair.Value.ToString();
            }

            if (!string.IsNullOrEmpty(msg))
            {
                Logger.Log(msg, LoggingLevels.Managed);
            }
        }
    }
}
