========================================================================
    Islander Managed Overview
========================================================================

Provides an interface to create entities and components which can be
queried on both the .net and the native side.

A native application which wishes to host the .net side, should first
create a channel via the native scripting library and then invoke the
Islander.Managed.Hosting.Initialize method with the name of the channel.
Then it should load the required .net libraries using the
Islander.Managed.Hosting.LoadLibrary method. From there it can create
scripts using the Islander.Managed.Hosting.CreateScript method to create
an instance of the script in question.

To create a script, a .net class should be defined which has inherited
the IScript base class. The script can override the OnLoad, OnTriggered
& OnActivate methods which can be called at the discretion of the native
container. Further the script may create bindings, this is done by defining
a property with the Binding attribute. This can be used by the container to
feed values into properties. Similarly, with methods these can be invoked by
decorating them with a Action attribute. This allows them to handle adhoc actions,
therefore not be limited by the standard set of OnLoad, OnTriggered & OnActivate.

The script can access the shared world object using the ScriptingSystem class which
is a property on the IScript itself. This allows the creation of entities and components.
Another feature of the ScriptingSystem is the QueueEvent family of methods which sends
a message down the channel to the container. The container can then process the event.
Optionally, a callback can be supplied, this is handy for events which could be called
after a amount of time for example.
