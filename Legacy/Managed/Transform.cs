﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;
using System.Text;

namespace Islander.Managed
{
    [Serializable]
    public class ITransform : IManagedComponent, WellDefinedComponent
    {
        [StructLayout(LayoutKind.Sequential)]
        internal struct InternalTransform
        {
            public float _px;
            public float _py;
            public float _pz;
            public float _sx;
            public float _sy;
            public float _sz;
            public float _dx;
            public float _dy;
            public float _dz;
            public float _shearX;
            public float _shearY;
        }

        private Vec3<float> position;
        private Vec3<float> scale;
        private Vec3<float> direction;
        private Vec2<float> shear;
        private bool suspended;

        public ITransform() : base("Transform")
        {
            this.suspended = true;
            this.Position = new Vec3<float>(0, 0, 0);
            this.Scale = new Vec3<float>(0, 0, 0);
            this.Direction = new Vec3<float>(0, 0, 0);
            this.Shear = new Vec2<float>(0, 0);
            this.suspended = false;
        }

        public unsafe void CreateNew()
        {
            InternalTransform t = new InternalTransform();
            t._px = this.position.X;
            t._py = this.position.Y;
            t._pz = this.position.Z;
            t._sx = this.scale.X;
            t._sy = this.scale.Y;
            t._sz = this.scale.Z;
            t._dx = this.direction.X;
            t._dy = this.direction.Y;
            t._dz = this.direction.Z;
            t._shearX = this.shear.X;
            t._shearY = this.shear.Y;
        }

        private static HashSet<ITransform> updates = new HashSet<ITransform>();
        private static bool throttleUpdates;
        /// <summary>
        /// Maintains performance when doing large numbers of updates to transform components.
        /// </summary>
        public static void BeginBatchUpdate()
        {
            throttleUpdates = true;
        }

        public static void EndBatchUpdate()
        {
            throttleUpdates = false;
            foreach (var transform in updates)
            {
                transform.Update();
            }
            updates.Clear();
        }

        void WellDefinedComponent.Update()
        {
            Update();
        }

        private unsafe void Update()
        {
            if (!suspended && this.Row != null)
            {
                if (throttleUpdates)
                {
                    updates.Add(this);
                }
                else
                {
                    InternalTransform t = new InternalTransform();
                    t._px = this.position.X;
                    t._py = this.position.Y;
                    t._pz = this.position.Z;
                    t._sx = this.scale.X;
                    t._sy = this.scale.Y;
                    t._sz = this.scale.Z;
                    t._dx = this.direction.X;
                    t._dy = this.direction.Y;
                    t._dz = this.direction.Z;
                    t._shearX = this.shear.X;
                    t._shearY = this.shear.Y;

                    PerfStats.Record("UpdateComponent");
                    NativeMethods.UpdateComponentPartial(this.Row.World.Binding.world, (int)this.Row.Entity.ComponentType, Row.Entity.ID, new IntPtr(&t),
                        sizeof(Entity.InternalEntity) + sizeof(CustomComponentSink.Properties), sizeof(InternalTransform));
                }
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                updates.Remove(this);
            }

            base.Dispose(disposing);
        }

        public Vec3<float> Position
        {
            get
            {
                return this.position;
            }
            set
            {
                if (this.position != null)
                {
                    this.position.ValueChanged -= position_ValueChanged;
                }

                this.position = value;
                this.Update();

                if (this.position != null)
                {
                    this.position.ValueChanged +=position_ValueChanged;
                }
            }
        }

        void position_ValueChanged(object sender, VectorEventArgs e)
        {
            this.Update();
        }

        public Vec3<float> Scale
        {
            get
            {
                return this.scale;
            }
            set
            {
                if (this.scale != null)
                {
                    this.scale.ValueChanged -= scale_ValueChanged;
                }

                this.scale = value;
                this.Update();

                if (this.scale != null)
                {
                    this.scale.ValueChanged += scale_ValueChanged;
                }
            }
        }

        void scale_ValueChanged(object sender, VectorEventArgs e)
        {
            this.Update();
        }

        public Vec3<float> Direction
        {
            get
            {
                return this.direction;
            }
            set
            {
                if (this.direction != null)
                {
                    this.direction.ValueChanged -= direction_ValueChanged;
                }

                this.direction = value;
                this.Update();

                if (this.direction != null)
                {
                    this.direction.ValueChanged += direction_ValueChanged;
                }
            }
        }

        void direction_ValueChanged(object sender, VectorEventArgs e)
        {
            this.Update();
        }

        public Vec2<float> Shear
        {
            get
            {
                return this.shear;
            }
            set
            {
                if (this.shear != null)
                {
                    this.shear.ValueChanged -= shear_ValueChanged;
                }

                this.shear = value;
                this.Update();

                if (this.shear != null)
                {
                    this.shear.ValueChanged += shear_ValueChanged;
                }
            }
        }

        void shear_ValueChanged(object sender, VectorEventArgs e)
        {
            this.Update();
        }
    }
}
