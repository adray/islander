﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Islander.Managed
{
    public enum VectorValue
    {
        X,
        Y,
        Z,
    }

    public sealed class VectorEventArgs : EventArgs
    {
        public VectorValue Value { get; private set; }

        public VectorEventArgs(VectorValue value)
        {
            this.Value = value;
        }
    }

    [Serializable]
    public class Vec2<T>
    {
        private T x;
        private T y;
        public event EventHandler<VectorEventArgs> ValueChanged;
        private static VectorEventArgs eX = new VectorEventArgs(VectorValue.X);
        private static VectorEventArgs eY = new VectorEventArgs(VectorValue.Y);

        public Vec2(T x, T y)
        {
            this.X = x;
            this.Y = y;
        }

        public T X
        {
            get
            {
                return this.x;
            }
            set
            {
                this.x = value;
                this.OnValueChanged(eX);
            }
        }

        public T Y
        {
            get
            {
                return this.y;
            }
            set
            {
                this.y = value;
                this.OnValueChanged(eY);
            }
        }

        protected void OnValueChanged(VectorEventArgs e)
        {
            if (this.ValueChanged != null)
            {
                this.ValueChanged(this, e);
            }
        }
    }

    [Serializable]
    public class Vec3<T> : Vec2<T>
    {
        private T z;
        private static VectorEventArgs eZ = new VectorEventArgs(VectorValue.Z);

        public Vec3(T x, T y, T z)
            : base(x, y)
        {
            this.z = z;
        }

        public T Z
        {
            get
            {
                return this.z;
            }
            set
            {
                this.z = value;
                this.OnValueChanged(eZ);
            }
        }
    }

    [Serializable]
    public class Vec2f : Vec2<float>
    {
        public Vec2f()
            : base(0, 0)
        { }

        public Vec2f(float x, float y)
            : base(x, y)
        { }
    }

    [Serializable]
    public class Vec3f : Vec3<float>
    {
        public Vec3f()
            : base(0, 0, 0)
        { }

        public Vec3f(float x, float y, float z)
            : base(x, y, z)
        { }
    }

    public class Matrix : IDisposable
    {
        private IntPtr ptr;

        private Matrix(IntPtr ptr)
        {
            this.ptr = ptr;
        }

        public static Matrix CreateRotationMatrix(float rx, float ry, float rz)
        {
            return new Matrix(NativeMethods.CreateRotationMatrix(rx, ry, rz));
        }

        public static Matrix CreateTransformationMatrix(float px, float py, float pz, float rx, float ry, float rz, float sx, float sy, float sz)
        {
            return new Matrix(NativeMethods.CreateTransformationMatrix(px, py, pz, rx, ry, rz, sx, sy, sz));
        }

        public void Dispose()
        {
            if (this.ptr != IntPtr.Zero)
            {
                NativeMethods.DestroyMatrix(this.ptr);
                this.ptr = IntPtr.Zero;
            }
            GC.SuppressFinalize(this);
        }

        ~Matrix()
        {
            if (this.ptr != IntPtr.Zero)
            {
                NativeMethods.DestroyMatrix(this.ptr);
                this.ptr = IntPtr.Zero;
            }
        }

        public Vec3f TransformVector(float x, float y, float z)
        {
            unsafe
            {
                NativeMethods.TransformVector(this.ptr, new IntPtr(&x), new IntPtr(&y), new IntPtr(&z));
            }

            return new Vec3f(x, y, z);
        }
    }
}
