﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;
using System.Text;

namespace Islander.Managed
{
    public interface IEntity
    {
        T AddComponent<T>() where T : IManagedComponent, new();
        T GetComponent<T>() where T : IManagedComponent, new();
        bool HasComponent<T>() where T : IManagedComponent, new();
        void RemoveComponent<T>() where T : IManagedComponent, new();
        T AddComponent<T>(T component) where T : IManagedComponent, new();
        int ID { get; }
        bool Active { get; set; }
    }

    [Serializable]
    internal class Entity : IEntity
    {
        private World world;
        private int id;
        private int active;
        private Dictionary<Type, object> components
            = new Dictionary<Type, object>();
        private WorldComponents componentType = WorldComponents.Entity;
        private int size;

        public Entity(World world)
        {
            this.world = world;
            PerfStats.Record("CreateEntity");
            this.id = NativeMethods.CreateEntity(this.world.Binding.world);
            this.active = 1;
        }

        [StructLayout(LayoutKind.Sequential)]
        internal struct InternalEntity
        {
            public int id;
            public int active;
        }

        internal WorldComponents ComponentType => componentType;
        internal int Size => size;

        public int ID
        {
            get
            {
                return this.id;
            }
        }

        private unsafe void UpdateWorldComponent()
        {
            WorldComponents next = WorldComponents.Entity;
            size = sizeof(InternalEntity) + sizeof(CustomComponentSink.Properties);
            if (components.ContainsKey(typeof(IMesh)))
            {
                next = WorldComponents.Mesh;
                size = sizeof(IMesh.InternalMesh) + sizeof(ITransform.InternalTransform) + sizeof(InternalEntity) + sizeof(CustomComponentSink.Properties);
            }
            else if (components.ContainsKey(typeof(ISprite)))
            {
                next = WorldComponents.Sprite;
                size = sizeof(ISprite.InternalSprite) + sizeof(ITransform.InternalTransform) + sizeof(InternalEntity) + sizeof(CustomComponentSink.Properties);
            }
            else if (components.ContainsKey(typeof(ITextSprite)))
            {
                next = WorldComponents.Text;
                size = sizeof(ITextSprite.InternalTextSprite) + sizeof(ITransform.InternalTransform) + sizeof(InternalEntity) + sizeof(CustomComponentSink.Properties);
            }
            else if (components.ContainsKey(typeof(ITransform)))
            {
                next = WorldComponents.Transform;
                size = sizeof(ITransform.InternalTransform) + sizeof(InternalEntity) + sizeof(CustomComponentSink.Properties);
            }

            if (next != this.componentType)
            {
                NativeMethods.RemoveComponent(world.Binding.world, (int)this.componentType, this.ID);

                this.componentType = next;
                byte* data = stackalloc byte[size]; // TODO: zero memory?

                InternalEntity* entity = (InternalEntity*)data;
                entity->active = this.active;
                entity->id = this.id;

                NativeMethods.AddComponent(world.Binding.world, (int)this.componentType, new IntPtr(data));
                foreach (var component in this.components)
                {
                    var wdc = component.Value as WellDefinedComponent;
                    var m = component.Value as IManagedComponent;
                    if (wdc != null && m.Row != null) wdc.Update();
                }
            }
        }

        public T AddComponent<T>(T component) where T : IManagedComponent, new()
        {
            if (this.components.ContainsKey(typeof(T)))
            {
                throw new InvalidOperationException("Component already exists");
            }
            this.components.Add(typeof(T), component);
            this.UpdateWorldComponent();
            component.Init(new Row()
                {
                    Entity = this,
                    World = this.world,
                });
            if (component is WellDefinedComponent)
            {
                ((WellDefinedComponent)component).CreateNew();
            }
            return component;
        }

        public T AddComponent<T>() where T : IManagedComponent, new()
        {
            return AddComponent<T>(new T());
        }

        public T GetComponent<T>() where T : IManagedComponent, new()
        {
            object c = null;
            if (this.components.TryGetValue(typeof(T), out c))
            {
                return (T)c;
            }
            return AddComponent<T>();
        }

        public bool HasComponent<T>() where T : IManagedComponent, new()
        {
            return this.components.ContainsKey(typeof(T));
        }

        public void RemoveComponent<T>() where T : IManagedComponent, new()
        {
            this.components.Remove(typeof(T));
        }

        public unsafe bool Active
        {
            get
            {
                return this.active == 1;
            }
            set
            {
                int val = value ? 1 : 0;
                if (val != this.active)
                {
                    this.active = val;

                    InternalEntity entity = new InternalEntity();
                    entity.active = this.active;
                    entity.id = this.id;

                    PerfStats.Record("UpdateComponent");
                    NativeMethods.UpdateComponentPartial(this.world.Binding.world, (int)this.componentType, this.id, new IntPtr(&entity), 0, sizeof(InternalEntity));
                }
            }
        }
    }
}
